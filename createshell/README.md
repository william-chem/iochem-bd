# Create shell client 

The project has two main functionalities:

1. it allows to operate *Create* module from a linux terminal. 
1. it is bundled inside *Create* project to convert textual files into CML using *jumbo-saxon* converter and its templates from *jumbo-converters*.

## Usage

### As a shell utility   

The project is bundled as a fat jar with all of its dependencies and some other script files and utilities (located at *scripts/* folder).

It allows to login, navigate, create content, convert and upload calculations, among others.

When a user demands to download the shell utility from the Create web interface, the Create module will pick the aforementioned bundle 
and append a configuration text file with user info. Then, the user will download it as a *.zip* file, which can be used on Linux desktop clients or inside clusters.

More information about shell client usage on [this page](https://docs.iochem-bd.org/en/latest/guides/usage/uploading-content-to-create/using-shell-client.html#shell-client).

### As a conversion package

Create module project will use this project as a dependency to perform the file data conversion from textual files to Chemical Markup Language (CML), please review its usage inside the following class: 

```java
cat.iciq.tcg.labbook.zk.components.uploadtoolbar.ConversionService
```

Especially its method *convertFiles()*.
