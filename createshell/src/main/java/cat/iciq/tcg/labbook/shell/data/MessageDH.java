/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.data;

import java.io.Serializable;
import java.util.Set;
import java.util.Vector;

public class MessageDH implements IDataHolder,Serializable {

	private String titleMessage;
	private String bodyMessage;
	private boolean error;
	
	public MessageDH(String titleMessage, String bodyMessage,boolean error) {
		super();
		this.titleMessage 	= titleMessage;
		this.bodyMessage 	= bodyMessage;
		this.error 			= error;
	}

	@Override
	public String getBodyMessage() {

		return bodyMessage;
	}

	@Override
	public int getColumnType(int column) {
		return 0;
	}

	@Override
	public Vector<Float> getFloatColumn(int column) {
		return null;
	}

	@Override
	public Vector<Integer> getIntegerColumn(int column) {
		return null;
	}

	@Override
	public Vector<String> getRowAsString(int row) {
		return null;
	}

	@Override
	public Vector<String> getStringColumn(int column) {
		return null;
	}

	@Override
	public String getTitleMessage() {
		return titleMessage;
	}

	@Override
	public boolean isError() {
		return error;
	}

	@Override
	public void setFloatColumn(int column, Vector<Float> values) {

	}

	@Override
	public void setIntegerColumn(int column, Vector<Integer> values) {

	}

	@Override
	public void setStringColumn(int column, Vector<String> values) {

	}

	@Override
	public int getMatrixRows() {
		return 0;
	}

	@Override
	public String getElement(int i) {

		return null;
	}

	@Override
	public int getNumElements() {
		return 0;
	}

	@Override
	public void setElement(String element) {
		
	}

	@Override
	public void setElementAt(String element, int pos) {
		
		
	}

	@Override
	public Set<String> getKeys() {
		
		return null;
	}

	@Override
	public String getValue(String key) {
		
		return null;
	}

	@Override
	public void setKeyValue(String key, String value) {
		
		
	}

	@Override
	public Set<String> getHiddenData() {
		return null;
	}

	@Override
	public void setHiddenData(String str) {
		
	}

}
