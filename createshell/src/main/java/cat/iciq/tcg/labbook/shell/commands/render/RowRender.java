/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.render;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Vector;

import cat.iciq.tcg.labbook.shell.data.IDataHolder;

public class RowRender implements IRender, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private IDataHolder			data;
	private static final String SEPARATOR 			= ":";
	private static final String TITLE_SEPARATOR 			= "----------------------------------------------------------";
	private String 				mainTitle;
	private Vector<String>		titles;
	
	public RowRender()
	{
		this.titles = new Vector<String>();
	}
	public void addTitle(String title)
	{
		titles.add(title);
	}

	public void setData(IDataHolder data)
	{
		this.data = data;
	}
	
	public void setMainTitle(String title)
	{
		this.mainTitle = title;
	}
	@Override
	public void render(PrintStream output) {
		
		String firstLine = "Printing the contents of " + mainTitle;
		output.println(firstLine);
		output.println(TITLE_SEPARATOR.substring(0,firstLine.length()));
		for (int i=0;i<data.getNumElements();i++)
		{
			if(!"Mets file".equals(titles.get(i)))
				output.println(titles.get(i) + SEPARATOR + data.getElement(i));
		}
		if (data.getKeys().size()>0)
		{
			output.println("---------------------------");
			output.println("Key - Values pair printing:");
			output.println("---------------------------");
			for (String key:data.getKeys())
			{
				output.println("<<<" + key + ">>>");
				output.println("[[[" + data.getValue(key) + "]]]");
			}
		}

	}
	

}
