/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.DefaultRender;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class CPro extends Command implements ICommand{

	
	public CPro(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler,CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] wholeParameters        = {"-n","-d","-cg"};
		String[] mandatoryParameters 	= {"-n","-d"};
		String[] optionalParameters  	= {};
		String[] singleValueParameters 	= wholeParameters;
		String[] integerFields			= {};
		String[] stringFields			= wholeParameters;
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-n",
			 targetHost,byteResponseHandler,httpClient,shell	);
		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", 	" Name of the project                  (mandatory)");
		help.put("-d", 	" Detailed Description of the project  (mandatory)");
		help.put("-cg", " Concept Group of the project         (optional)");
		
		this.help = new HelpRender("cpro","cpro description",help);
		
	}
	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
		
		String name = parameters.get("-n");
		String desc = parameters.get("-d");
		String cg   = parameters.get("-cg");
		String path = shell.getPath();
					
		//Clear symbols and blank spaces
		name = normalizeField(name);
		
		if(cg == null)
			cg = "PRO";
		
		renewHttpParm();		
        httpParam.add(new BasicNameValuePair("name", 		name));
        httpParam.add(new BasicNameValuePair("description",	desc));
        httpParam.add(new BasicNameValuePair("cg", 			cg  ));
        httpParam.add(new BasicNameValuePair("path", 		path));
        
		IDataHolder data = executeHttpMethod("userCommands/cpro",	httpParam);

		if (data.isError()) return new ErrorRender(data.getTitleMessage(),data.getBodyMessage());
		else				return new DefaultRender();
	}
	@Override
	public String getDefOption()
	{
		return "-n";
	}

}
