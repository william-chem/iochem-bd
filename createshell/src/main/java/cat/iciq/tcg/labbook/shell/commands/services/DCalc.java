/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.DefaultRender;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;
import cat.iciq.tcg.labbook.shell.utils.Paths;

public class DCalc extends Command implements ICommand{

	
	public DCalc(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler,CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] mandatoryParameters 	= {"-n"};
		String[] optionalParameters  	= {};
		String[] singleValueParameters 	= mandatoryParameters;
		String[] integerFields			= {};
		String[] stringFields			= mandatoryParameters;
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-n",
			 targetHost,byteResponseHandler,httpClient,shell	);
		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", 	" Relative or absolute project path           (mandatory)");
		
		this.help = new HelpRender("dcalc","Remove a calculation from a project.",help);
		
	}
	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
        String newPath = parameters.get("-n");
        
        if (newPath.equals(GeneralConstants.PARENT_PROJECT))
        {
        	newPath = Paths.getParent(shell.getPath());
        }
        
        // newPath and path have a value.
        renewHttpParm();
        httpParam.add(new BasicNameValuePair("newPath", 	newPath				)	);
        httpParam.add(new BasicNameValuePair("path", 		shell.getPath()     )	);
        
		IDataHolder data = executeHttpMethod("userCommands/dcalc",	httpParam);

		if (data.isError()) return new ErrorRender(data.getTitleMessage(),data.getBodyMessage());
		else				
		{
			return new DefaultRender();
		}
	}
	@Override
	public String getDefOption()
	{
		return "-n";
	}

}
