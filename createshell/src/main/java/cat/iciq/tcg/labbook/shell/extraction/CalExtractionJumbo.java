/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.extraction;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentMap;

import javax.xml.xpath.XPathExpressionException;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.util.StringUtils;
import org.xmlcml.cml.converters.DynamicConverter;

import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.ExtractionException;
import cat.iciq.tcg.labbook.shell.extraction.utils.Resolver;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import net.sf.jmimemagic.Magic;
import net.sf.jmimemagic.MagicMatch;

public class CalExtractionJumbo implements Callable<Map<String,String>>{

	private static Logger logger  = LogManager.getLogger(CalExtractionJumbo.class.getName());
	
	private static final String OUTPUT = "-o";
	private static final String EXTRACTION_METHOD = "jumbo";
	private static final String METS_FILE_NAME = "mets.xml";
	private static final String APPEND_USE = "append";
	private static final String OUTPUT_USE = "output";
	private static final String OUTPUT_FILENAME = "output.cml";
	public  static final String EXTRACTION_FOLDER = "extracted-areas";
	
	private static final String APPEND_REMOVE_TAGS_EXPR = "(<\\?xml version=\"1\\.0\" encoding=\"[Uu][Tt][Ff]-8\"\\?>)";
	
	private static HashMap<String,ArrayList<String>> calTypeToJumInType = new HashMap<>();  // JUMBO conversion input type
	private static HashMap<String,ArrayList<String>> calTypeToJumOutType = new HashMap<>(); // JUMBO conversion output type
	private static HashMap<String,ArrayList<String>> calTypeToJumClass = new HashMap<>(); 	// JUMBO conversion main class
	private static HashMap<String,ArrayList<String>> calTypeToMimetype = new HashMap<>(); 	// mimetype
	private static HashMap<String,ArrayList<String>> calTypeToUse = new HashMap<>();		// file use
	private static HashMap<String,ArrayList<String>> calTypeToLabel = new HashMap<>();		// type label
	private static HashMap<String,ArrayList<String>> calTypeToRequires = new HashMap<>();		// requires
	private static HashMap<String,ArrayList<String>> calTypeToRenameTo = new HashMap<>();		// rename to
	
	private MatrixDH calDefinitions = null;
	private ConcurrentMap<String, String> filesToProcess = null;
	private String sourceDirectory = null;
	private String temporalDirectory = null;
	private File extractionDirectory = null;
	private Map<String, String> params = null;	
		
	
	public CalExtractionJumbo(MatrixDH calDefinitions,ConcurrentMap<String,String> filesToProcess,String sourceDirectory, String temporalDirectory) {					
		this.params = new HashMap<>();
		this.calDefinitions = calDefinitions;
		this.filesToProcess = filesToProcess;
		this.sourceDirectory = sourceDirectory;
		this.temporalDirectory = temporalDirectory; 
	}
	
	public CalExtractionJumbo(MatrixDH calDefinitions,ConcurrentMap<String,String> filesToProcess,String temporalDirectory, Map<String,String> params) {	
		this.calDefinitions = calDefinitions;
		this.filesToProcess = filesToProcess;
		this.sourceDirectory = temporalDirectory;
		this.temporalDirectory = temporalDirectory;
		this.params = params;
	}
	
	public void setEnvironmentProperties(String templateHostname, String institutionName) {	
		System.setProperty("JUMBO_TEMPLATE_BASE_URL",templateHostname);
		System.setProperty("INSTITUTION_NAME",institutionName);				
	}
	
	@Override
	public HashMap<String,String> call() throws Exception {
		ExtractionException extExc = null;
		LinkedHashMap <String, String> appendedFiles = new LinkedHashMap<>(); 
		//StringBuilder appendedFiles = new StringBuilder();
		TreeMap<String,String> abrToFileToProcess = new TreeMap<>(new AbbreviationComparator());
		String abrCalType = null;
		String jumClass = null;
		String jumInType = null;
		String jumOutType = null;
		String mimetype = null;
		String use = null;
		String label = null;
		String requires = null;
		String renameTo = null;
		
		double size = -1;
		boolean isLastOutputFile = false;
				
		String destinationFile = null;
		String destinationPath = null;
		
		String append = null; 
		DynamicConverter converter = null; 
		try{
			readCalculationTypeDefinitions(calDefinitions);		
			createTemporalDirectory();									
			String detectedFormat = parseFilesAndParameters(abrToFileToProcess, filesToProcess, sourceDirectory);
					
			MetsFileHandler metsFile = new MetsFileHandler();

			for(String abrToProcess:abrToFileToProcess.keySet()){  									// for each abbreviation involved in this upload
				for(String filename : abrToFileToProcess.get(abrToProcess).split("#")){				// for each file associated with this abbreviation, can have the form file1#file2
					abrCalType 	= abrToProcess + "#" + detectedFormat;	
					for(int inx = 1; inx <= calTypeToJumClass.get(abrCalType).size(); inx++){		// for each use for same file and abbreviation
						File sourceFile = new File(sourceDirectory + File.separatorChar + filename);
						jumClass = calTypeToJumClass.containsKey(abrCalType)?calTypeToJumClass.get(abrCalType).get(inx-1):"";
						jumInType = calTypeToJumInType.containsKey(abrCalType)?calTypeToJumInType.get(abrCalType).get(inx-1):"";
						jumOutType = calTypeToJumOutType.containsKey(abrCalType)?calTypeToJumOutType.get(abrCalType).get(inx-1):"";
						mimetype = calTypeToMimetype.containsKey(abrCalType)?calTypeToMimetype.get(abrCalType).get(inx-1): MimeTypeUtils.getContentTypeByFileName(sourceFile);;
						use = calTypeToUse.containsKey(abrCalType)?calTypeToUse.get(abrCalType).get(inx-1):"";
						label = calTypeToLabel.containsKey(abrCalType)?calTypeToLabel.get(abrCalType).get(inx-1).replaceAll("\\(.*\\)", ""):""; //Replace possible additional info enclosed in parentheses
						requires = calTypeToRequires.containsKey(abrCalType)?calTypeToRequires.get(abrCalType).get(inx-1): null;
						renameTo = calTypeToRenameTo.containsKey(abrCalType)?calTypeToRenameTo.get(abrCalType).get(inx-1): "";


						isLastOutputFile = use.equals(OUTPUT_USE) && abrToFileToProcess.get(abrToProcess).endsWith(filename);																			
												
						destinationFile = getFileName(isLastOutputFile, use , sourceFile.getName(), renameTo);						
						destinationPath =  Paths.get(extractionDirectory.getAbsolutePath(), destinationFile).toString();
						
						logger.info("Converting %s file : %s to  %s file : %s  JumClass: %s", jumInType, sourceFile.getCanonicalPath(), jumOutType, destinationPath, jumClass);
						append = getAppendContent(isLastOutputFile, requires, appendedFiles);
						
						converter = new DynamicConverter(jumClass, sourceFile.getCanonicalPath(), destinationPath, append);
						converter.call();
						if(use.equals(APPEND_USE) || (use.equals(OUTPUT_USE) && !isLastOutputFile)) {
						    addToAppendedFiles(appendedFiles, jumOutType, destinationPath);						
						} else {								
							String md5Sum = DigestUtils.md5Hex(new FileInputStream(destinationPath));
							size = new File(destinationPath).length();
							metsFile.addFile(jumInType, jumOutType, jumClass, mimetype, md5Sum, use, destinationFile, label, size);
						}						
					}
				}
			}
			
			//Save mets file
			try {
				metsFile.saveToFile(extractionDirectory.getAbsolutePath() + File.separatorChar + METS_FILE_NAME);
			} catch (IOException e) {
				extExc = new ExtractionException();
				extExc.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
				extExc.setMessage(CommandsMessages.EXT_METS_FILE_GEN.MESSAGE());			
				throw extExc;
			}				
			
			if(params != null)
				params.put("type", detectedFormat);
		
		}catch(ExtractionException e){
			if(params != null){			//Used inside web interface to discover which user launched it and it's associated thread uuid 
				e.setThreadUUID(params.get("UUID"));
				e.setUser(params.get("userLogin"));
			}				
			throw e;
		}catch(IOException e){
			ExtractionException ext = new ExtractionException(e.getMessage(), e.getCause());
			if(params != null){			//Used inside web interface to discover which user launched it and it's associated thread uuid 
				ext.setThreadUUID(params.get("UUID"));
				ext.setUser(params.get("userLogin"));
			}
			ext.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
			ext.setMessage(CommandsMessages.EXT_FILE_IO_EXCEPTION.MESSAGE());
			throw ext;
		} catch (Exception e) {
			ExtractionException ext = new ExtractionException(e.getMessage(), e.getCause());
			if(params != null){			//Used inside web interface to discover which user launched it and it's associated thread uuid 
				ext.setThreadUUID(params.get("UUID"));
				ext.setUser(params.get("userLogin"));
			}
			ext.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
			ext.setMessage(CommandsMessages.EXT_FILE_CONVERSION_ERROR.MESSAGE());
			throw ext;
		}
		return (HashMap<String, String>) params;			
	}
	
	private void addToAppendedFiles(LinkedHashMap<String, String> appendedFiles, String jumOutType, String destinationPath) throws IOException {
	    String content = FileUtils.readFileToString(new File(destinationPath));
        FileUtils.deleteQuietly(new File(destinationPath));
        content = content.replaceAll(APPEND_REMOVE_TAGS_EXPR, "");      
        if(appendedFiles.containsKey(jumOutType))
            appendedFiles.put(jumOutType, appendedFiles.get(jumOutType) + content);
        else
            appendedFiles.put(jumOutType, content);
    }

    private String getFileName(boolean isLastOutputFile, String use, String filename, String fixedName) {
		String name = null; 
		
		if(isLastOutputFile) {
			name = OUTPUT_FILENAME;  
		}else {
			if(fixedName == null || fixedName.isEmpty()) {
			    name = filename.replaceAll(" ", "_");
			}else {
				name = fixedName;
			}
		}		
		if(use.equals(APPEND_USE))
			name+=".append";		
		return name;		
	}
	
	private String getAppendContent(boolean isLastOutputFile, String requires, LinkedHashMap <String, String> appendedFiles) {
		StringBuilder sb = new StringBuilder();
		if(isLastOutputFile) {									// Output files will require all appended text
			for(String key: appendedFiles.keySet())
				sb.append(appendedFiles.get(key));
		}else if(requires != null && !requires.isEmpty()) {		// Custom append rules can apply also, multivalued using double colon 			
			for(String req : requires.split(":"))
				sb.append(appendedFiles.get(req));
		} else {
			return null;
		}
		
		return sb.toString();
	}
	
	/**
	 * Method use to detect the calculation type, into the file that the user send us.  
	 * @param refFileToProcess File path of file to be detected
	 * @return String definition of program used to generate this file 
	 * @throws FileNotFoundException 
	 * @throws IOException 
	 * @throws XPathExpressionException 
	 */
	
	private String determineCalculationType(String refFileToProcess) throws ExtractionException, FileNotFoundException {
		String format = Resolver.getFileType(new BufferedReader(new FileReader(refFileToProcess)));
		if(format == null){
			ExtractionException extExc = new ExtractionException();
			extExc.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
			extExc.setMessage(CommandsMessages.EXT_FORMAT_NOT_DETECTED.MESSAGE());
			throw extExc;
		}
		return format; 
	}
	
	public File getExtractionDirectory() {
		return extractionDirectory;
	}

	public String getExtractionMethod() {
		return EXTRACTION_METHOD; 
	}
	
	private void readCalculationTypeDefinitions(MatrixDH calDefinitions) {
		if(calTypeToJumClass.size() > 0)
			return;
		
		String calType = null;
		String abr = null;
		String jumClass = null;
		String jumInType = null;
		String jumOutType = null;
		String mimetype = null;
		String use = null;
		String label = null;
		String requires = null;
		String renameTo = null;
				
		for (int i = 0;i<calDefinitions.getMatrixRows();i++) {								
			calType = calDefinitions.getStringColumn(1).elementAt(i);
			abr = calDefinitions.getStringColumn(4).elementAt(i);
			jumClass = calDefinitions.getStringColumn(5).elementAt(i);
			jumInType = calDefinitions.getStringColumn(6).elementAt(i);
			jumOutType = calDefinitions.getStringColumn(7).elementAt(i);
			mimetype = calDefinitions.getStringColumn(8).elementAt(i);
			use = calDefinitions.getStringColumn(9).elementAt(i);
			label = calDefinitions.getStringColumn(10).elementAt(i);
			requires= calDefinitions.getStringColumn(12).elementAt(i);
			renameTo = calDefinitions.getStringColumn(13).elementAt(i);
			
			
			String abrCalType = abr + "#" + calType;							// Ex:  -o#Gaussian
			
			insertPerUse(abrCalType, jumClass, jumInType, jumOutType, mimetype, use, label, requires, renameTo);		
		}
	
	}
	
	private void insertPerUse(String abrCalType, String jumClass, String jumInType, String jumOutType, String mimetype, String use, String label, String requires, String renameTo) {
		if(!useAlreadyStored(abrCalType, use)) {			
			insertIntoMultivaluedHashMap(calTypeToJumClass, abrCalType, jumClass);
			insertIntoMultivaluedHashMap(calTypeToJumInType, abrCalType ,jumInType);
			insertIntoMultivaluedHashMap(calTypeToJumOutType, abrCalType, jumOutType);
			insertIntoMultivaluedHashMap(calTypeToMimetype, abrCalType, mimetype);
			insertIntoMultivaluedHashMap(calTypeToUse, abrCalType, use);
			insertIntoMultivaluedHashMap(calTypeToLabel, abrCalType, label);
			insertIntoMultivaluedHashMap(calTypeToRequires, abrCalType, requires);
			insertIntoMultivaluedHashMap(calTypeToRenameTo, abrCalType, renameTo);
		}
	}
	
	private boolean useAlreadyStored(String abrCalType, String use) {
		if(calTypeToUse.containsKey(abrCalType)){
			Iterator<String> iter = calTypeToUse.get(abrCalType).iterator();
			while(iter.hasNext()){
				String storedUse = iter.next();
				if(storedUse.equals(use))
					return true;
			}			
		}
		return false;
	}
	
	private void insertIntoMultivaluedHashMap(HashMap<String,ArrayList<String>> map, String key, String value) {
		if(map.containsKey(key))
			map.get(key).add(value);
		else{
			ArrayList<String> list = new ArrayList<String>();
			list.add(value);
			map.put(key, list);
		}
	}
	
	private void createTemporalDirectory() throws ExtractionException {
		// creating the tmp directory.
		extractionDirectory = new File(temporalDirectory + File.separatorChar +  EXTRACTION_FOLDER);
		if(extractionDirectory.exists())
			try {
				FileUtils.forceDelete(extractionDirectory);
			} catch (IOException e) {				
				ExtractionException extExc = new ExtractionException();
				extExc.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
				extExc.setMessage(CommandsMessages.EXT_TMPDIR_DELETE_ERROR.MESSAGE());
				throw extExc;				
			}		
		if (!(extractionDirectory.mkdir()))  // if not possible to create the TMP dir.
		{
			ExtractionException extExc = new ExtractionException();
			extExc.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
			extExc.setMessage(CommandsMessages.EXT_TMPDIR.MESSAGE());
			throw extExc;
		}
	}
	
	private String parseFilesAndParameters(Map<String,String> abrToFileToProcess, ConcurrentMap<String,String> filesToProcess, String path) throws ExtractionException {
		boolean hasOutput = false;
		String detectedFormat = null;
		ExtractionException extExc = null;
		
		for (String opFile:filesToProcess.keySet()) // for each file specified as a parameter.
		{
			abrToFileToProcess.put(opFile, filesToProcess.get(opFile));			
			if (opFile.matches(OUTPUT)){							
				hasOutput = true;
				try {	
					for(String filename : filesToProcess.get(opFile).split("#")){
						detectedFormat = determineCalculationType(path + "/" + filename);
						if(detectedFormat != null)
							break;
					}									
				} catch (IOException e) {
					extExc = new ExtractionException();
					extExc.setTitle(CommandsMessages.EXT_TMPDIR.TITLE());
					extExc.setMessage(e.getMessage());
					throw extExc;
				} 								
			}		
		}
		
		if(!hasOutput){
			extExc = new ExtractionException();
			extExc.setTitle(CommandsMessages.EXT_EXPEC_MORE_FILES.TITLE());
			extExc.setMessage(CommandsMessages.EXT_EXPEC_MORE_FILES.MESSAGE());
			throw extExc;
		}
		return detectedFormat;
	}
	
	
	private class AbbreviationComparator implements Comparator<String> {		
		public int compare(String o1, String o2) {		//We'll set always output command as the last one. We do so because it will append XML outputs generated from previously parsed files.
			if(o1.equals(OUTPUT) && o2.equals(OUTPUT))
				return 0;
			else if(o1.equals(OUTPUT))
				return 100 ;
			else if(o2.equals(OUTPUT))
				return -100 - o1.compareTo(o2) ;
			else 
				return o1.compareTo(o2);
		}
		
	}

}

class MimeTypeUtils {

    private static final Map<String, String> fileExtensionMap;
    
    static {
        fileExtensionMap = new HashMap<String, String>();
        // MS Office
        fileExtensionMap.put("doc", "application/msword");
        fileExtensionMap.put("dot", "application/msword");
        fileExtensionMap.put("docx", "application/vnd.openxmlformats-officedocument.wordprocessingml.document");
        fileExtensionMap.put("dotx", "application/vnd.openxmlformats-officedocument.wordprocessingml.template");
        fileExtensionMap.put("docm", "application/vnd.ms-word.document.macroEnabled.12");
        fileExtensionMap.put("dotm", "application/vnd.ms-word.template.macroEnabled.12");
        fileExtensionMap.put("xls", "application/vnd.ms-excel");
        fileExtensionMap.put("xlt", "application/vnd.ms-excel");
        fileExtensionMap.put("xla", "application/vnd.ms-excel");
        fileExtensionMap.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
        fileExtensionMap.put("xltx", "application/vnd.openxmlformats-officedocument.spreadsheetml.template");
        fileExtensionMap.put("xlsm", "application/vnd.ms-excel.sheet.macroEnabled.12");
        fileExtensionMap.put("xltm", "application/vnd.ms-excel.template.macroEnabled.12");
        fileExtensionMap.put("xlam", "application/vnd.ms-excel.addin.macroEnabled.12");
        fileExtensionMap.put("xlsb", "application/vnd.ms-excel.sheet.binary.macroEnabled.12");
        fileExtensionMap.put("ppt", "application/vnd.ms-powerpoint");
        fileExtensionMap.put("pot", "application/vnd.ms-powerpoint");
        fileExtensionMap.put("pps", "application/vnd.ms-powerpoint");
        fileExtensionMap.put("ppa", "application/vnd.ms-powerpoint");
        fileExtensionMap.put("pptx", "application/vnd.openxmlformats-officedocument.presentationml.presentation");
        fileExtensionMap.put("potx", "application/vnd.openxmlformats-officedocument.presentationml.template");
        fileExtensionMap.put("ppsx", "application/vnd.openxmlformats-officedocument.presentationml.slideshow");
        fileExtensionMap.put("ppam", "application/vnd.ms-powerpoint.addin.macroEnabled.12");
        fileExtensionMap.put("pptm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12");
        fileExtensionMap.put("potm", "application/vnd.ms-powerpoint.presentation.macroEnabled.12");
        fileExtensionMap.put("ppsm", "application/vnd.ms-powerpoint.slideshow.macroEnabled.12");
        // Open Office
        fileExtensionMap.put("odt", "application/vnd.oasis.opendocument.text");
        fileExtensionMap.put("ott", "application/vnd.oasis.opendocument.text-template");
        fileExtensionMap.put("oth", "application/vnd.oasis.opendocument.text-web");
        fileExtensionMap.put("odm", "application/vnd.oasis.opendocument.text-master");
        fileExtensionMap.put("odg", "application/vnd.oasis.opendocument.graphics");
        fileExtensionMap.put("otg", "application/vnd.oasis.opendocument.graphics-template");
        fileExtensionMap.put("odp", "application/vnd.oasis.opendocument.presentation");
        fileExtensionMap.put("otp", "application/vnd.oasis.opendocument.presentation-template");
        fileExtensionMap.put("ods", "application/vnd.oasis.opendocument.spreadsheet");
        fileExtensionMap.put("ots", "application/vnd.oasis.opendocument.spreadsheet-template");
        fileExtensionMap.put("odc", "application/vnd.oasis.opendocument.chart");
        fileExtensionMap.put("odf", "application/vnd.oasis.opendocument.formula");
        fileExtensionMap.put("odb", "application/vnd.oasis.opendocument.database");
        fileExtensionMap.put("odi", "application/vnd.oasis.opendocument.image");
        fileExtensionMap.put("oxt", "application/vnd.openofficeorg.extension");
        //Chemical extensions : Taken from Chemical MIME project : http://www.ch.ic.ac.uk/chemime/ 
        fileExtensionMap.put("molden.input", "chemical/x-molden");
        fileExtensionMap.put("molden", "chemical/x-molden");
        fileExtensionMap.put("alc", "chemical/x-alchemy");
        fileExtensionMap.put("csf", "chemical/x-cache-csf");
        fileExtensionMap.put("cbin", "chemical/x-cactvs-binary");
        fileExtensionMap.put("cascii", "chemical/x-cactvs-binary");
        fileExtensionMap.put("ctab", "chemical/x-cactvs-binary");
        fileExtensionMap.put("cdx", "chemical/x-cdx");
        fileExtensionMap.put("cer", "chemical/x-cerius");
        fileExtensionMap.put("chm", "chemical/x-chemdraw");
        fileExtensionMap.put("cif", "chemical/x-cif");
        fileExtensionMap.put("mcif", "chemical/x-mmcif");
        fileExtensionMap.put("c3d", "chemical/x-chem3d");
        fileExtensionMap.put("cmdf", "chemical/x-cmdf");
        fileExtensionMap.put("cpa", "chemical/x-compass");
        fileExtensionMap.put("bsd", "chemical/x-crossfire");
        fileExtensionMap.put("cml", "chemical/x-cml");
        fileExtensionMap.put("csml, csm", "chemical/x-csml");
        fileExtensionMap.put("ctx", "chemical/x-ctx");
        fileExtensionMap.put("cxf", "chemical/x-cxf");
        fileExtensionMap.put("smi", "chemical/x-daylight-smiles");
        fileExtensionMap.put("emb", "chemical/x-embl-dl-nucleotide");
        fileExtensionMap.put("spc", "chemical/x-galactic-spc");
        fileExtensionMap.put("inp, gam", "chemical/x-gamess-input");
        fileExtensionMap.put("gau", "chemical/x-gaussian-input");
        fileExtensionMap.put("fch,fchk", "chemical/x-gaussian-checkpoint");
        fileExtensionMap.put("cub", "chemical/x-gaussian-cube");
        fileExtensionMap.put("gcg", "chemical/x-gcg8-sequence");
        fileExtensionMap.put("gen", "chemical/x-genbank");
        fileExtensionMap.put("istr, ist", "chemical/x-isostar");
        fileExtensionMap.put("jdx, dx", "chemical/x-jcamp-dx");
        fileExtensionMap.put("kin", "chemical/x-kinemage");
        fileExtensionMap.put("mcm", "chemical/x-macmolecule");
        fileExtensionMap.put("mmd, mmod", "chemical/x-macromodel-input");
        fileExtensionMap.put("mol", "chemical/x-mdl-molfile");
        fileExtensionMap.put("rd", "chemical/x-mdl-rdfile");
        fileExtensionMap.put("rxn", "chemical/x-mdl-rxnfile");
        fileExtensionMap.put("sd", "chemical/x-mdl-sdfile");
        fileExtensionMap.put("tgf", "chemical/x-mdl-tgf");
        fileExtensionMap.put("mif", "chemical/x-mif");
        fileExtensionMap.put("mol2", "chemical/x-mol2");
        fileExtensionMap.put("b", "chemical/x-molconn-Z");
        fileExtensionMap.put("mop", "chemical/x-mopac-input");
        fileExtensionMap.put("gpt", "chemical/x-mopac-graph");
        fileExtensionMap.put("asn (old form)", "chemical/x-ncbi-asn1");
        fileExtensionMap.put("val", "chemical/x-ncbi-asn1-binary");
        fileExtensionMap.put("pdb", "chemical/x-pdb");
        fileExtensionMap.put("sw", "chemical/x-swissprot");
        fileExtensionMap.put("vms", "chemical/x-vamas-iso14976");
        fileExtensionMap.put("vmd", "chemical/x-vmd");
        fileExtensionMap.put("xtel", "chemical/x-xtel");
        fileExtensionMap.put("xyz", "chemical/x-xyz");
        fileExtensionMap.put("mdp", "chemical/x-gromacs-input");
        fileExtensionMap.put("gro", "chemical/x-gromos87");
        fileExtensionMap.put("xtc", "chemical/x-gromacs-trajectory");
    }
    
    private MimeTypeUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String getContentTypeByFileName(File file) {
		String fileName = FilenameUtils.getName(file.getAbsolutePath());		
			
        FileNameMap mimeTypes = URLConnection.getFileNameMap();						// 1. first use java's built-in utils
        String contentType = mimeTypes.getContentTypeFor(fileName);
        if(StringUtils.hasText(contentType))
        	return contentType;        
      
        String extension =  fileName.substring(fileName.indexOf(".") +1);		   // 2. nothing found -> lookup our in extension map to find types like ".doc" or ".docx"
        contentType = fileExtensionMap.get(extension);	
        if(contentType != null){
        	return contentType;
        }
        else if(extension.contains(".")){													
        	extension = extension.substring(extension.lastIndexOf(".")+ 1);
        	contentType = fileExtensionMap.get(extension);	
            if(contentType != null)
            	return contentType;            	
        }
                                   
		MagicMatch match;											  	          // 3. Try at last with JMimeMagic library
		int maxLength = 2*1024*1024;
		try {			
			if(file.length() < maxLength)		//Exclude large files
				match = Magic.getMagicMatch(file, true);
			else {			
				byte[] buf = new byte[maxLength];				
				new FileInputStream(file).read(buf,0,maxLength);				
				match = Magic.getMagicMatch(buf, true);
			}
		} catch(Exception e){
			return "application/octet-stream";
		}
		return match.getMimeType();
    }
}
