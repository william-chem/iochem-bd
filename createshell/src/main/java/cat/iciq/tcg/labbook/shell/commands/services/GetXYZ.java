/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.DefaultRender;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.commands.render.MessageRender;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class GetXYZ extends Command implements ICommand {
	
	public GetXYZ(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler,CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] mandatoryParameters 	= {"-n"};
		String[] optionalParameters  	= {"-o"};
		String[] singleValueParameters 	= mandatoryParameters;
		String[] integerFields			= {"-n"};
		String[] stringFields			= {"-o"};
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-n",
			 targetHost,byteResponseHandler,httpClient,shell);
		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", " Calculation number id (mandatory)");
		help.put("-o filename", " Output to file instead of stdout (optional)");		
		this.help = new HelpRender("getxyz","getxyz description",help);
		
	}
	
	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
        String calcId = parameters.get("-n");
        
        renewHttpParm();
        
		String geometry = executeHttpGetMethod("innerServices/xyz?id=" + calcId);
		if(geometry == null)
			 return new ErrorRender("Error retrieving geometry", "Could no retrieve geometry for calculation id = " + calcId);
		
		if(parameters.get("-o") != null){
			saveToFile(parameters.get("-o"), geometry);
			return new DefaultRender();
		}else
			return new MessageRender(geometry);		
	}
	
	@Override
	public String getDefOption()
	{
		return "-n";
	}
	
	private String executeHttpGetMethod(String url)
	{
		HttpGet httpGet = new HttpGet(shell.getAppName() + url);
		try{
			return httpClient.execute(targetHost, httpGet, new BasicResponseHandler());	
		}catch(Exception e){
			return null;
		}  
	}
	
	private void saveToFile(String filename, String geometry){
		File file = new File(filename);
		try (FileOutputStream fop = new FileOutputStream(file)) {
			if (!file.exists()) {
				file.createNewFile();
			}
			byte[] contentInBytes = geometry.getBytes();
			fop.write(contentInBytes);
			fop.flush();
			fop.close();
		} catch (IOException e) {
			
		}			
	}
}
