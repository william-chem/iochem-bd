/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Vector;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.commands.render.ListRender;
import cat.iciq.tcg.labbook.shell.commands.render.MatrixRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;
import cat.iciq.tcg.labbook.shell.utils.Paths;

public class LSPro extends Command implements ICommand{

	private Vector<String> 	titles;
	private Vector<Integer> colSizes;
	private MatrixRender 	mr;
	private ListRender 	    lr;
	
	public LSPro(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler, CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] mandatoryParameters 	= {};
		String[] optionalParameters  	= {"-n","-f","-o"};
		String[] singleValueParameters 	= optionalParameters;
		String[] integerFields			= {};
		String[] stringFields			= optionalParameters;
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-n",
			 targetHost,byteResponseHandler,httpClient,shell	);
		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", 	" Relative or absolute project path                           (optional)");
		help.put("-f", 	" If present means long format in listing                     (optional)");
		help.put("-o", 	" Order by. Possible values:n,o,g,t,c,s                       (optional)\n"+
				        "              n->name, o->owner, g->group,t->time,c->concept,s->state\n"+
				        "              if used -f option is activated automatically");
		
		this.help = new HelpRender("lspro","lspro description",help);
		
		// rendering data:
		titles = new Vector<String>();
		titles.add("Type");
		titles.add("Name");
		titles.add("Description");
		titles.add("Permissions");
		titles.add("Owner");
		titles.add("Group");
		titles.add("Creation time");
		titles.add("Concept Group");
		titles.add("State");
		
		colSizes = new Vector<Integer>();
		colSizes.add(4);
		colSizes.add(10);
		colSizes.add(20);
		colSizes.add(10);
		colSizes.add(10);
		colSizes.add(10);
		colSizes.add(20);
		colSizes.add(20);
		colSizes.add(8);
		
		mr = new MatrixRender();
		mr.setColumnSizes(colSizes);
		mr.setTitles(titles);	
		
		lr = new ListRender();
		
	}
	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		boolean shortFormat = true;
		if (parameters.containsKey("-f")) parameters.put("-f", "dummy"); // TODO: MAYBE CAN BE IMPROVED THIS WAY OF WORKING.
		
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
        String newPath = parameters.get("-n");
        
        if (newPath == null) newPath = shell.getPath();
        
        if (newPath.equals(GeneralConstants.PARENT_PROJECT))
        {
        	newPath = Paths.getParent(shell.getPath());
        }
        renewHttpParm();
        httpParam.add(new BasicNameValuePair("newPath", 	newPath				)	);
        httpParam.add(new BasicNameValuePair("path", 		shell.getPath())		);
        if (parameters.get("-f") != null)
        {
       		shortFormat = false;
        }
        else
        {
    		shortFormat = true;
        }
        
        if (parameters.get("-o") != null)
        {
        	shortFormat = false;
        	String orderby = parameters.get("-o").trim();
        	if (orderby.length()>1)  // more than one letter is not allowed.
        	{
        		return new ErrorRender(CommandsMessages.PRO_LS_MULTI_O_OPTION.TITLE(),CommandsMessages.PRO_LS_MULTI_O_OPTION.MESSAGE());
        	}
        	else
        	{
        		if (!(orderby.matches("[nogtcs]")))
        		{
        			return new ErrorRender(CommandsMessages.PRO_LS_INVALID_O_OPTION.TITLE(),CommandsMessages.PRO_LS_INVALID_O_OPTION.MESSAGE());
        		}
        	}
        	httpParam.add(new BasicNameValuePair("orderby", orderby));
        }
        
		IDataHolder data = executeHttpMethod("userCommands/lspro",	httpParam);

		if (data.isError()) return new ErrorRender(data.getTitleMessage(),data.getBodyMessage());
		else				
		{
			// rendering the contents of the project
			if (shortFormat)
			{
				lr.setData(data);
				return lr;
			}
			else
			{
				mr.setData(data);
				return mr;
			}
			
		}
	}
	@Override
	public String getDefOption()
	{
		return "-n";
	}

}
