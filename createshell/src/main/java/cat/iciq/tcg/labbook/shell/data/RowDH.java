/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

public class RowDH implements IDataHolder,Serializable {

	private Vector<String> data;
	private HashMap<String,String> keyValue;
	private boolean error;
	private Set<String> hiddenData;
	
	public RowDH() {
		super();
		data = new Vector<String>();
		keyValue = new HashMap<String,String>();
		hiddenData = new HashSet<String>();
	}
	
	@Override
	public String getElement(int i)
	{
		return this.data.get(i);
	}
	@Override
	public void setElement(String element)
	{
		this.data.add(element);
	}
	@Override
	public int getNumElements()
	{
		return this.data.size();
	}
	
	@Override
	public String getBodyMessage() {

		return null;
	}

	@Override
	public int getColumnType(int column) {
		return 0;
	}

	@Override
	public Vector<Float> getFloatColumn(int column) {
		return null;
	}

	@Override
	public Vector<Integer> getIntegerColumn(int column) {
		return null;
	}

	@Override
	public Vector<String> getRowAsString(int row) {
		return null;
	}

	@Override
	public Vector<String> getStringColumn(int column) {
		return null;
	}

	@Override
	public String getTitleMessage() {
		return null;
	}

	@Override
	public boolean isError() {
		return error;
	}

	@Override
	public void setFloatColumn(int column, Vector<Float> values) {

	}

	@Override
	public void setIntegerColumn(int column, Vector<Integer> values) {

	}

	@Override
	public void setStringColumn(int column, Vector<String> values) {

	}

	@Override
	public int getMatrixRows() {
		return 0;
	}

	@Override
	public void setElementAt(String element, int pos) {
		this.data.add(pos, element);
		
	}

	@Override
	public Set<String> getKeys() {
		
		return keyValue.keySet();
	}

	@Override
	public String getValue(String key) {
		
		return keyValue.get(key);
	}

	@Override
	public void setKeyValue(String key, String value) {
		
		keyValue.put(key, value);
		
	}

	@Override
	public Set<String> getHiddenData() {
		return hiddenData;
	}

	@Override
	public void setHiddenData(String str) {
		hiddenData.add(str);
	}

}
