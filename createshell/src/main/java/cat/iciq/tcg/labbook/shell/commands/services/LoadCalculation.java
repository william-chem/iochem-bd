/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.CloseableHttpClient;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.commands.render.MessageRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.data.MessageDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.extraction.CalExtractionJumbo;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class LoadCalculation extends Command implements ICommand{

	public String[] notJumboParameters 	= {"-n", "-d", "-dbp", "-dcp", "-autopublish"};
	
	public LoadCalculation(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler, CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] wholeParameters        = {"-n","-d","-i", "-o","-a", "-nc", "-dc", "-oe", "-oc", "-ob", "-as", "-dcp", "-kp", "-t", "-ir", "-p", "-r", "-og", "-xcd", "-rkf1", "-rkf2", "-autopublish"};
		String[] mandatoryParameters 	= {"-n","-d"};
		String[] optionalParameters  	= {"-i","-o","-a", "-nc", "-dc", "-oe", "-oc","-ob", "-as", "-dcp", "-kp", "-t", "-ir", "-p", "-r", "-og", "-rkf1", "-rkf2", "-xcd", "-autopublish"};
		String[] singleValueParameters 	= wholeParameters;
		String[] integerFields			= {};
		String[] stringFields			= wholeParameters;
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-n", targetHost,byteResponseHandler,httpClient,shell	);

		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", "Name of the calculation in the data base.\t\t(mandatory)");
		help.put("-d", "Description of the calculation in the data base.\t(mandatory)");
		help.put("-i", "Input file \t\t\t\t\t\t(mandatory)");
		help.put("-o", "Output file (vasprun.xml on VASP, job.last on Turbomole, log.lammps on LAMMPS. .castep on CASTEP).\t\t\t(mandatory)");		
		help.put("-a", "Additional file.\t\t(optional), for multiple additional files, concatenate all names with #");
		help.put("-nc", "Vasp NEB POSCAR/CONTCAR files\t\t\t\t\t(optional)");
		help.put("-dc", "Vasp DOSCAR file.\t\t\t\t\t(optional)");
		help.put("-kp", "Vasp KPOINTS file.\t\t\t\t\t(optional)");
		help.put("-oe", "Turbomole energy file.\t\t\t\t\t(optional)");
		help.put("-oc", "Coordinate, CASTEP cell file.\t\t\t\t\t(optional)");
		help.put("-ob", "Turbomole basis file.\t\t\t\t\t(optional)");
        help.put("-og", "CASTEP .geom file.\t\t\t\t\t(optional)");
        help.put("-as", "QuantumEspresso absorption spectra data file\t\t(optional)");
        help.put("-xcd", "CASTEP .xcd graph file\t\t(optional)");
		help.put("-t", "LAMMPS/GROMACS/Amber trajectory file.\t\t\t\t(mandatory)");
		help.put("-ir", "Amber Input coordinates or initial .ncrst file.\t\t\t(mandatory)");
		help.put("-p", "LAMMPS/Amber parameter/topology file.\t\t\t\t\t(mandatory)");
		help.put("-r", "Amber final restart file .ncrst.\t\t\t\t\t(mandatory)");
		help.put("-rkf1", "AMS ams.rkf binary file.\t\t\t\t\t(mandatory)");
		help.put("-rkf2", "AMS adf.rkf binary file.\t\t\t\t\t(mandatory)");
		
		help.put("-dcp", "Absolute path in the desktop computer.\t\t\t\t(optional)");
		
		this.help = new HelpRender("loadcalc","This command is for loading calculation into the database.\nThe commands prints the calculation type detected. ",help);	
		
	}
	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
		
		if (parameters.containsKey("-i") && !(parameters.containsKey("-o")))
				return new ErrorRender(CommandsMessages.LC_ERROR_IO_PARAM.TITLE(),CommandsMessages.LC_ERROR_IO_PARAM.MESSAGE());
	
		renewHttpParm();
        
		IDataHolder calTypes = executeHttpMethod("systemUserCommands/getcaltypes",	httpParam);
		if (calTypes.isError()) 
			return new ErrorRender(calTypes.getTitleMessage(),calTypes.getBodyMessage());
		
		String calculationType = null;
		String sourceDirectory = null;
		
		if (parameters.containsKey("-dcp")) sourceDirectory = parameters.get("-dcp");
		else								sourceDirectory = System.getenv().get("PWD"); // pwd system provided variable.
		
		ConcurrentHashMap<String,String> files 	= null;
		HashMap<String,String> filesVersa = null;
		
		files = singleFileParam(parameters);
		
		if (files == null) // some files specified as a parameter and the contents of the directory also contains information to process (files)
		{
			files = listDir(sourceDirectory);
			filesVersa = viceVersa(files);
			
			// adding parameter files:
			String value;
			for (String option:parameters.keySet())
			{
				value = parameters.get(option);
				if (files.containsValue(value)) // of the file is part of the parameters.
				{
					files.remove(filesVersa.get(value));
					files.put(option, value);
				}
			}
		}
		
		String temporalDirectory = createTemporalFolder();		
		CalExtractionJumbo calExt = new CalExtractionJumbo((MatrixDH) calTypes, files, sourceDirectory, temporalDirectory);
		try{			
			HashMap<String, String> params = calExt.call();				//TODO: Implement non blocking thread pool call
			calculationType = params.get("type");
		}
		catch (Exception e){
			FileUtils.deleteQuietly(new File(temporalDirectory));			
			return new ErrorRender("Error during file conversion",e.getMessage());
		}
		
		String name = parameters.get("-n");
		String description = parameters.get("-d");
		//Clear symbols and blank spaces				
		name = normalizeField(name);

		IDataHolder data = storeFiles(calExt.getExtractionDirectory(), calculationType, name, description, calExt.getExtractionMethod());
		FileUtils.deleteQuietly(new File(temporalDirectory));				
		if (data.isError()) 
			return new ErrorRender(data.getTitleMessage(),data.getBodyMessage());
		else 		
			return new MessageRender(((MessageDH)data).getBodyMessage());
		
	}
	
	protected ConcurrentHashMap<String,String> singleFileParam(Hashtable<String,String>param) {		
		ConcurrentHashMap<String,String> toReturn = new ConcurrentHashMap<String,String>();
		Iterator<String> iter = param.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			if(!Arrays.asList(notJumboParameters).contains(key))
				toReturn.put(key, param.get(key));		
		}
		return toReturn;
	}
	
	private String createTemporalFolder() throws IOException{
		String uuid = UUID.randomUUID().toString();
		File systemTemporalDirectory = new File(System.getProperty("java.io.tmpdir"));
		File temporalExtractionDirectory = new File(systemTemporalDirectory.getCanonicalPath() + File.separatorChar + "jumbo_saxon_" + uuid);
		if(!temporalExtractionDirectory.mkdir())
			throw new IOException("Could not create temporal extraction folder. " + temporalExtractionDirectory.getCanonicalPath());
		return temporalExtractionDirectory.getCanonicalPath();
	}
	
	private IDataHolder storeFiles(File extractionDirectory, String type, String name,String description, String extractionMethod) throws ClientProtocolException, IOException, ClassNotFoundException {
		MultipartEntityBuilder entityBuilder = MultipartEntityBuilder.create();
		try{
			String user;
			user = this.shell.getUser();
			entityBuilder.addTextBody("sys_user", user);
			entityBuilder.addTextBody("name",name);
			entityBuilder.addTextBody("description",description);
			entityBuilder.addTextBody("type", type);
			entityBuilder.addTextBody("path", shell.getPath());
			entityBuilder.addTextBody("method", extractionMethod);
			for (File file:extractionDirectory.listFiles()) 
				entityBuilder.addPart(file.getName(), new FileBody(file));
			IDataHolder data = executeHttpMethod("userCommands/loadcalc",entityBuilder.build());
			return data;
		} catch (Exception e) {
			throw new IOException(e.getMessage());		
		}finally{
			FileUtils.deleteQuietly(extractionDirectory);				
		}		
	}

	private ConcurrentHashMap<String,String> listDir(String path)
	{
		ConcurrentHashMap<String,String> dir = new ConcurrentHashMap<String,String>();
		
		File directory = new File(path);
		int i = 0;
		for (File file:directory.listFiles())
		{
			// if it's not a directory then we add to the list.
			if (file.isFile()) dir.put(new Integer(i).toString(), file.getName());
			i++;
		}
		return dir;
	}
	
	private HashMap<String,String> viceVersa(ConcurrentHashMap<String,String> vice)
	{
		HashMap<String,String> versa = new HashMap<String,String>();
		for (String k:vice.keySet())  versa.put(vice.get(k), k);
		return versa;
		
	}
	
	@Override
	public String getDefOption()
	{
		return "-n";
	}
}
