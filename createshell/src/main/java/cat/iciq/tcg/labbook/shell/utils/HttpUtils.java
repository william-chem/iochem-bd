package cat.iciq.tcg.labbook.shell.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.shell.tests.utils.MyX509TrustManager;

public class HttpUtils {
	
	private static final Logger log = LogManager.getLogger(HttpUtils.class.getName());

    public static CloseableHttpClient buildAllowAllHttpClient(String keystorePath, String keystorePassword) throws GeneralSecurityException, IOException {    	
		trustLocalKeystoreCertificates(keystorePath, keystorePassword);
    	
    	KeyStore ks = KeyStore.getInstance("JKS");
        ks.load(new FileInputStream(keystorePath), keystorePassword.toCharArray());    	
    	    	
    	SSLConnectionSocketFactory trustSelfSignedSocketFactory = new SSLConnectionSocketFactory(
    						new SSLContextBuilder()
    							.loadTrustMaterial(ks, new TrustSelfSignedStrategy())    							
    							.build(),
    						new TrustAllHostNameVerifier()
    					);
    	Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder
    			.<ConnectionSocketFactory>create()
    			.register("http", new PlainConnectionSocketFactory())
    			.register("https", trustSelfSignedSocketFactory)
    			.build();    	
    	
    	PoolingHttpClientConnectionManager cm = (socketFactoryRegistry != null) ? 
                   new PoolingHttpClientConnectionManager(socketFactoryRegistry):
                   new PoolingHttpClientConnectionManager();
                   
        cm.setMaxTotal(1000);
        cm.setDefaultMaxPerRoute(1000);
        
        return HttpClients.custom()
                .useSystemProperties()
                .setConnectionManager(cm)
                .setMaxConnPerRoute(1000)
                .setMaxConnTotal(1000)
                .build();
    }

	private static void trustLocalKeystoreCertificates(String keystorePath, String keystorePassword) {	
		SSLContext sc;
		try {					
			TrustManager[] trustLocalKeystoreCerts = new TrustManager[] { new MyX509TrustManager(new FileInputStream(keystorePath), keystorePassword)};
			sc = SSLContext.getInstance("SSL");		
			sc.init(null, trustLocalKeystoreCerts, new java.security.SecureRandom());
			HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());		
		} catch (Exception e) {			
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	private static class TrustAllHostNameVerifier implements HostnameVerifier {
	    public boolean verify(String hostname, SSLSession session) {
	        return true;
	    }
	}
}
