/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.definitions;

/**
 * Here goes the messages that command can show in a centralized way.
 * @author jiglesias
 *
 */
public class CommandsMessages {
	
	public static final String TITLE_MESSAGE="Problems ocurred during the execution of the command:";
	public static final String DESCRIPTION  ="Description:";
	
	
	// PARAMETERS CHECKING CONSTANTS:
	
	public static final String 	THE_PARAM_IS = "The parameter is ";
	public static final String	FINAL_LINE   = ".";
	
	// special parameters of loadcalc messages
	
	public static class LOAD_CALC_SP_PARAM
	{
		public static final String TITLE() 
		{ return "Error in special parameters of loadcalc"; 											}
		
	}
	public static class LC_ERROR_IO_PARAM extends LOAD_CALC_SP_PARAM 
	{
		public static final String MESSAGE() 
										{ return "If the -i parameter is specified, it's mandatory to specify the -o parameter.";	}
		
	}
	public static class LC_ERROR_OI_PARAM extends LOAD_CALC_SP_PARAM 
	{
		public static final String MESSAGE() 
										{ return "If the -o parameter is specified, it's mandatory to specify the -i parameter.";	}
		
	}
	public static class LC_ERROR_IGOG_PARAM extends LOAD_CALC_SP_PARAM 
	{
		public static final String MESSAGE() 
										{ return "If the -ig parameter is specified, it's mandatory to specify the -og parameter.";	}
		
	}
	public static class LC_ERROR_OGIG_PARAM extends LOAD_CALC_SP_PARAM 
	{
		public static final String MESSAGE() 
										{ return "If the -og parameter is specified, it's mandatory to specify the -ig parameter.";	}
		
	}
	public static class LC_ERROR_OAIA_PARAM extends LOAD_CALC_SP_PARAM 
	{
		public static final String MESSAGE() 
										{ return "If the -oa parameter is specified, it's mandatory to specify the -ia parameter.";	}
		
	}
	public static class LC_ERROR_IAOA_PARAM extends LOAD_CALC_SP_PARAM 
	{
		public static final String MESSAGE() 
										{ return "If the -ia parameter is specified, it's mandatory to specify the -oa parameter.";	}
		
	}
	
	// end special parameters of loadcalc messages
	public static class LIST_VALUE
	{
		public static final String MESSAGE(String value)
		{ return THE_PARAM_IS + value + FINAL_LINE;																		}
		
	}
	public static class PARAM_DO_NOT_EXIST extends LIST_VALUE 
	{
		public static final String TITLE() 
										{ return "Parameter specified doesn't exist"; 									}
		
	}
	public static class PARAM_WITH_NO_DATA extends LIST_VALUE
	{
		public static final String TITLE() 
										{ return "Parameter with no data.";			 									}
		
	}

	public static class MISSED_MAND_PARAM extends LIST_VALUE 
	{
		public static final String TITLE() 
										{ return "Missed mandatory parameter for this command.";						}
		
	}

	public static class MULT_VAL_NOT_ALLOWED extends LIST_VALUE 
	{
		public static final String TITLE() 
										{ return "Field filled with multiple values, but not allowed to have.";			}
		
	}
	public static class FIELD_MUST_BE_INTEGER extends LIST_VALUE 
	{
		public static final String TITLE() 
										{ return "Field that should contain an integer doesn't contain it";				}
		
	}

	public static class FIELD_MUST_BE_FLOAT extends LIST_VALUE 
	{
		public static final String TITLE() 
										{ return "Field that should contain a float doesn't contain it";				}
		
	}
	
	// GENERAL AND COMMON ERROR MESSAGES:
	public static class PROJECT_ERROR 
	{
		public static final String TITLE() 
										{ return "Project Operation Error"; 											}
		
	}
	public static class PROJECT_ERROR_WRITE_PERM extends PROJECT_ERROR 
	{
		public static final String MESSAGE() 
										{ return "User doesn't have write access to the resource. Operation aborted";	}
		
	}
	
	public static class PROJECT_ERROR_READ_PERM extends PROJECT_ERROR 
	{
		public static final String MESSAGE() 
										{ return "User doesn't have read access to the resource. Operation aborted";	}
		
	}
	
	// PROJECT CREATION MESSAGES:
	
	public static class PROJECT_CREATION_ERROR 
	{
		public static final String TITLE() 
										{ return "Error creating the project"; 											}
		
	}
	public static class PROJECT_CRE_ALREADY_EXIST extends PROJECT_CREATION_ERROR 
	{
		public static final String MESSAGE() 
										{ return "Project already exist. Cannot be created."; 							}
		
	}
	
	// PROJECT MODIFICATION MESSAGES.
	
	public static class PROJECT_MODIFICATION_ERROR 
	{
		public static final String TITLE() 
										{ return "Error modifying the project"; 											}
		
	}
	
	// relational data base messages -- begin
	public static class PRO_MOD_RES_ACCESS extends PROJECT_MODIFICATION_ERROR 
	{
		public static final String MESSAGE() 
										{ return "It's not possible to access the specified resource.";}
		
	}
	public static class PRO_MOD_OWNER_DONT_EXIST extends PROJECT_MODIFICATION_ERROR 
	{
		public static final String MESSAGE() 
										{ return "The specified owner it's not valid. Specify another one.";}
		
	}
	public static class PRO_MOD_PERM_NOT_VALID extends PROJECT_MODIFICATION_ERROR 
	{
		public static final String MESSAGE() 
										{ return "The specified permission field doesn't have a correct format.";}
		
	}
	public static class PRO_MOD_PROJ_ALREADY_EXIST extends PROJECT_MODIFICATION_ERROR 
	{
		public static final String MESSAGE() 
										{ return "The project specified as New Name already exist. Please, choose another one.";}
		
	}
	// relational data base messages -- end

	public static class PRO_MOD_GROUP_DONT_EXIST extends PROJECT_MODIFICATION_ERROR 
	{
		public static final String MESSAGE() 
										{ return "The specified groups it's not valid.";}
		
	}
	public static class PROJECT_LS_ERROR 
	{
		public static final String TITLE() 
										{ return "Error listing project"; 											}
		
	}

	public static class PRO_LS_INVALID_O_OPTION extends PROJECT_LS_ERROR 
	{
		public static final String MESSAGE() 
										{ return "The option specified at -o it's not valid. Type mpro -h for help.";}
		
	}
	public static class PRO_LS_MULTI_O_OPTION extends PROJECT_LS_ERROR 
	{
		public static final String MESSAGE() 
										{ return "Only one option is allowed to be specified using -o option. Type mpro -h for help.";}
		
	}
	
	// ERRORS FOR EXTRACTION PROCESS.
	public static class EXTRACTION_ERROR
	{
		public static final String TITLE() 
										{ return "Error in the Extraction Process."; 											}
		
	}
	public static class EXT_TMPDIR extends EXTRACTION_ERROR 
	{	// TODO: this class maybe it's temporal.
		public static final String MESSAGE() 
										{ return "Temporal directory of another extraction already exist. Delete it.";	}
		
	}
	public static class EXT_TMPDIR_DELETE_ERROR extends EXTRACTION_ERROR 
	{	// TODO: this class maybe it's temporal.
		public static final String MESSAGE() 
										{ return "Exception raised while deleting temporal directory. Contact software administrators.";	}
		
	}
	public static class EXT_FILE_IO_EXCEPTION extends EXTRACTION_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "An error raised during file I/O operation.";	}
		
	}
	public static class EXT_FILE_CONVERSION_ERROR extends EXTRACTION_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "An error raised file conversion, please check extraction templates and contact software administrators.";	}
		
	}
	public static class EXT_FILE_NOT_SAME_TYPE extends EXTRACTION_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "One of the detected files is not of the same calculation type, than the rest of them.";	}
		
	}
	public static class EXT_EXPEC_MORE_FILES extends EXTRACTION_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "More files are expected for this type of calculation. \nPlease check it.";	}
		
	}
	public static class EXT_CLOSE_ERROR extends EXTRACTION_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "Area definition error. An initial Match was found, but not and final Match. Contact software administrators.";	}
		
	}
	public static class EXT_FORMAT_NOT_DETECTED extends EXTRACTION_ERROR 
	{
		public static final String MESSAGE()
										{ return "File format can't be detected, please check uploaded files are valid.";	}
		
	}
	public static class EXT_METS_FILE_GEN extends EXTRACTION_ERROR
	{
		public static final String MESSAGE()
										{ return "Compliant METS file couldn't be generated. Check data extraction directory rights.";	}
	}
	

	// calculation messages.
	public static class CALC_ERROR
	{
		public static final String TITLE() 
										{ return "Error on the calculation operation."; 									}
		
	}
	public static class CALC_ALREADY_EXIST_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "Calculation already exist in the database. Calculation not inserted.";	}
		
	}
	
	public static class CALC_UPLOAD_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "There was a problem on the files upload. Calculation not inserted.";	}
		
	}
	public static class CALC_IO_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "There was an Input/Output error. Calculation not inserted.";	}
		
	}
	public static class CALC_PATH_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "Calculation path error. Operation aborted.";	}
		
	}
	public static class CALC_LONG_PATH_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "Project path exceeds maximum length (256)";	}
		
	}
	public static class CALC_READ_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "User not allowed to read this project.";	}
		
	}
	public static class CALC_NON_EXIST_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "You are trying to read a calculation that doesn't exist.";	}
		
	}
	public static class CALC_FILE_TRANSMISSION_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "Error on file transmission.";	}
		
	}
	
	public static class CALC_LOADING_GENERAL_ERROR extends CALC_ERROR 
	{	
		public static final String MESSAGE() 
										{ return "General error encountered at loading the calculation.";	}
		
	}
	
	//JCampDX conversion service errors
	
	public static class JCAMPDX_ERROR 
	{
		public static final String TITLE() 
										{ return "JCamp-DX conversion error"; 											}
		
	}
	
	public static class JCAMPDX_ERROR_NO_SUCH_CALCULATION extends JCAMPDX_ERROR 
	{
		public static final String MESSAGE() 
										{ return "Calculation id provided doesn't exists.";	}
		
	}

	public static void main(String args[])
	{
		System.out.println(CommandsMessages.PARAM_DO_NOT_EXIST.MESSAGE("hello"));
	}

}
