package cat.iciq.tcg.labbook.shell.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.bouncycastle.openssl.PasswordException;

import cat.iciq.tcg.labbook.shell.tests.utils.ByteArrayResponseHandler;

public class RetrieveTGT {
	private static final Logger log = LogManager.getLogger(RetrieveTGT.class.getName());
	
	// Client parameters:
	private static final String	SECURE_SERVER_SCHEME = "https";

	// CAS Server URI variables
    private String casServerHostname = null;
    private String casServerApp = null;
    private String casServerPort = null;
    private String casServerPath	= null;
	private String casServerTicketPath = null;
	private String casUsername = null;
	
	// CAS CREATE registered service urls
	private static String casRegisteredService = null;	
		
	// CAS ticket management variables
	private String ticketGrantingTicketPath = null;
	private String ticketGrantingTicket = null;
	private boolean hasTGTicket = false;
	private boolean isTGTValid = false;

	private ResponseHandler<byte[]> byteResponseHandler;
	private CloseableHttpClient httpClient;
	private HttpHost casHttpHost = null;
	
	public RetrieveTGT(String propertiesPath, String tgtPath) throws FileNotFoundException, IOException{
		ticketGrantingTicketPath = tgtPath;
		loadProperties(propertiesPath);		
	}
	
	private void loadProperties(String propertiesPath) throws FileNotFoundException, IOException {		
		Properties prop = new Properties();
		prop.load(new FileInputStream(propertiesPath));
		casServerHostname = prop.getProperty("cas.server.hostname");
		casServerPort = prop.getProperty("cas.server.port");
		casServerApp = prop.getProperty("cas.server.app");		
	    casServerPath = "/" + casServerApp;
		casServerTicketPath = casServerPath + "/v1/tickets";
		casUsername = prop.getProperty("cas.username").trim();
		
		String createServerHostname = prop.getProperty("create.server.hostname");		
		String createServerPort = prop.getProperty("create.server.port");
		createServerPort = createServerPort.equals("")? "443": createServerPort;   
		String createServerPortDelimiter = createServerPort.equals("")? "" : ":";  
		String createServerApp = prop.getProperty("create.server.app");				
		String createServerPath = "/" + createServerApp;
		casRegisteredService = SECURE_SERVER_SCHEME + "://" + createServerHostname + createServerPortDelimiter + createServerPort + createServerPath+ "/shiro-cas";			
	}

	public void obtain(String keystorePath, String keystorePassword) throws Exception {
		initConnectionVariables(keystorePath, keystorePassword);		
		readTicketGrantingTicket();
		generateTGTTicket();
	}

	private void initConnectionVariables(String keystorePath, String keystorePassword) throws GeneralSecurityException, IOException{
        httpClient = HttpUtils.buildAllowAllHttpClient(keystorePath, keystorePassword);
        casHttpHost = new HttpHost(casServerHostname, casServerPort.equals("")? -1: Integer.valueOf(casServerPort), SECURE_SERVER_SCHEME);
        byteResponseHandler = new ByteArrayResponseHandler();                        
	}

	private void readTicketGrantingTicket(){		
		ticketGrantingTicket = "";
		
		isTGTValid = false;				// Must login first to validate ticket
		if(!new File(ticketGrantingTicketPath).exists()) {
			hasTGTicket = false;			
		} else {
			try {
				ticketGrantingTicket = FileUtils.readFileToString(new File(ticketGrantingTicketPath));
				hasTGTicket = true;
			} catch (IOException e) {
				hasTGTicket = false;			
			}			
		}
	}

	private void generateTGTTicket() throws Exception{	
		while(!isTGTValid) {
			if(hasTGTicket) {
				try {
					generateServiceTicket();
					saveTGT(ticketGrantingTicket);
					isTGTValid = true;
					hasTGTicket = true;					
				}catch(Exception e) {				// Could not retrieve Service Ticket (ST), request Ticket Granting Ticket (TGT) again					
					ticketGrantingTicket = getTicketGrantingTicket(casHttpHost, casServerTicketPath, casUsername, readPassword());
					if(ticketGrantingTicket != null) {				
						hasTGTicket = true; 
					}
				}
			} else {
				ticketGrantingTicket = getTicketGrantingTicket(casHttpHost, casServerTicketPath, casUsername, readPassword());
				if(ticketGrantingTicket != null) {				
					hasTGTicket = true; 
				}
			}
		}	
	}

	private String getTicketGrantingTicket(HttpHost casHost, String ticketPath, String username, String password) throws Exception {		
		HttpPost post = new HttpPost(ticketPath);
		List <NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("username", username));
		parameters.add(new BasicNameValuePair("password", password));		
		post.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));		
		if(password == null || password.isEmpty())
			throw new PasswordException("Password reading cancelled.");
		
		try {			
			
			byte[] response = httpClient.execute(casHost, post,byteResponseHandler);
			String responseStr = new String(response);
		   
		    final Matcher matcher = Pattern.compile(".*action=\".*/(.*?)\".*").matcher(responseStr);

			if (matcher.matches())
				return matcher.group(1);

			log.warn("Successful ticket granting request, but no ticket found!");
			log.info("Response (1k): " + response.toString().substring(0, Math.min(1024, responseStr.length())));
		    
		} catch (final Exception e) {
			log.warn(e.getMessage());
			return null;
		}
		finally {
			post.releaseConnection();
		}

		return null;
	}
	
	private void saveTGT(String ticketGrantingTicket) {
		FileOutputStream fos = null;
		try {
			Files.deleteIfExists(Paths.get(ticketGrantingTicketPath));
			fos = new FileOutputStream(ticketGrantingTicketPath);
			fos.write(ticketGrantingTicket.getBytes());
			fos.flush();
			fos.close();
		} catch (IOException e) {
			try {
				fos.close();
			} catch (IOException e1) {
				log.error(e1.getMessage());
			}
		}
	}
	
	private String generateServiceTicket() throws KeyManagementException, UnsupportedEncodingException {
		String serviceTicket = getServiceTicket(casHttpHost, casServerTicketPath,ticketGrantingTicket, casRegisteredService);		
		if(serviceTicket == null)
			throw new KeyManagementException("Unable to get Service Ticket (ST) from CAS login server.");
		return serviceTicket; 
	}
	
	private String getServiceTicket(HttpHost casHost, String ticketPath, String ticketGrantingTicket, String service) throws UnsupportedEncodingException {		
		if (ticketGrantingTicket == null)
			return null;
		HttpPost post = new HttpPost(ticketPath + "/" + ticketGrantingTicket);
		
		List <NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("service", service));
		post.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));		

		try {
			byte[] response = httpClient.execute(casHost, post,byteResponseHandler);
			return new String(response);
					    
		} catch (final IOException e) {
			log.warn(e.getMessage());			
		}
		finally {
			post.releaseConnection();
		}

		return null;
	}
	
	private String readPassword() throws IOException {
		System.out.println("Could not retrieve latest session cookie, please login again.");
		System.out.println("Username: " + casUsername );		
		ReadMyPassword r = new ReadMyPassword();
		return new String(r.readPassword("Password (empty to quit): "));
	}
	
}

class ReadMyPassword{	
    public char[] readPassword(String format, Object... args) throws IOException {
        if (System.console() != null) {
    		return System.console().readPassword(format, args);        		        
        }
        return this.readLine(format, args).toCharArray();
    }
    private String readLine(String format, Object... args) throws IOException {
        if (System.console() != null) {
            return System.console().readLine(format, args);
        }
        System.out.print(String.format(format, args));
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        return reader.readLine();
    }
}


