/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedHashMap;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.commands.render.RowRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;
import cat.iciq.tcg.labbook.shell.utils.Paths;

public class ViewCalc extends Command implements ICommand{

	private RowRender rr;
	
	public ViewCalc(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler,CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] mandatoryParameters 	= {"-n"};
		String[] optionalParameters  	= {"-f","-dcp"};
		String[] singleValueParameters 	= mandatoryParameters;
		String[] integerFields			= {};
		String[] stringFields			= {"-n","-f","-dcp"};
		String[] floatFields			= {};
		init(	mandatoryParameters, optionalParameters, singleValueParameters,
				integerFields, stringFields, floatFields, "-n", targetHost,
				byteResponseHandler, httpClient, shell);

		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", " Relative or absolute project path           (mandatory)");
		help.put("-f [FILENAME]", " Download single calculation file, all if not specified. Requires -dcp parameter. 	(optional)");
		help.put("-dcp PATH",     " Folder where to store the files. Requires -f parameter.					(optional)");
		this.help = new HelpRender("viewcalc","viewcalc description",help);
	}
	

	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		rr = new RowRender();
		rr.addTitle("Name");
		rr.addTitle("Description");
		rr.addTitle("Permissions");
		rr.addTitle("Owner");
		rr.addTitle("Group");
		rr.addTitle("Type");
		rr.addTitle("Path");
		rr.addTitle("Concept Group");
		rr.addTitle("Creation Time");
		rr.addTitle("Mets file");
		rr.addTitle("Published");
		rr.addTitle("Handle");
		rr.addTitle("Published name");
		rr.addTitle("Publication time");
		rr.addTitle("");
		
		if(parameters.containsKey("-f")) {
			if(!parameters.containsKey("-dcp"))
				return new ErrorRender(CommandsMessages.MISSED_MAND_PARAM.TITLE(),CommandsMessages.MISSED_MAND_PARAM.MESSAGE("-dcp"));
			else if(parameters.get("-f").isEmpty())
				parameters.put("-f", "dummy");		
		}
		
		IRender ren = super.execute(parameters);
		if (ren != null) 
			return ren;
		
        String newPath = parameters.get("-n");        
        if (newPath.equals(GeneralConstants.PARENT_PROJECT))        
        	newPath = Paths.getParent(shell.getPath());
        
        renewHttpParm();
        httpParam.add(new BasicNameValuePair("newPath", newPath));
        httpParam.add(new BasicNameValuePair("path", shell.getPath()));        
        if (parameters.containsKey("-f")) 
        	httpParam.add(new BasicNameValuePair("full","dummy"));
        
		IDataHolder data = executeHttpMethod("userCommands/viewcalc",	httpParam);

		if (data.isError()) { 
			return new ErrorRender(data.getTitleMessage(),data.getBodyMessage());
		} else {
			//getting the files:
			if(parameters.containsKey("-f")) {
				for (String file:data.getHiddenData()) {
					String filename = Paths.getTail(file);
					if(parameters.get("-f").equals("dummy") || parameters.get("-f").equals(filename)) {
						renewHttpParm();
				        httpParam.add(new BasicNameValuePair("newPath",	newPath));
				        httpParam.add(new BasicNameValuePair("path", shell.getPath()));
						httpParam.add(new BasicNameValuePair("calcFile", file));
						if (executeHttpMethod("userCommands/getareafile",httpParam, parameters.get("-dcp"), Paths.getTail(file)))
							return new ErrorRender(CommandsMessages.CALC_FILE_TRANSMISSION_ERROR.TITLE(),CommandsMessages.CALC_FILE_TRANSMISSION_ERROR.MESSAGE());
					}
				}
			}
			rr.setMainTitle(Paths.getTail(newPath));
			rr.setData(data);
			return rr;
		}
	}
	@Override
	public String getDefOption()
	{
		return "-n";
	}

}
