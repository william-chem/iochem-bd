/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.data;

import java.util.Set;
import java.util.Vector;

public interface IDataHolder {
	
	public void setStringColumn(int column,Vector<String> values);
	public void setFloatColumn(int column,Vector<Float> values);
	public void setIntegerColumn(int column,Vector<Integer> values);
	public int getColumnType(int column);
	public Vector<String> getStringColumn(int column);
	public Vector<Float> getFloatColumn(int column);
	public Vector<Integer> getIntegerColumn(int column);
	public Vector<String> getRowAsString(int row);
	public String getTitleMessage();
	public String getBodyMessage();
	public boolean isError();
	public int getMatrixRows();
	public String getElement(int i);
	public void setElement(String element);
	public void setElementAt(String element,int pos);
	public int getNumElements();
	public void setKeyValue(String key,String value);
	public Set<String> getKeys();
	public String getValue(String key);
	public void setHiddenData (String str);
	public Set<String> getHiddenData ();

}
