/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.commands.render.MessageRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.extraction.CalExtractionJumbo;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class Query extends Command implements ICommand{

	private static String[] notJumboParameters 	= {"-n", "-d", "-dbp", "-dcp", "-q"};
	private static DocumentBuilder docBuilder;
	private static DocumentBuilderFactory bFactory = DocumentBuilderFactory.newInstance();
	private static XPathFactory xFactory = new net.sf.saxon.xpath.XPathFactoryImpl();
	
	static{
		bFactory.setNamespaceAware(true);
//		bFactory.setXIncludeAware(true);
		try {
			docBuilder = bFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			
		}	
	}
	
	
	public Query(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler,CloseableHttpClient httpClient,ShellClient shell){
		
		String[] wholeParameters        = {"-q", "-i", "-o", "-nc", "-dc", "-oe", "-oc", "-ob", "-dcp", "-kp"};
		String[] mandatoryParameters 	= {"-q", "-o"};
		String[] optionalParameters  	= {"-i", "-nc", "-dc", "-oe", "-oc", "-ob", "-dcp", "-kp"};
		String[] singleValueParameters 	= wholeParameters;
		String[] integerFields			= {};
		String[] stringFields			= wholeParameters;
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-q", targetHost,byteResponseHandler,httpClient,shell);
		
		//TODO: for the moment this is done using this local programation. In the future the program will access the database to generate this documentation help.
		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-q", 		"XPath query command");
		help.put("-i", 		"Input file");
		help.put("-o", 		"Output file (vasprun.xml on VASP, job.last on Turbomole).\t\t\t(mandatory)");				
		help.put("-nc",     "Vasp NEB POSCAR/CONTCAR files\t\t\t\t\t(optional)");
		help.put("-dc", 	"Vasp DOSCAR file.\t\t\t\t\t(optional)");
		help.put("-kp", 	"Vasp KPOINTS file.\t\t\t\t\t(optional)");
		help.put("-oe", 	"Turbomole energy file.\t\t\t\t\t(optional)");
		help.put("-oc", 	"Turbomole coords file.\t\t\t\t\t(optional)");
		help.put("-ob", 	"Turbomole basis file.\t\t\t\t\t(optional)");		
		help.put("-dcp", 	"Absolute path in the desktop computer.\t\t\t\t(optional)");
		
		this.help = new HelpRender("query","This command converts calculation into CML and then performs an XPATH query on it. ",help);	
	}

	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
		
		if (parameters.containsKey("-i") && !(parameters.containsKey("-o")))
				return new ErrorRender(CommandsMessages.LC_ERROR_IO_PARAM.TITLE(),CommandsMessages.LC_ERROR_IO_PARAM.MESSAGE());
        
		renewHttpParm();
		
		IDataHolder calTypes = executeHttpMethod("systemUserCommands/getcaltypes",	httpParam);
		if (calTypes.isError()) 
			return new ErrorRender(calTypes.getTitleMessage(),calTypes.getBodyMessage());
		
		String sourceDirectory = null;
		
		if (parameters.containsKey("-dcp")) sourceDirectory = parameters.get("-dcp");
		else								sourceDirectory = System.getenv().get("PWD"); // pwd system provided variable.

		ConcurrentHashMap<String,String> files 		= null;
		HashMap<String,String> filesVersa 	= null;
		
		files = singleFileParam(parameters);
		
		if (files == null) // some files specified as a parameter and the contents of the directory also contains information to process (files)
		{
			files = listDir(sourceDirectory);
			filesVersa = viceVersa(files);
			
			// adding parameter files:
			String value;
			for (String option:parameters.keySet())
			{
				value = parameters.get(option);
				if (files.containsValue(value)) // of the file is part of the parameters.
				{
					files.remove(filesVersa.get(value));
					files.put(option, value);
				}
			}
		}
		
		String temporalDirectory = createTemporalFolder();		
		CalExtractionJumbo calExt = new CalExtractionJumbo((MatrixDH) calTypes, files, sourceDirectory, temporalDirectory);
		try{			
			HashMap<String, String> params = calExt.call();			
		}
		catch (Exception e){
			deleteFiles(new File(temporalDirectory));
			return new ErrorRender("Error during file conversion",e.getMessage());
		}
		
		String outputPath = calExt.getExtractionDirectory().getAbsolutePath() + File.separatorChar +  "output.cml";				 
		String queryResult = query(outputPath + "", parameters.get("-q"));
		MessageRender result = new MessageRender(queryResult);
		deleteFiles(new File(temporalDirectory));		
		return result; 
		
	}
	
	private String query(String outputFilePath, String xpathQuery) {
		Document document = null;				
		try {
			InputStream is = new FileInputStream(new File(outputFilePath));
			document =  docBuilder.parse(is);	
			XPath xPath = xFactory.newXPath();
			return (String)xPath.evaluate(xpathQuery, document, XPathConstants.STRING);			
		} catch (Exception e1) {
			return "";
		}				
	}

	protected ConcurrentHashMap<String,String> singleFileParam(Hashtable<String,String>param)
	{		
		ConcurrentHashMap<String,String> toReturn = new ConcurrentHashMap<String,String>();
		Iterator<String> iter = param.keySet().iterator();
		while(iter.hasNext()){
			String key = (String)iter.next();
			if(!Arrays.asList(notJumboParameters).contains(key))
				toReturn.put(key, param.get(key));		
		}
		return toReturn;
	}
	
	private String createTemporalFolder() throws IOException{
		String uuid = UUID.randomUUID().toString();
		File systemTemporalDirectory = new File(System.getProperty("java.io.tmpdir"));
		File temporalExtractionDirectory = new File(systemTemporalDirectory.getCanonicalPath() + File.separatorChar + "jumbo_saxon_" + uuid);
		if(!temporalExtractionDirectory.mkdir())
			throw new IOException("Could not create temporal extraction folder. " + temporalExtractionDirectory.getCanonicalPath());
		return temporalExtractionDirectory.getCanonicalPath();
	}
	
	private void deleteFiles(File tmpDir) {		
		for (File file:tmpDir.listFiles()) 
			file.delete();
		tmpDir.delete();
	}
	
	private ConcurrentHashMap<String,String> listDir(String path)
	{
		ConcurrentHashMap<String,String> dir = new ConcurrentHashMap<String,String>();
		
		File directory = new File(path);
		int i = 0;
		for (File file:directory.listFiles())
		{
			// if it's not a directory then we add to the list.
			if (file.isFile()) dir.put(new Integer(i).toString(), file.getName());
			i++;
		}
		return dir;
	}
	private HashMap<String,String> viceVersa(ConcurrentHashMap<String,String> vice)
	{
		HashMap<String,String> versa = new HashMap<String,String>();
		for (String k:vice.keySet())  versa.put(vice.get(k), k);
		return versa;
		
	}
	
}
