/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Properties;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.shell.main.ShellClient;
import cat.iciq.tcg.labbook.shell.utils.ShellTools;


public class Main {
	private static Logger log  = LogManager.getLogger(Main.class.getName());
	
	public static final String KEYSTORE_FILE = "keystore"; 
	public static final String KEYSTORE_PASSWORD = "changeit";
	public static final String PROPERTY_FILE = "resources.properties"; 	
	public static final String TGT_FILE = "ticketgranting.ticket";

	public static FileInputStream fis = null;
	public static InputStreamReader isr	= null;
	public static FileOutputStream fos = null;
	public static BufferedReader stdin = null;
	public static PrintStream out = null;
	
	public static boolean bashMode = false;  // false = debugging on IDE, true = running on production
	public static Path basePath = null;
	
	private static ShellClient shell = null;
	private static Properties properties = null;
	
	public static void main(String[] args){
		if(Arrays.asList(args).contains("-admin")){
			ShellTools.main(args);
		}else{
			startShellClient(args);	
		}
	}
		
	private static void startShellClient(String[] args){
		try{
			setRunMode(args);
			readPropertiesFile();
			setupRKFTool();
			initShellClient();		
			loadInputOutputStreams(args);		  
		    readCommandsLoop(args);
		}catch(Exception e){
			log.error(e.getMessage());			
			System.exit(1);
		}
	}
	
    private static void setRunMode(String[] args){
		 if((args.length != 0 ) &&(args[0].equals("bash"))) {
			 bashMode = true;		
			 basePath = Paths.get(args[4]);			 
		 } else {
			 bashMode = false;	 
			 basePath = Paths.get(System.getProperty("user.home"), "shell");
		 }
	}
	
	private static void readPropertiesFile() throws IOException{				 						
		properties = new Properties();
		properties.load(new FileInputStream(getLocalPath("resources", PROPERTY_FILE)));
	}

    private static void setupRKFTool() {
        String rkf2cmlPath = System.getenv("RKF2CML_FILE_PATH");
        if(rkf2cmlPath != null && new File(rkf2cmlPath).exists())
            System.setProperty("RKF2CML_FILE_PATH", rkf2cmlPath);
    }

	
	private static void initShellClient() throws Exception{
		String keystorePath = getLocalPath("resources", KEYSTORE_FILE);
		String tgtPath = getLocalPath("resources", TGT_FILE);		
		shell = new ShellClient(properties, tgtPath, keystorePath, KEYSTORE_PASSWORD);	
	}
	
	private static String getLocalPath(String ... paths) {		
		return Paths.get(basePath.toString(), paths).toString(); 	
	}
	
	private static void loadInputOutputStreams(String[] args) throws FileNotFoundException{
		if(bashMode){
			fis 	= new FileInputStream(args[1]);
	    	isr 	= new InputStreamReader(fis);
	    	stdin 	= new BufferedReader(isr);
	    	fos   	= new FileOutputStream(args[2]);
	    	out   	= new PrintStream(fos);
		}else{
			stdin 		= new BufferedReader(new InputStreamReader(System.in));				
		    out 		= System.out;
		}	
		shell.setOut(out);	
		
	    if(!LogManager.getRootLogger().getLevel().equals(Level.DEBUG))
	    	System.err.close();
	}
	
	public static void readBash(String readPipe,String writePipe) throws IOException{		
		stdin.close();
		isr.close();
		fis.close();
		out.close();
		fos.close();
		
    	fis 	= new FileInputStream(readPipe);
    	isr 	= new InputStreamReader(fis);
    	stdin 	= new BufferedReader(isr);
    	fos   	= new FileOutputStream(writePipe);
    	out   	= new PrintStream(fos);		
	}
	
	private static void readCommandsLoop(String[] args){
		String message; // Creates a variable called message for input
	    if (!(bashMode)) shell.prompt();
	    
	    try{	    	
	    	message = stdin.readLine();
		    while (!(message.trim().equals("exit"))){
		    	message = message.trim();		    	
		    	shell.execCommand(message);
		    	out.flush();
		    	if (bashMode){
			    	readBash(args[1],args[2]);
			    	shell.setOut(out);
		    	}
		    	else shell.prompt();
			    message = stdin.readLine();
		    }
	    }catch (IOException e) {
			log.error("IO Error reading the shell command.");
			log.error(e.getMessage());
			e.printStackTrace();
	    }catch (OutOfMemoryError e) {
	    	log.error("Out of memory exception");
	    	log.error(e.getMessage());
	    	e.printStackTrace();
		}finally{
	    	try{
	    		if(bashMode){
		    		stdin.close();
		    		isr.close();
		    		fis.close();
		    		out.close();
		    		fos.close();
		    	}
				shell.close();	
	    	}catch(Exception e){
	    		log.error(e.getMessage());
	    	}	    	
	    }
	}

}
