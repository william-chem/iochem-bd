/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.utils;

import java.io.IOException;
import java.nio.file.Paths;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.shell.Main;

public class ShellTools {
	private static final Logger log = LogManager.getLogger(ShellTools.class.getName());
	
	private static enum Operations {check_tgt};	
	private static Options options = null;
	private static CommandLine cmd = null;
	
	public static void main(String[] args) {		
		try {		
			initOptions();
			parseArguments(args);			
			processArguments();
			System.exit(0);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			System.exit(1);
		}		
	}

	private static void initOptions(){
		options = new Options();
		options.addOption("admin", false, "Admin mode");
		options.addOption("dcp", true, "Current path to shell client folder");
		options.addOption("t", true, "Current operation types: check_tgt");		
		options.addOption("h", "help", false, "Prints command help");
	}
	
	private static void parseArguments(String[] args) throws ParseException{
		CommandLineParser parser = new DefaultParser();
		cmd = parser.parse(options, args);
	}
		
	private static void processArguments() throws Exception{
		if(cmd.hasOption("h")){
			printHelp();
		}
		
		if(!cmd.hasOption("t"))
			throw new IOException("Undefined action type, please use -t option");
		
		Operations operation = Operations.valueOf(cmd.getOptionValue("t"));
		switch(operation){
			case check_tgt:	generateTGTTicket();
									break;
		}
	}

	private static void generateTGTTicket() throws Exception {	
		String basePath = cmd.getOptionValue("dcp");
		String propertiesPath = Paths.get(basePath, "resources", Main.PROPERTY_FILE).toString();
		String tgtPath = Paths.get(basePath, "resources", Main.TGT_FILE).toString();
		String keystorePath = Paths.get(basePath, "resources", Main.KEYSTORE_FILE).toString();
		
		RetrieveTGT retrieveTicket = new RetrieveTGT(propertiesPath, tgtPath);
		retrieveTicket.obtain(keystorePath, Main.KEYSTORE_PASSWORD);		
	}
	
	private static void printHelp(){	
		HelpFormatter formatter = new HelpFormatter();
		formatter.printHelp( "ShellTools", options );		
        }
}
