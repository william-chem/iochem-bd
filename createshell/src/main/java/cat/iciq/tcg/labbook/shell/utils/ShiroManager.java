/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.utils;

import java.util.HashMap;
import java.util.Iterator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.SimplePrincipalCollection;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;

/**
 * Helper class used to handle current subject principals from Apache Shiro Framework
 * This class retrieves logged user information from Apache Shiro user principal variables,
 * they also are retrieved from CAS server using SAML protocol.   
 * @author malvarez
 *
 */
public class ShiroManager {
	
	private static final Logger log = LogManager.getLogger(ShiroManager.class.getName());
	
	//User related principals
	private static final String CAS_USER_EMAIL		 		    = "user_email";
	private static final String CAS_USER_ID		 				= "user_id";
	private static final String CAS_USER_FULL_NAME				= "user_fullname";
	private static final String CAS_USER_PATH					= "user_path";
	private static final String CAS_USER_MAIN_GROUP_ID			= "main_group_id";
	private static final String CAS_USER_ASSOCIATED_GROUPS_ID 	= "group_id";
	//System related principals
	private static final String CAS_USERS 				= "users_path";
	private static final String CAS_USERS_ID			= "users_id";	
	private static final String CAS_GROUPS 				= "groups_name";
	private static final String CAS_GROUPS_ID			= "groups_id";
	private static final String CAS_FIELD_DELIMITER 	= "/";
	
	private String userEmail		= "";
	private int userId 				= -1;
	private String userName 		= "";
	private String userFullName		= "";
	private int mainGroupId 		= -1;
	private String[] userGroups	 	= null;
	private String userGroupsSQL 	= "";
		
	private String[] users			= null;
	private String[] usersId		= null;
	private String[] groups			= null;
	private String[] groupsId		= null;
	
	public HashMap<String,Integer> userIdByName = null;
	public HashMap<Integer,String> userNameById = null;
	
	public HashMap<String,Integer> groupIdByName = null;
	public HashMap<Integer,String> groupNameById = null;
	
	public static boolean isValidSubject(){
		try{
			if(getCurrent().getUserName().equals("") || getCurrent().getMainGroupId()<=0)				
				return false;
		}catch(BrowseCredentialsException ex){
			return false;
		}
		return true;
	}
	
	public int getGroupIdByName(String groupName){
		return groupIdByName.get(groupName);
	}
	
	public String getGroupNameById(int groupId){
		return groupNameById.get(groupId);
	}
	
	public int getUserIdByName(String userName){
		return userIdByName.get(userName);
	}
	
	public String getUserNameById(int userId){
		return userNameById.get(userId);
	}
	
	public static ShiroManager getCurrent() throws BrowseCredentialsException{
		ShiroManager shiroManager = (ShiroManager) SecurityUtils.getSubject().getSession().getAttribute("shiroManager");
		if(shiroManager == null){
			shiroManager = new ShiroManager(); 			
			SecurityUtils.getSubject().getSession().setAttribute("shiroManager",shiroManager);
		}
		return shiroManager;
	}
	
	public ShiroManager() throws BrowseCredentialsException{
		try{
			//User related environment variables
			userEmail 	 = getPrincipalValue(CAS_USER_EMAIL);
			userId 		 = Integer.valueOf(getPrincipalValue(CAS_USER_ID));
			userName 	 = getPrincipalValue(CAS_USER_PATH);
			userFullName = getPrincipalValue(CAS_USER_FULL_NAME);
			if(userName == null)
				throw new BrowseCredentialsException();
			mainGroupId	= Integer.valueOf(getPrincipalValue(CAS_USER_MAIN_GROUP_ID));
			userGroups	= getPrincipalValue(CAS_USER_ASSOCIATED_GROUPS_ID).split(CAS_FIELD_DELIMITER);
			userGroupsSQL = getPrincipalValue(CAS_USER_ASSOCIATED_GROUPS_ID).replace(CAS_FIELD_DELIMITER, ",");
			//System related environment variables
			users 		= getPrincipalValue(CAS_USERS).split(CAS_FIELD_DELIMITER);
			usersId		= getPrincipalValue(CAS_USERS_ID).split(CAS_FIELD_DELIMITER);		
			groups		= getPrincipalValue(CAS_GROUPS).split(CAS_FIELD_DELIMITER);
			groupsId	= getPrincipalValue(CAS_GROUPS_ID).split(CAS_FIELD_DELIMITER);
			//Build id-user relation hashmaps
			userIdByName = new HashMap<String,Integer>();
			userNameById = new HashMap<Integer,String>();
			String keys[] 	= users;
			String values[] = usersId;
			for(int inx = 0; inx < keys.length; inx++){
				userIdByName.put(keys[inx], Integer.parseInt(values[inx]));
				userNameById.put(Integer.parseInt(values[inx]), keys[inx]);			
			}	
			//Build id-group relation hashmaps
			groupIdByName = new HashMap<String,Integer>();
			groupNameById = new HashMap<Integer,String>();
			keys 	= groups;
			values  = groupsId;
			for(int inx = 0; inx < keys.length; inx++){
				groupIdByName.put(keys[inx], Integer.parseInt(values[inx]));
				groupNameById.put(Integer.parseInt(values[inx]), keys[inx]);			
			}	
		}catch(Exception e){
			log.error(e.getMessage());
			throw new BrowseCredentialsException();
		}
	}
	
	public String getUserEmail(){
		return userEmail;
	}
	
	public int getUserId(){	
		return userId;		
	}
	
	public String getUserName(){		
		return userName;
	}
	
	public String getUserFullName(){
		return userFullName;
	}
	
	public int getMainGroupId(){				
		return mainGroupId;
	}
	
	public String[] getUserGroups(){	
		return userGroups;		
	}
	
	public String getUserGroupsSQL(){
		return userGroupsSQL;		
	}
	
	public String[] getOwners(){		
		return users;		
	}
	
	public String[] getGroups(){
		return groups;		
	}
	
	public String[] getGroupsId(){
		return groupsId;
	}
		
	private String getPrincipalValue(String principalName){
		SimplePrincipalCollection principals =  (SimplePrincipalCollection) SecurityUtils.getSubject().getPrincipals();		
		Iterator iter = principals.iterator();
		iter.next();
		HashMap<String,String> values = (HashMap<String,String>) iter.next();
		return values.get(principalName);
	}
	
	public int getNumberOfUsers(){
		return userNameById.size();
	}
	
	public int getNumberOfGroups(){
		return groupIdByName.size(); 
	}
}
