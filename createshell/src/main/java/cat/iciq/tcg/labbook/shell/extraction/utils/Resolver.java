/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.extraction.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.regex.Pattern;

public class Resolver {
	private static int SHORT_SEARCH_NUM_LINES = 16;
	private static int LONG_SEARCH_NUM_LINES = 10000;
	private static LinkedHashMap<String, Pattern> pattern;
	
	private final static String[] amberRecords = 
	{ "Amber", "^\\s+Amber\\s+.*$" };	    
	
	private final static String[] gronorRecords = 
	{ "GronOR", "^.*(http:\\/\\/www\\.iochem-bd\\.org\\/dictionary\\/gronor\\/|<scalar.*>GronOR<\\/scalar>).*$" };
	
	private final static String[] gaussianRecords =
	{ "Gaussian", "^.*(Entering\\sGaussian\\sSystem|Entering\\sLink\\s1|1998\\sGaussian,\\sInc\\.).*$" };

    private final static String[] amsRecords =
    { "AMS", "^.*Amsterdam\\sModeling\\sSuite.*$" };

	private final static String[] adfRecords =
	{ "ADF", "^.*Amsterdam\\sDensity\\sFunctional.*$" };
	
	private final static String[] turbomoleRecords = 
	{ "Turbomole", "^.*(TURBOMOLE).*$" };
	  
	private final static String[] orcaRecords =
	{ "Orca", "^.*(O\\s+R\\s+C\\s+A).*$" };

	private final static String[] vaspOutcarRecords = 
	{ "Vasp", "^.*(\\svasp\\.|\\sINCAR:).*$" };	
	
	private final static String[] siestaRecords =
	{ "Siesta", "^.*(MD\\.TypeOfRun|SolutionMethod|MeshCutoff|WELCOME\\sTO\\sSIESTA).*$" };

	private final static String[] nwchemRecords =
	{ "NWChem", "^.*(\\s+argument\\s+1\\s=\\s).*$"};

	private final static String[] moldenFileStartRecords =
	{ "Molden", "^.*(\\[Molden).*$"};

	private final static String[] molcasFileStartRecords =
	{ "Molcas", "^.*(MOLCAS|M\\sO\\sL\\sC\\sA\\sS).*$"};

	private final static String[] espressoRecords =
	{ "QuantumEspresso", "^.*(Program\\sPWSCF|Program\\sPHONON).*$" }; 
	
	private final static String[] mopacRecords =
	{ "Mopac", "^.*(MOPAC\\s93\\s\\(c\\)\\sFujitsu|MOPAC\\sFOR\\sLINUX\\s\\(PUBLIC\\sDOMAIN\\sVERSION\\)|MOPAC:\\s+VERSION\\s+6|MOPAC\\s+7|MOPAC2|MOPAC\\s+\\(PUBLIC).*$" };
	
	private final static String[] gromacsRecords =
	{ "GROMACS", "^\\s*GROMACS:.*$" };

	private final static String[] lammpsRecords =
	{ "LAMMPS", "^\\s*LAMMPS.*$" };
	
	private final static String[] castepRecords =
	{ "CASTEP", "^.*(Materials\\s*Studio\\s*CASTEP|\\s*CCC\\s*AA\\s*SSS\\s*TTTTT\\s*EEEEE\\s*PPPP).*$" };
	    
	
	private final static String[][] headerRecords =
	{ 
	    amsRecords, adfRecords, amberRecords, gaussianRecords, turbomoleRecords, mopacRecords, orcaRecords, vaspOutcarRecords, 
		gromacsRecords, molcasFileStartRecords, espressoRecords, siestaRecords, nwchemRecords,  gronorRecords, 
		moldenFileStartRecords, lammpsRecords, castepRecords
	};
	
	static { 
		pattern = new LinkedHashMap<String, Pattern>();
		for(String[] records : headerRecords)
			pattern.put(records[0], Pattern.compile(records[1]));		
	}
	
	
	public static String getFileType(BufferedReader br) {
		try {
			return determineFileType(br);
		} catch (Exception e) {
			return null;
		}
	}

	private static String determineFileType(BufferedReader br) throws IOException{
		String type = null;		
		type = search(br, SHORT_SEARCH_NUM_LINES);
		if(type == null)
			type = search(br, LONG_SEARCH_NUM_LINES);
		return type;
	}
	
	private static String search(BufferedReader br, int maxLines) throws IOException{		
		for(int inx = 0; inx < maxLines; inx++){
			String currentLine = br.readLine();
			if(currentLine == null)
				return null;
			for(String key : pattern.keySet())
				if(pattern.get(key).matcher(currentLine).matches())
					return key;
		}
		return null;		
	}
	
	
	
}
