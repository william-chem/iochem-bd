/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;


import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.data.DefaultDH;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MessageDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;


public class Command implements ICommand{
	
	//private static final CharSequence 		VALUES_SEPARATOR = ",";
	private static final int				BUFFER_SIZE = 10240;
	private static final String				OK = "OK";
	private static final String				UNKNOWN= "UNKNOWN";
	protected Set<String> 					mandatoryParameters; 
	protected Set<String> 					optionalParameters;
	protected Set<String> 					singleValueParameters;
	protected Set<String> 					integerFields;
	protected Set<String> 					stringFields;
	protected Set<String> 					floatFields;
	protected String 						defaultField;
	protected Hashtable<String, String> 	parameters = null;
	protected Set<String>					allParameters;
	protected HelpRender					help;
	protected HttpHost 						targetHost;
	protected ResponseHandler<byte[]> 		byteResponseHandler;
	protected CloseableHttpClient 			httpClient;
	protected ShellClient 					shell;
	protected List <NameValuePair>          httpParam;

	protected void init(String[] 					mandatoryParameters, 
						String[] 					optionalParameters,
						String[] 					singleValueParameters,
						String[] 					integerFields,
						String[] 					stringFields,
						String[] 					floatFields,
						String   					defaultField,
						HttpHost 					targetHost,
						ResponseHandler<byte[]> 	byteResponseHandler,
						CloseableHttpClient 		httpClient,
						ShellClient					shell
						)
	{
		this.mandatoryParameters 	= createSet(mandatoryParameters		); 
		this.optionalParameters		= createSet(optionalParameters		);
		this.singleValueParameters	= createSet(singleValueParameters	);
		this.integerFields			= createSet(integerFields			);
		this.stringFields			= createSet(stringFields			);
		this.floatFields			= createSet(floatFields				);
		this.defaultField			= defaultField;
		this.allParameters			= new HashSet<String>();
		this.targetHost				= targetHost;
		this.byteResponseHandler	= byteResponseHandler;
		this.httpClient				= httpClient;
		this.shell					= shell;
		
		
		for (String op: mandatoryParameters) 	allParameters.add(op);
		for (String op: optionalParameters) 	allParameters.add(op);
		
	}
	
	protected void renewHttpParm()
	{		
		String user = "error";
		try {
			user = shell.getUser();
		} catch (BrowseCredentialsException e) {}
		httpParam = new ArrayList <NameValuePair>();
		httpParam.add(new BasicNameValuePair("sys_user", user));
	}
 
	
	private Set<String> createSet(String[] options)
	{
		HashSet<String> optionsSet = new HashSet<String>();
		for (String op:options) optionsSet.add(op);
		return optionsSet;
	}
	
	/**
	 * check if the parameters hash is correct according to the command specification. 
	 * @return null if everything is ok, or the IRender object to render the error.
	 */
	protected IRender checkParameters()
	{
		// checking if the option received exist or not for this command:
		
		for (String option:parameters.keySet())
		{
			if (!(this.allParameters.contains(option)))  // if the option doesn't exist.
			{
				return new ErrorRender(	CommandsMessages.PARAM_DO_NOT_EXIST.TITLE(),
										CommandsMessages.PARAM_DO_NOT_EXIST.MESSAGE(option));
			}
			
			if (parameters.get(option).equals("")) // blank data
			{
				return new ErrorRender(	CommandsMessages.PARAM_WITH_NO_DATA.TITLE(),
										CommandsMessages.PARAM_WITH_NO_DATA.MESSAGE(option));
				
			}
		}
		
		// checking mandatory fields
		for (String op: mandatoryParameters)
		{
			if (!(parameters.containsKey(op)))
			{
				return new ErrorRender(	CommandsMessages.MISSED_MAND_PARAM.TITLE(),
										CommandsMessages.MISSED_MAND_PARAM.MESSAGE(op));
										
			}
			
		}

		for (String option: parameters.keySet())
		{
			if (integerFields.contains(option))
			{
				return checkIfInteger(option,parameters.get(option));
			}
			else if(floatFields.contains(option))
			{
				return checkIfFloat(option,parameters.get(option));
			}
		}
		
		return null; // no errors found.
		
	}

	private IRender checkIfInteger(String option, String value) {
		
		try
		{
			new Integer(value);
		}
		catch (NumberFormatException e)
		{
			return new ErrorRender(	CommandsMessages.FIELD_MUST_BE_INTEGER.TITLE(),
									CommandsMessages.FIELD_MUST_BE_INTEGER.MESSAGE(option));
		}
		return null;
	}
	private IRender checkIfFloat(String option, String value){
		
		try
		{
			new Float(value);
		}
		catch (NumberFormatException e)
		{
			return new ErrorRender(	CommandsMessages.FIELD_MUST_BE_FLOAT.TITLE(),
									CommandsMessages.FIELD_MUST_BE_FLOAT.MESSAGE(option));
		}
		return null;
	}

	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException,ClassNotFoundException, XMLException {
		this.parameters = parameters;
		return this.checkParameters();
	}

	@Override
	public String getDefOption() {
		
		return defaultField;
	}
	
	@Override
	public IRender help()
	{
		return help;
		
	}

	/**
	 * 
	 * @param method
	 * @param parameters
	 * @param dir
	 * @param calcFile
	 * @return true if an error, false otherwise
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	protected boolean executeHttpMethod(String method,List <NameValuePair> parameters,String dir,String calcFile) throws ClientProtocolException, IOException
	{
		HttpPost httpPost 			= new HttpPost(shell.getAppName() + method);
		httpPost.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));
  	    HttpResponse response = httpClient.execute(targetHost,httpPost);
  	    
  	    HttpEntity entity = response.getEntity();
  	    InputStream instream = null;
  	    BufferedInputStream reader = null;
  	    byte[] buffer = null;
  	    int read=0;
  	    FileOutputStream file = null;
  	    BufferedOutputStream writer = null;
  	    try
  	    {
	  	    instream = entity.getContent();
	  	    reader = new BufferedInputStream(instream);
	  	    buffer = new byte[BUFFER_SIZE];
	  	    file = new FileOutputStream(dir + GeneralConstants.PARENT_SEPARATOR + calcFile);
	  	    writer = new BufferedOutputStream(file);
	  	    
	  	    read=reader.read(buffer,0,BUFFER_SIZE);
	  	    
	  	    while (-1 != read)
	  	    {
	  	    	writer.write(buffer, 0, read);
	  	    	read=reader.read(buffer,0,BUFFER_SIZE);
	  	    }
	  	    return false;
  	    }
  	    finally
  	    {
  	    	if (writer != null) 	writer.close();
  	    	if (file != null) 		file.close();
  	    	if (reader != null) 	reader.close();
  	    	if (instream != null) 	instream.close();
  	    }
		
	}
	protected IDataHolder executeHttpMethod(String method,List <NameValuePair> parameters) throws ClientProtocolException, IOException, ClassNotFoundException
	{
        
		HttpPost httpPost 			= new HttpPost(shell.getAppName() + method);
		httpPost.setEntity(new UrlEncodedFormEntity(parameters, HTTP.UTF_8));
		return execMethod(httpPost);
	}

	protected IDataHolder executeHttpMethod(String method,HttpEntity reqEntity) throws ClientProtocolException, IOException, ClassNotFoundException
	{        
		HttpPost httpPost 			= new HttpPost(shell.getAppName() + method);
		httpPost.setEntity(reqEntity);
		return execMethod(httpPost);
	}

	
	private IDataHolder execMethod(HttpPost httpPost) throws ClassNotFoundException, IOException
	{	   		
	    try{
		    byte[] response = httpClient.execute(targetHost,httpPost,byteResponseHandler);
	    	String responseStr = new String(response);	    	    	
	    	if (responseStr.matches(OK + "|" + UNKNOWN))  
	    		return new DefaultDH();	    
	    	if (responseStr.matches("[0-9]+"))
	    		return new MessageDH("", responseStr, false);
	    	return readObject(response);
	    }catch(Exception e){
	    	throw e;
	    }
	}
	
	private IDataHolder readObject(byte[] response){
		ByteArrayInputStream bais = null;
		ObjectInputStream ois = null;
	    try{
	    	bais = new ByteArrayInputStream(response);
		    ois = new ObjectInputStream(bais);
			return  (IDataHolder) ois.readObject();
	    }catch(IOException | ClassNotFoundException e){
	    	return new MessageDH("Error while receiving data from server", e.getMessage(), true);
	    }finally{
	    	try{
	    		if (ois  != null ) ois.close();
		        if (bais != null ) bais.close();	
	    	}catch(IOException e){
	    		return new MessageDH("Error while receiving data from server", e.getMessage(), true);
	    	}
	    }
	}
	
	public static String normalizeField(String value) {
	    String symbolPattern  = "[\\.\\[\\~#%&*{}/:<>¿?|\\\'\\\"\\]\\$\\^\\\\]+";       
        return value.replaceAll(symbolPattern,"_").replaceAll("[ ]+", "_");
	}	
}
