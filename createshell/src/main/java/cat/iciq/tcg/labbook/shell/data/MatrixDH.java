/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.data;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Set;
import java.util.Vector;

/**
 * Matrix Data Holder
 * @author jiglesias
 *
 */
public class MatrixDH implements IDataHolder,Serializable{
	
	private static final int STRING_TYPE = 0,FLOAT_TYPE=1, INT_TYPE=2;
	private Vector<Vector<String>> 		strs;
	private Vector<Vector<Float>>  		flts;
	private Vector<Vector<Integer>>    	ints;
	private Vector<HashMap<Integer,Integer>> 	colMap;  // tries to map column to type and column into the type
	private HashMap<Integer,TableColumn>		colToTypeMap;
	
	public MatrixDH()
	{
		strs 			= new Vector<Vector<String>>	();
		flts 			= new Vector<Vector<Float>>		();
		ints 			= new Vector<Vector<Integer>>	();
		colMap 			= new Vector<HashMap<Integer,Integer>>	();
		colToTypeMap 	= new HashMap<Integer,TableColumn>	();
		
		colMap.add(STRING_TYPE, new HashMap<Integer,Integer>());
		colMap.add(FLOAT_TYPE, 	new HashMap<Integer,Integer>());
		colMap.add(INT_TYPE, 	new HashMap<Integer,Integer>());
	}

	// This functions sets the values of the columns of the different types
	@Override	
	public void setStringColumn(int column,Vector<String> values)
	{
		int position = strs.size();
		strs.add(position, values);
		
		colMap.get(STRING_TYPE).put(column, position);
		colToTypeMap.put(column, new TableColumn(STRING_TYPE,position));
	}
	@Override	
	public void setFloatColumn(int column,Vector<Float> values)
	{
		int position = flts.size();
		flts.add(position, values);
		
		colMap.get(FLOAT_TYPE).put(column, position);
		colToTypeMap.put(column, new TableColumn(FLOAT_TYPE,position));
	}
	@Override	
	public void setIntegerColumn(int column,Vector<Integer> values)
	{
		int position = ints.size();
		ints.add(position, values);
		
		colMap.get(INT_TYPE).put(column, position);
		colToTypeMap.put(column, new TableColumn(INT_TYPE,position));
	}
	@Override	
	public int getColumnType(int column)
	{
		return colToTypeMap.get(column).getTable();	
	}
	@Override	
	public Vector<String> getStringColumn(int column)
	{
		return strs.get(colToTypeMap.get(column).getColumn());	
	}
	@Override	
	public Vector<Float> getFloatColumn(int column)
	{
		return flts.get(colToTypeMap.get(column).getColumn());	
	}
	@Override	
	public Vector<Integer> getIntegerColumn(int column)
	{
		return ints.get(colToTypeMap.get(column).getColumn());	
	}


	/**
	 * Utility function to return a row as a column. Very useful for printing purposes... 
	 * @param row
	 * @return
	 */
	@Override	
	public Vector<String> getRowAsString(int row)
	{
		Vector<String> rowToReturn = new Vector<String>();
		
		for (int i=0;i<colToTypeMap.size();i++)
		{
			switch (colToTypeMap.get(i).getTable())
			{
			case STRING_TYPE:
				rowToReturn.add(strs.get(colToTypeMap.get(i).getColumn()).get(row));
				break;
			case FLOAT_TYPE:
				rowToReturn.add(flts.get(colToTypeMap.get(i).getColumn()).get(row).toString());
				break;
			case INT_TYPE:
				rowToReturn.add(ints.get(colToTypeMap.get(i).getColumn()).get(row).toString());
				break;
				
			}
		}
		
		return rowToReturn;
	}
	@Override	
	public String getTitleMessage()
	{
		return null;
	}
	@Override	
	public String getBodyMessage()
	{
		return null;
	}

	@Override
	public boolean isError() {

		return false;
	}
	@Override
	public int getMatrixRows() { 
		
				if (strs.size() > 0) 	return strs.get(0).size();
		else 	if (flts.size()>0) 		return flts.get(0).size();
		else 							return ints.get(0).size();
	}

	@Override
	public String getElement(int i) {
		
		return null;
	}

	@Override
	public int getNumElements() {
		
		return 0;
	}

	@Override
	public void setElement(String element) {
		
		
	}

	@Override
	public void setElementAt(String element, int pos) {
		
		
	}

	@Override
	public Set<String> getKeys() {
		
		return null;
	}

	@Override
	public String getValue(String key) {
		
		return null;
	}

	@Override
	public void setKeyValue(String key, String value) {
		
		
	}

	@Override
	public Set<String> getHiddenData() {
		return null;
	}

	@Override
	public void setHiddenData(String str) {
		
	}
}
