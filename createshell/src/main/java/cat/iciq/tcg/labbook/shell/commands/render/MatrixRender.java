/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.render;

import java.io.PrintStream;
import java.io.Serializable;
import java.util.Vector;

import cat.iciq.tcg.labbook.shell.data.IDataHolder;

public class MatrixRender implements IRender, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public static final String 	COLUMN_SEPARATION ="  ";
	public static final String 	WHITE_SPACE_TO_FILL = "                                                                                                                                                                                                              ";
	public static final String 	SEPARATION_LINE = "----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------";
	private Vector<String> 		titles;
	private Vector<Integer> 	colSizes;
	private IDataHolder			data;

	public void setTitles(Vector<String> titles)
	{
		this.titles = titles;
	}
	public void setColumnSizes(Vector<Integer> colSizes)
	{
		this.colSizes = colSizes;
	}
	public void setData(IDataHolder data)
	{
		this.data = data;
	}
	@Override
	public void render(PrintStream output) {
	
		int size = data.getMatrixRows();

		printRow(output,titles);
		printSeparator(output);
		// printing the contents.
		for (int i=0;i<size;i++)
		{
			printRow(output,data.getRowAsString(i));
		}

	}
	
	private void printSeparator(PrintStream output) {
		
		for (int i=0;i<colSizes.size()-1;i++)
		{
			output.print(SEPARATION_LINE.substring(0,colSizes.get(i)+ COLUMN_SEPARATION.length()));
		}
		output.print(SEPARATION_LINE.substring(0,colSizes.get(colSizes.size()-1)));
		output.print("\n");
	}
	protected void printRow(PrintStream output, Vector<String> row)
	{
		String colValue;
		int    colSize;
		
		for (int j =0;j<row.size()-1;j++)
		{
			colValue = row.get(j);
			colSize  = colSizes.get(j);
			if (colValue.length() < colSize)
			{
				output.print(colValue + WHITE_SPACE_TO_FILL.substring(0, colSize - colValue.length()) +COLUMN_SEPARATION);
			}
			else	// bigger or the same
			{
				output.print(colValue.substring(0, colSize) + COLUMN_SEPARATION);
			}

		}
		colValue = row.get(row.size()-1);
		colSize  = colSizes.get(row.size()-1);
		if (colValue.length() < colSize)
		{
			output.print(colValue + WHITE_SPACE_TO_FILL.substring(0, colSize - colValue.length()));
		}
		else	// bigger or the same
		{
			output.print(colValue.substring(0, colSize));
		}
		output.print("\n");
		
	}

}
