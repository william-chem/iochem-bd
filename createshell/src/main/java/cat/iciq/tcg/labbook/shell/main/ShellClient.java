/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.main;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLConnection;
import java.security.GeneralSecurityException;
import java.security.InvalidKeyException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;

import org.apache.commons.codec.DecoderException;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpHost;
import org.apache.http.NameValuePair;
import org.apache.http.NoHttpResponseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.ssl.SSLContextBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.AuthenticationException;
import org.apache.shiro.cas.CasRealm;
import org.apache.shiro.cas.CasSubjectFactory;
import org.apache.shiro.cas.CasToken;
import org.apache.shiro.mgt.DefaultSecurityManager;
import org.apache.shiro.subject.Subject;

import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.services.CATPro;
import cat.iciq.tcg.labbook.shell.commands.services.CDPro;
import cat.iciq.tcg.labbook.shell.commands.services.CPro;
import cat.iciq.tcg.labbook.shell.commands.services.DCalc;
import cat.iciq.tcg.labbook.shell.commands.services.DPro;
import cat.iciq.tcg.labbook.shell.commands.services.FindPro;
import cat.iciq.tcg.labbook.shell.commands.services.GetXYZ;
import cat.iciq.tcg.labbook.shell.commands.services.LSPro;
import cat.iciq.tcg.labbook.shell.commands.services.LoadCalculation;
import cat.iciq.tcg.labbook.shell.commands.services.MCalc;
import cat.iciq.tcg.labbook.shell.commands.services.MPro;
import cat.iciq.tcg.labbook.shell.commands.services.PWDPro;
import cat.iciq.tcg.labbook.shell.commands.services.Query;
import cat.iciq.tcg.labbook.shell.commands.services.ViewCalc;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.tests.utils.ByteArrayResponseHandler;
import cat.iciq.tcg.labbook.shell.tests.utils.MyX509TrustManager;
import cat.iciq.tcg.labbook.shell.tests.utils.StopWatch;
import cat.iciq.tcg.labbook.shell.utils.HttpUtils;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;

public class ShellClient {
	
	private static final Logger log = LogManager.getLogger(ShellClient.class.getName());
	
	// client parameters:
	private static final String	SECURE_SERVER_SCHEME = "https";
	private static final int SHIRO_SESSION_TIMEOUT_MILLIS = 60 * 60 * 1000;	//One hour timeout
	//Session timeout daemon
	private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
	private ScheduledFuture<?> closerHandle = null; 	
	//CAS Server URI variables
	private static String casServerHostname = null;
	private static String casServerApp = null;
	private static String casServerPort = null;
	private static String casServerPath	= null;
	private static String casServerTicketPath = null;
	private static String casServerLogoutPath = null;
	//CREATE Server URI variables
	private static String createServerHostname = null;
	private static String createServerApp = null;
	private static String createServerPortDelimiter = null;
	private static String createServerPort = null;
	private static String createServerPath = null;
	private static String createServerLogin = null;
	//CAS CREATE registered service urls
	private static String casRegisteredService = null;	
	//CAS server credentials / tickets	
	private static String ticketGrantingTicket = null;	
		
	private static HttpHost targetHost = null;
	private static HttpHost casHttpHost = null;
	
	private static final Pattern flagPattern = Pattern.compile("\\s-\\w+");
	private static final Pattern defaultOptionPattern = Pattern.compile("^\\w+\\s+[^-][\\S]+");
	private static final CharSequence HELP_FLAG = " -h";

	private PrintStream out;	
	private String path;
	private HashMap<String,ICommand> commands;
	
	private ResponseHandler<byte[]> byteResponseHandler;
	private CloseableHttpClient httpClient;
	private boolean debugMode;
	
	public String getAppName() { 
		if (createServerApp.equals("")) 	
			return "/";
		else 
			return "/"+createServerApp+"/";
	}
	
	public String getPath() {
		return path;
	}
	
	public void setPath(String path) {
		this.path = path;
	}
	
	public String getUser() throws BrowseCredentialsException {
		return ShiroManager.getCurrent().getUserName();
	}
	
	public void setOut(PrintStream p) {
		this.out = p;
	}
	
	private String getCommandName(String commandLine){		
		if (commandLine.indexOf(" ") != -1)	// an space at least		
			return commandLine.substring(0,commandLine.indexOf(" "));
		else								// no spaces.
			return commandLine.substring(0,commandLine.length());		
	}
	
	public ShellClient(Properties prop, String tgtPath, String keystorePath, String keystorePassword) throws Exception {				
		try {
			loadProperties(prop);	
			initHttpClient(keystorePath, keystorePassword, tgtPath);
			defineAvailableCommands();
		} catch (Exception e) {
			log.error(e.getMessage());
			throw e;
		}
	}
	
	private void readTicketGrantingTicket(String tgtPath) throws IOException{
		try {
			ticketGrantingTicket = FileUtils.readFileToString(new File(tgtPath));
		} catch (IOException e) {
			throw e;
		}
	}
	
	private void loadProperties(Properties prop) throws IOException, NumberFormatException, InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException, DecoderException{
		casServerHostname = prop.getProperty("cas.server.hostname");
		casServerPort = prop.getProperty("cas.server.port");
		casServerApp = prop.getProperty("cas.server.app");		
	    casServerPath = "/" + casServerApp;
		casServerTicketPath = casServerPath + "/v1/tickets";
		casServerLogoutPath = casServerPath + "/logout";		
		createServerHostname = prop.getProperty("create.server.hostname");		
		createServerPort = prop.getProperty("create.server.port");
		createServerPort = createServerPort.equals("")? "443": createServerPort;   
		createServerPortDelimiter = createServerPort.equals("")? "" : ":";  
		createServerApp = prop.getProperty("create.server.app");		
		
		createServerPath = "/" + createServerApp;
		createServerLogin = createServerPath + "/shiro-cas";	
		casRegisteredService = SECURE_SERVER_SCHEME + "://" + createServerHostname + createServerPortDelimiter + createServerPort + createServerPath+ "/shiro-cas";	
			
		debugMode = Boolean.parseBoolean(prop.getProperty("debug.mode"));
				
		System.setProperty("JUMBO_TEMPLATE_BASE_URL", prop.getProperty("jumbo.template.base.url"));
		System.setProperty("INSTITUTION_NAME", prop.getProperty("mets.institution.name"));
	}
	
	private boolean existDefaultParam(String command)
	{
		return defaultOptionPattern.matcher(command).find(); 
	}
	private String addDefaultParam(String option,String command)
	{
		int place = command.indexOf(" ");
		return command.substring(0,place) + " " + option + " " + command.substring(place);
	}
	private boolean isThereParameters(String commandLine)
	{
		return (commandLine.contains(" -"));	// if there is an space plus a - then there are parameters.
	}
	
	private int getOptionEnd(String comm)
	{
		Matcher m = flagPattern.matcher(comm);
		
		
		if (m.find()) 	return m.end();
		else			return comm.length();
		
	}
	private int getValueEnd(String comm)
	{
		Matcher m = flagPattern.matcher(comm);
		
		if (comm.startsWith("-")) 	return 0;
		else if (m.find()) 			return m.start()+1;
		else						return comm.length();
		
	}
	/**
	 * All the parameters MUST have a value in this version of getParameters.
	 * @param commandLine string containing the command line entered by the user.
	 * @return a hashtable with the option and values of that options.
	 */
	private Hashtable<String,String> getParameters(String commandLine)
	{
		int paramBegin;
		Hashtable<String,String> param= new Hashtable<String,String>();
		
		paramBegin = commandLine.indexOf(" ");
		
		int optionBegin;
		int optionEnd;
		String optionName;
		String optionValue;
		int valueEnd;
		
		if (paramBegin == -1) return param;
		else
		{
			commandLine = " " + commandLine.substring(paramBegin).trim();
			while (!(commandLine.equals(" ")))
			{
				
				optionBegin	= commandLine.indexOf(" -")+1;
				optionEnd	= getOptionEnd(commandLine);
				optionName  = commandLine.substring(optionBegin,optionEnd);
				commandLine = commandLine.substring(optionEnd).trim();
				valueEnd	= getValueEnd(commandLine);
				optionValue = commandLine.substring(0, valueEnd);
				commandLine	= " " + commandLine.substring(valueEnd).trim();
				param.put(optionName.trim(), optionValue.trim());				
			}
			
		}
		return param;
	}
	
	public void execCommand(String message) {
		
		StopWatch watch = new StopWatch();
		String commandName = getCommandName(message); 
		SecurityUtils.getSubject().getSession().touch();	//Refresh Shiro session timeout
		resetTimeoutDaemon();								//Refresh Session timeout daemon (handles shell client shutdown)
		if (!(commands.containsKey(commandName))) 			
		{
			out.println("Command not found.");
			return;
		}
		
		ICommand 					comm 	= commands.get(commandName);
		Hashtable<String,String>   	param	= null; 
		
		if (helpRequest(message))		
		{
			comm.help().render(out);
			return;
		}
		if (existDefaultParam(message)) message = addDefaultParam(comm.getDefOption(),message);
		
		if (isThereParameters(message)) param = getParameters(message);
		else 							param = new Hashtable<String,String>();
		try
		{
			if (debugMode) 
				watch.start();
			if (commandName.equals("loadcalc")) 
				out.println("Processing calculation, please wait.");
			
			try {
				comm.execute(param).render(out);
			}catch(NoHttpResponseException e) {
				comm.execute(param).render(out);
			}
			if (debugMode)
			{
				watch.stop();
				out.println("Elapsed Time : " + watch.toString());
			}
		}
		catch (Exception e)
		{
			out.println(e.getMessage());
			//e.printStackTrace();
		}
	}

	private boolean helpRequest(String message) {
		
		return message.contains(HELP_FLAG);
	}

	public void prompt()
	{
		out.print ("rep-shell>> ");
		out.flush();
	}

	public void close() throws ClientProtocolException, IOException {
		logoutCAS();
		httpClient.getConnectionManager().shutdown();	
		if(closerHandle != null)
			closerHandle.cancel(true);	//End timeout daemon
		scheduler.shutdown();		
	}
	
	private void defineAvailableCommands(){		
		commands = new HashMap<String,ICommand>();
		commands.put("cpro", 	new CPro(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("cdpro", 	new CDPro(targetHost,byteResponseHandler,httpClient,this));
		commands.put("lspro", 	new LSPro(targetHost,byteResponseHandler,httpClient,this));
		commands.put("mpro", 	new MPro(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("dpro", 	new DPro(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("pwdpro", 	new PWDPro(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("catpro", 	new CATPro(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("findpro",	new FindPro(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("loadcalc",new LoadCalculation(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("viewcalc",new ViewCalc(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("dcalc",	new DCalc(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("mcalc",	new MCalc(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("query",	new Query(targetHost,byteResponseHandler,httpClient,	this));
		commands.put("getxyz",	new GetXYZ(targetHost,byteResponseHandler,httpClient,	this));
	}
	
	protected void initHttpClient(String keystorePath, String keystorePassword, String tgtPath) throws Exception
	{        	
        initConnectionVariables(keystorePath, keystorePassword);
        readTicketGrantingTicket(tgtPath);    	
        loadShiroEnvironment();
        buildUserPath();
        loginCAS();		
		resetTimeoutDaemon();		
	}
	
	private void initConnectionVariables(String keystorePath, String keystorePassword) throws GeneralSecurityException, IOException{
        httpClient = HttpUtils.buildAllowAllHttpClient(keystorePath, keystorePassword);
        casHttpHost = new HttpHost(casServerHostname, casServerPort.equals("")? -1: Integer.valueOf(casServerPort), SECURE_SERVER_SCHEME);
        targetHost = new HttpHost(createServerHostname, createServerPort.equals("")? -1 : Integer.valueOf(createServerPort), SECURE_SERVER_SCHEME);
        byteResponseHandler = new ByteArrayResponseHandler();
	}

	public InputStream getUrlInputStream(URL url, SSLContext ctx) throws Exception {		
		URLConnection conn = url.openConnection();
		if (conn instanceof HttpsURLConnection && ctx != null) {
			((HttpsURLConnection)conn).setSSLSocketFactory(
				ctx.getSocketFactory());
		}
		InputStream is = conn.getInputStream();
		int bytesRead, bufsz = Math.max(is.available(), 4096);
		ByteArrayOutputStream os = new ByteArrayOutputStream(bufsz);
		byte[] buffer = new byte[bufsz];
		while ((bytesRead = is.read(buffer)) > 0)
		  os.write(buffer, 0, bytesRead);
		byte[] content = os.toByteArray();
		os.close(); 
		is.close();
		return new ByteArrayInputStream(content);
	}
	
	private void loginCAS() throws ClientProtocolException, IOException, BrowseCredentialsException, KeyManagementException{
		HttpGet get = new HttpGet(createServerLogin + "?ticket=" + generateServiceTicket());
		byte[] response = httpClient.execute(casHttpHost, get, byteResponseHandler);
		log.info(new String(response));			
	}
	
	private void logoutCAS() throws ClientProtocolException, IOException{
		HttpPost post = new HttpPost(casServerLogoutPath);
		byte[] response = httpClient.execute(casHttpHost, post,byteResponseHandler);
		log.info(new String(response));
	}	
		
	private String generateServiceTicket() throws KeyManagementException, UnsupportedEncodingException {
		String serviceTicket = getServiceTicket(casHttpHost, casServerTicketPath,ticketGrantingTicket, casRegisteredService);		
		if(serviceTicket == null)
			throw new KeyManagementException("Unable to get Service Ticket (ST) from CAS login server.");
		return serviceTicket; 
	}


	
	private String getServiceTicket(HttpHost casHost, String ticketPath, String ticketGrantingTicket, String service) throws UnsupportedEncodingException {		
		if (ticketGrantingTicket == null)
			return null;
		HttpPost post = new HttpPost(ticketPath + "/" + ticketGrantingTicket);
		
		List <NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("service", service));
		post.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));		

		try {
			byte[] response = httpClient.execute(casHost, post,byteResponseHandler);
			return new String(response);
					    
		} catch (final IOException e) {
			log.warn(e.getMessage());			
		}
		finally {
			post.releaseConnection();
		}

		return null;
	}
	
	private void loadShiroEnvironment() throws IOException, BrowseCredentialsException, KeyManagementException{	
		CasToken token = new CasToken(generateServiceTicket());
		CasRealm realm = new CasRealm();
		realm.setDefaultRoles("ROLE_USER");
		realm.setValidationProtocol("SAML");
		realm.setCasServerUrlPrefix(casHttpHost.toURI() + casServerPath);
		realm.setCasService(casRegisteredService);		
		DefaultSecurityManager securityManager = new DefaultSecurityManager(realm);
		CasSubjectFactory casSubjectFactory = new CasSubjectFactory();
		securityManager.setSubjectFactory(casSubjectFactory);
		
		SecurityUtils.setSecurityManager(securityManager);
		//Validate against CAS server to access principal information (retrieved via SAML)
		Subject currentUser = SecurityUtils.getSubject();
		try{
			currentUser.login(token);	
			currentUser.getSession().setTimeout(SHIRO_SESSION_TIMEOUT_MILLIS);
		}catch(AuthenticationException e){
			log.error(e.getMessage());
			throw new IOException("Error login on CREATE web system. Check credentials");
		}
	}
	
	private void buildUserPath() throws BrowseCredentialsException{
		path = GeneralConstants.DB_ROOT + ShiroManager.getCurrent().getUserName();
		log.info("Currently loaded, " + path);		
	}
		
	private void resetTimeoutDaemon(){
		if(closerHandle != null)
			closerHandle.cancel(true);	//If it exists we'll cancel it		
		ExitByTimeoutDaemon logout = new ExitByTimeoutDaemon(httpClient);				
		closerHandle = scheduler.schedule(logout, SHIRO_SESSION_TIMEOUT_MILLIS, TimeUnit.MILLISECONDS);		
	}	
	
	private class ExitByTimeoutDaemon implements Runnable{
		CloseableHttpClient httpClient = null;
		public ExitByTimeoutDaemon(CloseableHttpClient httpClient){
			this.httpClient = httpClient;
		}		
		@Override
		public void run() {			
			try {
				HttpPost post = new HttpPost(casServerLogoutPath);
				byte[] response = httpClient.execute(casHttpHost, post, new ByteArrayResponseHandler());
				log.info("Session expired due to inactivity. " + new String(response));				
			} catch (ClientProtocolException e) {
				log.error(e.getMessage());				
			} catch (IOException e) {
				log.error(e.getMessage());
			} finally{				
				System.exit(0);
			}
		}
	}	
}

