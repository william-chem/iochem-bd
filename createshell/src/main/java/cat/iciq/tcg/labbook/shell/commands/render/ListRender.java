/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.render;

import java.io.PrintStream;
import java.io.Serializable;

import cat.iciq.tcg.labbook.shell.data.IDataHolder;

public class ListRender implements IRender, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private IDataHolder			data;
	private static final String SEPARATOR 			= "  ";
	private static final String WORD_SEPARATOR 		= " ";
	private static final String START_DECORATION 	= "{";
	private static final String END_DECORATION 		= "}";

	public void setData(IDataHolder data)
	{
		this.data = data;
	}
	@Override
	public void render(PrintStream output) {
	
		int size = data.getMatrixRows();
		int wordSize = 0;
		String word  = "";
		boolean project = true;

		// printing the contents.
		for (int i=0;i<size;i++)
		{
			word 	 = data.getStringColumn(1).get(i);
			project = (data.getStringColumn(0).get(i).charAt(0) == 'P'); // if it's a project we put decoration.
			wordSize = word.length();
			
			if (project) 	output.print(START_DECORATION + word + END_DECORATION + SEPARATOR + WORD_SEPARATOR);
			else			output.print(word + SEPARATOR + WORD_SEPARATOR);
		}
		output.print("\n");

	}
	

}
