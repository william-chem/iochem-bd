/**
 * ioChem-BD Create shell project - Upload shell interface for ioChem-BD Create module.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.shell.commands.services;

import java.io.IOException;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.Vector;

import org.apache.http.HttpHost;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.message.BasicNameValuePair;

import cat.iciq.tcg.labbook.shell.commands.Command;
import cat.iciq.tcg.labbook.shell.commands.ICommand;
import cat.iciq.tcg.labbook.shell.commands.render.DefaultRender;
import cat.iciq.tcg.labbook.shell.commands.render.ErrorRender;
import cat.iciq.tcg.labbook.shell.commands.render.HelpRender;
import cat.iciq.tcg.labbook.shell.commands.render.IRender;
import cat.iciq.tcg.labbook.shell.commands.render.MatrixRender;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.exceptions.XMLException;
import cat.iciq.tcg.labbook.shell.main.ShellClient;

public class FindPro extends Command implements ICommand{
	
	private Vector<String> 	titles;
	private Vector<Integer> colSizes;
	private MatrixRender 	mr;
	public FindPro(HttpHost targetHost,ResponseHandler<byte[]> byteResponseHandler, CloseableHttpClient httpClient,ShellClient shell)
	{
		String[] mandatoryParameters 	= {};
		String[] optionalParameters  	= {"-n","-d","-p"};
		String[] singleValueParameters 	= {"-n","-d","-p"};
		String[] integerFields			= {};
		String[] stringFields			= singleValueParameters;
		String[] floatFields			= {};
		init(mandatoryParameters,optionalParameters,singleValueParameters,integerFields,stringFields,floatFields,"-n",
			 targetHost,byteResponseHandler,httpClient,shell	);
		LinkedHashMap<String,String> help = new LinkedHashMap<String,String>();
		help.put("-n", " Regular expression to find in the name field of project          (optional)");
		help.put("-d", " Regular expression to find in the description field of project   (optional)");
		help.put("-p", " Regular expression to find in the path field of project          (optional)");

		
		this.help = new HelpRender("findpro","findpro description. Always searches in a recursive way.",help);
	
		titles = new Vector<String>();
		titles.add("Name");
		titles.add("Path");
		colSizes = new Vector<Integer>();
		colSizes.add(20);
		colSizes.add(60);
		
		mr = new MatrixRender();
		mr.setColumnSizes(colSizes);
		mr.setTitles(titles);	
		
	}
	@Override
	public IRender execute(Hashtable<String, String> parameters) throws ClientProtocolException, IOException, ClassNotFoundException,XMLException
	{
		IRender ren = super.execute(parameters);
		if (ren != null) return ren;
        // newPath and path have a value.
        
        String nameSearch 	= parameters.get("-n"	); 
        String pathSearch 	= parameters.get("-p"	); 
        String descSearch 	= parameters.get("-d"	); 
        renewHttpParm();
        if (nameSearch 		!= null) 		httpParam.add(new BasicNameValuePair("nameSearch", 		nameSearch		)	);
        if (pathSearch 		!= null) 		httpParam.add(new BasicNameValuePair("pathSearch", 		pathSearch		)	);
        if (descSearch 		!= null) 		httpParam.add(new BasicNameValuePair("descSearch", 		descSearch		)	);
        									httpParam.add(new BasicNameValuePair("path", 			shell.getPath() )	);
        
		IDataHolder data = executeHttpMethod("userCommands/findpro",	httpParam);

		if (data.isError()) return new ErrorRender(data.getTitleMessage(),data.getBodyMessage());
		else				
		{
			if (data instanceof MatrixDH)
			{
				mr.setData(data);
				return mr;
			}
			else return new DefaultRender();
		}
	}
	@Override
	public String getDefOption()
	{
		return "";
	}

}
