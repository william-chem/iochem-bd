#!/bin/bash
#GROMACS upload helper script, see more at https://docs.iochem-bd.org/en/latest/guides/usage/uploading-content-to-create/using-shell-client/shell-automated-scripts.html 

# Default static file names, change them in order to fit your naming conventions
INPUT_DEFAULT_FILENAME="input.mdp"
OUTPUT_DEFAULT_FILENAME="output.log"
GEOMETRY_DEFAULT_FILENAME="geometry.gro"
TRAJECTORY_DEFAULT_FILENAME="trajectory.xtc"


if [ -z "$HOME_PATH" ]; then   #If HOME_PATH not defined use default $HOME 
    HOME_PATH="$HOME"
fi

#Constants
LOAD_CALC_EXPECTED_MESSAGE="Processing calculation, please wait.\n?\s?[0-9]+"
EMPTY_MESSAGE="^$"


# Reset all variables that might be set
input=
output=
geometry=
trajectory=
additional=
name=
desc=
verbose=0
auto=0
retval=

function show_help(){
   echo -e "loadgromacs - Utility to upload GROMACS calculations to ioChem-BD Create module"
   echo
   echo -e "Usage: loadgromacs [arguments]"
   echo
   echo -e "Arguments"
   echo -e "\t-h or --help\t\tDisplay this help"
   echo -e "\t-i <input>\t\tUse this file as input file of calculation, *.mdp (required)"
   echo -e "\t-o <output>\t\tUse this file as output file of calculation, *.log (required)"
   echo -e "\t-oc <geometry>\t\tGeometry file of calculation, *.gro (required)"
   echo -e "\t-t <trajectory>\t\tTrajectory file of calculation, *.xtc (required)"
   echo -e "\t-a file \t\tAdd an additional file to current upload."
   echo -e "\t-n  name\t\tSet uploaded calculation name (required)"
   echo -e "\t-d  description\t\tSet uploaded calculation description (required)"
   echo -e "\t-v      \t\tVerbose mode"
   echo -e "\t--auto\t\t Autogenerate project and subproject structure using current calculation path"
   echo
   echo -e "If name or description fields are not defined, will use parent folder name as default value."
   echo -e "Will use following file naming conventions as default, if they exists in current directory:"
   echo -e "\t- Will upload \"$INPUT_DEFAULT_FILENAME\" file otherwise defined with -i parameter"
   echo -e "\t- Will upload \"$OUTPUT_DEFAULT_FILENAME\" file otherwise defined with -o parameter"
   echo -e "\t- Will upload \"$GEOMETRY_DEFAULT_FILENAME\" file otherwise defined with -oc parameter"
   echo -e "\t- Will upload \"$TRAJECTORY_DEFAULT_FILENAME\" file otherwise defined with -t parameter"
   echo
   echo -e "Examples:"
   echo -e "loadgromacs"
   echo -e "    This command will set upload calculation name and description equals to parent folder and upload all files that match by name."
   echo -e "loadgromacs -n Au-CoO-H2O -d \"Sample upload\""
   echo -e "    This command will upload all matching files in directory, set calculation name as \"Au-CoO-H2O\" and description as \"Sample upload\"."
   echo -e "loadgromacs -n Au-CoO-H2O -d \"Sample upload\" -i input2.in -o output2.log -oc Au-CoO-H2O.gro -t Au-CoO-H2O.xtc"
   echo -e "    This command will behave the same as previous one but will upload different files rather than default."
   exit
}

function executeRepCommand {
   local RES=`$1`
   local EXP=`echo -e "$2"`

   if [[ ! ($RES =~ $EXP) ]]; then
      if [ ! $verbose -eq 0 ]; then
      	echo The execution of this command failed, not matching results.
	echo "RESULT:"
	echo "======="
	echo $RES
	echo "EXPECTED:"
	echo "========"
	echo $EXP
      fi
      retval=-1
   else
      if [[ $RES != "" ]]; then
	echo $RES
      fi
      retval=0
   fi
   
}

function moveBasePath {
   local RES=`$REP_SCRIPTS/exe-rep-command pwdpro`
   userhomepath=`echo $RES | sed -r 's/(\/db\/[a-zA-Z0-9]+).*/\1/'`
   if [ ! $verbose -eq 0 ]; then
	echo "User path: "$userhomepath
   fi 
   RES=`$REP_SCRIPTS/exe-rep-command cdpro -n $userhomepath`
}

#Read parameters from command line

while [ "$#" -gt 0 ]; do
    case $1 in
        -h|-\?|--help)   # Call a "show_help" function to display a synopsis, then exit.
            show_help
            exit
            ;;
        -i)       # Takes an option argument, ensuring it has been specified.
            if [ "$#" -gt 1 ]; then
                input=$2
                shift 2
                continue
            else
                echo 'ERROR: Must specify a non-empty input "-i FILE" argument.' >&2
                exit 1
            fi
            ;;
        -o)       # Takes an option argument, ensuring it has been specified.
            if [ "$#" -gt 1 ]; then
                output=$2
                shift 2
                continue
            else
                echo 'ERROR: Must specify a non-empty output "-o FILE" argument.' >&2
                exit 1
            fi
            ;;      
        -oc)       # Takes an option argument, ensuring it has been specified.
            if [ "$#" -gt 1 ]; then
                geometry=$2
                shift 2
                continue
            else
                echo 'ERROR: Must specify a non-empty output "-oc FILE" argument.' >&2
                exit 1
            fi
            ;;                  
        -t)       # Takes an option argument, ensuring it has been specified.
            if [ "$#" -gt 1 ]; then
                trajectory=$2
                shift 2
                continue
            else
                echo 'ERROR: Must specify a non-empty output "-t FILE" argument.' >&2
                exit 1
            fi
            ;; 
        -a)
            if [ "$#" -gt 1 ]; then
               if [ -z "$additional" ]; then
                  additional=$2
               else
                  additional+="#"$2  #Concatenate                 
               fi
               shift 2
               continue
            else
                echo 'ERROR: Must specify a non-empty additional "-a FILE" argument.' >&2
                exit 1
            fi
            ;;
        -n)       # Takes an option argument, ensuring it has been specified.
            if [ "$#" -gt 1 ]; then
                name=$2
                shift 2
                continue
            else
                echo 'ERROR: Must specify a non-empty calculation name "-n name" argument.' >&2
                exit 1
            fi
            ;;
        -d)       # Takes an option argument, ensuring it has been specified.
            if [ "$#" -gt 1 ]; then
                desc=$2
                shift 2
                continue
            else
                echo 'ERROR: Must specify a non-empty calculation description "-d description" argument, doble quote when using blank spaces.' >&2
                exit 1
            fi
            ;;
	-v|--verbose)
            verbose=$((verbose + 1)) # Each -v argument adds 1 to verbosity.
            ;;	
        --auto)
            auto=$((auto + 1))					
            ;;	
        --)                          # End of all options.
            shift
            break
            ;;
        -?*)
            printf 'WARN: Unknown option (ignored): %s\n' "$1" >&2
            ;;
        *)               # Default case: If no more options then break out of the loop.
            break
    esac

    shift
done

#Check required files existence
if [ -z "$input" ]; then   #Not used defined
   if [ ! -f $INPUT_DEFAULT_FILENAME ]; then    #Not used defined
	echo "GROMACS input file : '"$INPUT_DEFAULT_FILENAME"' not found. Aborting upload."	
        exit 1
   else
	input=$INPUT_DEFAULT_FILENAME
   fi
else
    if [ ! -f $input ]; then   #Check user-defined file existence 
	echo "GROMACS input file not found on path '"$input"'. Aborting upload."	
        exit 1
    fi
fi

if [ -z "$output" ]; then   #Not used defined
   if [ ! -f $OUTPUT_DEFAULT_FILENAME ]; then    #Not used defined
	echo "GROMACS output file : '"$OUTPUT_DEFAULT_FILENAME"' not found. Aborting upload."	
        exit 1
   else
	output=$OUTPUT_DEFAULT_FILENAME
   fi
else
    if [ ! -f $output ]; then   #Check user-defined file existence 
	echo "GROMACS output file not found on path '"$output"'. Aborting upload."	
        exit 1
    fi
fi

if [ -z "$geometry" ]; then   #Not used defined
   if [ ! -f $GEOMETRY_DEFAULT_FILENAME ]; then    #Not used defined
	echo "GROMACS geometry file : '"$GEOMETRY_DEFAULT_FILENAME"' not found. Aborting upload."	
        exit 1
   else
	geometry=$GEOMETRY_DEFAULT_FILENAME
   fi
else
    if [ ! -f $geometry ]; then   #Check user-defined file existence 
	echo "GROMACS geometry file not found on path '"$geometry"'. Aborting upload."	
        exit 1
    fi
fi

if [ -z "$trajectory" ]; then   #Not used defined
   if [ ! -f $TRAJECTORY_DEFAULT_FILENAME ]; then    #Not used defined
	echo "GROMACS trajectory file : '"$TRAJECTORY_DEFAULT_FILENAME"' not found. Aborting upload."	
        exit 1
   else
	trajectory=$TRAJECTORY_DEFAULT_FILENAME
   fi
else
    if [ ! -f $trajectory ]; then   #Check user-defined file existence 
	echo "GROMACS trajectory file not found on path '"$trajectory"'. Aborting upload."	
        exit 1
    fi
fi

#Check additional files existence
if [ ! -z "$additional" ]; then
  addfiles=$(echo $additional | tr "#" "\n")
  for addfile in $addfiles
  do
    if [ ! -f $addfile ]; then
      echo "Additional file not found on path '"$addfile"'. Aborting upload."
      exit 1
    fi
  done
fi

#Check name and description otherwise parent folder name will be used
parentdir="$(basename "$(cd "$(dirname "$input")"; pwd)")"

if [ -z "$name" ]; then 
  escapeddir="${parentdir//[-@\$\%\&\(\)\"\' ]/_}"  
  name=$escapeddir
fi

if [ -z "$desc" ]; then  
  desc=$parentdir
fi

# Build upload calculation.
command="loadcalc -i "$input" -o "$output" -n "$name" -d "$desc" -oc "$geometry" -t "$trajectory 
if [ ! -z "$additional" ]; then
   command=$command" -a "$additional
fi

#Display defined parameters
if [ $verbose -gt 0 ]; then
  echo -e "Parameters summary:"
  echo -e "\tinput=\t"$input
  echo -e "\toutput=\t"$output
  echo -e "\tgeometry=\t"$geometry
  echo -e "\ttrajectory=\t"$trajectory
  echo -e "\tadditional=\t"$additional
  echo -e "\tname=\t"$name
  echo -e "\tdesc=\t"$desc
fi

# CREATE commands
# On auto mode we will generate project path, navigate inside and then upload calculation
if [ $auto -gt 0 ]; then
	moveBasePath
	full_path="$(cd "$(dirname "$output")"; pwd)"       #Will use 'output' file parent folders 
	home_path="$(cd $HOME_PATH; pwd)"
	partial_path="${full_path/$home_path/''}"            #Remove user home folder
	partial_path="${partial_path//[-@\$\%\&\(\) \"]/_}"   #Replace special characters and blank spaces by underscore
	if [ $verbose -gt 0 ]; then
		echo "Generating path : "$partial_path
	fi
	projects=$(echo $partial_path | tr "/" "\n")
	for project in $projects
	do            	
            if [ $project = "''" -o $project = "" ]; then
              continue
            fi
            project="${project//[-@\$\%\&\(\)\"\' ]/_}"
	    createpro="cpro -n "$project" -d "$project  
	    changepro="cdpro "$project                                   
	    executeRepCommand "$REP_SCRIPTS/exe-rep-command $changepro" $EMPTY_MESSAGE #Try cdpro to project if fails we'll create this project           
            if [ ! $retval -eq  0 ]; then              	  
		executeRepCommand "$REP_SCRIPTS/exe-rep-command $createpro" $EMPTY_MESSAGE
                executeRepCommand "$REP_SCRIPTS/exe-rep-command $changepro" $EMPTY_MESSAGE
	    fi	
	done 
fi

executeRepCommand "$REP_SCRIPTS/exe-rep-command $command" "$LOAD_CALC_EXPECTED_MESSAGE"     #Execute load calculation command
