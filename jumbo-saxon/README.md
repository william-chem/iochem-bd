# jumbo-saxon project

The project contains a **format conversion tool** that applies a series of hierarchical 
templates to convert textual files from:

 * Computational Chemistry and 
 * Material Science

output files into Chemical Markup Language.

The templates come from *jumbo-converters* project and comprise 
the following software packages:

 * ADF
 * Gaussian
 * Orca
 * Turbomole
 * QuantumEspresso
 * Molcas
 * VASP
 * ...

Not only the output files are converted: input files, coordinates files and
other specific format files are converted to extract the most, the better from
each format.

The class used to convert formats is:

```java
org.xmlcml.cml.converters.DynamicConverter
```
It is a callable class that runs inside threads to asynchronously convert files.

A system-wide variable must be also set to point where the conversion templates reside.
Its name is **JUMBO_TEMPLATE_BASE_URL**, and must point to the base where the templates are stored. It can point:

```
a folder (when testing functionality locally)
 JUMBO\_TEMPLATE\_BASE\_URL=file:///home/user/iochembd/workspace/jumbo-converters/jumbo-converters-compchem/jumbo-converters-compchem-gaussian/src/main/resources
 
a URL (when running inside Create, it points to /jumbo folder)
 JUMBO\_TEMPLATE\_BASE\_URL=https://test.iochem-bd.org:8445/create/jumbo
```

## Test conversion templates

Create a bundle folder with all the current templates by running the ant *publish* target inside *jumbo-converters* project.
Copy *jumbo-converters/target* folder to another folder like */tmp*  

This is an example code snippet to converts Gaussian output file to CML. 

```java

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.nio.file.Files;
import java.io.IOException;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import org.xmlcml.cml.converters.DynamicConverter;

public class TestJumbo {
	
	public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        System.setProperty("JUMBO_TEMPLATE_BASE_URL", "file:///tmp/target");
        convertFile("/home/user/test/metastable1h2o_without_alkali/NB10_opt_BH.out", "org.xmlcml.cml.converters.compchem.gaussian.log.GaussianLog2XMLConverter", null);
    }

	public static byte[] convertFile(String filePath, String converterClass, String appendText) throws IOException{
		String inputFilePath = filePath;
		String outputFilePath = filePath + ".xml";
		Long start = System.currentTimeMillis();
		try {			
			DynamicConverter converter = new DynamicConverter(converterClass, inputFilePath, outputFilePath, appendText);	
			converter.call();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	    Long end = System.currentTimeMillis();
		double seconds = (end-start)/1000.0;
		System.out.println("Ellapsed time : " + String.valueOf(seconds) + " s.");
		return Files.readAllBytes(new File(outputFilePath).toPath()); 
	}

```

Some formats combine multiple CML files into the output file, like on VASP file format which processes INCAR, POSCAR and other files 
into the OUTCAR file.

```java
    // Join to previous code 
    
    private static final String APPEND_REMOVE_TAGS_EXPR = "(<\\?xml version=\"1\\.0\" encoding=\"UTF-8\"\\?>)";

    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException, ClassNotFoundException, InstantiationException, IllegalAccessException{
        ...
        System.setProperty("JUMBO_TEMPLATE_BASE_URL", "file:///tmp/target");
        convertVasp("/home/user/test/vasp/test", "INCAR", "OUTCAR");        
        ...            
    }

	public static void convertVasp(String folderPath, String incar, String outcar) throws IOException{		
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream( );					
		outputStream.write(convertFile(folderPath + "/" + incar, "org.xmlcml.cml.converters.compchem.vasp.doscar.VaspIncar2XMLConverter", null));
		String append = new String(outputStream.toByteArray(), "UTF-8");
		append = append.replaceAll(APPEND_REMOVE_TAGS_EXPR, "");				
		convertFile(folderPath + "/" + outcar, "org.xmlcml.cml.converters.compchem.vasp.outcar.VaspOutcar2CompchemConverter", append);		
	}
```

