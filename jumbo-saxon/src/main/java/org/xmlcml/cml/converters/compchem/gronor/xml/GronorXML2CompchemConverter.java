package org.xmlcml.cml.converters.compchem.gronor.xml;

import java.nio.file.Files;
import java.nio.file.Paths;

import org.xmlcml.cml.converters.compchem.CompChemConverter;

public class GronorXML2CompchemConverter extends CompChemConverter {
		
	@Override
	public Type getInputType() {
		return Type.XML;
	}

	@Override
	public Type getOutputType() {
		return Type.CML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception {
		Files.copy(Paths.get(inputFilePath), Paths.get(outputFilePath));
	}

	@Override
	public String getRegistryInputType() {
		return "gronor_xml";
	}

	@Override
	public String getRegistryOutputType() {
		return "gronor_xml_compchem";
	}

	@Override
	public String getRegistryMessage() {
		return "Passthrough GronOr output file to CML-compchem";
	}

}
