/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.generic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jumbo2.converters.PlainTextToXmlConverter;
import jumbo2.util.Utils;

import org.w3c.dom.Document;
import org.xmlcml.cml.converters.compchem.CompChemConverter;

public class GenericText2XMLConverter extends CompChemConverter  {

	private static final String FORMAT_TYPE_REGEX = "org\\.xmlcml\\.cml\\.converters\\.compchem\\.([\\w\\d]+)\\.([\\w\\d]+)\\..*";
	private String formatType = null;
	private String formatFile = null;
	private String SOURCETOXML_TEMPLATE_RESOURCE = null;	
	
	public GenericText2XMLConverter (String classpath) throws Exception{
		Pattern p = Pattern.compile(FORMAT_TYPE_REGEX);
		Matcher m = p.matcher(classpath);
		if(m.matches()){
			formatType = m.group(1);
			formatFile = m.group(2);			
			SOURCETOXML_TEMPLATE_RESOURCE = buildTopTemplateClassPath(classpath);
		}else{
			throw new Exception("Could not parse top template classpath from " + classpath);
		}
	}
	
	private String buildTopTemplateClassPath(String classpath){
		StringBuilder topTemplateClassPath = new StringBuilder();
		String parentPath = classpath.substring(0,classpath.lastIndexOf("."));
		topTemplateClassPath.append(parentPath.replace(".", "/"));
		topTemplateClassPath.append("/templates/");
		topTemplateClassPath.append("topTemplate.xml");
		return topTemplateClassPath.toString();		
	}
	
	@Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.XML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception {
		PlainTextToXmlConverter txtToCompChem = new PlainTextToXmlConverter(inputFilePath, SOURCETOXML_TEMPLATE_RESOURCE, appendedXML);
		Document result = txtToCompChem.call();
		Utils.outputResultDocument(result, outputFilePath, false);		
	}

	@Override
	public String getRegistryInputType() {
		return formatType + "_" + formatFile;		
	}

	@Override
	public String getRegistryOutputType() {
		return formatType + "_" + formatFile + "_xml";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert " + formatType + " " + formatFile + " to XML";
	}

}
