/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem;

public abstract class CompChemConverter {

	public enum Type {TEXT, XML, CML, BINARY, CIF}; 
	
	public abstract Type getInputType();

    public abstract Type getOutputType();
    
    public void convert(String inputFilePath, String outputFilePath) throws Exception{
    	this.convert(inputFilePath, outputFilePath, null);
    }
   
    public abstract void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception;
		
	public abstract String getRegistryInputType();

	public abstract String getRegistryOutputType();

	public abstract String getRegistryMessage();

	
}
