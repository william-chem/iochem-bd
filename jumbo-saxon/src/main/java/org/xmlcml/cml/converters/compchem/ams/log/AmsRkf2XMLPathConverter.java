/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.ams.log;

import org.xmlcml.cml.converters.compchem.CompChemConverter;

import jumbo2.util.Utils;
import jumbo2.util.XMLFileManager;

public class AmsRkf2XMLPathConverter extends CompChemConverter {

    // The first .rkf file is not processed, will only keep its file path to later process it 
    // along with the second .rdk file using the rkf2cml rtool
    
    @Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.XML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
	    XMLFileManager xml = new XMLFileManager("http://www.xml-cml.org/schema", null, "<module id='filepath'>" + inputFilePath +"</module>"); 
		Utils.outputResultDocument(xml.getDocument(), outputFilePath, false);
	}

	@Override
	public String getRegistryInputType() {
		return "ams_rkf";
	}

	@Override
	public String getRegistryOutputType() {
		return "ams_rkf_xml";
	}

	@Override
	public String getRegistryMessage() {
		return "No conversion, just stores AMS .rkf real path into an XML file for its subsequent conversion.";
	}

}
