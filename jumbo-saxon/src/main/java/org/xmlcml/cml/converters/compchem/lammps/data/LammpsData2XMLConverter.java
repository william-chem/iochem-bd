package org.xmlcml.cml.converters.compchem.lammps.data;

import org.w3c.dom.Document;
import org.xmlcml.cml.converters.compchem.CompChemConverter;

import jumbo2.converters.PlainTextToXmlConverter;
import jumbo2.util.Utils;

public class LammpsData2XMLConverter extends CompChemConverter {
	
	private static final String SOURCETOXML_TEMPLATE_RESOURCE 	= "org/xmlcml/cml/converters/compchem/lammps/data/templates/topTemplate.xml";

	@Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.XML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
		PlainTextToXmlConverter txtToCompChem = new PlainTextToXmlConverter(inputFilePath, SOURCETOXML_TEMPLATE_RESOURCE, appendedXML);
		Document result = txtToCompChem.call();
		Utils.outputResultDocument(result, outputFilePath, false);
	}

	@Override
	public String getRegistryInputType() {
		return "lammps_data";
	}

	@Override
	public String getRegistryOutputType() {
		return "lammps_data_xml";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert LAMMPS data to XML";
	}
}