/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.vasp.xml;

import jumbo2.converters.PlainXmlToXmlConverter;
import jumbo2.util.Utils;

import org.w3c.dom.Document;
import org.xmlcml.cml.converters.compchem.CompChemConverter;

public class Vasprun2XMLConverter extends CompChemConverter {

		private static final String SOURCETOXML_TEMPLATE_RESOURCE = "org/xmlcml/cml/converters/compchem/vasp/xml/templates/topTemplate.xsl";

		@Override
		public Type getInputType() {
			return Type.XML;
		}

		@Override
		public Type getOutputType() {
			return Type.XML;
		}

		@Override
		public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
			PlainXmlToXmlConverter plainXmlToCompChem = new PlainXmlToXmlConverter(inputFilePath,SOURCETOXML_TEMPLATE_RESOURCE ,appendedXML);
			Document result = plainXmlToCompChem.call();		
			Utils.outputResultDocument(result, outputFilePath, false);
		}

		@Override
		public String getRegistryInputType() {
			return "vasprun";
		}

		@Override
		public String getRegistryOutputType() {
			return "vasprun_xml";
		}

		@Override
		public String getRegistryMessage() {
			return "Convert Vasp OUTCAR file to XML";
		}

	}
