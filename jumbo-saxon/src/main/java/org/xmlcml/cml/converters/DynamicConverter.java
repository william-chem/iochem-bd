/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters;

import java.util.concurrent.Callable;

import org.xmlcml.cml.converters.compchem.CompChemConverter;
import org.xmlcml.cml.converters.compchem.generic.GenericText2CompchemConverter;
import org.xmlcml.cml.converters.compchem.generic.GenericText2XMLConverter;

public class DynamicConverter implements Callable {

	private CompChemConverter converter = null;
	private String inputFilePath = null;
	private String outputFilePath = null;
	private String appendXML = null;
	
	public DynamicConverter(String convertersClass, String inputFilePath, String outputFilePath, String appendXML) throws Exception{		 
		converter =  findConverter(convertersClass);
		this.inputFilePath = inputFilePath;
		this.outputFilePath = outputFilePath;
		this.appendXML = appendXML;	
	}

	private CompChemConverter findConverter(String convertersClass) throws Exception{		
		try{
			Class matchingClass = Class.forName(convertersClass);
			return (CompChemConverter) matchingClass.newInstance();
		}catch(ClassNotFoundException e){		
			if(convertersClass.endsWith("2XMLConverter")){
				return new GenericText2XMLConverter(convertersClass);
			}else if(convertersClass.endsWith("2CompchemConverter")){
				return new GenericText2CompchemConverter(convertersClass);
			}else{
				throw e;
			}			
		} 
	}
	
	@Override
	public Object call() throws Exception {
		converter.convert(inputFilePath, outputFilePath, appendXML);
		return null;
	}
	
}
