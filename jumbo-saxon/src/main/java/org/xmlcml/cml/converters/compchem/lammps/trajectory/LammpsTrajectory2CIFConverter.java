package org.xmlcml.cml.converters.compchem.lammps.trajectory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.TreeMap;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.xmlcml.cml.converters.compchem.CompChemConverter;

import jumbo2.chemistry.Atom;
import jumbo2.chemistry.Molecule;
import jumbo2.chemistry.Point3d;
import jumbo2.chemistry.Vector3d;
import jumbo2.converters.CompressedFileToFolder;
import jumbo2.util.XMLFileManager;

public class LammpsTrajectory2CIFConverter extends CompChemConverter {
       
    private static final String[][] NAMESPACES = {	{"cc", "http://www.xml-cml.org/dictionary/compchem/"},
										    		{"cml", "http://www.xml-cml.org/schema"},
										    		{"cmlx", "http://www.xml-cml.org/schema/cmlx"},
										    		{"convention", "http://www.xml-cml.org/convention/"},
										    		{"l", "http://www.iochem-bd.org/dictionary/lammps/"},
										    		{"nonsi", "http://www.xml-cml.org/unit/nonSi/"},
										    		{"nonsi2", "http://www.iochem-bd.org/unit/nonSi2/"},
										    		{"si", "http://www.xml-cml.org/unit/si/"},
										    		{"xi", "http://www.w3.org/2001/XInclude"},
										    		{"xsd", "http://www.w3.org/2001/XMLSchema"}};
    @Override
    public Type getInputType() {
        return Type.BINARY;
    }

    @Override
    public Type getOutputType() {
        return Type.CIF;
    }

    @Override
    public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception{
    	TreeMap<Integer, String> cifMap = new TreeMap<>();    	
    	File folder = new CompressedFileToFolder(inputFilePath).call();    	    	
    	convertFolderContent(folder, cifMap, readAtomcConversionMap(appendedXML));
    	try(FileOutputStream fos = new FileOutputStream(outputFilePath);){
    		for(int step: cifMap.keySet())
        		fos.write(cifMap.get(step).getBytes());	
    	}
    	deleteDir(folder);
    }

    private HashMap<Integer, String> readAtomcConversionMap(String appendedXML) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException {
    	HashMap<Integer, String> conversionMap = new HashMap<>();  
    	
    	XMLFileManager xml = new XMLFileManager("http://www.xml-cml.org/schema", NAMESPACES, appendedXML);
    	NodeList list = xml.getItemIteratorQuery("//cml:module[@cmlx:templateRef='masses']/cml:map[@id='atomTypeLabels']/cml:link");
    	for(int inx = 0; inx < list.getLength(); inx++) {
    		Element node = (Element)list.item(inx);
    		int from = Integer.parseInt(node.getAttribute("from"));
    		String to = node.getAttribute("to");
    		conversionMap.put(from, to);
    	}    	    
		return conversionMap;
	}

    private void convertFolderContent(File extractedFolder, TreeMap<Integer, String> cifMap, HashMap <Integer, String> atomTypes) throws Exception {
    	for(File file: extractedFolder.listFiles()) {
    		if(file.isFile())
    			buildCifFile(file, cifMap, atomTypes);    		
    		else
    			convertFolderContent(file, cifMap, atomTypes);
    	}
	}
   
    private void buildCifFile(File file, TreeMap<Integer, String> cifMap, HashMap <Integer, String> atomTypes) throws IOException, Exception {    	
    	int timestep = 0;
    	Vector3d[] lattice = null;
    	Molecule mol = null;
    	
    	try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file)))) {
			String line = "";
			do {				
				if(line.matches("ITEM:\\s+BOX\\sBOUNDS.*")) { 
					lattice = readLatticeFromFile(br, line);
				}				
				if(line.matches("ITEM:\\s+ATOMS.*")) {
					line = readAtoms(br, line, mol, atomTypes, lattice);
					cifMap.put(timestep, generateCIFText(mol, lattice));
					if(line == null)	// EOF
						break;
				}					
				if(line.matches("ITEM:\\s+TIMESTEP.*")) {
					timestep = Integer.parseInt(br.readLine());
					mol = new Molecule("step-" + timestep);
				}								
			}while ((line = br.readLine()) != null);								
	    }       
    	    	
    }
    
    
    private Vector3d[] readLatticeFromFile(BufferedReader br, String line) throws IOException {
    	Vector3d[] lattice = new Vector3d[3];
    	boolean isTriclinic = line.contains("xy");

		List<Double> valuesX = parseDoublesFromText(br.readLine());
		List<Double> valuesY = parseDoublesFromText(br.readLine());
		List<Double> valuesZ = parseDoublesFromText(br.readLine());

		
		Double[][] coords = new Double[3][3];
		if(isTriclinic) {	
			double xlo = valuesX.get(0) - getMin(0.0, valuesX.get(2).doubleValue(), valuesY.get(2).doubleValue(), (valuesX.get(2) + valuesY.get(2)));
			double xhi = valuesX.get(1) - getMax(0.0, valuesX.get(2).doubleValue(), valuesY.get(2).doubleValue(), (valuesX.get(2) + valuesY.get(2)));									
			coords[0][0] = xhi - xlo;
			coords[0][1] = 0.0;
			coords[0][2] = 0.0;

			double ylo = valuesY.get(0) - getMin(0.0, valuesZ.get(2));
			double yhi = valuesY.get(1) - getMax(0.0, valuesZ.get(2));			

			coords[1][0] = valuesX.get(2);
			coords[1][1] = yhi - ylo;
			coords[1][2] = 0.0;

			coords[2][0] = valuesY.get(2);
			coords[2][1] = valuesZ.get(2);
			coords[2][2] = valuesZ.get(1) - valuesZ.get(0);
		}else {
			coords[0][0] = valuesX.get(1) - valuesX.get(0);
			coords[0][1] = 0.0;
			coords[0][2] = 0.0;

			coords[1][0] = 0.0;
			coords[1][1] = valuesY.get(1) - valuesY.get(0);
			coords[1][2] = 0.0;

			coords[2][0] = 0.0;
			coords[2][1] = 0.0;
			coords[2][2] = valuesZ.get(1) - valuesZ.get(0);						
		}

		lattice[0] = new Vector3d(coords[0][0],coords[0][1],coords[0][2]);
		lattice[1] = new Vector3d(coords[1][0],coords[1][1],coords[1][2]);
		lattice[2] = new Vector3d(coords[2][0],coords[2][1],coords[2][2]);					 
    	return lattice;    	
    }
    
    
    private Double getMin(Double... values) {
    	List<Double> list = Arrays.asList(values);
		return Collections.min(list);		
	}
    
    private Double getMax(Double... values) {
    	List<Double> list = Arrays.asList(values);
		return Collections.max(list);		
	}    
    

	private String readAtoms(BufferedReader br, String line, Molecule mol, HashMap <Integer, String> atomTypes, Vector3d[] lattice) throws IOException, Exception {
    	TreeMap<Integer, Atom> orderedAtoms = new TreeMap<>();
		List<String> fields = splitWordsFromText(line.trim().replaceAll("\\s*ITEM:\\s+ATOMS\\s*", ""));
		int id = fields.indexOf("id");
		int type = fields.indexOf("type");
		int x = getCoordinateField(fields, "x", "xu", "xsu");
		int y = getCoordinateField(fields, "y", "yu", "ysu");
		int z = getCoordinateField(fields, "z", "zu", "zsu");
		while((line = br.readLine()) != null && !line.trim().equals("") &&  !line.trim().startsWith("ITEM")) {  //File can store one or multiple geometries
			List<Double> values = parseDoublesFromText(line);
			Atom a = new Atom(atomTypes.get(values.get(type).intValue()), String.valueOf(values.get(id).intValue()));
			a.setXYZ3(new Point3d(values.get(x), values.get(y), values.get(z)));
			a.setFractionalXYZ3(cartesianToFractional(lattice, a.getXYZ3()));
			orderedAtoms.put(values.get(id).intValue(), a);			
		}
		for(int inx: orderedAtoms.keySet())
			mol.addAtom(orderedAtoms.get(inx));
		return line;
    }

    private int getCoordinateField(List<String> fields, String... headers) {
        for(String header : headers)
            if(fields.indexOf(header) != -1)
                return fields.indexOf(header);
        return -1;
    }

    private String generateCIFText(Molecule mol, Vector3d[] lattice) {    	
    	Double alpha = Math.round(57.2957795 * lattice[2].angle(lattice[1]) * 100.0) / 100.0;
		Double beta = Math.round(57.2957795 * lattice[2].angle(lattice[0]) * 100.0) / 100.0;
		Double gamma = Math.round(57.2957795 * lattice[0].angle(lattice[1]) * 100.0) / 100.0;    	

    	StringBuilder cif = new StringBuilder();    	
    	cif.append("# CIF file generated by ioChem-BD, see http://www.iochem-bd.org\n");
    	cif.append("data_I\n");
    	cif.append("_chemical_name_common '" + mol.getId() + "'\n");
    	cif.append("_cell_length_a " + lattice[0].length() + "\n");
    	cif.append("_cell_length_b " + lattice[1].length() + "\n");
    	cif.append("_cell_length_c " + lattice[2].length() + "\n");
    	cif.append("_cell_angle_alpha " + alpha + "\n");
    	cif.append("_cell_angle_beta " + beta + "\n");
    	cif.append("_cell_angle_gamma " + gamma + "\n");
    	cif.append("loop_\n");
    	cif.append("_atom_site_label\n");
    	cif.append("_atom_site_type_symbol\n");
    	cif.append("_atom_site_fract_x\n");
    	cif.append("_atom_site_fract_y\n");
    	cif.append("_atom_site_fract_z\n");
    	cif.append("_atom_site_occupancy\n");    	
    	for(Atom a : mol.getAtoms()) {    	
    		cif.append(String.format("%1$-8d%2$-5s%3$10.5f%4$10.5f%5$10.5f   1.000\n", a.getIndex(), a.getElementType(), a.getFractionaXYZ3().x, a.getFractionaXYZ3().y, a.getFractionaXYZ3().z));
    	}    
    	cif.append("\n");
    	return cif.toString();    	
    }	

	@Override
    public String getRegistryInputType() {
        return "lammps_trajectory";
    }

    @Override
    public String getRegistryOutputType() {
        return "lammps_trajectory_xml";
    }

    @Override
    public String getRegistryMessage() {
        return "Convert LAMMPS trajectory file to XML";
    }
    
    private List<Double> parseDoublesFromText(String line) {
    	List<Double> values = new ArrayList<>();
    	for(String value : line.split("\\s+"))
    		values.add(Double.parseDouble(value));
    	return values;
    }
    
    private List<String> splitWordsFromText(String line) {
    	List<String> values = new ArrayList<>();
    	Collections.addAll(values, line.trim().split("\\s+"));    	
    	return values;
    }  
       
    private Point3d cartesianToFractional(Vector3d[] lattice, Point3d coord){		
		Vector3d[] invaxis = calcInvertedAxes(lattice[0], lattice[1], lattice[2]);
	    Point3d frac = new Point3d();
	    frac.x = invaxis[0].x * coord.x + invaxis[0].y * coord.y + invaxis[0].z * coord.z;
	    frac.y = invaxis[1].x * coord.x + invaxis[1].y * coord.y + invaxis[1].z * coord.z;
	    frac.z = invaxis[2].x * coord.x + invaxis[2].y * coord.y + invaxis[2].z * coord.z;
	    return frac;
	} 
    
    private static Vector3d[] calcInvertedAxes(Vector3d aAxis, Vector3d bAxis, Vector3d cAxis) {
        double det = aAxis.x * bAxis.y * cAxis.z - aAxis.x * bAxis.z * cAxis.y - aAxis.y * bAxis.x * cAxis.z + aAxis.y * bAxis.z * cAxis.x + aAxis.z * bAxis.x * cAxis.y - aAxis.z * bAxis.y * cAxis.x;
        Vector3d[] invaxes = new Vector3d[3];
        invaxes[0] = new Vector3d();
        invaxes[0].x = (bAxis.y * cAxis.z - bAxis.z * cAxis.y) / det;
        invaxes[0].y = (bAxis.z * cAxis.x - bAxis.x * cAxis.z) / det;
        invaxes[0].z = (bAxis.x * cAxis.y - bAxis.y * cAxis.x) / det;

        invaxes[1] = new Vector3d();
        invaxes[1].x = (aAxis.z * cAxis.y - aAxis.y * cAxis.z) / det;
        invaxes[1].y = (aAxis.x * cAxis.z - aAxis.z * cAxis.x) / det;
        invaxes[1].z = (aAxis.y * cAxis.x - aAxis.x * cAxis.y) / det;

        invaxes[2] = new Vector3d();
        invaxes[2].x = (aAxis.y * bAxis.z - aAxis.z * bAxis.y) / det;
        invaxes[2].y = (aAxis.z * bAxis.x - aAxis.x * bAxis.z) / det;
        invaxes[2].z = (aAxis.x * bAxis.y - aAxis.y * bAxis.x) / det;
        return invaxes;
    }

    private void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
}
