/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.xmlcml.cml.converters.compchem.generic;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import jumbo2.converters.PlainTextToCompChemConverter;
import jumbo2.converters.PlainTextToXmlConverter;
import jumbo2.util.Utils;

import org.w3c.dom.Document;
import org.xmlcml.cml.converters.compchem.CompChemConverter;
import org.xmlcml.cml.converters.compchem.CompChemConverter.Type;

/**
 * This generic class is used to manage calculations types that are not coded as existing classes of jumbo-saxon project. 
 * Such new conversions will fall into this class to convert plain text output files to Compchem CML files.
 * 
 * The conversion from text to Compchem CML has two stages : text2xml and then xml2cml, we must code both conversion templates using jumbo-saxon convention. 
 * To use it, programmers must define a valid top conversion classpath that:
 * 		- Must start with this literal: org.xmlcml.cml.converters.compchem.
 * 		- Followed by the calculation software name in lowercase followed by a dot :  adf.
 * 		- Followed by the type of this file inside the calculation, for example log, outcar, input, and a dot:   log.
 * 		- And ending with this literal: templates/topTemplate.xml
 * So gathering all the previous the classpath for the templates of adf log file will be:
 * 		- org/xmlcml/cml/converters/compchem/adf/log/templates/topTemplate.xml
 * Now the programmer must build a template structure to conform this path (this will do the text2xml conversion), and an additional template structure 
 * to perform xml2cml conversion. Such structure will have the same classpath than befofe but the name of the file will be the calculation software name plus "2compchem.xml"
 * So previous example will have this other folder structure:
 *      - org/xmlcml/cml/converters/compchem/adf/log/templates/adf2compchem.xml
 *      
 * @author malvarez
 *
 */
public class GenericText2CompchemConverter extends CompChemConverter {
	
	private static final String FORMAT_TYPE_REGEX = "org\\.xmlcml\\.cml\\.converters\\.compchem\\.([\\w\\d]+)\\.([\\w\\d]+)\\..*";
	private String formatType = null;
	private String formatFile = null;
	private String SOURCETOXML_TEMPLATE_RESOURCE = null;
	private String XMLTOCOMPCHEM_TEMPLATE_RESOURCE = null;
	
	public GenericText2CompchemConverter (String classpath) throws Exception{
		Pattern p = Pattern.compile(FORMAT_TYPE_REGEX);
		Matcher m = p.matcher(classpath);
		if(m.matches()){
			formatType = m.group(1);
			formatFile = m.group(2);
			SOURCETOXML_TEMPLATE_RESOURCE = buildTopTemplateClassPath(classpath);						
			XMLTOCOMPCHEM_TEMPLATE_RESOURCE = buildToCompchemClassPath(classpath);
			
		}else{
			throw new Exception("Could not parse classpath from " + classpath);
		}
	}
	
	private String buildTopTemplateClassPath(String classpath){
		StringBuilder topTemplateClassPath = new StringBuilder();
		String parentPath = classpath.substring(0,classpath.lastIndexOf("."));
		topTemplateClassPath.append(parentPath.replace(".", "/"));
		topTemplateClassPath.append("/templates/");
		topTemplateClassPath.append("topTemplate.xml");
		return topTemplateClassPath.toString();				
	}
	
	private String buildToCompchemClassPath(String classpath){
		StringBuilder toCompchemClassPath = new StringBuilder();
		String parentPath = classpath.substring(0,classpath.lastIndexOf("."));
		toCompchemClassPath.append(parentPath.replace(".", "/"));
		toCompchemClassPath.append("/templates/");
		toCompchemClassPath.append(formatType);
		toCompchemClassPath.append("2compchem.xml");
		return toCompchemClassPath.toString();		
	}
	
	@Override
	public Type getInputType() {
		return Type.TEXT;
	}

	@Override
	public Type getOutputType() {
		return Type.CML;
	}

	@Override
	public void convert(String inputFilePath, String outputFilePath, String appendedXML) throws Exception {
		PlainTextToCompChemConverter txtToCompChem = new PlainTextToCompChemConverter(inputFilePath, SOURCETOXML_TEMPLATE_RESOURCE, XMLTOCOMPCHEM_TEMPLATE_RESOURCE, appendedXML);
		Document result = txtToCompChem.call();
		Utils.outputResultDocument(result, outputFilePath, false);
	}

	@Override
	public String getRegistryInputType() {
		return formatType + "_" + formatFile;		
	}

	@Override
	public String getRegistryOutputType() {
		return formatType + "_" + formatFile + "_compchem";
	}

	@Override
	public String getRegistryMessage() {
		return "Convert " + formatType + " " + formatFile + " to CML-compchem";
	}

}
