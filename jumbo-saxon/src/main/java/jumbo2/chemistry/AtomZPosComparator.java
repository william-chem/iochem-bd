/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
///////////////////////////////////////////////////////////////////////////////
//  Filename: $RCSfile: AtomZPosComparator.java,v $
//  Purpose:  Atom representation.
//  Language: Java
//  Compiler: JDK 1.4
//  Authors:  Joerg Kurt Wegner
//  Version:  $Revision: 1.6 $
//            $Date: 2005/02/17 16:48:36 $
//            $Author: wegner $
//  Original Author: ???, OpenEye Scientific Software
//  Original Version: babel 2.0a1
//
// Copyright OELIB:          OpenEye Scientific Software, Santa Fe,
//                           U.S.A., 1999,2000,2001
// Copyright JOELIB/JOELib2: Dept. Computer Architecture, University of
//                           Tuebingen, Germany, 2001,2002,2003,2004,2005
// Copyright JOELIB/JOELib2: ALTANA PHARMA AG, Konstanz, Germany,
//                           2003,2004,2005
//
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation version 2 of the License.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////////////
package jumbo2.chemistry;

import java.util.Comparator;

public class AtomZPosComparator implements Comparator
{
    public AtomZPosComparator(){
    }

    public int compare(Object o1, Object o2) throws ClassCastException
    {
        if ((o1 == null) && (o2 == null))
        {
            throw new IllegalArgumentException("Object to compare is 'null'");
        }

        if (!(o1 instanceof BasicAtomDouble) ||
                !(o2 instanceof BasicAtomDouble))
        {
            throw new ClassCastException("Objects must of type AtomZPos");
        }

        BasicAtomDouble a1 = (BasicAtomDouble) o1;
        BasicAtomDouble a2 = (BasicAtomDouble) o2;

        if (a1.doubleValue < a2.doubleValue)
        {
            return -1;
        }
        else if (a1.doubleValue > a2.doubleValue)
        {
            return 1;
        }

        return 0;
    }

    public boolean equals(Object obj){
        if (obj instanceof AtomZPosComparator)
            return true;        
        return false;
    }

    public int hashCode(){
        return 0;
    }
}