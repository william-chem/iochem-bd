/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import edu.wlu.cs.levy.CG.KDTree;
import edu.wlu.cs.levy.CG.KeyDuplicateException;
import edu.wlu.cs.levy.CG.KeySizeException;


public class ConnectionHelper{ 

	private static final Logger log = LogManager.getLogger(ConnectionHelper.class.getName());
	
    public static float BOND_TOLERANCE = 0.35f;		// >= 0.45f will generate too much extra bonds  
    public static float MIN_BOND_DISTANCE = 0.4f;
    public static float MIN_BOND_DISTANCE2 = MIN_BOND_DISTANCE * MIN_BOND_DISTANCE;
    
    public static void connectTheDots(Molecule mol){
    	Vector<String> connections = new Vector<String>();
    	Iterator<Atom> ait = mol.getAtomIterator();
    	
	    double d2;	    
	    double maxBondingRadius = Double.MIN_VALUE;
	    
	    KDTree<String> kd = new KDTree<String>(3);
	    HashMap<Integer, Atom> atomMap = new HashMap<Integer,Atom>();
	    HashMap<Integer, double[]> atomCoordMap = new HashMap<Integer,double[]>();            
	    while(ait.hasNext()){
	    	Atom a = (Atom)ait.next();
	    	atomMap.put(a.getIndex(),a);
	    	double coords[] = {a.getX3(), a.getY3(), a.getZ3()};
	    	atomCoordMap.put(a.getIndex(), coords);
	    	try {
				kd.insert(coords, String.valueOf(a.getIndex()));
			} catch (KeySizeException e) {
				e.printStackTrace();
			} catch (KeyDuplicateException e) {
				log.info(e.getMessage());
				//e.printStackTrace();
			}
			double fvalue = ChemicalElement.getCovalentRad(a.getAtomicNumber());
	        if(fvalue > maxBondingRadius)
	        	maxBondingRadius = fvalue;
	    }   
	    ait = mol.getAtomIterator();
	    ArrayList<Integer> firstList = new ArrayList<Integer>();
	    ArrayList<Integer> secondList = new ArrayList<Integer>();
	    ArrayList<Bond.BondType> orderList = new ArrayList<Bond.BondType>();
	    
	    while(ait.hasNext()){
	    	Atom a = (Atom)ait.next();
	    	double myBondingRadius = ChemicalElement.correctedBondRad(a.getAtomicNumber(), 3);
	    	double searchRadius = myBondingRadius + maxBondingRadius + BOND_TOLERANCE;
	    	double coords[] = {a.getX3(),a.getY3(), a.getZ3()};
	    	try {
				List<String> atomsNear = kd.nearestEuclidean(coords, searchRadius);
				for(String atomIndex : atomsNear){
					Atom b = atomMap.get(Integer.valueOf(atomIndex));
					log.debug(a.getId() + " is near " + b.getId());
					double bBondingRadius = ChemicalElement.correctedBondRad(b.getAtomicNumber(), 3);
					d2 = Math.pow(a.getX3() - b.getX3(),2) + Math.pow(a.getY3() - b.getY3(),2) + Math.pow(a.getZ3() - b.getZ3(),2);  
	                short order = getBondOrder((float)myBondingRadius,(float)bBondingRadius ,(float)d2, MIN_BOND_DISTANCE2,BOND_TOLERANCE);
	                if (order > 0) {
	                	if(connections.contains(a.getId() + "_" + b.getId()))
	                	    continue;	                    
	                    if (ChemicalElement.isHydrogen(a) && ChemicalElement.isHydrogen(b))
	                    	continue;	
	                    if(a.getId().equals(b.getId()))
	                    	continue;
	                    firstList.add(a.getIndex());
	                    secondList.add(b.getIndex());
	                    orderList.add(Bond.BondType.SINGLE);
	                    
	                    connections.add(a.getId() + "_" + b.getId());
	                    connections.add(b.getId() + "_" + a.getId());
	                    }                    						
				}
			} catch (KeySizeException e) {
				e.printStackTrace();
			}
	    }
	    Integer[] firstAtomIds = firstList.toArray(new Integer[firstList.size()]);
	    Integer[] secondAtomIds = secondList.toArray(new Integer[secondList.size()]);
	    Bond.BondType[] bondTypes = orderList.toArray(new Bond.BondType[orderList.size()]);
	    mol.addBondArray(firstAtomIds, secondAtomIds, bondTypes);	
    }
    
    private static short getBondOrder(float bondingRadiusA, float bondingRadiusB, float distance2, float minBondDistance2, float bondTolerance) {
    	if (bondingRadiusA == 0 || bondingRadiusB == 0 || distance2 < minBondDistance2)
    		return 0;
    	float maxAcceptable = bondingRadiusA + bondingRadiusB + bondTolerance;
    	float maxAcceptable2 = maxAcceptable * maxAcceptable;
    	return (distance2 > maxAcceptable2 ? (short) 0 : (short) 1);
    }
    
    
    
}
