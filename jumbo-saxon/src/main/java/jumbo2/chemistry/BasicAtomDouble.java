/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;


public class BasicAtomDouble implements java.io.Serializable
{
   
    private static final long serialVersionUID = 1L;
    public double doubleValue;
    public Atom atom;
   
    public BasicAtomDouble(Atom atom, double doubleValue){
        this.atom = atom;
        this.doubleValue = doubleValue;
    }

    public boolean equals(Object obj){
        boolean isEqual = false;
        BasicAtomDouble atomDouble = (BasicAtomDouble) obj;

        if (this.doubleValue == atomDouble.getDoubleValue()){
            isEqual = true;
        }

        return isEqual;
    }

    public double getDoubleValue(){
        return doubleValue;
    }

    public int hashCode(){
        long bits = Double.doubleToLongBits(doubleValue);
        int dh = (int) (bits ^ (bits >>> 32));

        return atom.hashCode() & dh;
    }

    public void setDoubleValue(double doubleValue){
        this.doubleValue = doubleValue;
    }
}
