/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

public class Vector3d {	
	 public double x = 0.0d;
	 public double y = 0.0d;
	 public double z = 0.0d;
	 
	 public Vector3d() {
		 this.x = 0.0d;
		 this.y = 0.0d;
		 this.z = 0.0d;
	 }
	 
	 public Vector3d(Double x, Double y, Double z) {
		 this.x = x;
		 this.y = y;
		 this.z = z;
	 }
	 
	 public Vector3d(Point3d p0) {
		 this.x = p0.x;
		 this.y = p0.y;
		 this.z = p0.z;
	 }

	 public Vector3d(Vector3d v1) {
		 this.x = v1.x;
		 this.y = v1.y;
		 this.z = v1.z;
	}

	public double length() {
		 return Math.sqrt( x * x + y * y + z * z );				
	 }
	 
	 public double dot(Vector3d v1) {
		 return (this.x*v1.x + this.y*v1.y + this.z*v1.z);
	 }
	
	 public double angle(Vector3d v1) {
		 double vDot = this.dot(v1) / ( this.length()*v1.length() );
		 if( vDot < -1.0) vDot = -1.0;
		 if( vDot >  1.0) vDot =  1.0;     
		 return((double) (Math.acos( vDot )));
	}

	public void sub(Point3d p1) {		
	    this.x -= p1.x;
	    this.y -= p1.y;
	    this.z -= p1.z;
	}

	public void cross(Vector3d v1, Vector3d v2) {
		double x,y, z;
		x = v1.y * v2.z - v1.z * v2.y;
		y = v2.x * v1.z - v2.z * v1.x;
		z = v1.x * v2.y - v1.y * v2.x;
		
		this.x = x;
		this.y = y;
		this.z = z;
	}

	public void normalize(Vector3d v1){
		double norm;
	    norm = 1.0 / Math.sqrt(v1.x * v1.x + v1.y * v1.y + v1.z * v1.z);
	    this.x = v1.x * norm;
	    this.y = v1.y * norm;
	    this.z = v1.z * norm;
	}

	public void normalize(){
        double norm;
        norm = 1.0 / Math.sqrt( x * x + y * y + z * z);
        this.x *= norm;
        this.y *= norm;
        this.z *= norm;
    }

	public final void scale(double value) {
        this.x *= value;
        this.y *= value;
        this.z *= value;
    }

	public final void add(Vector3d v1){ 
		this.x += v1.x;
	    this.y += v1.y;
	    this.z += v1.z;
	}

}
