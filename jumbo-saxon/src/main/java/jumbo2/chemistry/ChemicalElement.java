/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import java.util.HashMap;

public class ChemicalElement {

	private static String[] atomTypes = {"Xxx", "H","He","Li","Be","B","C","N","O","F","Ne","Na","Mg","Al","Si","P","S","Cl","Ar","K","Ca","Sc","Ti","V","Cr","Mn","Fe","Co","Ni","Cu","Zn","Ga","Ge","As","Se","Br","Kr","Rb","Sr","Y","Zr","Nb","Mo","Tc","Ru","Rh","Pd","Ag","Cd","In","Sn","Sb","Te","I","Xe","Cs","Ba","La","Ce","Pr","Nd","Pm","Sm","Eu","Gd","Tb","Dy","Ho","Er","Tm","Yb","Lu","Hf","Ta","W","Re","Os","Ir","Pt","Au","Hg","Tl","Pb","Bi","Po","At","Rn","Fr","Ra","Ac","Th","Pa","U","Np","Pu","Am","Cm","Bk","Cf","Es","Fm","Md","No","Lr","Rf","Db","Sg","Bh","Hs","Mt","Ds","Rg","Cn","Uut","Fl","Uup","Lv","Uus","Uuo"};
	private static double[] mass = {0.0, 1.00794, 4.002602, 6.941, 9.012182, 10.811, 12.0107, 14.0067, 15.9994, 18.9984032, 20.1797, 22.98977, 24.305, 26.981538, 28.0855, 30.973761, 32.065, 35.453, 39.948, 39.0983, 40.078, 44.95591, 47.867, 50.9415, 51.9961, 54.938049, 55.845, 58.9332, 58.6934, 63.546, 65.409, 69.723, 72.64, 74.9216, 78.96, 79.904, 83.798, 85.4678, 87.62, 88.90585, 91.224, 92.90638, 95.94, 98, 101.07, 102.9055, 106.42, 107.8682, 112.411, 114.818, 118.71, 121.76, 127.6, 126.90447, 131.293, 132.90545, 137.327, 138.9055, 140.116, 140.90765, 144.24, 145, 150.36, 151.964, 157.25, 158.92534, 162.5, 164.93032, 167.259, 168.93421, 173.04, 174.967, 178.49, 180.9479, 183.84, 186.207, 190.23, 192.217, 195.078, 196.96655, 200.59, 204.3833, 207.2, 208.98038, 209, 210, 222, 223, 226, 227, 232.0381, 231.03588, 238.02891, 237, 244, 243, 247, 247, 251, 252, 257, 258, 259, 262, 261, 262, 266, 264, 269, 268, 271, 272, 285, 284, 289, 288, 292};
	private static double[] rbo = {0, 0.33, 0.7, 1.23, 0.9, 0.82, 0.77, 0.7, 0.66, 0.611, 0.7, 1.54, 1.36, 1.18, 0.937, 0.89, 1.04, 0.997, 1.74, 2.03, 1.74, 1.44, 1.32, 1.22, 1.18, 1.17, 1.17, 1.16, 1.15, 1.17, 1.25, 1.26, 1.188, 1.2, 1.17, 1.167, 1.91, 2.16, 1.91, 1.62, 1.45, 1.34, 1.3, 1.27, 1.25, 1.25, 1.28, 1.34, 1.48, 1.44, 1.385, 1.4, 1.378, 1.387, 1.98, 2.35, 1.98, 1.69, 1.83, 1.82, 1.81, 1.8, 1.8, 1.99, 1.79, 1.76, 1.75, 1.74, 1.73, 1.72, 1.94, 1.72, 1.44, 1.34, 1.3, 1.28, 1.26, 1.27, 1.3, 1.34, 1.49, 1.48, 1.48, 1.45, 1.46, 1.45, 2.4, 2, 1.9, 1.88, 1.79, 1.61, 1.58, 1.55, 1.53, 1.07, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
	private static double[] rCov = {0, 0.23, 0.93, 0.68, 0.35, 0.83, 0.68, 0.68, 0.68, 0.64, 0.7, 0.97, 1.1, 1.35, 1.2, 0.75, 1.02, 0.99, 0.7, 1.33, 0.99, 1.44, 1.47, 1.33, 1.35, 1.35, 1.34, 1.33, 1.5, 1.52, 1.45, 1.22, 1.17, 1.21, 1.22, 1.21, 1.91, 1.47, 1.12, 1.78, 1.56, 1.48, 1.47, 1.35, 1.4, 1.45, 1.5, 1.59, 1.69, 1.63, 1.46, 1.46, 1.47, 1.4, 1.98, 1.67, 1.34, 1.87, 1.83, 1.82, 1.81, 1.8, 1.8, 1.99, 1.79, 1.76, 1.75, 1.74, 1.73, 1.72, 1.94, 1.72, 1.57, 1.43, 1.37, 1.35, 1.37, 1.32, 1.5, 1.5, 1.7, 1.55, 1.54, 1.54, 1.68, 0.7, 2.4, 2, 1.9, 1.88, 1.79, 1.61, 1.58, 1.55, 1.53, 1.51, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    private static HashMap<String, Integer> atomTypePerIndex = null;
	
	private String element = null;
	private int atomicNumber = -1;
	
	static{
		atomTypePerIndex = new HashMap<String,Integer>();
		for(int inx = 1; inx < atomTypes.length; inx++)
			atomTypePerIndex.put(atomTypes[inx], inx);		
	}
	
	public ChemicalElement(int index) throws Exception{
		if(index > atomTypes.length || index < 1)
			throw new Exception("Element index out of bounds " + index);
		this.element = atomTypes[index];
		this.atomicNumber = index;
	}
		
	public static ChemicalElement getElement(int i) throws Exception {
		return new ChemicalElement(i);
	}

	public static ChemicalElement getChemicalElement(String elem) throws Exception {		
		if(atomTypePerIndex.containsKey(elem))
			return new ChemicalElement(atomTypePerIndex.get(elem));
		else if(elem.matches("[0-9]+")) {
			int index = Integer.parseInt(elem);
			if(index > atomTypePerIndex.size())
				throw new Exception("Element index out of range " + elem);
			else
				return new ChemicalElement(atomTypePerIndex.get(atomTypes[index]));
		}
		else
			throw new Exception("Element string doesn't exist " + elem);		
	}
	
	public String getSymbol() {
		return element;
	}
	
	public int getAtomicNumber() {
		return atomicNumber;
	}
	
	public double getMass(){
		return mass[this.atomicNumber];
	}

	public static double correctedBondRad(int atomicnum, int hyb) {	
        double rad;

        if ((atomicnum < 0) || (atomicnum > atomTypes.length))
            return 1.0f;	        

        rad = getRadiusBondOrder(atomicnum);

        if (hyb == 2)
            rad *= 0.95f;
        
        else if (hyb == 1)
            rad *= 0.90f;
        return rad;	  
	}

	private static double getRadiusBondOrder(int atomicnum) {
		return rbo[atomicnum];	
	}

	public static double getCovalentRad(int atomicnum) {		
	    if ((atomicnum < 0) || (atomicnum > atomTypes.length))
	        return 0.0f;
	    return getRadiusCovalent(atomicnum);
	}

	private static double getRadiusCovalent(int atomicnum) {
		return rCov[atomicnum];
	}
	

	public static boolean isHydrogen(Atom a) {
		return a.getElementType().toUpperCase().equals("H");
	}

}
