/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.chemistry;

import jumbo2.util.Constants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

public class Angle {
	Node originalNode;
	String atomRefs[] = {null, null, null};
	double value;

	public Angle(Node line) {
		Element element = (Element)line;
		atomRefs = element.getAttribute("atomRefs3").trim().split(Constants.WHITESPACES_REGEX);
		value = Double.parseDouble(element.getTextContent());
		originalNode = line;
	}
	
	public String[] getAtomRefs3(){
		return atomRefs;
	}
	
	public double getValue(){
		return value;
	}
	
	public Node buildNode(Document document){
		return originalNode;		
	}		

}
