/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.util;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;


public class XMLFileManager {
	
	private static Logger logger = LogManager.getLogger(XMLFileManager.class.getName());
	private static XPathFactory xFactory = new net.sf.saxon.xpath.XPathFactoryImpl();
	private DocumentBuilderFactory dbFactory = null; 
	private NamespaceContext namespaceContext = null;
	private Document document = null;
	
	
	public XMLFileManager(String defaultNamespace, String[][] otherNamespaces){
		dbFactory = DocumentBuilderFactory.newInstance();		
		dbFactory.setXIncludeAware(true);
		dbFactory.setIgnoringComments(true);						
		if(otherNamespaces == null){
			namespaceContext = null;
			dbFactory.setNamespaceAware(false);
		}else{
			HashMap<String, String> namespaces = new HashMap<String,String>();
			if(defaultNamespace != null)
				namespaces.put("DEFAULT", defaultNamespace);
			if(otherNamespaces != null)
				for(String[] namespace : otherNamespaces)
					namespaces.put(namespace[0], namespace[1]);	
			namespaceContext = new NamespaceContextMap(namespaces);
			dbFactory.setNamespaceAware(false);
		}
	}
	
	public XMLFileManager(String defaultNamespace, String[][] otherNamespaces, String content) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException  {
		this(defaultNamespace, otherNamespaces);		
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();		
		document = dBuilder.parse(new ByteArrayInputStream(content.getBytes("UTF-8")));	
	}	
	
	public XMLFileManager(String defaultNamespace, String[][] otherNamespaces, File file) throws ParserConfigurationException, FileNotFoundException, SAXException, IOException  {
		this(defaultNamespace, otherNamespaces);
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();		
		document = dBuilder.parse(new FileInputStream(file));
	}
	
	public String getStringFromQuery(String xpathQuery) {
		XPath xpath = xFactory.newXPath();
		if(namespaceContext != null)
			xpath.setNamespaceContext(namespaceContext);		
		try{
			XPathExpression expr = xpath.compile(xpathQuery);
			String result = (String)expr.evaluate(document, XPathConstants.STRING);
			return result;
		}catch(Exception e){
			logger.error(e.getMessage());
			return null;	
		}		
	}	
	
	public String getSingleAttributeValueQuery(String xpathQuery) {
		XPath xpath = xFactory.newXPath();
		if(namespaceContext != null)
			xpath.setNamespaceContext(namespaceContext);		
		try{
			XPathExpression expr = xpath.compile(xpathQuery);
			Attr result = (Attr)expr.evaluate(document, XPathConstants.NODE);
			return result.getTextContent();
		}catch(Exception e){
			logger.error(e.getMessage());
			return null;	
		}		
	}
	
	public NodeList getItemIteratorQuery(String xpathQuery) {
		XPath xpath = xFactory.newXPath();
		if(namespaceContext != null)
			xpath.setNamespaceContext(namespaceContext);		
		try{
			XPathExpression expr = xpath.compile(xpathQuery);
			NodeList result = (NodeList)expr.evaluate(document.getFirstChild(), XPathConstants.NODESET);
			return result;
		}catch(Exception e){
			logger.error(e.getMessage());
			return null;	
		}		
	}
	
	public Document getDocument() {
		return document;
	}
	
	
}
