/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.LinkedList;
import java.util.UUID;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import jumbo2.navigator.text.filecontentmanager.helper.Section;

import org.w3c.dom.Document;
import org.w3c.dom.Node;

public class Utils {
	
	public static Integer uuid = 0;
	
	public static String buildRandomUUID(){		
		return UUID.randomUUID().toString();						
	}

	public static void outputResultDocument(Document resultDocument,String outputFile, boolean printXML){
		if(printXML){
			try {
				StringWriter sw = new StringWriter();
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer transformer = tf.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");	
				transformer.transform(new DOMSource(resultDocument), new StreamResult(sw));
				System.out.println(sw.toString());;						
			} catch (Exception ex) {
			    throw new RuntimeException("Error converting to String", ex);
			}						
		}
		
		try {
			StreamResult result = new StreamResult(new File(outputFile));
		    // Use a Transformer for output
		    TransformerFactory tFactory = TransformerFactory.newInstance();
		    Transformer transformer;
			transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    DOMSource source = new DOMSource(resultDocument);		  
		    transformer.transform(source, result);	
		} catch (TransformerConfigurationException e) {		
			e.printStackTrace();
		} catch (TransformerException e) {	
			e.printStackTrace();
		}		
	}

	public static void outputNode(Node node,String outputFile, boolean printXML){
		if(printXML){
			try {
				StringWriter sw = new StringWriter();
				TransformerFactory tf = TransformerFactory.newInstance();
				Transformer transformer = tf.newTransformer();
				transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
				transformer.setOutputProperty(OutputKeys.METHOD, "xml");
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");
				transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");	
				transformer.transform(new DOMSource(node), new StreamResult(sw));
				System.out.println(sw.toString());;						
			} catch (Exception ex) {
			    throw new RuntimeException("Error converting to String", ex);
			}						
		}
		
		try {
			StreamResult result = new StreamResult(new File(outputFile));
		    // Use a Transformer for output
		    TransformerFactory tFactory = TransformerFactory.newInstance();
		    Transformer transformer;
			transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    DOMSource source = new DOMSource(node);		  
		    transformer.transform(source, result);	
		} catch (TransformerConfigurationException e) {		
			e.printStackTrace();
		} catch (TransformerException e) {	
			e.printStackTrace();
		}		
	}	

	public static String documentToString(Document resultDocument){
		try {
			StringWriter sw = new StringWriter();
			TransformerFactory tf = TransformerFactory.newInstance();
			Transformer transformer = tf.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");	
			transformer.transform(new DOMSource(resultDocument), new StreamResult(sw));
			return sw.toString();					
		} catch (Exception ex) {
		    throw new RuntimeException("Error converting to String", ex);
		}		
	}
	
	public static void printSectionsToFile(String filePath, LinkedList<Section> sections){
		StringBuilder sb = new StringBuilder();
		for(Section section : sections){
			sb.append("SECTION\n");
			for(String line : section.getLines())
				sb.append(line + "\n");
			sb.append("\n");
		}		
		
		File file = new File(filePath);
		FileOutputStream fop;
		try {
			fop = new FileOutputStream(file);
			if (!file.exists()) {
				file.createNewFile();
			}		
			byte[] contentInBytes = sb.toString().getBytes();
			fop.write(contentInBytes);
			fop.flush();
			fop.close();
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		
	}
	
	public static String buildFullUri(String uri) throws MalformedURLException, IOException {
		String fullUri = null;
		String baseURL = System.getProperty("JUMBO_TEMPLATE_BASE_URL");		
		fullUri = buildHttpUrl(baseURL, uri);
		if(fullUri == null)
				fullUri = getUrlForClasspathResoruce(uri);		
	  	return fullUri;
	}
	
	private static String buildHttpUrl(String baseURL, String uri) {
		InputStream in = null;
		if(baseURL == null)
			return null;		
		StringBuilder sb = new StringBuilder();
		sb.append(baseURL);
		if(!baseURL.trim().endsWith("/") && !uri.trim().startsWith("/"))
			sb.append("/");
		sb.append(uri);				
		try {
			in = new URL(sb.toString()).openStream();
			if (in == null)
		        return null;
			in.close();
		} catch (Exception e) {
			return null;
		}
	  	return sb.toString();
		
	}
	
	private static String getUrlForClasspathResoruce(String uri) throws IOException{
		InputStream in = null;
		StringBuilder sb = new StringBuilder();
		sb.append("classpath:");
		if(uri.trim().startsWith("/"))
			sb.append(uri.substring(1));
		else
			sb.append(uri);
		try {
			in = new URL(sb.toString()).openStream();
			if (in == null)
		        return null;
			in.close();
		} catch (Exception e) {
			return null;
		}		
		return sb.toString();
	}
	
}
