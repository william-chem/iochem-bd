/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.text.filecontentmanager.helper;

public class LineProperties {
	private String parentUUID; 
	private boolean processed;
	
	public LineProperties(String parentUUID){
		this.parentUUID = parentUUID;
		processed = false;
	}
	
	public boolean isProcessed(){
		return processed;
	}
	
	public String getParentUUID(){
		return parentUUID;
	}
	
	public void setProceesed(boolean processed){
		this.processed = processed;	
	}
	
	public void setParentUUID(String parentUUID){
		this.parentUUID = parentUUID;
	}
	
}
