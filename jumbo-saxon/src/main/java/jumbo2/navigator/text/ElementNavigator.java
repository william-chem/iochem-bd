/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.text;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.Callable;

import javax.xml.namespace.NamespaceContext;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import jumbo2.matchers.RecordMatcher;
import jumbo2.matchers.TemplateMatcher;
import jumbo2.matchers.TransformMatcher;
import jumbo2.navigator.text.filecontentmanager.FileContentManager;
import jumbo2.navigator.text.filecontentmanager.helper.LineRange;
import jumbo2.navigator.text.filecontentmanager.helper.Section;
import jumbo2.util.Constants;
import jumbo2.util.Utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class ElementNavigator implements Runnable {
	private static final Logger logger = LogManager.getLogger(ElementNavigator.class.getName());
	
	private NamespaceContext namespaceContext = null;
	private FileContentManager fileManager = null;
	private Node templateElement;
	private Node resultElement;
	private String parendUUID;
	private String indent;
	
	public ElementNavigator(NamespaceContext namespaceContext, FileContentManager fileManager, Node templateElement, Node resultElement, String parentUUID, String indent){
		this.namespaceContext = namespaceContext;
		this.fileManager = fileManager;
		this.templateElement = templateElement;
		this.resultElement = resultElement;
		this.parendUUID = parentUUID;
		this.indent = indent;
	}
	
	
	@Override
	public void run(){
		try {
			navigate(templateElement, resultElement, parendUUID, indent);	//Exceptions raised while parsing a node will be logged and then ommitted, this is an error prone mechanism (user defined template parsing) 
		} catch (DOMException e) {
			logger.error("Exception raised while parsing file");
		} catch (Exception e) {
			logger.error("Exception raised while parsing file");
		}
	}
	
	private void navigate(Node currentNode, Node result, String parentUUID,  String indent) throws DOMException, Exception{
		logger.debug(indent + "navigate to " + parentUUID);
		NodeList children = currentNode.getChildNodes();
		for(int i = 0; i < children.getLength(); i++){
			Node child = children.item(i);
			if(!child.getNodeName().equals("comment") && !child.getNodeName().equals("#text")){ //Discard text nodes and comments
				if(child.getNodeName().equals("template"))				
					processTemplate(child, result, parentUUID, indent + "\t");
				else if(child.getNodeName().equals("templateList")){
					processTemplateList(child, result, parentUUID, indent);					
				}else if(child.getNodeName().equals("record")){
					processRecord(child, result, parentUUID, indent + "  ");
				}else if(child.getNodeName().equals("transform")){					processTransform(child, result, parentUUID, indent);
				}else if(child.getNodeName().equals("transformList")){
					processTransformList(child, result, parentUUID, indent);
				}else{
					logger.error(indent + "<" + child.getNodeName() + ">" );
				}				
			}	
		}		
	}
	
	private void processTemplate(Node templateNode, Node resultNode, String parentUUID, String indent) throws DOMException, Exception{				
		TemplateMatcher templateMatcher = new TemplateMatcher();		
		NamedNodeMap attributes = templateNode.getAttributes();
		logger.debug(indent + "<template>" );
		for(int i = 0; i < attributes.getLength(); i++)
			templateMatcher.processAttribute(attributes.item(i), indent);		
		ArrayList<LineRange> matchingBlockEnds = templateMatcher.getMatches(fileManager, parentUUID, indent);		
		for(LineRange matchingBlockEnd : matchingBlockEnds){
			String nodeUUID = Utils.buildRandomUUID();
			logger.debug(indent + "  Matching textblock size :" + matchingBlockEnd.size() + " lines, start : " + matchingBlockEnd.getStart() + " end : " + matchingBlockEnd.getEnd()  +" UUID=" + nodeUUID);			
			Element node = (Element)templateMatcher.buildNode(resultNode.getOwnerDocument(), matchingBlockEnd);
			
			if(!node.hasAttribute("startLine"))		//append last
				resultNode.appendChild(node);
			else{
				int startLine = Integer.valueOf(node.getAttribute("startLine"));	//append ordered by it's startLine attribute 
				NodeList childNodes = resultNode.getChildNodes();
				boolean inserted = false;
				for(int inx = 0 ; inx < childNodes.getLength(); inx++){
					Element childNode = (Element)childNodes.item(inx);
					if(!childNode.hasAttribute("startLine"))
						continue;
					else{
						int startLineChild = Integer.valueOf(childNode.getAttribute("startLine"));
						if( startLine < startLineChild){
							resultNode.insertBefore(node, childNode);
							inserted = true;
							break;
						}
					}									
				}
				if(!inserted)
					resultNode.appendChild(node);
			}
			
			fileManager.assignLinesToNewSection(matchingBlockEnd,nodeUUID, indent);
			ElementNavigator elementNavigator = new ElementNavigator(namespaceContext, fileManager, templateNode, node, nodeUUID, indent + "\t");
			elementNavigator.run();
		}
		logger.debug(indent + "</template>" );
	}
	
	private void processTemplateList(Node templateNode, Node resultNode, String parentUUID, String indent) throws DOMException, Exception{
		logger.debug(indent + "<templateList>" );
		navigate(templateNode, resultNode, parentUUID, indent + "\t");		//TemplateList is only a template container, we'll pass through it
		logger.debug(indent + "</templateList>" );
	}
			
	private void processRecord(Node recordNode, Node resultNode, String parentUUID, String indent) throws DOMException, Exception{
		RecordMatcher recordMatcher = new RecordMatcher();
		NamedNodeMap attributes = recordNode.getAttributes();		
		logger.debug(indent + "<record> Parent UUID=" + parentUUID);
		for(int i = 0; i < attributes.getLength(); i++)
			recordMatcher.processAttribute(attributes.item(i), indent + " ");
		recordMatcher.processInnerText(((Element)recordNode).getTextContent(), indent + " ");
		recordMatcher.readLines(fileManager, resultNode, parentUUID, indent + "   ");
		logger.debug(indent + "</record>" );
		
	}
	
	private void processTransform(Node transformNode, Node resultNode, String parentUUID, String indent) throws Exception{	
		TransformMatcher transformMatcher = TransformMatcher.newTransformMatcher(namespaceContext, transformNode.getAttributes().getNamedItem("process").getNodeValue());
		NamedNodeMap attributes = transformNode.getAttributes();
		logger.debug(indent + "<transform> Parent UUID=" + parentUUID);
		for(int i = 0; i < attributes.getLength(); i++)
			transformMatcher.processAttribute(attributes.item(i), indent + "  ");		
		transformMatcher.transform(resultNode, indent);
		logger.debug(indent + "</transform>");
		
	}
	
	private void processTransformList(Node transformListNode, Node resultNode, String parentUUID, String indent) throws DOMException, Exception{
		logger.debug(indent + "<transformList>" );
		navigate(transformListNode, resultNode, parentUUID, indent + "\t");		//TransformList is only a transform container, we'll pass through it
		logger.debug(indent + "</transformList>"); 
	}
	

	public void saveUnprocessedSectionsToFile(String filePath) throws IOException{
		Utils.printSectionsToFile(filePath, fileManager.getAllUnProcessedSections(true));
	}	
	
	public void removeStartEndLineAttributes() throws XPathExpressionException{
		XPathFactory factory = new net.sf.saxon.xpath.XPathFactoryImpl();
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile("//cml:module");
		NodeList nodes = (NodeList) expr.evaluate(resultElement, XPathConstants.NODESET);
		for(int inx = 0; inx < nodes.getLength(); inx++){
			Element element = (Element)nodes.item(inx);
			element.removeAttribute("startLine");
			element.removeAttribute("endLine");
			element.removeAttributeNS(Constants.cmlxPrefixNamespace[1], "lineCount");
		}
		expr = xpath.compile("//cml:list[@cmlx:lineCount]");
		nodes = (NodeList) expr.evaluate(resultElement, XPathConstants.NODESET);
		for(int inx = 0; inx < nodes.getLength(); inx++){
			Element element = (Element)nodes.item(inx);
			element.removeAttributeNS(Constants.cmlxPrefixNamespace[1], "lineCount");
		}
	}
	
	public void appendTextAsXML(String append) throws ParserConfigurationException, SAXException, IOException{		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		dbf.setNamespaceAware(true);
		DocumentBuilder db = dbf.newDocumentBuilder();
		append = wrapAppendedText(append);		
		Document doc = db.parse(new InputSource(new StringReader(append)));
		Node appendNode = doc.getDocumentElement();
		Node importedNode = resultElement.getOwnerDocument().importNode(appendNode, true);			
		resultElement.appendChild(importedNode);		
	}
	
	private String wrapAppendedText(String append){ 		
		StringBuilder sb = new StringBuilder();
		sb.append("<module id='external' xmlns='http://www.xml-cml.org/schema'>");
		sb.append(append);
		sb.append("</module>");
		return sb.toString();
	}
	
}
