/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator.xml;

import javax.xml.namespace.NamespaceContext;

import jumbo2.matchers.TransformMatcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class ElementNavigator implements Runnable {
	private static final Logger logger = LogManager.getLogger(ElementNavigator.class.getName());
	
	private NamespaceContext namespaceContext = null;
	private Node templateElement;
	private Document inputDocument;
	
	public ElementNavigator(NamespaceContext namespaceContext, Node templateElement, Document inputDocument ){
		this.namespaceContext = namespaceContext;
		this.inputDocument = inputDocument;
		this.templateElement = templateElement;
	}
		
	@Override
	public void run() {
		try {
			navigate(templateElement, inputDocument.getFirstChild(), "");
		} catch (Exception e) {
			e.printStackTrace();
		}		
	}
	
	private void navigate(Node currentNode, Node result, String indent) throws DOMException, Exception{
		NodeList children = currentNode.getChildNodes();
		for(int i = 0; i < children.getLength(); i++){
			Node child = children.item(i);
			if(!child.getNodeName().equals("comment") && !child.getNodeName().equals("#text")){ //Discard text nodes and comments
				if(child.getNodeName().equals("template") || child.getNodeName().equals("templateList") || child.getNodeName().equals("record"))				
					throw new Exception("Found template/templateList/record element on transform only templates");
				else if(child.getNodeName().equals("transform"))
					processTransform(child, result, indent);
				else if(child.getNodeName().equals("transformList"))
					processTransformList(child, result, indent);
				else
					logger.error(indent + "<" + child.getNodeName() + ">" );								
			}	
		}		
	}
	
	private void processTransform(Node transformNode, Node resultNode, String indent) throws Exception{	
		TransformMatcher transformMatcher = TransformMatcher.newTransformMatcher(namespaceContext, transformNode.getAttributes().getNamedItem("process").getNodeValue());
		NamedNodeMap attributes = transformNode.getAttributes();
		logger.debug(indent + "<transform>");
		for(int i = 0; i < attributes.getLength(); i++)
			transformMatcher.processAttribute(attributes.item(i), indent + "  ");		
		transformMatcher.transform(resultNode, indent);
		logger.debug(indent + "</transform>");
		
	}
	
	private void processTransformList(Node transformListNode, Node resultNode, String indent) throws DOMException, Exception{
		logger.debug(indent + "<transformList>" );
		navigate(transformListNode, resultNode, indent + "\t");		//TransformList is only a transform container, we'll pass through it
		logger.debug(indent + "</transformList>"); 
	}
	
}
