/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.navigator;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.concurrent.Callable;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import jumbo2.navigator.text.filecontentmanager.helper.Section;
import jumbo2.util.Constants;
import jumbo2.util.UniversalNamespaceCache;
import jumbo2.util.Utils;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

public abstract class DocumentNavigator implements Callable<Document> {
	
	protected DocumentBuilderFactory dbFactory = null;
	protected Document templateDocument = null;	
	protected LinkedHashMap<String, String> namespaces =  null;
	protected Document resultDocument = null;
	protected UniversalNamespaceCache namespaceContext = null;
	
	public DocumentNavigator(String topTemplatePath) throws ParserConfigurationException, SAXException, IOException{
		dbFactory = createDocumentFactory();
		templateDocument = readCaptureTemplate(topTemplatePath);
		namespaces = readXmlNamespaces(templateDocument);
		resultDocument = dbFactory.newDocumentBuilder().newDocument();
    	Node rootNode = buildRootNode(templateDocument, namespaces);
		resultDocument.appendChild(rootNode);		
		namespaceContext = new UniversalNamespaceCache(resultDocument, true);
	}

	abstract public Document call() throws Exception;
	
	public Document getResultDocument(){
		return resultDocument;
	}
	
	protected DocumentBuilderFactory createDocumentFactory(){
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		dbFactory.setXIncludeAware(true);
		dbFactory.setNamespaceAware(true);
		dbFactory.setIgnoringComments(true);
		return dbFactory;
	}
	
	private Document readCaptureTemplate(String topTemplatePath) throws ParserConfigurationException, SAXException, IOException{
		Document templateDocument = null;
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		templateDocument = dBuilder.parse(Utils.buildFullUri(topTemplatePath));
		return templateDocument;
	}
	
	protected LinkedHashMap<String,String> readXmlNamespaces(Document topTemplateDocument){
		LinkedHashMap<String, String> namespaces = new LinkedHashMap<String,String>();
		Element element = (Element)topTemplateDocument.getFirstChild();
		NamedNodeMap attributes = element.getAttributes();
		for(int i = 0; i < attributes.getLength(); i++)
			if(attributes.item(i).getNodeName().startsWith("xmlns"))
				namespaces.put(attributes.item(i).getNodeName(), attributes.item(i).getNodeValue());
		return namespaces;
	}
	

	
	
	private Node buildRootNode(Document document, LinkedHashMap<String,String> namespaces){
		Node topTemplateNode = document.getFirstChild();
		Element parentNode = resultDocument.createElementNS(Constants.cmlNamespace, "module");
		parentNode.setAttributeNS("http://www.w3.org/2000/xmlns/", "xmlns", Constants.cmlNamespace);		
		for(String[] namespace : Constants.namespaces)
			parentNode.setAttributeNS("http://www.w3.org/2000/xmlns/", namespace[0], namespace[1]);			
		parentNode.setAttributeNS(Constants.cmlNamespace, "id", ((Element)topTemplateNode).getAttribute("id"));
		parentNode.setAttributeNS(Constants.cmlNamespace, "startLine", "0");
		for(String namespace:namespaces.keySet())
			parentNode.setAttributeNS("http://www.w3.org/2000/xmlns/", namespace, namespaces.get(namespace));
		return parentNode;		
	}
	
	/**
	 * Debug function, only used while testing 
	 * @param sectionToPrint
	 * @param filePath
	 */
	public void saveSectionToFile(Section sectionToPrint, String filePath){		
		try {
			PrintWriter out = new PrintWriter(filePath);
			for(int inx = 0; inx < sectionToPrint.getLines().size(); inx++)
				out.println(inx + "\t\t" + sectionToPrint.getLines().get(inx));
			out.close();
		} catch (Exception ex) {
			throw new RuntimeException("Error converting to String", ex);
		}			
	}
	/**
	 * Debug function, only used while testing 
	 * @param sectionToPrint
	 * @param filePath
	 */	
	public void saveMultipleSectionsToFile(LinkedList<Section> sections, String filePath){
		try {
			PrintWriter out = new PrintWriter(filePath);
			for(Section sectionToPrint : sections){
				out.println("\n\n\nSECTION\n");
				for(int inx = 0; inx < sectionToPrint.getLines().size(); inx++)
					out.println(inx + "\t\t" + sectionToPrint.getLines().get(inx));
			}
			out.close();
		} catch (Exception ex) {
			throw new RuntimeException("Error converting to String", ex);
		}					
	}

}
