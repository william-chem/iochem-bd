package jumbo2.navigator.amber.nc;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import jumbo2.navigator.DocumentNavigator;
import jumbo2.util.Constants;
import ucar.ma2.ArrayDouble;
import ucar.nc2.NetcdfFile;
import ucar.nc2.NetcdfFiles;

public class NetCDFDocumentNavigator extends DocumentNavigator {

    private final String UNITS_ANGSTROM = "nonsi:angstrom";
    private final String UNITS_DEGREE = "nonsi:degree";
    
    String inputFilePath;
    
    public NetCDFDocumentNavigator(String inputFilePath, String topTemplatePath) throws ParserConfigurationException, SAXException, IOException {
        super(topTemplatePath);
        this.inputFilePath = inputFilePath;
    }

    @Override
    public Document call() throws Exception {
        List<Double> x3 = new ArrayList<>();
        List<Double> y3 = new ArrayList<>();
        List<Double> z3 = new ArrayList<>();        
        
        List<Double> cellLengths= new ArrayList<>();
        List<Double> cellAngles= new ArrayList<>();
        
        try (NetcdfFile ncfile = NetcdfFiles.open(inputFilePath)) {
            Document document = this.getResultDocument();       
            Element element = document.createElementNS(Constants.cmlNamespace, "module");       
            element.setAttribute("id", "trajectory");                                
            
            ArrayDouble array = (ArrayDouble) ncfile.findVariable("coordinates").read();
            for(int inx = 0; inx < (array.getSize() / 3); inx++) {            
                x3.add(array.getDouble(inx * 3));
                y3.add(array.getDouble(inx * 3 + 1));
                z3.add(array.getDouble(inx * 3 + 2));
            }
            element.appendChild(createArray(document, "xsd:double", "cc:x3", x3, " "));
            element.appendChild(createArray(document, "xsd:double", "cc:y3", y3, " "));
            element.appendChild(createArray(document, "xsd:double", "cc:z3", z3, " "));
            document.getFirstChild().appendChild(element);

            // when cell is defined, will read its angles and lengths
            if(ncfile.findVariable("cell_lengths") != null && ncfile.findVariable("cell_angles") != null) {
                array = (ArrayDouble) ncfile.findVariable("cell_lengths").read();               
                for(int inx = 0; inx < (array.getSize()); inx++) {
                    cellLengths.add(array.getDouble(inx));
                }
                array = (ArrayDouble) ncfile.findVariable("cell_angles").read();
                if(array != null) {
                    for(int inx = 0; inx < (array.getSize()); inx++) {
                        cellAngles.add(array.getDouble(inx));   
                    }
                 
                }    
                document.getFirstChild().appendChild(createLatticeVectors(document, cellLengths, cellAngles));
                document.getFirstChild().appendChild(createCrystal(document, cellLengths, cellAngles));
            }
            return document;
        }
    }
    
    private Node createCrystal(Document document, List<Double> cellLengths, List<Double> cellAngles) {
        Element crystal = (Element)document.createElementNS(Constants.cmlNamespace, "crystal");        
        crystal.appendChild(createScalar(document, "scalar", UNITS_ANGSTROM, "a", "sc1", cellLengths.get(0).toString()));
        crystal.appendChild(createScalar(document, "scalar", UNITS_ANGSTROM, "b", "sc2", cellLengths.get(1).toString()));
        crystal.appendChild(createScalar(document, "scalar", UNITS_ANGSTROM, "c", "sc3", cellLengths.get(2).toString()));
        crystal.appendChild(createScalar(document, "scalar", UNITS_DEGREE, "alpha", "sc4", cellAngles.get(0).toString()));
        crystal.appendChild(createScalar(document, "scalar", UNITS_DEGREE, "beta", "sc5", cellAngles.get(1).toString()));
        crystal.appendChild(createScalar(document, "scalar", UNITS_DEGREE, "gamma", "sc6", cellAngles.get(2).toString()));
        return crystal;
    }

    private Node createLatticeVectors(Document document, List<Double> cellLengths, List<Double> cellAngles) {    
        Double a = cellLengths.get(0);
        Double b = cellLengths.get(1);
        Double c = cellLengths.get(2);
        
        Double alpha = cellAngles.get(0);
        Double beta = cellAngles.get(1);
        Double gamma = cellAngles.get(2);

        alpha = Math.toRadians(alpha);
        beta = Math.toRadians(beta);
        gamma = Math.toRadians(gamma);
        double cosa = Math.cos(alpha);
        double sina = Math.sin(alpha);
        double cosb = Math.cos(beta);
        double sinb = Math.sin(beta);
        double cosg = Math.cos(gamma); 
        double sing = Math.sin(gamma);
        double volume = 1.0 - Math.pow(cosa, 2) - Math.pow(cosb, 2) - Math.pow(cosg, 2) + 2.0 * cosa * cosb * cosg;
        volume = Math.sqrt(volume);
        
        double[][] vectors = {{0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}, {0.0, 0.0, 0.0}};
        
        List<Double> vector1 = new ArrayList<Double>();        
        vector1.add(a);
        vector1.add(0.0);
        vector1.add(0.0);
        
        List<Double> vector2 = new ArrayList<Double>();
        vector2.add(b * cosg);
        vector2.add(b * sing);
        vector2.add(0.0);
        
        List<Double> vector3 = new ArrayList<Double>();
        vector3.add(c * cosb);
        vector3.add(c * (cosa - cosb * cosg) / sing);
        vector3.add(c * volume / sing);
        
        Element lattice = (Element)document.createElementNS(Constants.cmlNamespace, "module");
        lattice.setAttributeNS(Constants.cmlxPrefixNamespace[1],"cmlx:templateRef","lattice");        
        lattice.appendChild(createArray(document, "xsd:double", "cc:lattice", vector1, " "));
        lattice.appendChild(createArray(document, "xsd:double", "cc:lattice", vector2, " "));
        lattice.appendChild(createArray(document, "xsd:double", "cc:lattice", vector3, " "));
        
        return lattice;
    }
    
    private Node createScalar(Document document, String element, String units, String title, String id, String value) {        
        Element scalar = document.createElementNS(Constants.cmlNamespace, element);        
        scalar.setAttribute("units", units);
        scalar.setAttribute("title", title);
        scalar.setAttribute("id", id);
        scalar.setTextContent(value);
        return scalar;
    }

    private Node createArray(Document document, String dataTypex, String dictRefx, List<Double> values, String delimiter){
        StringBuilder sb = new StringBuilder();
        Element array = document.createElementNS(Constants.cmlNamespace, "array");
        if(!dataTypex.equals("")) 
            array.setAttribute("dataType", dataTypex);
        if(!dictRefx.equals(""))
            array.setAttribute("dictRef", dictRefx);
        if(!delimiter.equals(" "))
            array.setAttribute("delimiter", delimiter);
        array.setAttribute("size", String.valueOf(values.size()));
        for(Object value : values)
            sb.append(value + delimiter);
        if(sb.toString().endsWith(delimiter))
            array.setTextContent(sb.toString().substring(0,sb.toString().length()-delimiter.length()));
        else
            array.setTextContent(sb.toString().trim());
        return array;
    }
    
}
