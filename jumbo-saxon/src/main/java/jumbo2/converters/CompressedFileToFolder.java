package jumbo2.converters;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import org.apache.commons.compress.archivers.ArchiveException;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.utils.IOUtils;

public class CompressedFileToFolder extends BasicConverter<File> {

	private String inputFilePath;

	public CompressedFileToFolder(String inputFilePath) {
		this.inputFilePath = inputFilePath;
	}

	@Override
	public File call() {
		Path tmpFile = null;
		try {
			tmpFile = Files.createTempDirectory("iochem-bd-extraction-");			
			getFileContent(new File(inputFilePath), tmpFile.toFile());
			return tmpFile.toFile();
		} catch (Exception e) {
			if(tmpFile != null)
				deleteDir(tmpFile.toFile());			
		}	
		return null;
	}

	private void getFileContent(File zipFile, File destFolder) throws IOException, ArchiveException {
		String mimetype = Files.probeContentType(zipFile.toPath());	
		switch (mimetype) {
			case "application/zip":				    getZipContent(zipFile, destFolder);
													break;

			case "application/x-compressed-tar": 	getGzipContent(zipFile, destFolder);
													break;

			case "text/plain":						
			default:								getPlainTextContent(zipFile, destFolder);
													break;
								
		}
	}

	private void getZipContent(File zip, File destFolder) throws IOException {
		try (ZipInputStream zis = new ZipInputStream(new FileInputStream(zip))) {			
			ZipEntry zipEntry = zis.getNextEntry();
			while (zipEntry != null) {
	            final File out = new File(destFolder, zipEntry.getName());	            
				if (zipEntry.isDirectory()) {
					if (!out.exists()) {
						if (!out.mkdirs()) {
							throw new IllegalStateException(String.format("Couldn't create directory %s.", out.getAbsolutePath()));
						}
					}
				} else {					
					extractFile(zis, out);										
				}								
				zipEntry = zis.getNextEntry();
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw e;
		}
	}
	
	private static void extractFile(ZipInputStream is, File out) throws IOException {
		byte[] buffer = new byte[2048];
		
		try(FileOutputStream fos = new FileOutputStream(out);
			BufferedOutputStream bos = new BufferedOutputStream(fos, buffer.length)) {

            int len;
            while ((len = is.read(buffer)) > 0) {
                bos.write(buffer, 0, len);
            }						
		} catch (IOException e) {
			logger.error(e.getMessage());
			throw e;
		} 
	}
	
    public void getGzipContent(File zip, File destFolder) throws IOException, ArchiveException {    	
        try{
        	unTar(unGzip(zip, destFolder), destFolder);                                   
        } catch (IOException e) {
            throw e;
        }
    }

    private static File unGzip(final File inputFile, final File destFolder) throws FileNotFoundException, IOException {
        final File outputFile = new File(destFolder, inputFile.getName().substring(0, inputFile.getName().length() - 3));
        try (final GZIPInputStream in = new GZIPInputStream(new FileInputStream(inputFile));
             final FileOutputStream out = new FileOutputStream(outputFile);){
        	IOUtils.copy(in, out);
            return outputFile;
        }catch(Exception e) {
        	throw e;
        }
    }

    private static List<File> unTar(final File inputFile, final File outputDir) throws FileNotFoundException, IOException, ArchiveException {
        final List<File> untaredFiles = new LinkedList<File>();
        final InputStream is = new FileInputStream(inputFile);
        final TarArchiveInputStream debInputStream = (TarArchiveInputStream) new ArchiveStreamFactory().createArchiveInputStream("tar", is);
        TarArchiveEntry entry = null;
        while ((entry = (TarArchiveEntry)debInputStream.getNextEntry()) != null) {
            final File outputFile = new File(outputDir, entry.getName());
            if (entry.isDirectory()) {
                if (!outputFile.exists()) {
                    if (!outputFile.mkdirs()) {
                        throw new IllegalStateException(String.format("Couldn't create directory %s.", outputFile.getAbsolutePath()));
                    }
                }
            } else {
                final OutputStream outputFileStream = new FileOutputStream(outputFile);
                IOUtils.copy(debInputStream, outputFileStream);
                outputFileStream.close();
            }
            untaredFiles.add(outputFile);
        }        
        debInputStream.close();        
        Files.delete(inputFile.toPath());
        return untaredFiles;
    }

	private void getPlainTextContent(File text, File destFolder) throws IOException {
		Files.copy(text.toPath(), Paths.get(destFolder.getCanonicalPath(), text.getName()));		
	}

    private void deleteDir(File file) {
        File[] contents = file.listFiles();
        if (contents != null) {
            for (File f : contents) {
                deleteDir(f);
            }
        }
        file.delete();
    }
}
