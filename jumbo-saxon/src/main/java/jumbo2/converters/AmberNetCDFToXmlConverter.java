package jumbo2.converters;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import jumbo2.navigator.amber.nc.NetCDFDocumentNavigator;

public class AmberNetCDFToXmlConverter extends BasicConverter<Document> {
    
    private String inputFilePath = null;       
    private NetCDFDocumentNavigator documentNavigator = null;
    
    public AmberNetCDFToXmlConverter(String inputFilePath,  String binaryToXmlTemplatePath) throws ParserConfigurationException, SAXException, IOException {      
        this.inputFilePath = inputFilePath;        
        this.documentNavigator = new NetCDFDocumentNavigator(inputFilePath, binaryToXmlTemplatePath);
    }
    
    public Document call() throws Exception {                           
        Long start = System.currentTimeMillis();        
        Document result = documentNavigator.call();
        Long end = System.currentTimeMillis();
        if(logger.getLevel() == Level.DEBUG)                
            printResume(inputFilePath, start, end);
        return result;
    }
}