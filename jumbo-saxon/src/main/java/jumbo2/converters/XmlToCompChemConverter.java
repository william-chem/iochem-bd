/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.converters;

import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import jumbo2.navigator.xml.XmlDocumentNavigator;

public class XmlToCompChemConverter extends BasicConverter<Document>{
	
	private XmlDocumentNavigator documentNavigator = null;
	
	public XmlToCompChemConverter(String inputFilePath, String templateFilePath) throws IOException, ParserConfigurationException, SAXException{		
		documentNavigator = new XmlDocumentNavigator(inputFilePath, templateFilePath);		
	}
	
	public XmlToCompChemConverter(Document inputDocument, String templateFilePath) throws IOException, ParserConfigurationException, SAXException{		
		documentNavigator = new XmlDocumentNavigator(inputDocument, templateFilePath);		
	}
	
	@Override
	public Document call() throws Exception{
		Long start = System.currentTimeMillis();
		Document result = documentNavigator.call();		
	    Long end = System.currentTimeMillis();	
	    if(logger.getLevel() == Level.DEBUG)
	    	printResume(documentNavigator.getInputDocument(), start, end);
	    return result;
	}
	
	
}
