/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.converters;

import org.apache.logging.log4j.Level;
import org.w3c.dom.Document;


public class PlainTextToCompChemConverter extends BasicConverter<Document> {
	
	private String inputFilePath = null;
	private String textToXmlTemplatePath = null;
	private String xmlToCmlTemplatePath = null;
	private String append = null;
	
	public PlainTextToCompChemConverter(String inputFilePath, String textToXmlTemplatePath, String xmlToCmlTemplatePath) {		
		this(inputFilePath, textToXmlTemplatePath, xmlToCmlTemplatePath, null);
	}
	
	public PlainTextToCompChemConverter(String inputFilePath, String textToXmlTemplatePath, String xmlToCmlTemplatePath, String append) {		
		this.inputFilePath = inputFilePath;
		this.textToXmlTemplatePath = textToXmlTemplatePath;
		this.xmlToCmlTemplatePath = xmlToCmlTemplatePath;
		this.append = append;
	}
	
	public Document call() throws Exception {							
		Long start = System.currentTimeMillis();
		
		PlainTextToXmlConverter textToXml = new PlainTextToXmlConverter(inputFilePath, textToXmlTemplatePath, append);
		Document result = textToXml.call();
		
		XmlToCompChemConverter xmlToCompChem = new XmlToCompChemConverter(result, xmlToCmlTemplatePath); 
		result = xmlToCompChem.call();
		
	    Long end = System.currentTimeMillis();
	    if(logger.getLevel() == Level.DEBUG)		    	
	    	printResume(inputFilePath, start, end);
	    return result;
	}
}
