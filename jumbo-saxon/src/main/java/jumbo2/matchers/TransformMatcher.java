/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.namespace.NamespaceContext;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathFactoryConfigurationException;

import jumbo2.matchers.transform.AddAttribute;
import jumbo2.matchers.transform.AddChild;
import jumbo2.matchers.transform.AddDictRef;
import jumbo2.matchers.transform.AddId;
import jumbo2.matchers.transform.AddMap;
import jumbo2.matchers.transform.AddNamespace;
import jumbo2.matchers.transform.AddSibling;
import jumbo2.matchers.transform.AddUnits;
import jumbo2.matchers.transform.Copy;
import jumbo2.matchers.transform.CreateAngle;
import jumbo2.matchers.transform.CreateArray;
import jumbo2.matchers.transform.CreateAtom;
import jumbo2.matchers.transform.CreateCrystal;
import jumbo2.matchers.transform.CreateDate;
import jumbo2.matchers.transform.CreateDouble;
import jumbo2.matchers.transform.CreateFormula;
import jumbo2.matchers.transform.CreateLength;
import jumbo2.matchers.transform.CreateList;
import jumbo2.matchers.transform.CreateMatrix;
import jumbo2.matchers.transform.CreateMatrix33;
import jumbo2.matchers.transform.CreateMolecule;
import jumbo2.matchers.transform.CreateMoleculeVasp;
import jumbo2.matchers.transform.CreateNameValue;
import jumbo2.matchers.transform.CreateString;
import jumbo2.matchers.transform.CreateTable;
import jumbo2.matchers.transform.CreateTorsion;
import jumbo2.matchers.transform.CreateVector3;
import jumbo2.matchers.transform.CreateWrapper;
import jumbo2.matchers.transform.CreateWrapperMetadata;
import jumbo2.matchers.transform.CreateWrapperParameter;
import jumbo2.matchers.transform.CreateWrapperProperty;
import jumbo2.matchers.transform.CreateZMatrix;
import jumbo2.matchers.transform.Delete;
import jumbo2.matchers.transform.GroupSiblings;
import jumbo2.matchers.transform.JoinArrays;
import jumbo2.matchers.transform.Move;
import jumbo2.matchers.transform.MoveRelative;
import jumbo2.matchers.transform.NotImplemented;
import jumbo2.matchers.transform.OperateArray;
import jumbo2.matchers.transform.OperateMatrix;
import jumbo2.matchers.transform.OperateScalar;
import jumbo2.matchers.transform.Pullup;
import jumbo2.matchers.transform.PullupSingleton;
import jumbo2.matchers.transform.SetValue;
import jumbo2.matchers.transform.Split;
import jumbo2.util.Constants;
import net.sf.saxon.lib.NamespaceConstant;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public abstract class TransformMatcher {

	protected static Logger logger = LogManager.getLogger(TransformMatcher.class.getName());
	
	protected static XPathFactory factory = null;
	protected static LinkedHashMap<String,String> namespaces = null;
	protected LinkedHashMap<String,String> attributes = null;
	protected String[] requiredAttributes = null; 
	protected static NamespaceContext namespaceContext = null;
	
	static{
			factory = new net.sf.saxon.xpath.XPathFactoryImpl();
	}
	
	public TransformMatcher(){
		attributes = new LinkedHashMap<String,String>();	
	}

	public static TransformMatcher newTransformMatcher(NamespaceContext namespaceContext2, String process){
		namespaceContext = namespaceContext2;
		switch(process){
		case "addAttribute":
				return new AddAttribute(new String[]{"xpath", "name", "value"});
		case  "addChild":
			return new AddChild(new String[]{"xpath", "elementName"});
		case "addDictRef":
			return new AddDictRef(new String[]{"xpath", "value"}); 
		case "addId":
			return new AddId(new String[]{"xpath", "value"});
		case "addMap":
			return new AddMap(new String[]{"xpath", "id", "from", "to"});
		case "addNamespace":
			return new AddNamespace(new String[]{"xpath", "name", "value"});
		case "addSibling":
			return new AddSibling(new String[]{"xpath", "id", "elementName", "position"});
		case "addUnits":
			return new AddUnits(new String[]{"xpath", "value"});
		case "createLength":
			return new CreateLength(new String[]{"xpath", "atomRefs", "value"});
		case "createAtom":
			return new CreateAtom(new String[]{"xpath"});			
		case "createAngle":
			return new CreateAngle(new String[]{"xpath","atomRefs","value"});
		case "createTorsion":
			return new CreateTorsion(new String[]{"xpath", "atomRefs", "value"});
		case "createZMatrix":
			return new CreateZMatrix(new String[]{"xpath", "id"});
		case "copy":
			return new Copy(new String[]{"xpath", "to"});
		case "createArray":
			return new CreateArray(new String[]{"xpath", "from"});
		case "createCrystal":
			return new CreateCrystal(new String[]{"xpath", "lattice"});
		case "createDate":
			return new CreateDate(new String[]{"xpath"});
		case "createDouble":
			return new CreateDouble(new String[]{"xpath", "dictRef"});
		case "createFormula":
			return new CreateFormula(new String[]{"xpath"});
		case "createList":
			return new CreateList(new String[]{"xpath"});
		case "createMatrix":
			return new CreateMatrix(new String[]{"xpath", "from", "dictRef"});
		case "createMatrix33":
			return new CreateMatrix33(new String[]{"xpath", "from", "dictRef"});
		case "createMolecule":
			return new CreateMolecule(new String[]{"xpath", "id"});
		case "createMoleculeVasp":
			return new CreateMoleculeVasp(new String[]{"xpath", "incar", "potcar", "lattice"});
		case "createNameValue":
			return new CreateNameValue(new String[]{"xpath"});
		case "createString":			
			return new CreateString(new String[]{"xpath"});
		case "createTable":
			return new CreateTable(new String[]{"xpath"});
		case "createVector3":
			return new CreateVector3(new String[]{"xpath", "from", "dictRef"});
		case "createWrapper":
			return new CreateWrapper(new String[]{"xpath", "elementName"});
		case "createWrapperMetadata":
			return new CreateWrapperMetadata(new String[]{"xpath"});
		case "createWrapperParameter":
			return new CreateWrapperParameter(new String[]{"xpath"});
		case "createWrapperProperty":
			return new CreateWrapperProperty(new String[]{"xpath"});
		case "delete":
			return new Delete(new String[]{"xpath"});
		case "groupSiblings":			
			return new GroupSiblings(new String[]{"xpath"});		
		case "joinArrays":
			return new JoinArrays(new String[]{"xpath"});
		case "move":
			return new Move(new String[]{"xpath", "to"});
		case "moveRelative":
			return new MoveRelative(new String[]{"xpath", "to"});
		case "operateScalar":
			return new OperateScalar(new String[]{"xpath", "args"});
		case "operateArray":
			return new OperateArray(new String[]{"xpath", "args"});
		case "operateMatrix":
			return new OperateMatrix(new String[]{"xpath", "args"});
		case "pullup":		
			return new Pullup(new String[]{"xpath"});
		case "pullupSingleton":
			return new PullupSingleton(new String[]{"xpath"});
		case "setValue":
			return new SetValue(new String[]{"xpath", "value"});
		case "split":
			return new Split(new String[]{"xpath"});
		default:
			return new NotImplemented(new String[]{});
		}
	}
	
	public abstract void transformNode(Node parentNode, String indent) throws Exception;
	
	public void processAttribute(Node attribute, String indentation){
		attributes.put(attribute.getNodeName(), attribute.getNodeValue());
		logger.debug(indentation + attribute.getNodeName() + " : " + attribute.getNodeValue());
	}	
	
	public void transform(Node parentNode, String indent) throws Exception{
		for(String requiredAttribute : requiredAttributes)
			if(!attributes.containsKey(requiredAttribute))
				throw new Exception("Missing required attributes on transform");	
		transformNode(parentNode, indent);
	}
	
	protected Element createElement(Document sourceDocument, String elementName){
		Pattern p = Pattern.compile("\\s*(\\S+)\\s*:\\s*(\\S+)\\s*"); 		// cml:module  , cmlx:templateRef    ...
		Matcher matcher = p.matcher(elementName);
		if(matcher.matches()){
			String namespace = namespaceContext.getNamespaceURI(matcher.group(1));
			String element = matcher.group(2);
			if(namespace.equals(Constants.cmlNamespace))
				return sourceDocument.createElementNS(namespace, element);	//default namespace
			else
				return sourceDocument.createElementNS(namespace, elementName);	//other namespace
		}else
			return sourceDocument.createElementNS(Constants.cmlNamespace, elementName);
		
	}
	
	protected String getValue(Node node, String value) throws XPathExpressionException{
		if(value == null)
			return null;
		if(value.equals(""))
			return "";
		else if(value.contains("$string(") || value.contains("$number(")){
			if(value.trim().startsWith("("))
				value = value.trim().substring(1,value.trim().length()-2);	//Replace outer parentheses			
			Pattern pattern = Pattern.compile("(?:\\$string\\(|\\$number\\()(.*?)(?:\\))");			
			Matcher matcher = pattern.matcher(value);
			if(matcher.matches()){
				String query = matcher.group(1);
				XPath xpath = factory.newXPath();
				xpath.setNamespaceContext(namespaceContext);
				XPathExpression expr = xpath.compile(query);
				return (String)expr.evaluate(node, XPathConstants.STRING);
			}else
				return value;
		}
		else
			return value;	
	}
	
	protected HashMap<String, String> readMap(Node node, String xpathToMap) throws XPathExpressionException{
		HashMap<String, String> mapValues = new HashMap<String, String>();
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(xpathToMap);		
		Node map = (Node) expr.evaluate(node, XPathConstants.NODE);		
		for(int inx = 0; inx < map.getChildNodes().getLength(); inx++){
			try{
				Element list = (Element)map.getChildNodes().item(inx);
				String from = list.getAttribute("from");
				String to = list.getAttribute("to");
				mapValues.put(from, to);	
			}catch(Exception e){
				
			}
									
		}
		return mapValues;
	}
	
	public static NodeList queryUsingNamespaces(Element element, String xpathQuery) throws XPathExpressionException {
		NodeList nodeList = null;
		XPathExpression expr = null;		
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		if (xpathQuery == null) 			
			expr = xpath.compile(".");				 
		else 			
			expr = xpath.compile(xpathQuery);							
		nodeList = (NodeList) expr.evaluate(element, XPathConstants.NODESET);			
		return nodeList;
	}
	
	public static ArrayList<Node> queryUsingNamespaces(Element element, String xpathQuery, Integer position) throws XPathExpressionException {
		ArrayList<Node> nodeList = new ArrayList<Node>();
		XPathExpression expr = null;		
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		if (xpathQuery == null) 			
			nodeList.add(element);				 
		else {			
			expr = xpath.compile(xpathQuery);
			NodeList nodes = (NodeList) expr.evaluate(element, XPathConstants.NODESET);
			for(int inx = 0; inx < nodes.getLength(); inx++)
				if(position == null || position == inx+1)
					nodeList.add(nodes.item(inx));
				
		}					
		return nodeList;
	}
	
	
	protected String[] createAtomRefs(Node refNode, String atomRefs, int nrefs) throws XPathExpressionException {
		String[] aRefs = atomRefs.split(Constants.WHITESPACES_REGEX);
		if (aRefs.length != nrefs) {
			throw new RuntimeException("Require "+nrefs+" atomRefs");
		}
		for (int i = 0; i < aRefs.length; i++) {
			Object obj = getValue(refNode, aRefs[i]);
			if (obj == null || obj.equals("")) {
				aRefs = null;
				break;
			}
			aRefs[i] = obj.toString();
			if (!aRefs[i].startsWith("a")) {
				aRefs[i] = "a"+aRefs[i];
			}
		}
		return aRefs;
	}
	
}
