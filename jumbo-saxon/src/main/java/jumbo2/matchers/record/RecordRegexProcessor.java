/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.record;

import java.util.LinkedHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class RecordRegexProcessor {
	
	Logger logger = LogManager.getLogger(RecordRegexProcessor.class.getName());

	public static final String FIELD_REGEX = "\\{\\s*((\\d+)(_(\\d+))?\\s*)?\\s*(B|I|F|E|A|X)\\s*((\\d*)\\.*(\\d*))?(\\s*,\\s*([a-zA-Z]\\w*)\\s*:\\s*([a-zA-Z0-9][\\w\\._-]*)\\s*)?(?:,\\s*[\\w-]+\\s*:\\s*[\\w+-]+\\s*)?\\}";
	public static final String GROUPREGEX  = "(" + FIELD_REGEX +")";	     //Sample captured group = {1_5F10.5,cc:energy} 
	private static Pattern p = Pattern.compile(GROUPREGEX);
	
	private LinkedHashMap<String,RecordField> fields = null;		//QName + recordField object	
	private String initialRegex = null;
	private String finalRegex = null;	
	
	public RecordRegexProcessor(String initialRegex, String indentation) throws Exception{
		fields = new LinkedHashMap<String,RecordField>();		
		Matcher groups = p.matcher(initialRegex);
		logger.debug(indentation + "Text : " + initialRegex);
		StringBuffer sb = new StringBuffer();
		int recordFieldCounter = 1;
		while(groups.find()){
    		logger.debug(indentation + groups.group());
    		RecordField recordField = new RecordField(recordFieldCounter++, groups.group()); 
    		String rep = recordField.toRegexExpression(indentation);
	        groups.appendReplacement(sb, rep);
//	        if(fields.containsKey(recordField.getQName()))
//	        	throw new Exception ("Duplicated RecordField name on same innertext" + initialRegex);
			fields.put(recordFieldCounter + "_" + recordField.getQName(), recordField);	       
	     }
	     groups.appendTail(sb);
	     this.initialRegex = initialRegex;
	     this.finalRegex = sb.toString().trim();	//Remove final tabs and new lines special characters
	}	

	public String getInitialRegex(){
		return this.initialRegex;
	}
	
	public String getFinalRegex(){
		return this.finalRegex;
	}
	
	public LinkedHashMap<String,RecordField> getQNameAndRecordFields(){				
		return fields;
	}
	
	public int numberOfContainedRecordFields(){
		return fields.size();
	}
}
