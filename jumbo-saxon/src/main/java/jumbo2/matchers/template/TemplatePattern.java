/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.template;

import java.util.ArrayList;
import java.util.ListIterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
//http://stackoverflow.com/questions/19807253/java-string-parsing-what-is-faster-regex-or-string-methods
//java.util.regex.Matcher is used inside this class for matching regular expressions
public class TemplatePattern {
	
	private ArrayList<Pattern> matchers;
	private boolean isMultiline = false;	
	private int numberOfUsedLines = 1;
	
	public TemplatePattern(String patternString){
		matchers = new ArrayList<Pattern>();
		if(patternString.contains("$"))
			isMultiline = true;		
		String array[] = patternString.split("\\$");	
		numberOfUsedLines = array.length; 
		for(String pattern : array){	
		    Pattern p = Pattern.compile("^" + pattern + "$");  //We'll add line terminator characters to every pattern that we build
		    matchers.add(p);
		}
	}
	
	public boolean isMultiline(){
		return isMultiline;
	}
	
	public int getNumberOfUsedLines(){
		return numberOfUsedLines;
	}
	
	public boolean matches(String currentLine, ListIterator<String> iter){
		int previousCounter = 0;
		Matcher firstPatternMatcher = matchers.get(0).matcher(currentLine);
		if(!isMultiline() && firstPatternMatcher.find())
			return true;
		else if(isMultiline() && firstPatternMatcher.find()){
			for(int inx = 1; inx < numberOfUsedLines; inx++){				
				if(!iter.hasNext()){
					reverseMovement(iter, previousCounter);
					return false;
				}					
				currentLine = iter.next();
				previousCounter++;
				if(!matchers.get(inx).matcher(currentLine).find()){
					reverseMovement(iter, previousCounter);
					return false;
				}	
			}
			reverseMovement(iter, previousCounter);
			return true;	
		}
		return false;
	}

	/**
	 * This function is used to reverse all lines advanced on our Iterator during multiline pattern check 
	 * @param iter
	 * @param previousCounter
	 */
	private void reverseMovement(ListIterator<String> iter, int previousCounter){
		for(int inx = 0; inx < previousCounter; inx++)
			iter.previous();
	}
	
}
