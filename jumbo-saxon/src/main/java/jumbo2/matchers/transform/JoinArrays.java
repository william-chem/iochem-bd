/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class JoinArrays extends TransformMatcher {

	public JoinArrays(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		String key = attributes.get("key");
		String from = attributes.get("from");
		if (key != null) 
			joinArrays(result, key);
		else if (from != null) {
			Integer fromPos = (attributes.get("fromPosition") == null) ? null : new Integer(attributes.get("fromPosition"));
			for (int inx = 0; inx < result.getLength(); inx++) {				
				Element element = (Element) result.item(inx);				
				ArrayList<Node> fromNodeList = queryUsingNamespaces(element, from, fromPos);
				joinArrays(fromNodeList);
			}
		}else{
			joinArrays(nodeListToListOfNodes(result));
		}
		logger.debug(indent + "transform joinArrays :" + attributes.get("xpath"));
	}
	
		
	private void joinArrays(NodeList arrays, String key) throws XPathExpressionException{
		HashMap<String, List<Node>> nodeListByKey = new HashMap<String, List<Node>>();
		for (int i = 0; i < arrays.getLength(); i++) {
			Element node = (Element)arrays.item(i);
			String keyx = getValue(node, key);
			if (keyx != null && !keyx.equals("")) {
				List<Node> list = nodeListByKey.get(keyx);
				if (list == null) {
					list = new ArrayList<Node>();
					nodeListByKey.put(keyx,list);
				}
				list.add(node);
			}
		}
		Set<String> keySet = nodeListByKey.keySet();
		for (String keyz : keySet) {
			List<Node> list = nodeListByKey.get(keyz);
			joinArrays(list);			
		}
	}
		
	
	private List<Node> nodeListToListOfNodes(NodeList nodeList){
		List<Node> result = new ArrayList<Node>();
		for(int inx = 0; inx < nodeList.getLength(); inx++)
			result.add(nodeList.item(inx));
		return result;
	}
	
	
	
	private void joinArrays(List<Node> arrays){
		if(arrays.size() <= 1)
			return;
		Element baseArray = (Element) arrays.get(0);				
		if (baseArray.getNodeName().equals("array")) {
			String dictRefx = attributes.containsKey("dictRef")? attributes.get("dictRef") : baseArray.getAttribute("dictRef");
			String dataType = baseArray.getAttribute("dataType");
			StringBuilder arrayValues = new StringBuilder();
			int totalSize = 0;
			for (int i = 0; i < arrays.size(); i++) {
				Element array = (Element) arrays.get(i);
				arrayValues.append(array.getTextContent()+ " ");
				totalSize += Integer.parseInt(array.getAttribute("size"));
				if (i > 0) 
					array.getParentNode().removeChild(array);									
			}
			Element finalArray = baseArray.getOwnerDocument().createElementNS(Constants.cmlNamespace, "array");
			finalArray.setAttribute("dictRef", dictRefx);
			finalArray.setAttribute("dataType", dataType);
			finalArray.setAttribute("size", String.valueOf(totalSize));
			if(dataType.equals("xsd:string") && !arrayValues.toString().contains("|")){				
				arrayValues = new StringBuilder(arrayValues.toString().trim().replaceAll(Constants.WHITESPACES_REGEX, "|"));
				finalArray.setAttribute("delimiter", "|");
			}				
			finalArray.setTextContent(arrayValues.toString().trim());			
			baseArray.getParentNode().replaceChild(finalArray, baseArray);
		}
	}

}
