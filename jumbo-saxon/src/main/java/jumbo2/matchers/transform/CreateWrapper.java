/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateWrapper extends TransformMatcher {


	public CreateWrapper(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		String elementName = attributes.get("elementName");
		if(elementName.contains(":") && elementName.split(":")[0].equals(Constants.cmlPrefix))
			elementName = elementName.split(":")[1];		
		Element wrapper = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, elementName);
		if(attributes.containsKey("id"))
			wrapper.setAttribute("id", attributes.get("id"));
		if(attributes.containsKey("dictRef"))
			wrapper.setAttribute("dictRef", attributes.get("dictRef"));
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);
			Node parent = node.getParentNode();
			Element element = (Element) wrapper.cloneNode(true);
			if(parent != null)
				parent.replaceChild(element, node);
			if(node instanceof Text){
				String normalized = node.getTextContent().trim().replaceAll(Constants.WHITESPACES_REGEX, " ");
				String[] strings = normalized.split(Constants.WHITESPACES_REGEX);
				Element array = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "array");
				array.setAttribute("size", String.valueOf(strings.length));
				array.setAttribute("dataType", "xsd:string");				
				StringBuilder value = new StringBuilder();
				for(int inx2 = 0; inx2 < strings.length; inx2++)
					value.append(strings[inx2] + " ");
				array.setTextContent(value.toString().trim());
				node = array;	
			}
			element.appendChild(node);			
		}		
		logger.debug(indent + "transform createWrapper : " + attributes.get("xpath"));
	}
}
