/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import jumbo2.chemistry.Atom;
import jumbo2.chemistry.ConnectionHelper;
import jumbo2.chemistry.Molecule;
import jumbo2.chemistry.MoleculeElement;
import jumbo2.chemistry.Point3d;
import jumbo2.chemistry.Vector3d;
import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CreateMoleculeVasp extends TransformMatcher {	
	
	public CreateMoleculeVasp(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		boolean areFractional = areFractionalCoords(xpath, parentNode);
		Point3d coords[] = readCoords(xpath, parentNode, areFractional);
		Vector3d lattice[] = readLattice(xpath, parentNode);
		ArrayList<String> atomTypes = readAtomTypes(xpath, parentNode);
	
		
		if(coords.length > 0 && atomTypes.size() > 0 && lattice.length > 0){
			XPathExpression expr = xpath.compile(attributes.get("xpath"));
			Element element = (Element)expr.evaluate(parentNode, XPathConstants.NODE);
			Element parent = (Element) element.getParentNode();
			Element dummy = element.getOwnerDocument().createElement("dummy");					
		
			int natoms = atomTypes.size();
			Molecule molecule = new Molecule(attributes.get("id"));
			parent.insertBefore(dummy ,element);
			Atom[] atomArray = createAtoms(natoms);
			for(int inx = 0; inx < natoms; inx++){
				molecule.addAtom(atomArray[inx]);
				addAtomProperties(atomArray[inx], atomTypes.get(inx), coords[inx], lattice, areFractional);
			};
			
			if(!attributes.containsKey("connect") || attributes.get("connect").equals("yes"))
			    ConnectionHelper.connectTheDots(molecule);			
			
			MoleculeElement moleculeElement = new MoleculeElement(factory, namespaceContext, parentNode.getOwnerDocument(), molecule); 
			Node moleculeNode =moleculeElement.buildNode();						
			addCrystalParameters(moleculeNode, lattice);			
			parent.replaceChild(moleculeNode, dummy);
		}
		logger.debug(indent + "transform createMolecule :" + attributes.get("xpath"));		
	}

	private Vector3d[] readLattice(XPath xpath,Node parentNode) throws XPathExpressionException{		
		XPathExpression expr = xpath.compile(attributes.get("lattice"));		
		NodeList lattice = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		Element element = (Element)lattice.item(0);
		Vector3d[] latticeVector = new Vector3d[3];
		NodeList children = element.getChildNodes();
		for(int inx = 0; inx < children.getLength(); inx++){
			Element child = (Element) children.item(inx);
			String[] values = child.getTextContent().trim().split("\\s+");
			Vector3d vector = new Vector3d(Double.valueOf(values[0]), Double.valueOf(values[1]), Double.valueOf(values[2]));
			latticeVector[inx] = vector;
		}
		return latticeVector;
	}
	
	private Point3d[] readCoords(XPath xpath,Node parentNode, boolean areFractional) throws XPathExpressionException{		
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList coords = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		Element element = (Element)coords.item(0);
		Point3d[] coord = new Point3d[element.getChildNodes().getLength()];
		NodeList children = element.getChildNodes();
		for(int inx = 0; inx < children.getLength(); inx++){
			Element child = (Element) children.item(inx);
			Point3d vector = null;
			if(areFractional)
				vector = new Point3d(Double.valueOf(child.getAttribute("xFract")), Double.valueOf(child.getAttribute("yFract")), Double.valueOf(child.getAttribute("zFract")));
			else
				vector = new Point3d(Double.valueOf(child.getAttribute("x3")), Double.valueOf(child.getAttribute("y3")), Double.valueOf(child.getAttribute("z3")));
			coord[inx] = vector;
		}
		return coord;
	}
		
	private ArrayList<String> readAtomTypes(XPath xpath, Node parentNode) throws Exception{
		ArrayList<String> atomCountMap  = new ArrayList<String>();
		XPathExpression expr = xpath.compile(attributes.get("incar"));
		Node incar = (Node)expr.evaluate(parentNode, XPathConstants.NODE);
		expr = xpath.compile(".//cml:array[@dictRef='cc:atomcount']");		
		Node atomCountNode = (Node)expr.evaluate(incar, XPathConstants.NODE);
		String[] atomCount = atomCountNode.getTextContent().trim().split("\\s+");
		
		expr = xpath.compile(attributes.get("potcar"));		
		Node potcar = (Node)expr.evaluate(parentNode, XPathConstants.NODE);
		expr = xpath.compile(".//cml:array[@dictRef='cc:atomType']");
		Node atomTypesNode = (Node)expr.evaluate(potcar, XPathConstants.NODE);		 
		String[] atomTypes = atomTypesNode.getTextContent().trim().split("\\s+");		
		
		if(atomCount.length != atomTypes.length)
			throw new Exception("'Atom count' array doesn't have the same number of elements than 'Atom Types' array!");
		for(int inx = 0; inx < atomCount.length; inx++)
			for(int inx2 = 0; inx2 < Integer.valueOf(atomCount[inx]); inx2++)
				atomCountMap.add(atomTypes[inx]);
		
		return atomCountMap;
	}	
	
	private boolean areFractionalCoords(XPath xpath, Node parentNode) throws XPathExpressionException{
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		Node coords = (Node)expr.evaluate(parentNode, XPathConstants.NODE);
		expr = xpath.compile("exists(./cml:atom[@xFract])");
		return (boolean)expr.evaluate(coords, XPathConstants.BOOLEAN);		
	}
	
	private Atom[] createAtoms(int nAtoms){
		Atom[] atomArray = new Atom[nAtoms];
		for (int iatom = 0; iatom < nAtoms; iatom++) {
			String id = "a"+(iatom+1);
			Atom newAtom = new Atom(id);
			atomArray[iatom] = newAtom;
		}
		return atomArray;		
	}	
	
	private void addAtomProperties(Atom atom, String atomType, Point3d coord, Vector3d[] lattice, boolean areFractional) throws Exception{
		atom.setElementType(atomType);
		if(areFractional){
			atom.setFractionalXYZ3(coord);
			atom.setXYZ3(fractionalToCartesian(lattice, coord));
		}else{
			atom.setXYZ3(coord);
			atom.setFractionalXYZ3(cartesianToFractional(lattice, coord));
		}		
	}
	
	public String[] getSplitContent(String content, String delimiter) {
		String[] ss = new String[0];
		content = content.trim();
		if (content.length() > 0) 
			ss = content.split(delimiter);		    	
		return ss;
	}

	private void addCrystalParameters(Node moleculeNode, Vector3d[] lattice){
		Document doc = moleculeNode.getOwnerDocument();
		Element crystal = (Element)doc.createElementNS(Constants.cmlNamespace, "crystal");
		Element angleA = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element angleB = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element angleC = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element alpha  = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element beta   = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element gamma  = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		
		angleA.setAttribute("units", "nonsi:angstrom");
		angleA.setAttribute("title", "a");
		angleA.setAttribute("id", "sc1");
		angleA.setTextContent(String.valueOf(lattice[0].length()));
		
		angleB.setAttribute("units", "nonsi:angstrom");
		angleB.setAttribute("title", "b");
		angleB.setAttribute("id", "sc2");
		angleB.setTextContent(String.valueOf(lattice[1].length()));

		angleC.setAttribute("units", "nonsi:angstrom");
		angleC.setAttribute("title", "c");
		angleC.setAttribute("id", "sc3");
		angleC.setTextContent(String.valueOf(lattice[2].length()));
		
		alpha.setAttribute("units", "nonsi:degree");
		alpha.setAttribute("title", "alpha");
		alpha.setAttribute("id", "sc4");
		Double angle = Math.round(57.2957795 * lattice[2].angle(lattice[1]) * 100.0) / 100.0;
		alpha.setTextContent(String.valueOf(angle));

		beta.setAttribute("units", "nonsi:degree");
		beta.setAttribute("title", "beta");
		beta.setAttribute("id", "sc5");
		angle = Math.round(57.2957795 * lattice[2].angle(lattice[0]) * 100.0) / 100.0;
		beta.setTextContent(String.valueOf(angle));

		gamma.setAttribute("units", "nonsi:degree");
		gamma.setAttribute("title", "gamma");
		gamma.setAttribute("id", "sc6");
		angle = Math.round(57.2957795 * lattice[0].angle(lattice[1]) * 100.0) / 100.0;
		gamma.setTextContent(String.valueOf(angle));

		crystal.appendChild(angleA);
		crystal.appendChild(angleB);
		crystal.appendChild(angleC);
		crystal.appendChild(alpha);
		crystal.appendChild(beta);
		crystal.appendChild(gamma);
		
		moleculeNode.insertBefore(crystal, moleculeNode.getFirstChild());				
	}
	 
	
	
	 

	
	/********************** Geometry operations *********************/
	
	private Point3d fractionalToCartesian(Vector3d[] lattice, Point3d coord){
		Point3d point = new Point3d();
		point.x = coord.x * lattice[0].x + coord.y * lattice[1].x + coord.z * lattice[2].x;
		point.y = coord.x * lattice[0].y + coord.y * lattice[1].y + coord.z * lattice[2].y;
		point.z = coord.x * lattice[0].z + coord.y * lattice[1].z + coord.z * lattice[2].z;
		return point;		
	}
		
	private Point3d cartesianToFractional(Vector3d[] lattice, Point3d coord){		
		Vector3d[] invaxis = calcInvertedAxes(lattice[0], lattice[1], lattice[2]);
	    Point3d frac = new Point3d();
	    frac.x = invaxis[0].x * coord.x + invaxis[0].y * coord.y + invaxis[0].z * coord.z;
	    frac.y = invaxis[1].x * coord.x + invaxis[1].y * coord.y + invaxis[1].z * coord.z;
	    frac.z = invaxis[2].x * coord.x + invaxis[2].y * coord.y + invaxis[2].z * coord.z;
	    return frac;
	}
	
	 private static Vector3d[] calcInvertedAxes(Vector3d aAxis, Vector3d bAxis, Vector3d cAxis) {
	        double det = aAxis.x * bAxis.y * cAxis.z - aAxis.x * bAxis.z * cAxis.y - aAxis.y * bAxis.x * cAxis.z + aAxis.y * bAxis.z * cAxis.x + aAxis.z * bAxis.x * cAxis.y - aAxis.z * bAxis.y * cAxis.x;
	        Vector3d[] invaxes = new Vector3d[3];
	        invaxes[0] = new Vector3d();
	        invaxes[0].x = (bAxis.y * cAxis.z - bAxis.z * cAxis.y) / det;
	        invaxes[0].y = (bAxis.z * cAxis.x - bAxis.x * cAxis.z) / det;
	        invaxes[0].z = (bAxis.x * cAxis.y - bAxis.y * cAxis.x) / det;

	        invaxes[1] = new Vector3d();
	        invaxes[1].x = (aAxis.z * cAxis.y - aAxis.y * cAxis.z) / det;
	        invaxes[1].y = (aAxis.x * cAxis.z - aAxis.z * cAxis.x) / det;
	        invaxes[1].z = (aAxis.y * cAxis.x - aAxis.x * cAxis.y) / det;

	        invaxes[2] = new Vector3d();
	        invaxes[2].x = (aAxis.y * bAxis.z - aAxis.z * bAxis.y) / det;
	        invaxes[2].y = (aAxis.z * bAxis.x - aAxis.x * bAxis.z) / det;
	        invaxes[2].z = (aAxis.x * bAxis.y - aAxis.y * bAxis.x) / det;
	        return invaxes;
	 }
}
