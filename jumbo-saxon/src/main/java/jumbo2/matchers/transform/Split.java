/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class Split extends TransformMatcher {

	public Split(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = result.item(inx);
			if (node.getNodeName().equals("array")){
				splitArray(node);
			} else if (node.getNodeName().equals("scalar")){
				splitScalarValueToScalars(node);
			}
			
		}
		logger.debug(indent + "transform split :" + attributes.get("xpath"));
	}

	private void splitScalarValueToScalars(Node scalar) {		
		String splitter = null;
		if(attributes.containsKey("splitter"))
			splitter = attributes.get("splitter");
		else 
			splitter = Constants.WHITESPACES_REGEX;		
		String s = scalar.getTextContent().trim();
		String[] ss = s.split(splitter);
		if (ss.length > 1) {
			Element list = (Element)scalar.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");						
			list.setAttribute("dictRef", attributes.containsKey("dictRef")? attributes.get("dictRef"): ((Element)scalar).getAttribute("dictRef"));
			scalar.getParentNode().replaceChild(list, scalar);
			for (String sss : ss) {
				Element scalarx = (Element)scalar.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");
				scalarx.setTextContent(sss);				
				if (attributes.containsKey("dictRef")) 
					scalarx.setAttribute("dictRef", attributes.get("dictRef"));
				if(((Element)scalar).hasAttribute("dataType"))
					scalarx.setAttribute("dataType", ((Element)scalar).getAttribute("dataType"));
				list.appendChild(scalarx);
			}
		}
	}
	
	private void splitArray(Node array) throws NumberFormatException, XPathExpressionException {
		Element list = (Element)array.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");
		Integer icols = null;
		Integer irows = null;
		if(attributes.containsKey("cols") || attributes.containsKey("rows")){
			try{
				icols = Integer.parseInt(getValue(array, attributes.get("cols")));
			}catch(Exception e){}
			try{
				irows = Integer.parseInt(getValue(array, attributes.get("rows")));	
			}catch(Exception e){}			
			list = (Element)splitToArrays(array, irows, icols);
		} else 
			list = (Element)splitToScalars(array);
		
		if (list != null) 
			array.getParentNode().replaceChild(list, array);		
	}
	
	private Node splitToScalars(Node array) {
		int size = Integer.parseInt(((Element)array).getAttribute("size"));
		Element list = (Element)array.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");
		String splitter = ((Element)array).getAttribute("delimiter");
		if(splitter == null || splitter.equals(""))
			splitter = Constants.WHITESPACES_REGEX;		
		for (int j = 0; j < size; j++) {
			Element scalar = (Element)array.getOwnerDocument().createElementNS(Constants.cmlNamespace, "scalar");
			scalar.setTextContent(array.getTextContent().trim().split(splitter)[j]);			
			if (attributes.containsKey("dictRef")) 
				scalar.setAttribute("dictRef", attributes.get("dictRef"));
			if(((Element)array).hasAttribute("dataType"))
				scalar.setAttribute("dataType", ((Element)array).getAttribute("dataType"));
			list.appendChild(scalar);
		}
		return list;
	}

	private Node splitToArrays(Node array, Integer irows, Integer icols) {
		if (irows == null && icols == null) {
			return null;
		}
		int size = Integer.parseInt(((Element)array).getAttribute("size"));
		Element list = (Element)array.getOwnerDocument().createElementNS(Constants.cmlNamespace, "list");
		if (icols != null) {
			if (icols <= 0 || size%icols != 0) {
				throw new RuntimeException("cols ("+icols+") must be factor of array ("+size+")");
			}
			if (irows == null) {
				irows = size/icols;
			} else if (icols * irows != size) {
				throw new RuntimeException("rows ("+irows+") * cols ("+ icols+") != size ("+size+")");
			}
		} else {
			if (irows <= 0 || size%irows != 0) {
				throw new RuntimeException("rows ("+irows+") must be factor of array ("+size+")");
			}
			icols = size/irows;
		}
		for (int j = 0; j < size; j+=icols) {			
			Node subArray = createSubArray((Element)array, j, j+icols-1);
			list.appendChild(subArray);
		}
		return list;
	}
	
	public Node createSubArray(Element array, int start, int end) throws IllegalArgumentException {
			int size = Integer.parseInt(array.getAttribute("size"));
			if (start < 0 || end >= size || end < start) {
				throw new IllegalArgumentException("bad array slice indexes "+start+"/"+end+" in "+size);
			}			
			
			Element subArray = null;			

			String[] sout = new String[end-start+1];
			String[] ss = array.getTextContent().trim().split(Constants.WHITESPACES_REGEX);
			for (int i = start; i <= end; i++) 
				sout[i-start] = ss[i];
			
			StringBuilder values = new StringBuilder();
			for(String soutx : sout)
				values.append(soutx + " ");					
			subArray = array.getOwnerDocument().createElementNS(Constants.cmlNamespace, "array");			
			subArray.setTextContent(values.toString().trim());
			subArray.setAttribute("dataType", array.getAttribute("dataType"));
			subArray.setAttribute("size", String.valueOf(end-start + 1));
			if(array.hasAttribute("delimiter"))
				subArray.setAttribute("delimiter",array.getAttribute("delimiter"));					
			String dictRef = array.getAttribute("dictRef");					
			if (dictRef != null && !dictRef.equals("")) 
				subArray.setAttribute("dictRef", array.getAttribute("dictRef"));
			
			return subArray;
	}
}
