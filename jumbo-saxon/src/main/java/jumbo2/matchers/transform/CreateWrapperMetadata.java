/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateWrapperMetadata extends TransformMatcher {

	public CreateWrapperMetadata(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		
		Pattern p = Pattern.compile("(scalar|array|list|array|table|matrix)");
		
		Element wrapperx = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "metadata");
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);
			Matcher m = p.matcher(node.getNodeName());					
			if(m.matches()){				
				Element wrapper = (Element)wrapperx.cloneNode(true);
				Element element = (Element)node;
				String dictRefx = attributes.containsKey("dictRef")? attributes.get("dictRef") : element.getAttribute("dictRef");
				if(dictRefx == null)
					throw new Exception("Cannot find dictRef for wrapper" + element.toString());
				Pattern hasDictRefPattern = Pattern.compile("(array|list|matrix|module|parameter|property|scalar)");
				Matcher hasDictRefMatcher = hasDictRefPattern.matcher(wrapper.getNodeName());
				if(hasDictRefMatcher.matches())
					wrapper.setAttribute("dictRef", dictRefx);
				else if(wrapper.getNodeName().equals("metadata"))
					wrapper.setAttribute("name", dictRefx);
				
				element.getParentNode().replaceChild(wrapper, element);
				wrapper.appendChild(element);
				element.removeAttribute("dictRef");					
			}
		}
		logger.debug(indent + "transform createWrapperMetadata :" + attributes.get("xpath"));
	}


}
