/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class AddMap extends TransformMatcher {

	public AddMap(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}

	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		ArrayList<String> from = new ArrayList<String>();
		ArrayList<String> to = new ArrayList<String>();
		
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("from"));		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++)
			from.add(((Node)result.item(inx)).getTextContent());			
		expr = xpath.compile(attributes.get("to"));		
		result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++)
			to.add(((Node)result.item(inx)).getTextContent());
		if(from.size() != to.size())
			throw new Exception("Error on addMap transform. The number of elements inside 'From' and 'To' doesn't match!");
		expr = xpath.compile(attributes.get("xpath"));		
		Node baseNode = (Node)expr.evaluate(parentNode, XPathConstants.NODE);
		Element map = parentNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "map");
		map.setAttribute("id", attributes.get("id"));
		for(int inx = 0; inx < from.size(); inx++){
			Element link = baseNode.getOwnerDocument().createElementNS(Constants.cmlNamespace, "link");
			link.setAttribute("from", from.get(inx));
			link.setAttribute("to", to.get(inx));
			map.appendChild(link);
		}
		baseNode.appendChild(map);
		logger.debug(indent + "transform addMap : " + attributes.get("xpath"));
	}

}
