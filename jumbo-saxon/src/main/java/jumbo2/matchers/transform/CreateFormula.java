/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.chemistry.ChemicalElement;
import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateFormula extends TransformMatcher {

	private static String[] atomTypes = {"C","H","Ac","Ag","Al","Am","Ar","As","At","Au","B","Ba","Be","Bh","Bi","Bk","Br","Ca","Cd","Ce","Cf","Cl","Cm","Cn","Co","Cr","Cs","Cu","Db","Ds","Dy","Er","Es","Eu","F","Fe","Fl","Fm","Fr","Ga","Gd","Ge","He","Hf","Hg","Ho","Hs","I","In","Ir","K","Kr","La","Li","Lr","Lu","Lv","Md","Mg","Mn","Mo","Mt","N","Na","Nb","Nd","Ne","Ni","No","Np","O","Os","P","Pa","Pb","Pd","Pm","Po","Pr","Pt","Pu","Ra","Rb","Re","Rf","Rg","Rh","Rn","Ru","S","Sb","Sc","Se","Sg","Si","Sm","Sn","Sr","Ta","Tb","Tc","Te","Th","Ti","Tl","Tm","U","Uuo","Uup","Uus","Uut","V","W","Xe","Y","Yb","Zn","Zr"};
	private static ArrayList<String> atomTypesList = null; 
	private static final String S_SPACE = " ";
	private static final String S_PLUS  = "+";
	private static final String S_MINUS = "-";
    private static final String S_EMPTY = "";
    private static final char C_MINUS 	= '-';
    private static final char C_PLUS 	= '+';
    private static final char C_PERIOD  = '.';
    
    private String concise = "";
    private int charge = 0;
    
	static {
		atomTypesList = new ArrayList<String>(Arrays.asList(atomTypes));
	}
	
	public CreateFormula(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			try{
				Node node = (Node)result.item(inx);
				String value = node.getTextContent();
				Node formula = buildFormula(node.getOwnerDocument(), value);
				node.getParentNode().replaceChild(formula, node);
			}catch(Exception e){
				logger.error(e.getMessage());
				continue;
			}		
		}
		logger.debug(indent + "transform createFormula :" + attributes.get("xpath"));
	}

	private Node buildFormula(Document document, String value) throws Exception{
		return parseAny(document, value);
	}
	
	private Node buildFormulaOLD(Document document, String value) throws Exception{		
		ArrayList<String> elements = new ArrayList<String>();
		ArrayList<Integer> elementCount = new ArrayList<Integer>();	
		Pattern p = Pattern.compile("[A-Z]");		
		int start = 0;
		int end = 0;
		ArrayList<String> matches = new ArrayList<String>();
		for(int inx = 0; inx < value.length(); inx ++){
			Matcher m = p.matcher(value.substring(inx, inx + 1));		
			if(m.matches()){
				if(inx > 0)					
					matches.add(value.substring(start, inx));
				start = inx;
			}			
		}
		matches.add(value.substring(start, value.length()));
		
		Pattern p2 = Pattern.compile("([A-Z][a-z]*)\\s*([0-9]+)?");
		for(String match : matches){			
			Matcher m = p2.matcher(match.trim());
			if(m.matches()){
				String atomType = m.group(1);
				String atomCount = m.group(2);
				if(!atomTypesList.contains(atomType))					
					throw new Exception("Bad element " + atomType);
				elements.add(atomType);
				elementCount.add(atomCount == null? 1 : Integer.valueOf(atomCount));				
			}else{
				throw new Exception("Bad element " + match.trim());
			}
		}			
		StringBuilder concise = new StringBuilder();
		StringBuilder elementType = new StringBuilder();		
		StringBuilder count = new StringBuilder();
		for(int inx = 0; inx < elements.size(); inx++){			
			concise.append(elements.get(inx) + " " + elementCount.get(inx) + " ");
			elementType.append(elements.get(inx) + " ");
			count.append(elementCount.get(inx) + " ");			
		}
		Element formula = (Element)document.createElementNS(Constants.cmlNamespace, "formula");
		Element atomArray = (Element)document.createElementNS(Constants.cmlNamespace, "atomArray");
		formula.appendChild(atomArray);			
		formula.setAttribute("concise", concise.toString().trim());
		atomArray.setAttribute("elementType", elementType.toString().trim());
		atomArray.setAttribute("count", count.toString().trim());
		return formula;					//TODO: Atoms are displayed in the same order as they were read, maybe we must order them in Hill notation 
	}
	
	private Node parseAny(Document document, String formula) throws Exception {
		StringBuilder result = new StringBuilder();
		String ss = formula.trim();
		int l = ss.length();
		int i = 0;
		int ii = 0;
		int charge = 0;
		while (i < l) {
			// skip white
			i = grabWhite(ss, i, l);
			if (i >= l) {
				break; // end of line
			}
			result.append(S_SPACE);
			ii = grabElement(ss, i, l);
			if (ii == i) {
				ii = grabCharge(ss, i, l);
				if (ii != i) {
					charge = parseCharge(ss.substring(i, ii));
				}
				break;
			}
			result.append(ss.substring(i, ii));
			result.append(S_SPACE);
			i = ii;
			i = grabWhite(ss, i, l);
			ii = grabCount(ss, i, l);
			if (ii == i) {
				result.append(1);
			} else {
				result.append(ss.substring(i, ii));
			}
			i = ii;
			i = grabWhite(ss, i, l);
		}
		String sss = result.toString().trim();		
		return createFromString(document, sss, charge);		
	}
	
	private static int parseCharge(String ss) {
		int i = 0;
		ss = ss.trim();
		if (ss.equals(S_PLUS)) {
			i = 1;
		} else if (ss.startsWith(S_PLUS)) {
			i = Integer.parseInt(ss.substring(1));
		} else if (ss.equals(S_MINUS)) {
			i = -1;
		} else if (ss.endsWith(S_MINUS)) {
			i = -Integer.parseInt(ss.substring(0, ss.length()-1));
		} else {
			i = Integer.parseInt(ss);
		}
		return i;
	}
	
	private int grabCharge(String ss, int i, int l) {
		char c = ss.charAt(i);
		if (i < l && (c == C_MINUS || c == C_PLUS)) {
			i++;
		}
		while (i < l) {
			c = ss.charAt(i);
			if (!Character.isDigit(c)) {
				break;
			}
			i++;
		}
		return i;
	}

	private int grabCount(String ss, int i, int l) {
		while (i < l) {
			char c = ss.charAt(i);
			if (c != C_PERIOD && !Character.isDigit(c)) {
				break;
			}
			i++;
		}
		return i;
	}

	private int grabElement(String ss, int i, int l) {
		if (i < l && Character.isUpperCase(ss.charAt(i))) {
			i++;
		}
		if (i < l && Character.isLowerCase(ss.charAt(i))) {
			i++;
		}
		return i;
	}

	private int grabWhite(String ss, int i, int l) {
		while (i  < l) {
			if (!Character.isWhitespace(ss.charAt(i))) {
				break;
			}
			i++;
		}
		return i;
	}
	
	private Node createFromString(Document document, String formulaString, int charge) throws Exception {
		ArrayList<String> elements = new ArrayList<String>();
		ArrayList<Double> elementCount = new ArrayList<Double>();	
		
		StringTokenizer st = new StringTokenizer(formulaString);
		while (st.hasMoreTokens()) {
			String s = st.nextToken();
			String countS = null;
			String elTypeS = null;
	
			try {
				countS = st.nextToken();
			} catch (NoSuchElementException e) {
				throw new RuntimeException("Bad formula: {" + formulaString );
			}
			elTypeS = s;
		
			double c = 0.0;
			ChemicalElement element = ChemicalElement.getChemicalElement(elTypeS);
			if (element == null) {
				throw new RuntimeException("Bad element (" + elTypeS + ") in: " + s);
			}
			if (countS.equals(S_EMPTY)) {
				c = 1;
			} else {
				try {
					c = new Double(countS).doubleValue();
				} catch (NumberFormatException nfe) {
					throw new RuntimeException("Bad element count (" + countS + ") in: " + s);
				}
			}
			elements.add(elTypeS);
			elementCount.add(c);
		}
		
		StringBuilder concise = new StringBuilder();
		StringBuilder elementType = new StringBuilder();		
		StringBuilder count = new StringBuilder();
		
		
		for(int inx = 0; inx < atomTypes.length; inx++){
			if(elements.contains(atomTypes[inx])){
				int index = elements.indexOf(atomTypes[inx]);
				concise.append(elements.get(index) + " " + elementCount.get(index) + " ");
				elementType.append(elements.get(index) + " ");
				count.append(elementCount.get(index) + " ");							
			}
		}
		Element formula = (Element)document.createElementNS(Constants.cmlNamespace, "formula");
		Element atomArray = (Element)document.createElementNS(Constants.cmlNamespace, "atomArray");
		formula.appendChild(atomArray);			
		formula.setAttribute("concise", concise.toString().trim());
		formula.setAttribute("formalCharge", String.valueOf(charge));
		atomArray.setAttribute("elementType", elementType.toString().trim());
		atomArray.setAttribute("count", count.toString().trim());		
		return formula;							
	}
	

}
