/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;

public class OperateArray  extends TransformMatcher {
	
	static DecimalFormatSymbols otherSymbols;
	
	static {
		otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
		otherSymbols.setDecimalSeparator('.');
		otherSymbols.setGroupingSeparator(',');
	}	
	
	public OperateArray(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList resultList = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);		
		Pattern p = Pattern.compile("\\s*operator\\s*=\\s*(\\S+)\\s*operand\\s*=\\s*(\\$(?:string|number)\\(.*\\)|\\S+)\\s*(?:\\s*format\\s*=\\s*(\\S+)\\s*)?");
		Matcher m = p.matcher(attributes.get("args"));				
		if(m.matches()){			
			String operator = m.group(1);
			String operand = m.group(2);
			String format = m.group(3);				
			for(int inx = 0; inx < resultList.getLength(); inx++){
				StringBuilder resultStr = new StringBuilder();							
				Element node = (Element)resultList.item(inx);
				Double secondOperand = Double.valueOf(getValue(node, operand));
				String firstOperands[] = node.getTextContent().trim().split("\\s+");				
				for(String firstOperandStr : firstOperands){
					double finalValue = 0.0;
					double firstOperand = Double.valueOf(firstOperandStr);
					switch(operator){
						case	"add" :			finalValue = firstOperand + secondOperand;		
												break;
						case 	"substract":	finalValue = firstOperand - secondOperand;						
												break;
						case	"multiply":		finalValue = firstOperand * secondOperand;
												break;
						case 	"divide":		finalValue = firstOperand / secondOperand;
												break;
					}									
					resultStr.append(formatValue(finalValue, format) + " ");
				}
				node.setTextContent(String.valueOf(resultStr.toString().trim()));
			}			
		}
		logger.debug(indent + "transform operateScalar :" + attributes.get("xpath") + " " + attributes.get("args"));
	}
		

	private String formatValue(double finalValue, String format) {
		if(format == null || format.equals(""))
			return String.valueOf(finalValue);		
		try {
			DecimalFormat decimalFormat = new DecimalFormat(format, otherSymbols);
			return decimalFormat.format(finalValue);			
		}catch(Exception e) {
			return String.valueOf(finalValue);
		}
	}
	
}


