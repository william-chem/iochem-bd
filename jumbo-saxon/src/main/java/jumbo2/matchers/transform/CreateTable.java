/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateTable extends TransformMatcher {

	public CreateTable(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Element element = (Element)result.item(inx);
			if(element.getNodeName().equals("list")){
				Element table = element.getOwnerDocument().createElementNS(Constants.cmlNamespace, "table");
				table.setAttribute("tableType", "columnBased");
				Element arrayList = element.getOwnerDocument().createElementNS(Constants.cmlNamespace, "arrayList");
				table.appendChild(arrayList);							
				while(element.hasChildNodes()){				
					Element childElement = (Element) element.getFirstChild();
					if(!childElement.getNodeName().equals("array"))
						throw new Exception("list must only have array children");
					arrayList.appendChild(childElement);				
				}
				element.getParentNode().replaceChild(table, element);								
			}	
		}
		logger.debug(indent + "transform createTable :" + attributes.get("xpath"));
	}
}
