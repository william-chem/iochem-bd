/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import java.util.ArrayList;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.chemistry.Atom;
import jumbo2.chemistry.ConnectionHelper;
import jumbo2.chemistry.Molecule;
import jumbo2.chemistry.MoleculeElement;
import jumbo2.chemistry.Point3d;
import jumbo2.chemistry.Vector3d;
import jumbo2.matchers.TransformMatcher;
import jumbo2.util.Constants;

public class CreateCrystal extends TransformMatcher {	

	enum CoordinatesType { CARTESIAN, FRACTIONAL, BOTH };
	
	public CreateCrystal(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}

	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {		
		try {
			XPath xpath = factory.newXPath();
			xpath.setNamespaceContext(namespaceContext);			
			if(attributes.containsKey("mode")){					//New version of createMolecule transform
				if(attributes.get("mode").equals("multiple")){
					XPathExpression expr = xpath.compile(attributes.get("xpath"));
					NodeList atomArrays = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
					
					expr = xpath.compile(attributes.get("lattice"));
					NodeList lattices = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
					
					for(int inx = 0; inx < atomArrays.getLength(); inx++){
						Element atomArray = (Element)atomArrays.item(inx);						
						Vector3d[] lattice = readLattice(xpath, lattices.item(inx));
						Atom[] atoms = readAtoms(xpath, lattice, atomArray);
						if(atoms.length > 0 && lattice.length > 0){

							//Element element = (Element)expr.evaluate(parentNode, XPathConstants.NODE);
							Element parent = (Element)atomArray.getParentNode().getParentNode();
							Element dummy = parent.getOwnerDocument().createElement("dummy");					

							int natoms = atoms.length;
							Molecule molecule = new Molecule(attributes.get("id"));
							parent.appendChild(dummy);
							for(int inx2 = 0; inx2 < natoms; inx2++){
								molecule.addAtom(atoms[inx2]);
							};
							if(!attributes.containsKey("connect") || attributes.get("connect").equals("yes"))
							    ConnectionHelper.connectTheDots(molecule);
							
							MoleculeElement moleculeElement = new MoleculeElement(factory, namespaceContext, parentNode.getOwnerDocument(), molecule); 
							Node moleculeNode =moleculeElement.buildNode();
							addCrystalParameters(moleculeNode, lattice);
							parent.replaceChild(moleculeNode, dummy);
						}
					}
				}
			}else{
				Vector3d lattice[] = readLattice(xpath, parentNode);
				Atom atoms[] = readAtoms(xpath, lattice, parentNode);
				if(atoms.length > 0 && lattice.length > 0){
					XPathExpression expr = xpath.compile(attributes.get("xpath"));
					Element element = (Element)expr.evaluate(parentNode, XPathConstants.NODE);
					Element parent = (Element) element.getParentNode();
					Element dummy = element.getOwnerDocument().createElement("dummy");					
				
					int natoms = atoms.length;
					Molecule molecule = new Molecule(attributes.get("id"));
					parent.insertBefore(dummy ,element);
					for(int inx = 0; inx < natoms; inx++){
						molecule.addAtom(atoms[inx]);
					};
					if(!attributes.containsKey("connect") || attributes.get("connect").equals("yes"))
					    ConnectionHelper.connectTheDots(molecule);			
					
					MoleculeElement moleculeElement = new MoleculeElement(factory, namespaceContext, parentNode.getOwnerDocument(), molecule); 
					Node moleculeNode =moleculeElement.buildNode();						
					addCrystalParameters(moleculeNode, lattice);			
					parent.replaceChild(moleculeNode, dummy);
				}	
			}
		}catch(Exception e) {
			logger.error("Exception raised on transform createMolecule :" + attributes.get("xpath"));
		}	
		logger.debug(indent + "transform createMolecule :" + attributes.get("xpath"));		
	}

	private Vector3d[] readLattice(XPath xpath,Node parentNode) throws XPathExpressionException{		
		XPathExpression expr = xpath.compile(attributes.get("lattice"));		
		NodeList lattice = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		Element element = (Element)lattice.item(0);
		ArrayList<Vector3d> latticeVector = new ArrayList<>();
		NodeList children = element.getChildNodes();
		for(int inx = 0; inx < children.getLength(); inx++){
		    if(children.item(inx) instanceof Element) {
		        Element child = (Element) children.item(inx);
	            String[] values = child.getTextContent().trim().split("\\s+");
	            latticeVector.add(new Vector3d(Double.valueOf(values[0]), Double.valueOf(values[1]), Double.valueOf(values[2])));	            
		    }			
		}
		return latticeVector.toArray(new Vector3d[latticeVector.size()]);
	}
	
	private Atom[] readAtoms(XPath xpath,Vector3d lattice[], Node parentNode) throws Exception{
		ArrayList<Atom> atoms = new ArrayList<Atom>();
		
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList coords = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		Element element = (Element)coords.item(0);
		NodeList children = element.getChildNodes();
		for(int inx = 0; inx < children.getLength(); inx++){
			Element child = (Element) children.item(inx);
			Atom atom = new Atom(child.getAttribute("id"));						
			if(child.hasAttribute("xFract") && !child.getAttribute("xFract").equals("")) 
				atom.setFractionalXYZ3(new Point3d(Double.valueOf(child.getAttribute("xFract")), Double.valueOf(child.getAttribute("yFract")), Double.valueOf(child.getAttribute("zFract"))));
			else 
				atom.setFractionalXYZ3(null);
			if(child.hasAttribute("x3") && !child.getAttribute("x3").equals(""))				
				atom.setXYZ3(new Point3d(Double.valueOf(child.getAttribute("x3")), Double.valueOf(child.getAttribute("y3")), Double.valueOf(child.getAttribute("z3"))));
			else
				atom.setXYZ3(null);			
			
			//Fill missing values
			if(atom.getFractionaXYZ3() == null) 
				atom.setFractionalXYZ3(cartesianToFractional(lattice, atom.getXYZ3()));
			if(atom.getXYZ3() == null) 
				atom.setXYZ3(fractionalToCartesian(lattice, atom.getFractionaXYZ3()));
			
			atom.setElementType(child.getAttribute("elementType"));
			atoms.add(atom);
		}
		return atoms.toArray(new Atom[atoms.size()]);
	}
	
	public String[] getSplitContent(String content, String delimiter) {
		String[] ss = new String[0];
		content = content.trim();
		if (content.length() > 0) 
			ss = content.split(delimiter);		    	
		return ss;
	}

	private void addCrystalParameters(Node moleculeNode, Vector3d[] lattice){
		Document doc = moleculeNode.getOwnerDocument();
		Element crystal = (Element)doc.createElementNS(Constants.cmlNamespace, "crystal");
		Element angleA = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element angleB = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element angleC = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element alpha  = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element beta   = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		Element gamma  = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		
		angleA.setAttribute("units", "nonsi:angstrom");
		angleA.setAttribute("title", "a");
		angleA.setAttribute("id", "sc1");
		angleA.setTextContent(String.valueOf(lattice[0].length()));
		
		angleB.setAttribute("units", "nonsi:angstrom");
		angleB.setAttribute("title", "b");
		angleB.setAttribute("id", "sc2");
		angleB.setTextContent(String.valueOf(lattice[1].length()));

		angleC.setAttribute("units", "nonsi:angstrom");
		angleC.setAttribute("title", "c");
		angleC.setAttribute("id", "sc3");
		angleC.setTextContent(String.valueOf(lattice[2].length()));
		
		alpha.setAttribute("units", "nonsi:degree");
		alpha.setAttribute("title", "alpha");
		alpha.setAttribute("id", "sc4");
		Double angle = Math.round(57.2957795 * lattice[2].angle(lattice[1]) * 100.0) / 100.0;
		alpha.setTextContent(String.valueOf(angle));

		beta.setAttribute("units", "nonsi:degree");
		beta.setAttribute("title", "beta");
		beta.setAttribute("id", "sc5");
		angle = Math.round(57.2957795 * lattice[2].angle(lattice[0]) * 100.0) / 100.0;
		beta.setTextContent(String.valueOf(angle));

		gamma.setAttribute("units", "nonsi:degree");
		gamma.setAttribute("title", "gamma");
		gamma.setAttribute("id", "sc6");
		angle = Math.round(57.2957795 * lattice[0].angle(lattice[1]) * 100.0) / 100.0;
		gamma.setTextContent(String.valueOf(angle));

		crystal.appendChild(angleA);
		crystal.appendChild(angleB);
		crystal.appendChild(angleC);
		crystal.appendChild(alpha);
		crystal.appendChild(beta);
		crystal.appendChild(gamma);
		
		moleculeNode.insertBefore(crystal, moleculeNode.getFirstChild());				
	}
	 
	
	
	 

	
	/********************** Geometry operations *********************/
	
	private Point3d fractionalToCartesian(Vector3d[] lattice, Point3d coord){
		Point3d point = new Point3d();
		point.x = coord.x * lattice[0].x + coord.y * lattice[1].x + coord.z * lattice[2].x;
		point.y = coord.x * lattice[0].y + coord.y * lattice[1].y + coord.z * lattice[2].y;
		point.z = coord.x * lattice[0].z + coord.y * lattice[1].z + coord.z * lattice[2].z;
		return point;		
	}
		
	private Point3d cartesianToFractional(Vector3d[] lattice, Point3d coord){		
		Vector3d[] invaxis = calcInvertedAxes(lattice[0], lattice[1], lattice[2]);
	    Point3d frac = new Point3d();
	    frac.x = invaxis[0].x * coord.x + invaxis[0].y * coord.y + invaxis[0].z * coord.z;
	    frac.y = invaxis[1].x * coord.x + invaxis[1].y * coord.y + invaxis[1].z * coord.z;
	    frac.z = invaxis[2].x * coord.x + invaxis[2].y * coord.y + invaxis[2].z * coord.z;
	    return frac;
	}
	
	 private static Vector3d[] calcInvertedAxes(Vector3d aAxis, Vector3d bAxis, Vector3d cAxis) {
	        double det = aAxis.x * bAxis.y * cAxis.z - aAxis.x * bAxis.z * cAxis.y - aAxis.y * bAxis.x * cAxis.z + aAxis.y * bAxis.z * cAxis.x + aAxis.z * bAxis.x * cAxis.y - aAxis.z * bAxis.y * cAxis.x;
	        Vector3d[] invaxes = new Vector3d[3];
	        invaxes[0] = new Vector3d();
	        invaxes[0].x = (bAxis.y * cAxis.z - bAxis.z * cAxis.y) / det;
	        invaxes[0].y = (bAxis.z * cAxis.x - bAxis.x * cAxis.z) / det;
	        invaxes[0].z = (bAxis.x * cAxis.y - bAxis.y * cAxis.x) / det;

	        invaxes[1] = new Vector3d();
	        invaxes[1].x = (aAxis.z * cAxis.y - aAxis.y * cAxis.z) / det;
	        invaxes[1].y = (aAxis.x * cAxis.z - aAxis.z * cAxis.x) / det;
	        invaxes[1].z = (aAxis.y * cAxis.x - aAxis.x * cAxis.y) / det;

	        invaxes[2] = new Vector3d();
	        invaxes[2].x = (aAxis.y * bAxis.z - aAxis.z * bAxis.y) / det;
	        invaxes[2].y = (aAxis.z * bAxis.x - aAxis.x * bAxis.z) / det;
	        invaxes[2].z = (aAxis.x * bAxis.y - aAxis.y * bAxis.x) / det;
	        return invaxes;
	 }
}
