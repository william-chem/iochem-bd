/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import jumbo2.matchers.TransformMatcher;

public class Move extends TransformMatcher {

	public Move(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));		
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		expr = xpath.compile(attributes.get("to"));
		NodeList toParent =  (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		if(toParent.getLength() != 1){
			logger.debug(indent + "wrong toXpath: "+ attributes.get("to"));
			return;
		}				
		Node parent = toParent.item(0);		
		for(int inx = 0; inx < result.getLength(); inx++){
			Node node = (Node)result.item(inx);
			if(node instanceof Element && !(node.equals(parent))){
				int pos = attributes.containsKey("position") ? Integer.parseInt(attributes.get("position")) -1 : -1;
				int toChildCount = parent.getChildNodes().getLength();
				if(pos > toChildCount)
					pos = toChildCount;
				if (pos == -1 || pos == toChildCount) {
					node.getParentNode().removeChild(node);
					parent.appendChild(node);
				}else if (pos >= 0 && pos <= toChildCount) {
					node.getParentNode().removeChild(node);
					parent.insertBefore(node, parent.getChildNodes().item(pos));					
				}
				
			}			
		}
		logger.debug(indent + "transform move :" + attributes.get("xpath"));
	}

}
