/**
 * jumbo-saxon - Implementation of jumbo-converters tool using Saxon parsing library.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package jumbo2.matchers.transform;

import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;

import jumbo2.chemistry.Atom;
import jumbo2.chemistry.ConnectionHelper;
import jumbo2.chemistry.Molecule;
import jumbo2.chemistry.MoleculeElement;
import jumbo2.matchers.TransformMatcher;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class CreateZMatrix extends TransformMatcher {

	public CreateZMatrix(String[] requiredAttributes){
		super();
		this.requiredAttributes = requiredAttributes;
	}
	
	@Override
	public void transformNode(Node parentNode, String indent) throws Exception {
		String selectChild = ".//cml:atom[not(@x3) or not(@y3) or not(@z3)] | .//cml:angle | .//cml:length | .//cml:torsion";		
		XPath xpath = factory.newXPath();
		xpath.setNamespaceContext(namespaceContext);
		XPathExpression expr = xpath.compile(attributes.get("xpath"));
		NodeList result = (NodeList)expr.evaluate(parentNode, XPathConstants.NODESET);
		for(int inx = 0; inx < result.getLength(); inx++){
			Element node = (Element)result.item(inx);			
			NodeList children = queryUsingNamespaces(node, selectChild);	
			if (children.getLength() > 0) 
				node.appendChild(buildMoleculeFromZMatrix(node.getOwnerDocument(), children));
		}
		logger.debug(indent + "transform createZmatrix :" + attributes.get("xpath"));
	}
	
	private Node buildMoleculeFromZMatrix(Document document, NodeList children) throws Exception{		
		Molecule molecule = new Molecule(attributes.get("id"));
		for(int inx = 0; inx < children.getLength(); inx++ ){
			Node childNode = children.item(inx);
			if (childNode.getLocalName().equals("atom")) 				
				molecule.addAtom(new Atom(childNode.cloneNode(true)));
			else				
				molecule.addZMatrixLine(childNode.cloneNode(true));				
		} 	
		molecule.addCartesiansTo(); 
		ConnectionHelper.connectTheDots(molecule);
		MoleculeElement moleculeElement = new MoleculeElement(factory, namespaceContext, document, molecule); 
		Node moleculeNode =moleculeElement.buildNode();				
		return moleculeNode;
	}

		
}

