# jumbo-converters project 

This project bundles all conversion templates used inside ioChem-BD *Create* module by *jumbo-saxon* tool 
to convert textual files to Chemical Markup Language (CML), an XML format specially crafted to 
store computational chemistry information.

This project has derived from the original [jumbo-converters](https://bitbucket.org/wwmm/jumbo-converters) from Peter Murray Rust. 
In contrast to the original project, this project only stores conversion templates, the converter tool has been moved 
to *jumbo-saxon* to improve task independence.

## Bundle templates

To package all the templates into one folder, place in the root of the project and run the *publish* ant target:

```shell
$ cd jumbo-converters
$ ant publish
```

It will generate a *target* folder with all templates in it and also synchronize current templates into Create project. 




