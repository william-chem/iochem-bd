<?xml version="1.0" encoding="UTF-8"?>
<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<template id="program.header" name="Program header" pattern="\s*\^{5}\s*M\sO\sL\sC\sA\sS.*" pattern2="\s{25,}(MOL|OPE)\s*"  pattern3="\s{30,}@@@@@\s{30,}"  
	endPattern="\s+For\sthe\sauthor\slist\sand\sthe\srecommended\scitation\sconsult.*" endPattern2="\s+consult\shttp:\/\/www\.molcas\.org\/citations\.html.*" endPattern3="\s*consult\sthe\sfile\sCONTRIBUTORS\.md.*"
	endOffset="1" repeat="*">
	
	<comment class="example.input" id="program.header">
                                              ^^^^^            M O L C A S
                                             ^^^^^^^           version 7.4 patchlevel 097
                               ^^^^^         ^^^^^^^
                              ^^^^^^^        ^^^ ^^^
                              ^^^^^^^       ^^^^ ^^^
                              ^^^ ^^^       ^^^^ ^^^
                              ^^^ ^^^^      ^^^  ^^^
                              ^^^  ^^ ^^^^^  ^^  ^^^^
                             ^^^^      ^^^^   ^   ^^^
                             ^   ^^^   ^^^^   ^^^^  ^
                            ^   ^^^^    ^^    ^^^^   ^
                        ^^^^^   ^^^^^   ^^   ^^^^^   ^^^^^
                     ^^^^^^^^   ^^^^^        ^^^^^    ^^^^^^^
                 ^^^^^^^^^^^    ^^^^^^      ^^^^^^^   ^^^^^^^^^^^
               ^^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^   ^^^^^^^^^^^^^
               ^^^^^^^^^^^^^   ^^^^             ^^^   ^^^      ^^^^
               ^^^^^^^^^^^^^                          ^^^      ^^^^
               ^^^^^^^^^^^^^       ^^^^^^^^^^^^        ^^      ^^^^
               ^^^^^^^^^^^^      ^^^^^^^^^^^^^^^^      ^^      ^^^^
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^
               ^^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^    ^^      ^^^^    ^^^^^     ^^^    ^^^^^^
               ^^^^^^^^^^^^   ^^^^^^^^^^^^^^^^^^^^^^   ^^      ^^^^   ^^^^^^^    ^^^   ^^^  ^^^
               ^^^^^^^^^^^    ^^^^^^^^^^^^^^^^^^^^^^   ^       ^^^^  ^^^    ^^   ^^^   ^^    ^^
               ^^^^^^^^^^^     ^^^^^^^^^^^^^^^^^^^^            ^^^^  ^^         ^^ ^^  ^^^^^
               ^^^^^^^^^^      ^^^^^^^^^^^^^^^^^^^^        ^   ^^^^  ^^         ^^ ^^   ^^^^^^
               ^^^^^^^^          ^^^^^^^^^^^^^^^^          ^   ^^^^  ^^        ^^^^^^^     ^^^^
               ^^^^^^      ^^^     ^^^^^^^^^^^^     ^      ^   ^^^^  ^^^   ^^^ ^^^^^^^ ^^    ^^
                         ^^^^^^                    ^^      ^   ^^^^   ^^^^^^^  ^^   ^^ ^^^  ^^^
                      ^^^^^^^^^^^^             ^^^^^^      ^           ^^^^^  ^^     ^^ ^^^^^^
               ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                 ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                     ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                        ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
                            ^^^^^^^^^^^^^^^^^^^^^^^^^^
                               ^^^^^^^^^^^^^^^^^^^^
                                   ^^^^^^^^^^^^
                                       ^^^^^^

                           Copyright, all rights, reserved:
                         Permission is hereby granted to use
                but not to reproduce or distribute any part of this
             program. The use is restricted to research purposes only.
                            Lund University Sweden, 2008.

 For the author list and the recommended citation consult section 1.5 of the MOLCAS user's guide.
	</comment>
	<comment class="example.input" id="program.header2">
                               MOL
                               MOL
                   MOL        CASMO
                  MOLCA       SM OL
                  MO LC       AS  MO
                  MO  LC     AS   MO
                  MO  L  CAS      MO
                  M  O   LCA   S  MO
                    MOL   CA   SMO  L
              MOL   CAS   M   OLCA   SM
          MOLCASM  OLCAS     MOLCA   SMOLCA
       MOLCASMOL   CASMOLCASMOLCAS   MOLCASMOL
      MOLCASMOLC   AS            MO  LCA    SMO
      MOLCASMOLC       ASMOLCA        S     MOL
      MOLCASMOLC    ASMOLCASMOLCA     S     MOL
      MOLCASMOLC   ASMOLCASMOLCASM    O     LCA
      MOLCASMOL   CASMOLCASMOLCASMO   L     CAS    MOLC    AS     MOL
      MOLCASMOL   CASMOLCASMOLCASMOL  C     ASM   OL  CA   SM    O   L
      MOLCASMOL   CASMOLCASMOLCASMO         LCA   S        M O   L
      MOLCASMO     LCASMOLCASMOLCA          SMO   L       C  A    SMO
      MOLCAS        MOLCASMOLCASM           OLC   A       SMOL       C
      MOL     CASM     OLCASMO     L     C  ASM   O    L CA  SM O    L
            MOLCASMOL            CASM    O         LCAS  M    O  LCAS
       MOLCASMOLCASMOLCASMOLCASMOLCASMOLCASMOL
          MOLCASMOLCASMOLCASMOLCASMOLCASMOL
              MOLCASMOLCASMOLCASMOLCASM
                 MOLCASMOLCASMOLCASM        version 8.0
                     MOLCASMOLCA            Molcas 8 service pack 1
                         MOL                fixed ANO-RCC and CASPT2

                     Copyright, all rights, reserved:
                   Permission is hereby granted to use
          but not to reproduce or distribute any part of this
       program. The use is restricted to research purposes only.
                      Lund University, Sweden, 2015.

            For the author list and the recommended citation,
             consult http://www.molcas.org/citations.html
	</comment>	
	<comment class="example.input" id="program.header3">
                             OPE
                             OPE          NMOL  CASOP  ENMOLC A   SO
                 OPE        NMOLC        AS  OP EN  MO LC     AS  OP
                OPENM       OL CA        SO  PE NM  OL CA     SOP EN
                OP EN      MO   LC       AS  OP ENMOL  CASO   PENMOL
                OP  EN     MO   LC       AS  OP EN     MO     LC ASO
               OP   E  NMOL  C  AS       OP  EN MO     LC     AS  OP
               OP  E   NMO   LC AS        OPEN  MO     LCASOP EN   M
               O  PEN   MO  LCA  SO
            OPE   NMO   L   CAS    OP
        OPENMOL  CASOP     ENMOL   CASOPE
     OPENMOLCA   SOPENMOLCASOPEN   MOLCASOPE
    OPENMOLCAS   OP           EN   MOL    CAS
    OPENMOLCAS       OP  ENM        O     LCA
    OPENMOLCAS    OPEN  MOLCASO     P  E  NMO
    OPENMOLCAS     OP               E  N  MOL
    OPENMOLCA   SO           PENM   O  L  CAS    OPEN    MO    LCAS
    OPENMOLCA   SOP           ENM   O  L  CAS   OP  EN  MOLC  AS   O
    OPENMOLCA   SOPE           NM      O  LCA   S      OP  EN MO
    OPENMOLC                AS         O  PEN   M      OL  CA  SOPE
    OPENMO        LCASOPE  NMOL        C  ASO   P      ENMOLC     AS
    OPE     NMO      LCA  SO     P     E   NM   OL  CA SO  PE N   MO
          OPENMOLCA            SOPE   NMO        LCAS  O    P  ENMO
     OPENMOLCASOPENMOLCASOPENMOLCASOPENMOLCA
        OPENMOLCASOPENMOLCASOPENMOLCASOPE
            OPENMOLCASOPENMOLCASOPENM
               OPENMOLCASOPENMOLCA        version: 18.09
                   OPENMOLCASO
                       OPE                tag: 790-g23d48b3-dirty

 OpenMolcas is free software; you can redistribute it and/or modify it
 under the terms of the GNU Lesser General Public License version 2.1.
 OpenMolcas is distributed in the hope that it will be useful, but it
 is provided "as is" and without any express or implied warranties.
 For more details see the full text of the license in the file
 LICENSE or in &lt;http://www.gnu.org/licenses/&gt;.

                 Copyright (C) The OpenMolcas Authors
           For the author list and the recommended citation,
                   consult the file CONTRIBUTORS.md	
	</comment>

	<templateList>
		<template pattern="\s*\^{5}\s*M\sO\sL\sC\sA\sS.*" endPattern="\s+For\sthe\sauthor\slist\sand\sthe\srecommended\scitation\sconsult.*" endOffset="1">
			<record repeat="1"/>			
			<record>\s*\^{7}\s+version{A,cc:programVersion}{X,cc:programSubversion}</record>
			<transform process="addChild" xpath="." position="1" elementName="cml:scalar" dictRef="cc:program" value="Molcas"/>
			<transform process="addAttribute" xpath=".//cml:scalar[@dictRef='cc:program']" name="dataType" value="xsd:string"/>						
		</template>
	
		<template pattern="\s{30,}MOL\s{30,}" endPattern2="\s+consult\shttp:\/\/www\.molcas\.org\/citations\.html.*" endOffset="1">
			<record repeat="26"/>
			<record>\s+MOLCASMOLCASMOLCASM\s+version{A,cc:programVersion}.*</record>
			<record>\s+MOLCASMOLCA\s+{A,cc:program}\s+\S+\s+{X,cc:programSubversion}</record>						
		</template>
				
		<template pattern="\s{30,}@@@@@\s{30,}" endPattern2="\s+consult\shttp:\/\/www\.molcas\.org\/citations\.html.*" endOffset="1">
			<record repeat="7"/>
			<record>\s+.*@@@@\s+Version{A,cc:programVersion}.*</record>
			<record/>
			<record>\s+.*@@@\s+Release\sat\s{X,cc:programSubversion}</record>						
		</template>
		
		<template pattern="\s{25,}OPE\s*" endPattern2="\s*consult\sthe\sfile\sCONTRIBUTORS\.md.*" endOffset="1">
			<record repeat="26"/>
			<record>\s+OPENMOLCASOPENMOLCA\s+version:{A,cc:programVersion}.*</record>
			<record/>
			<record>\s+OPE\s+tag:\s+{X,cc:programSubversion}</record>						
		</template>
		
		
	</templateList>
	
	<transform process="addChild" id="program" xpath="." elementName="cml:scalar" dictRef="cc:program" value="Molcas" />
	<transform process="addAttribute" xpath=".//cml:scalar[not(@datatType) and @dictRef='cc:program']" name="dataType" value="xsd:string" />

	<transform process="delete" xpath="(.//cml:scalar[@dictRef='cc:program'])[2]" />
	<transform process="move" xpath=".//cml:scalar" to="." />
	<transform process="delete" xpath=".//cml:list" />
	<transform process="delete" xpath=".//cml:module" />
	 
	<comment class="example.output" id="program.header">
      <module cmlx:templateRef="program.header" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
         <scalar dataType="xsd:string" dictRef="cc:program">Molcas</scalar>
         <scalar dataType="xsd:string" dictRef="cc:programVersion">7.4</scalar>
         <scalar dataType="xsd:string" dictRef="cc:programSubversion">patchlevel 097</scalar>
      </module>
	</comment>

	<comment class="example.output" id="program.header2">
		<module cmlx:templateRef="program.header" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
	        <scalar dataType="xsd:string" dictRef="cc:program">Molcas</scalar>
	        <scalar dataType="xsd:string" dictRef="cc:programVersion">8</scalar>
	        <scalar dataType="xsd:string" dictRef="cc:programSubversion">service pack 1</scalar>
      </module>	
	</comment>
	
	<comment class="example.output" id="program.header3">
	  <module cmlx:templateRef="program.header" xmlns="http://www.xml-cml.org/schema" xmlns:cmlx="http://www.xml-cml.org/schema/cmlx">
         <scalar dataType="xsd:string" dictRef="cc:programVersion">18.09</scalar>
         <scalar dataType="xsd:string" dictRef="cc:programSubversion">790-g23d48b3-dirty</scalar>
         <scalar dataType="xsd:string" dictRef="cc:program" id="program">Molcas</scalar>
      </module>	
	</comment>
	
</template>