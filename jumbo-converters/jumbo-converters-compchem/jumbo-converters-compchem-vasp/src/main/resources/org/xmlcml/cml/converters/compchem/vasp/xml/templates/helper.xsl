<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:foldl-func="foldl-func"
    xmlns:f="http://fxsl.sf.net/"    
    xmlns="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="xs f foldl-func"    
    version="2.0">
 
    <xsl:include href="f/sqrt.xsl"/>
    <xsl:include href="f/arcTrignm.xsl"/>
    
    <xsl:variable name="atomType2AtomicNumber">
        <entry key="H">1</entry>
        <entry key="He">2</entry>
        <entry key="Li">3</entry>
        <entry key="Be">4</entry>
        <entry key="B">5</entry>
        <entry key="C">6</entry>
        <entry key="N">7</entry>
        <entry key="O">8</entry>
        <entry key="F">9</entry>
        <entry key="Ne">10</entry>
        <entry key="Na">11</entry>
        <entry key="Mg">12</entry>
        <entry key="Al">13</entry>
        <entry key="Si">14</entry>
        <entry key="P">15</entry>
        <entry key="S">16</entry>
        <entry key="Cl">17</entry>
        <entry key="Ar">18</entry>
        <entry key="K">19</entry>
        <entry key="Ca">20</entry>
        <entry key="Sc">21</entry>
        <entry key="Ti">22</entry>
        <entry key="V">23</entry>
        <entry key="Cr">24</entry>
        <entry key="Mn">25</entry>
        <entry key="Fe">26</entry>
        <entry key="Co">27</entry>
        <entry key="Ni">28</entry>
        <entry key="Cu">29</entry>
        <entry key="Zn">30</entry>
        <entry key="Ga">31</entry>
        <entry key="Ge">32</entry>
        <entry key="As">33</entry>
        <entry key="Se">34</entry>
        <entry key="Br">35</entry>
        <entry key="Kr">36</entry>
        <entry key="Rb">37</entry>
        <entry key="Sr">38</entry>
        <entry key="Y">39</entry>
        <entry key="Zr">40</entry>
        <entry key="Nb">41</entry>
        <entry key="Mo">42</entry>
        <entry key="Tc">43</entry>
        <entry key="Ru">44</entry>
        <entry key="Rh">45</entry>
        <entry key="Pd">46</entry>
        <entry key="Ag">47</entry>
        <entry key="Cd">48</entry>
        <entry key="In">49</entry>
        <entry key="Sn">50</entry>
        <entry key="Sb">51</entry>
        <entry key="Te">52</entry>
        <entry key="I">53</entry>
        <entry key="Xe">54</entry>
        <entry key="Cs">55</entry>
        <entry key="Ba">56</entry>
        <entry key="Hf">72</entry>
        <entry key="Ta">73</entry>
        <entry key="W">74</entry>
        <entry key="Re">75</entry>
        <entry key="Os">76</entry>
        <entry key="Ir">77</entry>
        <entry key="Pt">78</entry>
        <entry key="Au">79</entry>
        <entry key="Hg">80</entry>
        <entry key="Tl">81</entry>
        <entry key="Pb">82</entry>
        <entry key="Bi">83</entry>
        <entry key="Po">84</entry>
        <entry key="At">85</entry>
        <entry key="Rn">86</entry>
        <entry key="Fr">87</entry>
        <entry key="Ra">88</entry>
        <entry key="Rf">104</entry>
        <entry key="Db">105</entry>
        <entry key="Sg">106</entry>
        <entry key="Bh">107</entry>
        <entry key="Hs">108</entry>
        <entry key="Mt">109</entry>
        <entry key="Ds">110</entry>
        <entry key="Rg">111</entry>
        <entry key="Cn">112</entry>
        <entry key="Uut">113</entry>
        <entry key="Uuq">114</entry>
        <entry key="Uup">115</entry>
        <entry key="Uuh">116</entry>
        <entry key="Uus">117</entry>
        <entry key="Uuo">118</entry>
    </xsl:variable>
    
    <!-- Convert atom types to it's correspoding atomic numbers -->
    <xsl:function name="helper:atomType2Number">
        <xsl:param name="atomType" as="xs:string"/>
        <xsl:sequence select="$atomType2AtomicNumber/entry[@key=$atomType]"/>                        
    </xsl:function>

    <!-- Given a vector, this function calculates it's lenght -->
    <xsl:function name="helper:calcAxisLength">        
        <xsl:param name="a"/>
        <xsl:variable name="result">
            <xsl:call-template name="sqrt">
                <xsl:with-param name="N" select="number($a[1])*number($a[1]) + number($a[2])*number($a[2]) + number($a[3])*number($a[3])"/>             
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="format-number($result,'#0.00000000')"/>        
    </xsl:function>
    
    <!-- Given two vectors, this function returns the angle between them in degrees (rounded to two decimals) -->  
    <xsl:function name="helper:calcAxesAngle">
        <xsl:param name="a"/>
        <xsl:param name="b"/>
        <xsl:variable name="aAxisLength" select="helper:calcAxisLength($a)"/>
        <xsl:variable name="bAxisLength" select="helper:calcAxisLength($b)"/>
     
        <xsl:variable name="result">            
            <xsl:call-template name="arccos">
                <xsl:with-param name="pX" select="((number($a[1]) * number($b[1])) + (number($a[2]) * number($b[2])) + (number($a[3]) * number($b[3]))) div ($aAxisLength * $bAxisLength)"/>
            </xsl:call-template>          
        </xsl:variable>
        <xsl:variable name="resultGrad" select="57.2957795 * $result"/>
        <xsl:value-of select="format-number( round($resultGrad*100) div 100 ,'#0.00')"/>
      
    </xsl:function>
    
    <!-- Convert fractionals coordinates to cartesian, using unit cell vectors and atom fractional atom coordinate -->
    <xsl:function name="helper:fractionalToCartesian" xml:space="default">
        <xsl:param name="a"/>
        <xsl:param name="b"/>
        <xsl:param name="c"/>        
        <xsl:param name="frac"/>
        <xsl:element name="coords" xml:space="default">
            <xsl:attribute name="x3" select="format-number((number($frac[1]) * number($a[1])) + (number($frac[2]) * number($b[1])) + (number($frac[3]) * number($c[1])),'#0.00000000')"/>
            <xsl:attribute name="y3" select="format-number((number($frac[1]) * number($a[2])) + (number($frac[2]) * number($b[2])) + (number($frac[3]) * number($c[2])),'#0.00000000')"/>
            <xsl:attribute name="z3" select="format-number((number($frac[1]) * number($a[3])) + (number($frac[2]) * number($b[3])) + (number($frac[3]) * number($c[3])),'#0.00000000')"/>
        </xsl:element>        
    </xsl:function>    
    
    <!-- Trim whitespaces from string -->
    <xsl:function name="helper:trim" as="xs:string">
        <xsl:param name="arg" as="xs:string?"/>        
        <xsl:sequence select="replace(replace($arg,'\s+$',''),'^\s+','')"/>        
    </xsl:function>
    
    <!-- This function reads a group of nodes which contain one or multiple text values, separated by blanks and builds an array element -->
    <xsl:function name="helper:arrayFromStringNodes">        
        <xsl:param name="dictRef"/>       
        <xsl:param name="dataType"/>
        <xsl:param name="nodes"/>                 
        
        <xsl:variable name="values">
            <xsl:for-each select="$nodes">
                <xsl:value-of select="tokenize(helper:trim(.),'\s+')"/><xsl:text> </xsl:text>                             
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:element name="array">           
            <xsl:attribute name="dataType"><xsl:value-of select="$dataType"/></xsl:attribute>
            <xsl:attribute name="dictRef"><xsl:value-of select="$dictRef"/></xsl:attribute>
            <xsl:attribute name="size"><xsl:value-of select="count(tokenize(helper:trim($values),'\s+'))"/></xsl:attribute>
            <xsl:value-of select="helper:trim($values)"/>           
        </xsl:element>        
    </xsl:function>
    
    <xsl:function name="helper:arrayFromStringNodesWithDelimiter">        
        <xsl:param name="dictRef"/>       
        <xsl:param name="dataType"/>
        <xsl:param name="nodes"/>          
        <xsl:param name="delimiter"/>
        <xsl:param name="delimiterRegExp"/>
       
    
        <xsl:variable name="values">
            <xsl:for-each select="$nodes">                
                <xsl:value-of select="helper:trim(.)"/><xsl:if test="position() != last()"><xsl:value-of select="$delimiter"/></xsl:if>                             
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:element name="array">
            <xsl:attribute name="delimiter"><xsl:value-of select="$delimiter"/></xsl:attribute>
            <xsl:attribute name="dataType"><xsl:value-of select="$dataType"/></xsl:attribute>
            <xsl:attribute name="dictRef"><xsl:value-of select="$dictRef"/></xsl:attribute>
            <xsl:attribute name="size"><xsl:value-of select="count(tokenize($values,$delimiterRegExp))"/></xsl:attribute>
            <xsl:value-of select="$values"/>           
        </xsl:element>        
    </xsl:function>        
    
    <!-- This function reads a group of nodes which contain one or multiple text values, separated by blanks and builds a matrix element -->
    <!-- It also calculates rows and cols, one such fields must be passed to the function, the other will be set to -1 -->
    <xsl:function name="helper:matrixFromStringNodes">       
        <xsl:param name="dictRef"/>        
        <xsl:param name="dataType"/>
        <xsl:param name="nodes"/>   
        <xsl:param name="refRows"/>
        <xsl:param name="refCols"/>    
        
        <xsl:variable name="values">
            <xsl:for-each select="$nodes">
                <xsl:value-of select="tokenize(helper:trim(.),'\s+')"/><xsl:text> </xsl:text>                             
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="rows">
            <xsl:choose>
                <xsl:when test="$refRows != -1">
                    <xsl:value-of select="$refRows"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="count(tokenize(helper:trim($values),'\s+')) div $refCols"/>
                </xsl:otherwise>
            </xsl:choose>                        
        </xsl:variable>
        
        <xsl:variable name="cols">
            <xsl:choose>
                <xsl:when test="$refCols != -1">
                    <xsl:value-of select="$refCols"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="count(tokenize(helper:trim($values),'\s+')) div $refRows"/>
                </xsl:otherwise>
            </xsl:choose>            
        </xsl:variable>
        <xsl:element name="matrix">
            <xsl:attribute name="dataType"><xsl:value-of select="$dataType"/></xsl:attribute>
            <xsl:attribute name="dictRef"><xsl:value-of select="$dictRef"/></xsl:attribute>
            <xsl:attribute name="rows"><xsl:value-of select="$rows"/></xsl:attribute>
            <xsl:attribute name="cols"><xsl:value-of select="$cols"/></xsl:attribute>            
            <xsl:value-of select="helper:trim($values)"/>           
        </xsl:element>
        
    </xsl:function>
    <xsl:function name="helper:readBoolean">
        <xsl:param name="value"/>
        <xsl:choose>
            <xsl:when test="compare(helper:trim($value), 'T') = 0">
                <xsl:sequence select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="helper:countNumberOfValues">
        <xsl:param name="value"/>
        <xsl:value-of select="count(tokenize(helper:trim($value),'\s+'))"/>       
    </xsl:function>
</xsl:stylesheet>