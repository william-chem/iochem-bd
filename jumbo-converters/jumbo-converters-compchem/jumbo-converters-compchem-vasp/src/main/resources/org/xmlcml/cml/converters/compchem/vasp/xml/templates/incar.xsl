<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
   
    <xsl:template match="./incar" >
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="incar">
                <incar>
                    <i type="int" name="ISTART">     1</i>
                    <i type="string" name="PREC">high</i>
                    <i type="string" name="GGA"> 91</i>
                    <i type="int" name="IALGO">    48</i>
                    <i type="int" name="ISPIN">     1</i>
                    <i type="int" name="ICHARG">     1</i>
                    <i type="int" name="INIWAV">     1</i>
                    <i type="int" name="NELM">   999</i>
                    <i type="int" name="NELMIN">     2</i>
                    <i name="EDIFF">      0.00000100</i>
                    <i name="EDIFFG">      0.00001000</i>
                    <i type="int" name="IBRION">     5</i>
                    <i type="int" name="NSW">   171</i>
                    <i type="int" name="IWAVPR">    11</i>
                    <i type="int" name="ISYM">     0</i>
                    <i name="ENCUT">    415.00000000</i>
                    <i name="POTIM">      0.01000000</i>
                    <i type="string" name="LREAL"> Auto</i>
                    <i type="int" name="ISMEAR">     0</i>
                    <i name="SIGMA">      0.02000000</i>
                    <i type="int" name="NWRITE">     2</i>
                    <i type="logical" name="LELF"> T  </i>
                    <i type="logical" name="LDAU"> F  </i>
                    <i name="EDIFF">      0.00001000</i>
  					<i name="EDIFFG">      0.00010000</i>  
                    <i name="POTIM">      0.05000000</i>
                    <i type="logical" name="LDIPOL"> T  </i>
                    <i type="int" name="IDIPOL">     3</i>                                  
                </incar>
            </comment>
        </xsl:if>  
                
        <module id="incar" cmlx:templateRef="incar">
            <xsl:if test="exists(./i[@name='IBRION'])">
                <scalar dataType="xsd:integer" dictRef="v:ibrion"><xsl:value-of select="helper:trim(./i[@name='IBRION'])"/></scalar>    
            </xsl:if>
            <xsl:if test="exists(./i[@name='ISPIN'])">
                <scalar dataType="xsd:integer" dictRef="v:ispin"><xsl:value-of select="helper:trim(./i[@name='ISPIN'])"/></scalar>
            </xsl:if>           
            <xsl:if test="exists(./i[@name='ENCUT'])">
                <scalar dataType="xsd:double" dictRef="v:energyCutOff" units="nonsi:electronvolt"><xsl:value-of select="helper:trim(./i[@name='ENCUT'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='GGA'])">
                <scalar dataType="xsd:string" dictRef="v:gga"><xsl:value-of select="helper:trim(./i[@name='GGA'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='LDAU'])">
                <scalar dataType="xsd:string" dictRef="v:ldau"><xsl:value-of select="helper:readBoolean(./i[@name='LDAU'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='EDIFF'])">
                <scalar dataType="xsd:double" dictRef="v:ediff"><xsl:value-of select="helper:trim(./i[@name='EDIFF'])"/></scalar>                
            </xsl:if>           
            <xsl:if test="exists(./i[@name='EDIFFG'])">
                <scalar dataType="xsd:double" dictRef="v:ediffg"><xsl:value-of select="helper:trim(./i[@name='EDIFFG'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='POTIM'])">
                <scalar dataType="xsd:double" dictRef="v:potim"><xsl:value-of select="helper:trim(./i[@name='POTIM'])"/></scalar>                
            </xsl:if>            
            <xsl:if test="exists(./i[@name='LDIPOL'])">
                <scalar dataType="xsd:string" dictRef="v:ldipol"><xsl:value-of select="helper:readBoolean(./i[@name='LDIPOL'])"/></scalar>                
            </xsl:if>
            <xsl:if test="exists(./i[@name='IDIPOL'])">
                <scalar dataType="xsd:integer" dictRef="v:idipol"><xsl:value-of select="helper:trim(./i[@name='IDIPOL'])"/></scalar>                
            </xsl:if>
            
        </module>
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="incar">
                <module id="incar" cmlx:templateRef="incar">
                    <scalar dataType="xsd:integer" dictRef="v:ibrion">5</scalar>
                    <scalar dataType="xsd:integer" dictRef="v:ispin">1</scalar>
                    <scalar dataType="xsd:double" dictRef="v:energyCutOff" units="nonsi:electronvolt">415.00000000</scalar>
                    <scalar dataType="xsd:string" dictRef="v:gga">91</scalar>                    
                    <scalar dataType="xsd:string" dictRef="v:ldau">false</scalar>
                    <scalar dataType="xsd:double" dictRef="v:ediff">0.00000100</scalar>                                        
                    <scalar dataType="xsd:double" dictRef="v:eddifg">-0.01000000</scalar>
                    <scalar dataType="xsd:double" dictRef="v:potim">0.05000000</scalar>                    
                    <scalar dataType="xsd:string"  dictRef="v:ldipol">true</scalar>
                    <scalar dataType="xsd:integer" dictRef="v:idipol">3</scalar>
                </module>
            </comment>
        </xsl:if>  
    </xsl:template>
</xsl:stylesheet>