<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <xsl:template match="./separator[@name='electronic']" mode="parameters">
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="electronic">
                <separator name="electronic" >
                    <i type="string" name="PREC">normal</i>
                    <i name="ENMAX">    500.00000000</i>
                    <i name="ENAUG">    644.87300000</i>
                    <i name="EDIFF">      0.00001000</i>
                    <i type="int" name="IALGO">    38</i>
                    <i type="int" name="IWAVPR">    11</i>
                    <i type="int" name="NBANDS">   192</i>
                    <i name="NELECT">    310.00000000</i>
                    <i type="int" name="TURBO">     0</i>
                    <i type="int" name="IRESTART">     0</i>
                    <i type="int" name="NREBOOT">     0</i>
                    <i type="int" name="NMIN">     0</i>
                    <i name="EREF">      0.00000000</i>
                    ...
                </separator>
            </comment>            
        </xsl:if>
        
        <module id="electronic" cmlx:templateRef="electronic">
            <xsl:if test="exists(./i[@name='NELECT'])">
                <scalar type="xsd:double" dictRef="v:nelect"><xsl:value-of select="helper:trim(./i[@name='NELECT'])"/></scalar>
            </xsl:if>
            <xsl:apply-templates mode="parameters.electronic"/>
        </module>
           
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="electronic">
                <module xmlns="" id="electronic" cmlx:templateRef="electronic">
                    <scalar type="xsd:double" dictRef="v:nelect">310.00000000</scalar>
                </module>                                    
            </comment>
        </xsl:if>
            
    </xsl:template>
    <xsl:include href="electronic/electronic.smearing.xsl"/>    
    <xsl:include href="electronic/electronic.spin.xsl"/>
 
    <!-- Override default templates -->
    <xsl:template match="text()" mode="parameters.electronic"/>
    <xsl:template match="*" mode="parameters.electronic"/>  
    
</xsl:stylesheet>