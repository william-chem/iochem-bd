<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:template match="./calculation">
        <module id="calculation" cmlx:templateRef="calculation">        
            <xsl:apply-templates mode="calculation"/>
        </module>
    </xsl:template>
    
    <xsl:include href="calculation/eigenvalues.xsl"/>
    <xsl:include href="calculation/dos.xsl"/>
    
    <!-- Override default templates -->
    <xsl:template match="text()" mode="calculation"/>
    <xsl:template match="*" mode="calculation"/>
    
</xsl:stylesheet>