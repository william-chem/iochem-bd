<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs"
    version="2.0">
    <xsl:template match="./xpath_expression" >
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="XXXX">
            
            </comment>
        </xsl:if>  
        
        
        
        
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="XXXX">
                
            </comment>
        </xsl:if>  
    </xsl:template>
</xsl:stylesheet>