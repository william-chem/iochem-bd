<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="xs helper"
    version="2.0">
    
     
    <xsl:template match="./atominfo">
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <module class="example.input" id="atominfo">
                <array name="atomtypes" >
                    <dimension dim="1">type</dimension>
                    <field type="int">atomspertype</field>
                    <field type="string">element</field>
                    <field>mass</field>
                    <field>valence</field>
                    <field type="string">pseudopotential</field>
                    <set>
                        <rc><c>  12</c><c>Ce</c><c>    140.11500000</c><c>     12.00000000</c><c>  PAW_PBE Ce 23Dec2003                  </c></rc>
                        <rc><c>  24</c><c>O </c><c>     16.00000000</c><c>      6.00000000</c><c>  PAW_PBE O 08Apr2002                   </c></rc>
                        <rc><c>   3</c><c>C </c><c>     12.01100000</c><c>      4.00000000</c><c>  PAW_PBE C 08Apr2002                   </c></rc>
                        <rc><c>  10</c><c>H </c><c>      1.00000000</c><c>      1.00000000</c><c>  PAW_PBE H 15Jun2001                   </c></rc>
                    </set>
                </array>
            </module>            
        </xsl:if>
        
        <module id="atominfo" cmlx:templateRef="atominfo">
            <module id="atomtypes" cmlx:templateRef="atomtypes">
                <xsl:copy-of select="helper:arrayFromStringNodes('v:atomsPerType','xsd:double',./array[@name='atomtypes']/set/rc/c[1])"/>
                <xsl:copy-of select="helper:arrayFromStringNodes('cc:element','xsd:string',./array[@name='atomtypes']/set/rc/c[2])"/>
                <xsl:copy-of select="helper:arrayFromStringNodes('cc:mass','xsd:double',./array[@name='atomtypes']/set/rc/c[3])"/>
                <xsl:copy-of select="helper:arrayFromStringNodes('cc:valence','xsd:double',./array[@name='atomtypes']/set/rc/c[4])"/>
                <xsl:copy-of select="helper:arrayFromStringNodesWithDelimiter('v:pseudopotential','xsd:string',./array[@name='atomtypes']/set/rc/c[5],'|','\|')"/>                
            </module>           
        </module>
        
        <xsl:if test="$isVerbose">
            <module id="atominfo" cmlx:templateRef="atominfo">
                <module id="atomtypes" cmlx:templateRef="atomtypes">
                    <array dataType="xsd:double" dictRef="v:atomsPerType" size="4">12 24 3 10</array>
                    <array dataType="xsd:string" dictRef="cc:element" size="4">Ce O C H</array>
                    <array dataType="xsd:double" dictRef="cc:mass" size="4">140.11500000 16.00000000 12.01100000 1.00000000</array>
                    <array dataType="xsd:double" dictRef="cc:valence" size="4">12.00000000 6.00000000 4.00000000 1.00000000</array>
                    <array delimiter="|" dataType="xsd:string" dictRef="v:pseudopotential" size="4">PAW_PBE Ce 23Dec2003|PAW_PBE O 08Apr2002|PAW_PBE C 08Apr2002|PAW_PBE H 15Jun2001</array>
                </module>
            </module>            
        </xsl:if>
    </xsl:template>
</xsl:stylesheet>