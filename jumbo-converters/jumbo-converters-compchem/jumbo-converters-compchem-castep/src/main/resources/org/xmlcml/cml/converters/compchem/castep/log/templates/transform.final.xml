<!--

    jumbo-converters - Conversion templates used for format conversions by the jumbo-saxon project.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<transformList xmlns:xi="http://www.w3.org/2001/XInclude" >

<!--  ====================== FINALIZATION ===================== -->
    
  <!-- build finalization module -->
  
  <transform process="addChild" xpath="//cml:module[@id='job']" elementName="cml:module" id="finalization"/>
  <transform process="addDictRef" xpath="//cml:module[@id='finalization']" value="cc:finalization"/>  
  <transform process="addChild" xpath="//cml:module[@id='finalization']" elementName="cml:propertyList" />
  
  <!--  geometry modules -->
  
  <transform process="copy" xpath="/cml:module/cml:module[@id='external']/cml:module[@id='castep.geom']//cml:molecule[@id='final']" 
		to="//cml:module[@dictRef='cc:job']/cml:module[@id='finalization']"/>
		
  <transform process="copy" xpath="(/cml:module/cml:module[@id='jobList1']/cml:module[@cmlx:templateRef='job']/cml:module[@cmlx:templateRef='setup']/cml:molecule[@id='coordinates'])[1]" 
    to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']"/>

  <transform process="addAttribute" xpath="//cml:module[@dictRef='cc:job']/cml:module[@id='finalization']/cml:molecule"  
    name="id" value="final" />
	
  <!-- If working with geometry optimizations, must provide the .geom file, otherwise this action deletes all molecules and breaks the upload -->
  <!-- Feature disabled, we will allow upload but will mark geometry as not converged --> 
<!--
  <transform process="delete" xpath="/cml:module[
										exists(//cml:parameter[child::scalar[@dictRef='x:value' and matches(text(),'geometry optimization')]]) 
										and not(exists(./cml:module[@id='external' and child::cml:module[@id='castep.geom']]))]
  									//cml:molecule" />
	-->
  <transform process="addAttribute" xpath="/cml:module[
										exists(//cml:parameter[child::cml:scalar[@dictRef='x:value' and matches(text(),'geometry optimization')]]) 
										and not(exists(./cml:module[@id='external' and child::cml:module[@id='castep.geom']]))]
  									//cml:molecule[@id='final']" 
  		name="id" value="not.converged"	/>


  <!-- move properties to finalization -->
  <transform process="addChild" xpath="//cml:module[@id='finalization']" elementName="cml:module" id="otherComponents"/>
  <transform process="addDictRef" xpath=".//cml:module[@id='finalization']/cml:module[@id='otherComponents']" value="cc:userDefinedModule" />

  <transform process="moveRelative" xpath="//cml:module[@id='external']/cml:module[@id='castep.chart']/cml:module[@id='chart']"
    	to="//cml:module[@id='job']/cml:module[@id='finalization']/cml:module[@id='otherComponents']"/>

  <transform process="moveRelative" xpath=".//cml:module[@dictRef='cc:job']/cml:module[@dictRef='cc:calculation']/cml:module[@cmlx:templateRef='step']/cml:module[@cmlx:templateRef='scf']//cml:scalar[dictRef='cc:scfConverged']" 
  		to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:propertyList"/>

  <transform process="moveRelative" xpath=".//cml:module[@dictRef='cc:job']/cml:module[@cmlx:templateRef='timing']/cml:scalar" 
  		to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:propertyList"/>

  <transform process="moveRelative" xpath=".//cml:module[@dictRef='cc:job']/cml:module[@cmlx:templateRef='final']/cml:module[@cmlx:templateRef='enthalpy']//cml:scalar" 
  		to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:propertyList"/>
  		
  		
  <!-- The final energy is retrieved from .castep file instead from the .geom -->
  <!--   <transform process="moveRelative" xpath="(/cml:module/cml:module[@id='external']/cml:module[@id='castep.geom']//cml:scalar[@dictRef='cc:finalEnergy'])[last()]" -->
  <!-- 		to="//cml:module[@id='job']/cml:module[@id='finalization']/cml:propertyList"/>	 -->

  <transform process="copy" xpath="(.//cml:module[@dictRef='cc:job']/cml:module[@dictRef='cc:calculation']//cml:module[@cmlx:templateRef='step']/cml:module[@cmlx:templateRef='scf']//cml:scalar[@dictRef='cc:finalEnergy'])[last()]"
  		to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:propertyList"/>
  <transform process="copy" xpath="(//cml:module[@dictRef='cc:job']/cml:module[@dictRef='cc:calculation']//cml:module[@cmlx:templateRef='step']/cml:module[@cmlx:templateRef='scf']//cml:scalar[@dictRef='cc:freeEnergy'])[last()]"
  		to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:propertyList"/>

 		
  <transform process="moveRelative"
   		xpath=".//cml:module[@dictRef='cc:job']/cml:module[@cmlx:templateRef='final']/cml:module[@cmlx:templateRef='mulliken']"
    	to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:module[@id='otherComponents']"/>

  <transform process="moveRelative"
   		xpath=".//cml:module[@dictRef='cc:job']/cml:module[@cmlx:templateRef='final']/cml:module[@cmlx:templateRef='cell']"
    	to="./ancestor::*[@dictRef='cc:job']/cml:module[@id='finalization']/cml:module[@id='otherComponents']"/>

  <!-- wrap all properties -->
  <transform process="createWrapperProperty" xpath="//cml:module[@id='finalization']/cml:propertyList/cml:*" />
  <transform process="createWrapper" xpath="//cml:module[@id='finalization']/cml:propertyList/cml:module" elementName="cml:property"/>

</transformList>
