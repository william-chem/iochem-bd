<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:foldl-func="foldl-func"
    xmlns:f="http://fxsl.sf.net/"    
    xmlns="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="xs f foldl-func"    
    version="2.0">

    <xsl:include href="f/sqrt.xsl"/>
    <xsl:include href="f/arcTrignm.xsl"/>
    <xsl:include href="f/trignm.xsl"/>    
    
    <xsl:variable name="CONVERT_UNITS">        
        <entry from="nonsi:bohr" to="nonsi:angstrom">0.52918</entry>             
        <entry from="nonsi:angstrom" to="nonsi:bohr">1.88971616463207</entry>
    </xsl:variable>
    
    <xsl:variable name="CONVERT_QEX_UNITS">
        <entry qeunits="BOHR" cmlunits="nonsi:bohr"/>
        <entry qeunits="2 PI / A" cmlunits="nonsi2:2.pi.a-1"/>
        <entry qeunits="A.M.U." cmlunits="nonsi:dalton"/>
        <entry qeunits="CRYSTAL" cmlunits="nonsi2:crystal"/>
        <entry qeunits="HARTREE" cmlunits="nonsi:hartree"/>
    </xsl:variable>
    
    <xsl:variable name="PI" select="3.14159265359"/>
        
      
    <xsl:variable name="atomType2AtomicNumber">
        <entry key="H">1</entry>
        <entry key="He">2</entry>
        <entry key="Li">3</entry>
        <entry key="Be">4</entry>
        <entry key="B">5</entry>
        <entry key="C">6</entry>
        <entry key="N">7</entry>
        <entry key="O">8</entry>
        <entry key="F">9</entry>
        <entry key="Ne">10</entry>
        <entry key="Na">11</entry>
        <entry key="Mg">12</entry>
        <entry key="Al">13</entry>
        <entry key="Si">14</entry>
        <entry key="P">15</entry>
        <entry key="S">16</entry>
        <entry key="Cl">17</entry>
        <entry key="Ar">18</entry>
        <entry key="K">19</entry>
        <entry key="Ca">20</entry>
        <entry key="Sc">21</entry>
        <entry key="Ti">22</entry>
        <entry key="V">23</entry>
        <entry key="Cr">24</entry>
        <entry key="Mn">25</entry>
        <entry key="Fe">26</entry>
        <entry key="Co">27</entry>
        <entry key="Ni">28</entry>
        <entry key="Cu">29</entry>
        <entry key="Zn">30</entry>
        <entry key="Ga">31</entry>
        <entry key="Ge">32</entry>
        <entry key="As">33</entry>
        <entry key="Se">34</entry>
        <entry key="Br">35</entry>
        <entry key="Kr">36</entry>
        <entry key="Rb">37</entry>
        <entry key="Sr">38</entry>
        <entry key="Y">39</entry>
        <entry key="Zr">40</entry>
        <entry key="Nb">41</entry>
        <entry key="Mo">42</entry>
        <entry key="Tc">43</entry>
        <entry key="Ru">44</entry>
        <entry key="Rh">45</entry>
        <entry key="Pd">46</entry>
        <entry key="Ag">47</entry>
        <entry key="Cd">48</entry>
        <entry key="In">49</entry>
        <entry key="Sn">50</entry>
        <entry key="Sb">51</entry>
        <entry key="Te">52</entry>
        <entry key="I">53</entry>
        <entry key="Xe">54</entry>
        <entry key="Cs">55</entry>
        <entry key="Ba">56</entry>
        <entry key="Hf">72</entry>
        <entry key="Ta">73</entry>
        <entry key="W">74</entry>
        <entry key="Re">75</entry>
        <entry key="Os">76</entry>
        <entry key="Ir">77</entry>
        <entry key="Pt">78</entry>
        <entry key="Au">79</entry>
        <entry key="Hg">80</entry>
        <entry key="Tl">81</entry>
        <entry key="Pb">82</entry>
        <entry key="Bi">83</entry>
        <entry key="Po">84</entry>
        <entry key="At">85</entry>
        <entry key="Rn">86</entry>
        <entry key="Fr">87</entry>
        <entry key="Ra">88</entry>
        <entry key="Rf">104</entry>
        <entry key="Db">105</entry>
        <entry key="Sg">106</entry>
        <entry key="Bh">107</entry>
        <entry key="Hs">108</entry>
        <entry key="Mt">109</entry>
        <entry key="Ds">110</entry>
        <entry key="Rg">111</entry>
        <entry key="Cn">112</entry>
        <entry key="Uut">113</entry>
        <entry key="Uuq">114</entry>
        <entry key="Uup">115</entry>
        <entry key="Uuh">116</entry>
        <entry key="Uus">117</entry>
        <entry key="Uuo">118</entry>
    </xsl:variable>
    
    <!-- Convert atom types to it's correspoding atomic numbers -->
    <xsl:function name="helper:atomType2Number">
        <xsl:param name="atomType" as="xs:string"/>
        <xsl:sequence select="$atomType2AtomicNumber/entry[@key=$atomType]"/>                        
    </xsl:function>

    <!-- Given a vector, this function calculates it's lenght -->
    <xsl:function name="helper:calcAxisLength">        
        <xsl:param name="a"/>
        <xsl:variable name="result">
            <xsl:call-template name="sqrt">
                <xsl:with-param name="N" select="number($a[1])*number($a[1]) + number($a[2])*number($a[2]) + number($a[3])*number($a[3])"/>             
            </xsl:call-template>
        </xsl:variable>
        <xsl:value-of select="format-number($result,'#0.00000000')"/>        
    </xsl:function>
    
    <!-- Given two vectors, this function returns the angle between them in degrees (rounded to two decimals) -->  
    <xsl:function name="helper:calcAxesAngle">
        <xsl:param name="a"/>
        <xsl:param name="b"/>
        <xsl:variable name="aAxisLength" select="helper:calcAxisLength($a)"/>
        <xsl:variable name="bAxisLength" select="helper:calcAxisLength($b)"/>
     
        <xsl:variable name="result">            
            <xsl:call-template name="arccos">
                <xsl:with-param name="pX" select="((number($a[1]) * number($b[1])) + (number($a[2]) * number($b[2])) + (number($a[3]) * number($b[3]))) div ($aAxisLength * $bAxisLength)"/>
            </xsl:call-template>          
        </xsl:variable>
        <xsl:variable name="resultGrad" select="57.2957795 * $result"/>
        <xsl:value-of select="format-number( round($resultGrad*100) div 100 ,'#0.00')"/>
      
    </xsl:function>
    
    <!-- Convert fractional coordinates to cartesian, using unit cell vectors and atom fractional atom coordinate -->
    <xsl:function name="helper:fractionalToCartesian" xml:space="default">
        <xsl:param name="a"/>
        <xsl:param name="b"/>
        <xsl:param name="c"/>        
        <xsl:param name="frac"/>
        <xsl:element name="coords" xml:space="default">
            <xsl:attribute name="x3" select="format-number((number($frac[1]) * number($a[1])) + (number($frac[2]) * number($b[1])) + (number($frac[3]) * number($c[1])),'#0.00000000')"/>
            <xsl:attribute name="y3" select="format-number((number($frac[1]) * number($a[2])) + (number($frac[2]) * number($b[2])) + (number($frac[3]) * number($c[2])),'#0.00000000')"/>
            <xsl:attribute name="z3" select="format-number((number($frac[1]) * number($a[3])) + (number($frac[2]) * number($b[3])) + (number($frac[3]) * number($c[3])),'#0.00000000')"/>
        </xsl:element>        
    </xsl:function>    
    
    
    <!-- Convert cartesian coordinates to fractional, using unit cell vectors and atom cartesian coordinates. Sources:  
            https://en.wikipedia.org/wiki/Fractional_coordinates
            https://ipfs.io/ipfs/QmXoypizjW3WknFiJnKLwHCnL72vedxjQkDDP1mXWo6uco/wiki/Fractional_coordinates.html
            https://pymolwiki.org/index.php/Cart_to_frac        
         Test conversion on:
            https://cci.lbl.gov/cctbx/frac_cart.html
    -->
    
    <xsl:function name="helper:cartesian2fractional" xml:space="default">
        <xsl:param name="a"/>
        <xsl:param name="b"/>
        <xsl:param name="c"/>
        <xsl:param name="alphaDegree"/>
        <xsl:param name="betaDegree"/>
        <xsl:param name="gammaDegree"/>        
        <xsl:param name="coords"/>
                
        <xsl:variable name="alpha" select="$alphaDegree * $PI div 180.0"/>
        <xsl:variable name="beta" select="$betaDegree * $PI div 180.0"/>
        <xsl:variable name="gamma" select="$gammaDegree * $PI div 180.0"/>
        
        <xsl:variable name="cosa" select="helper:cos($alpha)"/>        
        <xsl:variable name="sina" select="helper:sin($alpha)"/>
        <xsl:variable name="cosb" select="helper:cos($beta)"/>        
        <xsl:variable name="sinb" select="helper:sin($beta)"/>
        <xsl:variable name="cosg" select="helper:cos($gamma)"/>
        <xsl:variable name="sing" select="helper:sin($gamma)"/>        
                        
        <xsl:variable name="volume" select="helper:sqrt(1 - $cosa * $cosa - $cosb * $cosb - $cosg * $cosg + 2 * $cosa * $cosb * $cosg)"/>
       
        <!-- Matrix in form of variables -->
        <xsl:variable name="d1_1" select="1.0 div $a"/>
        <xsl:variable name="d1_2" select="-$cosg div ($a*$sing)"/>
        <xsl:variable name="d1_3" select="($cosa * $cosg - $cosb) div ($a * $volume * $sing)"/>
        <xsl:variable name="d2_1" select="0.0"/>
        <xsl:variable name="d2_2" select="1.0 div ($b * $sing)"/>
        <xsl:variable name="d2_3" select="($cosb * $cosg - $cosa) div ($b * $volume * $sing)"/>
        <xsl:variable name="d3_1" select="0.0"/>
        <xsl:variable name="d3_2" select="0.0"/>
        <xsl:variable name="d3_3" select="$sing div ($c * $volume)"/>                
                
        <!-- Alternative calculus of fractional coordinates            
        <xsl:variable name="volume" select=" $a * $b * $c * helper:sqrt(1 - $cosa * $cosa - $cosb * $cosb - $cosg * $cosg + 2 * $cosa * $cosb * $cosg) "/>
        <xsl:variable name="d1_1" select="1.0 div $a"/>
        <xsl:variable name="d1_2" select="-$cosg div ($a * $sing)"/>
        <xsl:variable name="d1_3" select="((($b * $cosg * $c * ($cosa - ($cosb * $cosg))) div $sing)- $b * $c * $cosb * $sing) * (1 div $volume)"/>
        <xsl:variable name="d2_1" select="0.0"/>
        <xsl:variable name="d2_2" select="1.0 div ($b * $sing)"/>
        <xsl:variable name="d2_3" select="-(($a * $c * ($cosa - ($cosb * $cosg))) div ($volume * $sing))"/>
        <xsl:variable name="d3_1" select="0.0"/>
        <xsl:variable name="d3_2" select="0.0"/>
        <xsl:variable name="d3_3" select="$a * $b * ($sing div $volume)"/>         
        -->
        
        <xsl:variable name="x" select="number($coords[1])"/>
        <xsl:variable name="y" select="number($coords[2])"/>
        <xsl:variable name="z" select="number($coords[3])"/>
        
        <xsl:value-of select="format-number($d1_1 * $x + $d1_2 * $y + $d1_3 * $z, '#0.00000000')"/>
        <xsl:value-of select="format-number($d2_1 * $x + $d2_2 * $y + $d2_3 * $z, '#0.00000000')"/>
        <xsl:value-of select="format-number($d3_1 * $x + $d3_2 * $y + $d3_3 * $z, '#0.00000000')"/>        

    </xsl:function>
    
    <!-- Trim whitespaces from string -->
    <xsl:function name="helper:trim" as="xs:string">
        <xsl:param name="arg" as="xs:string?"/>        
        <xsl:sequence select="replace(replace($arg,'\s+$',''),'^\s+','')"/>        
    </xsl:function>

    <!-- Function that reads a QEXML element text node and converts it into a CML scalar -->
    <xsl:function name="helper:toText">
        <xsl:param name="text" as="xs:string"/>
        <xsl:param name="currentUnits"/>
        <xsl:param name="newUnits" as="xs:string"/>
        <xsl:param name="unitMask" as="xs:string"/>    
        
        <xsl:variable name="units">
            <xsl:if test="$currentUnits != ''">
                <xsl:value-of select="helper:normalizeUnits($currentUnits)"/>
            </xsl:if>
        </xsl:variable>
        
        <xsl:value-of select="helper:convertDataValues(helper:trim($text), '', $units, $newUnits, $unitMask)"/>        
    </xsl:function>
    
    
    
    <xsl:function name="helper:toScalar">
        <xsl:param name="dictRef"/>
        <xsl:param name="node" as="node()"/>        
        <xsl:copy-of select="helper:toScalar($dictRef, $node, '', '', '')"/>
    </xsl:function>
    
    <xsl:function name="helper:toScalar">
        <xsl:param name="dictRef"/>
        <xsl:param name="node"/>
        <xsl:param name="currentUnits"/>
        <xsl:copy-of select="helper:toScalar($dictRef, $node, $currentUnits, '', '')"/>
    </xsl:function>

    <xsl:function name="helper:toScalar">
        <xsl:param name="dictRef"/>
        <xsl:param name="node"/>
        <xsl:param name="currentUnits"/>
        <xsl:param name="newUnits" as="xs:string"/>
        <xsl:param name="unitMask" as="xs:string"/>

        <xsl:variable name="units">
            <xsl:if test="$currentUnits != ''">
                <xsl:value-of select="helper:normalizeUnits($node/preceding-sibling::node()[name() = $currentUnits]/@UNITS)"/>
            </xsl:if>
        </xsl:variable>
        
        <xsl:variable name="dataType" select="helper:convertDataType($node/@type)"/>
        <xsl:variable name="values">            
            <xsl:value-of select="helper:convertDataValues(helper:trim($node/text()), $dataType, $units, $newUnits, $unitMask)"/>
        </xsl:variable>

        <xsl:element name="scalar">            
            <xsl:attribute name="dataType"><xsl:value-of select="$dataType"/></xsl:attribute>
            <xsl:attribute name="dictRef"><xsl:value-of select="$dictRef"/></xsl:attribute>                                                
            <xsl:if test="$units != ''">
                <xsl:attribute name="units"><xsl:value-of select="$units"/></xsl:attribute>
            </xsl:if>
            <xsl:value-of select="$values"/>
        </xsl:element>
    </xsl:function>
    
    <!-- Function that reads a QEXML element text node and converts it into a CML array, there are multiple call options, reading without units, with units, with units and changing such units -->
    <xsl:function name="helper:toArray">
        <xsl:param name="dictRef"/>
        <xsl:param name="node"/>            
        <xsl:copy-of select="helper:toArray($dictRef,$node, '', '','')"/>
    </xsl:function>
    
    <xsl:function name="helper:toArray">
        <xsl:param name="dictRef"/>
        <xsl:param name="node"/>
        <xsl:param name="currentUnits"/>    
        <xsl:copy-of select="helper:toArray($dictRef,$node, $currentUnits, '','')"/>
    </xsl:function>
   
    <xsl:function name="helper:toArray">
        <xsl:param name="dictRef"/>
        <xsl:param name="node"/>
        <xsl:param name="currentUnits"/>
        <xsl:param name="newUnits" as="xs:string"/>
        <xsl:param name="unitMask" as="xs:string"/>

        <xsl:variable name="units">
            <xsl:if test="$currentUnits != ''">
                <xsl:value-of select="helper:normalizeUnits($currentUnits)"/>                
            </xsl:if>
        </xsl:variable>
     
        <xsl:variable name="dataType" select="helper:convertDataType($node/@type)"/>
        <xsl:variable name="values">            
            <xsl:value-of select="helper:convertDataValues(tokenize(helper:trim($node/text()),'\s+'), $dataType, $units, $newUnits, $unitMask)"/>
        </xsl:variable>
               
        <xsl:element name="array">            
            <xsl:attribute name="dataType"><xsl:value-of select="$dataType"/></xsl:attribute>
            <xsl:attribute name="dictRef"><xsl:value-of select="$dictRef"/></xsl:attribute>
            <xsl:attribute name="size"><xsl:value-of select="$node/@size"/></xsl:attribute>                                    
            <xsl:if test="$units != '' and $newUnits != ''"><xsl:attribute name="units"><xsl:value-of select="$newUnits"/></xsl:attribute></xsl:if>
            <xsl:if test="$units != '' and $newUnits = ''"><xsl:attribute name="units"><xsl:value-of select="$units"/></xsl:attribute></xsl:if>            
            <xsl:value-of select="$values"/>
        </xsl:element> 
    </xsl:function>
   
    <!-- This function reads an QEXML element text node and converts it into a CML matrix -->
    <xsl:function name="helper:toMatrix">
        <xsl:param name="dictRef"/> 
        <xsl:param name="node"/>        
        <xsl:copy-of select="helper:toMatrix($dictRef, $node,'' ,'' ,'')"/>
    </xsl:function>

    <xsl:function name="helper:toMatrix">
        <xsl:param name="dictRef"/> 
        <xsl:param name="node"/>
        <xsl:param name="currentUnits"/>        
        <xsl:copy-of select="helper:toMatrix($dictRef, $node, $currentUnits, '', '')"/>
    </xsl:function>
    
    <xsl:function name="helper:toMatrix">
        <xsl:param name="dictRef"/>
        <xsl:param name="node"/>
        <xsl:param name="currentUnits"/>
        <xsl:param name="newUnits" as="xs:string"/>
        <xsl:param name="unitMask" as="xs:string"/>

        <xsl:variable name="units">
            <xsl:if test="$currentUnits != ''">
                <xsl:value-of select="helper:normalizeUnits($node/preceding-sibling::node()[name() = $currentUnits]/@UNITS)"/>
            </xsl:if>
        </xsl:variable>

        <xsl:variable name="dataType" select="helper:convertDataType($node/@type)"/>
        <xsl:variable name="values">            
            <xsl:value-of select="helper:convertDataValues(tokenize(helper:trim($node/text()),'\s+'), $dataType, $units, $newUnits, $unitMask)"/>
        </xsl:variable>
        <xsl:variable name="rows" select="$node/@size div $node/@columns"/>       
    
        <xsl:element name="matrix">            
            <xsl:attribute name="dataType"><xsl:value-of select="$dataType"/></xsl:attribute>
            <xsl:attribute name="dictRef"><xsl:value-of select="$dictRef"/></xsl:attribute>
            <xsl:attribute name="rows"><xsl:value-of select="$rows"/></xsl:attribute>
            <xsl:attribute name="cols"><xsl:value-of select="$node/@columns"/></xsl:attribute>
            <xsl:if test="$units != '' and $newUnits != ''"><xsl:attribute name="units"><xsl:value-of select="$newUnits"/></xsl:attribute></xsl:if>
            <xsl:if test="$units != '' and $newUnits = ''"><xsl:attribute name="units"><xsl:value-of select="$units"/></xsl:attribute></xsl:if>
            <xsl:value-of select="$values"/>
        </xsl:element>
    </xsl:function>
        
    <xsl:function name="helper:convertDataType">
        <xsl:param name="datatype"/>                
        <xsl:choose>
            <xsl:when test="$datatype = 'logical'"><xsl:value-of select="'xsd:boolean'"/></xsl:when>
            <xsl:when test="$datatype = 'character'"><xsl:value-of select="'xsd:string'"/></xsl:when>
            <xsl:when test="$datatype = 'integer'"><xsl:value-of select="'xsd:integer'"/></xsl:when>
            <xsl:when test="$datatype = 'real'"><xsl:value-of select="'xsd:double'"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="'xsd:string'"/></xsl:otherwise>            
        </xsl:choose>             
    </xsl:function>
   
    <xsl:function name="helper:convertDataValues">
        <xsl:param name="values"/>
        <xsl:param name="dataType"/>       
        <xsl:copy-of select="helper:convertDataValues($values, $dataType,'', '', '')"></xsl:copy-of>
    </xsl:function>
    
    <xsl:function name="helper:convertDataValues">
        <xsl:param name="values"/>
        <xsl:param name="dataType"/>
        <xsl:param name="units"/>
        <xsl:param name="newUnits"/>
        <xsl:param name="unitsMask"/>

        <xsl:variable name="textValue">
            <!-- Convert QuantumEspresso formats to default CML -->
            <xsl:choose>
                <xsl:when test="$dataType = 'xsd:boolean'">
                    <xsl:for-each select="$values">
                        <xsl:variable name="value" select="."/>
                        <xsl:choose>
                            <xsl:when test="$value = 'T'"><xsl:value-of select="true()"/><xsl:text> </xsl:text></xsl:when>
                            <xsl:otherwise><xsl:value-of select="false()"/><xsl:text> </xsl:text></xsl:otherwise>
                        </xsl:choose>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <!-- If newUnits is defined we will convert its values -->
                    <xsl:choose>
                        <xsl:when test="$newUnits != ''">
                            <xsl:for-each select="$values">
                                <xsl:variable name="value" select="."/>
                                <xsl:value-of select="helper:changeUnits($value, $units, $newUnits, $unitsMask)"/><xsl:text> </xsl:text>
                            </xsl:for-each>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$values"/>
                        </xsl:otherwise>                        
                    </xsl:choose>       
                </xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        <xsl:value-of select="tokenize(helper:trim($textValue), '\s+')"/>        
    </xsl:function>
  
    <xsl:function name="helper:changeUnits">
        <xsl:param name="value"/>
        <xsl:param name="currentUnit"/>
        <xsl:param name="newUnit"/>        
        <xsl:copy-of select="helper:changeUnits($value, $currentUnit, $newUnit,'')"/>        
    </xsl:function>
  
    <xsl:function name="helper:changeUnits">
        <xsl:param name="value"/>
        <xsl:param name="currentUnit" as="xs:string"/>
        <xsl:param name="newUnit" as="xs:string"/>
        <xsl:param name="formatMask" as="xs:string"/>        
        <xsl:variable name="coefficient" select="$CONVERT_UNITS/*:entry[@from=$currentUnit and @to=$newUnit]/text()"/>                
        <xsl:choose>
            <xsl:when test="exists($coefficient)">
                <xsl:choose>
                    <xsl:when test="$formatMask != ''"><xsl:value-of select="format-number(number($value) * number($coefficient), $formatMask)"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="number($value) * number($coefficient)"/></xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise> <!-- In case we reach this point, we must add the apropriate entry element ion CONVERT_UNITS -->
        </xsl:choose>
    </xsl:function>
  
    <xsl:function name="helper:normalizeUnits">
        <xsl:param name="units"/>
        <xsl:variable name="filteredUnits" select="upper-case(helper:trim($units))"/>
        
        <xsl:choose>
            <xsl:when test="exists($CONVERT_QEX_UNITS/*[@qeunits = $filteredUnits])"><xsl:value-of select="$CONVERT_QEX_UNITS/node()[@qeunits=$filteredUnits]/@cmlunits"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="$units"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
  
    <xsl:function name="helper:countNumberOfValues">
        <xsl:param name="value"/>
        <xsl:value-of select="count(tokenize(helper:trim($value),'\s+'))"/>       
    </xsl:function>
    
    <xsl:function name="helper:sqrt">
        <xsl:param name="value"/>        
        <xsl:call-template name="sqrt">
            <xsl:with-param name="N" select="$value"/>
            <xsl:with-param name="Eps" select="0.00001"/>
        </xsl:call-template>       
    </xsl:function>
    
    <xsl:function name="helper:cos">
        <xsl:param name="value"/>        
        <xsl:call-template name="cos">
            <xsl:with-param name="pX" select="$value"/>
        </xsl:call-template>       
    </xsl:function>
    
    <xsl:function name="helper:sin">
        <xsl:param name="value"/>        
        <xsl:call-template name="sin">
            <xsl:with-param name="pX" select="$value"/>
        </xsl:call-template>       
    </xsl:function>
    
    
</xsl:stylesheet>