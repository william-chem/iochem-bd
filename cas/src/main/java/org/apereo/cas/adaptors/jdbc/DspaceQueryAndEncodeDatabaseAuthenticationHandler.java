/**
 * cas-overlay - ioChem-BD platform is a suite of web services aimed to manage Computational Chemistry and Material Science digital files and aid scientist in their research.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.apereo.cas.adaptors.jdbc;

import lombok.extern.slf4j.Slf4j;


import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.lang3.BooleanUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.crypto.hash.ConfigurableHashService;
import org.apache.shiro.crypto.hash.DefaultHashService;
import org.apache.shiro.crypto.hash.HashRequest;
import org.apache.shiro.util.ByteSource;
import org.apereo.cas.adaptors.jdbc.AbstractJdbcUsernamePasswordAuthenticationHandler;
import org.apereo.cas.authentication.AuthenticationHandlerExecutionResult;
import org.apereo.cas.authentication.PreventedException;
import org.apereo.cas.authentication.UsernamePasswordCredential;
import org.apereo.cas.authentication.exceptions.AccountDisabledException;
import org.apereo.cas.authentication.exceptions.AccountPasswordMustChangeException;
import org.apereo.cas.authentication.principal.PrincipalFactory;
import org.apereo.cas.services.ServicesManager;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;


import javax.security.auth.login.AccountNotFoundException;
import javax.security.auth.login.FailedLoginException;
import javax.sql.DataSource;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * A JDBC querying handler that will pull back the password and
 * the private salt value for a user and validate the encoded
 * password using the public salt value. Assumes everything
 * is inside the same database table. Supports settings for
 * number of iterations as well as private salt.
 * <p>
 * This handler uses the hashing method defined by Apache Shiro's
 * {@link org.apache.shiro.crypto.hash.DefaultHashService}. Refer to the Javadocs
 * to learn more about the behavior. If the hashing behavior and/or configuration
 * of private and public salts does nto meet your needs, a extension can be developed
 * to specify alternative methods of encoding and digestion of the encoded password.
 * </p>
 *
 */
@Slf4j
public class DspaceQueryAndEncodeDatabaseAuthenticationHandler extends AbstractJdbcUsernamePasswordAuthenticationHandler {

	
	private final String[] DSPACE_EPERSON_PUBLIC_PRINCIPAL_FIELDS = {"eperson_id", "email", "netid", "username", "main_group_id"};
    /**
     * The Algorithm name.
     */
    protected String algorithmName;

    /**
     * The Sql statement to execute.
     */
    protected String sql;

    /**
     * The Password field name.
     */
    protected String passwordFieldName = "password";

    /**
     * The Salt field name.
     */
    protected String saltFieldName = "salt";

    /**
     * The Expired field name.
     */
    protected String expiredFieldName;

    /**
     * The Expired field name.
     */
    protected String disabledFieldName;

    /**
     * The Number of iterations field name.
     */
    protected String numberOfIterationsFieldName;

    /**
     * The number of iterations. Defaults to 0.
     */
    protected long numberOfIterations;

    /**
     * Static/private salt to be combined with the dynamic salt retrieved
     * from the database. Optional.
     * <p>If using this implementation as part of a password hashing strategy,
     * it might be desirable to configure a private salt.
     * A hash and the salt used to compute it are often stored together.
     * If an attacker is ever able to access the hash (e.g. during password cracking)
     * and it has the full salt value, the attacker has all of the input necessary
     * to try to brute-force crack the hash (source + complete salt).</p>
     * <p>However, if part of the salt is not available to the attacker (because it is not stored with the hash),
     * it is much harder to crack the hash value since the attacker does not have the complete inputs necessary.
     * The privateSalt property exists to satisfy this private-and-not-shared part of the salt.</p>
     * <p>If you configure this attribute, you can obtain this additional very important safety feature.</p>
     */
    protected String staticSalt;

    public DspaceQueryAndEncodeDatabaseAuthenticationHandler(final String name, final ServicesManager servicesManager, final PrincipalFactory principalFactory,
                                                       final Integer order, final DataSource dataSource,
                                                       final String algorithmName, final String sql, final String passwordFieldName,
                                                       final String saltFieldName, final String expiredFieldName, final String disabledFieldName,
                                                       final String numberOfIterationsFieldName, final long numberOfIterations,
                                                       final String staticSalt) {
        super(name, servicesManager, principalFactory, order, dataSource);
        this.algorithmName = algorithmName;
        this.sql = sql;
        this.passwordFieldName = passwordFieldName;
        this.saltFieldName = saltFieldName;
        this.expiredFieldName = expiredFieldName;
        this.disabledFieldName = disabledFieldName;
        this.numberOfIterationsFieldName = numberOfIterationsFieldName;
        this.numberOfIterations = numberOfIterations;
        this.staticSalt = staticSalt;
    }

    @Override
    protected AuthenticationHandlerExecutionResult authenticateUsernamePasswordInternal(final UsernamePasswordCredential transformedCredential,
                                                                                        final String originalPassword)
        throws GeneralSecurityException, PreventedException {

        if (StringUtils.isBlank(this.sql) || StringUtils.isBlank(this.algorithmName) || getJdbcTemplate() == null) {
            throw new GeneralSecurityException("Authentication handler is not configured correctly");
        }

        final String username = transformedCredential.getUsername();
        try {
            final Map<String, Object> values = getJdbcTemplate().queryForMap(this.sql, username);
            final String digestedPassword = digestEncodedPassword(transformedCredential.getPassword(), values);

            if (!values.get(this.passwordFieldName).equals(digestedPassword)) {
                throw new FailedLoginException("Password does not match value on record.");
            }
            if (StringUtils.isNotBlank(this.expiredFieldName) && values.containsKey(this.expiredFieldName)) {
                final String dbExpired = values.get(this.expiredFieldName).toString();
                if (BooleanUtils.toBoolean(dbExpired) || "1".equals(dbExpired)) {
                    throw new AccountPasswordMustChangeException("Password has expired");
                }
            }
            
            // Disabled field inside Dspace is "can_log_in" column, must invert result
            if (StringUtils.isNotBlank(this.disabledFieldName) && values.containsKey(this.disabledFieldName)) {            	
                final String canLogIn = values.get(this.disabledFieldName).toString();
                if (!BooleanUtils.toBoolean(canLogIn) || "0".equals(canLogIn)) {
                    throw new AccountDisabledException("Account has been disabled");
                }
            }
            
            Map<String, Object> attributes = loadPrincipalAttributes(username, values);            
            return createHandlerResult(transformedCredential, this.principalFactory.createPrincipal(username, attributes), new ArrayList<>(0));

        } catch (final IncorrectResultSizeDataAccessException e) {
            if (e.getActualSize() == 0) {
                throw new AccountNotFoundException(username + " not found with SQL query");
            }
            throw new FailedLoginException("Multiple records found for " + username);
        } catch (final DataAccessException e) {
            throw new PreventedException("SQL exception while executing query for " + username, e);
        } catch(final IllegalArgumentException e) {
        	throw new PreventedException("Bad parameters for " + username, e);
        } catch (final DecoderException e) {
        	throw new PreventedException("Bad decoder provided for " + username, e);
        }
    }

    
    private Map<String, Object> loadPrincipalAttributes(String username, Map<String, Object> userValues) {
    	Map<String, Object> attributes  = new HashMap<String, Object>();    	
    	loadUserAttributes(username, userValues, attributes);
    	loadUserGroups(username, attributes);
    	loadUserIds(attributes);
    	loadUserGroupIds(attributes);
		return attributes;
	}
    
	private void loadUserAttributes(String username, Map<String, Object> values, Map<String, Object> attributes) {
		// Load common user fields		
		for(String key : DSPACE_EPERSON_PUBLIC_PRINCIPAL_FIELDS) 
			if(values.containsKey(key))
				attributes.put(key, values.get(key));					
		// Load additional user fields
    	String userAttributesSql = "select eperson_id as user_id, getuserfullname(eperson_id) as user_fullname,  username as user_path, main_group_id, email as user_email from eperson where email = ?";
    	Map<String, Object> userAttrs = getJdbcTemplate().queryForMap(userAttributesSql, username);     	  
		for(String key : userAttrs.keySet())
			attributes.put(key, userAttrs.get(key));
    	
    }
    
	private void loadUserGroups(String username, Map<String, Object> attributes) {
		String userGroupsIdSql = "SELECT array_to_string(array_agg(eperson_group_id), '/')  as group_id \n" + 
				"                                          FROM  (\n" + 
				"                                                    SELECT eperson_group_id \n" + 
				"                                                    FROM eperson \n" + 
				"                                                    NATURAL JOIN epersongroup2eperson \n" + 
				"                                                    NATURAL JOIN epersongroup \n" + 
				"                                                    WHERE email = ?\n" + 
				"                                                    ORDER BY eperson_group_id ASC) \n" + 
				"                                          AS x";
		Map<String, Object> userIds = getJdbcTemplate().queryForMap(userGroupsIdSql, username);
		for(String key : userIds.keySet())
			attributes.put(key, userIds.get(key));    	
	}
	
    private void loadUserIds(Map<String, Object> attributes) {
    	String userAttributesSql = "SELECT array_to_string(array_agg(username), '/') as users_path , array_to_string(array_agg(eperson_id), '/') as users_id  FROM  (   \n" + 
    			"                                                    SELECT username, eperson_id\n" + 
    			"                                                    FROM eperson\n" + 
    			"                                                    WHERE username is not null\n" + 
    			"                                                    AND username not like 'Reviewer%'                                               \n" + 
    			"                                                    ORDER BY username ASC\n" + 
    			"                                                ) AS x";
    	Map<String, Object> userIds = getJdbcTemplate().queryForMap(userAttributesSql, null);
    	for(String key : userIds.keySet())
    		attributes.put(key, userIds.get(key));    	
    }
    
    private void loadUserGroupIds(Map<String, Object> attributes) {
    	String userAttributesSql = "SELECT array_to_string(array_agg(name), '/') as groups_name, array_to_string(array_agg(eperson_group_id), '/') as groups_id\n" + 
    			"                                            FROM  (\n" + 
    			"                                                SELECT text_value AS name,\n" + 
    			"                                                       resource_id AS eperson_group_id\n" + 
    			"                                                FROM metadatavalue\n" + 
    			"                                                WHERE resource_type_id = 6\n" + 
    			"                                                      AND text_value not like 'COMMUNITY_%'\n" + 
    			"                                                      AND text_value not like 'COLLECTION_%'\n" + 
    			"                                                      AND text_value not like 'anonymous'\n" + 
    			"													  AND text_value not like 'Anonymous'\n" + 
    			"                                                      AND text_value not like 'administrator'\n" + 
    			"                                                      AND text_value not like 'Reviewers'\n" + 
    			"                                                      ORDER BY text_value ASC\n" + 
    			"                                                  ) AS x;";
    	Map<String, Object> groupIds = getJdbcTemplate().queryForMap(userAttributesSql, null);
    	for(String key : groupIds.keySet())
    		attributes.put(key, groupIds.get(key)); 
 	}
    
	/**
     * Digest encoded password.
     *
     * @param encodedPassword the encoded password
     * @param values          the values retrieved from database
     * @return the digested password
     * @throws DecoderException 
     * @throws IllegalArgumentException 
     */
    protected String digestEncodedPassword(final String encodedPassword, final Map<String, Object> values) throws IllegalArgumentException, DecoderException {
        final ConfigurableHashService hashService = new DefaultHashService();
        if (StringUtils.isNotBlank(this.staticSalt)) {
            hashService.setPrivateSalt(ByteSource.Util.bytes(this.staticSalt));
        }
        hashService.setHashAlgorithmName(this.algorithmName);

        Long numOfIterations = this.numberOfIterations;
        if (values.containsKey(this.numberOfIterationsFieldName)) {
            final String longAsStr = values.get(this.numberOfIterationsFieldName).toString();
            numOfIterations = Long.valueOf(longAsStr);
        }

        hashService.setHashIterations(numOfIterations.intValue());
        if (!values.containsKey(this.saltFieldName)) {
            throw new IllegalArgumentException("Specified field name for salt does not exist in the results");
        }

        final String dynaSalt = values.get(this.saltFieldName).toString();
        final HashRequest request = new HashRequest.Builder()
            .setSalt(Hex.decodeHex(dynaSalt.toCharArray()))
            .setSource(encodedPassword)
            .build();
        return hashService.computeHash(request).toHex();
    }
}

