<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
	xmlns="http://www.w3.org/1999/xhtml" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:cml="http://www.xml-cml.org/schema"
	xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
	xmlns:orca="https://orcaforum.cec.mpg.de/"
	xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
	
	xpath-default-namespace="http://www.xml-cml.org/schema" 
	exclude-result-prefixes="xs xd"
	version="2.0">    
	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> Nov 13, 2015</xd:p>
			<xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
			<xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
		</xd:desc>
	</xd:doc>	
	<xsl:param name="title"/>
	<xsl:param name="description"/>
	<xsl:param name="hasmolecularorbitals"/>
	
	
	<xsl:output method="text" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:include href="../xslt/helper/chemistry/orca.xsl"/>
	<xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
	
	<xsl:template match="/">		
		<xsl:variable name="program" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
		<xsl:variable name="programVersion" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
		<xsl:variable name="programSubversion" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programSubversion']"/>
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
		<xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
		</xsl:if>
		<xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>   
		
<!-- start job section -->		
		<xsl:for-each select="//cml:module[@dictRef='cc:job']">
			<xsl:variable name="outerIndex" select="position()"/>
			<xsl:variable name="currentJob" select="."/>
			
			<!-- Build calculation type variable for this job -->			
			<xsl:variable name="calcType">								
					<xsl:element name="cml:calcType">
						<xsl:variable name="commands" select="((//cml:module[@id='initialization'])[1]//cml:module[@cmlx:templateRef='job'])[$outerIndex]"/>
						<xsl:variable name="position" select="position()" />
						
						<xsl:variable name="scfsettings" select="(//cml:module[@id='initialization'])[$position]//cml:module[@cmlx:templateRef='scfsettings'][1]" />
						<xsl:variable name="basissets" select="orca:getBasis($commands)"/>
						<xsl:variable name="functionals" select="orca:getFunctionals($commands, $scfsettings)"/>
						<xsl:variable name="isTddft" select="orca:isTddft($commands)" as="xs:boolean"/>
						<xsl:variable name="isTS" select="orca:isTS($commands)" as="xs:boolean"/>
						<xsl:variable name="isRestricted" select="exists((//cml:module[@id='initialization'])[$position]//cml:module[@cmlx:templateRef='optsetup'])"/>
						<xsl:variable name="methods" >
							<xsl:variable name="currentMethods" select="orca:getMethods($commands, $isTddft)"/>
							<xsl:choose>
								<xsl:when test="exists($functionals)"><!--  If functionals exists = DFT -->                            
									<xsl:if test="$isTddft">TD</xsl:if><xsl:text>DFT</xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$currentMethods"/>
								</xsl:otherwise>
							</xsl:choose>
						</xsl:variable>                
						<!-- Get calculation type -->                
						<xsl:variable name="isOptimization" select="orca:isOptimization($commands)"/>
						<xsl:variable name="isBrokenSymm" select="orca:isBrokenSymm($commands)"/>
						
						<xsl:variable name="hasVibrations" select="exists(//cml:module[@dictRef='cc:job'][$position]//cml:property[@dictRef='cc:frequencies'])"/>                            
						<xsl:variable name="negativeFrequenciesCount" select="orca:countNegativeFrequencies((//cml:module[@dictRef='cc:job'][$position]//cml:property[@dictRef='cc:frequencies'])[last()]//cml:array[@dictRef='cc:frequency']/text())"/>
						
						<xsl:variable name="calcTmp" select="orca:getCalcType($isOptimization, $isBrokenSymm, $hasVibrations, $negativeFrequenciesCount, $isTS)"/>
						<xsl:variable name="calcType" select="$calcTmp"/>						
						<!-- Build element -->
						<xsl:attribute name="isTDDFT" select="$isTddft"/> 
						<xsl:attribute name="isOptimization" select="$isOptimization"/>
						<xsl:attribute name="isBrokenSymm" select="$isBrokenSymm"/>
						<xsl:attribute name="hasVibrations" select="$hasVibrations"/>
						<xsl:attribute name="negativeFrequenciesCount" select="$negativeFrequenciesCount"/>
						<xsl:attribute name="methods" select="$methods" />
						<xsl:attribute name="isCalculationLevel" select="boolean($basissets/@isCalculationLevel)"/>
						<xsl:attribute name="basis" select="
							if(boolean($basissets/@isCalculationLevel) and compare($basissets/@definedBasis, ' ') = 0) then
								$basissets/@calculationLevelBasis
							else
								$basissets/@definedBasis
							"/>
						<xsl:attribute name="functionals" select="$functionals"/>
						<!-- Charge / multiplicity -->
						<xsl:attribute name="multiplicity" select="((//cml:module[@dictRef='cc:job'])[$outerIndex]//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Mult']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
						<xsl:attribute name="charge" select="((//cml:module[@dictRef='cc:job'])[$outerIndex]//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Charge']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
						
						<!-- Energy, formula, calculation type -->
						<xsl:variable name="energy" select="((//cml:module[@dictRef='cc:job'])[$outerIndex]//cml:module[@cmlx:templateRef='totalenergy'])[last()]"/>
						<xsl:if test="exists($energy)">
							<xsl:attribute name="energyValue" select="($energy/cml:scalar[@dictRef='cc:totalener'])[last()]"/>
							<xsl:attribute name="energyUnits" select="helper:printUnitSymbol(($energy/cml:scalar[@dictRef='cc:totalener'])[last()]/@units)"/>													
						</xsl:if>
						<xsl:attribute name="formula" select="((//cml:module[@dictRef='cc:job'])[$outerIndex]//formula/@concise)[1]"/>
						
						<xsl:value-of select="$calcType"/>
					</xsl:element>			
			</xsl:variable>
cml:multiplicity&#x9;<xsl:value-of select="$calcType/cml:calcType/@multiplicity"/>
cml:charge&#x9;<xsl:value-of select="$calcType/cml:calcType/@charge"/>
cml:energy.value&#9;<xsl:value-of select="$calcType/cml:calcType/@energyValue"/>			
cml:energy.units&#9;<xsl:value-of select="$calcType/cml:calcType/@energyUnits"/>
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation($calcType/cml:calcType/@formula)"/>
cml:calculationtype&#9;<xsl:value-of select="$calcType/cml:calcType/text()"/>			
			<xsl:variable name="hasVibrationalFrequencies" select="exists((//cml:module[@dictRef='cc:job'])[$outerIndex]//cml:module[@dictRef='cc:finalization']/propertyList/property[@dictRef='cc:frequencies'])"/>
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrationalFrequencies"/>			
			<xsl:variable name="spinType" select="orca:isMethodUnrestricted(((//cml:module[@dictRef='cc:job'])[$outerIndex]//cml:scalar[@dictRef='cc:parameter' and upper-case(text())= 'HFTYP' ]/parent::cml:list/cml:scalar[@dictRef='cc:value']/text())[1])"/>
			<xsl:choose>
				<xsl:when test="$spinType = false()">
cml:spintype&#9;Restricted
				</xsl:when>
				<xsl:when test="$spinType = true()">
cml:spintype&#9;Unrestricted
				</xsl:when>				
			</xsl:choose>			
			<!-- Get basis sets, we'll do it for each atom. First check if assigned on geometry, or on input line and at last by calculation level -->			
			<xsl:variable name="inputMolecule" select="(//cml:module[@dictRef='cc:finalization']/cml:molecule)[$outerIndex]"/>			
			<xsl:variable name="basis">
				<!-- Atom defined basis -->
				<xsl:for-each select="1 to count($inputMolecule/cml:atomArray/cml:atom)">
					
					<xsl:variable name="innerIndex" select="position()"/>
					<xsl:variable name="elementType" select="($inputMolecule/cml:atomArray/cml:atom)[$innerIndex]/@elementType"/>
					<xsl:choose>				
						<xsl:when test="exists(($inputMolecule/cml:atomArray/cml:atom)[$innerIndex]/cml:scalar[@dictRef='cc:basis']/text()) and compare(($inputMolecule/cml:atomArray/cml:atom)[$outerIndex]/cml:scalar[@dictRef='cc:basis']/text(),'N/A') !=0 ">
							<xsl:value-of select="($inputMolecule/cml:atomArray/cml:atom)[$innerIndex]/cml:scalar[@dictRef='cc:basis']/text()"/><xsl:text> </xsl:text>
						</xsl:when>				
						<xsl:otherwise>
							<xsl:choose>
								<xsl:when test="boolean($calcType/cml:calcType/@isCalculationLevel) and starts-with($calcType/cml:calcType/@basis,'DefBas')">
									<xsl:value-of select="orca:getDefaultBasisForAtomType($elementType, $calcType/cml:calcType/@basis)"></xsl:value-of><xsl:text> </xsl:text>
								</xsl:when>
								<xsl:otherwise>
									<xsl:value-of select="$calcType/cml:calcType/@basis"/><xsl:text> </xsl:text>
								</xsl:otherwise>
							</xsl:choose>						
						</xsl:otherwise>
					</xsl:choose>
				</xsl:for-each>
				<!-- Section defined basis -->
				<xsl:variable name="userdefbasis" select="(.//cml:module[@cmlx:templateRef='input']/cml:module[@cmlx:templateRef='job'])[$outerIndex]/cml:module[@cmlx:templateRef='basis']"/>
				<xsl:for-each select="$userdefbasis">	
					<xsl:for-each select=".//descendant::cml:array[matches(upper-case(text()), '.*NEWGTO\s.*')]">
						<xsl:variable name="basis" select="."/>
						<xsl:value-of select="concat((tokenize($basis/text(),'&quot;'))[2], ' ')"/>						
					</xsl:for-each>										
				</xsl:for-each>				
			</xsl:variable>
			<xsl:for-each select="distinct-values(tokenize($basis,'\s+'))">
				<xsl:if test=". != ''">
cml:basisset&#x9;<xsl:value-of select="."/>	
				</xsl:if>				
			</xsl:for-each>	
			<!-- Get Methods -->			                        
			<xsl:for-each select="distinct-values(tokenize($calcType/cml:calcType/@methods, '\s+'))">
				<xsl:if test=". != ''">
cml:method&#9;<xsl:value-of select="."/>
				</xsl:if>
			</xsl:for-each>			
			<xsl:variable name="input" select="((//cml:module[@id='initialization'])[1]//cml:module[@cmlx:templateRef='job'])[$outerIndex]"/>
cml:hassolvent&#9;<xsl:value-of select="exists((//cml:module[@dictRef='cc:job'])[$outerIndex]//cml:module[@cmlx:templateRef='cosmo' or @cmlx:templateRef='cpcm'])"/>
		</xsl:for-each>
<!--end job section-->	
<!-- Calculation global values -->	
		<xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>	
cml:numberofjobs&#9;<xsl:value-of select="count(//cml:module[@dictRef='cc:jobList']/cml:module[@dictRef='cc:job'])"/>
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>        
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>		
<!-- Create provided metadata -->
	<xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
	</xsl:if>
	<xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
	</xsl:if>	
	</xsl:template>
	
	<xsl:function name="orca:getDefaultBasisForAtomType">
		<xsl:param name="elementType"/>
		<xsl:param name="basis"/>        
		<!-- Standard Basis Sets  and Computational Levels -->
		<xsl:variable name="basisNumber" select="number(substring-after($basis,'DefBas-'))"/>        
		<xsl:choose>
			<xsl:when test="$basisNumber >2">TZV</xsl:when>
			<xsl:otherwise>
				<xsl:variable name="elementNumber" select="helper:atomType2Number($elementType)"/>
				<xsl:choose>
					<xsl:when test="$elementNumber = 1 or not(helper:isTransitionMetal($elementNumber))">SV</xsl:when>
					<xsl:otherwise>TZ</xsl:otherwise>
				</xsl:choose>
			</xsl:otherwise>            
		</xsl:choose>
	</xsl:function>
	
	<!-- Override default templates -->
	<xsl:template match="text()"/>
	<xsl:template match="*"/>
		
</xsl:stylesheet>