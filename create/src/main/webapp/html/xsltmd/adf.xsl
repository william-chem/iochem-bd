<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"     
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:adf="http://www.scm.com/ADF/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>Jan 23, 2014</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/adf.xsl"/>    
    <xsl:template match="/">
        <xsl:variable name="program" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:program']/scalar)[last()]"/>
        <xsl:variable name="programVersion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programVersion']/scalar)[last()]"/>
        <xsl:variable name="programSubversion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programSubversion']/scalar)[last()]"/>
        <xsl:variable name="basis" select="distinct-values(//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='fragment.files']//scalar[@dictRef='cc:basis'])"/>
        <xsl:variable name="charge" select="if(exists((//module[@id='finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:charge'])[last()])) then
                                                (//module[@id='finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:charge'])[last()] 
                                            else 
                                                 //module[@cmlx:templateRef='symmetry']/scalar[@dictRef='a:charge']"/>
        <xsl:variable name="spinpolarization" select="(//scalar[@dictRef='a:spinPolarization'])[last()]"/>
        <xsl:variable name="multiplicity" select="if(exists($spinpolarization)) then
                                                     string(number($spinpolarization)+1)
                                                  else if(exists(//module[@cmlx:templateRef='logfile'])) then
                                                     '1'
                                                  else '' "/>
            
        <xsl:variable name="spinType" select="distinct-values(//module[@id='initialization']/module[@id='otherComponents']/module[@cmlx:templateRef='parameters']//scalar[@dictRef='cc:spinMolecule'])"/>
        <xsl:variable name="potentialEnergy" select="(//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='cc:potentialEnergy'])[last()]"/> 
        <xsl:variable name="formula" select="(//formula/@concise)[1]"/>
        <xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>        
        <xsl:variable name="functionals" select="distinct-values(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:functional']/scalar)"/>                
        <xsl:variable name="method" select="helper:normalizeMethods($functionals)"/>        
        <xsl:variable name="runType" select="concat((//parameter[@dictRef='cc:runtype']/scalar/text())[last()], '')"/>
        <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])" />
        <xsl:variable name="isQuild" select="exists(//module[@cmlx:templateRef='quild.iteration'])"/>
        <xsl:variable name="isNMR" select="exists(//module[@cmlx:templateRef='nucleus'])"/>
        <xsl:variable name="calcType" select="adf:getCalcType($runType,$isQuild,$isNMR)"/>        
        <xsl:variable name="hasSolvent" select="exists(//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='solvation'])"/>
        <xsl:variable name="numberOfJobs" select="count(//module[@dictRef='cc:jobList']/module[@dictRef='cc:job'])"/>
        
        <!-- Print metadata fields -->
        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
        </xsl:if>  
        <xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
        </xsl:if>        
        <xsl:for-each select="$basis">            
cml:basisset&#x9;<xsl:value-of select="."/>                
        </xsl:for-each>                                    
cml:charge&#9;<xsl:value-of select="$charge"/>
<xsl:if test="$multiplicity != ''">
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity"/>
</xsl:if>        
        <xsl:for-each select="$spinType">
cml:spintype&#x9;<xsl:value-of select="."/>    
        </xsl:for-each>                        
        <xsl:if test="exists($potentialEnergy)">
cml:energy.value&#9;<xsl:value-of select="$potentialEnergy/text()"/>
cml:energy.units&#9;<xsl:value-of select="helper:printUnitSymbol($potentialEnergy/@units)"/>
        </xsl:if>        
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation($formula)"/>
        <xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>              
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrations"/>
cml:method&#9;<xsl:value-of select="$method"/>                                   
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>               
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
        <!-- Create provided metadata -->      
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>
    </xsl:template>
        
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>