<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
	xmlns="http://www.w3.org/1999/xhtml" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
	xmlns:cml="http://www.xml-cml.org/schema"
	xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
	xmlns:mp="http://www.iochem-bd.org/dictionary/mopac/"
	xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
	
	xpath-default-namespace="http://www.xml-cml.org/schema" 
	exclude-result-prefixes="xs xd"
	version="2.0">    
	<xd:doc scope="stylesheet">
		<xd:desc>
			<xd:p><xd:b>Created on:</xd:b> Mar 06, 2018</xd:p>
			<xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
			<xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
		</xd:desc>
	</xd:doc>	
	<xsl:param name="title"/>
	<xsl:param name="description"/>
	<xsl:param name="hasmolecularorbitals"/>
	
	
	<xsl:output method="text" indent="no"/>
	<xsl:strip-space elements="*"/>
	<xsl:include href="../xslt/helper/chemistry/mopac.xsl"/>
	<xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
	
	<xsl:template match="/">		
		<xsl:variable name="program" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
		<xsl:variable name="programVersion" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
		<xsl:variable name="programSubversion" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programSubversion']"/>
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
		<xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
		</xsl:if>
		<xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>   		
<!-- start job section -->		
		<xsl:for-each select="//cml:module[@dictRef='cc:job']">
			<xsl:variable name="outerIndex" select="position()"/>
			<xsl:variable name="currentJob" select="."/>
			
			<!-- Build calculation type variable for this job -->
			<xsl:variable name="calcType">
				<xsl:variable name="inputLines" select="./cml:module [@dictRef='cc:initialization']/cml:module[@cmlx:templateRef='inputlines']/cml:list[@cmlx:templateRef='lines']" />
				<xsl:variable name="isOptimization" select="exists(.//cml:module[@cmlx:templateRef='optimization'])" />
				<xsl:variable name="vibrations" select=".//cml:module[@dictRef='cc:finalization']//cml:property[@dictRef='cc:frequencies']" />
				<xsl:variable name="hasVibrations" select="exists($vibrations)"/>
				<xsl:variable name="negativeFrequenciesCount" select="count(tokenize($vibrations//cml:module[@dictRef='cc:finalization']//cml:property[@dictRef='cc:frequencies']//cml:array[@dictRef='cc:frequency'],'-')) -1" />
				<xsl:variable name="isTs" select="mp:isTS($inputLines)" />            
				
				<xsl:element name="cml:calcType">
					<xsl:attribute name="methods" select="distinct-values(mp:getMethods($inputLines))" />
					<xsl:attribute name="isOptimization" select="$isOptimization"/>
					<xsl:attribute name="hasVibrations" select="$hasVibrations"/>
					<xsl:attribute name="negativeFrequenciesCount" select="$negativeFrequenciesCount"/>
					<xsl:value-of select="mp:getCalcType($isOptimization, $hasVibrations, $negativeFrequenciesCount, $isTs)"/>
					</xsl:element>
			</xsl:variable>	
cml:multiplicity&#x9;<xsl:value-of select="mp:getMultiplicity(//cml:module[@cmlx:templateRef='inputlines'])"/>
<xsl:variable name="charge">
	<xsl:variable name="value" select="helper:trim(mp:getCharge(//cml:module[@cmlx:templateRef='inputlines']))"/>
	<xsl:choose>
		<xsl:when test="$value = ''">0</xsl:when>
		<xsl:otherwise><xsl:value-of select="$value"/></xsl:otherwise>
	</xsl:choose>
</xsl:variable>						
cml:charge&#x9;<xsl:value-of select="$charge"/>
			
			
<xsl:variable name="energy" select=".//cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='cc:totalener']" />
<xsl:if test="exists($energy)">
cml:energy.value&#9;<xsl:value-of select="$energy"/>			
cml:energy.units&#9;<xsl:value-of select="$energy/@units"/>		
</xsl:if>			
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation((//cml:module[@dictRef='cc:job']//cml:molecule)[last()]/cml:formula/@concise)"/>
cml:calculationtype&#9;<xsl:value-of select="$calcType/cml:calcType/text()"/>			
<!-- Get basis sets, we'll do it for each atom. First check if assigned on geometry, or on input line and at last by calculation level -->			
<xsl:variable name="inputMolecule" select="(//cml:module[@dictRef='cc:finalization']/cml:molecule)[$outerIndex]"/>			
<!-- Get Methods -->			                        
	<xsl:for-each select="distinct-values(tokenize($calcType/cml:calcType/@methods, '\s+'))">
		<xsl:if test=". != ''">
cml:method&#9;<xsl:value-of select="."/>
		</xsl:if>
		</xsl:for-each>
	</xsl:for-each>
<!--end job section-->
<xsl:variable name="hasVibrationalFrequencies" select="exists((//cml:module[@dictRef='cc:job'])//cml:module[@dictRef='cc:finalization']/propertyList/property[@dictRef='cc:frequencies'])"/>
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrationalFrequencies"/>
cml:hassolvent&#9;false
<!-- Calculation global values -->	
<xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
</xsl:if>	
cml:numberofjobs&#9;<xsl:value-of select="count(//cml:module[@dictRef='cc:jobList']/cml:module[@dictRef='cc:job'])"/>
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>        
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>		
<!-- Create provided metadata -->
	<xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
	</xsl:if>
	<xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
	</xsl:if>	
	</xsl:template>
	
	<!-- Override default templates -->
	<xsl:template match="text()"/>
	<xsl:template match="*"/>
		
</xsl:stylesheet>