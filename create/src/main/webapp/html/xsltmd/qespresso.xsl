<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:qex="http://www.iochem-bd.org/dictionary/qespresso/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Feb 21, 2018</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
        </xd:desc>
    </xd:doc>	
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/qespresso.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    
    
    <xsl:template match="/">
        <xsl:variable name="program" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:program']/scalar)[last()]"/>
        <xsl:variable name="programVersion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programVersion']/scalar)[last()]"/>
        <xsl:variable name="programSubversion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programSubversion']/scalar)[last()]"/>
   
        <xsl:variable name="modNames" select="//cml:module[@dictRef='cc:job']/module[@id='environment']/parameterList/parameter[@dictRef='cc:module']/scalar/text()"/>
        <xsl:variable name="calcParameter" select="replace(qex:getInputParameter(., 'CONTROL', 'calculation'), '&quot;','')"/>
        <xsl:variable name="calcType" select="qex:getCalcType($modNames,$calcParameter)"/>
        <xsl:variable name="method" select="'DFT'"/>
        
        <xsl:variable name="otherComponents" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@id='otherComponents']"/>
        <xsl:variable name="userDefinedModules" select="$otherComponents/module[@dictRef='cc:userDefinedModule']"/>
        <xsl:variable name="basis" select="distinct-values(qex:getBasis($userDefinedModules/module[@cmlx:templateRef='pseudopotential']/scalar[@dictRef='qex:pseudopotential']/text()))"/>
                                
        <xsl:variable name="nspin" select="qex:getInputParameter(.,'SYSTEM','nspin')"/>
        <xsl:variable name="shellType" select="qex:getShellType($nspin)"/>
            
        <xsl:variable name="charge" select="qex:getCharge(qex:getInputParameter(., 'tot_charge'))" /> 
       
        <xsl:variable name="pseudoFilenames" select="$userDefinedModules/module[@cmlx:templateRef='pseudopotential']/scalar[@dictRef='qex:pseudofile']/text()"/>

        <xsl:variable name="functional" select="helper:trim(replace($otherComponents/module[@cmlx:templateRef='parameters']//scalar[@dictRef='cc:parameter' and text() = 'Exchange-correlation']/following-sibling::scalar[@dictRef='cc:value']/text(),'\(.*\)',''))"/>
        <xsl:variable name="functionalsFromFilenames" select="distinct-values(qex:getFunctionalsFromFilenames($pseudoFilenames))"/>
        <xsl:variable name="functionalMethods" select="distinct-values(qex:getMethodsFromFilenames($pseudoFilenames))"/>
       
        <xsl:variable name="switchingFunction" select="//module[@dictRef='cc:initialization']//module[@cmlx:templateRef='environ']//scalar[@dictRef='cc:parameter' and text()='switching function adopted']/following-sibling::cml:scalar[@dictRef='cc:value']" />
        <xsl:variable name="hasSolvent" select="if(exists($switchingFunction) and $switchingFunction = 'SCCS') then 'true' else 'false'"/>
        <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])"/>
        <xsl:variable name="numberOfJobs" select="count(//module[@dictRef='cc:jobList']/module[@dictRef='cc:job'])"/>
        
        <xsl:variable name="atomArray" select="(//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule/atomArray"/>
        <xsl:variable name="formula" select="helper:calculateHillNotationFormula($atomArray)"/>
        <xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>  

        <xsl:variable name="finalOtherComponents" select="//module[@dictRef='cc:finalization']/module[@id='otherComponents']" />
        <xsl:variable name="finalEnergy" select="($finalOtherComponents/module[@cmlx:templateRef='energies']//scalar[@dictRef='qex:totalener'])[last()]"/>
        
        <!-- Print metadata fields -->
        
        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
        </xsl:if>  
        <xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
        </xsl:if>
        <xsl:if test="exists($shellType)">            
cml:shelltype&#x9;<xsl:value-of select="$shellType"/>
        </xsl:if>
        <xsl:if test="exists($finalEnergy)">
cml:energy.value&#9;<xsl:value-of select="$finalEnergy/text()"/>
cml:energy.units&#9;<xsl:value-of select="helper:printUnitSymbol($finalEnergy/@units)"/>
        </xsl:if>        
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation((//formula/@concise)[1])"/>
        <xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>        
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>              
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrations"/>
        <xsl:choose>
          <xsl:when test="exists($functional)">
cml:method&#9;<xsl:value-of select="$functional"/>
          </xsl:when>
          <xsl:when test="exists($functionalsFromFilenames)">
            <xsl:for-each select="$functionalsFromFilenames">
cml:method&#9;<xsl:value-of select="."/>    
            </xsl:for-each>
          </xsl:when>
        </xsl:choose>
                              
        <xsl:for-each select="$basis">
cml:basisset&#9;<xsl:value-of select="."/>
        </xsl:for-each>
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>       
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>        
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
cml:charge&#x9;<xsl:value-of select="$charge"/>

        <!-- Create provided metadata -->       
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>
        
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
    
    
</xsl:stylesheet>