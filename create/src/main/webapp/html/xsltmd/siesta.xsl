<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:qex="http://www.iochem-bd.org/dictionary/siesta/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Apr 23, 2022</xd:p>
            <xd:p><xd:b>Author:</xd:b>William Bro-Jørgensen</xd:p>
            <xd:p><xd:b>Center:</xd:b>Chemistry Department at University of Copenhagen</xd:p>
        </xd:desc>       
    </xd:doc>	
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <!-- <xsl:param name="hasmolecularorbitals"/> -->
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/siesta.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    
    
    <xsl:template match="/">
        <xsl:variable name="initialparameters" select="//cml/parameterList"/>
        <xsl:variable name="finalMolecule" select="//module[@dictRef='cc:finalization' and child::molecule]//molecule"/>
        <xsl:variable name="metadatalist" select="//cml/metadataList"/>

        <xsl:variable name="program" select = "$metadatalist/metadata[@name='siesta:Program']/@content"/> 
        <xsl:variable name="programVersion" select = "$metadatalist/metadata[@name='siesta:Version']/@content"/> 
   
        <!-- <xsl:variable name="modNames" select="//cml:module[@dictRef='cc:job']/module[@id='environment']/parameterList/parameter[@dictRef='cc:module']/scalar/text()"/> -->
        <xsl:variable name="calcType" select="$initialparameters/parameter[@name='MD.TypeOfRun']"/>
        <xsl:variable name="method" select="'DFT'"/>
        
        <!-- <xsl:variable name="otherComponents" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@id='otherComponents']"/>
        <xsl:variable name="userDefinedModules" select="$otherComponents/module[@dictRef='cc:userDefinedModule']"/>
        <xsl:variable name="basis" select="distinct-values(qex:getBasis($userDefinedModules/module[@cmlx:templateRef='pseudopotential']/scalar[@dictRef='qex:pseudopotential']/text()))"/> -->
                                
        <!-- <xsl:variable name="nspin" select="qex:getInputParameter(.,'SYSTEM','nspin')"/> -->
        <!-- <xsl:variable name="shellType" select="qex:getShellType($nspin)"/> -->
            
        <xsl:variable name="charge" select="number($initialparameters/parameter[@name='NetCharge'])"/> 
       
        <!-- <xsl:variable name="pseudoFilenames" select="$userDefinedModules/module[@cmlx:templateRef='pseudopotential']/scalar[@dictRef='qex:pseudofile']/text()"/> -->

        <xsl:variable name="functionalBlock" select="//cml/propertyList[@title='XC.Mix']"/>
       
        <xsl:variable name="formula" select="helper:calculateHillNotationFormula($finalMolecule/molecule/atomArray/atom)"/>

        <xsl:variable name="finalpropertylist" select="//cml/module[@title='Finalization']/propertyList[@title='Final Energy']"/>
        <xsl:variable name="Etot" select="$finalpropertylist/property[@dictRef='siesta:Etot']"/>

        <!-- Print metadata fields -->

        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>
        </xsl:if>  
        <!-- <xsl:if test="exists($shellType)">
cml:shelltype&#x9;<xsl:value-of select="$shellType"/>
        </xsl:if> -->
        <xsl:if test="exists($finalEnergy)">
cml:energy.value&#9;<xsl:value-of select="$Etot"/>
cml:energy.units&#9;<xsl:value-of select="siesta:convertSymbol(substring-after($Etot/scalar/@units, ':'))"/>
        </xsl:if>        
cml:formula.generic&#9;<xsl:value-of select="helper:calculateHillNotationFormula($finalMolecule/molecule/atomArray/atom)"/>
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>
        <xsl:choose>
          <xsl:when test="exists($functionalBlock)">
            <xsl:for-each select="$functionalBlock/propertyList">
cml:method&#9;<xsl:value-of select="./property[@title='Functional']"/><xsl:value-of select="./property[@title='Authors']"/>
            </xsl:for-each>
          </xsl:when>
        </xsl:choose>

        <!-- <xsl:for-each select="$basis">
cml:basisset&#9;<xsl:value-of select="."/>
        </xsl:for-each> -->
<!-- cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/> -->
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
cml:charge&#x9;<xsl:value-of select="$charge"/>

        <!-- Create provided metadata -->
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>
        </xsl:if>
        
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
</xsl:stylesheet>