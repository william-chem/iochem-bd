<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"     
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:gr="http://www.iochem-bd.org/dictionary/gronor/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>Apr 28, 2021</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>    
    <xsl:template match="/">
        <xsl:variable name="program" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:program']/scalar)[last()]"/>
        <xsl:variable name="programVersion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programVersion']/scalar)[last()]"/>
        <xsl:variable name="programSubversion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programSubversion']/scalar)[last()]"/>        
        <xsl:variable name="initialization" select="//module[@dictRef='cc:initialization']"/>
        <xsl:variable name="charge" select="$initialization/parameterList/parameter[@dictRef='cc:charge']/scalar" />
        <xsl:variable name="multiplicity" select="$initialization/parameterList/parameter[@dictRef='cc:spinMultiplicity']/scalar" />
        <xsl:variable name="formula" select="(//formula/@concise)[1]" />
        <xsl:variable name="method" select="$initialization/parameterList/parameter[@dictRef='cc:method']" />
        <xsl:variable name="calcType" select="'NOCI'"/>
        <xsl:variable name="hasSolvent" select="'false'"/>
        <xsl:variable name="hasVibrations" select="'false'"/>
        <xsl:variable name="numberOfJobs" select="1"/>

        <xsl:variable name="nociStates" select="//module[@dictRef='cc:calculation']/module[@dictRef='gr:nociStates']" />        
        <xsl:variable name="potentialEnergy" select="$nociStates//propertyList[matches(child::property[@dictRef='gr:rootNumber']/scalar/text(),'1')]//property[@dictRef='gr:nociEnergy']/scalar" />
        
        <!-- Print metadata fields -->
        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
        </xsl:if>  
        <xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
        </xsl:if>
        <xsl:if test="exists($potentialEnergy)">
cml:energy.value&#9;<xsl:value-of select="$potentialEnergy/text()"/>
cml:energy.units&#9;<xsl:value-of select="helper:printUnitSymbol($potentialEnergy/@units)"/>
        </xsl:if>
cml:charge&#9;<xsl:value-of select="$charge"/>
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity"/>
cml:spintype&#x9;restricted        
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation($formula)"/>   
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>        
cml:method&#9;<xsl:value-of select="$method"/>                                   
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="boolean($hasVibrations)"/>
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
        <!-- Create provided metadata -->      
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>