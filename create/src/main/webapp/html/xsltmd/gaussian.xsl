<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"     
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:gaussian="http://www.gaussian.com/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 16, 2013</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>   
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/gaussian.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    <xsl:template match="/">
        <xsl:variable name="program" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:program']/scalar)[last()]"/>
        <xsl:variable name="programSubversion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:version']/scalar)[last()]"/>
        <xsl:variable name="basis">
           <xsl:variable name="values">
               <xsl:for-each select="distinct-values(//module[@id='job']//module[@cmlx:templateRef='l301.basis2'][1]/module[@cmlx:templateRef='centers']/scalar[@dictRef='cc:basis'])">
                   <xsl:value-of select="concat(' ',.)"/>
               </xsl:for-each>
               <xsl:for-each select="distinct-values(//module[@id='initialization']/parameterList/parameter[@dictRef='cc:basis']/scalar)">
                   <xsl:value-of select="concat(' ',.)"/>
               </xsl:for-each>
               <xsl:for-each select="distinct-values(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:basis']/scalar)">
                   <xsl:value-of select="concat(' ',.)"/>
               </xsl:for-each>
           </xsl:variable>
            <xsl:value-of select="distinct-values(tokenize(upper-case(helper:trim($values)),' '))"/>
        </xsl:variable>
                
        <xsl:variable name="qmmModule"   select="(//module[@cmlx:templateRef='l101.qmmm'])[1]"/>
        <xsl:variable name="zmatModule"  select="//module[@cmlx:templateRef='l101.zmat']"/>
        <xsl:variable name="zmataModule" select="//module[@cmlx:templateRef='l101.zmata']"/>
        
        <xsl:variable name="charge" select="if((//module[@id='initialization']/molecule/@formalCharge)[1] != '') then
            (//module[@id='initialization']/molecule/@formalCharge)[1]
            else
            (//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@type='chargemult']//scalar[@dictRef='x:formalCharge'])[last()]                                            
            "/>
        <xsl:variable name="multiplicity" select="if((//module[@id='initialization']/molecule/@spinMultiplicity)[1] != '') then
            (//module[@id='initialization']/molecule/@spinMultiplicity)[1]
            else
            (//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@type='chargemult']//scalar[@dictRef='x:spinMultiplicity'])[last()]
            "/>

        <xsl:variable name="shellTypes" select="distinct-values(//module[@cmlx:templateRef='l502']/scalar[@dictRef='g:l502.type'])"/>
        <xsl:variable name="energy" select="helper:getGaussianPotentialEnergy(.)"/>
        <xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>
                
        <xsl:variable name="isOptimization" select="exists(//module[@cmlx:templateRef='l103']) and exists(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='g:keyword'][matches(., '[Oo][Pp][Tt](\s*=.*)?')])"/>
        <xsl:variable name="hasStationaryPoint" select="count(//module[@cmlx:templateRef='l103.optimizedparam']//scalar[@dictRef='g:optimization' and contains(text(),'Stationary point found')]) > 0"/>
        <xsl:variable name="hasMinimum" select="count(//module[@cmlx:templateRef='l103.localminsaddle']/scalar[@dictRef='cc:minmaxts' and contains(text(),'local minimum')])> 0"/>
        <xsl:variable name="isEET" select="count(//module[@id='initialization']/parameterList/parameter[@dictRef='g:keyword']/scalar[matches(lower-case(text()),'eet..*')]) > 0" />
        <!-- Deduce spin type first by orbital section existence, otherwise by method prefixes -->
        <xsl:variable name="spinType">
            <xsl:choose>
                <xsl:when test="exists(//module[@cmlx:templateRef='orbitalsection'])">
                    <xsl:choose>
                        <xsl:when test="exists(//module[@cmlx:templateRef='orbitalsection']/scalar[@dictRef='cc:orbitaltype'])">
                            <xsl:value-of select="'Unrestricted'"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="'Restricted'"/>                            
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:for-each select="distinct-values(tokenize(string-join(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:method']/scalar,' '), '[ /\(\):,]+'))">
                        <xsl:variable name="spinTypeFromMethod" select="gaussian:methodSpinType(.)"/>            
                        <xsl:if test="compare($spinTypeFromMethod,'')!=0">
                            <xsl:value-of select="$spinTypeFromMethod"/>
                        </xsl:if>  
                        <xsl:value-of select="' '"/>
                    </xsl:for-each>                          
             </xsl:otherwise>
            </xsl:choose>     
        </xsl:variable>
cml:program.name&#x9;<xsl:value-of select="tokenize($program,'[\s]+')[1]"/>
cml:program.version&#x9;<xsl:value-of select="tokenize($program,'[\s]+')[2]"/>                                   
        <xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
        </xsl:if>        
        <xsl:for-each select="distinct-values($basis)">
cml:basisset&#x9;<xsl:value-of select="."/>            
        </xsl:for-each>                        
        <xsl:choose>                                            
            <xsl:when test="exists($qmmModule)">              
                    <xsl:variable name="multiplicity" select="tokenize($qmmModule//array[@dictRef='g:multiplicity'],'[\s]')"/>
                    <xsl:variable name="charge"       select="tokenize($qmmModule//array[@dictRef='g:charge'],'[\s]')"/>
                    <xsl:for-each select="1 to count($multiplicity)">
                        <xsl:variable name="outerIndex" select="."/> 
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity[$outerIndex]"/>
cml:charge&#x9;<xsl:value-of select="$charge[$outerIndex]"/>
                    </xsl:for-each>                                                                        
            </xsl:when>
            <xsl:otherwise>                    
                    <xsl:choose>
                        <xsl:when test="exists($zmatModule/molecule)">
cml:charge&#9;<xsl:value-of select="$zmatModule/molecule/@formalCharge"/>
cml:multiplicity&#x9;<xsl:value-of select="$zmatModule/molecule/@spinMultiplicity"/>
                        </xsl:when>
                        <xsl:when test="exists($zmataModule)">
cml:charge&#9;<xsl:value-of select="$zmataModule//scalar[@dictRef='x:formalCharge']"/>
cml:multiplicity&#x9;<xsl:value-of select="$zmataModule//scalar[@dictRef='x:multiplicity']"/>                                            
                        </xsl:when>                                        
                        <xsl:otherwise>
cml:charge&#9;<xsl:value-of select="$charge"/>
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity"/>                                                                                                                                   
                        </xsl:otherwise>
                    </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>                    
        <xsl:choose>
            <xsl:when test="exists($spinType) and compare($spinType,'') != 0">
                <xsl:for-each select="distinct-values($spinType)">
cml:spintype&#9;<xsl:value-of select="."/>    
                </xsl:for-each>                               
            </xsl:when>
            <xsl:otherwise>
cml:spintype&#9;Restricted
            </xsl:otherwise>                
        </xsl:choose>
        <xsl:for-each select="$shellTypes">            
cml:shelltype&#x9;<xsl:value-of select="."/>
        </xsl:for-each>
        <xsl:if test="exists($energy)">
cml:energy.value&#9;<xsl:value-of select="($energy)[last()]"/>
cml:energy.units&#9;<xsl:value-of select="helper:printUnitSymbol(($energy)[last()]/@units)"/>
        </xsl:if>
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation((//formula/@concise)[1])"/>
        <xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>
cml:calculationtype&#9;<xsl:value-of select="gaussian:getCalcType($isOptimization,$hasStationaryPoint,$hasMinimum, $isEET)"/>
        <!-- HasvibrationalFrequencies -->
        <xsl:variable name="hasVibrationalFrequencies" select="exists(//module[@cmlx:templateRef='l716.freq.chunkx' and @dictRef='cc:vibrations'])"/>
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrationalFrequencies"/>
        <!-- Method -->
        <xsl:for-each select="distinct-values(tokenize(string-join(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:method']/scalar,' '), '[ /\(\):,]+'))">
            <xsl:variable name="candidate" select="."/>            
            <xsl:if test="gaussian:isMethod($candidate)">
cml:method&#9;<xsl:value-of select="$candidate"/>
            </xsl:if>            
        </xsl:for-each>
        <!-- HasSolvent -->        
        <xsl:variable name="hasSolvent" select="exists(//module[@cmlx:templateRef='l301.pcm.standard'])"/>            
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:numberofjobs&#9;<xsl:value-of select="count(//module[@dictRef='cc:jobList']/module[@dictRef='cc:job'])"/>       
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>        
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>
        <!-- Create provided metadata -->
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>        
    </xsl:template>
       
    <xsl:function name="helper:getGaussianPotentialEnergy">
        <xsl:param name="base"/>                       
        <xsl:variable name="archiveEnergies" select="$base//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@dictRef='g:archive.namevalue']"/>
        <xsl:variable name="method">            
            <xsl:if test="exists($base//scalar[@dictRef='g:rbhflyp'])">
                <xsl:variable name="hf" select="($base//scalar[@dictRef='g:rbhflyp'])[last()]"/>                
                
                <xsl:element name="energy">
                    <xsl:attribute name="method"  select="'hf'" />
                    <xsl:attribute name="value" select="$hf/text()" />
                    <xsl:attribute name="units" select="$hf/@units" />
                    <xsl:value-of select="$hf/text()"/>
                </xsl:element>
            </xsl:if>
            
            <xsl:if test="$archiveEnergies != ''">
                <xsl:for-each select="$archiveEnergies/scalar">
                    <xsl:variable name="field" select="."/>
                    <xsl:if test="matches($field,'.*=.*')">
                        <xsl:variable name="splittedField" select="tokenize($field,'=')"/>
                        <xsl:if test="gaussian:isMethod($splittedField[1])">
                            <xsl:element name="energy">
                                <xsl:attribute name="method"  select="$splittedField[1]"/>
                                <xsl:attribute name="value" select="$splittedField[2]"/>
                                <xsl:attribute name="units" select="'nonsi:hartree'"/>
                                <xsl:value-of select="$splittedField[2]"/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
        </xsl:variable>
        
        <xsl:if test="exists($method/*:energy)">
            <xsl:copy-of select="$method/*:energy[last()]"/>     <!-- Capture last (and more accurate) method energy -->                       
        </xsl:if>        
    </xsl:function>
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>