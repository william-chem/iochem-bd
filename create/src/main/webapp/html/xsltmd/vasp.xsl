<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"     
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"        
    xmlns:vasp="https://www.vasp.at/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"    
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>Jan 23, 2014</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>   
    <xsl:param name="title"/>
    <xsl:param name="description"/>
    <xsl:param name="hasmolecularorbitals"/>
    
    <xsl:output method="text" indent="no"/>
    <xsl:strip-space elements="*"/>
    <xsl:include href="../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../xslt/helper/chemistry/vasp.xsl"/>    
    <xsl:template match="/">
        <xsl:variable name="program" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:program']/scalar)[last()]"/>
        <xsl:variable name="programVersion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:programVersion']/scalar)[last()]"/>
        <xsl:variable name="programSubversion" select="(//module[@dictRef='cc:environment']/parameterList/parameter[@dictRef='cc:subversion']/scalar)[last()]"/>
        <xsl:variable name="basis" select="tokenize(//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='atominfo']/module[@cmlx:templateRef='atomtypes']/array[@dictRef='v:pseudopotential'],'\|')"/>  
        <xsl:variable name="multiplicity" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='electronic']/module[@cmlx:templateRef='electronic.spin']/scalar[@dictRef='v:nupdown']"/>
        <xsl:variable name="ispin" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ispin']/scalar"/>
        <xsl:variable name="shellType" select="vasp:getShellType($ispin)"/>
        <xsl:variable name="finalEnergy" select="tokenize(//module[@id='job'][1]/module[@dictRef='cc:finalization']//property[@dictRef='cc:energies']/module[@cmlx:templateRef='energies']/*[@dictRef='cc:e0'], '\s+')[last()]"/>
        
        <xsl:variable name="atomArray" select="(//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule/atomArray"/>
        <xsl:variable name="formula" select="helper:calculateHillNotationFormula($atomArray)"/>
        <xsl:variable name="formulaSmiles" select="(//formula[@convention='daylight:smiles']/@inline)[last()]"/>
        <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])"/>
       
       
        <!-- Functional calculation -->   
        <xsl:variable name="gga"      select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:gga']/scalar"/>
        <xsl:variable name="lhfcalc"  select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:lhfcalc']/scalar"/>
        <xsl:variable name="ldau"     select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ldau']/scalar"/>
        
        <!-- Fields not captured, need to code its capture -->
        <xsl:variable name="aggac"    select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aggac']"/>
        <xsl:variable name="hfscreen" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:hfscreen']"/>    
        <xsl:variable name="luseVdw"  select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:luseVdw']"/>
        <xsl:variable name="zabVdw"   select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:zabVdw']"/>
        <xsl:variable name="param1"   select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:param1']"/>    
        <xsl:variable name="param2"   select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:param2']"/>
        
        <xsl:variable name="aexx" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aexx']"/>
        <xsl:variable name="aggax" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aggax']"/>
        <xsl:variable name="aldac" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aldac']"/>
        <xsl:variable name="functional" select="vasp:getFunctional($gga,$lhfcalc,$aggac,$hfscreen,$luseVdw,$zabVdw, $param1, $param2, $ldau, $aexx, $aggax, $aldac)"/>
                
       
      
        <!-- Functional calculation -->
        <xsl:variable name="parameterList" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList"/>
        <xsl:variable name="gga"      select="$parameterList/parameter[@dictRef='v:gga']/scalar"/>
        <xsl:variable name="lhfcalc"  select="$parameterList/parameter[@dictRef='v:lhfcalc']/scalar"/>
        <xsl:variable name="ldau"     select="$parameterList/parameter[@dictRef='v:ldau']/scalar"/>             
        <xsl:variable name="aggac"    select="$parameterList/parameter[@dictRef='v:aggac']/scalar"/>
        <xsl:variable name="hfscreen" select="$parameterList/parameter[@dictRef='v:hfscreen']/scalar"/>    
        <xsl:variable name="luseVdw"  select="$parameterList/parameter[@dictRef='v:luseVdw']/scalar"/>
        <xsl:variable name="zabVdw"   select="$parameterList/parameter[@dictRef='v:zabVdw']/scalar"/>
        <xsl:variable name="param1"   select="$parameterList/parameter[@dictRef='v:param1']/scalar"/>    
        <xsl:variable name="param2"   select="$parameterList/parameter[@dictRef='v:param2']/scalar"/>        
        <xsl:variable name="aexx"     select="$parameterList/parameter[@dictRef='v:aexx']/scalar"/>
        <xsl:variable name="aggax"    select="$parameterList/parameter[@dictRef='v:aggax']/scalar"/>
        <xsl:variable name="aldac"    select="$parameterList/parameter[@dictRef='v:aldac']/scalar"/>
        <xsl:variable name="functional" select="vasp:getFunctional($gga,$lhfcalc,$aggac,$hfscreen,$luseVdw,$zabVdw, $param1, $param2, $ldau, $aexx, $aggax, $aldac)"/>
        
        <xsl:variable name="ibrion" select="$parameterList/parameter[@dictRef='v:ibrion']/scalar"/>    
        <xsl:variable name="calcType" select="vasp:getCalcType($ibrion)"/>            
        
        <xsl:variable name="hasSolvent" select="'false'"/>
        <xsl:variable name="numberOfJobs" select="count(//module[@dictRef='cc:jobList']/module[@dictRef='cc:job'])"/>
   
        <!-- Print metadata fields -->
        
        <xsl:if test="exists($program)">
cml:program.name&#x9;<xsl:value-of select="$program"/>
cml:program.version&#x9;<xsl:value-of select="$programVersion"/>                                   
        </xsl:if>  
        <xsl:if test="exists($programSubversion)">
cml:program.other&#x9;<xsl:value-of select="$programSubversion"/>
        </xsl:if>        
        <xsl:for-each select="distinct-values($basis)">
            <xsl:if test=". != ''">
cml:basisset&#x9;<xsl:value-of select="."/>    
            </xsl:if>                            
        </xsl:for-each>                                    
        <xsl:if test="exists($multiplicity) and $multiplicity &gt; -1">
cml:multiplicity&#x9;<xsl:value-of select="$multiplicity"/>    
        </xsl:if>        
cml:shelltype&#x9;<xsl:value-of select="$shellType"/>        
        <xsl:if test="exists($finalEnergy)">
cml:energy.value&#9;<xsl:value-of select="$finalEnergy"/>
cml:energy.units&#9;<xsl:value-of select="helper:printUnitSymbol((//module[@id='job'][1]/module[@dictRef='cc:finalization']//property[@dictRef='cc:energies']/module[@cmlx:templateRef='energies']/*[@dictRef='cc:e0'])[last()]/@units)"/>
        </xsl:if>        
cml:formula.generic&#9;<xsl:value-of select="helper:normalizeHillNotation((//formula/@concise)[1])"/>
        <xsl:if test="exists($formulaSmiles)">
cml:formula.smiles&#9;<xsl:value-of select="$formulaSmiles"/>
        </xsl:if>        
cml:calculationtype&#9;<xsl:value-of select="$calcType"/>              
cml:hasvibrationalfrequencies&#9;<xsl:value-of select="$hasVibrations"/>
cml:method&#9;DFT                                 
cml:hassolvent&#9;<xsl:value-of select="$hasSolvent"/>
cml:numberofjobs&#9;<xsl:value-of select="$numberOfJobs"/>       
cml:hasmolecularorbitals&#9;<xsl:value-of select="boolean($hasmolecularorbitals)"/>        
dcterms:created&#9;<xsl:value-of select="current-dateTime()"/>
dcterms:issued&#9;<xsl:value-of select="current-dateTime()"/>        
        <!-- Create provided metadata -->       
        <xsl:if test="exists($title)">
dc:title&#9;<xsl:value-of select="$title"/>            
        </xsl:if>
        <xsl:if test="exists($description)">
dc:description&#9;<xsl:value-of select="$description"/>            
        </xsl:if>
        
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>