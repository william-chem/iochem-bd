<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:turbo="http://www.turbomole.com"    
    xmlns:f="http://fxsl.sf.net/"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs"
    version="2.0">

    <!-- Calculation type related variables -->
    <xsl:variable name="turbo:RestrictedGeometryOptimization" select="'Restricted geometry optimization'" />
    <xsl:variable name="turbo:GeometryOptimization" select="'Geometry optimization'" />
    <xsl:variable name="turbo:ExcitedState" select="'Excited state'"/>
    <xsl:variable name="turbo:SinglePoint" select="'Single point'" />    
    <xsl:variable name="turbo:Minimum" select="'Minimum'"/>
    <xsl:variable name="turbo:TransitionState" select="'TS'" />
    
    <xsl:function name="turbo:getCalcType">
        <xsl:param name="isRestrictedOptimization" as="xs:boolean"/>
        <xsl:param name="isOptimization" as="xs:boolean"/>
        <xsl:param name="isIncomplete" as="xs:boolean"/>
        <xsl:param name="vibrations" as="node()?"/>
        <xsl:param name="statpt" as="node()?"/>
        <xsl:param name="soes" as="node()?"/>
        
        <xsl:variable name="isMinimum" select="not(contains(replace($vibrations/cml:module[@cmlx:templateRef='spectrum']/array[@dictRef='cc:frequency'],'-0.00',''), '-'))"/>
        <xsl:variable name="isExcitedState">           
            <xsl:if test="exists($soes) and number($soes/cml:array[@dictRef='t:irrep']/@size) = 1">
                <xsl:value-of select="$turbo:ExcitedState"/>
                <xsl:text> </xsl:text>
                <xsl:if test="$isRestrictedOptimization or $isOptimization">
                    (<xsl:value-of select="$soes/cml:array[@dictRef='t:lowest']"/><xsl:value-of select="$soes/cml:array[@dictRef='t:irrep']"/>)    
                </xsl:if>                                
            </xsl:if>              
        </xsl:variable>
        
        <xsl:variable name="isTS" select="
            if(exists($statpt) and number($statpt//cml:scalar[@dictRef='t:itrvec']) > 0) then
                $turbo:TransitionState
            else
                ''
            "/>
        <xsl:variable name="itrvecdsd" select="number($statpt//cml:scalar[@dictRef='t:itrvec'])"/>
        <xsl:variable name="calcType" select="
            if($isRestrictedOptimization) then               
                $turbo:RestrictedGeometryOptimization                 
            else if($isOptimization) then
                concat($turbo:GeometryOptimization, ' ', $isTS)
            else 
                $turbo:SinglePoint                
       "/>
        
        
        <xsl:variable name="vibration" select="
            if(exists($vibrations) and not($isRestrictedOptimization) and compare($isTS,'') = 0) then
                if($isMinimum) then 
                    $turbo:Minimum
                else
                    $turbo:TransitionState             
            else ''
        "/>
        <xsl:sequence select="concat($calcType, ' ', $vibration, ' ', $isExcitedState)"/>
       
    </xsl:function>

    <xsl:function name="turbo:getMethod">
        <xsl:param name="soes"/>
        <xsl:param name="methodScalar"/>
        <xsl:variable name="methodsTmp">
            <xsl:for-each select="$methodScalar">
                <xsl:for-each select="tokenize(.,'\s+')">
                    <xsl:element name="method">
                        <xsl:value-of select="upper-case(.)"/>
                        <xsl:text> </xsl:text>
                    </xsl:element>                    
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        <xsl:choose>            
            <xsl:when test="not(exists($methodScalar))">
                <xsl:sequence select="                 
                    if(exists($soes)) then
                        'TDHF'
                    else
                        'HF'                                          
                    ">
                </xsl:sequence>                
            </xsl:when>      
            <xsl:otherwise>
                <xsl:variable name="step1">
                    <xsl:choose>
                        <xsl:when test="contains($methodsTmp, 'RIR12') and contains($methodsTmp, 'MP2') and contains($methodsTmp, 'RICC2')">
                            <xsl:text>MP2-F12 </xsl:text>
                            <xsl:value-of select="replace(replace(replace($methodsTmp,'RIR12', ''), 'MP2', ''), 'RICC2', '')"></xsl:value-of>
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="$methodsTmp"/></xsl:otherwise>
                    </xsl:choose>           
                </xsl:variable>
                
                <xsl:variable name="step2">
                    <xsl:choose>
                        <xsl:when test="contains($step1, 'UHF') and contains($step1, 'DFT')">
                            <xsl:text>U-DFT </xsl:text>
                            <xsl:value-of select="replace(replace($step1,'UHF',''), 'DFT','')"/>
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="$step1"/></xsl:otherwise>
                    </xsl:choose>           
                </xsl:variable>
                
                <xsl:variable name="step3">
                    <xsl:choose>
                        <xsl:when test="contains($step2, 'DFT') and exists($soes)">
                            <xsl:value-of select="replace($step2,'DFT', 'TDDFT')"/>
                        </xsl:when>
                        <xsl:otherwise><xsl:value-of select="$step2"/></xsl:otherwise>
                    </xsl:choose>               
                </xsl:variable>
                
                <xsl:value-of select="$step3"/>                
            </xsl:otherwise>
        </xsl:choose>       
    </xsl:function>

    <xsl:function name="turbo:getBasisPerAtomType">
        <xsl:param name="atomTypesStr"/>
        <xsl:param name="basisStr"/>
        <xsl:param name="contractionStr"/>
        <xsl:variable name="atomTypes" select="tokenize($atomTypesStr ,'\s+')"/>
        <xsl:variable name="basis" select="tokenize($basisStr ,'\s+')"/>
        <xsl:variable name="contraction" select="tokenize($contractionStr ,'\s+')"/>
        <xsl:element name="atomArray">        
            <xsl:for-each select="$atomTypes">
                <xsl:variable name="outerIndex" select="position()"/>
                <xsl:element name="atom">
                    <xsl:attribute name="atomType" select="turbo:correctAtomTypeCase($atomTypes[$outerIndex])"/>
                    <xsl:attribute name="basis"><xsl:value-of select="$basis[$outerIndex]"/></xsl:attribute>
                    <xsl:variable name="contr" select="tokenize(replace($contraction[$outerIndex],'[\[\]]+',''),'\|')"/>
                    <xsl:attribute name="contraction"><xsl:value-of select="concat('(', $contr[2],') / [', $contr[1], ']')"/></xsl:attribute>
                </xsl:element>
            </xsl:for-each>
        </xsl:element>
    </xsl:function>
    
    <xsl:function name="turbo:calculateMultiplicity">                
        <xsl:param name="occupiedAlphaLabels" />
        <xsl:param name="occupiedAlpha" />
        <xsl:param name="occupiedBetaLabels" />
        <xsl:param name="occupiedBeta"/>
        <xsl:variable name="totalOccupiedAlpha" select="sum(turbo:sumRanges($occupiedAlphaLabels, $occupiedAlpha))"/>
        <xsl:variable name="totalOccupiedBeta" select="sum(turbo:sumRanges($occupiedBetaLabels, $occupiedBeta))"/>
        <xsl:variable name="difference" select="$totalOccupiedAlpha - $totalOccupiedBeta"/>               
        <xsl:value-of select="number($difference) + 1"/>        
    </xsl:function>
    
    <xsl:function name="turbo:calculateS2">
        <xsl:param name="multiplicity"/>
        <xsl:variable name="s" select="(number($multiplicity)-1) div 2"/> 
        <xsl:value-of select="$s * ($s + 1)"/>                
    </xsl:function>
  
    <xsl:function name="turbo:correctAtomTypeCase">
        <xsl:param name="atomType"/> 
        <xsl:sequence select="concat(upper-case(substring($atomType,1,1)), lower-case(substring($atomType, 2,2)))"/>             
    </xsl:function>
    
    <xsl:function name="turbo:sumRanges">
        <xsl:param name="labels"/>
        <xsl:param name="ranges"/>
        <xsl:variable name="totalRanges">
            <xsl:for-each select="$ranges">
                <xsl:variable name="index" select="position()"/>
                <xsl:element name="range" namespace="http://www.xml-cml.org/schema">
                    <xsl:attribute name="value" select="turbo:getRangeValue($labels[$index], .)"/>
                </xsl:element>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="sum($totalRanges/cml:range/@value)"/>
    </xsl:function>
    
    <xsl:function name="turbo:getRangeValue">
        <xsl:param name="label"/>
        <xsl:param name="value"/>
        <!-- Depending on the orbital label the number of electrons will be multiplied -->
        <xsl:variable name="multiplier">
            <xsl:choose>
                <xsl:when test="starts-with(lower-case($label), 'a')">
                    <xsl:value-of select="1"/>
                </xsl:when>
                <xsl:when test="starts-with(lower-case($label), 'b')">
                    <xsl:value-of select="1"/>
                </xsl:when>
                <xsl:when test="starts-with(lower-case($label), 'e')">
                    <xsl:value-of select="2"/>
                </xsl:when>
                <xsl:when test="starts-with(lower-case($label), 't')">
                    <xsl:value-of select="3"/>
                </xsl:when>
                <xsl:when test="starts-with(lower-case($label), 'g')">
                    <xsl:value-of select="4"/>
                </xsl:when>                
                <xsl:when test="starts-with(lower-case($label), 'h')">
                    <xsl:value-of select="5"/>
                </xsl:when>                
                <xsl:otherwise><xsl:value-of select="1"/></xsl:otherwise>
            </xsl:choose>
        </xsl:variable>
        
        <xsl:choose>
            <xsl:when test="not(exists($value))">
                0
            </xsl:when>
            <xsl:when test="contains($value,'-')">
                <xsl:variable name="operands" select="tokenize($value,'-')"/>
                <xsl:value-of select="number($multiplier) * (number($operands[2]) + 1 - number($operands[1]))"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="number($multiplier) * $value"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="turbo:getOrbitalRangeValue">
        <xsl:param name="label"/>
        <xsl:param name="rangeLabels" />
        <xsl:param name="rangeValues" />
        
        <xsl:variable name="range">
            <xsl:for-each select="1 to count($rangeLabels)">
                <xsl:variable name="outerIndex" select="."/>
                <xsl:if test="compare($rangeLabels[$outerIndex], $label) = 0" >
                    <xsl:value-of select="turbo:getRangeValue($label, $rangeValues[$outerIndex])"/>
                </xsl:if>            
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:value-of select="if($range != '') then number($range) else 0"/>
    </xsl:function>
    
    
    <xsl:function name="turbo:getNumberOfBasis">
        <xsl:param name="value"/>
        <xsl:param name="label"/>
        <xsl:choose>
            <xsl:when test="compare($label, 'e') = 0 ">
                <xsl:value-of select="number($value) * 2"/>
            </xsl:when>
            <xsl:when test="compare($label, 't') = 0">
                <xsl:value-of select="number($value) * 3"/>
            </xsl:when>
            <xsl:when test="compare($label,'g') = 0">
                <xsl:value-of select="number($value) * 4"/>
            </xsl:when>
            <xsl:when test="compare($label,'h') = 0">
                <xsl:value-of select="number($value) * 5"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="number($value)"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="turbo:translateDispersion">
        <xsl:param name="dispKeyword"/>
        <xsl:choose>
            <xsl:when test="upper-case($dispKeyword) = 'OLDDISP'"><xsl:value-of select="'D1'"/></xsl:when>                
            <xsl:when test="upper-case($dispKeyword) = 'DISP'"><xsl:value-of select="'D2'"/></xsl:when>
            <xsl:when test="upper-case($dispKeyword) = 'DISP3'"><xsl:value-of select="'D3'"/></xsl:when>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="turbo:translateParameters">
        <xsl:param name="parameters"/>
        <xsl:for-each select="$parameters">
            <xsl:choose>
                <xsl:when test="lower-case(.) = 'ri'"><xsl:text>&#32;</xsl:text>ri</xsl:when>
                <xsl:when test="lower-case(.) = 'rij'"><xsl:text>&#32;</xsl:text>ri-j</xsl:when>
                <xsl:when test="lower-case(.) = 'ricc2'"><xsl:text>&#32;</xsl:text>ri</xsl:when>
                <xsl:otherwise><xsl:text>&#32;</xsl:text><xsl:value-of select="."></xsl:value-of></xsl:otherwise>
            </xsl:choose>
        </xsl:for-each>        
    </xsl:function>
    
    <xsl:function name="turbo:translateSCFinstab">
        <xsl:param name="scfinstab"/>
        <xsl:choose>
            <xsl:when test="$scfinstab = 'CIST' or $scfinstab = 'CISS' or $scfinstab = 'UCIS'"> TDA </xsl:when>
            <xsl:when test="$scfinstab = 'RPAS' or $scfinstab = 'RPAT' or $scfinstab = 'URPA'"> RPA </xsl:when>
        </xsl:choose>        
    </xsl:function>
    
    <xsl:function name="turbo:translateGridSize">
        <xsl:param name="gridsize"/>
        <xsl:if test="exists($gridsize)">
            <xsl:text>  gridsize:</xsl:text><xsl:value-of select="$gridsize"/>
        </xsl:if>
    </xsl:function>
    
    <!-- Discarded -->
    <xsl:function name="turbo:sequenceContainsText">
        <xsl:param name="sequence"/>
        <xsl:param name="text"/>
        <xsl:variable name="matches">
            <xsl:for-each select="1 to count($sequence)">
                <xsl:variable name="outerIndex"/>
                <xsl:if test="compare($sequence[$outerIndex],$text) = 0">
                    <xsl:text>true</xsl:text>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:choose>
            <xsl:when test="contains($matches, 'true')">
                <xsl:sequence select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
</xsl:stylesheet>