<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:am="http://www.iochem-bd.org/dictionary/amber/"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    exclude-result-prefixes="xs"
    version="2.0">

    <!-- Calculation type related variables -->
    <xsl:variable name="am:GeometryOptimization">Geometry optimization</xsl:variable>
    <xsl:variable name="am:npt">NPT</xsl:variable>
    <xsl:variable name="am:nvt">NVT</xsl:variable>
    <xsl:variable name="am:nve">NVE</xsl:variable>
    <xsl:variable name="am:na">N/A</xsl:variable>
    
    <xsl:variable name="thermostat" >
        <entry index="0" key="No temperature scaling" />
        <entry index="1" key="Berendsen" />
        <entry index="2"  key="Anderson" />
        <entry index="3"  key="Langevin" />
        <entry index="4"  key="N/A" />
        <entry index="5"  key="Nose–Hoover(chain)–Langevin" />
        <entry index="6" key="Adaptive Langevin" />
        <entry index="7" key="Adaptive Nose-Hoover chain" />
        <entry index="8" key="Adaptive Nose-Hoover chain–Langevin" />
        <entry index="9" key="Optimized-isokinetic integrator" />
        <entry index="10" key="Stochastic Isokinetic Nose-Hoover RESPA integrator" />
        <entry index="11" key="Bussi" />        
    </xsl:variable>
    
    <xsl:variable name="barostat" >     
        <entry index="1" key="Berendsen" />
        <entry index="2"  key="Monte Carlo" />
    </xsl:variable>
    
    <xsl:function name="helper:getMethod">
        <xsl:param name="imin" />
        <xsl:param name="ntp" />
        <xsl:param name="ntt" />
        
        <xsl:choose>
            <xsl:when test="exists($imin) and number($imin) = 1">                
                <xsl:value-of select="$am:GeometryOptimization" />
            </xsl:when>
            <xsl:when test="exists($ntp) and number($ntp) &gt; 0">
                <xsl:value-of select="$am:npt"/>
            </xsl:when>            
            <xsl:otherwise>
                <xsl:choose>
                    <xsl:when test="exists($ntt) and number($ntt) = 0">
                        <xsl:value-of select="$am:nve"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$am:nvt"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="helper:getCouplingMethods">
        <xsl:param name="ntt" />
        <xsl:param name="ntp" />
        <xsl:param name="bar" />
                
        <xsl:choose>
            <xsl:when test="not(exists($ntt) or $ntt = 0)">
                <xsl:value-of select="'T= No temperature scaling '"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="concat('T=', $thermostat/*:entry[@index = $ntt]/@key), ' '"/>    
            </xsl:otherwise>
        </xsl:choose>
        
        <xsl:choose>
            <xsl:when test="not(exists($ntp) or $ntp = 0)">
                <xsl:value-of select="'P= No pressure scaling '"/>                                
            </xsl:when>
            <xsl:when test="exists($bar)">
                <xsl:value-of select="concat('P=', $barostat/*:entry[@index = $bar]/@key), ' '"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="'P= Berendsen'"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="helper:hasSolvent">
        <xsl:param name="initialization" />        
        <xsl:variable name="residue" select="$initialization/cml:module[@dictRef='cc:userDefinedModule']/cml:module[@cmlx:templateRef='residue']/cml:array[@dictRef='am:residue']"/>
        <xsl:value-of select="exists($residue) and matches($residue/text(),'.*(\|HOH|\|WAT|\|H20|\|TIP|\|SOL).*')"/>        
    </xsl:function>
      
    
</xsl:stylesheet>