<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:siesta="http://www.iochem-bd.org/dictionary/siesta/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    version="2.0">
    <!-- exclude-result-prefixes="xs qex cml cmlx" -->

    <xsl:variable name="siestaUnitsSymbolMap">
        <entry key="Bohr">Bohr</entry>
        <entry key="Ang">Å</entry>
        <entry key="Ang__2">Å²</entry>
        <entry key="Ang__3">Å³</entry>
        <entry key="Ry">Ry</entry>
        <entry key="Ry_Bohr">Ry / Bohr</entry>
        <entry key="Ry_Bohr__3">Ry / Bohr³</entry>
        <entry key="eAng_3">e * Å³</entry>
        <entry key="eVeAng_3">e * eV * Å³</entry>
        <entry key="eSpin">e * spin (1/2)</entry>
        <entry key="e__">e</entry>
        <entry key="ha">ha</entry>
        <entry key="elementaryCharge">e</entry>
        <entry key="eV">eV</entry>
        <entry key="countable"></entry>
        <entry key="dimensionless"></entry>
    </xsl:variable>

    <!-- Simple map example from http://stackoverflow.com/questions/3626118/xslt-creating-a-map-in-xslt -->
    <xsl:function name="siesta:convertSymbol">
        <xsl:param name="unitQName" as="xs:string?"/>
        <xsl:choose>
            <xsl:when test="not(exists($unitQName)) or compare($unitQName,'') = 0 ">
                <xsl:sequence select="''"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:sequence select="$siestaUnitsSymbolMap/entry[@key=normalize-space($unitQName)]"></xsl:sequence>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
</xsl:stylesheet>
