<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:molcas="http://www.iochem-bd.org/dictionary/molcas/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs xd cml ckbk molcas helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Mar 31, 2016</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:include href="helper/chemistry/molcas.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>    
    <xsl:param name="browseurl"/>
    <xsl:param name="jcampdxurl"/>
    <xsl:param name="moldenurl"/>
   
    <xsl:variable name="inputLines" select="(//cml:module[@id='m:inputlines'])[1]/cml:scalar/text()"/>
    <xsl:variable name="isRestrictedOpt" select="exists(//cml:module[@cmlx:templateRef='constraint'])" />
    <xsl:variable name="isOptimization" select="molcas:isOptimization($inputLines)" />    
    <xsl:variable name="isTS" select="molcas:isTS($inputLines)"/>
    <xsl:variable name="hasLastEnergySection" select="exists(//cml:module[@id='calculation']//cml:module[@cmlx:templateRef='module' and @role='last_energy'])"/>
    <xsl:variable name="hasOrbitals" select="exists($moldenurl) and ($moldenurl != '')" />    
    
    <xsl:variable name="isIncomplete" select="molcas:isIncomplete(//cml:module[@cmlx:templateRef='energy.statistics'], $isOptimization, $hasLastEnergySection )" />
    <xsl:variable name="calcType" select="molcas:getCalcType($isRestrictedOpt, $isOptimization, $isTS, $isIncomplete) "/>
    <xsl:variable name="isUnrestricted" select="exists((//cml:module[@id='m:inputlines'])[1]/cml:scalar[contains(upper-case(text()),$molcas:unrestrictedRegex)])"/>
    
    <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])" />
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    <xsl:variable name="subversionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programSubversion']"/>

    <xsl:variable name="spin">
        <xsl:if test="exists((//cml:module[@cmlx:templateRef='wave.specs']/cml:scalar[@dictRef='m:spinquantumnum'])[last()])">
            <xsl:value-of select="number((//cml:module[@cmlx:templateRef='wave.specs']/cml:scalar[@dictRef='m:spinquantumnum'])[last()]) * 2 + 1 "/>
        </xsl:if>
        <xsl:if test="exists((//cml:module[@cmlx:templateRef='scf-ksdft']/cml:scalar[@dictRef='m:spin'])[last()])">
            <xsl:value-of select="number((//cml:module[@cmlx:templateRef='scf-ksdft']/cml:scalar[@dictRef='m:spin'])[last()]) * 2 + 1 "/>
        </xsl:if>                       
    </xsl:variable>               
    <xsl:variable name="multiplicity" select="substring-before(concat(string($spin),'.'),'.')"/>         
    <xsl:variable name="charge">
        <xsl:choose>
            <xsl:when test="exists((//cml:module[@id='job']/cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='m:charge'])[last()])">
                <xsl:value-of select="(//cml:module[@id='job']/cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='m:charge'])[last()]"/>
            </xsl:when>
            <xsl:otherwise>                           
                <xsl:value-of select="(//cml:module[@cmlx:templateRef='mulliken']//cml:scalar[@dictRef='cc:charge'])[last()]"/>                    
            </xsl:otherwise>
        </xsl:choose>            
    </xsl:variable>
    <xsl:variable name="functional" select="if(exists(//cml:module[@cmlx:templateRef='scf-ksdft']/cml:scalar[@dictRef='m:program' and text() = 'KS-DFT'])) then
                                                distinct-values(//cml:module[@dictRef='cc:initialization']//cml:parameter[@dictRef='cc:functional']/cml:scalar)
                                            else 
                                                ''"/>    
    <xsl:variable name="modules" select="//cml:module[@id='finalization']/cml:propertyList/cml:property[@dictRef='m:module']/cml:array/text()"/>
    <xsl:variable name="ksdft" select="(//cml:module[@cmlx:templateRef='scf-ksdft'])[last()]/cml:scalar[@dictRef='m:program']"/>
    <xsl:variable name="wavespecs" select="(//cml:module[@cmlx:templateRef='wave.specs'])[last()]"/>
   
    <xsl:variable name="methods" select="molcas:getMethods($modules, $ksdft,$wavespecs)"/>

    <xsl:variable name="quote">"</xsl:variable>
    <xsl:variable name="quote_escaped">\\"</xsl:variable>

    <xsl:variable name="singlequote">'</xsl:variable>
    <xsl:variable name="singlequote_escaped">\\'</xsl:variable>

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"></script>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>            
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-ui.min.js"/>
                <xsl:if test="$hasVibrations or $hasOrbitals">
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"></script>                    
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>                            
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>
                </xsl:if>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />                                                
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                                                                                 
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>
                        </div>
                    </div>
                    <div class="row bottom-buffer">
                       
                        <div class="col-md-12">
                            <xsl:call-template name="atomicCoordinates">
                                <xsl:with-param name="molecule" select="(//cml:module[@dictRef='cc:finalization']/cml:molecule)[last()]"/>
                            </xsl:call-template>                            
                            <xsl:call-template name="molecularInfo"/>                            
                            
                            <xsl:for-each select="//cml:module[@dictRef='cc:job']">                                    
                                    <xsl:variable name="jobContent">
                                        <!-- Are we working with a geometry opt.? Then only print modules from last_energy onward-->
                                        <xsl:for-each select="if(not(exists(.//cml:module[@role='last_energy'])))           
                                                            then ./cml:module[@id='calculation']//cml:module[@cmlx:templateRef='module'] 
                                                            else ./cml:module[@id='calculation']//cml:module[@cmlx:templateRef='module' and @role='last_energy']">
                                        <xsl:variable name="moduleName" select="upper-case(./@id)"/>
                                        <xsl:variable name="currModuleName" select="molcas:filterModuleName($moduleName, $methods)"/>
                                        <xsl:if test="not($currModuleName='SLAPAF')">
                                            <xsl:variable name="moduleContent">
                                                <xsl:call-template name="wavefunctionspecs"/>
                                                <xsl:call-template name="orbitalSpecsSymmetrySpecies"/>
                                                <xsl:call-template name="ciExpansion"/>
                                                <xsl:call-template name="waveFunctionEnergies"/>
                                                <xsl:call-template name="waveFunctionTopWeightsValues"/>                                                
                                                <xsl:call-template name="naturalOccupation" />
                                                <xsl:call-template name="mullikenSpinPopulationMatrix"/>                                                
                                                <xsl:call-template name="dipoleMomentMatrix"/>                                           
                                                <xsl:call-template name="populationAnalysis"/>
                                                <xsl:call-template name="finalCASPT2Energies"/>
                                                <xsl:call-template name="finalSCFEnergy" />
                                                <xsl:call-template name="hzero"/>                                                
                                            </xsl:variable>
                                            <!-- We will discard modules without content -->
                                            <xsl:choose>                                                
                                                <xsl:when test="string-length($moduleContent) > 0 ">
                                                    <xsl:choose>
                                                        <xsl:when test="($currModuleName = 'SCF' and exists(following::cml:module[@id='SCF']))"></xsl:when>
                                                        <xsl:otherwise>
                                                            <h4>
                                                                <xsl:value-of select="$currModuleName"/>
                                                            </h4>
                                                            <xsl:copy-of select="$moduleContent"/>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:when>
                                            </xsl:choose>
                                            </xsl:if>
                                        </xsl:for-each>
                                        <xsl:call-template name="vibrations"/>
                                        <xsl:call-template name="vibrationalFrequencies" />           
                                        <xsl:call-template name="multipoleExpansion" />
                                        <xsl:call-template name="loprop" />                                        
                                    </xsl:variable>
                                    
                                <xsl:if test="string-length($jobContent) > 0">
                                    <div id="module-{generate-id(.)}">                                
                                        <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>                                        
                                        <xsl:copy-of select="$jobContent"/>
                                    </div>    
                                </xsl:if>    
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>
                        </div>                        
                    </div>   
                    
                    <!-- If there are molden orbital files, load its viewer -->
                    <xsl:if test="$hasOrbitals">                                                
                        <xsl:copy-of select="helper:appendMoldenViewerCode($moldenurl, $webrootpath)"/>                        
                    </xsl:if>
                    
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            <!-- $('div#module-' + moduleID + ' .collapse').collapse('show');//-->
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });                        
                            
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                                                     
                            $.unblockUI();                             
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf', 'print' ]
                             } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();
                        }
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                        $(this).find('.dataTable').each(function(index){
                            var oTable = $(this).dataTable();
                            $(oTable).css({ width: $(oTable).parent().width() });
                            oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                    </script>
                </div>
            </body>
        </html>
    </xsl:template>
  
    <!-- General info -->  
    <xsl:template name="generalInfo">
    <div class="page-header">
        <h3>GENERAL INFO</h3>
    </div>        
    <table>
        <xsl:if test="$title">
            <tr>
                <td>Title:</td>
                <td>
                    <xsl:value-of select="$title"/>
                </td>
            </tr>                                   
        </xsl:if>
        <xsl:if test="$browseurl">
            <tr>
                <td>Browse item:</td>
                <td>
                    <a href="{$browseurl}">
                        <xsl:value-of select="$browseurl"/>
                    </a>
                </td>
            </tr>
        </xsl:if>   
        <tr>
            <td>Program:</td>
            <td>
                <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                <xsl:text> </xsl:text>
                <xsl:value-of select="$versionParameter/scalar/text()" />
                <xsl:text> - </xsl:text>
                <xsl:value-of select="$subversionParameter/scalar/text()" />
            </td>
        </tr>
        <xsl:if test="$author">
            <tr>
                <td>Author:</td>
                <td>
                    <xsl:value-of select="$author"/>
                </td>
            </tr>
        </xsl:if>
        <tr>
            <td>Formula:</td>
            <td>
                <xsl:value-of select="(//formula/@concise)[1]"/>
            </td>
        </tr>
      
        <tr>               
            <td>Calculation type:</td>
            <td>                                                                 
                <xsl:value-of select="$calcType"/>                        
            </td>                    
        </tr>
        <tr>
            <td>Method:</td>
            <td>
                <xsl:variable name="modules" select="//cml:module[@id='finalization']/cml:propertyList/cml:property[@dictRef='m:module']/cml:array/text()"/>                               
                <xsl:variable name="ksdft" select="(//cml:module[@cmlx:templateRef='scf-ksdft'])[last()]/cml:scalar[@dictRef='m:program']"/>
                <xsl:variable name="wavespecs" select="(//cml:module[@cmlx:templateRef='wave.specs'])[last()]"/>                
                
                <xsl:value-of select="molcas:getMethods($modules, $ksdft,$wavespecs)"/>
                <xsl:if test="exists(.//cml:module[@cmlx:templateRef='cchc']/cml:scalar[@dictRef='m:e2mp2energy'])"> MP2 </xsl:if>
                <xsl:if test="exists(.//cml:module[@cmlx:templateRef='cchc']/cml:scalar[@dictRef='m:e2ccsdenergy'])"> CCSD </xsl:if>
                <xsl:if test="exists(.//cml:module[@cmlx:templateRef='ccsdt']/cml:scalar[@dictRef='m:ccsdtcorrenergy'])"> CCSD(T) </xsl:if>
                        
                <xsl:if test="$functional != ''">
                    ( <xsl:for-each select="$functional"><xsl:value-of select="."/> </xsl:for-each> )
                </xsl:if>
            </td>                    
        </tr>                   
    </table>
    </xsl:template>
 
    <!-- Molecular info -->
    <xsl:template name="molecularInfo">         
        <div class="row bottom-buffer">
            <div class="col-md-12 col-sm-12">
                <h3>MOLECULAR INFO</h3>
                <xsl:call-template name="symmetry"/>
                <br/>
                <xsl:call-template name="chargemultiplicity"/>
                <xsl:call-template name="sewardlines"/>
                <xsl:call-template name="atomicDistances"/> 
                <xsl:call-template name="restrictions"/>
            </div>
        </div>
    </xsl:template>
     
    <xsl:template name="atomicCoordinates">
        <xsl:param name="molecule"/>
        <xsl:variable name="basisSections" select="(//cml:module[@id='initialization']//cml:module[@cmlx:templateRef='basisset'])[1]"/>
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinates-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;] 
                    <xsl:if test="$isOptimization">
                        <xsl:choose>
                            <xsl:when test="not($isIncomplete)">
                                <small>(optimized)</small>
                            </xsl:when>
                            <xsl:otherwise>                                                                               
                                <small><strong>(calculation did not converge)</strong></small>        
                            </xsl:otherwise>
                        </xsl:choose>                                                           
                    </xsl:if>
                </h4>
            </div>
            <div id="atomicCoordinates-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row bottom-buffer">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <tr>
                                        <td><xsl:value-of select="$title"/></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                        <xsl:variable name="id" select="@id"/>
                                        <tr>
                                            <td><xsl:value-of select="$elementType"/></td>
                                            <td><xsl:value-of select="format-number(@x3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@y3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@z3,'#0.000000')"/></td>
                                        </tr>
                                    </xsl:for-each>                                       
                                </tbody>                            
                            </table>                              
                            <script type="text/javascript">                                                                  
                                $(document).ready(function() {
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                });
                                function getXYZ(){
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons( table, {
                                        buttons: [ {
                                            extend: 'copyHtml5',
                                            text: 'XYZ'
                                            }
                                        ]
                                    });
                                    table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));                                    
                                    $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                }     
                            </script>                                                        
                            
                            <table class="display" id="atomicCoordinatesT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Atom</th>
                                        <th rowspan="2"></th>
                                        <th rowspan="2" style="text-align: center">x</th>
                                        <th rowspan="2" style="text-align: center">y</th>
                                        <th rowspan="2" style="text-align: center">z</th>
                                        <th colspan="2" style="text-align: center">basis set</th>                                        
                                    </tr>
                                    <tr>
                                        <th>TYPE</th>
                                        <th>(primitive) / [contracted]</th>
                                    </tr>
                                </thead>
                            </table>
                                                        
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#atomicCoordinatesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable( {
                                "aaData": [
                                <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                    <xsl:variable name="outerIndex" select="position()"/>
                                    <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                    <xsl:variable name="id" select="@id"/>
                                    
                                    <xsl:variable name="atomTypeChars" select="concat(helper:atomType2Number($elementType),'.')" />
                                    <xsl:variable name="shells" select="($basisSections//cml:module[@cmlx:templateRef='valence' and cml:scalar[@dictRef='m:actualCharge' and starts-with(text(),$atomTypeChars) ]]/cml:list[@cmlx:templateRef='shells'])[1]"/>
                                    <xsl:variable name="shell" select="tokenize($shells/cml:array[@dictRef='m:shell'],'\s+')"/>
                                    <xsl:variable name="nprim" select="tokenize($shells/cml:array[@dictRef='m:nprim'],'\s+')"/>
                                    <xsl:variable name="nbasis" select="tokenize($shells/cml:array[@dictRef='m:nbasis'],'\s+')"/>
                                    <xsl:variable name="primitive" >
                                        <xsl:for-each select="1 to count($shell)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <xsl:value-of select="$nprim[$outerIndex]"/><xsl:value-of select="upper-case($shell[$outerIndex])"/>
                                        </xsl:for-each>                                                                                                                    
                                    </xsl:variable>
                                    <xsl:variable name="contracted">
                                        <xsl:for-each select="1 to count($shell)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <xsl:value-of select="$nbasis[$outerIndex]"/><xsl:value-of select="upper-case($shell[$outerIndex])"/>
                                        </xsl:for-each>                                                                                                                                                            
                                    </xsl:variable>

                                    <xsl:variable name="basisLine" select="($basisSections/cml:module[@cmlx:templateRef='section' and cml:array[@dictRef='m:basis' and starts-with(text(),concat($elementType,'|'))]])[1]" />
                                    <xsl:variable name="elementBasis" >
                                        <xsl:choose>
                                            <xsl:when test="exists($basisLine)">
                                                <xsl:value-of select="tokenize($basisLine/cml:array[@dictRef='m:basis']/text(),'\|+')[2]"/>
                                            </xsl:when>
                                            <xsl:when test="exists(//cml:module[@id='initialization']//cml:module[@id='inputbasis']//cml:array[@dictRef='m:basis1' and starts-with(upper-case(text()),upper-case(concat($elementType, '|')))])">
                                                <xsl:variable name="inputbasis" select="//cml:module[@id='initialization']//cml:module[@id='inputbasis']//cml:array[@dictRef='m:basis1' and starts-with(upper-case(text()),upper-case(concat($elementType, '|')))]"/>
                                                <xsl:value-of select="upper-case(tokenize($inputbasis,'\|+')[2])"/>                                                
                                            </xsl:when>
                                            <xsl:when test="exists(//cml:module[@id='initialization']//cml:module[@id='m:inputlines']/cml:scalar[matches(upper-case(text()),'\s*BASIS\sSET\s*=.*')])">
                                                <xsl:variable name="line" select="//cml:module[@id='initialization']//cml:module[@id='m:inputlines']/cml:scalar[matches(upper-case(text()),'\s*BASIS\sSET\s*=.*')]" />
                                                <xsl:value-of select="molcas:findBasisForElementFromInput($elementType, $line)"/>
                                            </xsl:when>
                                            <xsl:otherwise>                                                
                                                <xsl:variable name="defaultBasis" select="//cml:module[@id='initialization']//cml:module[@id='m:inputlines']/cml:scalar[matches(upper-case(text()),'\s*BASIS\s*=.*')]" />
                                                <xsl:value-of select="upper-case(helper:trim(tokenize($defaultBasis,'=')[2]))"/>                                                           
                                            </xsl:otherwise>
                                        </xsl:choose>                                        
                                    </xsl:variable>
                                    [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.000000')"/>","<xsl:value-of select="format-number(@y3,'#0.000000')"/>","<xsl:value-of select="format-number(@z3,'#0.000000')"/>","<xsl:value-of select="$elementBasis"/>","[<xsl:value-of select="lower-case($primitive)"/>/<xsl:value-of select="lower-case($contracted)"/>]"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                </xsl:for-each>    
                                ],
                                "aoColumns": [
                                null,
                                null,
                                { "sClass": "right" },
                                { "sClass": "right" },
                                { "sClass": "right" },
                                null,
                                null                           
                                ],   
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                                } );   
                                } );                                       
                            </script>                          
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>   
    <xsl:function name="molcas:findBasisForElementFromInput">
        <xsl:param name="elementType"/>
        <xsl:param name="line"/>
        
        <xsl:for-each select="tokenize( (tokenize($line,'=')[2]) , ',')">
            <xsl:variable name="subline" select="."/>                        
            <xsl:if test="matches(upper-case($subline), concat('\s*',upper-case($elementType), '\..*'))">
                <xsl:variable name="chunk" select="tokenize($subline, '\.')"/>                    
                <xsl:value-of select="$chunk[2]"/>
                <xsl:if test="exists($chunk[3])"><xsl:text>.</xsl:text><xsl:value-of select="$chunk[3]"/></xsl:if>
            </xsl:if>
        </xsl:for-each>
       
    </xsl:function>
   
    <xsl:template name="restrictions">        
       <xsl:variable name="constraint" select="(.//cml:module[@cmlx:templateRef='constraint'])[1]" />
       <xsl:if test="exists($constraint)">
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#restrictions-{generate-id($constraint)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Restrictions in the Geometry Optimization
                </h4>
            </div>            
            <div id="restrictions-{generate-id($constraint)}" class="panel-collapse collapse">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-md-6 col-sm-12">                                
                            <table class="display" id="restrictionsT-{generate-id($constraint)}"></table>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $("table#restrictionsT-<xsl:value-of select="generate-id($constraint)"/>").dataTable({
                                        "aaData" : [  
                                        
                                    <xsl:variable name="numrestrictions" select="count($constraint/cml:list[cml:scalar[@dictRef='m:restriction']])" />
                                        
                                    <xsl:for-each select="$constraint/cml:list[cml:scalar[@dictRef='m:restriction']]">                                                  
                                            <xsl:variable name="outerIndex" select="position()"/>                                                
                                            <xsl:variable name="currentRestriction" select="."/>
                                            <xsl:variable name="currentValue" select="($constraint/cml:list[cml:scalar[@dictRef='m:restrictionvalue']])[$outerIndex]" />
                                            <xsl:variable name="serial" select="tokenize($currentRestriction/cml:scalar[@dictRef='m:restrictionextras'],'\s+')" />                                            
                                            <xsl:choose>
                                                <xsl:when test="compare($currentRestriction/cml:scalar[@dictRef='m:restriction']/text(),'BOND') = 0">
                                                    [ 'Bond', '<xsl:value-of select="$serial[1]"/>', '<xsl:value-of select="$serial[2]"/>', '','','','<xsl:value-of select="$currentValue/cml:scalar[@dictRef='m:restrictionvalue']"/>','<xsl:value-of select="molcas:constrainUnitReplace($currentValue/cml:scalar[@dictRef='m:restrictionunit'])"/>']<xsl:if test="$outerIndex &lt; $numrestrictions">,</xsl:if>        
                                                </xsl:when>          
                                                <xsl:when test="compare($currentRestriction/cml:scalar[@dictRef='m:restriction']/text(),'ANGLE') = 0">
                                                    [ 'Angle', '<xsl:value-of select="$serial[1]"/>', '<xsl:value-of select="$serial[2]"/>', '<xsl:value-of select="$serial[3]"/>','','','<xsl:value-of select="$currentValue/cml:scalar[@dictRef='m:restrictionvalue']"/>','<xsl:value-of select="molcas:constrainUnitReplace($currentValue/cml:scalar[@dictRef='m:restrictionunit'])"/>']<xsl:if test="$outerIndex &lt; $numrestrictions">,</xsl:if>        
                                                </xsl:when>                                                    
                                                <xsl:when test="compare($currentRestriction/cml:scalar[@dictRef='m:restriction']/text(),'DIHEDRAL') = 0">
                                                    [ 'Dihedral', '<xsl:value-of select="$serial[1]"/>', '<xsl:value-of select="$serial[2]"/>', '<xsl:value-of select="$serial[3]"/>','<xsl:value-of select="$serial[4]"/>','','<xsl:value-of select="$currentValue/cml:scalar[@dictRef='m:restrictionvalue']"/>','<xsl:value-of select="molcas:constrainUnitReplace($currentValue/cml:scalar[@dictRef='m:restrictionunit'])"/>']<xsl:if test="$outerIndex &lt; $numrestrictions">,</xsl:if>        
                                                </xsl:when>
                                                <xsl:when test="compare($currentRestriction/cml:scalar[@dictRef='m:restriction']/text(),'OUTOFP') = 0">
                                                    [ 'Out of plane <xsl:value-of select="$currentRestriction/cml:scalar[@dictRef='m:restrictionextras']/text()"/>', '<xsl:value-of select="$serial[1]"/>', '', '','','','<xsl:value-of select="$currentValue/cml:scalar[@dictRef='m:restrictionvalue']"/>','<xsl:value-of select="molcas:constrainUnitReplace($currentValue/cml:scalar[@dictRef='m:restrictionunit'])"/>']<xsl:if test="$outerIndex &lt; $numrestrictions">,</xsl:if>        
                                                </xsl:when>
                                            </xsl:choose>
                                        </xsl:for-each>
                                        ],
                                        "aoColumns" : [
                                            { "sTitle": "Type" },
                                            { "sTitle": "" },
                                            { "sTitle": "" },
                                            { "sTitle": "" },
                                            { "sTitle": "" },
                                            { "sTitle": "" },
                                            { "sTitle": "Value", "sClass": "right" },
                                            { "sTitle": "Units", "sClass": "right" }
                                            ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                        
                                    });
                                }); 
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </xsl:if>         
    </xsl:template>
    <xsl:function name="molcas:constrainUnitReplace">
        <xsl:param name="currentValue"/>
        
        <xsl:choose>
            <xsl:when test="compare($currentValue,'ANGSTROM') = 0">
                <xsl:value-of select="'Å'"/>
            </xsl:when>
            <xsl:when test="compare($currentValue,'DEGREE') = 0">
                <xsl:value-of select="'deg'"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$currentValue"/>
            </xsl:otherwise>
        </xsl:choose>
        
    </xsl:function> 
    
    <!-- Interatomic distances (bond distances) -->       
    <xsl:template name="atomicDistances">
        <xsl:variable name="molecule" select="(//cml:module[@dictRef='cc:finalization']/cml:molecule)[last()]"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#bondDistances-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Bond distances                           
                </h4>
            </div>
            <div id="bondDistances-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-4 col-sm-12">
                            <table class="display" id="bondDistancesT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th>Atom1</th>
                                        <th>Atom2</th>
                                        <th class="right">Distance</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="$molecule/cml:bondArray/cml:bond">
                                        <xsl:variable name="atomRefs2" select="tokenize(./@atomRefs2,' ')"/>
                                        <!-- For each bond calculate interatomic distances -->
                                        <xsl:variable name="atomType1" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@elementType"/>
                                        <xsl:variable name="x1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@x3"/>
                                        <xsl:variable name="y1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@y3"/>
                                        <xsl:variable name="z1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@z3"/>
                                        <xsl:variable name="position1" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/preceding-sibling::*)+1"/>
                                        <!-- http://stackoverflow.com/questions/226405/find-position-of-a-node-using-xpath -->
                                        <xsl:variable name="atomType2" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@elementType"/>
                                        <xsl:variable name="x2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@x3"/>
                                        <xsl:variable name="y2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@y3"/>
                                        <xsl:variable name="z2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@z3"/>
                                        <xsl:variable name="position2" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/preceding-sibling::*)+1"/>
                                        <xsl:variable name="distance"  select="ckbk:sqrt((($x2 - $x1) * ($x2 - $x1)) + ($y2 - $y1) * ($y2 - $y1) + ($z2 - $z1) * ($z2 - $z1))"/>
                                        <tr>
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType1"/>
                                                <xsl:value-of select="$position1"/>
                                            </td>                                            
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType2"/>
                                                <xsl:value-of select="$position2"/>
                                            </td>
                                            <td class="right">                                                
                                                <xsl:value-of select="format-number($distance,'#0.000000')"/>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                 
                                    $('table#bondDistancesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable();                               
                                });  
                            </script>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- Symmetry section -->
    <xsl:template name="symmetry">                
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <xsl:variable name="symmetry" select="(//cml:module[@cmlx:templateRef='symmetry'])[last()]"/>
                <xsl:if test="exists($symmetry)">    
                    <h4>Symmetry</h4>
                    <xsl:for-each select="$symmetry/cml:scalar[@dictRef='m:symmdesc']">
                        <p><xsl:value-of select="."/></p>
                    </xsl:for-each>
                    <xsl:variable name="charTable" select="$symmetry/cml:module[@cmlx:templateRef='charactertable']" />
                    <xsl:variable name="symElem" select="tokenize($charTable/cml:array[@dictRef='m:symmelementrow'],'\s+')" />
                    <xsl:variable name="symIrred" select="tokenize($charTable/cml:array[@dictRef='m:irreductiblerepcol'],'\s+')" />
                    <xsl:variable name="cols" select="$charTable/cml:matrix[@dictRef='m:characters']/@cols" />                                
                    <xsl:variable name="rows" select="$charTable/cml:matrix[@dictRef='m:characters']/@rows" />
                    <xsl:variable name="charMatrix" select="tokenize($charTable/cml:matrix[@dictRef='m:characters'],'\s+')" />                                
                    <xsl:variable name="symExample" select="$charTable/cml:list[@dictRef='m:symelemexample']/cml:scalar[@dictRef='m:symelemexample']" />
                    <p>Character Table for <xsl:value-of select="$charTable/cml:scalar[@dictRef='m:symmelemdesc']" /></p>
                    <table class="display" id="restrictionsT-{generate-id($symmetry)}"></table>
                    <script type="text/javascript">
                        $(document).ready(function(){
                        $("table#restrictionsT-<xsl:value-of select="generate-id($symmetry)"/>").dataTable({
                            "aaData" : [ <xsl:for-each select="1 to $rows">
                                <xsl:variable name="outerIndex" select="."/>
                                [ '<xsl:value-of select="$outerIndex"/>','<xsl:value-of select="molcas:escapeQuotes($symIrred[$outerIndex])"/>',<xsl:for-each select="1 to $cols"><xsl:variable name="innerIndex" select="."/><xsl:value-of select="$charMatrix[($outerIndex - 1) * $cols + $innerIndex ]"/>,</xsl:for-each>'<xsl:value-of select="$symExample[$outerIndex]"/>']<xsl:if test="$outerIndex &lt; $rows">,</xsl:if>                                        
                            </xsl:for-each>                                    
                            ],
                            "aoColumns" : [{"sTitle" : ""}, { "sTitle": "" ,  "sClass": "oneline"}, <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/>{ "sTitle": "<xsl:value-of select="$symElem[$outerIndex]"/>","sClass": "right" },</xsl:for-each>{ "sTitle": "" ,  "sClass": "oneline"}],
                            "bFilter": false,
                            "bPaginate": false,
                            "bSort": false,
                            "bInfo": false                                        
                            });
                        }); 
                    </script>
                    <style type="text/css">
                        .oneline{
                            white-space: nowrap;
                        }
                    </style>
                </xsl:if>
            </div>
        </div> 
    </xsl:template>
  
    <!-- Charge, multiplicity and solvent section -->
    <xsl:template name="chargemultiplicity">       
        <div class="row">
            <div class="col-md-6 col-sm-12">
                <table id="molecularInfo" cellspacing="0" width="100%">
                    <thead>
                        <tr>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>    
                    <tbody>
                        <xsl:if test="exists($charge) and compare($charge,'') != 0">
                            <tr>                        
                                <td>Charge</td>
                                <td><xsl:value-of select="molcas:formatCharge($charge)"/></td>                            
                            </tr>
                        </xsl:if>
                        <tr>
                            <td>Multiplicity</td>
                            <td><xsl:value-of select="$multiplicity"/></td>
                        </tr>                     
                    </tbody>
                </table>                    
            </div>          
        </div>    
        <br/>
        <xsl:variable name="pcm" select="(//cml:module[@cmlx:templateRef='pcm'])[last()]" />
        <xsl:variable name="kirkwood" select="(//cml:module[@cmlx:templateRef='kirkwood'])[last()]" />
        <xsl:if test="exists($pcm) or exists($kirkwood)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#solvation" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Solvation input</h4>
                </div>            
                <div id="solvation" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-8 col-sm-12">
                                <table id="solvationInfo" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>    
                                    <tbody>                                            
                                        <xsl:choose>
                                            <xsl:when test="exists($pcm)">
                                                <tr>
                                                    <td>Solvent:</td>
                                                    <td><xsl:value-of select="$pcm/cml:scalar[@dictRef='m:solvent']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Modelling:</td>
                                                    <td>PCM (<xsl:value-of select="$pcm/cml:scalar[@dictRef='m:solventVersion']"/> version)</td>
                                                </tr>
                                                <tr>
                                                    <td>Calculation type:</td>
                                                    <td><xsl:value-of select="$pcm/cml:scalar[@dictRef='m:calctype']"/></td>
                                                </tr>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <tr>
                                                    <td>Solvent:</td>
                                                    <td>epsilon=<xsl:value-of select="number($kirkwood/cml:scalar[@dictRef='m:dielectricvalue'])"/></td>
                                                </tr>
                                                <tr>
                                                    <td>Modelling:</td>
                                                    <td>Kirkwood (cavity radius = <xsl:value-of select="number($kirkwood/cml:scalar[@dictRef='m:cavityradius'])"/>)</td>
                                                </tr>
                                                 <tr>                                                    
                                                    <td>Calculation type:</td>
                                                    <td><xsl:value-of select="$kirkwood/cml:scalar[@dictRef='m:calctype']"/></td>
                                                 </tr>
                                            </xsl:otherwise>
                                        </xsl:choose>                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>                                
        </xsl:if>  
    </xsl:template>

    <!-- SEWARD generation fields -->
    <xsl:template name="sewardlines">
        <xsl:variable name="lines" select="(//cml:module[@cmlx:templateRef='seward.generate'])[1]/cml:list/cml:scalar" />
        <xsl:if test="exists($lines)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#sewardlines" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Integrals</h4>
                </div>            
                <div id="sewardlines" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-8 col-sm-12">
                                <table>
                                    <thead>
                                        <tr>
                                            <th> </th>
                                        </tr>
                                    </thead>    
                                    <tbody>
                                        <xsl:for-each select="$lines">
                                            <xsl:variable name="line" select="."/>
                                            <tr><td><xsl:value-of select="$line"/></td></tr>
                                        </xsl:for-each>        
                                        
                                    </tbody>
                                </table>
                                                                  
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="wavefunctionspecs">
        <xsl:variable name="waveSpecs" select="(.//cml:module[@cmlx:templateRef='wave.specs'])[last()]"/>
        <xsl:if test="exists($waveSpecs)">          
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#waveSpecs-{generate-id($waveSpecs)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Wave function specification</h4>
                </div>            
                <div id="waveSpecs-{generate-id($waveSpecs)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12" >                            
                                <table class="display" id="waveSpecsT-{generate-id($waveSpecs)}">
                                    <thead>
                                        <tr><td></td><td></td></tr>
                                    </thead>
                                    <tbody>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:closedelec'])">
                                            <tr>
                                                <td>Number of closed shell electrons</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:closedelec']"/></td>
                                            </tr>                                        
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:activeelec'])">
                                            <tr>
                                                <td>Number of electrons in active shells</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:activeelec']" /> </td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:ras1holes'])">
                                            <tr>
                                                <td>Max number of holes in RAS1 space</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:ras1holes']" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:ras3holes'])">
                                            <tr>
                                                <td>Max nr of electrons in RAS3 space</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:ras3holes']" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:inactiveorbitals'])">
                                            <tr>
                                                <td>Number of inactive orbitals</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:inactiveorbitals']" />
                                                </td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:activeorbitals'])">
                                            <tr>
                                                <td>Number of active orbitals</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:activeorbitals']" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:secondaryorbitals'])">
                                            <tr>
                                                <td>Number of secondary orbitals</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:secondaryorbitals']" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:spinquantumnum'])">
                                            <tr>
                                                <td>Spin quantum number</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:spinquantumnum']" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($waveSpecs/cml:scalar[@dictRef='m:statesymm'])">
                                            <tr>
                                                <td>State symmetry</td>
                                                <td><xsl:value-of select="$waveSpecs/cml:scalar[@dictRef='m:statesymm']" /> </td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:variable name="charge" select="(//cml:module[@id='job']/cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='m:charge'])[last()]"/>
                                        <xsl:if test="exists($charge)">
                                            <tr>
                                                <td>Total molecular charge</td>
                                                <td><xsl:value-of select="$charge"/></td>
                                            </tr>
                                        </xsl:if>                                    
                                    </tbody>                                
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('table#waveSpecsT-<xsl:value-of select="generate-id($waveSpecs)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </xsl:if>        
    </xsl:template>

    <xsl:template name="orbitalSpecsSymmetrySpecies">
        <xsl:variable name="orbSpecs" select="(.//cml:module[@cmlx:templateRef='orbital.specs'])[last()]"/>
        <xsl:if test="exists($orbSpecs)">            
            <xsl:variable name="symserial" select="tokenize($orbSpecs/cml:array[@dictRef='m:symserial'],'\s+')"/>
            <xsl:variable name="cols" select="count($symserial)"/>
            <xsl:variable name="symlabel" select="tokenize($orbSpecs/cml:array[@dictRef='m:symlabel'],'\s+')"/>
            <xsl:variable name="frozenorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:frozenorb'],'\s+')"/>
            <xsl:variable name="inactiveorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:inactiveorb'],'\s+')"/>
            <xsl:variable name="activeorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:activeorb'],'\s+')"/>
            <xsl:variable name="ras1orb" select="tokenize($orbSpecs/cml:array[@dictRef='m:ras1orb'],'\s+')"/>
            <xsl:variable name="ras2orb" select="tokenize($orbSpecs/cml:array[@dictRef='m:ras2orb'],'\s+')"/>
            <xsl:variable name="ras3orb" select="tokenize($orbSpecs/cml:array[@dictRef='m:ras3orv'],'\s+')"/>
            <xsl:variable name="secondaryorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:secondaryorb'],'\s+')"/>
            <xsl:variable name="deletedorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:deletedorb'],'\s+')"/>
            <xsl:variable name="totalorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:orbno'],'\s+')"/>
            <xsl:variable name="basisno" select="tokenize($orbSpecs/cml:array[@dictRef='m:basisno'],'\s+')"/>
                     
            <!--TODO: Data taken from Molden file -->
            <xsl:variable name="occupalpha"/>
            <xsl:variable name="occupbeta"/>
            <xsl:variable name="secondaryorbAlpha"/>    
            <xsl:variable name="secondaryorbBeta"/>
                        
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbSpecs-{generate-id($orbSpecs)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Orbital specifications</h4>
                </div>            
                <div id="orbSpecs-{generate-id($orbSpecs)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="orbSpecsT-{generate-id($orbSpecs)}">
                                    <thead>
                                        <tr>
                                            <th></th><xsl:for-each select="1 to $cols"><th></th></xsl:for-each>                                            
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Symmetry species</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$symserial[$outerIndex]"/></td></xsl:for-each>        
                                        </tr>
                                        <tr>
                                            <td></td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$symlabel[$outerIndex]"/></td></xsl:for-each>
                                        </tr>
                                        
                                        <xsl:if test="exists($frozenorb)">                                            
                                        <tr>
                                            <td>Frozen orbitals</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$frozenorb[$outerIndex]"/></td></xsl:for-each>
                                        </tr> 
                                        </xsl:if>
                                    <!--
                                        <xsl:if test="exists($occupalpha)">
                                            ['Occupied orbitals alpha', <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$occupalpha[$outerIndex]"/><xsl:if test="$outerIndex &lt; $cols">,</xsl:if></xsl:for-each>],                                              
                                        </xsl:if>
                                        <xsl:if test="exists($occupbeta)">
                                            ['Occupied orbitals beta', <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$occupbeta[$outerIndex]"/><xsl:if test="$outerIndex &lt; $cols">,</xsl:if></xsl:for-each>],                                              
                                        </xsl:if>                                   
                                    -->
                                            <xsl:if test="exists($inactiveorb)">
                                                <tr>
                                                    <td>Inactive orbitals</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$inactiveorb[$outerIndex]"/></td></xsl:for-each>                                                   
                                                </tr>
                                            </xsl:if>                                                                                
                                        
                                            <xsl:if test="exists($activeorb)">
                                                <tr>
                                                    <td>Active orbitals</td>
                                                    <xsl:choose>
                                                        <xsl:when test="$hasOrbitals">                                                            
                                                           <xsl:for-each select="1 to $cols">
                                                               <xsl:variable name="outerIndex" select="."/>    
                                                               <td>                                                                      
                                                                   <xsl:choose>
                                                                       <xsl:when test="number($activeorb[$outerIndex]) > 0">
                                                                           <div class="btn-group btn-link">
                                                                               <span class="dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                   <xsl:value-of select="$activeorb[$outerIndex]"/>
                                                                               </span>                                                                    
                                                                               <div class="dropdown-menu" >
                                                                                   <xsl:variable name="activeOrbitalStart" select="number($frozenorb[$outerIndex]) + number($inactiveorb[$outerIndex]) + 1"/>
                                                                                   <xsl:variable name="activeOrbitalEnd" select="$activeOrbitalStart + number($activeorb[$outerIndex]) "/>
                                                                                   <xsl:for-each select="xs:integer($activeOrbitalStart) to xs:integer($activeOrbitalEnd) - 1 ">
                                                                                       <xsl:variable name="innerIndex" select="."/>
                                                                                       <a class="dropdown-item" href="javascript:displayMoldenOrbital('{$innerIndex}{molcas:escapeQuotes($symlabel[$outerIndex])}')"><xsl:value-of select="concat($innerIndex,$symlabel[$outerIndex])"/></a>    
                                                                                   </xsl:for-each>
                                                                               </div>
                                                                           </div>
                                                                       </xsl:when>
                                                                       <xsl:otherwise>
                                                                           <xsl:value-of select="$activeorb[$outerIndex]"/>
                                                                       </xsl:otherwise>
                                                                   </xsl:choose>                                                                 
                                                                </td>
                                                           </xsl:for-each>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$activeorb[$outerIndex]"/></td></xsl:for-each>        
                                                        </xsl:otherwise>
                                                    </xsl:choose>                                                    
                                                </tr>                                               
                                            </xsl:if>
                                        
                                        
                                            <xsl:if test="exists($ras1orb)">                                                                                                
                                                <tr>
                                                    <td>RAS1 orbitals</td>
                                                    <xsl:choose>
                                                        <xsl:when test="$hasOrbitals">                                                            
                                                            <xsl:for-each select="1 to $cols">
                                                                <xsl:variable name="outerIndex" select="."/>    
                                                                <td>                                                                      
                                                                    <xsl:choose>
                                                                        <xsl:when test="number($ras1orb[$outerIndex]) > 0">
                                                                            <div class="btn-group btn-link">
                                                                                <span class="dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <xsl:value-of select="$ras1orb[$outerIndex]"/>
                                                                                </span>                                                                    
                                                                                <div class="dropdown-menu" >
                                                                                    <xsl:variable name="activeOrbitalStart" select="number($frozenorb[$outerIndex]) + number($inactiveorb[$outerIndex]) + 1"/>
                                                                                    <xsl:variable name="activeOrbitalEnd" select="$activeOrbitalStart + number($ras1orb[$outerIndex]) "/>
                                                                                    <xsl:for-each select="xs:integer($activeOrbitalStart) to xs:integer($activeOrbitalEnd) - 1 ">
                                                                                        <xsl:variable name="innerIndex" select="."/>
                                                                                        <a class="dropdown-item" href="javascript:displayMoldenOrbital('{$innerIndex}{molcas:escapeQuotes($symlabel[$outerIndex])}')"><xsl:value-of select="concat($innerIndex,$symlabel[$outerIndex])"/></a>    
                                                                                    </xsl:for-each>
                                                                                </div>
                                                                            </div>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="$ras1orb[$outerIndex]"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>                                                                 
                                                                </td>
                                                            </xsl:for-each>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$ras1orb[$outerIndex]"/></td></xsl:for-each>        
                                                        </xsl:otherwise>
                                                    </xsl:choose>                                                                                                                
                                                </tr>
                                            </xsl:if>
                                            <xsl:if test="exists($ras2orb)">
                                                <tr>
                                                    <td>RAS2 orbitals</td>                                                    
                                                    <xsl:choose>
                                                        <xsl:when test="$hasOrbitals">                                                            
                                                            <xsl:for-each select="1 to $cols">
                                                                <xsl:variable name="outerIndex" select="."/>    
                                                                <td>                                                                      
                                                                    <xsl:choose>
                                                                        <xsl:when test="number($ras2orb[$outerIndex]) > 0">
                                                                            <div class="btn-group btn-link">
                                                                                <span class="dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <xsl:value-of select="$ras2orb[$outerIndex]"/>
                                                                                </span>                                                                    
                                                                                <div class="dropdown-menu" >
                                                                                    <xsl:variable name="activeOrbitalStart" select="number($frozenorb[$outerIndex]) + number($inactiveorb[$outerIndex]) + number($ras1orb[$outerIndex]) + 1"/>
                                                                                    <xsl:variable name="activeOrbitalEnd" select="$activeOrbitalStart + number($ras2orb[$outerIndex]) "/>
                                                                                    <xsl:for-each select="xs:integer($activeOrbitalStart) to xs:integer($activeOrbitalEnd) - 1 ">
                                                                                        <xsl:variable name="innerIndex" select="."/>
                                                                                        <a class="dropdown-item" href="javascript:displayMoldenOrbital('{$innerIndex}{molcas:escapeQuotes($symlabel[$outerIndex])}')"><xsl:value-of select="concat($innerIndex,$symlabel[$outerIndex])"/></a>    
                                                                                    </xsl:for-each>
                                                                                </div>
                                                                            </div>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="$ras2orb[$outerIndex]"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>                                                                 
                                                                </td>
                                                            </xsl:for-each>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$ras2orb[$outerIndex]"/></td></xsl:for-each>        
                                                        </xsl:otherwise>
                                                    </xsl:choose>                                                           
                                                </tr>                                                                                             
                                            </xsl:if>                                        
                                            <xsl:if test="exists($ras3orb)">
                                                <tr>
                                                    <td>RAS3 orbitals</td>                                                    
                                                    <xsl:choose>
                                                        <xsl:when test="$hasOrbitals">                                                            
                                                            <xsl:for-each select="1 to $cols">
                                                                <xsl:variable name="outerIndex" select="."/>    
                                                                <td>                                                                      
                                                                    <xsl:choose>
                                                                        <xsl:when test="number($ras3orb[$outerIndex]) > 0">
                                                                            <div class="btn-group btn-link">
                                                                                <span class="dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                                    <xsl:value-of select="$ras3orb[$outerIndex]"/>
                                                                                </span>                                                                    
                                                                                <div class="dropdown-menu" >
                                                                                    <xsl:variable name="activeOrbitalStart" select="number($frozenorb[$outerIndex]) + number($inactiveorb[$outerIndex]) + number($ras1orb[$outerIndex]) + number($ras2orb[$outerIndex]) + 1"/>
                                                                                    <xsl:variable name="activeOrbitalEnd" select="$activeOrbitalStart + number($ras3orb[$outerIndex]) "/>
                                                                                    <xsl:for-each select="xs:integer($activeOrbitalStart) to xs:integer($activeOrbitalEnd) - 1 ">
                                                                                        <xsl:variable name="innerIndex" select="."/>
                                                                                        <a class="dropdown-item" href="javascript:displayMoldenOrbital('{$innerIndex}{molcas:escapeQuotes($symlabel[$outerIndex])}')"><xsl:value-of select="concat($innerIndex,$symlabel[$outerIndex])"/></a>    
                                                                                    </xsl:for-each>
                                                                                </div>
                                                                            </div>
                                                                        </xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="$ras3orb[$outerIndex]"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>                                                                 
                                                                </td>
                                                            </xsl:for-each>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$ras3orb[$outerIndex]"/></td></xsl:for-each>        
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </tr>                                                
                                            </xsl:if>
                                            <xsl:if test="exists($secondaryorb)">
                                                <tr>
                                                    <td>Secondary orbitals</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$secondaryorb[$outerIndex]"/></td></xsl:for-each>
                                                </tr>                                                                                             
                                            </xsl:if>             
                                            <!--
                                                <xsl:if test="exists($secondaryorbAlpha)">
                                                    ['Secondary orbitals alpha', 
                                                    <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/>
                                                        
                                                        <xsl:value-of select="$secondaryorbAlpha[$outerIndex]"/><xsl:if test="$outerIndex &lt; $cols">,</xsl:if>
                                                    </xsl:for-each>
                                                    ],                                              
                                                </xsl:if>                                    
                                                <xsl:if test="exists($secondaryorbBeta)">
                                                    ['Secondary orbitals beta', 
                                                    <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/>
                                            
                                                        <xsl:value-of select="$secondaryorbBeta[$outerIndex]"/><xsl:if test="$outerIndex &lt; $cols">,</xsl:if>
                                                    </xsl:for-each>
                                                    ],                                              
                                                </xsl:if>
                                                -->
                                            <xsl:if test="exists($deletedorb)">
                                                <tr>
                                                    <td>Deleted orbitals</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$deletedorb[$outerIndex]"/></td></xsl:for-each>                                                    
                                                </tr>
                                            </xsl:if>                                    
                                            <xsl:if test="exists($totalorb)">
                                                <tr>
                                                    <td>Total number of orbitals</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$totalorb[$outerIndex]"/></td></xsl:for-each>                                                   
                                                </tr>                                                                                              
                                            </xsl:if>
                                            <xsl:if test="exists($basisno) and not(exists($totalorb))">
                                                <tr>
                                                    <td>Number of basis functions</td><xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$basisno[$outerIndex]"/></td></xsl:for-each>
                                                </tr>
                                            </xsl:if>
                                    </tbody>                                    
                                </table>       
                                <script type="text/javascript">
                                    
                                    
                                    
                                    
                                    
                                    $(document).ready(function() {                        
                                    $('table#orbSpecsT-<xsl:value-of select="generate-id($orbSpecs)"/>').dataTable( {
                                    "aoColumns": [
                                        { "sTitle": "" }, <xsl:for-each select="1 to $cols"><xsl:variable name="outerIndex" select="."/>{ "sTitle": "", "sClass": "right" }<xsl:if test="$outerIndex &lt; $cols">,</xsl:if></xsl:for-each>                    
                                    ],
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false
                                    } );   
                                    } );                                        
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </xsl:if>   
    </xsl:template>

    <xsl:template name="ciExpansion">
        <xsl:variable name="ciExpansion" select="(.//cml:module[@cmlx:templateRef='ci.expansion'])[last()]"/>        
        <xsl:if test="exists($ciExpansion)">
            <xsl:variable name="conffnc" select="$ciExpansion/cml:scalar[@dictRef='m:conffnc']"/>
            <xsl:variable name="determinants" select="$ciExpansion/cml:scalar[@dictRef='m:determinants']"/>
            <xsl:variable name="requiredroot" select="$ciExpansion/cml:scalar[@dictRef='m:requiredroot']"/>
            <xsl:variable name="chosenroot" select="$ciExpansion/cml:scalar[@dictRef='m:chosenroot']"/>            
            <xsl:variable name="ciroots" select="tokenize($ciExpansion/cml:array[@dictRef='m:ciroots'],'\s+')"/>
            <xsl:variable name="rootweight" select="tokenize($ciExpansion/cml:array[@dictRef='m:rootweight'],'\s+')"/>            
            <xsl:variable name="highestroot" select="$ciExpansion/cml:scalar[@dictRef='m:highestroot']"/>
            <xsl:variable name="maxsizehamilt" select="$ciExpansion/cml:scalar[@dictRef='m:maxsizehamilt']"/>
            <xsl:variable name="passedroot" select="$ciExpansion/cml:scalar[@dictRef='m:passedroot']"/>            
            <xsl:variable name="numroots" select="count($ciroots)"/>            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#ciExpansion-{generate-id($ciExpansion)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">CI expansion specifications</h4>
                </div>            
                <div id="ciExpansion-{generate-id($ciExpansion)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-8 col-sm-12">
                                <table class="display" id="ciExpansionT-{generate-id($ciExpansion)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>                                    
                                    <tbody>
                                        <xsl:if test="exists($conffnc)">
                                            <tr>
                                                <td>Number of CSFs</td>
                                                <td colspan="{$numroots}"> <xsl:value-of select="$conffnc"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($determinants)">
                                            <tr>
                                                <td>Number of determinants</td>
                                                <td colspan="{$numroots}"><xsl:value-of select="$determinants"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($requiredroot)">
                                            <tr>
                                                <td>Number of root(s) required</td>
                                                <td colspan="{$numroots}"><xsl:value-of select="$requiredroot"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($chosenroot)">
                                            <tr>
                                                <td>Root chosen for geometry opt.</td>
                                                <td colspan="{$numroots}"><xsl:value-of select="$chosenroot" /></td>
                                            </tr>
                                        </xsl:if>
                                       
                                        <xsl:if test="exists($ciroots)">
                                            <tr>
                                                <td>CI roots used / Weights</td>                                            
                                                <td>                                                    
                                                    <table>                                                                                                               
                                                            <tr>
                                                                <xsl:for-each select="$ciroots">
                                                                    <td class="datacell">
                                                                        <xsl:value-of select="."/>
                                                                    </td>
                                                                </xsl:for-each>
                                                            </tr>
                                                            <tr>
                                                                <xsl:if test="exists($rootweight)">
                                                                    <xsl:for-each select="$rootweight">
                                                                        <td>
                                                                            <xsl:value-of select="."/>
                                                                        </td>
                                                                    </xsl:for-each>
                                                                </xsl:if>
                                                                <xsl:if test="not(exists($rootweight))">
                                                                    <td>1.00</td>
                                                                </xsl:if>
                                                            </tr>
                                                    </table>
                                                </td>
                                            </tr>    
                                        </xsl:if>
                                        
                                        <xsl:if test="$highestroot">
                                            <tr>
                                                <td>Highest root included in the CI</td>
                                                <td><xsl:value-of select="$highestroot"/>
                                                </td>
                                            </tr>
                                        </xsl:if>                                        
                                        <xsl:if test="$passedroot">
                                            <tr>
                                                <td>Root passed to geometry opt.</td>
                                                <td><xsl:value-of select="$passedroot" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($maxsizehamilt)">
                                            <tr>
                                                <td>Max. size of the explicit Hamiltonian</td>
                                                <td><xsl:value-of select="$maxsizehamilt" /></td>
                                            </tr>
                                        </xsl:if>
                                    </tbody>                                    
                                </table>       
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#ciExpansionT-<xsl:value-of select="generate-id($ciExpansion)"/>").dataTable({
                                        "columns" : [
                                            null,
                                            {"defaultContent": ""}
                                        ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="waveFunctionEnergies">
        <xsl:variable name="waveFunctionEnergies" select="(.//cml:module[@cmlx:templateRef='wave.printout'])[last()]"/>
        <xsl:if test="exists($waveFunctionEnergies)">
            <xsl:variable name="roots" select="$waveFunctionEnergies/cml:module[@cmlx:templateRef='ci.coefficients']"/>
            <xsl:variable name="rootEnergy" select="number($roots[1]/cml:scalar[@dictRef='m:orbitalenergy'])"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#waveEnergies-{generate-id($waveFunctionEnergies)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Energies</h4>
                </div>            
                <div id="waveEnergies-{generate-id($waveFunctionEnergies)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="waveEnergiesT-{generate-id($waveFunctionEnergies)}">
                                    <thead>
                                        <tr>
                                            <td>Root</td>
                                            <td>Total energy (au)</td>
                                            <td>∆E (eV)</td>
                                            <td>∆E (cm-1)</td>
                                        </tr>
                                    </thead>                                   
                                    <tbody>
                                        <xsl:for-each select="1 to count($roots)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <xsl:variable name="difference" select="number($roots[$outerIndex]/cml:scalar[@dictRef='m:orbitalenergy']) - $rootEnergy"/>                                            
                                            <tr>
                                                <td><xsl:value-of select="$roots[$outerIndex]/cml:scalar[@dictRef='m:rootnumber']"/></td>
                                                <td><xsl:value-of select="$roots[$outerIndex]/cml:scalar[@dictRef='m:orbitalenergy']"/></td>
                                                <td><xsl:value-of select="if(string($difference) = 'NaN') then '0.00' else format-number(number($difference) * $auToEV,'0.00')"/></td>
                                                <td><xsl:value-of select="if(string($difference) = 'NaN') then '0.00' else format-number(number($difference) * $auToCm1,'####0')"/></td>                                                
                                            </tr>    
                                        </xsl:for-each>                                      
                                    </tbody>
                                </table>       
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#waveEnergiesT-<xsl:value-of select="generate-id($waveFunctionEnergies)"/>").dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="waveFunctionTopWeightsValues">
        <xsl:variable name="waveFuncWeights" select="(.//cml:module[@cmlx:templateRef='wave.printout'])[last()]"/>
        <xsl:if test="exists($waveFuncWeights)">
            <xsl:variable name="symmetry" select="($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'])[1]/cml:scalar[@dictRef='m:symmetry']" />
            <xsl:variable name="colWidth" select="if(count($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients']/cml:scalar[@dictRef='m:rootnumber']) &gt; 4) then '12' else '6'"/>
            <xsl:variable name="weightsMatrix">
                <xsl:for-each select="$waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients']">
                    <xsl:variable name="coeff" select="." />
                    
                    <xsl:variable name="root" select="$coeff/cml:scalar[@dictRef='m:rootnumber']" />
                    <xsl:variable name="confs" select="tokenize($coeff/cml:array[@dictRef='x:value'],'\|')"/>
                    <xsl:variable name="confIndex" select="tokenize($coeff/cml:array[@dictRef='m:configuration'],'\s+')"/>
                    <xsl:variable name="weight" select="tokenize($coeff/cml:array[@dictRef='m:weight'],'\s+')" />
                    <xsl:element name="cml:root">
                        <xsl:attribute name="number" select="$root"/>
                       
                        <xsl:for-each select="1 to count($confs)">
                            <xsl:variable name="outerIndex" select="."/>
                            <xsl:element name="cml:value">
                                <xsl:attribute name="conf" select="$confs[$outerIndex]"/>
                                <xsl:attribute name="index" select="$confIndex[$outerIndex]"/>
                                <xsl:attribute name="weight" select="$weight[$outerIndex]"/>
                            </xsl:element>
                        </xsl:for-each>
                    </xsl:element>                    
                </xsl:for-each>
            </xsl:variable>
            
            <xsl:variable name="numbers">
                <xsl:for-each select="distinct-values($weightsMatrix/cml:root/cml:value/@index)">
                    <xsl:element name="cml:number">
                        <xsl:attribute name="value" select="."/>
                    </xsl:element>                                        
                </xsl:for-each>                
            </xsl:variable>
            
            <xsl:variable name="confNum">               
                <xsl:for-each select="$numbers/cml:number">
                    <xsl:sort select="number(./@value)"/>
                    <xsl:element name="cml:number">
                        <xsl:attribute name="value" select="./@value"/>
                    </xsl:element>                                        
                </xsl:for-each>
            </xsl:variable>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#waveFuncWeights-{generate-id($waveFuncWeights)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Wave functions / Weights of the most important CSFs</h4>
                </div>            
                <div id="waveFuncWeights-{generate-id($waveFuncWeights)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-{$colWidth} col-sm-12">
                                <table class="display" id="waveFuncWeightsT-{generate-id($waveFuncWeights)}">
                                    <thead>
                                        <xsl:choose>
                                            <xsl:when test="count($weightsMatrix/cml:root) = 1">
                                                <tr>
                                                    <th>Conf</th>
                                                    <th><xsl:value-of select="$symmetry"/></th>
                                                    <th>1</th>
                                                </tr>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <tr>
                                                    <th rowspan="2">Conf</th>
                                                    <th rowspan="2"><xsl:value-of select="$symmetry"/></th>
                                                    <th colspan="{count($weightsMatrix/cml:root)}" style="text-align:center">Roots</th>                                                  
                                                </tr>
                                                <tr>
                                                    <xsl:for-each select="1 to count($weightsMatrix/cml:root)">                                                        
                                                        <th><xsl:value-of select="."/></th>    
                                                    </xsl:for-each>
                                                </tr>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </thead>
                                </table>       
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#waveFuncWeightsT-<xsl:value-of select="generate-id($waveFuncWeights)"/>").dataTable({
                                        "aaData" : [
                                                <xsl:for-each select="1 to count($confNum/cml:number)">
                                                    [
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <xsl:variable name="conf" select="$confNum/cml:number[$outerIndex]/@value"/>
                                                    '<xsl:value-of select="$conf"/>','<xsl:value-of select="($weightsMatrix//cml:value[@index=$conf])[1]/@conf"/>',
                                                    <xsl:for-each select="1 to count($weightsMatrix/cml:root)">
                                                        <xsl:variable name="innerIndex" select="."/>
                                                        
                                                        <xsl:variable name="root" select="$weightsMatrix/cml:root[@number=$innerIndex]"/>
                                                        <xsl:variable name="weight" select="$root/cml:value[@index=$conf]"/>
                                                        '<xsl:value-of select="if(exists($weight)) then $weight/@weight else '-'"/>'<xsl:if test="$innerIndex &lt; count($weightsMatrix/cml:root)">,</xsl:if>
                                                    </xsl:for-each>
                                                    ],
                                                </xsl:for-each>
                                                ['Total','',
                                                        <xsl:for-each select="1 to count($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'])">
                                                            <xsl:variable name="outerIndex" select="."/>
                                                            <xsl:variable name="coef" select="$waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'][$outerIndex]"/>                                            
                                                            <xsl:variable name="partials">
                                                                <xsl:for-each select="tokenize($coef/cml:array[@dictRef='m:weight'],'\s+')">
                                                                    <xsl:element name="cml:value">
                                                                        <xsl:attribute name="value" select="number(.)"/>
                                                                    </xsl:element>
                                                                </xsl:for-each>
                                                            </xsl:variable>
                                                            <xsl:value-of select="round-half-to-even(sum($partials/cml:value/@value),3)"/>
                                                            <xsl:if test="$outerIndex &lt; count($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'])">,</xsl:if>
                                                    </xsl:for-each>
                                                ]
                                        ],
                                        "aoColumns" : [
                                        { "sClass": "left" }, { "sClass" : "left"}, <xsl:for-each select="1 to count($weightsMatrix/cml:root)"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right" }<xsl:if test="$outerIndex &lt; count($weightsMatrix/cml:root)">,</xsl:if></xsl:for-each>                                    
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                        
                                        });
                                    });                  
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Vibrational frequencies -->
    <xsl:template name="vibrationalFrequencies">
        <xsl:variable name="frequencies" select=".//cml:module[@id='finalization']/cml:propertyList/cml:property[@dictRef='cc:frequencies']/cml:module[@dictRef='cc:vibrations']"/>        
        <xsl:if test="exists($frequencies) and molcas:areFrequenciesValid($frequencies, $isOptimization, $isIncomplete, $isTS) ">
            <div id="vibrationPanel" class="panel panel-default" >
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        IR spectrum / Vibrational frequencies   
                    </h4>
                </div>
                <div id="frequencies" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <xsl:call-template name="irSpectrum">
                                <xsl:with-param name="frequencies" select="$frequencies"/>
                            </xsl:call-template>
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if>                    
    </xsl:template>
    
    <xsl:template name="irSpectrum">
        <xsl:param name="frequencies"/>                       
        <xsl:if test="exists($frequencies)">
            <xsl:variable name="frequency" select="tokenize($frequencies/array[@dictRef='cc:frequency'],'[\|\s]+')"/>
            <div class="col-lg-12">                                                                                             
                <script type="text/javascript">
                  	delete Jmol._tracker;
                    Jmol._local = true;
                
                    jsvApplet0 = "jsvApplet0";
                    jmolApplet0 = "jmolApplet0";                                
                    Jmol.setAppletSync([jsvApplet0, jmolApplet0], ["load <xsl:value-of select="$jcampdxurl"/>", ""], true);                                       
                    var jmolInfo = {
                        width: 560,
                        height: 400,
                        debug: false,
                        color: "0xF0F0F0",
                        use: "HTML5",
                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                        disableJ2SLoadMonitor: true,
                        disableInitialConsole: true,
                        readyFunction: vibrationsLoaded,
                        animframecallback: "modelchanged",
                        allowjavascript: true,
                        script: "background white; vibration off; vectors off; sync on;"
                    }                                       
                    var jsvInfo = {
                        width: 560,
                        height: 400,
                        debug: false,
                        color: "0xC0C0C0",
                        use: "HTML5",
                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                        script: null,
                        initParams: null,
                        disableJ2SLoadMonitor: true,
                        disableInitialConsole: true,
                        readyFunction: null,
                        allowjavascript: true
                    }
                    
                    $('div#frequencies').on('shown.bs.collapse', function () {
                        if($("div#jmolApplet").children().length == 0){       
                            use="HTML5";                                       
                            jsvApplet0 = Jmol.getJSVApplet("jsvApplet0", jsvInfo);
                            $("div#jsvApplet").html(Jmol.getAppletHtml(jsvApplet0));                                            
                            jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                            $("div#jmolApplet").html(Jmol.getAppletHtml(jmolApplet0));
                            vibrationsLoading();
                        }                                      
                    });
                    
                    function vibrationsLoading(){                                       
                        $('div#frequencies').block({ 
                            message: '<h3>Loading spectrum</h3>', 
                            css: { border: '3px solid #a00' } 
                        });                                                                                    
                    }
                    
                    function vibrationsLoaded(){
                        $('div#frequencies').unblock();                                                           
                    }
                    
                </script>                                
                <div id="irSpectrumDiv">
                    <div id="jsvApplet" style="display:inline; float:left">                                                                         
                    </div>
                    <div id="jmolApplet" style="display:inline; float:left">
                    </div>                                                                                                                               
                </div>                                
                <div id="frequencyComboboxDiv" style="display:block">
                    Selected frequency : 
                    <script type="text/javascript">
                        function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                        vibrationcboIndex = $("select[name='jmolMenu0'] option:selected").index();
                        if(vibrationcboIndex != objwhat + 1)
                        $("select[name='jmolMenu0']").val(objwhat + 2);                                                                                      
                        }                    
                        
                        Jmol.jmolMenu(jmolApplet0,[                    
                        ["vibration off", ".... select ....", true],
                        <xsl:for-each select="1 to count($frequency)">
                            <xsl:variable name="outerIndex" select="."/>
                            ["model <xsl:value-of select="$outerIndex"/>;vibration on" ,"<xsl:value-of select="$frequency[$outerIndex]"/>"]<xsl:if test="$outerIndex != count($frequency)"><xsl:text>,</xsl:text></xsl:if>
                        </xsl:for-each>                    
                        ]);                        
                    </script>                    
                </div>
                <script type="text/javascript">
                    $("#irSpectrumDiv" ).prepend( $("#frequencyComboboxDiv") );
                </script>                                                              
            </div>                                                        
        </xsl:if> 
    </xsl:template>

    <xsl:template name="naturalOccupation">
        <xsl:variable name="naturalOccup" select=".//cml:module[@cmlx:templateRef='wave.printout']/cml:module[@cmlx:templateRef='natural']"/>
        <xsl:if test="exists($naturalOccup)">
            
            <xsl:variable name="orbitalSpecs" select="(.//cml:module[@cmlx:templateRef='orbital.specs'])[last()]"/>
            <xsl:variable name="properties" select="(.//cml:module[@cmlx:templateRef='properties'])[last()]"/>
            <xsl:variable name="naturalHeaders" select="molcas:getNaturalHeaders($orbitalSpecs, $properties)"/>            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#naturalOcc-{generate-id($naturalOccup[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Natural Occupation numbers</h4>
                </div>            
                <div id="naturalOcc-{generate-id($naturalOccup[1])}" class="panel-collapse collapse">                      
                    <xsl:variable name="symmetries" select="tokenize($naturalOccup[1]/cml:array[@dictRef='m:symmetry'],'\s+')"/>
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12 col-sm-12 pre-scrollable">
                                <xsl:variable name="roots" select="count($naturalOccup)" />
                                <!-- Tokenize all possible values and move them into a element array with its appropiate indexes as attributes, for fast data retrieval --> 
                                <xsl:variable name="valueMatrix">
                                    <xsl:for-each select="1 to count($symmetries)">
                                        <xsl:variable name="symmIndex" select="number(.)"/>                        
                                        <xsl:for-each select="1 to count($naturalOccup)">
                                            <xsl:variable name="rootIndex" select="."/>
                                            <xsl:variable name="currentRootValues" select="tokenize($naturalOccup[$rootIndex]/cml:array[@dictRef='m:occup'][$symmIndex],'\s+')" />                            
                                            <xsl:for-each select="$currentRootValues">
                                                <xsl:variable name="orbital" select="position()"/>
                                                <xsl:element name="element">
                                                    <xsl:attribute name="symmetry" select="$symmetries[$symmIndex]"/>
                                                    <xsl:attribute name="root" select="$rootIndex"/>
                                                    <xsl:attribute name="orbital" select="$orbital" />
                                                    <xsl:attribute name="value" select="."/>
                                                </xsl:element>
                                            </xsl:for-each>                            
                                        </xsl:for-each>
                                    </xsl:for-each>                    
                                </xsl:variable>
                                <h4>Active orbitals</h4>
                                <xsl:for-each select="1 to count($symmetries)">                                   
                                    <xsl:variable name="symmIndex" select="number(.)"/>
                                    <xsl:variable name="orbitalNumber" select="$naturalOccup[1]/cml:array[@dictRef='m:occup'][$symmIndex]/@size" />
                                    <xsl:variable name="rootNumber" select="count($naturalOccup)" />
                                    <xsl:variable name="colWidth" select="if($orbitalNumber &gt; 4) then '12' else '6'"/>
                                    <div class="row">
                                        <div class="col-md-{$colWidth} col-sm-12">                                             
                                            <table class="display" id="naturalOccT-{generate-id($naturalOccup[1]/cml:array[@dictRef='m:occup'][$symmIndex])}">
                                                <thead>
                                                    <xsl:choose>
                                                        <xsl:when test="$rootNumber = 1">
                                                            <tr>
                                                                <th>Symmetry <xsl:value-of select="$symmetries[$symmIndex]"/></th>
                                                                <th>1</th>
                                                            </tr>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <tr>
                                                                <th rowspan="2">Symmetry <xsl:value-of select="$symmetries[$symmIndex]"/></th>
                                                                <th colspan="{$rootNumber}" style="text-align:center">Roots</th>                                                                
                                                            </tr>
                                                            <tr>                                                                
                                                                <xsl:for-each select="1 to $rootNumber">
                                                                    <th><xsl:value-of select="."/></th>
                                                                </xsl:for-each>
                                                            </tr>
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                    
                                                    
                                                </thead>
                                                <tbody>
                                                    <xsl:for-each select="1 to $orbitalNumber">
                                                        <xsl:variable name="orbitalIndex" select="."/>
                                                        <tr>
                                                            <td>
                                                                <xsl:variable name="orbital" select="($naturalHeaders/*:header[@specie = $symmetries[$symmIndex]])[$orbitalIndex]/@label"/>
                                                                <xsl:choose>
                                                                    <xsl:when test="$hasOrbitals">
                                                                        <a id="naturalorbs-{generate-id($orbital)}" href="javascript:displayMoldenOrbital('{molcas:escapeQuotes($orbital)}')">
                                                                            <xsl:value-of select="$orbital"/>
                                                                        </a>
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="$orbital"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                            <xsl:for-each select="1 to $rootNumber">
                                                                <xsl:variable name="rootIndex" select="."/>
                                                                <td><xsl:value-of select="$valueMatrix/*:element[@symmetry=$symmetries[$symmIndex] and @root=$rootIndex and @orbital=$orbitalIndex]/@value "/></td>
                                                            </xsl:for-each>                                 
                                                        </tr>
                                                    </xsl:for-each>
                                                </tbody>
                                            </table>                                            
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                    $("table#naturalOccT-<xsl:value-of select="generate-id($naturalOccup[1]/cml:array[@dictRef='m:occup'][$symmIndex])"/>").dataTable({
                                                        "aoColumns" : [
                                                        { "sClass": "left" }, <xsl:for-each select="1 to $rootNumber"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right" }<xsl:if test="$outerIndex &lt; $rootNumber">,</xsl:if></xsl:for-each>                                    
                                                         ],
                                                        "bFilter": false,
                                                        "bPaginate": false,
                                                        "bSort": false,
                                                        "bInfo": false                                        
                                                    });
                                                });    
                                            </script>
                                            <br/>
                                        </div>
                                    </div>
                                </xsl:for-each>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </xsl:if>        
    </xsl:template>

    <xsl:template name="finalCASPT2Energies">
        <xsl:variable name="caspt2modules" select=".//cml:module[@cmlx:templateRef='final.caspt2']"/>
        <xsl:if test="exists($caspt2modules)">
            <xsl:variable name="energies" select="$caspt2modules/cml:scalar[@dictRef='m:totalenergy']/text()"/>
            <xsl:variable name="weights" select="$caspt2modules/cml:scalar[@dictRef='m:refweight']/text()"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#finalCASPT2Energies-{generate-id($caspt2modules[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Single-State CASPT2</h4>
                </div>            
                <div id="finalCASPT2Energies-{generate-id($caspt2modules[1])}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="finalCASPT2EnergiesT-{generate-id($caspt2modules[1])}">
                                    <thead>
                                        <tr>
                                            <th>Root</th>
                                            <th>Total energy (au)</th>
                                            <th>∆E (eV)</th>
                                            <th>∆E (cm-1)</th>
                                            <th>Ref. Weight</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:for-each select="1 to count($energies)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <xsl:variable name="difference"
                                                select="number($energies[$outerIndex]) - number($energies[1])"/>
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="$outerIndex"/>
                                                </td>
                                                <td class="datacell">
                                                    <xsl:value-of select="$energies[$outerIndex]"/>
                                                </td>
                                                <td class="datacell">
                                                    <xsl:choose>
                                                        <xsl:when test="string($difference) = 'NaN'">0.00</xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of
                                                                select="format-number(number($difference) * $AU_TO_EV,'0.00')"
                                                            />
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                <td class="datacell">
                                                    <xsl:choose>
                                                        <xsl:when test="string($difference) = 'NaN'">0</xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of
                                                                select="format-number(number($difference) * $AU_TO_CM_1,'####0')"
                                                            />
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                <td class="datacell">
                                                    <xsl:value-of select="$weights[$outerIndex]"/>
                                                </td>
                                            </tr>
                                        </xsl:for-each>                                       
                                    </tbody>
                                </table>     
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#finalCASPT2EnergiesT-<xsl:value-of select="generate-id($caspt2modules[1])"/>").dataTable({
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false                                        
                                    });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>                        
    </xsl:template>
    
    <xsl:template name="finalSCFEnergy">
        <xsl:variable name="ksdft" select="(.//cml:module[@cmlx:templateRef='scf-ksdft'])[last()]"/>
        <xsl:if test="exists($ksdft)">
            <xsl:variable name="totalenergy" select="$ksdft/cml:scalar[@dictRef='m:scfener']"/>
            <xsl:variable name="mp2energy" select="(..//cml:module[@cmlx:templateRef='cchc']/cml:scalar[@dictRef='m:e2mp2energy'])[last()]"/>
            <xsl:variable name="ccsdenergy" select="(..//cml:module[@cmlx:templateRef='cchc']/cml:scalar[@dictRef='m:e2ccsdenergy'])[last()]"/>
            <xsl:variable name="ccsdtenergy" select="(..//cml:module[@cmlx:templateRef='ccsdt']/cml:scalar[@dictRef='m:ccsdtcorrenergy'])[last()]"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#naturalOcc-{generate-id($ksdft)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Final <xsl:value-of select="$ksdft/cml:scalar[@dictRef='m:program']"/> energy</h4>
                </div>            
                <div id="naturalOcc-{generate-id($ksdft)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="finalSCFEnergyT-{generate-id($ksdft)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total <xsl:value-of select="$ksdft/cml:scalar[@dictRef='m:program']"/> energy <xsl:if test="$functional != ''">(<xsl:value-of select="$functional"/>)</xsl:if></td>
                                            <td><xsl:value-of select="$totalenergy"/></td>                                            
                                        </tr>
                                        <xsl:if test="exists($mp2energy)">
                                            <tr>
                                                <td>Total MP2 energy</td>
                                                <td><xsl:value-of select="round-half-to-even(number($totalenergy) + number($mp2energy),10)"/></td>
                                            </tr>                                      
                                        </xsl:if>
                                        <xsl:if test="exists($ccsdenergy)">
                                            <tr>
                                                <td>Total CCSD</td>
                                                <td><xsl:value-of select="round-half-to-even(number($totalenergy) + number($ccsdenergy), 10)"/></td>
                                            </tr>                                            
                                        </xsl:if>                                        
                                        <xsl:if test="exists($ccsdtenergy)">
                                            <tr>
                                                <td>Total CCSD(T)</td>
                                                <td><xsl:value-of select="round-half-to-even(number($totalenergy) + number($ccsdtenergy), 10)"/></td>
                                            </tr>
                                        </xsl:if>
                                        <tr>
                                            <td>Total spin, S</td>
                                            <td><xsl:value-of select="format-number(round-half-to-even($ksdft/cml:scalar[@dictRef='m:spin'],3), '#0.000')"/></td>
                                        </tr>
                                        <tr>
                                            <td>Total spin, S(S+1)</td>
                                            <td><xsl:value-of select="format-number(round-half-to-even($ksdft/cml:scalar[@dictRef='m:spinsplusone'], 3), '#0.000')"/></td>
                                        </tr>
                                    </tbody>                                                                       
                                </table>
                              
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#finalSCFEnergyT-<xsl:value-of select="generate-id($ksdft)"/>").dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    });
                                </script>
                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="hzero">
        <xsl:variable name="extras" select="(.//cml:module[@cmlx:templateRef='extras'])[last()]"/>
        <xsl:if test="exists($extras)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#extras-{generate-id($extras)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">HZERO</h4>
                </div>            
                <div id="extras-{generate-id($extras)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="extrasT-{generate-id($extras)}">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>        
                                    <tbody>
                                        <tr>
                                            <td>H(0):</td>
                                            <td>
                                                <xsl:value-of select="$extras/cml:scalar[@dictRef='m:hzerooperator']"/>
                                            </td>
                                        </tr>
                                        <xsl:if test="exists($extras/cml:scalar[@dictRef='m:ipeashift'])">
                                            <tr>
                                                <td>IPEA shift</td>
                                                <td><xsl:value-of select="$extras/cml:scalar[@dictRef='m:ipeashift']"/></td>
                                            </tr>                                            
                                        </xsl:if>
                                        <xsl:if test="exists($extras/cml:scalar[@dictRef='m:shifti'])">
                                            <tr>
                                                <td>Imaginary level shift:</td>
                                                <td><xsl:value-of select="$extras/cml:scalar[@dictRef='m:shifti']"/></td>
                                            </tr>                                            
                                        </xsl:if>
                                        <xsl:if test="exists($extras/cml:scalar[@dictRef='m:shift'])">
                                            <tr>
                                                <td>Level shift:</td>
                                                <td><xsl:value-of select="$extras/cml:scalar[@dictRef='m:shift']"/></td>
                                            </tr>                                            
                                        </xsl:if>                                        
                                    </tbody>
                                </table>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
   
<!--
    <xsl:template name="waveFunctionNaturalMatrix">
        <xsl:variable name="waveFuncWeights" select="(.//cml:module[@cmlx:templateRef='wave.printout'])[last()]"/>
        <xsl:variable name="natural" select="$waveFuncWeights/cml:module[@cmlx:templateRef='natural']"/>
        <xsl:if test="exists($natural[1])">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#natural-{generate-id($natural[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Natural Occupation numbers</h4>
                </div>            
                <div id="natural-{generate-id($natural[1])}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12 col-sm-12">
                                <p>Active orbitals	Root</p>
                                <table class="display" id="naturalT-{generate-id($natural[1])}"></table>       
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#naturalT-<xsl:value-of select="generate-id($natural[1])"/>").dataTable({
                                        "aaData" : [
                                            <xsl:for-each select="1 to count($natural)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                <xsl:variable name="nat" select="$natural[$outerIndex]" />
                                                <xsl:variable name="number" select="$natural[$outerIndex]/cml:scalar[@dictRef='m:rootnumber']"/>
                                              
                                                <xsl:variable name="symmetry" select="'XXXX'"/>
                                                <xsl:variable name="occup" select="tokenize($natural[$outerIndex]/cml:array[@dictRef='m:occup'],'\s+')" />
                                                ['<xsl:value-of select="$symmetry"/>', <xsl:for-each select="$occup"><xsl:value-of select="."/><xsl:if test=". != $natural[last()]">,</xsl:if></xsl:for-each>]
                                                <xsl:if test="$outerIndex &lt; count($natural)">,</xsl:if>
                                            </xsl:for-each>
                                        <xsl:variable name="values" select="$natural[1]/cml:array[@dictRef='m:occup']/@size" />
                                        ],
                                        "aoColumns" : [
                                            { "sTitle": "" }, <xsl:for-each select="1 to $values"><xsl:variable name="outerIndex" select="."/>{ "sTitle": "<xsl:value-of select="$outerIndex"/>","sClass": "right" }<xsl:if test="$outerIndex &lt; $values">,</xsl:if></xsl:for-each>                                    
                                        ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
-->

    <!-- Harmonic frequencies -->
    <xsl:template name="vibrations">
        <xsl:variable name="frequencies" select=".//cml:module[@dictRef='cc:finalization']//cml:property[@dictRef='cc:frequencies']"/>
        <xsl:if test="exists($frequencies) and molcas:areFrequenciesValid($frequencies, $isOptimization, $isIncomplete, $isTS)">
            <xsl:variable name="vibrations" select="$frequencies/cml:module[@cmlx:templateRef='vibrations']" />
            <xsl:variable name="freqs" select="tokenize($vibrations/cml:array[@dictRef='cc:frequency'], '[\|\s]+')" />
            <xsl:variable name="irintensity" select="tokenize($vibrations/cml:array[@dictRef='cc:irintensity'], '\s+')" />
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies-{generate-id($frequencies)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Harmonic frequencies</h4>
                </div>            
                <div id="frequencies-{generate-id($frequencies)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <script type="text/javascript">
                                var customHarmonicFreqData = [];
                                var array_of_functions_harmonic_freq = [];                                
                                <xsl:for-each select="1 to count($irintensity)">
                                    <xsl:variable name="outerIndex" select="."/>                                                
                                function generateHarmonicFreqValues<xsl:value-of select="$outerIndex"/>(bandWidth, minX, maxX){
                                    var customHarmonicFreqData<xsl:value-of select="$outerIndex"/> = [];
                                    <xsl:text disable-output-escaping="yes" ><![CDATA[ for(var x = minX; x < maxX]]></xsl:text>
                                    <xsl:text disable-output-escaping="yes"><![CDATA[ ; x = x + 0.01 ) { ]]></xsl:text>
                                    if( <xsl:value-of select="$irintensity[$outerIndex]"/> * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - <xsl:value-of select="."/>),2)) <xsl:text disable-output-escaping="yes">> 0)</xsl:text>    
                                    customHarmonicFreqData<xsl:value-of select="$outerIndex"/>.push([ x,<xsl:value-of select="$irintensity[$outerIndex]"/> * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - <xsl:value-of select="."/>),2))]);
                                    }
                                    var customHarmonicFreqDataObject<xsl:value-of select="$outerIndex"/> = { name: 'Freq. <xsl:value-of select="$outerIndex"/>', data: customHarmonicFreqData<xsl:value-of select="$outerIndex"/> };
                                    customHarmonicFreqData.push(customHarmonicFreqDataObject<xsl:value-of select="$outerIndex"/>);
                                }
                                array_of_functions_harmonic_freq.push(generateHarmonicFreqValues<xsl:value-of select="$outerIndex"/>);
                                </xsl:for-each>
                            
                                <!-- Now we'll generate gaussian bell for every line-->                        
                                //On expand accordion we'll generate our graph, to be correctly resized 
                                $('#<xsl:value-of select="concat('frequencies-',generate-id($frequencies))"/>.panel-collapse').on('shown.bs.collapse', function () {
                                    generateHarmonicChart();                                   
                                })        
                                <xsl:text disable-output-escaping="yes">
                                 <![CDATA[                              
                                  $(function () {    
                                           $(document).ready(function(){
                                   ]]></xsl:text>
                                               $("#maxXHarmonic").val(<xsl:value-of select="number($freqs[last()]) + 1"/>);
                                               $("#minXHarmonic").val(<xsl:value-of select="0"/>);                                                  
                                               <xsl:text disable-output-escaping="yes">       
                                     <![CDATA[
                                         });
                                    });
                                     
                                     function generateHarmonicChart(){
                                          var data = fillHarmonicChart();
                                           
                                          var layout = {
                                            height: 300,
                                            margin: {
                                                l: 50,
                                                r: 50,
                                                b: 25,
                                                t: 25,
                                                pad: 0
                                            },
                                            xaxis: {
                                                title: 'cm-1',
                                                showgrid: true,
                                                zeroline: true,
                                                tickmode: 'auto'
                                            },                                            
                                            hovermode: 'closest',
                                            yaxis: {
                                                title: 'Intensity [km/mol]'
                                            }                                             
                                        };                                                
                                        Plotly.react('harmonicContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                     }
                                     
                                     function fillHarmonicChart(){
                                        // Now we'll generate root and all-roots-sum values and append them in customHarmonicFreqData array
                                        var bandWidth = $("#bandWidthHarmonic").val();
                                        var minX = parseFloat($("#minXHarmonic").val());
                                        var maxX = parseFloat($("#maxXHarmonic").val());
                                        
                                        customHarmonicFreqData = [];
                                        
                                        //Run every function
                                        for(var inx = 0; inx < array_of_functions_harmonic_freq.length; inx++ ){
                                            array_of_functions_harmonic_freq[inx](bandWidth, minX, maxX);
                                        }
                                        
                                        //Dump generated values into series
                                        var data = new Array();
                                        for(var inx = 0; inx < customHarmonicFreqData.length; inx++ ){
                                            var serieTmp = customHarmonicFreqData[inx];
                                            var xVal = new Array();
                                            var yVal = new Array();                                                
                                            for(var inx2 = 0;inx2 < serieTmp.data.length; inx2++){
                                                xVal.push(serieTmp.data[inx2][0]);
                                                yVal.push(serieTmp.data[inx2][1]);                                                
                                            }                                                
                                            var serie = {
                                                x : xVal,
                                                y : yVal,
                                                name : serieTmp.name                                                        
                                            };                                                
                                            data.push(serie);                                                                                                
                                        }
                                        return data;
                                     }                                         
                                        
                                  ]]>
                                  </xsl:text>
                            </script>
                            
                            <div class="col-md-12 col-sm-12">                                
                                <table id="frequenciesT-{generate-id($frequencies)}" class="display"/>
                                <script type="text/javascript">                                    
                                    $(document).ready(function(){
                                    $("table#frequenciesT-<xsl:value-of select="generate-id($frequencies)"/>").dataTable({
                                    "aaData" : [
                                    <xsl:for-each select="1 to count($freqs)">
                                        <xsl:variable name="outerIndex" select="."/>
                                        [ <xsl:value-of select="$outerIndex"/>, '<xsl:value-of select="$freqs[$outerIndex]"/>', '<xsl:value-of select="$irintensity[$outerIndex]"/>'] <xsl:if test="$outerIndex &lt; count($freqs)">,</xsl:if>
                                    </xsl:for-each>
                                    ],
                                    "aoColumns" : [
                                    { "sTitle": "" }, { "sTitle" : "Frequencies", "sClass": "left"}, { "sTitle": "IR Intensity", "sClass" : "right"}                                     
                                    ],
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false                                        
                                    });
                                    });
                                </script>                               
                                <!-- Zero point vibrational energy -->
                                <xsl:if test="exists(../cml:float[@id='zeroPointVibrationalEnergyAU'])">
                                    <p>Zero-point vibrational energy <xsl:value-of select="../cml:float[@id='zeroPointVibrationalEnergyAU']"/> au / 
                                        <xsl:value-of select="../cml:float[@id='zeroPointVibrationalEnergyKCALMOL']"/>
                                        kcal/mol
                                    </p>                
                                </xsl:if>
                            </div>
                            <div class="col-md-9 col-sm-12 mt-3">
                                <div id="harmonicContainer" style="min-height:350px"  class=" d-flex"/>
                            </div>                            
                            <div class="col-md-3 col-sm-12 mt-3 align-self-center">
                                <div id="harmonicControlsContainer" class="d-flex flex-column ">
                                    <p style="text-align:right;padding:0px;font-weight: 400;">Bandwidth: <input id="bandWidthHarmonic" type="text" size="5" value="1"/></p> 
                                    <p style="text-align:right;padding:0px;font-weight: 400;">min X: <input id="minXHarmonic" type="text" size="5" value="0"/></p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">max X: <input id="maxXHarmonic" type="text" size="5" value="10"/></p>
                                    <p style="text-align:right;padding:0px"><input type="button" value="Change" id="changeHarmonicParametersButton" onclick="generateHarmonicChart()"/></p>
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:function name="molcas:areFrequenciesValid" as="xs:boolean">        
        <xsl:param name="frequencies"/>
        <xsl:param name="isOptimization"/>
        <xsl:param name="isIncomplete"/>
        <xsl:param name="isTS"/>
                
        <xsl:variable name="countComplex" select="count(tokenize($frequencies//cml:array[@dictRef='cc:frequency'],'i')) - 1"/>        
        <xsl:choose>
            <xsl:when test="$isOptimization and $isIncomplete"><xsl:value-of select="false()"/></xsl:when>            
            <xsl:when test="$isOptimization and $isTS and $countComplex &lt; 2"><xsl:value-of select="true()"/></xsl:when>
            <xsl:when test="$countComplex &gt; 1"><xsl:value-of select="true()"/></xsl:when>
            <xsl:otherwise><xsl:value-of select="true()"/></xsl:otherwise>            
        </xsl:choose>        
    </xsl:function>


    <xsl:template name="mullikenSpinPopulationMatrix">
        <xsl:variable name="mullikenSpin" select="(.//cml:module[@dictRef='mulliken'][cml:module/cml:module[@cmlx:templateRef='mulliken.spin']])[last()]"/>
        <xsl:if test="exists($mullikenSpin)">
            <xsl:variable name="mullikenroot" select="$mullikenSpin/cml:module[@cmlx:templateRef='mulliken'][cml:module[@cmlx:templateRef='mulliken.spin']]"/>
            <xsl:variable name="rootcount" select="count($mullikenroot)" />
            <xsl:variable name="mspin">
                <xsl:for-each select="1 to $rootcount">
                    <xsl:variable name="outerIndex" select="."/>
                    <xsl:variable name="centers" select="tokenize($mullikenroot[$outerIndex]//cml:array[@dictRef='m:center'],'\|')"/>
                    <xsl:variable name="charges" select="tokenize($mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.spin']/cml:array[@dictRef='m:totalmulliken'],'\s+')"/>
                    <xsl:element name="cml:root">
                        <xsl:attribute name="number" select="$mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.header']//cml:scalar[@dictRef='m:rootnumber']/text()"/>
                        <xsl:for-each select="1 to count($centers)">
                            <xsl:variable name="innerIndex" select="."/>
                            <xsl:element name="cml:charge">
                                <xsl:attribute name="center" select="$centers[$innerIndex]"/>
                                <xsl:attribute name="value" select="$charges[$innerIndex]"/>
                            </xsl:element>
                        </xsl:for-each>                                                
                    </xsl:element>                   
                </xsl:for-each>                    
            </xsl:variable>
     
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#mullikenSpin-{generate-id($mullikenSpin)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Mulliken Spin Population</h4>
                </div>            
                <div id="mullikenSpin-{generate-id($mullikenSpin)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12 col-sm-12">
                                <table class="display" id="mullikenSpinT-{generate-id($mullikenSpin)}"></table>       
                                <script type="text/javascript">
                                            $(document).ready(function(){
                                                $("table#mullikenSpinT-<xsl:value-of select="generate-id($mullikenSpin)"/>").dataTable({
                                            "aaData" : [
                                            <xsl:for-each select="1 to count($mspin/cml:root[1]/cml:charge)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                ['<xsl:value-of select="$mspin/cml:root[1]/cml:charge[$outerIndex]/@center"/>',<xsl:for-each select="1 to $rootcount">
                                                    <xsl:variable name="innerIndex" select="."/>
                                                    <xsl:variable name="root" select="$mspin/cml:root[@number=$innerIndex]"/>                                                
                                                    '<xsl:value-of select="format-number(number($root/cml:charge[$outerIndex]/@value),'0.0000')"/>'<xsl:if test="$innerIndex &lt; $rootcount">,</xsl:if>
                                                </xsl:for-each>]<xsl:if test="$outerIndex &lt; count($mspin/cml:root[1]/cml:charge)">,</xsl:if>                                            
                                            </xsl:for-each> 
                                            ],
                                            "aoColumns" : [
                                            { "sTitle": "Atom/Root" }, <xsl:for-each select="1 to $rootcount"><xsl:variable name="outerIndex" select="."/>{ "sTitle": "<xsl:value-of select="$outerIndex"/>","sClass": "right" }<xsl:if test="$outerIndex &lt; $rootcount">,</xsl:if></xsl:for-each>                                    
                                            ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="dipoleMomentMatrix">
        <xsl:variable name="dipoleMoment" select="(.//cml:module[@dictRef='properties'])[last()]"/>
        <xsl:if test="exists($dipoleMoment)">           
            <xsl:variable name="roots" select="$dipoleMoment/cml:module[@cmlx:templateRef='properties']" />
            <xsl:variable name="colWidth" select="if(count($roots) &gt; 4) then '12' else '6'"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#dipoleMoment-{generate-id($dipoleMoment)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Electrostatic moments</h4>
                </div>            
                <div id="dipoleMoment-{generate-id($dipoleMoment)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-lg-{$colWidth} col-md-{$colWidth}">
                                <h4>Charge</h4>
                                <table class="display" id="charge">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><xsl:value-of select="molcas:formatCharge($charge)"/></td>
                                            </tr>
                                        </tbody>
                                </table>
                                
                                <h4>Dipole moment [Debye]</h4>
                                <table class="display" id="dipoleMomentT-{generate-id($dipoleMoment)}">
                                    <thead>
                                        <xsl:choose>
                                            <xsl:when test="count($roots) = 1">
                                                <tr>
                                                    <th> </th>
                                                    <th>1</th>
                                                </tr>                                                
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <tr>
                                                    <th rowspan="2"> </th>
                                                    <th colspan="{count($roots)}" style="text-align:center">Roots</th>                                                  
                                                </tr>
                                                <tr>
                                                    <xsl:for-each select="1 to count($roots)">
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <xsl:variable name="rootnumber" select="$roots[$outerIndex]/cml:scalar[@dictRef='m:rootnumber']"/>                                                                                                                
                                                        <th><xsl:value-of select="if(exists($rootnumber)) then $rootnumber else $outerIndex"/></th>    
                                                    </xsl:for-each>
                                                </tr>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </thead>
                                </table>       
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#dipoleMomentT-<xsl:value-of select="generate-id($dipoleMoment)"/>").dataTable({
                                        "aaData" : [
                                            <xsl:for-each select="('X','Y','Z', 'Total')">
                                                [
                                                <xsl:variable name="outerIndex" select="position()"/>                                                
                                                '<xsl:value-of select="."/>',
                                                <xsl:for-each select="1 to count($roots)">                                                   
                                                    <xsl:variable name="innerIndex" select="."/>
                                                    <xsl:variable name="rootnumber" select="$roots[$innerIndex]/cml:scalar[@dictRef='m:rootnumber']"/>                                                    
                                                    <xsl:value-of select="if($outerIndex != 4) then 
                                                                                (tokenize($roots[$innerIndex]//cml:list[@cmlx:templateRef='dipole']/cml:array[@dictRef='m:dipole'],'\s+'))[$outerIndex] 
                                                                           else
                                                                                $roots[$innerIndex]//cml:list[@cmlx:templateRef='dipole']/cml:scalar[@dictRef='m:total']
                                                                           "/>
                                                                          
                                                                            
                                                    <xsl:if test="$innerIndex &lt; count($roots)">,</xsl:if>
                                                </xsl:for-each>
                                                ]<xsl:if test="$outerIndex &lt; 4">,</xsl:if>
                                            </xsl:for-each>                                         
                                        ],
                                        "aoColumns" : [ { "sClass": "left" }, <xsl:for-each select="1 to count($roots)">{ "sClass": "right" }<xsl:if test=". &lt; count($roots)" >,</xsl:if></xsl:for-each> 
                                        ],                                         
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                            
                            <xsl:variable name="quadHeaders" select="('XX', 'XY', 'XZ', 'YY', 'YZ', 'ZZ', 'onethirdtrace', 'anisotropy')"/>                            
                            
                            <xsl:variable name="quadValues">
                                <xsl:for-each select="1 to count($roots)">
                                    <xsl:variable name="outerIndex" select="."/>
                                    <xsl:variable name="rootnumber" select="$roots[$outerIndex]/cml:scalar[@dictRef='m:rootnumber']"/>
                                    <xsl:variable name="quadValues" select="tokenize($roots[$outerIndex]//cml:list[@cmlx:templateRef='quadrupole']/cml:array[@dictRef='m:quadvalue'],'\s+')" />
                                    <xsl:element name="quadrupole">
                                        <xsl:attribute name="onethirdtrace" select="round-half-to-even( (number($quadValues[1]) + number($quadValues[4]) +  number($quadValues[6])) div 3 , 6)"></xsl:attribute>
                                        <xsl:attribute name="anisotropy" select="molcas:getAnisotropy(number($quadValues[1]),number($quadValues[4]),number($quadValues[6]),number($quadValues[2]),number($quadValues[3]),number($quadValues[5]))"/>
                                        <xsl:for-each select="1 to count($quadHeaders) -2 "><!-- Discard last two headers -->
                                            <xsl:variable name="innerIndex" select="."/>
                                            <xsl:attribute name="{$quadHeaders[$innerIndex]}" select="$quadValues[$innerIndex]"/>
                                        </xsl:for-each>
                                    </xsl:element>
                                </xsl:for-each>
                            </xsl:variable>
                                                        
                            <div class="col-lg-{$colWidth} col-md-{$colWidth}">
                                <h4>Quadrupole moment [Debye**Ang]</h4>
                                <table class="display" id="quadrupoleMomentT-{generate-id($dipoleMoment)}">
                                    <thead>
                                        <xsl:choose>
                                            <xsl:when test="count($roots) = 1">
                                                <tr>
                                                    <th> </th>
                                                    <th>1</th>
                                                </tr>                                                
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <tr>
                                                    <th rowspan="2"> </th>
                                                    <th colspan="{count($roots)}" style="text-align:center">Roots</th>                                                  
                                                </tr>
                                                <tr>
                                                    <xsl:for-each select="1 to count($roots)">
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <xsl:variable name="rootnumber" select="$roots[$outerIndex]/cml:scalar[@dictRef='m:rootnumber']"/>                                                                                                                
                                                        <th><xsl:value-of select="if(exists($rootnumber)) then $rootnumber else $outerIndex"/></th>    
                                                    </xsl:for-each>
                                                </tr>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </thead>                                
                                </table>
                                <script>
                                    $(document).ready(function(){
                                    $("table#quadrupoleMomentT-<xsl:value-of select="generate-id($dipoleMoment)"/>").dataTable({
                                        "aaData" : [
                                            <xsl:for-each select="1 to count($quadHeaders) ">
                                                <xsl:variable name="outerIndex" select="."/>
                                                ['<xsl:value-of select="if($quadHeaders[$outerIndex] ='onethirdtrace') then '1/3 trace' else if($quadHeaders[$outerIndex] = 'anisotropy') then 'Anisotropy' else $quadHeaders[$outerIndex]"/>',
                                                <xsl:for-each select="1 to count($roots)">
                                                    <xsl:variable name="innerIndex" select="."/>
                                                    <xsl:value-of select="($quadValues/*:quadrupole/@*[local-name()=$quadHeaders[$outerIndex]])[$innerIndex]"/>
                                                    <xsl:if test="$innerIndex &lt; count($roots)">,</xsl:if>
                                                </xsl:for-each>
                                                ]
                                                <xsl:if test="$outerIndex &lt; count($quadHeaders)">,</xsl:if>
                                            </xsl:for-each>                                            
                                            ],
                                            "aoColumns" : [ { "sClass": "left" }, <xsl:for-each select="1 to count($roots)">{ "sClass": "right" }<xsl:if test=". &lt; count($roots)" >,</xsl:if></xsl:for-each>
                                            ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });   
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:function name="molcas:getAnisotropy">
        <xsl:param name="xx"/>
        <xsl:param name="yy"/>
        <xsl:param name="zz"/>
        <xsl:param name="xy"/>
        <xsl:param name="xz"/>
        <xsl:param name="yz"/>
        
        <xsl:variable name="xx_yy_2" select="($xx - $yy) * ($xx - $yy)"/>
        <xsl:variable name="yy_zz_2" select="($yy - $zz) * ($yy - $zz)"/>
        <xsl:variable name="zz_xx_2" select="($zz - $xx) * ($zz - $xx)"/>
        <xsl:variable name="xy_2" select="$xy * $xy"/>
        <xsl:variable name="xz_2" select="$xz * $xz"/>
        <xsl:variable name="yz_2" select="$yz * $yz"/>
        
        <xsl:value-of select="round-half-to-even(ckbk:sqrt((0.5 * ($xx_yy_2 + $yy_zz_2 + $zz_xx_2)) + 3 * ($xy_2 + $xz_2 + $yz_2)),6)"/>        
    </xsl:function>
    
    <xsl:template name="populationAnalysis">
        <xsl:variable name="loprop" select="(.//cml:module[@dictRef='loprop'])[last()]"/>   
        <xsl:variable name="mulliken" select="(.//cml:module[@dictRef='mulliken'])[last()]"/>
        <xsl:if test="exists($loprop) or exists($mulliken)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#popanal-{generate-id($mulliken)}{generate-id($loprop)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Population analysis</h4>
                </div>            
                <div id="popanal-{generate-id($mulliken)}{generate-id($loprop)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <xsl:if test="exists($loprop)">
                                <xsl:variable name="loproproot" select="$loprop/cml:module[@cmlx:templateRef='loprop']"/>
                                <xsl:variable name="rootcount" select="count($loproproot)" />
                                <xsl:variable name="colWidth" select="if($rootcount &gt; 4) then '12' else '6'"/>
                                <xsl:variable name="lopropdata">
                                    <xsl:for-each select="1 to $rootcount">
                                        <xsl:variable name="outerIndex" select="."/>
                                        <xsl:variable name="labels" select="tokenize($loproproot[$outerIndex]//cml:array[@dictRef='m:label'],'[\|\s+]')"/>
                                        <xsl:variable name="charges" select="tokenize($loproproot[$outerIndex]//cml:array[@dictRef='m:loproptotal'],'\s+')"/>
                                        <xsl:element name="cml:root">
                                            <xsl:variable name="root" select="$loproproot[$outerIndex]//cml:scalar[@dictRef='m:rootnumber']/text()"/>
                                            <xsl:attribute name="number" select="if(exists($root)) then $root else $outerIndex "/>
                                            <xsl:for-each select="1 to count($labels)">
                                                <xsl:variable name="innerIndex" select="."/>
                                                <xsl:element name="cml:loprop">
                                                    <xsl:attribute name="label" select="$labels[$innerIndex]"/>
                                                    <xsl:attribute name="value" select="$charges[$innerIndex]"/>
                                                </xsl:element>
                                            </xsl:for-each>                                                
                                        </xsl:element>                   
                                    </xsl:for-each>                    
                                </xsl:variable>
                                <xsl:variable name="atomCount" select="count(tokenize($loproproot[1]//cml:array[@dictRef='m:label'],'[\|\s+]'))" />
                                    <div class="col-md-{$colWidth} col-sm-12">
                                        <h4>LoProp Charges</h4>
                                        <table class="display" id="loPropT-{generate-id($loprop)}"></table>       
                                        <script type="text/javascript">
                                            $(document).ready(function(){
                                            $("table#loPropT-<xsl:value-of select="generate-id($loprop)"/>").dataTable({
                                            "aaData" : [
                                                <xsl:for-each select="1 to $atomCount">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    ['<xsl:value-of select="$lopropdata/cml:root[1]/cml:loprop[$outerIndex]/@label"/>',<xsl:for-each select="1 to $rootcount"><xsl:variable name="innerIndex" select="."/><xsl:variable name="root" select="$lopropdata/cml:root[@number=$innerIndex]"/>'<xsl:value-of select="format-number(number($root/cml:loprop[$outerIndex]/@value),'0.0000')"/>'<xsl:if test="$innerIndex &lt; $rootcount">,</xsl:if></xsl:for-each>]<xsl:if test="$outerIndex &lt; $atomCount">,</xsl:if>                                            
                                                </xsl:for-each> 
                                                ],
                                                "aoColumns" : [{ "sTitle": "Atom/Root" }, <xsl:for-each select="1 to $rootcount"><xsl:variable name="outerIndex" select="."/>{ "sTitle": "<xsl:value-of select="$outerIndex"/>","sClass": "right" }<xsl:if test="$outerIndex &lt; $rootcount">,</xsl:if></xsl:for-each>],                                    
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false                                        
                                                });
                                            });                                           
                                        </script>
                                    </div>                                            
                            </xsl:if>
                            <xsl:if test="exists($mulliken)">
                                <xsl:variable name="hasAlphaBeta" select="exists($mulliken/cml:module[@cmlx:templateRef='mulliken'][cml:module[@cmlx:templateRef='mulliken.charges']]//cml:array[@dictRef='m:totalab'])"/>                                
                                <xsl:variable name="rootcount" select="count($mulliken/cml:module[@cmlx:templateRef='mulliken'][cml:module[@cmlx:templateRef='mulliken.charges']])"/>                                
                                <xsl:variable name="colWidth" select="if($rootcount &gt; 4) then '12' else '6'"/>                                
                                <div class="col-md-{$colWidth} col-sm-12">
                                    <h4>Mulliken atomic charges</h4>
                                    <table class="display" id="mullikenT-{generate-id($mulliken)}"></table>                                     
                                    <xsl:choose>
                                        <xsl:when test="not($hasAlphaBeta)">                                        
                                            <xsl:variable name="mullikenroot" select="$mulliken/cml:module[@cmlx:templateRef='mulliken'][cml:module[@cmlx:templateRef='mulliken.charges']]"/>
                                            <xsl:variable name="rootcount" select="count($mullikenroot)" />
                                            <xsl:variable name="nminuse">
                                                <xsl:for-each select="1 to $rootcount">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <xsl:variable name="centers" select="tokenize($mullikenroot[$outerIndex]//cml:array[@dictRef='m:center'],'[\|\s+]')"/>
                                                    <xsl:variable name="charges" select="tokenize($mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.charges']/cml:array[@dictRef='m:nminuse'],'\s+')"/>
                                                    <xsl:element name="cml:root">
                                                        <xsl:attribute name="number" select="
                                                            if ( exists($mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.header']/cml:scalar[@dictRef='m:rootnumber']/text())) then 
                                                            $mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.header']/cml:scalar[@dictRef='m:rootnumber']/text()
                                                            else 
                                                            '1'
                                                            "/>
                                                        <xsl:for-each select="1 to count($centers)">
                                                            <xsl:variable name="innerIndex" select="."/>
                                                            <xsl:element name="cml:charge">
                                                                <xsl:attribute name="center" select="$centers[$innerIndex]"/>
                                                                <xsl:attribute name="value" select="$charges[$innerIndex]"/>
                                                            </xsl:element>
                                                        </xsl:for-each>                                                
                                                    </xsl:element>                   
                                                </xsl:for-each>                    
                                            </xsl:variable>                                            
                                            <xsl:choose>
                                                <xsl:when test="count($nminuse/cml:root[@number=1]) &lt;= 1">
                                                    <script type="text/javascript">
                                                        $(document).ready(function(){
                                                        $("table#mullikenT-<xsl:value-of select="generate-id($mulliken)"/>").dataTable({
                                                        "aaData" : [
                                                        <xsl:for-each select="1 to count($nminuse/cml:root[1]/cml:charge)">
                                                            <xsl:variable name="outerIndex" select="."/>
                                                            ['<xsl:value-of select="$nminuse/cml:root[1]/cml:charge[$outerIndex]/@center"/>',<xsl:for-each select="1 to $rootcount">
                                                                <xsl:variable name="innerIndex" select="."/>
                                                                <xsl:variable name="root" select="$nminuse/cml:root[@number=$innerIndex]"/>                                                
                                                                '<xsl:value-of select="format-number(number($root/cml:charge[$outerIndex]/@value),'0.0000')"/>'<xsl:if test="$innerIndex &lt; $rootcount">,</xsl:if>
                                                            </xsl:for-each>]<xsl:if test="$outerIndex &lt; count($nminuse/cml:root[1]/cml:charge)">,</xsl:if>                                            
                                                        </xsl:for-each> 
                                                        ],
                                                        "aoColumns" : [
                                                        { "sTitle": "Atom/Root" }, <xsl:for-each select="1 to $rootcount"><xsl:variable name="outerIndex" select="."/>{ "sTitle": "<xsl:value-of select="$outerIndex"/>","sClass": "right" }<xsl:if test="$outerIndex &lt; $rootcount">,</xsl:if></xsl:for-each>                                    
                                                        ],
                                                        "bFilter": false,
                                                        "bPaginate": false,
                                                        "bSort": false,
                                                        "bInfo": false                                        
                                                        });
                                                        });                                     
                                                    </script>                                                                 
                                                </xsl:when>
                                                <xsl:otherwise>                                                    
                                                </xsl:otherwise>                                            
                                            </xsl:choose>
                                        </xsl:when>                                        
                                        <xsl:otherwise>                                                                               
                                            <xsl:variable name="mullikenroot" select="$mulliken/cml:module[@cmlx:templateRef='mulliken'][cml:module[@cmlx:templateRef='mulliken.charges']]"/>
                                            <xsl:variable name="rootcount" select="count($mullikenroot)" />
                                            <xsl:variable name="charge">
                                                <xsl:for-each select="1 to $rootcount">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <xsl:variable name="centers" select="tokenize($mullikenroot[$outerIndex]//cml:array[@dictRef='m:center'],'[\|\s+]')"/>
                                                    <xsl:variable name="charges" select="tokenize($mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.charges']/cml:array[@dictRef='m:charge'],'\s+')"/>
                                                    <xsl:variable name="totalab" select="tokenize($mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.charges']/cml:array[@dictRef='m:totalab'],'\s+')"/>
                                                    <xsl:element name="cml:root">
                                                        <xsl:attribute name="number" select="
                                                            if ( exists($mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.header']/cml:scalar[@dictRef='m:rootnumber']/text())) then 
                                                            $mullikenroot[$outerIndex]/cml:module[@cmlx:templateRef='mulliken.header']/cml:scalar[@dictRef='m:rootnumber']/text()
                                                            else 
                                                            '1'
                                                            "/>
                                                        <xsl:for-each select="1 to count($centers)">
                                                            <xsl:variable name="innerIndex" select="."/>
                                                            <xsl:element name="cml:charge">
                                                                <xsl:attribute name="center" select="$centers[$innerIndex]"/>
                                                                <xsl:attribute name="value" select="$charges[$innerIndex]"/>
                                                                <xsl:attribute name="totalab" select="number($totalab[(($innerIndex -1) * 2) + 1]) - number($totalab[(($innerIndex -1) * 2) + 2])"></xsl:attribute>
                                                            </xsl:element>
                                                        </xsl:for-each>                                                
                                                    </xsl:element>                   
                                                </xsl:for-each>                    
                                            </xsl:variable>
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                $("table#mullikenT-<xsl:value-of select="generate-id($mulliken)"/>").dataTable({
                                                "aaData" : [
                                                <xsl:for-each select="1 to count($charge/cml:root[1]/cml:charge)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    ['<xsl:value-of select="$charge/cml:root[1]/cml:charge[$outerIndex]/@center"/>',<xsl:for-each select="1 to $rootcount">
                                                        <xsl:variable name="innerIndex" select="."/>
                                                        <xsl:variable name="root" select="$charge/cml:root[@number=$innerIndex]"/>                                                
                                                        '<xsl:value-of select="format-number(number($root/cml:charge[$outerIndex]/@value),'0.0000')"/>',
                                                        '<xsl:value-of select="format-number(number($root/cml:charge[$outerIndex]/@totalab),'0.0000')"/>'
                                                    </xsl:for-each>]<xsl:if test="$outerIndex &lt; count($charge/cml:root[1]/cml:charge)">,</xsl:if>                                            
                                                </xsl:for-each> 
                                                ],
                                                "aoColumns" : [
                                                { "sTitle": "Atom" }, <xsl:for-each select="1 to $rootcount">{ "sTitle": "Total","sClass": "right" },</xsl:for-each>{ "sTitle": "alpha-beta" , "sClass" : "right"}                                    
                                                ],
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false                                        
                                                });
                                                });                                     
                                            </script>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                </div>                                            
                            </xsl:if>
                        </div>
                    </div>
                </div>
            </div>                        
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="multipoleExpansion">
        <xsl:variable name="multipole" select=".//cml:module[@cmlx:templateRef='atom.expansion'][last()]"/>
        <xsl:if test="exists($multipole)">
            <xsl:variable name="serial" select="tokenize($multipole/cml:array[@dictRef='cc:serial'],'\s+')"/>
            <xsl:variable name="label" select="tokenize($multipole/cml:array[@dictRef='m:label'],'\s+')"/>
            <xsl:variable name="charge" select="tokenize($multipole/cml:array[@dictRef='m:atomiccharge'],'\s+')"/>
            <xsl:variable name="coords" select="tokenize($multipole/cml:matrix[@dictRef='x:coords'],'\s+')"/>
            <xsl:variable name="dipole" select="tokenize($multipole/cml:matrix[@dictRef='m:atomicdipole'],'\s+')"/>
            <xsl:variable name="quadrupole" select="tokenize($multipole/cml:matrix[@dictRef='m:atomicquadrupole'],'\s+')"/>
            <xsl:variable name="polarizability" select="tokenize($multipole/cml:matrix[@dictRef='m:atomicpolarizability'],'\s+')"/>
            <xsl:variable name="dipolesum" select="tokenize($multipole/cml:array[@dictRef='m:atomicdipolesum'],'\s+')"/>
            <xsl:variable name="quadrupolesum" select="tokenize($multipole/cml:array[@dictRef='m:atomicquadrupolesum'],'\s+')"/>
            <xsl:variable name="polarizabilitysum" select="tokenize($multipole/cml:array[@dictRef='m:atomicpolarizabilitysum'],'\s+')"/>

            <xsl:variable name="firstcolumncell" select="('charge','dipole','','','','quadrupole','','','','','','','','polarizibility','','','','','','','')" />
            <xsl:variable name="secondcolumncell" select="('','x','y','z','total','xx','xy','xz','yy','yz','zz','(1/3)trace','anisotropy','xx','xy','xz','yy','yz','zz','(1/3) trace','anisotropy')"/>            

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#multipoleExpansion-{generate-id($multipole)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Multipole Expansion Analysis</h4>
                </div>
                <div id="multipoleExpansion-{generate-id($multipole)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <table id="multipoleExpansionT-{generate-id($multipole)}" class="display">
                                    <thead>                                        
                                        <tr>
                                            <th rowspan="2"> </th>
                                            <th rowspan="2"> </th>                                            
                                            <th colspan="{count($label)}" style="text-align:center">Atom</th>                                            
                                        </tr>
                                        <tr>
                                            <xsl:for-each select="$label"><th><xsl:value-of select="."/></th></xsl:for-each>
                                            <th>Total</th>                                            
                                        </tr>
                                    </thead>                                    
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#multipoleExpansionT-<xsl:value-of select="generate-id($multipole)"/>").dataTable({
                                        "aaData" : [
                                        [ '<xsl:value-of select="$firstcolumncell[1]"/>', '<xsl:value-of select="$secondcolumncell[1]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($charge[$outerIndex])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(helper:sumStringArray($charge))"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[2]"/>', '<xsl:value-of select="$secondcolumncell[2]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($dipole[($outerIndex  - 1)* 3 + 1])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($dipolesum[1])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[3]"/>', '<xsl:value-of select="$secondcolumncell[3]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($dipole[($outerIndex  - 1)* 3 + 2])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($dipolesum[2])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[4]"/>', '<xsl:value-of select="$secondcolumncell[4]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($dipole[($outerIndex  - 1)* 3 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($dipolesum[3])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[5]"/>', '<xsl:value-of select="$secondcolumncell[5]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:getTotalDipole($dipole[($outerIndex  - 1)* 3 + 1], $dipole[($outerIndex  - 1)* 3 + 2], $dipole[($outerIndex  - 1)* 3 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getTotalDipole($dipolesum[1], $dipolesum[2], $dipolesum[3]))"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[6]"/>', '<xsl:value-of select="$secondcolumncell[6]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 1])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($quadrupolesum[1])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[7]"/>', '<xsl:value-of select="$secondcolumncell[7]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 2])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($quadrupolesum[2])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[8]"/>', '<xsl:value-of select="$secondcolumncell[8]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($quadrupolesum[3])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[9]"/>', '<xsl:value-of select="$secondcolumncell[9]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 4])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($quadrupolesum[4])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[10]"/>', '<xsl:value-of select="$secondcolumncell[10]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 5])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($quadrupolesum[5])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[11]"/>', '<xsl:value-of select="$secondcolumncell[11]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 6])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($quadrupolesum[6])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[12]"/>', '<xsl:value-of select="$secondcolumncell[12]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:getOneThirdTrace($quadrupole[($outerIndex  - 1)* 6 + 1] , $quadrupole[($outerIndex  - 1)* 6 + 4], $quadrupole[($outerIndex  - 1)* 6 + 6])"/>', </xsl:for-each> '<xsl:value-of select="molcas:getOneThirdTrace($quadrupolesum[1], $quadrupolesum[4] , $quadrupolesum[6])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[13]"/>', '<xsl:value-of select="$secondcolumncell[13]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($quadrupole[($outerIndex  - 1)* 6 + 1]),number($quadrupole[($outerIndex  - 1)* 6 + 4]),number($quadrupole[($outerIndex  - 1)* 6 + 6]),number($quadrupole[($outerIndex  - 1)* 6 + 2]),number($quadrupole[($outerIndex  - 1)* 6 + 3]),number($quadrupole[($outerIndex  - 1)* 6 + 5])))"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($quadrupolesum[1]),number($quadrupolesum[4]),number($quadrupolesum[6]),number($quadrupolesum[2]),number($quadrupolesum[3]),number($quadrupolesum[5])))"/>'],                                    
                                          [ '<xsl:value-of select="$firstcolumncell[14]"/>', '<xsl:value-of select="$secondcolumncell[14]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($polarizability[($outerIndex  - 1)* 6 + 1])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[1])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[15]"/>', '<xsl:value-of select="$secondcolumncell[15]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($polarizability[($outerIndex  - 1)* 6 + 2])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[2])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[16]"/>', '<xsl:value-of select="$secondcolumncell[16]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($polarizability[($outerIndex  - 1)* 6 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[3])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[17]"/>', '<xsl:value-of select="$secondcolumncell[17]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($polarizability[($outerIndex  - 1)* 6 + 4])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[4])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[18]"/>', '<xsl:value-of select="$secondcolumncell[18]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($polarizability[($outerIndex  - 1)* 6 + 5])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[5])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[19]"/>', '<xsl:value-of select="$secondcolumncell[19]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($polarizability[($outerIndex  - 1)* 6 + 6])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[6])"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[20]"/>', '<xsl:value-of select="$secondcolumncell[20]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:getOneThirdTrace($polarizability[($outerIndex  - 1)* 6 + 1] , $polarizability[($outerIndex  - 1)* 6 + 4], $polarizability[($outerIndex  - 1)* 6 + 6])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getOneThirdTrace($polarizabilitysum[1] , $polarizabilitysum[4], $polarizabilitysum[6]))"/>'],
                                          [ '<xsl:value-of select="$firstcolumncell[21]"/>', '<xsl:value-of select="$secondcolumncell[21]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($polarizability[($outerIndex  - 1)* 6 + 1]),number($polarizability[($outerIndex  - 1)* 6 + 4]),number($polarizability[($outerIndex  - 1)* 6 + 6]),number($polarizability[($outerIndex  - 1)* 6 + 2]),number($polarizability[($outerIndex  - 1)* 6 + 3]),number($polarizability[($outerIndex  - 1)* 6 + 5])))"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($polarizabilitysum[1]),number($polarizabilitysum[4]),number($polarizabilitysum[6]),number($polarizabilitysum[2]),number($polarizabilitysum[3]),number($polarizabilitysum[5])))"/>'],
                                        ],
                                        "aoColumns" : [
                                            { "sClass": "left" }, { "sClass" : "left"} , <xsl:for-each select="1 to count($label)">{"sClass": "right" },</xsl:for-each>{ "sClass": "right"}                                    
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </xsl:if>
    </xsl:template>
 
    <xsl:function name="molcas:getTotalDipole">
        <xsl:param name="x"/>
        <xsl:param name="y"/>
        <xsl:param name="z"/>        
        <xsl:value-of select="molcas:formatMpoleNumber(ckbk:sqrt(number($x)*number($x) + number($y) * number($y) + number($z) * number($z)))"/>                
    </xsl:function>
    
    <xsl:function name="molcas:formatMpoleNumber">
        <xsl:param name="value"/>   
        <xsl:choose>
            <xsl:when test="round-half-to-even(number($value),4) = 0">
                <xsl:text>0.0000</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="format-number(round-half-to-even(number($value),4),'#0.0000')"/>        
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:function>
    
    <xsl:function name="molcas:formatCharge">
       <xsl:param name="charge"/> 
       <xsl:choose>
           <xsl:when test="number($charge) = 0">    <!-- Discard -0.00000 sign -->
                <xsl:value-of select="'0.000'"/>       
           </xsl:when>
           <xsl:otherwise>
               <xsl:value-of select="format-number(round-half-to-even($charge,3),'#0.000')" />       
           </xsl:otherwise>
       </xsl:choose>
    </xsl:function>
   
    <xsl:function name="molcas:getOneThirdTrace">
        <xsl:param name="xx"/>
        <xsl:param name="yy"/>
        <xsl:param name="zz"/>
        
        <xsl:value-of select="molcas:formatMpoleNumber((number($xx) + number($yy) + number($zz)) div 3)"/>       
    </xsl:function>
    
    <xsl:template name="loprop">
        <xsl:variable name="loprop" select=".//cml:module[@cmlx:templateRef='dynamic.loprop'][last()]"/>
        <xsl:if test="exists($loprop)">            
            <xsl:variable name="label" select="tokenize($loprop/cml:array[@dictRef='m:label'],'\s+')"/>
            <xsl:variable name="charge" select="tokenize($loprop/cml:array[@dictRef='m:lopropatomiccharge'],'\s+')"/>            
            <xsl:variable name="dipole" select="tokenize($loprop/cml:matrix[@dictRef='m:lopropatomicdipole'],'\s+')"/>
            <xsl:variable name="totaldipole" select="tokenize($loprop/cml:array[@dictRef='m:loproptotalatomicdipole'],'\s+')" />            
            <xsl:variable name="quadrupole" select="tokenize($loprop/cml:matrix[@dictRef='m:lopropatomicquadrupole'],'\s+')"/>
            <xsl:variable name="totalquadrupole" select="tokenize($loprop/cml:array[@dictRef='m:loproptotalatomicquadrupole'],'\s+')" />
            
            <xsl:variable name="polarizabilitysum" select="tokenize($loprop/cml:array[@dictRef='m:loproppolarizability'],'\s+')"/>            
            <xsl:variable name="firstcolumncell" select="('charge','dipole','','','','quadrupole','','','','','','','','polarizibility','','','','','','','')" />
            <xsl:variable name="secondcolumncell" select="('','x','y','z','total','xx','xy','xz','yy','yz','zz','(1/3)trace','anisotropy','xx','xy','xz','yy','yz','zz','(1/3) trace','anisotropy')"/>            
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#loProp-{generate-id($loprop)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">LoProp Analysis</h4>
                </div>
                <div id="loProp-{generate-id($loprop)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <table id="loPropT-{generate-id($loprop)}" class="display">
                                    <thead>                                        
                                        <tr>
                                            <th rowspan="2"> </th>
                                            <th rowspan="2"> </th>                                            
                                            <th colspan="{count($label)}" style="text-align:center">Atom</th>                                            
                                        </tr>
                                        <tr>
                                            <xsl:for-each select="$label"><th><xsl:value-of select="."/></th></xsl:for-each>
                                            <th>Total</th>                                            
                                        </tr>
                                    </thead>
                                </table>                                                                        
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#loPropT-<xsl:value-of select="generate-id($loprop)"/>").dataTable({
                                    "aaData" : [
                                    [ '<xsl:value-of select="$firstcolumncell[1]"/>', '<xsl:value-of select="$secondcolumncell[1]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($charge[$outerIndex])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(helper:sumStringArray($charge))"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[2]"/>', '<xsl:value-of select="$secondcolumncell[2]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($dipole[($outerIndex  - 1)* 3 + 1])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totaldipole[1])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[3]"/>', '<xsl:value-of select="$secondcolumncell[3]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($dipole[($outerIndex  - 1)* 3 + 2])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totaldipole[2])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[4]"/>', '<xsl:value-of select="$secondcolumncell[4]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($dipole[($outerIndex  - 1)* 3 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totaldipole[3])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[5]"/>', '<xsl:value-of select="$secondcolumncell[5]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:getTotalDipole($dipole[($outerIndex  - 1)* 3 + 1], $dipole[($outerIndex  - 1)* 3 + 2], $dipole[($outerIndex  - 1)* 3 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getTotalDipole($totaldipole[1], $totaldipole[2], $totaldipole[3]))"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[6]"/>', '<xsl:value-of select="$secondcolumncell[6]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 1])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totalquadrupole[1])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[7]"/>', '<xsl:value-of select="$secondcolumncell[7]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 2])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totalquadrupole[2])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[8]"/>', '<xsl:value-of select="$secondcolumncell[8]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 3])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totalquadrupole[3])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[9]"/>', '<xsl:value-of select="$secondcolumncell[9]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 4])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totalquadrupole[4])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[10]"/>', '<xsl:value-of select="$secondcolumncell[10]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 5])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totalquadrupole[5])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[11]"/>', '<xsl:value-of select="$secondcolumncell[11]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber($quadrupole[($outerIndex  - 1)* 6 + 6])"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($totalquadrupole[6])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[12]"/>', '<xsl:value-of select="$secondcolumncell[12]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:getOneThirdTrace($quadrupole[($outerIndex  - 1)* 6 + 1] , $quadrupole[($outerIndex  - 1)* 6 + 4], $quadrupole[($outerIndex  - 1)* 6 + 6])"/>', </xsl:for-each> '<xsl:value-of select="molcas:getOneThirdTrace($totalquadrupole[1], $totalquadrupole[4] , $totalquadrupole[6])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[13]"/>', '<xsl:value-of select="$secondcolumncell[13]"/>', <xsl:for-each select="1 to count($label)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($quadrupole[($outerIndex  - 1)* 6 + 1]),number($quadrupole[($outerIndex  - 1)* 6 + 4]),number($quadrupole[($outerIndex  - 1)* 6 + 6]),number($quadrupole[($outerIndex  - 1)* 6 + 2]),number($quadrupole[($outerIndex  - 1)* 6 + 3]),number($quadrupole[($outerIndex  - 1)* 6 + 5])))"/>', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($totalquadrupole[1]),number($totalquadrupole[4]),number($totalquadrupole[6]),number($totalquadrupole[2]),number($totalquadrupole[3]),number($totalquadrupole[5])))"/>'],
                                    
                                    [ '<xsl:value-of select="$firstcolumncell[14]"/>', '<xsl:value-of select="$secondcolumncell[14]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[1])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[15]"/>', '<xsl:value-of select="$secondcolumncell[15]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[2])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[16]"/>', '<xsl:value-of select="$secondcolumncell[16]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[3])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[17]"/>', '<xsl:value-of select="$secondcolumncell[17]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[4])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[18]"/>', '<xsl:value-of select="$secondcolumncell[18]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[5])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[19]"/>', '<xsl:value-of select="$secondcolumncell[19]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber($polarizabilitysum[6])"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[20]"/>', '<xsl:value-of select="$secondcolumncell[20]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getOneThirdTrace($polarizabilitysum[1] , $polarizabilitysum[4], $polarizabilitysum[6]))"/>'],
                                    [ '<xsl:value-of select="$firstcolumncell[21]"/>', '<xsl:value-of select="$secondcolumncell[21]"/>', <xsl:for-each select="1 to count($label)">'', </xsl:for-each> '<xsl:value-of select="molcas:formatMpoleNumber(molcas:getAnisotropy(number($polarizabilitysum[1]),number($polarizabilitysum[4]),number($polarizabilitysum[6]),number($polarizabilitysum[2]),number($polarizabilitysum[3]),number($polarizabilitysum[5])))"/>'],
                                    ],
                                    "aoColumns" : [
                                    { "sClass": "left" }, { "sClass" : "left"} , <xsl:for-each select="1 to count($label)">{"sClass": "right" },</xsl:for-each>{ "sClass": "right"}                                    
                                    ],
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false                                        
                                    });
                                    });                                     
                                </script>     
                            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>                                
    
    <xsl:function name="molcas:getNaturalHeaders">
        <xsl:param name="orbSpecs"/>
        <xsl:param name="properties"/>"        
        
        <xsl:variable name="symserial" select="tokenize($orbSpecs/cml:array[@dictRef='m:symserial'],'\s+')"/>
        <xsl:variable name="cols" select="count($symserial)"/>
        <xsl:variable name="symlabel" select="tokenize($orbSpecs/cml:array[@dictRef='m:symlabel'],'\s+')"/>
        <xsl:variable name="frozenorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:frozenorb'],'\s+')"/>
        <xsl:variable name="inactiveorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:inactiveorb'],'\s+')"/>        
        <xsl:variable name="activeorb" select="tokenize($orbSpecs/cml:array[@dictRef='m:activeorb'],'\s+')"/>
        
        <xsl:variable name="headers">     
            <xsl:if test="not(exists($symlabel)) and exists($properties)">

            </xsl:if>

            <xsl:if test="exists($activeorb)">
                <xsl:for-each select="1 to $cols">
                    <xsl:variable name="outerIndex" select="."/>
                    <xsl:if test="number($activeorb[$outerIndex]) >0">
                        <xsl:variable name="symLabel" select="if(exists($symlabel)) then $symlabel[$outerIndex] else 'a'"/>
                        <xsl:variable name="frozen" select="if(exists($frozenorb)) then xs:integer($frozenorb[$outerIndex]) else 0"/>
                        <xsl:variable name="inactive" select="if(exists($inactiveorb)) then xs:integer($inactiveorb[$outerIndex]) else 0"/>
                        <xsl:variable name="active" select="if(exists($activeorb)) then xs:integer($activeorb[$outerIndex]) else 0"/>
                        <xsl:for-each select="($frozen + $inactive + 1) to ($frozen+ $inactive + 1 + $active -1)">
                            <xsl:variable name="innerIndex" select="."/>
                            <xsl:element name="header">
                                <xsl:attribute name="specie" select="$outerIndex"/>                                
                                <xsl:attribute name="label" select="concat($innerIndex, $symLabel)"/>
                            </xsl:element>                            
                        </xsl:for-each>
                    </xsl:if>                
                </xsl:for-each>
            </xsl:if>    
        </xsl:variable>

        <xsl:copy-of select="$headers"/>
    </xsl:function>
    
    <xsl:function name="molcas:escapeQuotes">
        <xsl:param name="value"/>
        <xsl:value-of select="replace(replace($value,$quote,$quote_escaped),$singlequote,$singlequote_escaped)"/>
    </xsl:function>
    
    <!-- TEMPLATE -->
    <!--
    <xsl:template name="orbitalSpecsSymmetrySpecies">
        <xsl:variable name="orbSpecs" select="(.//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef=''])[last()]"/>
        <xsl:if test="exists($orbSpecs)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbSpecs-{generate-id($orbSpecs)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Orbital specifications</h4>
                </div>            
                <div id="orbSpecs-{generate-id($orbSpecs)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="orbSpecsT-{generate-id($orbSpecs)}"></table>       
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#orbSpecsT-<xsl:value-of select="generate-id($orbSpecs)"/>").dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    });                                     
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    -->
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>
        </div>
    </xsl:template>

    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>        
</xsl:stylesheet>
