<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:siesta="http://www.iochem-bd.org/dictionary/siesta/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk siesta helper cmlx"
    version="2.0">

    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Apr 23, 2022</xd:p>
            <xd:p><xd:b>Author:</xd:b>William Bro-Jørgensen</xd:p>
            <xd:p><xd:b>Center:</xd:b>Chemistry Department at University of Copenhagen</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:include href="helper/chemistry/siesta.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>

    <!-- Input parameters module -->
    <xsl:variable name="initialparameters" select="//cml/parameterList"/>

    <!-- Initialization module -->
    <xsl:variable name="method" select="'DFT'"/>
    <xsl:variable name="calcType" select="$initialparameters/parameter[@name='MD.TypeOfRun']"/>
    <xsl:variable name="functionalBlock" select="//cml/propertyList[@title='XC.Mix']"/>
    <xsl:variable name="kpointsInfo" select="//cml/propertyList[@title='k-points']"/>

    <!-- Environment module -->
    <xsl:variable name="metadatalist" select="//cml/metadataList"/>
    <xsl:variable name="programParameter" select = "$metadatalist/metadata[@name='siesta:Program']/@content"/>
    <xsl:variable name="versionParameter" select = "$metadatalist/metadata[@name='siesta:Version']/@content"/>
    <!-- <xsl:variable name="archParameter" select = "$metadatalist/metadata[@name='siesta:Arch']/@content"/> -->
    <!-- <xsl:variable name="flagsParameter" select = "$metadatalist/metadata[@name='siesta:Flags']/@content"/> -->
    <!-- <xsl:variable name="ppflagsParameter" select = "$metadatalist/metadata[@name='siesta:PPFlags']/@content"/> -->
    <xsl:variable name="mode" select = "$metadatalist/metadata[@name='siesta:Mode']/@content"/>
    <xsl:variable name="nodes" select = "$metadatalist/metadata[@name='siesta:Nodes']/@content"/>
    <!-- <xsl:variable name="netcdf" select = "$metadatalist/metadata[@name='siesta:NetCDF']/@content"/> -->
    <xsl:variable name="startTime" select = "$metadatalist/metadata[@name='siesta:StartTime']/@content"/>
    <xsl:variable name="endTime" select = "//cml/metadata[@name='siesta:EndTime']/@content"/>

    <!-- Geometry -->
    <xsl:variable name="initialMolecule" select="//cml/module[@title='Initial System']"/>
    <xsl:variable name="finalMolecule" select="//cml/module[@title='Finalization']"/>

    <!-- Calculation -->
    <xsl:variable name="finalpropertylist" select="//cml/module[@title='Finalization']/propertyList[@title='Final Energy']"/>
    <xsl:variable name="Etot" select="$finalpropertylist/property[@dictRef='siesta:Etot']"/>

    <!-- Pressure -->
    <xsl:variable name="pressurepropertylist" select="//cml/module[@title='Finalization']/propertyList[@title='Final Pressure']"/>
    <xsl:variable name="cellVolume" select="$pressurepropertylist/property[@dictRef='siesta:cellvol']"/>
    <xsl:variable name="pressSol" select="$pressurepropertylist/property[@dictRef='siesta:pressSol']"/>
    <xsl:variable name="pressMol" select="$pressurepropertylist/property[@dictRef='siesta:pressMol']"/>

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}../xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}../xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}../xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}../xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}../xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}../xslt/js/plotly-latest.min.js"/>

                <link rel="stylesheet" href="{$webrootpath}../xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}../xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}../xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}../xslt/css/bootstrap-theme.css" type="text/css" />

                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>
                        </div>
                    </div>

                    <!-- Settings section -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="settings"/>
                        </div>
                    </div>

                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>ATOM INFO</h3>
                            <div>
                                <xsl:variable name="molecule" select="
                                    if(exists($finalMolecule))
                                        then $finalMolecule
                                    else
                                        $initialMolecule
                                    "/>
                                <xsl:call-template name="atomicCoordinates">
                                    <xsl:with-param name="molecule" select="$molecule"/>
                                </xsl:call-template>
                            </div>
                        </div>
                    </div>

                    <!-- Molecular Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="molecularInfo"/>
                            <xsl:call-template name="kpoints"/>
                            <xsl:call-template name="functionalMixing"/>
                        </div>
                    </div>

                    <!-- Results -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <!-- <xsl:call-template name="noci"/> -->
                            <xsl:call-template name="energiesSection"/>
                            <!-- <xsl:call-template name="eigenvalues"/> -->
                            <xsl:call-template name="pressure"/>

                            <xsl:call-template name="timing"/>
                            <xsl:call-template name="printLicense"/>
                        </div>
                    </div>
                    <script type="text/javascript">
                        function expandModule(moduleID){
                        $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                        $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }

                        $(document).ready(function() {    
                        //Add custom styles to tables
                        $('div.dataTables_wrapper').each(function(){ 
                        $(this).children("table").addClass("compact");
                        $(this).children("table").addClass("display");
                        });

                        $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){
                        var tableName = $(this).children("table").attr("id");
                        if(tableName != null){
                        var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                        $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                        }
                        });
                        $.unblockUI();
                        });

                        function showDownloadOptions(tableName){
                        var table = $('#' + tableName).DataTable();
                        new $.fn.dataTable.Buttons( table, {
                        buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                        } );
                        table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                        $('div#downloadTable' + tableName + ' span.fa').hide();
                        }


                        $(window).resize(function() {
                        clearTimeout(window.refresh_size);
                        window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });

                        //Resize all tables on window resize to fit new width
                        var update_size = function() {
                        $('.dataTable').each(function(index){
                        var oTable = $(this).dataTable();
                        $(oTable).css({ width: $(oTable).parent().width() });
                        oTable.fnAdjustColumnSizing();
                        });
                        }

                        //On expand accordion we'll resize inner tables to fit current page width
                        $('.panel-collapse').on('shown.bs.collapse', function () {
                        $(this).find('.dataTable').each(function(index){
                        var oTable = $(this).dataTable();
                        $(oTable).css({ width: $(oTable).parent().width() });
                        oTable.fnAdjustColumnSizing();
                        });
                        })
                    </script>
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="molecularInfo">
        <div class="page-header">
            <h3>MOLECULAR INFO</h3>
        </div>
        <table>
            <tr>
                <td>Cell Volume:</td>
                <td>
                    <td><xsl:value-of select="round(number($cellVolume), 3)"/></td>
                    <xsl:text> </xsl:text>
                    <td><xsl:value-of select="siesta:convertSymbol(substring-after($cellVolume/scalar/@units, ':'))"/></td>
                </td>
            </tr>
        </table>
    </xsl:template>

    <xsl:template name="functionalMixing">
        <xsl:variable name="functionals" select="//cml/propertyList[@title='XC.Mix']/propertyList"/>
        <div class="row bottom-buffer">
            <div class="col-md-6 col-sm-12">
                <h4>Functional Mixing</h4>
                <table id="functionalsList" class="display">
                    <thead>
                        <tr>
                            <th>Functional</th>
                            <th>Authors</th>
                            <th>Weight (Ex)</th>
                            <th>Weight (Ec)</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="$functionals">
                            <xsl:variable name="functionalInfo" select="tokenize(.)"/>
                                <tr>
                                    <td><xsl:value-of select="$functionalInfo[1]"/></td>
                                    <td><xsl:value-of select="$functionalInfo[2]"/></td>
                                    <td><xsl:value-of select="number($functionalInfo[3])"/></td>
                                    <td><xsl:value-of select="number($functionalInfo[4])"/></td>
                                </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </div>

            <script type="text/javascript">
                $(document).ready(function() {
                    $('#functionalsList').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    });
            </script>

        </div>
    </xsl:template>

    <!-- kpoint list -->
    <xsl:template name="kpoints">
        <xsl:variable name="kcell" select="$kpointsInfo/property[@dictRef='siesta:kscell']"/>
        <div class="row bottom-buffer">
            <div class="col-md-6 col-sm-12">
                <h4>Kpoint list</h4>
                <table id="kpointList" class="display">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:variable name="kcellValues" select="tokenize($kcell)"/>
                        <xsl:for-each select="0 to 2">
                            <tr>
                                <xsl:variable name="outerIndex" select="." />
                                <xsl:for-each select="0 to 2">
                                    <xsl:variable name="innerIndex" select="." />
                                    <td><xsl:value-of select="number($kcellValues[($outerIndex * 3) + ($innerIndex) + 1])"/></td>
                                </xsl:for-each>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </div>

            <div class="col-md-6 col-sm-12">
                <table id="kpointCoordinates" class="display">
                    <thead>
                        <tr>
                            <th colspan="4">Coordinates</th>
                        </tr>
                        <tr>
                            <th style="text-align:right">x</th>
                            <th style="text-align:right">y</th>
                            <th style="text-align:right">z</th>
                            <th style="text-align:right">Weight</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="$kpointsInfo/kpoint">
                            <xsl:variable name="outer" select="tokenize(./@coords)"/>
                            <tr>
                                <td class="right"><xsl:value-of select="format-number(number($outer[1]),'#0.0000000')"/></td>
                                <td class="right"><xsl:value-of select="format-number(number($outer[2]),'#0.0000000')"/></td>
                                <td class="right"><xsl:value-of select="format-number(number($outer[3]),'#0.0000000')"/></td>
                                <td class="right"><xsl:value-of select="number(./@weight)"/></td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
            </div>
            <script type="text/javascript">
                $(document).ready(function() {
                    $('#kpointList').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    $('#kpointCoordinates').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    });
            </script>
        </div>
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter"/>
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="helper:calculateHillNotationFormula($finalMolecule/molecule/atomArray/atom)"/>
                </td>
            </tr>
            <tr>
                <td>Calculation type:</td>
                <td><xsl:value-of select="$calcType"/></td>
            </tr>
            <tr>
                <td>Method(s):</td>
                <td><xsl:value-of select="$method"/></td>
            </tr>
            <tr>
                <td>Functional:</td>
                <td>
                    <xsl:for-each select="$functionalBlock/propertyList">
                        <xsl:value-of select="./property[@title='Functional']"/>
                        <xsl:value-of select="./property[@title='Authors']"/>
                    </xsl:for-each>
                </td>
            </tr>
        </table>
    </xsl:template>

    <!-- Settings -->
    <xsl:template name="settings">
        <h3>SETTINGS</h3>
        <div class="row bottom-buffer">
            <div class="col-md-5 col-sm-12">
                <table class="display compact" id="settings">
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th class="right">Value</th>
                            <th class="right">Units</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="$initialparameters/parameter">
                            <xsl:if test="./@name!='MD.TypeOfRun'">
                                <tr>
                                    <td><xsl:value-of select="./@name"/></td>
                                    <xsl:choose>
                                        <xsl:when test="string(number(./scalar)) = 'NaN'">
                                            <td class="right"><xsl:value-of select="./scalar"/></td>
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <td class="right"><xsl:value-of select="round(number(./scalar), 6)"/></td>
                                        </xsl:otherwise>
                                    </xsl:choose>
                                    <td><xsl:value-of select="siesta:convertSymbol(substring-after(./scalar/@units, ':'))"/></td>
                                </tr>
                            </xsl:if>
                        </xsl:for-each>
                    </tbody>
                </table>
                <script>
                    $(document).ready(function() {
                    $('#settings').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    });
                </script>
            </div>
        </div>
    </xsl:template>

    <!-- Timing -->
    <xsl:template name="timing">
        <xsl:if test="exists($nodes)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#timing-{generate-id($nodes)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Timing
                    </h4>
                </div>
                <div id="timing-{generate-id($nodes)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <table id="timingT-{generate-id($nodes)}">
                                    <thead>
                                        <tr>
                                            <th>Environment</th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Mode</td>
                                            <td><xsl:value-of select="$mode"/></td>
                                        </tr>
                                        <tr>
                                            <td>Number of nodes</td>
                                            <td><xsl:value-of select="$nodes"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('table#timingT-<xsl:value-of select="generate-id($nodes)"/>').dataTable( {
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 1 ] }]
                                        } );
                                    } );
                                </script>
                            </div>
                        </div>

                        <xsl:if test="exists($endTime)">
                        </xsl:if>

                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <table id="timingT-{generate-id($endTime)}">
                                    <thead>
                                        <tr>
                                            <th>Timing</th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Start</td>
                                            <td><xsl:value-of select="$startTime"/></td>
                                        </tr>
                                        <tr>
                                            <td>End</td>
                                            <td><xsl:value-of select="$endTime"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('table#timingT-<xsl:value-of select="generate-id($endTime)"/>').dataTable( {
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false,
                                    "aoColumnDefs" : [
                                    { "sClass": "text-right", "aTargets": [ 1 ] }
                                    ]
                                    } );
                                    } );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">
        <xsl:param name="molecule"/>
        <xsl:variable name="unitcell" select="$molecule/lattice[@dictRef='siesta:ucell']"/>
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                                      
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h5>Lattice vectors</h5>
                                <table id="latticeVectors" class="display">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:variable name="unitcellValues" select="tokenize($unitcell)"/>
                                        <xsl:for-each select="0 to 2">
                                            <tr>
                                                <xsl:variable name="outerIndex" select="." />
                                                <xsl:for-each select="0 to 2">
                                                    <xsl:variable name="innerIndex" select="." />
                                                    <td><xsl:value-of select="round(number($unitcellValues[($outerIndex * 3) + ($innerIndex) + 1]), 6)"/></td>
                                                </xsl:for-each>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                </table>    
                            </div>
                            <div class="col-md-12 col-sm-12">
                                <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                    <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                                </div>                                                       
                                <table id="atomicCoordinates"></table>
                                
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        buildLatticeVectorsTable();
                                        buildAtomicCoordinatesTable();
                                    });

                                    function buildLatticeVectorsTable() {
                                        $('#latticeVectors').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    }

                                    function buildAtomicCoordinatesTable(){
                                        $('table#atomicCoordinates').dataTable( {
                                        "aaData": [
                                        <xsl:for-each select="$molecule/molecule/atomArray/atom">
                                            <xsl:variable name="elementType" select="@elementType"/>
                                            [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.0000')"/>","<xsl:value-of select="format-number(@y3,'#0.0000')"/>","<xsl:value-of select="format-number(@z3,'#0.0000')"/>"]<xsl:if test="(position() &lt; count($molecule/molecule/atomArray/atom))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>    
                                        ],
                                        "aoColumns": [
                                        { "sTitle": "ATOM" },
                                        { "sTitle": "" },
                                        { "sTitle": "x", "sClass": "right" },
                                        { "sTitle": "y", "sClass": "right" },
                                        { "sTitle": "z", "sClass": "right" }, 
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "bDestroy": true
                                        });
                                    }                                       
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>

    <!-- Energies section (module) -->
    <xsl:template name="energiesSection">
        <xsl:param name="energy"/>
        <xsl:variable name="uid" select="generate-id($Etot)"/>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#energies-{$uid}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Energies
                </h4>
            </div>
            <div id="energies-{$uid}" class="panel-collapse collapse show">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-4 col-sm-12">
                            <table class="display" id="energiesT-{$uid}">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th></th>
                                        <th></th>    
                                    </tr>                                        
                                </thead>
                                <tbody>
                                    <xsl:for-each select="$finalpropertylist/property">
                                        <tr>
                                            <td><xsl:value-of select="substring-after(./@dictRef, ':')"/></td>
                                            <td class="right"><xsl:value-of select="./scalar"/></td>
                                            <td><xsl:value-of select="substring-after(./scalar/@units, ':')"/></td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function(){
                                    $('#energiesT-<xsl:value-of select="$uid"/>').dataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });
                                });
                            </script>
                        </div>
                        <xsl:if test="normalize-space($calcType)='CG'">
                            <div class="col-md-8 col-sm-12">
                                <div id="iterationEnergyContainer" />  
                                <script type="text/javascript">
                                    <xsl:variable name="finalEnergies" select="//cml/module[@dictRef='Geom. Optim']"/>
                                    var energies = [<xsl:for-each select="1 to count($finalEnergies)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$finalEnergies[$outerIndex]/module[@title='SCF Finalization']/propertyList[@title='Energies and spin']/property[@dictRef='siesta:E_KS']"/><xsl:if test="$outerIndex &lt; count($finalEnergies)">,</xsl:if></xsl:for-each>];
                                    var indexes = [<xsl:for-each select="1 to count($finalEnergies)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$outerIndex"/><xsl:if test="$outerIndex &lt; count($finalEnergies)">,</xsl:if></xsl:for-each>];
                                    $(document).ready(function() {
                                    buildIterationEnergyGraph();
                                    });
                                    <xsl:text disable-output-escaping="yes">
                                        <![CDATA[
                                            function buildIterationEnergyGraph() {
                                                var data = new Array();
                                                var serie = {
                                                        x : indexes,
                                                        y : energies,                                                       
                                                        mode: 'lines+markers'                                                      
                                                };
                                                data.push(serie); 

                                                var xOffset = 0.25;
                                                var yOffset = (Math.max(...data[0].y) - Math.min(...data[0].y)) / 10;

                                                var layout = { 
                                                        height: 400,
                                                        margin: {
                                                        t: 40, r: 40, b: 40, pad: 0
                                                        },
                                                        modebar: {
                                                        orientation: 'v'
                                                        },
                                                        title: 'Geometric Optimization',                                                             
                                                        xaxis: {
                                                            title: {text: '# Iterations'},
                                                            showgrid: true,
                                                            showline: true,
                                                            automargin: true,
                                                            zeroline: true,
                                                            tickmode: 'auto',
                                                            range: [Math.min(...data[0].x) - xOffset, Math.max(...data[0].x) + xOffset],
                                                            tickformat:',d',
                                                            fixedrange : true
                                                        },
                                                        yaxis: {
                                                            title: { text: 'E_KS / eV'},
                                                            showgrid: true,
                                                            showline: true,
                                                            automargin: true,
                                                            range: [Math.min(...data[0].y) - yOffset, Math.max(...data[0].y) + yOffset],
                                                            tickmode: 'auto',
                                                            nticks : 10,
                                                            separatethousands : false,
                                                            fixedrange : true
                                                        },
                                                        separators: ','
                                                };
                                                Plotly.newPlot('iterationEnergyContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                            }
                                        ]]>
                                    </xsl:text>
                                </script>
                            </div>
                        </xsl:if>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- Pressure section -->
    <xsl:template name="pressure">  
        <xsl:if test="exists($cellVolume)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#pressure-{generate-id($cellVolume)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Pressure (static)
                    </h4>
                </div>
                <div id="pressure-{generate-id($cellVolume)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6"> 
                                <table id="pressureT-{generate-id($cellVolume)}">
                                    <thead>
                                        <tr>
                                            <th>Molecule</th>
                                            <th>Solid</th>    
                                            <th>Units</th>    
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><xsl:value-of select="round(number($pressSol), 6)"/></td>
                                            <td><xsl:value-of select="round(number($pressMol), 6)"/></td>
                                            <td><xsl:value-of select="substring-after($pressSol/scalar/@units, ':')"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#timingT-<xsl:value-of select="generate-id($cellVolume)"/>').dataTable( {
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 1 ] }]
                                        } );   
                                    } );                                       
                                </script>  
                            </div>
                        </div>             
                    </div>
                </div>
            </div> 
        </xsl:if>                
    </xsl:template>

    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>  
</xsl:stylesheet>
