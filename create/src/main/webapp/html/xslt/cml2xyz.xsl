<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"    
    xmlns:vasp="https://www.vasp.at/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions" 
    xmlns="http://www.xml-cml.org/schema"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs xd cml vasp helper cmlx"
    version="2.0">
    
    <!-- Given a CompChem CML file that contains frequency module, this stylesheet will build a multiple xyz file containing every vibrations and its displacements -->
    <xsl:include href="helper/chemistry/helper.xsl"/>
    
    <xsl:output method="text"/>    
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="mode"/>
   
    <xsl:template match="/">
        <xsl:choose>
            <xsl:when test="not($mode) or compare($mode,'xyz') = 0 or compare($mode,'single_xyz') = 0">
                <xsl:call-template name="xyz">
                    <xsl:with-param name="mode" select="$mode"/>
                </xsl:call-template>
            </xsl:when>            
            <xsl:when test="compare($mode,'vasp_neb') = 0">
                <xsl:call-template name="vaspneb"/>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    
    <xsl:template name="xyz">
        <xsl:param name="mode"/>
        <xsl:variable name="vibrations" select="(//module[@dictRef='cc:finalization']/propertyList/property/module[@cmlx:templateRef='vibrations'])[last()]"/>
        <xsl:variable name="finalMolecule" select="if(exists((//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule))
                                                        then (//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule
                                                        else (//module[@dictRef='cc:initialization' and child::molecule])[last()]//molecule"/>
        
        <xsl:variable name="atomCount" select="count($finalMolecule/atomArray/atom)"/>
        <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'\s+')"/>
        <xsl:variable name="type" select="tokenize($vibrations/array[@dictRef='v:freqtype'],'\s+')"/>
        <xsl:variable name="displacement" select="tokenize($vibrations/array[@dictRef='cc:displacement'],'\s+')"/>
        <xsl:variable name="coords" select="tokenize($vibrations/array[@dictRef='cc:coords'],'\s+')"></xsl:variable>    <!-- If this field exists, we'll use coordinates coming from this array instead of final geometry module ones -->
        <xsl:variable name="hasvibrationcoords" select="exists($coords)"/>
        
        <xsl:value-of select="$atomCount"/><xsl:text>&#xa;</xsl:text>
        <xsl:text>Base&#xa;</xsl:text>
        <xsl:variable name="outerIndex" select="."/>
        <xsl:for-each select="$finalMolecule/atomArray/atom">
            <xsl:variable name="innerIndex" select="position()"/>
            <xsl:choose>
                <xsl:when test="$hasvibrationcoords">
                    <xsl:value-of select="./@elementType"/><xsl:text>  </xsl:text><xsl:value-of select="$coords[($innerIndex -1 ) * 3 + 1]"/><xsl:text>  </xsl:text><xsl:value-of select="$coords[($innerIndex -1 ) * 3 + 2]"/><xsl:text>  </xsl:text><xsl:value-of select="$coords[($innerIndex -1 ) * 3 + 3]"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="./@elementType"/><xsl:text>  </xsl:text><xsl:value-of select="./@x3"/><xsl:text>  </xsl:text><xsl:value-of select="./@y3"/><xsl:text>  </xsl:text><xsl:value-of select="./@z3"/>        
                </xsl:otherwise>
            </xsl:choose>
            <xsl:text>&#xa;</xsl:text>                
        </xsl:for-each>        
        <xsl:if test="not($mode) or compare($mode,'xyz') = 0">
            <xsl:text>&#xa;</xsl:text>
            <xsl:for-each select="1 to count($frequency)">
                <xsl:variable name="outerIndex" select="."/>
                <xsl:value-of select="$atomCount"/><xsl:text>&#xa;</xsl:text>
                <xsl:text>Frequency  </xsl:text><xsl:if test="contains($type[$outerIndex],'f/i')"><xsl:text>-</xsl:text></xsl:if><xsl:value-of select="$frequency[$outerIndex]"/> <xsl:text>&#xa;</xsl:text>
                <xsl:for-each select="$finalMolecule/atomArray/atom">
                    <xsl:variable name="innerIndex" select="position()"/>
                    <xsl:choose>
                        <xsl:when test="$hasvibrationcoords"><xsl:value-of select="./@elementType"/><xsl:text>  </xsl:text><xsl:value-of select="$coords[($innerIndex -1 ) * 3 + 1]"/><xsl:text>  </xsl:text><xsl:value-of select="$coords[($innerIndex -1 ) * 3 + 2]"/><xsl:text>  </xsl:text><xsl:value-of select="$coords[($innerIndex -1 ) * 3 + 3]"/></xsl:when>
                        <xsl:otherwise><xsl:value-of select="./@elementType"/><xsl:text>  </xsl:text><xsl:value-of select="./@x3"/><xsl:text>  </xsl:text><xsl:value-of select="./@y3"/><xsl:text>  </xsl:text><xsl:value-of select="./@z3"/></xsl:otherwise>
                    </xsl:choose>
                    <xsl:text>  </xsl:text><xsl:value-of select="$displacement[(($outerIndex - 1) * $atomCount * 3 + ( ($innerIndex - 1) * 3) + 1)]"/><xsl:text>  </xsl:text><xsl:value-of select="$displacement[(($outerIndex - 1) * $atomCount * 3 + ( ($innerIndex - 1) * 3) + 2)]"/><xsl:text>  </xsl:text><xsl:value-of select="$displacement[(($outerIndex - 1) * $atomCount * 3 + ( ($innerIndex - 1) * 3) + 3)]"/><xsl:text>  </xsl:text>
                    <xsl:text>&#xa;</xsl:text>
                </xsl:for-each>
                <xsl:text>&#xa;</xsl:text>
            </xsl:for-each>               
        </xsl:if>                
    </xsl:template>   
    
    <xsl:template name="vaspneb">       
        <xsl:for-each select="//module[@dictRef='cc:finalization' and child::molecule]//molecule">
            <xsl:variable name="molecule" select="." />
            <xsl:variable name="index" select="position()" />                        
            <xsl:variable name="a" select="tokenize(((//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='lattice'])[1]/array[@dictRef='cc:lattice'])[1],'\s+')"/>
            <xsl:variable name="b" select="tokenize(((//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='lattice'])[1]/array[@dictRef='cc:lattice'])[2],'\s+')"/>
            <xsl:variable name="c" select="tokenize(((//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='lattice'])[1]/array[@dictRef='cc:lattice'])[3],'\s+')"/> 

            <xsl:value-of select="count($molecule/atomArray/atom)"/><xsl:text>&#xa;</xsl:text>
                <xsl:text>Image  </xsl:text>  
                <xsl:choose>
                    <xsl:when test="position()-1 &lt; 10"><xsl:value-of select="concat(0,position()-1)"/></xsl:when>
                    <xsl:otherwise><xsl:value-of select="position()-1"/></xsl:otherwise>
                </xsl:choose>    <xsl:text>&#xa;</xsl:text>            
            <xsl:for-each select="$molecule/atomArray/atom">                
                <xsl:variable name="innerIndex" select="position()"/>                               
                <xsl:value-of select="./@elementType"/><xsl:text>&#x20;</xsl:text><xsl:value-of select="./@x3"/><xsl:text>&#x20;&#x20;</xsl:text><xsl:value-of select="./@y3"/><xsl:text>&#x20;&#x20;</xsl:text><xsl:value-of select="./@z3"/>
                <xsl:text>&#xa;</xsl:text>
            </xsl:for-each>
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>        
    </xsl:template>
</xsl:stylesheet>
