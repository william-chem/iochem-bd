<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet  
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:adf="http://www.scm.com/ADF/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk adf helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Apr 5, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>       
    </xd:doc>
    <xsl:include href="helper/chemistry/adf.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>    
    <xsl:param name="browseurl"/>
    <xsl:param name="jcampdxurl"/>
                
    <xsl:variable name="runType" select="concat((//parameter[@dictRef='cc:runtype']/scalar/text())[last()], '')"/>
    <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])" /> 
    <xsl:variable name="isQuild" select="exists(//module[@cmlx:templateRef='quild.iteration'])"/>
    <xsl:variable name="isNMR" select="exists(//module[@cmlx:templateRef='nucleus'])"/>
    <xsl:variable name="calcType" select="adf:getCalcType($runType,$isQuild,$isNMR)"/>
    
    <!-- Environment module -->
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    <!-- Initializacion module -->
    <xsl:variable name="solvation" select="(//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='solvation'])[1]"/>
    <xsl:variable name="fragmentFiles" select="(//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='fragment.files'])[1]"/>
    <xsl:variable name="initialMolecule" select="(//module[@dictRef='cc:initialization' and child::molecule])[last()]//molecule"/>
    <xsl:variable name="functional" select="distinct-values(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:functional']/scalar)"/>
    <xsl:variable name="parameters" select="//module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='parameters']"/>
    <xsl:variable name="symmetry" select="distinct-values(//module[@dictRef='cc:calculation']//module[@cmlx:templateRef='symmetry']/scalar[@dictRef='a:symmetry'])"/>   

    <!-- Geometry -->
    <xsl:variable name="finalMolecule" select="((//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule)[1]"/>
    <!-- Thermochemistry -->
    <xsl:variable name="thermochemistryProperty" select="//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']"/>
    <xsl:variable name="temperature" select="($thermochemistryProperty[1]//scalar[@dictRef='cc:temp'])[1]"/>
    <xsl:variable name="pressure" select="($thermochemistryProperty[1]//scalar[@dictRef='cc:press'])[1]"/>
    <!-- Finalization module -->
    <xsl:variable name="converged" select="if(exists(//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:converged'])[last()]) then 
                                                (//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:converged'])[last()]
                                            else
                                                //list[@cmlx:templateRef='step']/scalar[@dictRef='x:converged' and upper-case(text())= 'CONVERGED'] "/>    
    <xsl:variable name="charge" select="if(exists((//module[@id='finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:charge'])[last()])) then
                                            (//module[@id='finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='logfile']/scalar[@dictRef='a:charge'])[last()] 
                                        else 
                                            //module[@cmlx:templateRef='symmetry']/scalar[@dictRef='a:charge']"/>
    <xsl:variable name="spinpolarization" select="(//scalar[@dictRef='a:spinPolarization'])[last()]"/>
    
    
    <xsl:variable name="multiplicity" select="if(exists($spinpolarization)) then
                                                    string(number($spinpolarization)+1)
                                              else if(exists(//module[@cmlx:templateRef='logfile'])) then
                                                    '1'
                                              else '' "/>

    <xsl:template match="/">        
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <xsl:if test="$hasVibrations">
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"></script>                    
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>                            
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>                    
                </xsl:if>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                       
                            <xsl:variable name="molecule" select="
                                if(exists($finalMolecule)) 
                                    then $finalMolecule
                                else
                                    $initialMolecule
                                "/>                                                             
                            <div>
                                <xsl:call-template name="atomicCoordinates">
                                    <xsl:with-param name="molecule"     select="$molecule"/>
                                    <xsl:with-param name="fragmentFiles" select="$fragmentFiles"/>                                                                                        
                                </xsl:call-template>                                        
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>MOLECULAR INFO</h3>                            
                            <xsl:call-template name="chargemultiplicity"/>                              
                            <xsl:call-template name="solvatationInfo"/>
                        </div>
                    </div>
                    <!-- Results -->
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">
                            <xsl:for-each select="//cml:module[@dictRef='cc:job']">                                
                                <xsl:variable name="scfConverged" select="exists(.//module[@id='calculation']//module[@cmlx:templateRef='scf']/scalar[@dictRef='cc:scfConverged'])"/>                                
                                <div id="module-{generate-id(.)}">                                
                                   <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small>
                                       <xsl:if test="exists($scfConverged)">
                                           <small> SCF Converged</small>
                                       </xsl:if>                                               
                                   </h3>
                                   <xsl:call-template name="bondingEnergy"/>
                                   <xsl:call-template name="fitTest"/>
                                   <xsl:call-template name="molecularOrbitals"/>
                                   <xsl:call-template name="orbitalEnergy"/>
                                   <xsl:call-template name="mullikenCharges"/>
                                   <xsl:call-template name="mdc"/>
                                   <xsl:call-template name="quadrupole"/>
                                   <xsl:call-template name="s2"/>
                                   <xsl:call-template name="frequencyIntensities"/>
                                   <xsl:call-template name="frequencies"/>
                                   <xsl:call-template name="zeropointenergy"/> 
                                   <xsl:call-template name="thermochemistry"/>
                                   <xsl:call-template name="excitations"/>
                                   <xsl:call-template name="rotatorystrengths"/>
                                   <xsl:call-template name="nmr"/>                                   
                                   <xsl:call-template name="timing"/>
                                   <xsl:call-template name="inputFile"/>                                                                                                          
                                   <br/>
                                </div>
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>                            
                        </div>
                    </div> 
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                         
                        $(document).ready(function() {    
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                        
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                            $.unblockUI();                             
                        });
                                               
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                                                
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
            </body>
        </html>
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>                               
            <tr>
                <td>Calculation type:</td>
                <td>
                    <xsl:value-of select="$calcType"/>
                    <xsl:choose>
                        <xsl:when test="exists($solvation)"><xsl:text>  (Solvation)</xsl:text></xsl:when>
                        <xsl:otherwise><xsl:text>  (Phase gas)</xsl:text></xsl:otherwise>
                    </xsl:choose>                                       
                </td>
            </tr>
            <tr>
                <td>Method(s):</td>
                <td>DFT<xsl:if test="exists($functional)">
                    <xsl:text> ( </xsl:text>
                    <xsl:for-each select="$functional">
                        <xsl:value-of select="concat(.,' ')"/>
                    </xsl:for-each>
                    <xsl:text> )</xsl:text>                    
                </xsl:if></td>                
            </tr>
            <xsl:if test="exists($parameters//scalar[@dictRef='cc:functional'])">
                <td></td>
                <td><xsl:for-each select="distinct-values($parameters//scalar[@dictRef='cc:functional'])">
                        <xsl:value-of select="."/><xsl:text> </xsl:text>    
                    </xsl:for-each>
                </td>
            </xsl:if>
            <xsl:if test="exists($parameters//scalar[@dictRef='a:relcor'])">
                <tr>
                    <td>Relativistic Corrections : </td>
                    <td>
                        <xsl:for-each select="distinct-values($parameters//scalar[@dictRef='a:relcor'])">
                            <xsl:value-of select="."/><xsl:text> </xsl:text>    
                        </xsl:for-each>
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="exists($parameters//scalar[@dictRef='a:coretreat'])">
                <tr>
                    <td>Core Treatment : </td>
                    <td>
                        <xsl:for-each select="distinct-values($parameters//scalar[@dictRef='a:coretreat'])">
                            <xsl:value-of select="."/><xsl:text> </xsl:text>
                        </xsl:for-each>                       
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="exists($parameters//scalar[@dictRef='a:electricField'])">
                <tr>
                    <td>Electric Field : </td>
                    <td>
                        <xsl:for-each select="distinct-values($parameters//scalar[@dictRef='a:electricField'])">
                            <xsl:value-of select="."/><xsl:text> </xsl:text>    
                        </xsl:for-each>
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="exists($parameters//scalar[@dictRef='a:zeeman'])">
                <tr>
                    <td>Hyperfine or Zeeman Interaction : </td>
                    <td>
                        <xsl:for-each select="distinct-values($parameters//scalar[@dictRef='a:zeeman'])">
                            <xsl:value-of select="."/><xsl:text> </xsl:text>    
                        </xsl:for-each>
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="exists($symmetry)">
                <tr>
                    <td>Symmetry : </td>
                    <td>
                        <xsl:for-each select="$symmetry">
                            <xsl:value-of select="."/><xsl:text> </xsl:text>
                        </xsl:for-each>
                    </td>
                </tr>
            </xsl:if>            
            <xsl:if test="exists($thermochemistryProperty)">                                  
                <tr>
                    <td>                                           
                        <xsl:text>Temperature</xsl:text>
                    </td>                                   
                    <td>                                            
                        <xsl:value-of select="$temperature/text()"></xsl:value-of>   
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($temperature/@units)"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <xsl:text>Pressure</xsl:text>
                    </td>
                    <td>
                        <xsl:value-of select="$pressure/text()"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($pressure/@units)"></xsl:value-of>
                    </td>
                </tr>
            </xsl:if>
        </table>        
    </xsl:template>

    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">        
        <xsl:param name="molecule"/>
        <xsl:param name="fragmentFiles"/>   
        <xsl:variable name="collapseAccordion" select="if(count($molecule/cml:atomArray/cml:atom) > 10) then '' else 'in'"/>
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                        Atomic coordinates [&#8491;] 
                            <xsl:if test="contains($calcType,$adf:GeometryOptimization)">
                                <xsl:choose>
                                    <xsl:when test="matches($converged/text(),$adf:GeometryConverged)">
                                        <small>(optimized)</small>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:choose>
                                            <xsl:when test="matches($converged/text(),$adf:GeometryNotConverged)">                                                
                                                <small><strong>(calculation did not converge)</strong></small>        
                                            </xsl:when>
                                        </xsl:choose>                                        
                                    </xsl:otherwise>
                                </xsl:choose>                                                           
                            </xsl:if>                    
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <tr>
                                        <td><xsl:value-of select="$title"/></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                        <xsl:variable name="id" select="@id"/>
                                        <tr>
                                            <td><xsl:value-of select="$elementType"/></td>
                                            <td><xsl:value-of select="format-number(@x3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@y3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@z3,'#0.000000')"/></td>
                                        </tr>
                                    </xsl:for-each>                                       
                                </tbody>                            
                            </table>                              
                            <script type="text/javascript">                                                                  
                                $(document).ready(function() {
                                $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                                });
                                $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                });
                                function getXYZ(){
                                var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                new $.fn.dataTable.Buttons( table, {
                                buttons: [ {
                                extend: 'copyHtml5',
                                text: 'XYZ'
                                }
                                ]
                                });
                                table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));                                    
                                $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                }     
                            </script>  
                            
                            
                            
                            <table id="atomicCoordinates"></table>
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#atomicCoordinates').dataTable( {
                                "aaData": [
                                /* Reduced data set */
                                <xsl:for-each select="$molecule/cml:atomArray/cml:atom">
                                    <xsl:variable name="elementType" select="@elementType"/>
                                    <xsl:variable name="id" select="@id"/>                      
                                    [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.0000')"/>","<xsl:value-of select="format-number(@y3,'#0.0000')"/>","<xsl:value-of select="format-number(@z3,'#0.0000')"/>","<xsl:value-of select="($fragmentFiles//cml:atom[@elementType=$elementType]/cml:scalar[@dictRef='cc:basis']/text())[1]"/>","<xsl:value-of select="($fragmentFiles//cml:atom[@elementType=$elementType]/cml:scalar[@dictRef='cc:contraction']/text())[1]"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                </xsl:for-each>    
                                ],
                                "aoColumns": [
                                { "sTitle": "ATOM" },
                                { "sTitle": "" },
                                { "sTitle": "x", "sClass": "right" },
                                { "sTitle": "y", "sClass": "right" },
                                { "sTitle": "z", "sClass": "right" },
                                { "sTitle": "TYPE" },
                                { "sTitle": "Core" }
                                ],
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                                } );   
                                } );                                       
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>

    <!-- Charge / Multiplicity section -->
    <xsl:template name="chargemultiplicity">
        <div class="row bottom-buffer">
            <div class="col-md-2 col-sm-6">
                <table class="display" id="chargemultiplicity">
                    <tbody>
                        <tr>
                            <td>Charge: </td>
                            <td class="right"><xsl:value-of select="$charge"/></td>                            
                        </tr>
                        <xsl:if test="$multiplicity != ''">
                            <tr>
                                <td>Multiplicity: </td>
                                <td class="right"><xsl:value-of select="$multiplicity"/></td>
                            </tr>
                        </xsl:if>                        
                        <xsl:if test="exists($spinpolarization)">
                            <tr>
                                <td>Spin polarization: </td>
                                <td class="right"><xsl:value-of select="($spinpolarization)"/></td>                                
                            </tr>
                        </xsl:if>            
                    </tbody>
                </table>        
            </div>
        </div>
    </xsl:template>
    
    <!-- Solvent input section (module) -->
    <xsl:template name="solvatationInfo">
        <xsl:if test="exists($solvation)">
            <div class="row bottom-buffer">
                <div class="col-sm-12 col-md-8 col-lg-6">
                    <h4>Solvation input</h4>                    
                    <xsl:variable name="cosmo" select="$solvation/list[@id='cosmo']"/>                                                         
                    <table class="display" id="solvationInfo">
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:ndiv'])">
                            <tr>
                                <td>
                                    Division Level for Surface Triangles  (NDIV)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:ndiv']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:nfdiv'])">
                            <tr>
                                <td>
                                    Final Division Level for Triangles (NFDIV)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:nfdiv']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>        
                        
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:rsol'])">
                            <tr>
                                <td>
                                    Radius of the Solvent (RSOL)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:rsol']"/>
                                </td>
                                <td><xsl:value-of select="helper:printUnitSymbol($cosmo/scalar[@dictRef='a:rsol']/@units)"/></td>
                            </tr>    
                        </xsl:if>        
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:rminsolv'])">
                            <tr>
                                <td>
                                    Minimum Radius for new sphere (RMINSOLV)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:rminsolv']"/>
                                </td>
                                <td><xsl:value-of select="helper:printUnitSymbol($cosmo/scalar[@dictRef='a:rminsolv']/@units)"/></td>
                            </tr>    
                        </xsl:if>        
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:ofac'])">
                            <tr>
                                <td>
                                    Overlapping Factor (OFAC)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:ofac']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>        
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:epsl'])">
                            <tr>
                                <td>
                                    Dielectric Constant (EPSL)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:epsl']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>        
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:cosmomethod'])">
                            <tr>
                                <td>
                                    COSMO equation is solved iteratively-
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:cosmomethod']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>        
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:ncix'])">
                            <tr>
                                <td>
                                    Maximun of Iterations for Charges (NCIX)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:ncix']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:ccnv'])">
                            <tr>
                                <td>
                                    Criterion for Charge convergence (CCNV)
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:ccnv']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>     
                        <xsl:if test="exists($cosmo/scalar[@dictRef='a:gdsf'])">
                            <tr>
                                <td>
                                    Geometry-dependent empirical factor
                                </td>
                                <td class="datacell">
                                    <xsl:value-of select="$cosmo/scalar[@dictRef='a:gdsf']"/>
                                </td>
                                <td></td>
                            </tr>    
                        </xsl:if>     
                    </table>                           
                </div>
            </div>       
        </xsl:if>
    </xsl:template>
    
    <!-- Input file section -->
    <xsl:template name="inputFile">
        <xsl:variable name="inputFile" select=".//module[@id='initialization']//module[@dictRef='cc:inputFile']"/>
        <xsl:if test="exists($inputFile)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#inputFile-{generate-id($inputFile)}" style="cursor: pointer; cursor: hand;">                    
                    <h4 class="panel-title">                        
                        Input file                                
                    </h4>
                </div>                 
                <div id="inputFile-{generate-id($inputFile)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <table id="inputFileT-{generate-id($inputFile)}"></table>                                    
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        var oTable = $('table#inputFileT-<xsl:value-of select="generate-id($inputFile)"/>').dataTable({                                            
                                            "aaData": [
                                            <xsl:for-each select="1 to count($inputFile/scalar)">
                                                <xsl:variable name="innerIndex" select="."/>
                                                [ '<xsl:value-of select="replace(($inputFile/scalar)[$innerIndex]/text(),'''','')"/>&#160;']<xsl:if test="$innerIndex &lt; count($inputFile/scalar)"><xsl:text>,</xsl:text></xsl:if>                         
                                            </xsl:for-each>
                                            ],
                                            "aoColumns": [
                                            { "sTitle": ""}                                          
                                            ],                                                                                      
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });                                       
                                   }); 
                                </script>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if>
    </xsl:template>
    
    <!-- Thermochemistry -->
    <xsl:template name="thermochemistry">
        <xsl:variable name="thermochemistry" select="./module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry']"/>        
        <xsl:if test="exists($thermochemistry)">
            <xsl:variable name="bonding" select="number((.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='bonding.energy']/cml:module[@cmlx:templateRef='summary']/cml:scalar[@dictRef='cc:total'])[last()])"/>
            <div class="panel panel-default">
                <div class="panel-heading"  data-toggle="collapse" data-target="div#thermochemistry-{generate-id($thermochemistry)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Thermochemistry                           
                    </h4>
                </div>
                <div id="thermochemistry-{generate-id($thermochemistry)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                      <div class="row bottom-buffer">
                          <div class="col-md-12 col-sm-12">                           
                              <table id="thermochemistryT-{generate-id($thermochemistry)}">
                                  <thead>
                                      <tr>
                                          <th class="right">Temp</th>
                                          <th> </th>
                                          <th class="right">Transl</th>
                                          <th class="right">Rotat</th>
                                          <th class="right">Vibrat</th>
                                          <th class="right">Total</th>                            
                                      </tr>
                                  </thead>
                                  <tbody>
                                      <xsl:for-each select="$thermochemistry/module[@cmlx:templateRef='energies']"> 
                                          <xsl:variable name="temp" select="./scalar[@dictRef='cc:temp']"/>
                                          <xsl:for-each select="./list">
                                              <xsl:variable name="outerIndex" select="position()"/>
                                              <tr>
                                                  <td class="right">   
                                                      <xsl:if test="$outerIndex = 1">
                                                          <xsl:value-of select="$temp"/>    
                                                      </xsl:if>                                    
                                                  </td>
                                                  <td>
                                                      <xsl:if test="compare(./@cmlx:templateRef,'entropy') = 0">Entropy <xsl:value-of select="concat('(',helper:printUnitSymbol(./scalar[1]/@units),')')"/>:</xsl:if>                                   
                                                      <xsl:if test="compare(./@cmlx:templateRef,'internalEnergy') = 0">Internal Energy <xsl:value-of select="concat('(',helper:printUnitSymbol(./scalar[1]/@units),')')"/>:</xsl:if>
                                                      <xsl:if test="compare(./@cmlx:templateRef,'heat') = 0">Constant Volume Heat Capacity <xsl:value-of select="concat('(',helper:printUnitSymbol(./scalar[1]/@units),')')"/>:</xsl:if>
                                                  </td>
                                                  <td class="right"><xsl:value-of select="./scalar[@dictRef='cc:transl']"/></td>
                                                  <td class="right"><xsl:value-of select="./scalar[@dictRef='cc:rotat']"/></td>
                                                  <td class="right"><xsl:value-of select="./scalar[@dictRef='cc:vibrat']"/></td>
                                                  <td class="right"><xsl:value-of select="./scalar[@dictRef='cc:total']"/></td>                            
                                              </tr>      
                                          </xsl:for-each>
                                          <xsl:variable name="internal" select="number((./cml:list[@cmlx:templateRef='internalEnergy']/cml:scalar[@dictRef='cc:total'])[last()])" />
                                          <xsl:variable name="entropy" select="number((./cml:list[@cmlx:templateRef='entropy']/cml:scalar[@dictRef='cc:total'])[last()])"/>
                                          <xsl:if test="exists($bonding) and exists($internal) and exists($entropy)">
                                              <xsl:variable name="freeenergy" select="(($bonding * 96.48531 * 1000 +  $internal * 4184 ) + (8.31441 * number($temp)) - (number($temp) * $entropy * 4.184)) div 1000"/>                                                  
                                              <tr>
                                                  <td></td>
                                                  <td>G (kJ.mol-1 // kcal.mol-1)</td>
                                                  <td></td>
                                                  <td></td>
                                                  <td></td>
                                                  <td class="right"><xsl:value-of select="round-half-to-even($freeenergy,1)"/> // <xsl:value-of select="round-half-to-even($freeenergy div 4.184 ,1)"/> </td>
                                              </tr>
                                          </xsl:if>                                                                                   
                                      </xsl:for-each>
                                  </tbody>              
                              </table>
                              <script type="text/javascript">
                                  $(document).ready(function() {
                                  $('table#thermochemistryT-<xsl:value-of select="generate-id($thermochemistry)"/>').dataTable({
                                          "bFilter": false,
                                          "bPaginate": false,
                                          "bSort": false,
                                          "bInfo": false
                                      });
                                  } );
                              </script>
                          </div>
                       </div>
                     </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Intensities -->
    <xsl:template name="frequencyIntensities">
        <xsl:variable name="intensities" select="adf:getIntensities(
                                                    .//module[@id='finalization']//property[@dictRef='cc:intensities']/module[@cmlx:templateRef='intensities'],
                                                    .//module[@id='finalization']//property/module[@cmlx:templateRef='scanfreq']
                                                    )"/>
        <xsl:if test="exists($intensities)">
            <xsl:variable name="frequency" select="tokenize($intensities/array[@dictRef='cc:frequency'],'\s+')"/>
            <xsl:variable name="dipole" select="tokenize($intensities/array[@dictRef='cc:dipole'],'\s+')"/>
            <xsl:variable name="absorption" select="tokenize($intensities/array[@dictRef='cc:absortion'],'\s+')"/>
            
            <div class="panel panel-default">
                <div class="panel-heading"  data-toggle="collapse" data-target="div#intensities-{generate-id($intensities)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Vibrational Frequencies and Intensities                           
                    </h4>
                </div>
                <div id="intensities-{generate-id($intensities)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-md-8 col-sm-12">                           
                                <table  id="intensitiesT-{generate-id($intensities)}"></table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#intensitiesT-<xsl:value-of select="generate-id($intensities)"/>').dataTable( {
                                            "aaData": [
                                            <xsl:for-each select="1 to count($frequency)">
                                                <xsl:variable name="innerIndex" select="."/>
                                                [ <xsl:value-of select="$frequency[$innerIndex]"/>,'<xsl:value-of select="$dipole[$innerIndex]"/>','<xsl:value-of select="$absorption[$innerIndex]"/>', ]<xsl:if test="$innerIndex &lt; count($frequency)"><xsl:text>,</xsl:text></xsl:if>                         
                                            </xsl:for-each>
                                            ],
                                            "aoColumns": [
                                                { "sTitle": "Frequency <xsl:value-of select="helper:printUnitSymbol($intensities/array[@dictRef='cc:frequency']/@units)"/>"},
                                                { "sTitle": "Dipole strength <xsl:value-of select="helper:printUnitSymbol($intensities/array[@dictRef='cc:dipole']/@units)"/>"},
                                                { "sTitle": "Absorption Intensity (degeneracy not counted) <xsl:value-of select="helper:printUnitSymbol($intensities/array[@dictRef='cc:absortion']/@units)"/>"}
                                            ],
                                            "aoColumnDefs" : [
                                                { "type": "string", "targets": [ 1,2 ] },                                   
                                                { "sClass": "text-right", "aTargets": [ 0,1,2 ] }
                                            ],
                                            "bFilter": false,
                                            "bPaginate": true,
                                            "bSort": false,
                                            "bInfo": true
                                        } );   
                                    } );                                       
                                </script>             
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- NMR -->
    <xsl:template name="nmr">
        <xsl:variable name="nmr" select=".//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='nmr']"/>
        <xsl:if test="exists($nmr)">
            <xsl:variable name="elementType" select="tokenize($nmr//array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="nucleus" select="tokenize($nmr//array[@dictRef='a:nucleus'], '\s+')"/>
            <xsl:variable name="paramagnetic" select="tokenize($nmr//array[@dictRef='a:paramagneticShielding'], '\s+')"/>
            <xsl:variable name="diamagnetic" select="tokenize($nmr//array[@dictRef='a:diamagneticShielding'], '\s+')"/>
            <xsl:variable name="spinOrbit" select="tokenize($nmr//array[@dictRef='a:spinorbitShielding'], '\s+')"/>
            <xsl:variable name="total" select="tokenize($nmr//array[@dictRef='a:total'], '\s+')"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#nmr-{generate-id($nmr)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        NMR Shielding Tensors                           
                    </h4>
                </div>
                <div id="nmr-{generate-id($nmr)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-12 col-sm-12">                           
                                <table id="nmrT-{generate-id($nmr)}">
                                    <thead>
                                        <tr>
                                            <th class="right">Atom</th>   
                                            <th class="right">Paramagnetic <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='a:paramagneticShielding']/@units),')')"/></th>
                                            <th class="right">Diamagnetic <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='a:diamagneticShielding']/@units),')')"/></th>
                                            <th class="right">Spin-orbit <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='a:spinorbitShielding']/@units),')')"/></th>
                                            <th class="right">Total <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='a:total']/@units),')')"/></th>
                                        </tr>                                        
                                    </thead>   
                                    <tbody>
                                        <xsl:for-each select="1 to count($elementType)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <tr>
                                                <td class="right"><xsl:value-of select="concat($elementType[$outerIndex],'(',$nucleus[$outerIndex],')')"/></td>   
                                                <td class="right"><xsl:value-of select="$paramagnetic[$outerIndex]"/></td>
                                                <td class="right"><xsl:value-of select="$diamagnetic[$outerIndex]"/></td>
                                                <td class="right"><xsl:value-of select="$spinOrbit[$outerIndex]"/></td>
                                                <td class="right"><xsl:value-of select="$total[$outerIndex]"/></td>
                                            </tr>
                                        </xsl:for-each>                                        
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('table#nmrT-<xsl:value-of select="generate-id($nmr)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    } );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </xsl:if>
    </xsl:template>
 
    <!-- Zero point vibrational energy -->
    <xsl:template name="zeropointenergy">        
        <xsl:variable name="zeroPoint" select=".//module[@id='finalization']/propertyList/property[@dictRef='cc:zeropoint']/scalar"/>               
        <xsl:if test="exists($zeroPoint)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#zpve-{generate-id($zeroPoint)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Zero Point Energy
                    </h4>
                </div>
                <div id="zpve-{generate-id($zeroPoint)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table id="zpveT-{generate-id($zeroPoint)}" class="display">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>    
                                        </tr>                                
                                    </thead>
                                    <tbody>                               
                                        <tr>
                                            <td>Zero-point</td>
                                            <td class="right"><xsl:value-of select="$zeroPoint/text()"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($zeroPoint/@units)"/></td>
                                        </tr>
                                    </tbody>                    
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#zpveT-<xsl:value-of select="generate-id($zeroPoint)"/>').dataTable({
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false
                                    });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Bonding energy -->
    <xsl:template name="bondingEnergy">
        <xsl:variable name="summary" select="(.//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='bonding.energy']/module[@cmlx:templateRef='summary'])[last()]"/>
        <xsl:if test="exists($summary)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#bondingEnergy-{generate-id($summary)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Bonding Energy (Summary)                           
                    </h4>
                </div>
                <div id="bondingEnergy-{generate-id($summary)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table id="bondingEnergyT-{generate-id($summary)}">
                                    <thead>
                                        <tr>
                                            <th>Type</th>
                                            <th>Value</th>
                                            <th>Units</th>    
                                        </tr>                                
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Electrostatic Energy</td>
                                            <td><xsl:value-of select="$summary/scalar[@dictRef='cc:eener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:eener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Kinetic Energy</td>
                                            <td><xsl:value-of select="$summary/scalar[@dictRef='cc:kinener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:kinener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Coulomb (Steric+OrbInt) Energy</td>
                                            <td><xsl:value-of select="$summary/scalar[@dictRef='cc:coulombener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:coulombener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>XC Energy</td>
                                            <td><xsl:value-of select="$summary/scalar[@dictRef='cc:xcener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:xcener']/@units)"/></td>
                                        </tr>
                                        <xsl:if test="exists($summary/scalar[@dictRef='cc:solvener'])">
                                            <tr>
                                                <td>Solvation</td>
                                                <td><xsl:value-of select="$summary/scalar[@dictRef='cc:solvener']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:solvener']/@units)"/></td>
                                            </tr>                                        
                                        </xsl:if>   
                                        <xsl:if test="exists($summary/scalar[@dictRef='cc:dispener'])">
                                            <tr>
                                                <td>Dispersion Energy</td>
                                                <td><xsl:value-of select="$summary/scalar[@dictRef='cc:dispener']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:dispener']/@units)"/></td>
                                            </tr>                                        
                                        </xsl:if>                                  
                                        <tr>
                                            <td>Total Bonding Energy</td>
                                            <td><xsl:value-of select="$summary/scalar[@dictRef='cc:total']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($summary/scalar[@dictRef='cc:total']/@units)"/></td>
                                        </tr>                                
                                    </tbody>                    
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#bondingEnergyT-<xsl:value-of select="generate-id($summary)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>        
    </xsl:template>

    <!-- Molecular Orbitals -->     
    <xsl:template name="molecularOrbitals">
        <xsl:variable name="molecularOrbitals" select="(.//module[@id='finalization']//module[@cmlx:templateRef='sfo.population']/module[@cmlx:templateRef='molecular.orbitals'])[last()]"/>
        <xsl:if test="exists($molecularOrbitals)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#molecularOrbitals-{generate-id($molecularOrbitals)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        MOs / SFO gross populations                           
                    </h4>
                </div>
                <div id="molecularOrbitals-{generate-id($molecularOrbitals)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <input type="radio" value="all" name="orbitalrangetype" onclick="javascript:filterTable(0)" checked="true">All</input>
                                <input type="radio" value="homolumo" name="orbitalrangetype" onclick="javascript:filterHomoLumo()">Homo/Lumo range:</input>
                                <input type="text"  value="10"       id="orbitalrange" oninput="javascript:filterHomoLumo()" />                                
                                <table id="molecularOrbitalsT-{generate-id($molecularOrbitals)}"></table>
                                <script type="text/javascript">
                                    var molecularOrbitalsData = [  
                                    <xsl:for-each select="$molecularOrbitals/list[@cmlx:templateRef='mo']">
                                        <xsl:variable name="outerIndex" select="position()"/> 
                                        <xsl:variable name="energy" select="./scalar[@dictRef='cc:mo.energy']"/>
                                        <xsl:variable name="occupation" select="./scalar[@dictRef='cc:mo.occupation']"/>
                                        <xsl:variable name="number" select="./scalar[@dictRef='cc:mo.number']"/>
                                        <xsl:variable name="label" select="./scalar[@dictRef='cc:mo.label']"/>
                                        <xsl:variable name="percent" select="tokenize(./array[@dictRef='cc:percent'],'\s+')"/>
                                        <xsl:variable name="sfo1" select="tokenize(./array[@dictRef='cc:sfo1'],'\s+')"/>
                                        <xsl:variable name="sfo2" select="tokenize(./array[@dictRef='cc:sfo2'],'\s+')"/>
                                        <xsl:variable name="partialEnergy" select="tokenize(./array[@dictRef='cc:energy'],'\s+')"/>
                                        <xsl:variable name="partialOccupation" select="tokenize(./array[@dictRef='cc:occupation'],'\s+')"/>
                                        <xsl:variable name="fragment1" select="tokenize(./array[@dictRef='cc:fragment1'],'\s+')"/>
                                        <xsl:variable name="fragment2" select="tokenize(./array[@dictRef='cc:fragment2'],'\s+')"/>                                        
                                        <xsl:for-each select="1 to count($partialEnergy)">
                                            <xsl:variable name="innerIndex" select="."/>
                                            <xsl:choose>
                                                <xsl:when test="$innerIndex = 1">[ <xsl:value-of select="$energy"/>, <xsl:value-of select="$occupation"/>,<xsl:value-of select="$number"/>, "<xsl:value-of select="$label"/>", <xsl:value-of select="$percent[$innerIndex]"/>, <xsl:value-of select="$sfo1[$innerIndex]"/>, "<xsl:value-of select="$sfo2[$innerIndex]"/>", <xsl:value-of select="$partialEnergy[$innerIndex]"/>,<xsl:value-of select="$partialOccupation[$innerIndex]"/>,<xsl:value-of select="$fragment1[$innerIndex]"/>, "<xsl:value-of select="$fragment2[$innerIndex]"/>"]<xsl:if test="($innerIndex &lt; count($partialEnergy)) or ($outerIndex &lt; count($molecularOrbitals/list[@cmlx:templateRef='mo']))"><xsl:text>,</xsl:text></xsl:if></xsl:when>
                                                <xsl:otherwise>["", "", "", "", <xsl:value-of select="$percent[$innerIndex]"/>, <xsl:value-of select="$sfo1[$innerIndex]"/>, "<xsl:value-of select="$sfo2[$innerIndex]"/>", <xsl:value-of select="$partialEnergy[$innerIndex]"/>,<xsl:value-of select="$partialOccupation[$innerIndex]"/>,<xsl:value-of select="$fragment1[$innerIndex]"/>, "<xsl:value-of select="$fragment2[$innerIndex]"/>"]<xsl:if test="($innerIndex &lt; count($partialEnergy)) or ($outerIndex &lt; count($molecularOrbitals/list[@cmlx:templateRef='mo']))"><xsl:text>,</xsl:text></xsl:if></xsl:otherwise>
                                            </xsl:choose>                                                                    
                                        </xsl:for-each>                                                   
                                    </xsl:for-each>
                                    ]; 
                                    
                                    $(document).ready(function() {                        
                                        $('table#molecularOrbitalsT-<xsl:value-of select="generate-id($molecularOrbitals)"/>').dataTable( {
                                            "aaData": molecularOrbitalsData,
                                            "aoColumns": [
                                                { "sTitle": "E(eV)", "sClass" : "right" },
                                                { "sTitle": "Occ"},
                                                { "sTitle": "MO"},
                                                { "sTitle": ""  },
                                                { "sTitle": "%"},
                                                { "sTitle": "SFO" },
                                                { "sTitle": "(first member)" },
                                                { "sTitle": "E(eV)", "sClass" : "right"  },
                                                { "sTitle": "Occ", "sClass" : "right"  },
                                                { "sTitle": "Fragment" },
                                                { "sTitle": "" }
                                            ],
                                            "bFilter": false,
                                            "bPaginate": true,
                                            "bSort": false,
                                            "bInfo": true
                                        } );   
                                    } );                                       
                                </script>                
                                <script type="text/javascript">                                                               
                                    var homoLumoFrontier = null;
                                    var molecularOrbitalsDataShort = null;
                                    
                                    function filterHomoLumo(){
                                        $("input[name='orbitalrangetype'][value='homolumo']").prop('checked',true);
                                        var range = $('input#orbitalrange').val();
                                        if(!$.isNumeric(range)){               
                                            range = 10;
                                        }
                                        filterTable(range);               
                                    }
                                    
                                    function findHomoRange(startIndex, range){
                                        energyChangesCounter = 0;
                                        for(inx = startIndex - 1; inx > 0 ; inx-- ){
                                            if(molecularOrbitalsData[inx][1].length != 0)                                          
                                                energyChangesCounter++;                                                                                                                    
                                            if(energyChangesCounter == range)
                                                return homoLumoFrontier - inx ;                                            
                                        }
                                        return 0;                                                                        
                                    }
                                    
                                    function findLumoRange(startIndex, range){
                                        energyChangesCounter = 0;    
                                        for(inx = startIndex + 1; inx &#60; molecularOrbitalsData.length ; inx++ ){
                                            if(molecularOrbitalsData[inx][1].length != 0)             
                                                energyChangesCounter++;                                                                                            
                                            if(energyChangesCounter == range)
                                                return (inx -1) - homoLumoFrontier;                                            
                                        }
                                        return molecularOrbitalsData.length - 1;
                                    }                                    
                                    
                                    function filterTable(range){                                                                                                            
                                        findHomoLumoFrontier();
                                        if(homoLumoFrontier == -1 || range == 0){
                                            molecularOrbitalsDataShort = molecularOrbitalsData;
                                        }else{                                        
                                            var lumoRange = findLumoRange(homoLumoFrontier, range);
                                            var homoRange = findHomoRange(homoLumoFrontier, range);
                                        
                                            var homoOrbitals = molecularOrbitalsData.slice(homoLumoFrontier - Number(homoRange) , homoLumoFrontier );
                                            var lumoOrbitals = molecularOrbitalsData.slice(homoLumoFrontier , homoLumoFrontier + Number(lumoRange) + 1);
                                            molecularOrbitalsDataShort = homoOrbitals.concat(lumoOrbitals);
                                        }                                    
                                        var table = $('#molecularOrbitalsT-<xsl:value-of select="generate-id($molecularOrbitals)"/>').DataTable();
                                        table.clear();
                                        table.rows.add(molecularOrbitalsDataShort);
                                        table.draw();                        
                                    }
                                    
                                    function findHomoLumoFrontier(){
                                        if(homoLumoFrontier == null){
                                            homoLumoFrontier = -1;
                                            for(inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> molecularOrbitalsData.length; inx++){
                                                if( (molecularOrbitalsData[inx][1] == 0) <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> (molecularOrbitalsData[inx][1].length != 0 )){
                                                    homoLumoFrontier = inx;
                                                    break;
                                                }
                                            }                              
                                        }
                                        return homoLumoFrontier;                          
                                    }
                                </script>                                                       
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Squared S -->
    <xsl:template name="s2">
        <xsl:variable name="s2" select="(.//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='s2'])"/>
        <xsl:variable name="s2exact" select="$s2/scalar[@dictRef='cc:s2']"/>
        <xsl:variable name="s2expected" select="$s2/scalar[@dictRef='cc:s2expected']"/>
        <xsl:if test="exists($s2)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#s2-{generate-id($s2)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        S**2
                    </h4>
                </div>
                <div id="s2-{generate-id($s2)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-sm-12">
                                <table class="display" >
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th class="right">exact</th>
                                            <th class="right">expectation value</th>
                                        </tr>  
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total S2 (S squared)</td>
                                            <td class="right"><xsl:value-of select="$s2exact/text()"/></td>
                                            <td class="right"><xsl:value-of select="$s2expected/text()"/></td>
                                        </tr>                           
                                    </tbody>                    
                                </table>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if> 
    </xsl:template>
    
    <!-- Fit Test -->
    <xsl:template name="fitTest">
        <xsl:variable name="fitTest" select="(.//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='fit.test'])[last()]"/>        
        <xsl:if test="exists($fitTest)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#fitTest-{generate-id($fitTest)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Fit Test                        
                    </h4>
                </div>
                <div id="fitTest-{generate-id($fitTest)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-8 col-sm-12">
                                <span>Fit test: (difference of exact and fit density, squared integrated, result summed over spins)</span>
                                <table class="display">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>  
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Sum-of-Fragments:</td>
                                            <td><xsl:value-of select="$fitTest/scalar[@dictRef='cc:sumfragments']/text()"/></td>                           
                                        </tr>
                                        <tr>
                                            <td>Orthogonalized Fragments:</td>
                                            <td><xsl:value-of select="$fitTest/scalar[@dictRef='cc:ortho']/text()"/></td>                           
                                        </tr>                           
                                        <tr>
                                            <td>SCF:</td>
                                            <td><xsl:value-of select="$fitTest/scalar[@dictRef='cc:fitscf']/text()"/></td>                           
                                        </tr>                           
                                    </tbody>                    
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>        
        </xsl:if> 
    </xsl:template>
    
    <!-- Mulliken charges and orbital occupation -->
    <xsl:template name="mullikenCharges">
        <xsl:variable name="charges" select="(.//module[@id='finalization']//module[@cmlx:templateRef='mulliken']/module[@cmlx:templateRef='charges'])[last()]"/>
        <xsl:if test="exists($charges)">
            
            <xsl:variable name="serial" select="tokenize($charges/list/*[@dictRef='cc:serial'],'\s+')"/>
            <xsl:variable name="elementType" select="tokenize($charges/list/*[@dictRef='cc:elementType'],'\s+')"/>
            <xsl:variable name="charge" select="tokenize($charges/list/*[@dictRef='x:charge'],'\s+')"/>
            <xsl:variable name="spinDensity" select="tokenize($charges/list/*[@dictRef='a:spinDensity'],'\s+')"/>
            <xsl:variable name="spin" select="tokenize($charges/list/*[@dictRef='a:spin'],'\s+')"/>
            <xsl:variable name="orbitalS" select="tokenize($charges/list/*[@dictRef='a:orbitalS'],'\s+')"/>
            <xsl:variable name="orbitalP" select="tokenize($charges/list/*[@dictRef='a:orbitalP'],'\s+')"/>
            <xsl:variable name="orbitalD" select="tokenize($charges/list/*[@dictRef='a:orbitalD'],'\s+')"/>
            <xsl:variable name="orbitalF" select="tokenize($charges/list/*[@dictRef='a:orbitalF'],'\s+')"/>            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#charges-{generate-id($charges)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Mulliken Atomic Charges   
                    </h4>
                </div>
                <div id="charges-{generate-id($charges)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <table id="chargesT-{generate-id($charges)}"></table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#chargesT-<xsl:value-of select="generate-id($charges)"/>').dataTable( {
                                    "aaData": [                                                                    
                                    <xsl:for-each select="1 to count($serial)">
                                        <xsl:variable name="innerIndex" select="."/>                                        
                                        <xsl:choose>
                                            <xsl:when test="not(exists($spin))">
                                                [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="$elementType[$innerIndex]"/>","<xsl:value-of select="$charge[$innerIndex]"/>","","","<xsl:value-of select="$orbitalS[$innerIndex]"/>","<xsl:value-of select="$orbitalP[$innerIndex]"/>","<xsl:value-of select="$orbitalD[$innerIndex]"/>","<xsl:value-of select="$orbitalF[$innerIndex]"/>"]<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                            </xsl:when>
                                            <xsl:otherwise>
                                                [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="$elementType[$innerIndex]"/>","<xsl:value-of select="$charge[$innerIndex]"/>",<xsl:value-of select="$spinDensity[$innerIndex]"/>,"<xsl:value-of select="$spin[$innerIndex * 2 - 1]"/>","<xsl:value-of select="$orbitalS[$innerIndex * 2 - 1]"/>","<xsl:value-of select="$orbitalP[$innerIndex * 2 - 1]"/>","<xsl:value-of select="$orbitalD[$innerIndex * 2 - 1]"/>","<xsl:value-of select="$orbitalF[$innerIndex * 2 - 1]"/>"],
                                                [ '','','','',"<xsl:value-of select="$spin[$innerIndex * 2]"/>","<xsl:value-of select="$orbitalS[$innerIndex * 2]"/>","<xsl:value-of select="$orbitalP[$innerIndex * 2]"/>","<xsl:value-of select="$orbitalD[$innerIndex * 2]"/>","<xsl:value-of select="$orbitalF[$innerIndex * 2]"/>"]<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    </xsl:for-each>                                                   
                                    ],
                                    "aoColumns": [
                                    { "sTitle": "Atom" },
                                    { "sTitle": ""},
                                    { "sTitle": "Charge"},
                                    { "sTitle": "Spin density"  },
                                    { "sTitle": ""},
                                    { "sTitle": "S" },
                                    { "sTitle": "P" },
                                    { "sTitle": "D" },
                                    { "sTitle": "F"  }                                    
                                    ],
                                    "aoColumnDefs" : [                           
                                    <xsl:if test="not(exists($spin))">
                                        { "bVisible": false, "aTargets": [ 3,4 ] },                                              
                                    </xsl:if>
                                    { "sClass": "text-right", "aTargets": [ 2,3,4,5,6,7,8 ] }                                    
                                    ],
                                    "bFilter": false,                                 
                                    "bPaginate": true,                                    
                                    "bSort": false,
                                    "bInfo": true
                                    } );   
                                    } );                                       
                                </script>                
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Frequencies section -->    
    <xsl:template name="frequencies">
        <xsl:variable name="frequencies" select="adf:getFrequencies(
                                                        .//module[@id='finalization']/propertyList/property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations'],
                                                        .//module[@id='finalization']//property/module[@cmlx:templateRef='scanfreq']
                                                        )"/>       
        <xsl:if test="exists($frequencies)">
            <xsl:variable name="frequency" select="tokenize($frequencies/array[@dictRef='cc:frequency'],'\s+')"/>
            <div id="vibrationPanel" class="panel panel-default" >
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        IR spectrum   
                    </h4>
                </div>
                <div id="frequencies" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-12">                                                                                             
                                <script type="text/javascript">
                                  	delete Jmol._tracker;
                                    Jmol._local = true;
                                
                                    jsvApplet0 = "jsvApplet0";
                                    jmolApplet0 = "jmolApplet0";                                
                                    Jmol.setAppletSync([jsvApplet0, jmolApplet0], ["load <xsl:value-of select="$jcampdxurl"/>", ""], true);                                       
                                    var jmolInfo = {
                                    width: 560,
                                    height: 400,
                                    debug: false,
                                    color: "0xF0F0F0",
                                    use: "HTML5",
                                    j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",                                        
                                    disableJ2SLoadMonitor: true,
                                    disableInitialConsole: true,
                                    readyFunction: vibrationsLoaded,
                                    animframecallback: "modelchanged",
                                    allowjavascript: true,
                                    script: "background white; vibration off; vectors off; sync on;"                                       
                                    }                                       
                                    var jsvInfo = {
                                    width: 560,
                                    height: 400,
                                    debug: false,
                                    color: "0xC0C0C0",
                                    use: "HTML5",
                                    j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",                                        
                                    script: null,
                                    initParams: null,
                                    disableJ2SLoadMonitor: true,
                                    disableInitialConsole: true,
                                    readyFunction: null,
                                    allowjavascript: true
                                    }
                                    
                                    $('div#frequencies').on('shown.bs.collapse', function () {
                                    if($("div#jmolApplet").children().length == 0){       
                                    use="HTML5";                                       
                                    jsvApplet0 = Jmol.getJSVApplet("jsvApplet0", jsvInfo);
                                    $("div#jsvApplet").html(Jmol.getAppletHtml(jsvApplet0))                                            
                                    jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                                    $("div#jmolApplet").html(Jmol.getAppletHtml(jmolApplet0));
                                    vibrationsLoading();
                                    }                                      
                                    });
                                    
                                    
                                    function vibrationsLoading(){                                       
                                    $('div#frequencies').block({ 
                                    message: '<h3>Loading spectrum</h3>', 
                                    css: { border: '3px solid #a00' } 
                                    });                                                                                    
                                    }
                                    
                                    function vibrationsLoaded(){
                                    $('div#frequencies').unblock();                                                           
                                    }             
                                    
                                </script>                                
                                <div id="irSpectrumDiv">
                                    <div id="jsvApplet" style="display:inline; float:left">                                                                         
                                    </div>
                                    <div id="jmolApplet" style="display:inline; float:left">
                                    </div>                                                                                                                               
                                </div>                                
                                <div id="frequencyComboboxDiv" style="display:block">
                                    Selected frequency : 
                                    <script type="text/javascript">
                                        function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                                            vibrationcboIndex = $("select[name='jmolMenu0'] option:selected").index();
                                            if(vibrationcboIndex != objwhat + 1)
                                                $("select[name='jmolMenu0']").val(objwhat + 2);                                                                                      
                                        }                    
                                        
                                        Jmol.jmolMenu(jmolApplet0,[                    
                                        ["vibration off", ".... select ....", true],
                                        <xsl:for-each select="1 to count($frequency)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            ["model <xsl:value-of select="$outerIndex"/>;vibration on" ,"<xsl:value-of select="$frequency[$outerIndex]"/>"]<xsl:if test="$outerIndex != count($frequency)"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>                    
                                        ]);                        
                                    </script>                    
                                </div>
                                <script type="text/javascript">
                                    $("#irSpectrumDiv" ).prepend( $("#frequencyComboboxDiv") );
                                </script>                                                              
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
       
    <!-- Multipole derived atomic charges -->
    <xsl:template name="mdc">
        <xsl:variable name="atomicCharges" select="(.//module[@id='finalization']//module[@cmlx:templateRef='atomic.charges']/list[@cmlx:templateRef='multipole'])[last()]"/>
        <xsl:variable name="atomicChargesSpin" select="(.//module[@id='finalization']//module[@cmlx:templateRef='atomic.charges.spin']/list[@cmlx:templateRef='spin'])[last()]"/>
        <xsl:variable name="spinDensity" select="(.//module[@id='finalization']//module[@cmlx:templateRef='spin.density']/list[@cmlx:templateRef='spinDensity'])[last()]"/>
        <xsl:variable name="uid" select="concat(generate-id($atomicCharges),generate-id($atomicChargesSpin),generate-id($spinDensity))"/>
        <xsl:if test="exists($atomicCharges) or exists($atomicChargesSpin) or exists($spinDensity)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#mdccharges-{$uid}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Multipole Derived Atomic Charges                           
                    </h4>
                </div>
                <div id="mdccharges-{$uid}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-8 col-md-8 col-sm-12">                                 
                                 <xsl:call-template name="atomicCharges">
                                     <xsl:with-param name="atomicCharges" select="$atomicCharges"/>
                                 </xsl:call-template>
                                <xsl:call-template name="atomicChargesSpin">
                                    <xsl:with-param name="atomicChargesSpin" select="$atomicChargesSpin"/>
                                </xsl:call-template>                              
                                <xsl:call-template name="spinDensity">
                                    <xsl:with-param name="spinDensity" select="$spinDensity"/>
                                </xsl:call-template>
                            </div>                
                        </div>
                    </div>
                </div>
            </div>  
        </xsl:if>
    </xsl:template>
    <!-- Multipole derived atomic charges : atomic charges -->
    <xsl:template name="atomicCharges">
        <xsl:param name="atomicCharges"/>
        <xsl:if test="exists($atomicCharges)">
            <xsl:variable name="serial" select="tokenize($atomicCharges/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="elementType" select="tokenize($atomicCharges/array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="mdcm" select="tokenize($atomicCharges/array[@dictRef='a:mdcm'], '\s+')"/>
            <xsl:variable name="mdcd" select="tokenize($atomicCharges/array[@dictRef='a:mdcd'], '\s+')"/>
            <xsl:variable name="mdcq" select="tokenize($atomicCharges/array[@dictRef='a:mdcq'], '\s+')"/>
            <div class="row bottom-buffer">
                <div class="col-md-12">
                    <h4>Multipole Derived Atomic Charges (a.u.)</h4>            
                    <table id="atomicChargesT-{generate-id($atomicCharges)}"></table>
                    <script type="text/javascript">
                        $(document).ready(function() {                        
                        $('table#atomicChargesT-<xsl:value-of select="generate-id($atomicCharges)"/>').dataTable( {
                            "aaData": [                                                                    
                            <xsl:for-each select="1 to count($serial)">
                                <xsl:variable name="innerIndex" select="."/>                                                          
                                [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="$elementType[$innerIndex]"/>",<xsl:value-of select="$mdcm[$innerIndex]"/>,<xsl:value-of select="$mdcd[$innerIndex]"/>,<xsl:value-of select="$mdcq[$innerIndex]"/>]<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>                   
                            </xsl:for-each>                                                   
                            ],
                            "aoColumns": [
                                { "sTitle": "Atom" },
                                { "sTitle": ""},
                                { "sTitle": "MDC-m"},
                                { "sTitle": "MDC-d"  },
                                { "sTitle": "MDC-q"}
                            ],
                            "aoColumnDefs" : [                                                               
                                { "sClass": "text-right", "aTargets": [ 2,3,4 ] }                                    
                            ],
                            "bFilter": false,                                 
                            "bPaginate": true,                                    
                            "bSort": false,
                            "bInfo": true
                            } );
                        } );                                       
                    </script>                    
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    <!-- Multipole derived atomic charges : atomic charges spin -->
    <xsl:template name="atomicChargesSpin">
        <xsl:param name="atomicChargesSpin"/>            
        <xsl:if test="exists($atomicChargesSpin)">
            <xsl:variable name="serial" select="tokenize($atomicChargesSpin/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="elementType" select="tokenize($atomicChargesSpin/array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="mdcm" select="tokenize($atomicChargesSpin/array[@dictRef='a:mdcm'], '\s+')"/>
            <xsl:variable name="mdcd" select="tokenize($atomicChargesSpin/array[@dictRef='a:mdcd'], '\s+')"/>
            <xsl:variable name="mdcq" select="tokenize($atomicChargesSpin/array[@dictRef='a:mdcq'], '\s+')"/>
            <div class="row bottom-buffer">
                <div class="col-md-12">
                    <h4>MDC atomic charges (spinA + spinB)</h4>           
                    <table id="atomicChargesSpinT-{generate-id($atomicChargesSpin)}"></table>
                    <script type="text/javascript">
                        $(document).ready(function() {                        
                        $('table#atomicChargesSpinT-<xsl:value-of select="generate-id($atomicChargesSpin)"/>').dataTable( {
                            "aaData": [                                                                    
                            <xsl:for-each select="1 to count($serial)">
                                <xsl:variable name="innerIndex" select="."/>                                                          
                                [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="$elementType[$innerIndex]"/>",<xsl:value-of select="$mdcm[$innerIndex]"/>,<xsl:value-of select="$mdcd[$innerIndex]"/>,<xsl:value-of select="$mdcq[$innerIndex]"/>]<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>                   
                            </xsl:for-each>                                                   
                            ],
                            "aoColumns": [
                                { "sTitle": "Atom" },
                                { "sTitle": ""},
                                { "sTitle": "MDC-m"},
                                { "sTitle": "MDC-d"  },
                                { "sTitle": "MDC-q"}
                            ],
                            "aoColumnDefs" : [                                                               
                                { "sClass": "text-right", "aTargets": [ 2,3,4 ] }                                    
                            ],
                            "bFilter": false,
                            "bPaginate": true,
                            "bSort": false,
                            "bInfo": true
                            } );   
                        } );                  
                    </script>                     
                </div>                
            </div>
        </xsl:if>
    </xsl:template>
    <!-- Multipole derived atomic charges : spin density -->
    <xsl:template name="spinDensity">
        <xsl:param name="spinDensity"/>        
        <xsl:if test="exists($spinDensity)">
            <xsl:variable name="serial" select="tokenize($spinDensity/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="elementType" select="tokenize($spinDensity/array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="mdcm" select="tokenize($spinDensity/array[@dictRef='a:mdcm'], '\s+')"/>
            <xsl:variable name="mdcd" select="tokenize($spinDensity/array[@dictRef='a:mdcd'], '\s+')"/>
            <xsl:variable name="mdcq" select="tokenize($spinDensity/array[@dictRef='a:mdcq'], '\s+')"/>
            <div class="row bottom-buffer">
                <div class="col-md-12">
                    <h4>MDC spin density (spinA - spinB)</h4>            
                    <table id="spinDensityT-{generate-id($spinDensity)}"></table>
                    <script type="text/javascript">
                        $(document).ready(function() {                        
                        $('table#spinDensityT-<xsl:value-of select="generate-id($spinDensity)"/>').dataTable( {
                        "aaData": [                                                                    
                        <xsl:for-each select="1 to count($serial)">
                            <xsl:variable name="innerIndex" select="."/>                                                          
                            [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="$elementType[$innerIndex]"/>",<xsl:value-of select="$mdcm[$innerIndex]"/>,<xsl:value-of select="$mdcd[$innerIndex]"/>,<xsl:value-of select="$mdcq[$innerIndex]"/>]<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>                   
                        </xsl:for-each>                                                   
                        ],
                        "aoColumns": [
                        { "sTitle": "Atom" },
                        { "sTitle": ""},
                        { "sTitle": "MDC-m"},
                        { "sTitle": "MDC-d"  },
                        { "sTitle": "MDC-q"}
                        ],
                        "bFilter": false,                                 
                        "bPaginate": true,                                    
                        "bSort": false,
                        "bInfo": true
                        } );   
                        } );                                       
                    </script>                                                            
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Orbital energies -->
    <xsl:template name="orbitalEnergy">
        <xsl:variable name="orbitalEnergies" select="
            if(exists(.//module[@id='finalization']//module[@cmlx:templateRef='orbital.energies']/list[@cmlx:templateRef='energies'])) then
                .//module[@id='finalization']//module[@cmlx:templateRef='orbital.energies']/list[@cmlx:templateRef='energies']
            else
                .//module[@id='finalization']//module[@cmlx:templateRef='orbital.energies.zora']/list[@cmlx:templateRef='energies']
            "/>
        
        <xsl:variable name="orbitalEnergiesSpin" select="
            if(exists(.//module[@id='finalization']//module[@cmlx:templateRef='orbital.energies.spin']/list[@cmlx:templateRef='energies'])) then 
                .//module[@id='finalization']//module[@cmlx:templateRef='orbital.energies.spin']/list[@cmlx:templateRef='energies']
            else
                .//module[@id='finalization']//module[@cmlx:templateRef='orbital.energies.spin.zora']/list[@cmlx:templateRef='energies']
            "/>
        <xsl:variable name="uid" select="concat(generate-id($orbitalEnergies),generate-id($orbitalEnergiesSpin))"/>
        <xsl:if test="exists($orbitalEnergies) or exists($orbitalEnergiesSpin)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbitalenergy-{$uid}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        MOs Energies                           
                    </h4>
                </div>
                <div id="orbitalenergy-{$uid}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-md-6 col-sm-12">                                 
                                <xsl:call-template name="orbitalEnergies">
                                    <xsl:with-param name="orbitalEnergies" select="$orbitalEnergies"/>
                                </xsl:call-template>
                                <xsl:call-template name="orbitalEnergiesSpin">
                                    <xsl:with-param name="orbitalEnergiesSpin" select="$orbitalEnergiesSpin"/>
                                </xsl:call-template>                              
                            </div>                
                        </div>
                    </div>
                </div>
            </div>  
        </xsl:if>     
    </xsl:template>
    <xsl:template name="orbitalEnergies">
        <xsl:param name="orbitalEnergies"/>
        <xsl:if test="exists($orbitalEnergies)">
            <xsl:variable name="irrep" select="tokenize($orbitalEnergies/array[@dictRef='cc:irrep'], '\s+')"/>
            <xsl:variable name="serial" select="tokenize($orbitalEnergies/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="occup" select="tokenize($orbitalEnergies/array[@dictRef='cc:occup'], '\s+')"/>
            <xsl:variable name="energy" select="tokenize($orbitalEnergies/array[@dictRef='cc:energy'], '\s+')"/>            
            <h4>Orbital Energies, all Irreps</h4>            
            <table id="orbitalEnergiesT-{generate-id($orbitalEnergies)}"></table>
            <script type="text/javascript">
                $(document).ready(function() {                        
                $('table#orbitalEnergiesT-<xsl:value-of select="generate-id($orbitalEnergies)"/>').dataTable( {
                "order": [[ 3, "asc" ]],
                "aaData": [                                                                    
                <xsl:for-each select="1 to count($irrep)">
                    <xsl:variable name="innerIndex" select="."/>                                                          
                    [ "<xsl:value-of select="$irrep[$innerIndex]"/>",
                       <xsl:value-of select="$serial[$innerIndex]"/>,
                       <xsl:value-of select="$occup[$innerIndex]"/>,
                       <xsl:value-of select="$energy[$innerIndex]"/>]
                       <xsl:if test="($innerIndex &lt; count($irrep))"><xsl:text>,</xsl:text></xsl:if>                   
                </xsl:for-each>                                                   
                ],
                "aoColumns": [
                { "sTitle": "Irrep" },
                { "sTitle": "no"},
                { "sTitle": "Occup"},
                { "sTitle": "E (eV)"  }
                ],
                "aoColumnDefs" : [                                    
                { "sClass": "text-right", "aTargets": [ 3 ] }
                ],
                "bFilter": false,                                 
                "bPaginate": true,
                "bInfo": true
                } );   
                } );                                       
            </script>            
        </xsl:if>                
    </xsl:template>
    <xsl:template name="orbitalEnergiesSpin">
        <xsl:param name="orbitalEnergiesSpin"/>     
        <xsl:if test="exists($orbitalEnergiesSpin)">
            <xsl:variable name="irrep" select="tokenize($orbitalEnergiesSpin/array[@dictRef='cc:irrep'], '\s+')"/>
            <xsl:variable name="serial" select="tokenize($orbitalEnergiesSpin/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="spin" select="tokenize($orbitalEnergiesSpin/array[@dictRef='cc:spin'], '\s+')"/>
            <xsl:variable name="occup" select="tokenize($orbitalEnergiesSpin/array[@dictRef='cc:occup'], '\s+')"/>
            <xsl:variable name="energy" select="tokenize($orbitalEnergiesSpin/array[@dictRef='cc:energy'], '\s+')"/>            
            <span>
                <h4>Orbital Energies, all Irreps, both Spins</h4>            
                <table id="orbitalEnergiesSpinT-{generate-id($orbitalEnergiesSpin)}"></table>
                <script type="text/javascript">
                    $(document).ready(function() {                        
                    $('table#orbitalEnergiesSpinT-<xsl:value-of select="generate-id($orbitalEnergiesSpin)"/>').dataTable( {
                    "order": [[ 4, "asc" ]],
                    "aaData": [                                                                    
                    <xsl:for-each select="1 to count($irrep)">
                        <xsl:variable name="innerIndex" select="."/>                                                          
                        [ "<xsl:value-of select="$irrep[$innerIndex]"/>",
                           <xsl:value-of select="$serial[$innerIndex]"/>,
                          "<xsl:value-of select="$spin[$innerIndex]"/>",
                           <xsl:value-of select="$occup[$innerIndex]"/>,
                           <xsl:value-of select="$energy[$innerIndex]"/>]
                        <xsl:if test="($innerIndex &lt; count($irrep))"><xsl:text>,</xsl:text></xsl:if>                   
                    </xsl:for-each>                                                   
                    ],
                    "aoColumns": [
                        { "sTitle": "Irrep" },
                        { "sTitle": "no"},
                        { "sTitle": "(spin)"},
                        { "sTitle": "Occup"},
                        { "sTitle": "E (eV)"}
                    ],
                    "aoColumnDefs" : [                                    
                        { "sClass": "text-right", "aTargets": [ 4 ] }
                    ],
                    "bFilter": false,                                 
                    "bPaginate": true,
                    "bInfo": true
                    } );   
                    } );                                       
                </script>
            </span>
        </xsl:if> 
    </xsl:template>
    
    <!-- Quadrupole -->
    <xsl:template name="quadrupole">
        <xsl:variable name="quadrupoleArray" select="(.//module[@id='finalization']//module[@cmlx:templateRef='quadrupole.moment']/array[@dictRef='cc:quadrupole'])[last()]"/>        
        <xsl:if test="exists($quadrupoleArray)">
            <xsl:variable name="quadrupole" select="tokenize($quadrupoleArray,'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#quadrupole-{generate-id($quadrupoleArray)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Quadrupole Moment (Buckingham convention)                        
                    </h4>
                </div>
                <div id="quadrupole-{generate-id($quadrupoleArray)}" class="panel-collapse collapse">
                    <div class="panel-body">                          
                        <div class="row bottom-buffer">
                            <div class="col-md-4 col-sm-12">                              
                                <table id="quadrupoleT-{generate-id($quadrupoleArray)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>quad-xx</td>
                                            <td><xsl:value-of select="$quadrupole[1]"/></td>
                                        </tr>
                                        <tr>
                                            <td>quad-xy</td>
                                            <td><xsl:value-of select="$quadrupole[2]"/></td>
                                        </tr>
                                        <tr>
                                            <td>quad-xz</td>
                                            <td><xsl:value-of select="$quadrupole[3]"/></td>
                                        </tr>
                                        <tr>
                                            <td>quad-yy</td>
                                            <td><xsl:value-of select="$quadrupole[4]"/></td>
                                        </tr>
                                        <tr>
                                            <td>quad-yz</td>
                                            <td><xsl:value-of select="$quadrupole[5]"/></td>
                                        </tr>
                                        <tr>
                                            <td>quad-zz</td>
                                            <td><xsl:value-of select="$quadrupole[6]"/></td>
                                        </tr>                                                       
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#quadrupoleT-<xsl:value-of select="generate-id($quadrupoleArray)"/>').dataTable( {
                                    "bFilter": false,                                 
                                    "bPaginate": false,                                    
                                    "bSort": false,
                                    "bInfo": false, 
                                    "aoColumnDefs" : [                                    
                                    { "sClass": "text-right", "aTargets": [ 1 ] }
                                    ],
                                    } );   
                                    } );                                       
                                </script>   
                            </div>
                        </div>
                    </div>
                </div>                       
            </div>
        </xsl:if>       
    </xsl:template>
    
    <!-- Timing -->
    <xsl:template name="timing">
        <xsl:variable name="cputime" select=".//module[@id='finalization']/propertyList/property[@dictRef='cc:cputime']/scalar"/>
        <xsl:variable name="systemtime" select=".//module[@id='finalization']/propertyList/property[@dictRef='cc:systemtime']/scalar"/>
        <xsl:variable name="elapsedtime" select=".//module[@id='finalization']/propertyList/property[@dictRef='cc:elapsedtime']/scalar"/>                
        <xsl:if test="exists($cputime)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#timing-{generate-id($cputime)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Timing                        
                    </h4>
                </div>
                <div id="timing-{generate-id($cputime)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6"> 
                                <table id="timingT-{generate-id($cputime)}">
                                    <thead>
                                        <tr>
                                            <th>Factor</th>
                                            <th> </th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Cpu</td>
                                            <td><xsl:value-of select="$cputime"/></td>                        
                                        </tr>                         
                                        <tr>
                                            <td>System</td>
                                            <td><xsl:value-of select="$systemtime"/></td>                        
                                        </tr>                                             
                                        <tr>
                                            <td>Elapsed</td>
                                            <td><xsl:value-of select="$elapsedtime"/></td>                        
                                        </tr>                                             
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#timingT-<xsl:value-of select="generate-id($cputime)"/>').dataTable( {
                                    "bFilter": false,                                 
                                    "bPaginate": false,                                    
                                    "bSort": false,
                                    "bInfo": false,
                                    "aoColumnDefs" : [                                    
                                        { "sClass": "text-right", "aTargets": [ 1 ] }
                                    ]
                                    } );   
                                    } );                                       
                                </script>  
                            </div>
                        </div>
                    </div>
                </div>
            </div>                                  
        </xsl:if>                
    </xsl:template>
    
    <!-- Excitations -->
    <xsl:template name="excitations">
        <xsl:variable name="excitations" select=".//module[@id='finalization']//module[@cmlx:templateRef='excitation.energy']"/>
        <xsl:if test="exists($excitations)">
            <xsl:variable name="excitationEnergies" select="$excitations/module[@cmlx:templateRef='excitationEnergies']"/>             
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#excitations-{generate-id($excitations)}" style="cursor: pointer; cursor: hand;">                    
                    <h4 class="panel-title">
                        Final Excitation Energies                        
                    </h4>
                </div>
                <div id="excitations-{generate-id($excitations)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        
                        <xsl:variable name="bandwidthev" select="0.15"/>
                        <xsl:variable name="bandwidthcm1" select="1200"/>
                        <xsl:variable name="bandwidthnm" select="20"/>                        
                        <script type="text/javascript">                                    
    
                            var evBandwidth = <xsl:value-of select="$bandwidthev"/>;
                            var cm1Bandwidth   = <xsl:value-of select="$bandwidthcm1"/>;
                            var nmBandwidth = <xsl:value-of select="$bandwidthnm"/>;
                            
                            var evStep = evBandwidth / 30;
                            var cm1Step = cm1Bandwidth / 30;
                            var nmStep = nmBandwidth / 30;
                            
                            var oscStrengths = new Array();                          
                            var energyEv     = new Array();                     
                            var energyCm1    = new Array();
                            var energyNm     = new Array();
                            
                            var energyMaxEv = new Array();                 
                            var energyMinEv = new Array();                
                            var energyMaxCm1 = new Array();          
                            var energyMinCm1 = new Array();
                            var energyMaxNm = new Array();   
                            var energyMinNm = new Array();   
                            
                            function round(num){
                                return Math.round(num * 100) / 100                                    
                            }
                            
                            function excitationParametersChanged(plotId){
                                $("excitation-generateBtn-" + "excitationPlotId").prop('disabled', false);
                                setExcitationGraphLimitValues(plotId);                                                                   
                                generateExcitationChart(plotId);                       
                            }
                            
                            function setExcitationGraphLimitValues(plotId){
                                value = $("input[name='excitationGraphUnits-" + plotId + "']:checked").val();
                                if(value == "ev"){                        		     
                                    $("#maxXEnergy-" + plotId).val(energyMaxEv[plotId]);
                                    $("#minXEnergy-" + plotId).val(energyMinEv[plotId]);      
                                    $("#bandwidth-" + plotId).val(evBandwidth);            
                                }
                                if(value == "cm1"){     
                                    $("#maxXEnergy-" + plotId).val(energyMaxCm1[plotId]);
                                    $("#minXEnergy-" + plotId).val(energyMinCm1[plotId]);
                                    $("#bandwidth-" + plotId).val(cm1Bandwidth);
                                }
                                if(value == "nm"){
                                   $("#maxXEnergy-" + plotId).val(energyMaxNm[plotId]);
                                   $("#minXEnergy-" + plotId).val(energyMinNm[plotId]);
                                   $("#bandwidth-" + plotId).val(nmBandwidth);
                                }
                            }
                            
                            function generateExcitationChart(plotId){
                                $('#excitationEnergyPlotlyContainer-' + plotId).show();
                                var data = fillExcitationChart(plotId);                                    
                                var xLabel = "";                                        
                                unitType = $("input[name='excitationGraphUnits-" + plotId + "']:checked").val();
                                if(unitType == "ev")                        		              
                                    xLabel = 'E (eV)';
                                else if(unitType == "cm1")
                                    xLabel = 'E (cm-1)';
                                else if(unitType == "nm")
                                    xLabel = "nm";
                            
                                var layout = {
                                    height: 550,
                                    xaxis: {
                                        title: xLabel,
                                        showgrid: true,
                                        zeroline: true,
                                        tickmode: 'auto'
                                    },
                                    hovermode: 'closest',
                                    yaxis: {
                                        title: 'Oscillator strengths (a.u.)'
                                    }
                                };                                                
                                Plotly.react('excitationEnergyPlotlyContainer-' + plotId, data, layout,  {responsive: true, showSendToCloud: true});
                            }
                            
                            function fillExcitationChart(plotId){
                                //Clear possible previous values                                        
                                var data = new Array();
                                var dataSum = new Array();
                                
                                //Now we'll generate individual energy and all-energy-sum series and append them in data and dataSum arrays
                                var bandWidth = $("#bandwidth-" + plotId).val();
                                var minX = parseFloat($("#minXEnergy-" + plotId).val());
                                var maxX = parseFloat($("#maxXEnergy-" + plotId).val());
                                
                                unitType = $("input[name='excitationGraphUnits-" + plotId + "']:checked").val();                                       
                                for(var inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> oscStrengths[plotId].length; inx++){
                                    generateGaussianValuesForPeak(plotId, data,dataSum,  inx+1, unitType, bandWidth, minX, maxX);
                                }                                        
                                
                                var xVal = new Array();
                                var yVal = new Array();
                                var sum = 0;
                                for(var inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> dataSum.length; inx++){
                                    xVal.push(dataSum[inx][0]);
                                    yVal.push(dataSum[inx][1]);
                                    sum += dataSum[inx][1];
                                }
                                
                                //Check if there is representable data, if not (no osc.strength values) we won't show the graph                                        
                                if(sum == 0){
                                    $('#excitationEnergyPlotlyContainer-' + plotId).hide();
                                    $('#excitationEnergyContainerControls-' + plotId).hide();
                                    return;
                                }
                                
                                //Add sum serie and plot graph
                                var serie = {
                                    x: xVal,
                                    y: yVal,
                                    mode: 'lines',
                                    name: 'Total sum',
                                    marker: {                                    
                                            color: 'rgb(57, 57, 69)'
                                    },
                                    line: {
                                            color: 'rgb(57, 57, 69)'
                                    }                                    
                                };                                                           
                                data.push(serie);
                                
                                // Add bars
                                var energyArray;
                                switch(unitType){
                                    case 'ev':  energyArray = energyEv[plotId];
                                                break;
                                    case 'cm1': energyArray = energyCm1[plotId];
                                                break;
                                    case 'nm':  energyArray = energyNm[plotId];
                                                break;         
                                }      
                                
                                var serieBars =  {
                                    x : energyArray,
                                    y : oscStrengths[plotId],
                                    type: 'bar',
                                    mode: 'markers',                                    
                                    name: 'Intensities',
                                    marker: {                                    
                                             color: 'rgb(224, 97, 97)'
                                            },
                                    line: {
                                             color: 'rgb(224, 97, 97)'
                                          }
                                }
                                data.push(serieBars);
                                return data;
                            }
                            
                            function generateGaussianValuesForPeak(plotId, data, dataSum, energyInx, units, bandWidth, minX, maxX) {
                                var step;
                                var energyValue;                                
                                switch(units){
                                    case 'ev':  step = evStep;
                                                energyValue = energyEv[plotId][energyInx-1];
                                                break;
                                    case 'cm1': step = cm1Step;
                                                energyValue = energyCm1[plotId][energyInx-1];
                                                break;
                                    case 'nm':  step = nmStep;
                                                energyValue = energyNm[plotId][energyInx-1];
                                                break;         
                                }      
                                
                                var inx = 0;
                                var xVal = new Array();
                                var yVal = new Array();
                                for(var x = minX; x <xsl:text disable-output-escaping="yes">&lt;</xsl:text> maxX ; x = x + step ) {  
                                    if(dataSum.length <xsl:text disable-output-escaping="yes">&lt;</xsl:text>= inx)     
                                        dataSum.push([x,oscStrengths[plotId][energyInx-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - energyValue),2))]);
                                    else
                                        dataSum[inx][1] += oscStrengths[plotId][energyInx-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - energyValue),2))
                                    
                                    xVal.push(x);
                                    yVal.push(oscStrengths[plotId][energyInx-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x -energyValue),2)));
                                    inx++;
                                }
                                
                                // Individual series are disabled right now
                                /*
                                var serie = {
                                    x: xVal,
                                    y: yVal,
                                    mode: 'lines',
                                    name: 'Energy ' + energyInx,
                                };                                                           
                                data.push(serie); 
                                */
                                
                            }
                        </script>                                                
                        
                        <xsl:for-each select="1 to count($excitationEnergies)">
                            <xsl:variable name="outerIndex" select="."/>
                            <xsl:variable name="energies" select="$excitationEnergies[$outerIndex]/module[@cmlx:templateRef='energies']"/>                            
                            <xsl:variable name="dipole"   select="$excitationEnergies[$outerIndex]/module[@cmlx:templateRef='dipole']"/>
                            <xsl:variable name="sym" select="$excitationEnergies[$outerIndex]/scalar[@dictRef='cc:symm']"></xsl:variable>
                            <!-- Print header -->
                           <div class="row">
                               <div class="col-md-12">
                                   <strong>Symmetry <xsl:value-of select="$sym"/></strong>        
                               </div>
                           </div>
                            <!-- Now energies and dipole on two different tables -->
                           <div class="row">
                               <div class="col-md-6 col-sm-12">                
                                   <xsl:variable name="energiesIndex" select="tokenize($energies/array[@dictRef='cc:serial'],'\s+')"></xsl:variable>
                                   <xsl:variable name="energy" select="tokenize($energies/array[@dictRef='cc:energy'],'\s+')"></xsl:variable>
                                   <xsl:variable name="oscillator" select="tokenize($energies/array[@dictRef='cc:oscillator'],'\s+')"></xsl:variable>
                                   <xsl:variable name="energyDiff" select="tokenize($energies/array[@dictRef='cc:energyDiff'],'\s+')"></xsl:variable>
                                   <span> Excitation energies E in a.u. , dE wrt prev. cycle,oscillator strengths f in a.u.</span>
                                   <table id="excitationEnergies-{generate-id($energies)}"></table>
                                   <script type="text/javascript">
                                       $(document).ready(function() {                        
                                            $("table#excitationEnergies-<xsl:value-of select="generate-id($energies)"/>").dataTable( { 
                                                "aaData" : [
                                                <xsl:for-each select="1 to count($energiesIndex)"> 
                                                    <xsl:variable name="innerIndex" select="."/>
                                                    [<xsl:value-of select="$energiesIndex[$innerIndex]"/>,'<xsl:value-of select="$energy[$innerIndex]"/>','<xsl:value-of select="$oscillator[$innerIndex]"/>','<xsl:value-of select="$energyDiff[$innerIndex]"/>']<xsl:if test="$innerIndex &lt; count($energiesIndex)"><xsl:text>,</xsl:text></xsl:if>                                                   
                                                </xsl:for-each>                                                                
                                                ],
                                                "aoColumns" : [
                                                    {"sTitle" : "no."},
                                                    {"sTitle" : "E/a.u."},
                                                    {"sTitle" : "f"},
                                                    {"sTitle" : "dE/a.u."}                                                    
                                                ],                                              
                                                "bFilter": false,                                 
                                                "bPaginate": false,                                    
                                                "bSort": false,
                                                "bInfo": false
                                             } );   
                                       } );                                       
                                   </script>
                               </div>
                               <div class="col-md-6 col-sm-12">
                                   <xsl:variable name="energiesIndex" select="tokenize($dipole/array[@dictRef='cc:serial'],'\s+')"></xsl:variable>
                                   <xsl:variable name="energy" select="tokenize($dipole/array[@dictRef='cc:energy'],'\s+')"></xsl:variable>
                                   <xsl:variable name="oscillator" select="tokenize($dipole/array[@dictRef='cc:oscillator' or @dictRef='cc:energyDiff'],'\s+')"></xsl:variable> <!-- Bug fix -->
                                   <xsl:variable name="muX" select="tokenize($dipole/array[@dictRef='cc:muX'],'\s+')"></xsl:variable>
                                   <xsl:variable name="muY" select="tokenize($dipole/array[@dictRef='cc:muY'],'\s+')"></xsl:variable>
                                   <xsl:variable name="muZ" select="tokenize($dipole/array[@dictRef='cc:muZ'],'\s+')"></xsl:variable>
                                   <span> Transition dipole moments mu (x,y,z) in a.u. (weak excitations are not printed)</span>                                   
                                   <table id="excitationDipole-{generate-id($dipole)}"></table>
                                   <script type="text/javascript">
                                       $(document).ready(function() {                        
                                       $("table#excitationDipole-<xsl:value-of select="generate-id($dipole)"/>").dataTable( { 
                                       "aaData" : [
                                       <xsl:for-each select="1 to count($energiesIndex)"> 
                                           <xsl:variable name="innerIndex" select="."/>
                                           [<xsl:value-of select="$energiesIndex[$innerIndex]"/>,
                                           '<xsl:value-of select="$energy[$innerIndex]"/>',
                                           '<xsl:value-of select="$oscillator[$innerIndex]"/>',
                                           '<xsl:value-of select="$muX[$innerIndex]"/>',
                                           '<xsl:value-of select="$muY[$innerIndex]"/>',
                                           '<xsl:value-of select="$muZ[$innerIndex]"/>']
                                           <xsl:if test="$innerIndex &lt; count($energiesIndex)"><xsl:text>,</xsl:text></xsl:if>                                                   
                                       </xsl:for-each>                                                                
                                       ],
                                       "aoColumns" : [
                                       {"sTitle" : "no."},
                                       {"sTitle" : "E/eV"},
                                       {"sTitle" : "f"},
                                       {"sTitle" : "X"},
                                       {"sTitle" : "Y"},
                                       {"sTitle" : "Z"}
                                       ],                                              
                                       "bFilter": false,                                 
                                       "bPaginate": false,                                    
                                       "bSort": false,
                                       "bInfo": false
                                       } );   
                                       } );                                       
                                   </script>
                               </div>
                           </div> 
                           

                           <xsl:variable name="excitationPlotId" select="generate-id($excitationEnergies[$outerIndex])" />
                            <div class="col-md-12">
                                <div id="excitationEnergyPlotlyContainer-{$excitationPlotId}" style="min-height:550px; width: 100%; display:none !important" class=" d-flex"/>
                            </div>                            
                            <xsl:variable name="energy" select="tokenize($energies/array[@dictRef='cc:energy'],'\s+')"></xsl:variable>
                            <xsl:variable name="oscillator" select="tokenize($energies/array[@dictRef='cc:oscillator'],'\s+')"></xsl:variable>
                            
                            <div class="col-sm-12 col-md-12">
                                <div id="excitationEnergyContainerControls-{$excitationPlotId}" class="mt-2 mb-5">                                
                                    <form>
                                        <div class="form-row align-items-center">
                                            <div class="col-auto">                                                
                                                <div class="form-group form-check-inline mt-4 mr-5">
                                                    <div id="excitationGraphUnits-{$excitationPlotId}" class="form-check">
                                                        <input class="form-check-input" type="radio" id="excitationGraphUnits-{$excitationPlotId}-ev" name="excitationGraphUnits-{$excitationPlotId}" value="ev" onclick="excitationParametersChanged('{$excitationPlotId}')" />
                                                        <label class="form-check-label" for="excitationGraphUnits-{$excitationPlotId}-ev">eV</label>                                                                        
                                                    </div>    
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" id="excitationGraphUnits-{$excitationPlotId}-cm1" name="excitationGraphUnits-{$excitationPlotId}" value="cm1" onclick="excitationParametersChanged('{$excitationPlotId}')" />
                                                        <label class="form-check-label" for="excitationGraphUnits-{$excitationPlotId}-cm1">cm-1</label>                                                                        
                                                    </div>
                                                    <div class="form-check">
                                                        <input class="form-check-input" type="radio" id="excitationGraphUnits-{$excitationPlotId}-nm" name="excitationGraphUnits-{$excitationPlotId}" value="nm" onclick="excitationParametersChanged('{$excitationPlotId}')" />
                                                        <label class="form-check-label" for="excitationGraphUnits-{$excitationPlotId}-nm">nm</label>                                                                        
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <label for="bandwidth-{$excitationPlotId}">Bandwidth</label>
                                                    <input type="text" class="form-control" id="bandwidth-{$excitationPlotId}" value="0.15"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <label for="minXEnergy-{$excitationPlotId}">min X</label>
                                                    <input type="text" class="form-control" id="minXEnergy-{$excitationPlotId}" value="0"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group">
                                                    <label for="maxXEnergy-{$excitationPlotId}">max X</label>
                                                    <input type="text" class="form-control" id="maxXEnergy-{$excitationPlotId}" value="10"/>
                                                </div>
                                            </div>
                                            <div class="col-auto">
                                                <div class="form-group mt-4">
                                                    <button type="button" class="btn btn-secondary" disabled="disabled" id="excitation-generateBtn-{$excitationPlotId}" onclick="generateExcitationChart('{$excitationPlotId}')">Plot!</button>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>                           
                            
                            <xsl:variable name="oscStrengths" as="xs:float*">                               
                                <xsl:for-each select="$oscillator"><xsl:element name="value"><xsl:value-of select="."/></xsl:element></xsl:for-each>
                            </xsl:variable>
                            <xsl:variable name="energyEv" as="xs:float*">                               
                                <xsl:for-each select="$energy"><xsl:element name="value"><xsl:value-of select="(number(.)*$AU_TO_EV)"/></xsl:element></xsl:for-each>                                                   
                            </xsl:variable>
                            <xsl:variable name="energyCm1" as="xs:float*">                             
                                <xsl:for-each select="$energy"><xsl:element name="value"><xsl:value-of select="(number(.)*$AU_TO_EV) * $EV_TO_CM_1"/></xsl:element></xsl:for-each>
                            </xsl:variable>
                            <xsl:variable name="energyNm" as="xs:float*">                              
                                <xsl:for-each select="$energy"><xsl:element name="value"><xsl:value-of select="format-number($EV_TO_NM div (number(.) * $AU_TO_EV),'#0.000')"/></xsl:element></xsl:for-each>
                            </xsl:variable>
                            <script type="text/javascript">                                                                  
                                oscStrengths['<xsl:value-of select="$excitationPlotId"/>'] = [<xsl:value-of select="helper:toTextArray($oscStrengths)"/>];                        
                                energyEv['<xsl:value-of select="$excitationPlotId"/>'] = [<xsl:value-of select="helper:toTextArray($energyEv)"/>];                     
                                energyCm1['<xsl:value-of select="$excitationPlotId"/>'] = [<xsl:value-of select="helper:toTextArray($energyCm1)"/>];
                                energyNm['<xsl:value-of select="$excitationPlotId"/>'] = [<xsl:value-of select="helper:toTextArray($energyNm)"/>];
                                
                                <xsl:variable name="energyMax" select="number(format-number( round(10*max($energyEv)) div 10 ,'##0.0'))"/>
                                <xsl:variable name="energyMin" select="number(format-number( round(10*min($energyEv)) div 10 ,'##0.0'))"/>
                                
                                energyMaxEv['<xsl:value-of select="$excitationPlotId"/>'] = round(<xsl:value-of select="$energyMax"/> + evBandwidth);                 
                                energyMinEv['<xsl:value-of select="$excitationPlotId"/>'] = round(<xsl:value-of select="$energyMin"/> - evBandwidth);                
                                energyMaxCm1['<xsl:value-of select="$excitationPlotId"/>'] = round(<xsl:value-of select="$energyMax * $EV_TO_CM_1"/> + cm1Bandwidth);          
                                energyMinCm1['<xsl:value-of select="$excitationPlotId"/>'] = round(<xsl:value-of select="$energyMin * $EV_TO_CM_1"/> - cm1Bandwidth);
                                energyMaxNm['<xsl:value-of select="$excitationPlotId"/>'] = round(<xsl:value-of select="$EV_TO_NM div $energyMin"/> + nmBandwidth);   
                                energyMinNm['<xsl:value-of select="$excitationPlotId"/>'] = round(<xsl:value-of select="$EV_TO_NM div $energyMax"/> - nmBandwidth);
                            </script>                                                
                        </xsl:for-each>
                    </div>
                </div>
            </div>
        </xsl:if>        
    </xsl:template>
    
    <!-- Rotatory strengths -->
    <xsl:template name="rotatorystrengths">
        <xsl:variable name="excitations" select=".//module[@id='finalization']//module[@cmlx:templateRef='excitation.energy']"/>
        <xsl:if test="exists($excitations//module[@cmlx:templateRef='rotatory'])">
            <xsl:variable name="excitationEnergies" select="$excitations/module[@cmlx:templateRef='excitationEnergies']"/>             
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#excitations-{generate-id($excitations)}-rotatory" style="cursor: pointer; cursor: hand;">                    
                    <h4 class="panel-title">
                        Rotatory strengths
                    </h4>
                </div>
                <div id="excitations-{generate-id($excitations)}-rotatory" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <xsl:for-each select="1 to count($excitationEnergies)">
                            <xsl:variable name="outerIndex" select="."/>
                            <xsl:variable name="energies"       select="$excitationEnergies[$outerIndex]/module[@cmlx:templateRef='energies']"/>                            
                            <xsl:variable name="rotstrengths"   select="$excitationEnergies[$outerIndex]/module[@cmlx:templateRef='rotatory']"/>
                            <xsl:variable name="sym" select="$excitationEnergies[$outerIndex]/scalar[@dictRef='cc:symm']"></xsl:variable>
                            <!-- Print header -->
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>Symmetry <xsl:value-of select="$sym"/></strong>        
                                </div>
                            </div>
                            <!-- Now energies and rotatory strengths on two different tables -->
                            <div class="row">
                                <div class="col-md-6 col-sm-12">                
                                    <xsl:variable name="energiesIndex" select="tokenize($energies/array[@dictRef='cc:serial'],'\s+')"></xsl:variable>
                                    <xsl:variable name="energy" select="tokenize($energies/array[@dictRef='cc:energy'],'\s+')"></xsl:variable>
                                    <xsl:variable name="oscillator" select="tokenize($energies/array[@dictRef='cc:oscillator'],'\s+')"></xsl:variable>
                                    <xsl:variable name="energyDiff" select="tokenize($energies/array[@dictRef='cc:energyDiff'],'\s+')"></xsl:variable>
                                    <span> Excitation energies E in a.u. , dE wrt prev. cycle,oscillator strengths f in a.u.</span><br/><br/>
                                    <table id="excitationEnergies-{generate-id($energies)}-rotatory"></table>
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                        $("table#excitationEnergies-<xsl:value-of select="generate-id($energies)"/>-rotatory").dataTable( { 
                                        "aaData" : [
                                        <xsl:for-each select="1 to count($energiesIndex)"> 
                                            <xsl:variable name="innerIndex" select="."/>
                                            [<xsl:value-of select="$energiesIndex[$innerIndex]"/>,'<xsl:value-of select="$energy[$innerIndex]"/>','<xsl:value-of select="$oscillator[$innerIndex]"/>','<xsl:value-of select="$energyDiff[$innerIndex]"/>']<xsl:if test="$innerIndex &lt; count($energiesIndex)"><xsl:text>,</xsl:text></xsl:if>                                                   
                                        </xsl:for-each>                                                                
                                        ],
                                        "aoColumns" : [
                                        {"sTitle" : "no."},
                                        {"sTitle" : "E/a.u."},
                                        {"sTitle" : "f"},
                                        {"sTitle" : "dE/a.u."}                                                    
                                        ],                                              
                                        "bFilter": false,                                 
                                        "bPaginate": false,                                    
                                        "bSort": false,
                                        "bInfo": false
                                        } );   
                                        } );                                       
                                    </script>
                                </div>
                                <div class="col-md-6 col-sm-12">
                                    <xsl:variable name="rotatoryIndex" select="tokenize($rotstrengths/array[@dictRef='cc:serial'],'\s+')"></xsl:variable>
                                    <xsl:variable name="strengths" select="tokenize($rotstrengths/array[@dictRef='a:strengths'],'\s+')"></xsl:variable>
                                    <xsl:variable name="mX" select="tokenize($rotstrengths/array[@dictRef='cc:mX'],'\s+')"></xsl:variable>
                                    <xsl:variable name="mY" select="tokenize($rotstrengths/array[@dictRef='cc:mY'],'\s+')"></xsl:variable>
                                    <xsl:variable name="mZ" select="tokenize($rotstrengths/array[@dictRef='cc:mZ'],'\s+')"></xsl:variable>
                                    <span>  Rotatory strengths R in 10**(-40) esu**2 * cm**2 , (multiply by 1.07827 to obtain reduced rotatory strengths), magnetic transition dipole vectors m in a.u.:</span>                                   
                                    <table id="excitationRotationStregths-{generate-id($rotstrengths)}"></table>
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                        $("table#excitationRotationStregths-<xsl:value-of select="generate-id($rotstrengths)"/>").dataTable( { 
                                        "aaData" : [
                                        <xsl:for-each select="1 to count($rotatoryIndex)"> 
                                            <xsl:variable name="innerIndex" select="."/>
                                            [<xsl:value-of select="$rotatoryIndex[$innerIndex]"/>,
                                            '<xsl:value-of select="$strengths[$innerIndex]"/>',                                            
                                            '<xsl:value-of select="$mX[$innerIndex]"/>',
                                            '<xsl:value-of select="$mY[$innerIndex]"/>',
                                            '<xsl:value-of select="$mZ[$innerIndex]"/>']
                                            <xsl:if test="$innerIndex &lt; count($rotatoryIndex)"><xsl:text>,</xsl:text></xsl:if>                                                   
                                        </xsl:for-each>                                                                
                                        ],
                                        "aoColumns" : [
                                        {"sTitle" : "no."},
                                        {"sTitle" : "R"},
                                        {"sTitle" : "X"},
                                        {"sTitle" : "Y"},
                                        {"sTitle" : "Z"}
                                        ],                                              
                                        "bFilter": false,                                 
                                        "bPaginate": false,                                    
                                        "bSort": false,
                                        "bInfo": false
                                        } );   
                                        } );                                       
                                    </script>
                                </div>
                            </div>                                                       
                        </xsl:for-each>
                    </div>
                </div>
            </div>
        </xsl:if>        
    </xsl:template>
    
    <xsl:function name="helper:toTextArray">
        <xsl:param name="sequence" as="xs:float*"/>
        <xsl:for-each select="1 to count($sequence)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:value-of select="$sequence[$outerIndex]"/>
            <xsl:if test="$outerIndex &lt; count($sequence)">, </xsl:if>
        </xsl:for-each>        
    </xsl:function>
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
        
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
	
</xsl:stylesheet>

