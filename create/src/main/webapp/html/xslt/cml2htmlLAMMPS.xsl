<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:l="http://www.iochem-bd.org/dictionary/lammps/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk l helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> May 02, 2022</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>       
    </xd:doc> 
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:include href="helper/chemistry/lammps.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>
    <xsl:param name="xyzurl"/>

    <!-- Environment module -->
    <xsl:variable name="environment" select="//cml:module[@id='job'][1]/cml:module[@id='environment']/cml:parameterList"/>
    <xsl:variable name="programParameter" select="$environment/cml:parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="$environment/cml:parameter[@dictRef='cc:programVersion']"/>
        
    <!-- Geometry -->
    <xsl:variable name="molecule" select="(//cml:molecule)[last()]"/>    
    
    <!-- Initialization -->    
    <xsl:variable name="initialization" select="//cml:module[@dictRef='cc:initialization']"/>
    <xsl:variable name="setup" select="if(exists($initialization/cml:parameterList[count(*) &gt; 0])) then 
                                            $initialization/cml:parameterList
                                        else
                                            $initialization/cml:module[@id='otherComponents']/cml:module[@id='input']/cml:parameterList" />
                                        
    <xsl:variable name="method" select="l:getMethod($setup)" />
    <xsl:variable name="units" select="$setup/cml:parameter/cml:scalar[@dictRef='x:label'][text()='units']/following-sibling::cml:scalar[@dictRef='x:value']/text()"/>
    
    <!-- Calculation -->
    <xsl:variable name="calculation" select="//cml:module[@dictRef='cc:calculation']"/>
    
    <!-- Finalization -->
    <xsl:variable name="finalization" select="//cml:module[@dictRef='cc:finalization']/cml:propertyList"/>               
    
    <xsl:template match="/">        
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Settings -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>SETTINGS</h3>                                                                                                                                               
                            <div>                                         
                                <xsl:call-template name="settings"/>                                
                            </div>
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                                                                                                               
                            <div>                                
                                <xsl:apply-templates select="$molecule"/>                                
                            </div>
                        </div>
                    </div>
                    <!-- Results -->
                    <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                        <div id="module-{generate-id(.)}">                                
                            <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                            <div class="row bottom-buffer">                     
                                <div class="col-md-12">                                
                                    <xsl:call-template name="energies"/>
                                    <xsl:call-template name="molecularDynamics" />
                                    <xsl:call-template name="timing"/>                                    
                                </div>
                            </div>
                        </div>
                    </xsl:for-each>
                    <xsl:call-template name="printLicense"/>
                    
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {    
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                        
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                            $.unblockUI();                             
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                               var oTable = $(this).dataTable();
                               $(oTable).css({ width: $(oTable).parent().width() });
                               oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
        </body>
    </html>
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinatesFractional" match="cml:molecule[cml:crystal]">                
        <xsl:variable name="collapseAccordion" select="if(count($molecule/atomArray/atom) > 1000) then '' else 'in'"/>
        <xsl:variable name="isLargeMolecule" select="count($molecule/atomArray/atom) > 1000"/>
        <xsl:variable name="boxCoords" select="//cml:module[@dictRef='cc:finalization']/cml:molecule/cml:crystal"/>
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                                      
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        
                        <xsl:if test="exists($boxCoords)">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 mb-4">
                                        <h5>Cell parameters:</h5>
                                        <table id="cellParameters" class="display">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>a = <xsl:value-of select="./crystal/scalar[@title='a']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>b = <xsl:value-of select="./crystal/scalar[@title='b']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>c = <xsl:value-of select="./crystal/scalar[@title='c']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>α = <xsl:value-of select="./crystal/scalar[@title='alpha']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>β = <xsl:value-of select="./crystal/scalar[@title='beta']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>γ = <xsl:value-of select="./crystal/scalar[@title='gamma']"/></td>
                                                </tr>
                                            </tbody>
                                        </table>    
                                    </div>
                                </div>
                            </div>
                        </xsl:if>

                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="coordinateTypeDiv">
                                <span>Coordinate type :</span>
                                <input type="radio" name="coordinateType" value="both" checked="checked" onclick="changeMoleculeColumnVisibility()" />Both
                                <input type="radio" name="coordinateType" value="cartesian" onclick="changeMoleculeColumnVisibility()"/>Cartesian
                                <input type="radio" name="coordinateType" value="fractional" onclick="changeMoleculeColumnVisibility()"/>Fractional
                            </div>
                            <div id="coordinatesXYZ-{generate-id($molecule)}" class="right">
                                <xsl:if test="$isLargeMolecule">
                                </xsl:if>
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>                             
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>                               
                            </table>
                                      
                                      
                            <table id="atomicCoordinates">
                                <thead>
                                    <tr>
                                        <th rowspan="2"/>
                                        <th rowspan="2"/>
                                        <th colspan="3" style="text-align:center">Cartesian coordinates</th>
                                        <th colspan="3" style="text-align:center">Fractional coordinates</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">x</th>
                                        <th style="text-align:right">y</th>
                                        <th style="text-align:right">z</th>
                                        <th style="text-align:right">u</th>
                                        <th style="text-align:right">v</th>
                                        <th style="text-align:right">w</th>
                                    </tr>
                                </thead>
                            </table>
                            <script type="text/javascript">                                
                                var coordinates = [<xsl:for-each select="$molecule/cml:atomArray/cml:atom"><xsl:variable name="outerIndex" select="position()"/>
                                    [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="@elementType"/>","<xsl:value-of select="format-number(@x3, '#0.0000')"/>","<xsl:value-of select="format-number(@y3, '#0.0000')"/>","<xsl:value-of select="format-number(@z3, '#0.0000')"/>","<xsl:value-of select="format-number(@xFract, '#0.0000')"/>","<xsl:value-of select="format-number(@yFract, '#0.0000')"/>","<xsl:value-of select="format-number(@zFract, '#0.0000')"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                    </xsl:for-each>];

                                $(document).ready(function() {
                                    buildXYZTable();
                                    buildMolecularInfoTable();
                                    <xsl:if test="not(exists($molecule/cml:atomArray/cml:atom/@xFract))">   
                                    viewCartesianCoordinatesOnly();
                                    </xsl:if> 
                                } );

                                function changeMoleculeColumnVisibility(){
                                    var mode = $('input[name=coordinateType]:checked').val();
                                    var dt = $('#atomicCoordinates').DataTable();
                                    if( mode == 'both'){
                                        dt.columns([2,3,4,5,6,7]).visible(true);
                                    }else if(mode == 'cartesian') {
                                        dt.columns([2,3,4]).visible(true); 
                                        dt.columns([5,6,7]).visible(false);
                                    }else {
                                        dt.columns([2,3,4]).visible(false); 
                                        dt.columns([5,6,7]).visible(true);
                                    }
                                    dt.columns.adjust().draw();
                                    // Adjust table width to its container
                                    dt = $('#atomicCoordinates').dataTable();
                                    $(dt).css({ width: $(dt).parent().width() });
                                    dt.fnAdjustColumnSizing();
                                }
                                
                                function buildMolecularInfoTable() {
                                    $('table#atomicCoordinates').dataTable( {
                                        "aaData": coordinates,
                                        "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" },
                                            { "sTitle": "u", "sClass": "right" },
                                            { "sTitle": "v", "sClass": "right" },
                                            { "sTitle": "w", "sClass": "right" }],
                                        "bFilter": false,
                                        "bPaginate": <xsl:value-of select="if($isLargeMolecule) then 'true' else 'false'" />,
                                        "bSort": false,
                                        "bInfo": false
                                    } );   
                                }  
                                
                                function buildXYZTable() {
                                    var coordinatesXYZ = coordinates.map((val) => val.slice(1,5));
                                    coordinatesXYZ.unshift(['<xsl:value-of select="$title"/>','','','']);
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "aaData" : coordinatesXYZ,
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "bDeferRender": true
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                }
                                
                                function getXYZ() {
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons(table, {
                                        <xsl:choose>
                                            <xsl:when test="$isLargeMolecule">
                                                buttons: [ {
                                                    extend: 'csv',
                                                    text: 'CSV'
                                                }]
                                            </xsl:when>
                                            <xsl:otherwise>
                                                buttons: [ {
                                                    extend: 'copyHtml5',
                                                    text: 'XYZ'
                                                }]
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    });
                                                                
                                    table.buttons().container().appendTo($('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));
                                    $('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                
                                <xsl:if test="$isLargeMolecule">
                                    $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").on('mousedown', function(){
                                        $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").addClass('processing');
                                    });
                                </xsl:if>     
                                }
                                
                                function viewCartesianCoordinatesOnly(){
                                    $('input[name=coordinateType ][value=cartesian]').prop('checked', true); 
                                    changeMoleculeColumnVisibility();
                                    $('#coordinateTypeDiv').hide()
                                }                                
                            </script>                              
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>

    <xsl:template name="atomicCoordinates" match="cml:molecule[not(cml:crystal)]">
        <xsl:variable name="boxCoords" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='averages']/cml:vector3[@dictRef='gm:box.coords'])[last()]"/>
        <xsl:variable name="collapseAccordion" select="if(count($molecule/atomArray/atom) > 1000) then '' else 'in'"/>
        <xsl:variable name="isLargeMolecule" select="count($molecule/atomArray/atom) > 1000"/>
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                                      
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <xsl:if test="exists($boxCoords)">
                            <xsl:variable name="coords" select="tokenize($boxCoords, '\s+')"/>
                            <div class="col-md-12">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <h5>Cell parameters:</h5>
                                    <table id="cellParameters" class="display">
                                        <thead>
                                            <tr>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>X = <xsl:value-of select="$coords[1]"/></td>
                                            </tr>
                                            <tr>
                                                <td>Y = <xsl:value-of select="$coords[2]"/></td>
                                            </tr>
                                            <tr>
                                                <td>Z = <xsl:value-of select="$coords[3]"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </xsl:if>
                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="coordinatesXYZ-{generate-id($molecule)}" class="right">
                                <xsl:if test="$isLargeMolecule">
                                </xsl:if>
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>                             
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>                               
                            </table>
                            
                            <table id="atomicCoordinates"></table>
                            
                            <script type="text/javascript">                                
                                var atomInfo = [<xsl:for-each select="$molecule/atomArray/atom">[<xsl:variable name="outerIndex" select="position()"/><xsl:variable name="elementType" select="@elementType"/><xsl:value-of select="$outerIndex"/>,'<xsl:value-of select="$elementType"/>',<xsl:value-of select="format-number(@x3,'#0.000000')"/>,<xsl:value-of select="format-number(@y3,'#0.000000')"/>,<xsl:value-of select="format-number(@z3,'#0.000000')"/>]<xsl:if test="$outerIndex&lt;count($molecule/atomArray/atom)">,</xsl:if></xsl:for-each>];
                                
                                $(document).ready(function() {      
                                    setupXYZTable();
                                    setupMolecularInfoTable();                                                                                                            
                                } );
                                
                                function setupXYZTable() {
                                    var coordinatesXYZ = atomInfo.map((val) => val.slice(1,5));
                                    coordinatesXYZ.unshift(['<xsl:value-of select="$title"/>','','','']); 
                                    
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "aaData" : coordinatesXYZ,
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "bDeferRender": true
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                }
                                
                                function setupMolecularInfoTable() {
                                    $('table#atomicCoordinates').dataTable( {
                                        "aaData": atomInfo,
                                        "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" }],                                
                                        "bFilter": false,
                                        "bPaginate": <xsl:value-of select="if($isLargeMolecule) then 'true' else 'false'" />,
                                        "bSort": false,
                                        "bInfo": false
                                    });   
                                }                               
                                
                                function getXYZ() {
                                var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                new $.fn.dataTable.Buttons(table, {
                                <xsl:choose>
                                    <xsl:when test="$isLargeMolecule">
                                        buttons: [ {
                                        extend: 'csv',
                                        text: 'CSV'
                                        }]
                                    </xsl:when>
                                    <xsl:otherwise>
                                        buttons: [ {
                                        extend: 'copyHtml5',
                                        text: 'XYZ'
                                        }]
                                    </xsl:otherwise>
                                </xsl:choose>
                                });
                                
                                table.buttons().container().appendTo($('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));
                                $('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                
                                <xsl:if test="$isLargeMolecule">
                                $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").on('mousedown', function(){
                                    $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").addClass('processing');
                                });
                                </xsl:if>     
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>          
            <tr>
                <td style="vertical-align: top;">Calculation type:</td>
                <td>Molecular Dynamics (<xsl:value-of select="$method"/>)             
                   <br/>
                    <xsl:for-each select="l:getCouplingMethods($setup)">
                        <xsl:value-of select="."/><br/>
                    </xsl:for-each>
                </td>                                
            </tr>          
        </table>        
    </xsl:template>
    
    <!-- Settings -->
    <xsl:template name="settings">
        <div class="row bottom-buffer">
            <div class="col-lg-6 col-md-8 col-sm-12">
                <table class="display" id="timeSettings">
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th>Value</th>                         
                        </tr>                        
                    </thead>
                    <tbody>                                               
                        <xsl:copy-of select="helper:printParameterRow('units')"/>
                        <xsl:copy-of select="helper:printParameterRow('boundary')"/>
                        <xsl:copy-of select="helper:printParameterRow('bond_style')"/>
                        <xsl:copy-of select="helper:printParameterRow('angle_style')"/>
                        <xsl:copy-of select="helper:printParameterRow('dihedral_style')"/>
                        <xsl:copy-of select="helper:printParameterRow('improper_style')"/>
                        <xsl:copy-of select="helper:printParameterRow('pair_style')"/>
                        <xsl:if test="l:getPotential(l:getParameter($setup, 'pair_style')) != ''">
                            <tr>
                                <td></td>
                                <td><xsl:value-of select="l:getPotential(l:getParameter($setup, 'pair_style'))"/></td>
                            </tr>
                        </xsl:if>
                        <xsl:copy-of select="helper:printParameterRow('pair_coeff')"/>
                        <xsl:copy-of select="helper:printParameterRow('minimize')"/>
                        <xsl:copy-of select="helper:printParameterRow('fix')"/>
                        <xsl:copy-of select="helper:printParameterRow('timestep', 'time')"/>
                        <xsl:copy-of select="helper:printParameterRow('run')"/>
                    </tbody>
                </table>
                <script type="text/javascript">
                    $(document).ready(function() {                        
                        $('table#timeSettings').dataTable( {                                                      
                            "bFilter": false,
                            "bPaginate": false,
                            "bSort": false,
                            "bInfo": false
                        } );   
                    } );                                       
                </script>
            </div>
        </div>   
    </xsl:template>

    <xsl:function name="helper:printParameterRow">        
        <xsl:param name="parameterName" as="xs:string"/>        
        <xsl:copy-of select="helper:printParameterRow($parameterName, '')"/>       
    </xsl:function>
    
    <xsl:function name="helper:printParameterRow">
        <xsl:param name="parameterName" as="xs:string"/>
        <xsl:param name="unitType" as="xs:string" />
        <xsl:variable name="params" select="l:getParameter($setup, $parameterName)" />
        <xsl:if test="exists($params)">            
            <xsl:for-each select="$params">
                <tr>
                    <td><xsl:if test="position() = 1"><xsl:value-of select="$parameterName"/></xsl:if></td>
                    <td>
                        <xsl:value-of select="./text()"/>                  
                        <xsl:if test="$unitType != ''">
                            <xsl:value-of select="concat(' ',l:getUnits($units, $unitType))" />
                        </xsl:if>
                    </td>
                </tr>
            </xsl:for-each>
            
        </xsl:if>
    </xsl:function>
    
    <!--Molecular dynamics animation  -->
    <xsl:template name="molecularDynamics">
        <xsl:variable name="dump" select="l:getParameter($setup, 'dump')" />
        <xsl:variable name="run" select="l:getParameter($setup, 'run')" />
        <xsl:if test="exists($dump)">
            <xsl:variable name="isLargeMolecule" select="count($molecule//cml:atom) &gt; 1000"/>
            <xsl:variable name="n" select="round(l:getFirstInteger($run) div l:getFirstInteger($dump)) "/>            
            <xsl:variable name="cifurl" select="replace($xyzurl, 'xyz', 'cif')"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#molecularDynamics" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Molecular dynamics
                    </h4>
                </div>          
                <div id="molecularDynamics" class="panel-collapse collapse">
                    <div class="panel-body">                   
                        <div class="row bottom-buffer">                                
                            <div class="col-sm-12">
                                <script type="text/javascript">
                                    var range = <xsl:value-of select="if($isLargeMolecule) then '50' else '200'"/>;
                                    var stepCount = 0;
                                    var dump = <xsl:value-of select="l:getFirstInteger(replace($dump, '^(\s*(\S+\s+){3})', ''))"/>;
                                    var timestep = <xsl:value-of select="if(exists(l:getParameter($setup, 'timestep'))) then 
                                                                            number(l:getParameter($setup, 'timestep'))
                                                                          else
                                                                            number(l:getDefaultTimeStep($setup))
                                                                          " />;
                                    var n = <xsl:value-of select="$n"/>;
                                    
                                    delete Jmol._tracker;
                                    Jmol._local = true;
                                    var jmolAppletMD = null;
                                    
                                    jmolMD_isReady = function(applet) {                               
                                        resizeMDApplet();
                                    }	
                                    
                                    var JmolConfigInfoMD = {
                                        debug: false,
                                        color: "0xFFFFFF",
                                        addSelectionOptions: false,		
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                        readyFunction: jmolMD_isReady,
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        allowJavaScript: true,
                                        console: "none"
                                    };
                                                                        
                                    
                                    $('#molecularDynamics').on('shown.bs.collapse', function () {
                                        if(jmolAppletMD == null)
                                            loadJSmolMD();
                                        else {
                                            resizeMDApplet();
                                        Jmol.script(jmolAppletMD,"animation MODE LOOP; animation OFF;");
                                        }
                                    });
                                    
                                    function loadJSmolMD(){
                                        jmolAppletMD = Jmol.getApplet("jmolAppletMD", JmolConfigInfoMD);
                                        $("#mdJSmolDiv").html(Jmol.getAppletHtml(jmolAppletMD));                                                                                                                   
                                    }
                                    
                                    function setMdSpeed() {
                                        var speed = $('#mdSpeedSelect').val();
                                        Jmol.script(jmolAppletMD,"animation FPS " + speed);
                                    }
                                    
                                    function displayMolecularDynamicClip() {
                                        var clip = $('#mdClipSelect').val();
                                        if(clip == 0)
                                            return;
                                    
                                        var speed = $('#mdSpeedSelect').val();                                        
                                        var from = (clip -1) * range;
                                        Jmol.script(jmolAppletMD,"set echo top right; echo Loading, please wait.;");
                                        if(Jmol.getPropertyAsString(jmolAppletMD, 'modelInfo.modelCount').endsWith('\t1'))
                                            Jmol.script(jmolAppletMD,"load <xsl:value-of select="$cifurl"/>&#38;mode=lammps_md&#38;index=" + clip + "&#38;range=" + range + "; set platformSpeed <xsl:value-of select="if($isLargeMolecule) then '2' else '3'"/>; center;");    
                                        else
                                            Jmol.script(jmolAppletMD,"save ORIENTATION previousOrientation; load <xsl:value-of select="$cifurl"/>&#38;mode=lammps_md&#38;index=" + clip + "&#38;range=" + range + "; set platformSpeed <xsl:value-of select="if($isLargeMolecule) then '2' else '3'"/>; center;");                                    
                                        
                                        Jmol.script(jmolAppletMD,"set echo top right; echo Frame: @{(_modelIndex  + " + from  + ")} / " + (stepCount - 1) + " | Time: @{((_modelIndex  + " + from  + ")*" + dump + "*" + timestep +")%3} <xsl:value-of select="l:getUnits($units, 'time')" />; restore ORIENTATION previousOrientation 0; animation MODE LOOP; animation ON; animation FPS " + speed + ";");
                                    }
                                    
                                    function resizeMDApplet(){
                                        var width = $("div#molecularDynamics").parent().width() - 50;
                                        var height = $( window ).height() - 50; 
                                        Jmol.resizeApplet(jmolAppletMD, [width,height]);                                                                 
                                    }
                                    
                                    window.onresize = function () {
                                        resizeMDApplet();
                                    };
                                    
                                    function startMD() {
                                        Jmol.script(jmolAppletMD,"animation ON;");
                                    }
                                    
                                    function stopMD() {
                                        Jmol.script(jmolAppletMD,"animation OFF;");
                                    }  
                                    
                                    $(document).ready(function() {                                    
                                        $.ajax({
                                            url: '<xsl:value-of select="$cifurl"/>&#38;mode=lammps_md' 
                                        }).done(function(data) {                                           
                                            stepCount = data;
                                            setStepsRange(data);        
                                        });
                                    });
                                    
                                    <![CDATA[
                                    function setStepsRange() {                                        
                                        var count = Math.trunc(stepCount / range) ;
                                        if(( stepCount % range ) > 0) {
                                            count++;
                                        }
                                                                                
                                        for(var inx = 1; inx <= count; inx++){
                                            $('#mdClipSelect').append(`<option value="${inx}">${inx}</option>`);
                                        }
                                    } 
                                    ]]>
                                </script>
                                
                                <div style="padding-bottom: 5px; display: flex; justify-content: flex-start;">                                    
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <label for="mdClipSelect" class="mr-2">Load clip: </label>
                                            <select class="form-control mr-4" id="mdClipSelect" onchange="displayMolecularDynamicClip()">
                                                <option value="0">Select one</option>
                                            </select>   
                                        </div>
                                        <div class="form-group">
                                            <label for="mdSpeedSelect" class="mr-2"> FPS: </label>
                                            <select class="form-control mr-4" id="mdSpeedSelect" onchange="setMdSpeed()">
                                                <option value="5">5</option>
                                                <option value="10" >10</option>
                                                <option value="20" selected="selected">20</option>
                                                <option value="30" >30</option>
                                                <option value="50" >50</option>                                                
                                            </select>   
                                        </div>                                                                                
                                    </form>                                                                     
                                    <button type="button" class="btn btn-default mr-3" name="mdStartAnimation" id="mdStartAnimation" value="Start" onclick="startMD()">
                                        <span class="fas fa-play" aria-hidden="true" /> Play                                        
                                    </button>
                                    <button type="button"  class="btn btn-default mr-5" name="mdStopAnimation" id="mdStopAnimation" value="Stop" onclick="stopMD()">
                                        <span class="fas fa-stop" aria-hidden="true" /> Stop
                                    </button>
                                    <a class="btn btn-secondary" href="{concat($cifurl, '&#38;mode=lammps_md&#38;index=0')}" target="_blank" title="Download entire MD as a multi-CIF file"><span class="fas fa-download" aria-hidden="true" /></a>
                                </div>
                                <div id="mdJSmolDiv" style="min-width: 400px; min-height: 400px; padding-left: 10px; padding-right: 10px; display: flex; justify-content: center;">
                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>                                              
        </xsl:if>
    </xsl:template>
    
    
    <xsl:function name="l:getFirstInteger" as="xs:integer">        
        <xsl:param name="line" />
        
        <xsl:variable name="integers" select="tokenize(helper:trim(replace($line,'\D+',' ')),'\s+')"/>
        <xsl:value-of select="
                if(exists($integers[1])) then 
                    format-number(number($integers[1]), '#0')
                else number(1)"/>        
    </xsl:function>
    
    <!-- Modules -->    
    <xsl:template name="energies">           
        <xsl:variable name="steps" select=".//cml:module[@dictRef='cc:finalization']//cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='steps']/cml:list[@cmlx:templateRef='steps']" />
        <xsl:if test="exists($steps)">
            
            <xsl:variable name="stepnumber" select="tokenize($steps/cml:array[@dictRef='l:step'], '\s+')" />
            <xsl:variable name="time" select="tokenize($steps/cml:array[@dictRef='l:time'], '\s+')" />
            <xsl:variable name="cpu" select="tokenize($steps/cml:array[@dictRef='l:cpu'], '\s+')" />
            <xsl:variable name="poteng" select="tokenize($steps/cml:array[@dictRef='l:poteng'], '\s+')" />
            <xsl:variable name="kineng" select="tokenize($steps/cml:array[@dictRef='l:kineng'], '\s+')" />
            <xsl:variable name="toteng" select="tokenize($steps/cml:array[@dictRef='l:toteng'], '\s+')" />
            <xsl:variable name="enthalpy" select="tokenize($steps/cml:array[@dictRef='l:enthalpy'], '\s+')" />
            <xsl:variable name="temp" select="tokenize($steps/cml:array[@dictRef='l:temp'], '\s+')" />
            <xsl:variable name="press" select="tokenize($steps/cml:array[@dictRef='l:press'], '\s+')" />
            <xsl:variable name="volume" select="tokenize($steps/cml:array[@dictRef='l:volume'], '\s+')" />
            <xsl:variable name="density" select="tokenize($steps/cml:array[@dictRef='l:density'], '\s+')" />
            <xsl:variable name="cella" select="tokenize($steps/cml:array[@dictRef='l:cella'], '\s+')" />
            <xsl:variable name="cellb" select="tokenize($steps/cml:array[@dictRef='l:cellb'], '\s+')" />
            <xsl:variable name="cellc" select="tokenize($steps/cml:array[@dictRef='l:cellc'], '\s+')" />
            <xsl:variable name="cellAlpha" select="tokenize($steps/cml:array[@dictRef='l:cellAlpha'], '\s+')" />
            <xsl:variable name="cellBeta" select="tokenize($steps/cml:array[@dictRef='l:cellBeta'], '\s+')" />
            <xsl:variable name="cellGamma" select="tokenize($steps/cml:array[@dictRef='l:cellGamma'], '\s+')" />
                        
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#steps-{generate-id($steps)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Steps</h4>
                </div>            
                <div id="steps-{generate-id($steps)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div id="downloadTablestepsT-{generate-id($steps)}" class="text-right"><a class="text-right" href="javascript:showDownloadOptions('stepsT-{generate-id($steps)}');"><span class="text-right fa fa-download"></span></a></div>
                                <table class="display nowrap" id="stepsT-{generate-id($steps)}">
                                    <thead>
                                        <tr>                                         
                                            <th>Step</th>
                                            <th>Time [<xsl:value-of select="l:getUnits($units, 'time')"/>]</th>
                                            <th>CPU </th>
                                            <th>PotEng [<xsl:value-of select="l:getUnits($units, 'energy')"/>]</th>
                                            <th>KinEng [<xsl:value-of select="l:getUnits($units, 'energy')"/>]</th>
                                            <th>TotEng [<xsl:value-of select="l:getUnits($units, 'energy')"/>]</th>
                                            <th>Enthalpy [<xsl:value-of select="l:getUnits($units, 'energy')"/>]</th>
                                            <th>Temp [<xsl:value-of select="l:getUnits($units, 'temperature')"/>]</th>
                                            <th>Press [<xsl:value-of select="l:getUnits($units, 'pressure')"/>]</th>
                                            <th>Volume [<xsl:value-of select="l:getUnits($units, 'distance')"/>^3]</th>
                                            <th>Density [<xsl:value-of select="l:getUnits($units, 'density')"/>]</th>
                                            <th>cella [<xsl:value-of select="l:getUnits($units, 'distance')"/>]</th>
                                            <th>cellb [<xsl:value-of select="l:getUnits($units, 'distance')"/>]</th>
                                            <th>cellc [<xsl:value-of select="l:getUnits($units, 'distance')"/>]</th>
                                            <th>cellAlpha</th>
                                            <th>cellBeta</th>
                                            <th>cellGamma</th>
                                        </tr>                                        
                                    </thead>
                                    <tbody></tbody>                                    
                                </table>                                                            
                                <div id="stepsPlotlyContainer" style="min-height:650px" class="mt-4 d-flex"/>
                               
                                <script type="text/javascript">
                                    //Build table
                                    var stepTable = null;
                                    
                                    var stepnumber = [<xsl:for-each select="$stepnumber"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var time = [<xsl:for-each select="$time"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cpu = [<xsl:for-each select="$cpu"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var poteng = [<xsl:for-each select="$poteng"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var kineng = [<xsl:for-each select="$kineng"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var toteng = [<xsl:for-each select="$toteng"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var enthalpy = [<xsl:for-each select="$enthalpy"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var temp = [<xsl:for-each select="$temp"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var press = [<xsl:for-each select="$press"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var volume = [<xsl:for-each select="$volume"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var density = [<xsl:for-each select="$density"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cella = [<xsl:for-each select="$cella"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cellb = [<xsl:for-each select="$cellb"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cellc= [<xsl:for-each select="$cellc"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cellAlpha = [<xsl:for-each select="$cellAlpha"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cellBeta = [<xsl:for-each select="$cellBeta"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                    var cellGamma = [<xsl:for-each select="$cellGamma"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];

                                    //Build graphs                                              
                                    var data = new Array();
                                    
                                    //Cell properties
                                    serie = {
                                        x: time,
                                        y: cella,
                                        mode: 'lines',
                                        name: 'a',
                                    };
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: cellb,
                                        mode: 'lines',
                                        name: 'b'                                     
                                    }; 
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: cellc,
                                        mode: 'lines',
                                        name: 'c'                                     
                                    }; 
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: cellAlpha,
                                        mode: 'lines',
                                        name: 'alpha'                                     
                                    };
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: cellBeta,
                                        mode: 'lines',
                                        name: 'beta'                                     
                                    }; 
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: cellGamma,
                                        mode: 'lines',
                                        name: 'gamma'                                     
                                    }; 
                                    data.push(serie);
                                    
                                    //Thermodinamic conditions
                                    serie = {
                                        x: time,
                                        y: temp,
                                        yaxis: 'y2',
                                        mode: 'lines',
                                        name: 'Temperature [<xsl:value-of select="l:getUnits($units, 'temperature')"/>]'                                        
                                    };                 
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: press,
                                        yaxis: 'y2',
                                        mode: 'lines',
                                        name: 'Pressure [<xsl:value-of select="l:getUnits($units, 'pressure')"/>]'                                     
                                    };                                      
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: volume,
                                        yaxis: 'y2',
                                        mode: 'lines',
                                        name: 'Volume '                                     
                                    };                                      
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: density,
                                        yaxis: 'y2',
                                        mode: 'lines',
                                        name: 'Density [<xsl:value-of select="l:getUnits($units, 'density')"/>]'                                     
                                    };                                      
                                    data.push(serie);
                                                                          
                                    var serie = {
                                        x: time, 
                                        y: poteng,
                                        yaxis: 'y3',
                                        mode: 'lines',
                                        name: 'Potential'                                        
                                    };                 
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: kineng,
                                        yaxis: 'y3',
                                        mode: 'lines',
                                        name: 'Kinetic'                                     
                                    };                                      
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: toteng,
                                        yaxis: 'y3',
                                        mode: 'lines',
                                        name: 'Total'                                       
                                    };                                      
                                    data.push(serie);
                                    
                                    serie = {
                                        x: time,
                                        y: enthalpy,
                                        yaxis: 'y3',
                                        mode: 'lines',
                                        name: 'Enthalpy'                                       
                                    };                                      
                                    data.push(serie);
                                                                 
                                    $(document).ready(function(){
                                        var layout = {
                                            title: 'Steps',
                                            height: 650,
                                            legend: {
                                                traceorder: 'reversed',
                                                tracegroupgap: 20
                                            },                                          
                                            xaxis: {
                                                title: 'Time [<xsl:value-of select="l:getUnits($units, 'time')" />]',
                                                showgrid: true,
                                                tickmode: 'auto',
                                                tickvals: time                                                
                                            },
                                            yaxis: {
                                                domain: [0, 0.30],
                                                title: 'Cell properties'
                                            },
                                            yaxis2: {
                                                domain:  [0.35, 0.65],
                                                title: 'Thermodynamic conditions'
                                            },
                                            yaxis3: {
                                                domain: [0.70, 1],
                                                title: 'Energy [<xsl:value-of select="l:getUnits($units, 'energy')"/>]'
                                            }
                                        };
                                                                               
                                        Plotly.react('stepsPlotlyContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                        
                                        $('#steps-<xsl:value-of select="generate-id($steps)"/>').on('shown.bs.collapse', function () {                                            
                                            Plotly.relayout($('#stepsPlotlyContainer')[0],{autosize: true});
                                            if(stepTable == null)
                                                buildStepsTable();
                                        });
                                     });
                                    
                                      
                                    function buildStepsTable() {
                                        stepTable = $('table#stepsT-<xsl:value-of select="generate-id($steps)"/>').dataTable({
                                            "aaData": buildSteps(),                                                                   
                                            "bFilter": false,
                                            "bPaginate": true,                                           
                                            "bSort": false,
                                            "scrollX": true,
                                            "aoColumnDefs" : [                                    
                                                { "sClass": "text-right", "aTargets": [ 2,3,4,5,6,7,8,9,10,11,12,13,14,15,16 ] }
                                            ]                                            
                                        });
                                    }
                                    function buildSteps() {
                                        var steps = new Array();
                                        for(i = 0; i &lt; stepnumber.length; i++){
                                            var step = new Array();
                                            step.push(stepnumber[i]);
                                            step.push(time[i]);
                                            step.push(cpu[i]);
                                            step.push(poteng[i]);
                                            step.push(kineng[i]);
                                            step.push(toteng[i]);
                                            step.push(enthalpy[i]);
                                            step.push(temp[i]);
                                            step.push(press[i]);
                                            step.push(volume[i]);
                                            step.push(density[i]);
                                            step.push(cella[i]);
                                            step.push(cellb[i]);
                                            step.push(cellc[i]);
                                            step.push(cellAlpha[i]);
                                            step.push(cellBeta[i]);
                                            step.push(cellGamma[i]);
                                            
                                            steps.push(step);
                                        }                                    
                                        return steps;
                                    }
                                    
                                    
                                                                          
                                 </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Timing -->
    <xsl:template name="timing">  
        
        <xsl:variable name="mpi" select="$finalization/cml:property[@dictRef='mpi']/cml:list" />    
        <xsl:variable name="walltime" select="$finalization/cml:property[@dictRef='cc:walltime']" />    
    
        <xsl:if test="exists($mpi)">
                                    
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#timing-{generate-id($mpi)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Timing                       
                    </h4>
                </div>
                <div id="timing-{generate-id($mpi)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">                        
                            <div class="col-lg-12 col-sm-12">
                                <div class="row">
                                    <div class="col-md-8">
                                        <p>MPI task timing breakdown:</p>
                                        <table id="timingT-{generate-id($mpi)}" class="display nowrap">
                                            <thead>
                                                <tr>
                                                    <th>Section</th>
                                                    <th>min time</th>
                                                    <th>avg time</th>
                                                    <th>max time</th>
                                                    <th>%varavg</th>
                                                    <th>%total</th>
                                                </tr> 
                                            </thead>                                            
                                        </table>
                                        <script type="text/javascript">
                                            <xsl:variable name="mpisection" select="tokenize($mpi/cml:array[@dictRef='l:section'],'\s+')" />
                                            <xsl:variable name="mpimin" select="tokenize($mpi/cml:array[@dictRef='l:mintime'],'\s+')" />
                                            <xsl:variable name="mpiavg" select="tokenize($mpi/cml:array[@dictRef='l:avgtime'],'\s+')" />
                                            <xsl:variable name="mpimax" select="tokenize($mpi/cml:array[@dictRef='l:maxtime'],'\s+')" />
                                            <xsl:variable name="mpivararg" select="tokenize($mpi/cml:array[@dictRef='l:varavg'],'\s+')" />
                                            <xsl:variable name="mpitotal" select="tokenize($mpi/cml:array[@dictRef='l:totalpercent'],'\s+')" />

                                            var mpidata = [<xsl:for-each select="1 to count($mpisection)"><xsl:variable name="outerIndex" select="."/>                                                            
                                                ['<xsl:value-of select="$mpisection[$outerIndex]"/>','<xsl:value-of select="if(exists($mpimin[$outerIndex]) and $mpimin[$outerIndex] != '') then $mpimin[$outerIndex] else '-'"/>','<xsl:value-of select="$mpiavg[$outerIndex]"/>','<xsl:value-of select="if(exists($mpimax[$outerIndex]) and $mpimax[$outerIndex] != '') then $mpimax[$outerIndex] else '-'"/>','<xsl:value-of select="if(exists($mpivararg[$outerIndex]) and $mpivararg[$outerIndex] != '') then $mpivararg[$outerIndex] else '-'"/>','<xsl:value-of select="$mpitotal[$outerIndex]"/>']<xsl:if test="position() != last()">,</xsl:if>
                                                        </xsl:for-each>];

                                            $(document).ready(function() {                        
                                                $('table#timingT-<xsl:value-of select="generate-id($mpi)"/>').dataTable( {
                                                    "aaData": mpidata,                                                    
                                                    "bFilter": false,                                 
                                                    "bPaginate": false,                                    
                                                    "bSort": false,
                                                    "bInfo": false,
                                                    "aoColumnDefs" : [                                    
                                                    { "sClass": "text-right", "aTargets": [ 1,2,3,4,5 ] }
                                                    ]
                                                });
                                            });
                                        </script>
                                    </div>                                                                                                                                                       
                                    <xsl:if test="exists($walltime)">
                                        <div class="col-md-6 mt-4">                                            
                                            <table id="timingT-{generate-id($walltime[0])}" class="display nowrap">
                                                <thead>
                                                    <tr>
                                                        <th>Timing</th>
                                                        <th> </th>
                                                        <th></th>
                                                    </tr> 
                                                </thead>
                                                <tbody>                                                    
                                                    <xsl:for-each select="$walltime">
                                                        <tr>
                                                            <td>Wall time</td>
                                                            <td><xsl:value-of select="."/></td>
                                                            <td><xsl:value-of select="helper:printUnitSymbol(./cml:scalar/@units)"/></td>
                                                        </tr>
                                                    </xsl:for-each>                                                    
                                                    <xsl:if test="exists($finalization/cml:property[@dictRef='l:nsperday'])">
                                                        <tr>
                                                            <td>ns/day</td>
                                                            <td><xsl:value-of select="$finalization/cml:property[@dictRef='l:nsperday']"/></td>
                                                            <td></td>
                                                        </tr>
                                                    </xsl:if>
                                                    <xsl:if test="exists($finalization/cml:property[@dictRef='l:hourperns'])">
                                                        <tr>
                                                            <td>Hours/ns</td>
                                                            <td><xsl:value-of select="$finalization/cml:property[@dictRef='l:hourperns']"/></td>
                                                            <td></td>
                                                        </tr>
                                                    </xsl:if>
                                                    <xsl:if test="exists($finalization/cml:property[@dictRef='l:timestepperns'])">
                                                        <tr>
                                                            <td>Timesteps/s</td>
                                                            <td><xsl:value-of select="$finalization/cml:property[@dictRef='l:timestepperns']"/></td>
                                                            <td></td>
                                                        </tr>
                                                    </xsl:if>
                                                </tbody>
                                            </table>
                                            <script type="text/javascript">
                                                $(document).ready(function() {                        
                                                    $('table#timingT-<xsl:value-of select="generate-id($walltime[0])"/>').dataTable( {                                                
                                                        "bFilter": false,                                 
                                                        "bPaginate": false,                                    
                                                        "bSort": false,
                                                        "bInfo": false,
                                                        "aoColumnDefs" : [                                    
                                                        { "sClass": "text-right", "aTargets": [ 1 ] }
                                                        ]
                                                    } );   
                                                } );                                       
                                            </script>
                                        </div>
                                    </xsl:if>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
                                          
                    
    </xsl:template> 
   
   
    <xsl:function name="l:getUnits">
       <xsl:param name="units" as="xs:string"/>
       <xsl:param name="value" as="xs:string"/>
       
        <xsl:variable name="unitsTable">
            <unit type="mass" real="nonsi2:gram.mol-1" metal="nonsi2:gram.mol-1" si="si:kg" cgs="nonsi:g" electron ="nonsi2:amu" micro="nonsi2:picogram" nano="nonsi2:attogram" />
            <unit type="distance" real="nonsi:angstrom" metal="nonsi:angstrom" si="si:m" cgs="nonsi:cm" electron ="nonsi:bohr" micro="nonsi:micrometers" nano="nonsi:nanometers" />
            <unit type="time" real="nonsi:femtoseconds" metal="nonsi:picoseconds" si="si:s" cgs="si:s" electron ="nonsi:femtoseconds" micro="nonsi:microseconds" nano="nonsi:nanoseconds" />
            <unit type="energy" real="nonsi2:kcal.mol-1" metal="nonsi:electronvolt" si="si:joule" cgs="nonsi2:erg" electron ="nonsi:hartree" micro="nonsi2:picogram-micrometers2.microseconds-2" nano="nonsi2:attogram-nanometers2.nanoseconds-2" />
            <unit type="velocity" real="nonsi2:angstrom.femtoseconds-1" metal="nonsi2:angstrom.picoseconds-1" si="si:m.s-1" cgs="nonsi:cm.s-1" electron ="nonsi2:bohr.amu-1" micro="nonsi2:micrometers.microseconds-1" nano="nonsi2:nanometers.nanoseconds-1" />
            <unit type="force" real="nonsi2:kcal.mol-angstrom-1" metal="nonsi2:electronvolt.angstrom-1" si="si:newton" cgs="nonsi2:dyne" electron ="nonsi2:hartree.bohr-1" micro="nonsi2:picogram-micrometers.microseconds-2" nano="nonsi2:attogram-nanometer.nanosecond-2" />
            <unit type="torque" real="nonsi2:kcal.mol-1" metal="nonsi:electronvolt" si="nonsi2:newton-meter" cgs="nonsi2:dyne-cm" electron ="si:none" micro="nonsi2:picogram-micrometers2.microseconds-2" nano="nonsi2:attogram-nanometer2.nanosecond-2" />
            <unit type="temperature" real="si:k" metal="si:k" si="si:k" cgs="si:k" electron ="si:k" micro="si:k" nano="si:k" />
            <unit type="pressure" real="nonsi:atm" metal="nonsi:bar" si="si:pascal" cgs="si:none" electron ="si:pascal" micro="nonsi2:picogram.micrometers-microsecond-2" nano="nonsi2:attogram.nanometers-nanoseconds-2" />
            <unit type="dynamic viscosity" real="nonsi2:poise" metal="nonsi2:poise" si="nonsi2:pascal.s" cgs="nonsi2:poise" electron ="si:none" micro="nonsi2:picogram.micrometers-microsecond-1" nano="nonsi2:attogram.nanometers-nanoseconds-1" />
            <unit type="charge" real="nonsi:elementaryCharge" metal="nonsi:elementaryCharge" si="si:coulomb" cgs="nonsi2:statcoulomb" electron ="nonsi:elementaryCharge" micro="nonsi2:picocoulombs" nano="nonsi:elementaryCharge" />
            <unit type="dipole" real="nonsi2:elementaryCharge.angstrom" metal="nonsi2:elementaryCharge.angstrom" si="nonsi2:coulombs.m" cgs="nonsi2:debye" electron ="nonsi2:debye" micro="nonsi2:picocoulombs-micrometers-1" nano="nonsi2:elementaryCharge-nanometer" />
            <unit type="electric field" real="nonsi2:volt.angstrom-1" metal="nonsi2:volt.angstrom-1" si="nonsi2:volt.meter-1" cgs="si:none" electron ="nonsi2:volt.meter-1" micro="nonsi2:volt.micrometer-1" nano="nonsi2:volt.nanometer-1" />
            <unit type="density" real="nonsi:g.cm-3" metal="nonsi:g.cm-3" si="si:kg.m-3" cgs="nonsi:g.cm-3" electron ="nonsi2:volt.cm-1" micro="nonsi2:picogram.micrometers-3" nano="nonsi2:attogram.nanometers-3" />
        </xsl:variable>
       
        <xsl:choose>
            <xsl:when test="exists($unitsTable/*:unit[@type=$value])">
                <xsl:value-of select="helper:printUnitSymbol($unitsTable/*:unit[@type=$value]//@*[name(.) = $units])"/>    
            </xsl:when>
            <xsl:otherwise>
                N/A
            </xsl:otherwise>
        </xsl:choose>
       
    </xsl:function>
    
    <xsl:function name="l:getDefaultTimeStep">
        <xsl:param name="units" as="xs:string" />
        <xsl:choose>
            <xsl:when test="matches($units,'lj')">0.005</xsl:when>
            <xsl:when test="matches($units,'real')">1.0</xsl:when>            
            <xsl:when test="matches($units,'metal')">0.001</xsl:when>
            <xsl:when test="matches($units,'si')">1.0e-8</xsl:when>
            <xsl:when test="matches($units,'cgs')">1.0e-8</xsl:when>
            <xsl:when test="matches($units,'electron')">0.001</xsl:when>
            <xsl:when test="matches($units,'micro')">2.0</xsl:when>
            <xsl:when test="matches($units,'nano')">0.00045</xsl:when>            
        </xsl:choose>
    </xsl:function>
    
   
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
    
</xsl:stylesheet>
