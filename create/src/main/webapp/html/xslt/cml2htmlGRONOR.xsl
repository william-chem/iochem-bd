<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:gr="http://www.iochem-bd.org/dictionary/gronor/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk gr helper cmlx"
    version="2.0">

    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Apr 23, 2021</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>       
    </xd:doc> 
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>

    <!-- Environment module -->
    <xsl:variable name="environment" select="//cml:module[@id='job'][1]/cml:module[@id='environment']/cml:parameterList"/>
    <xsl:variable name="programParameter" select="$environment/cml:parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="$environment/cml:parameter[@dictRef='cc:programVersion']"/>
    <xsl:variable name="subversionParameter" select="$environment/cml:parameter[@dictRef='cc:programSubversion']"/>
    <xsl:variable name="numProc" select="$environment/cml:parameter[@dictRef='cc:numProc']" />
    <xsl:variable name="numProcGPU" select="$environment/cml:parameter[@dictRef='gr:numProcGPU']" />
    <xsl:variable name="numRanks" select="$environment/cml:parameter[@dictRef='gr:numRanks']" />
    <xsl:variable name="numAccRanks" select="$environment/cml:parameter[@dictRef='gr:numAccRanks']" />
    <xsl:variable name="runDate" select="$environment/cml:parameter[@dictRef='cc:runDate']" />
    <!-- Geometry -->
    <xsl:variable name="molecule" select="(//cml:molecule)[last()]"/>
    
    <!-- Initialization -->
    <xsl:variable name="initialization" select="//cml:module[@dictRef='cc:initialization']"/>    
    <xsl:variable name="method" select="$initialization/cml:parameterList/cml:parameter[@dictRef='cc:method']" />
    <xsl:variable name="labels" select="tokenize($initialization/cml:parameterList/cml:parameter[@dictRef='gr:mebfLabels'], '\|')" />
    <xsl:variable name="vectorTitle" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gr:vectorTitle']/cml:array/text()"/>
    <xsl:variable name="integralTitle" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gr:integralTitle']/cml:array/text()"/>
    
    <xsl:variable name="charge" select="$initialization/cml:parameterList/cml:parameter[@dictRef='cc:charge']/cml:scalar" />
    <xsl:variable name="multiplicity" select="$initialization/cml:parameterList/cml:parameter[@dictRef='cc:spinMultiplicity']/cml:scalar" />    
    <xsl:variable name="mbefs" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gr:MEBFs']/cml:matrix"/>
    <xsl:variable name="tauCI" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gr:tauCI']/cml:scalar" />
    <xsl:variable name="nucRepEner" select="$initialization/cml:parameterList/cml:parameter[@dictRef='cc:nuclearRepulsionEnergy']/cml:scalar" />
    <xsl:variable name="basis" select="$initialization/cml:module[@dictRef='gr:atomicBasisSet']" />
    <xsl:variable name="fragments" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gr:fragments']/cml:scalar" />
    
    <!-- Dimensions -->
    <xsl:variable name="dimActive" select="tokenize($initialization/cml:parameterList/cml:parameter[@dictRef='gr:active']/cml:array,'\s+')" />
    <xsl:variable name="dimInactive" select="tokenize($initialization/cml:parameterList/cml:parameter[@dictRef='gr:inactive']/cml:array,'\s+')" />
    <xsl:variable name="dimDeterminants" select="tokenize($initialization/cml:parameterList/cml:parameter[@dictRef='gr:determinants']/cml:array,'\s+')" />
    <xsl:variable name="dimFrozen" select="tokenize($initialization/cml:parameterList/cml:parameter[@dictRef='gr:frozen']/cml:array,'\s+')" />
    <xsl:variable name="dimFragBasis" select="tokenize($initialization/cml:parameterList/cml:parameter[@dictRef='gr:fragBasisFunctions']/cml:array,'\s+')" />  
    
    <!-- Calculation -->
    <xsl:variable name="calculation" select="//cml:module[@dictRef='cc:calculation']"/>    
    <xsl:variable name="matrices" select="$calculation/cml:module[@dictRef='gr:matrices']" />
    <xsl:variable name="nociStates" select="$calculation/cml:module[@dictRef='gr:nociStates']" />
    <xsl:variable name="elecCoupling" select="$calculation/cml:module[@dictRef='gr:elCoupling']" />
    
    
    <!-- Finalization -->
    <xsl:variable name="finalization" select="//cml:module[@dictRef='cc:finalization']/cml:propertyList"/>        
    <xsl:variable name="endDate" select="$finalization/cml:property[@dictRef='cc:jobdatetime.end']" />
    <xsl:variable name="wallTime" select="$finalization/cml:property[@dictRef='cc:wallTime']" />
        
    
    <xsl:variable name="quote">"</xsl:variable>
    <xsl:variable name="quote_escaped">\\"</xsl:variable>
    
    <xsl:variable name="singlequote">'</xsl:variable>
    <xsl:variable name="singlequote_escaped">\\'</xsl:variable>
    
    
    <xsl:template match="/">        
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                                                                                                               
                            <div>
                                <xsl:call-template name="atomicCoordinates">
                                    <xsl:with-param name="molecule" select="$molecule"/>
                                    <xsl:with-param name="basis" select="$basis"/>
                                </xsl:call-template>                                        
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>MOLECULAR INFO</h3>                            
                            <xsl:call-template name="molecularInfo"/>                             
                        </div>
                    </div>
                    <!-- Results -->
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">
                            <xsl:call-template name="noci"/>                            
                            <xsl:call-template name="timing"/>
                            <xsl:call-template name="printLicense"/>                              
                        </div>
                    </div> 
                    <script type="text/javascript">
                        function expandModule(moduleID){
                        $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                        $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {    
                        //Add custom styles to tables
                        $('div.dataTables_wrapper').each(function(){ 
                        $(this).children("table").addClass("compact");
                        $(this).children("table").addClass("display");
                        });
                        
                        $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                        var tableName = $(this).children("table").attr("id");
                        if(tableName != null){
                        var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                        $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                        }
                        });                            
                        $.unblockUI();                             
                        });
                        
                        function showDownloadOptions(tableName){                            
                        var table = $('#' + tableName).DataTable();                                                    
                        new $.fn.dataTable.Buttons( table, {
                        buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                        } );                            
                        table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                        $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        
                        $(window).resize(function() {
                        clearTimeout(window.refresh_size);
                        window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                        $('.dataTable').each(function(index){
                        var oTable = $(this).dataTable();
                        $(oTable).css({ width: $(oTable).parent().width() });
                        oTable.fnAdjustColumnSizing();                           
                        });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                        $(this).find('.dataTable').each(function(index){
                        var oTable = $(this).dataTable();
                        $(oTable).css({ width: $(oTable).parent().width() });
                        oTable.fnAdjustColumnSizing();                           
                        });  
                        })                        
                        
                    </script>                    
                </div>
        </body>
    </html>
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">        
        <xsl:param name="molecule"/>
        <xsl:param name="basis" />
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                                      
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>                                                       
                            <table id="atomicCoordinates"></table>
                            
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#atomicCoordinates').dataTable( {
                                "aaData": [
                                /* Reduced data set */
                                <xsl:for-each select="$molecule/cml:atomArray/cml:atom">
                                    <xsl:variable name="elementType" select="@elementType"/>
                                    <xsl:variable name="elementLabel" select="@id" />
                                    <xsl:variable name="atomBasis" select="$basis/cml:list[child::cml:scalar[@dictRef='gr:atomLabel'] = $elementLabel]" />
                                    <xsl:variable name="basisContracted">
                                        <xsl:if test="exists($atomBasis)">
                                            <xsl:variable name="shell" select="tokenize($atomBasis/cml:array[@dictRef='gr:shells']/text(), '\|')"/>
                                            <xsl:variable name="contracted" select="tokenize($atomBasis/cml:array[@dictRef='gr:contractedFunctions'], '\s+')"/>
                                            <xsl:for-each select="1 to count($shell)">
                                                <xsl:variable name="outerIndex" select="." />
                                                <xsl:value-of select="$contracted[$outerIndex]"/><xsl:value-of select="$shell[$outerIndex]"/>                                                
                                            </xsl:for-each>
                                        </xsl:if>
                                   </xsl:variable>
                                    <xsl:variable name="id" select="@id"/>                                      
                                    [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.0000')"/>","<xsl:value-of select="format-number(@y3,'#0.0000')"/>","<xsl:value-of select="format-number(@z3,'#0.0000')"/>", "<xsl:value-of select="$basisContracted"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                </xsl:for-each>    
                                ],
                                "aoColumns": [
                                { "sTitle": "ATOM" },
                                { "sTitle": "" },
                                { "sTitle": "x", "sClass": "right" },
                                { "sTitle": "y", "sClass": "right" },
                                { "sTitle": "z", "sClass": "right" }, 
                                { "sTitle": "Contracted basis set", "sClass": "right" }
                                ],
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                                } );   
                                } );                                       
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                    <xsl:text> (</xsl:text>
                    <xsl:value-of select="$subversionParameter/scalar/text()" />
                    <xsl:text>)</xsl:text> 
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>                               
            <tr>
                <td>Calculation type:</td>
                <td>NOCI</td>
            </tr>
            <tr>
                <td>Method(s):</td>
                <td><xsl:value-of select="$method"/></td>                
            </tr>          
        </table>        
    </xsl:template>
    
    
    <!-- Charge / Multiplicity section -->
    <xsl:template name="molecularInfo">
        <div class="row bottom-buffer">
            <div class="col-lg-6 col-md-10 col-sm-12">
                <table id="molecularInfo">
                    <tbody>
                        <tr>
                            <td>Charge</td>
                            <td class="right"><xsl:value-of select="$charge"/></td>                            
                        </tr>                        
                        <tr>
                            <td>Multiplicity</td>
                            <td class="right"><xsl:value-of select="$multiplicity"/></td>
                        </tr>                                               
                        <xsl:if test="exists($tauCI)">
                            <tr>
                                <td>tauCI</td>
                                <td class="right"><xsl:value-of select="$tauCI"/></td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="exists($vectorTitle) or exists($integralTitle)">
                            <tr>
                                <td rowspan="2" style="vertical-align:top">Vector and Integral Info</td> 
                                <td><xsl:value-of select="replace($vectorTitle, '\|','&lt;br&#47;&gt;')" disable-output-escaping="yes"/></td>
                            </tr>
                            <tr>
                                <td><xsl:value-of select="replace($integralTitle, '\|','&lt;br&#47;&gt;')" disable-output-escaping="yes"/></td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="exists($nucRepEner)">
                            <tr>
                                <td style="padding-right: 1em; white-space:nowrap">Nuclear repulsion energy</td>
                                <td class="right"><xsl:value-of select="$nucRepEner"/><xsl:text> </xsl:text><xsl:value-of select="helper:printUnitSymbol($nucRepEner/@units)"/></td>
                            </tr>
                        </xsl:if>
                        <xsl:if test="exists($fragments)">
                            <tr>
                                <td>Number of fragments</td>
                                <td class="right"><xsl:value-of select="$fragments"/></td>
                            </tr>
                        </xsl:if>                        
                    </tbody>
                </table>
                <xsl:if test="exists($dimActive)">
                    <div class="mt-4">
                        <p class="m-0">Fragment wave functions</p>
                        <table id="fragment">  
                            <thead>
                                <tr>
                                    <th class="dt-head-right"> </th>
                                    <xsl:for-each select="1 to count($dimActive)"><xsl:variable name="outerIndex" select="."/><th class="dt-head-right"> <xsl:value-of select="$outerIndex"/></th></xsl:for-each>                                            
                                </tr>
                            </thead>
                            <tbody>
                                <tr><td>Active</td><xsl:for-each select="$dimActive"><td class="dt-body-right"><xsl:value-of select="."></xsl:value-of></td></xsl:for-each></tr>
                                <tr><td>Inactive</td><xsl:for-each select="$dimInactive"><td class="dt-body-right"><xsl:value-of select="."></xsl:value-of></td></xsl:for-each></tr>
                                <tr><td>Determinants</td><xsl:for-each select="$dimDeterminants"><td class="dt-body-right"><xsl:value-of select="."></xsl:value-of></td></xsl:for-each></tr>
                                <tr><td>Frozen</td><xsl:for-each select="$dimFrozen"><td class="dt-body-right"><xsl:value-of select="."></xsl:value-of></td></xsl:for-each></tr>
                                <tr><td>Fragment basis</td><xsl:for-each select="$dimFragBasis"><td class="dt-body-right"><xsl:value-of select="."></xsl:value-of></td></xsl:for-each></tr>                                            
                            </tbody>
                        </table>
                        <script type="text/javascript">
                            $(document).ready(function() {                        
                                $('table#fragment').dataTable( {                                                
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false                               
                                });
                            });
                        </script>
                    </div>                            
                </xsl:if>
                <xsl:if test="exists($mbefs)">
                    <div class="mt-4">
                        <p class="m-0">MEBFs</p>
                        <xsl:variable name="values" select="tokenize($mbefs,'\|')"/>
                        <table id="mbefs" class="dataTable no-footer compact display">
                            <thead>
                                <tr>
                                    <th></th>
                                    <xsl:for-each select="1 to $mbefs/@columns"><xsl:variable name="outerIndex" select="."/><th><xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/></th></xsl:for-each>
                                </tr>                                
                            </thead>
                            <tbody>
                                <xsl:for-each select="1 to $mbefs/@rows">
                                    <tr>                                    
                                        <xsl:variable name="outerIndex" select="." />
                                        <td><xsl:value-of select="$outerIndex"/></td>
                                        <xsl:for-each select="1 to $mbefs/@columns">
                                            <xsl:variable name="innerIndex" select="." />
                                            <td class="dt-body-right">                        
                                                <xsl:value-of select="$values[ ($outerIndex - 1) * $mbefs/@columns + ($innerIndex)]"/>
                                            </td>
                                        </xsl:for-each>
                                    </tr>
                                </xsl:for-each>
                            </tbody>
                        </table> 
                        
                        <script type="text/javascript">
                            $(document).ready(function() {                        
                            $('table#mbefs').dataTable( {
                            "aoColumns": [{ "sClass": "left"}, <xsl:for-each select="1 to $mbefs/@columns"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right", "sTitle": "<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/>"},</xsl:for-each>],                                        
                            "bFilter": false,                                 
                            "bPaginate": false,                                    
                            "bSort": false,
                            "bInfo": false                                     
                            });   
                            } );  
                        </script>                                    
                    </div>                              
                </xsl:if>
            </div>
        </div>
    </xsl:template>

    <!-- NOCI -->
    <xsl:template name="noci">
        <xsl:if test="exists($matrices)">
            <xsl:variable name="hamiltonian" select="$matrices/cml:propertyList/cml:property[@dictRef='gr:hamiltonian']/cml:matrix"/>
            <xsl:variable name="hValues" select="tokenize($hamiltonian, '\|')"/>

            <xsl:variable name="overlap" select="$matrices/cml:propertyList/cml:property[@dictRef='gr:overlap']/cml:matrix"/>
            <xsl:variable name="oValues" select="tokenize($overlap, '\|')"/>           
            
            <xsl:variable name="coupling" select="$elecCoupling/cml:propertyList/cml:property[@dictRef='gr:mebfCoupling']/cml:matrix"/>
            <xsl:variable name="cValues" select="tokenize($coupling, '\|')"/>
            
            <xsl:variable name="nociroots" select="$nociStates/cml:module[@dictRef='gr:nociroot']" />            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#noci-{generate-id($matrices)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        NOCI energies and wave functions         
                    </h4>
                </div>
                <div id="noci-{generate-id($matrices)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-8 col-sm-12 mb-4">
                                Hamiltonian matrix
                                <table id="hamiltonianT-{generate-id($hamiltonian)}" class="mb-5"></table>                                
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#hamiltonianT-<xsl:value-of select="generate-id($hamiltonian)"/>').dataTable( {
                                         "aaData" : [
                                            <xsl:for-each select="1 to $hamiltonian/@rows">
                                                <xsl:variable name="outerIndex" select="."/>
                                                ['<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/>',<xsl:for-each select="1 to $hamiltonian/@columns"><xsl:variable name="innerIndex" select="."/>'<xsl:value-of select="$hValues[($outerIndex - 1) * $hamiltonian/@columns + ($innerIndex)]"/>',</xsl:for-each>],
                                            </xsl:for-each>
                                    
                                         ],                                    
                                         "aoColumns": [ { "sClass": "left"}, <xsl:for-each select="1 to $hamiltonian/@columns"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right", "sTitle": "<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/>"},</xsl:for-each>],
                                         "bFilter": false,                                 
                                         "bPaginate": false,                                    
                                         "bSort": false,
                                         "bInfo": false,                                       
                                        });   
                                    } );                                       
                                </script>
                                Overlap
                                <table id="overlapT-{generate-id($overlap)}" class="mb-5"></table>                                
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#overlapT-<xsl:value-of select="generate-id($overlap)"/>').dataTable( {
                                            "aaData" : [
                                            <xsl:for-each select="1 to $overlap/@rows">
                                                <xsl:variable name="outerIndex" select="."/>
                                                ['<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex],$outerIndex)"/>',<xsl:for-each select="1 to $overlap/@columns"><xsl:variable name="innerIndex" select="."/>'<xsl:value-of select="$oValues[($outerIndex - 1) * $overlap/@columns + ($innerIndex)]"/>',</xsl:for-each>],
                                            </xsl:for-each>
                                            
                                            ],                                    
                                            "aoColumns": [ { "sClass": "left"}, <xsl:for-each select="1 to $overlap/@columns"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right", "sTitle": "<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/>"},</xsl:for-each>],                                            
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false                                      
                                        });   
                                    } );                                       
                                </script>                                
                            </div> 
                            <div class="col-lg-8 col-sm-12 mb-5">
                                NOCI states
                                <table id="nociStatesT-{generate-id($nociroots[1])}">
                                    <thead>
                                        <tr>
                                            <th class="dt-head-right">State</th>
                                            <xsl:for-each select="1 to count($nociroots)"><xsl:variable name="outerIndex" select="."/><th><xsl:value-of select="$nociroots[$outerIndex]//property[@dictRef='gr:rootNumber']/cml:scalar"/></th></xsl:for-each>                                            
                                        </tr>
                                    </thead>
                                </table>
            
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('table#nociStatesT-<xsl:value-of select="generate-id($nociroots[1])"/>').dataTable( {
                                            "aaData" : [
                                            ['Energy (<xsl:value-of select="helper:printUnitSymbol($nociroots[1]//cml:property[@dictRef='gr:nociEnergy']/cml:scalar/@units)"/>)',<xsl:for-each select="1 to count($nociroots)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="$nociroots[$outerIndex]//property[@dictRef='gr:nociEnergy']/cml:scalar"/>',</xsl:for-each>],
                                            ['Relative (<xsl:value-of select="helper:printUnitSymbol($nociroots[1]//cml:property[@dictRef='gr:nociRelEnergy']/cml:scalar/@units)"/>)',<xsl:for-each select="1 to count($nociroots)"><xsl:variable name="outerIndex" select="."/>'<xsl:value-of select="$nociroots[$outerIndex]//property[@dictRef='gr:nociRelEnergy']/cml:scalar"/>',</xsl:for-each>],
                                            <xsl:for-each select="1 to $nociroots[1]//property[@dictRef='gr:mebfCoeff']/cml:array/@size">
                                            [<xsl:variable name="outerIndex" select="."/>
                                                '<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/>',
                                                <xsl:for-each select="1 to count($nociroots)">
                                                    <xsl:variable name="innerIndex" select="."/>
                                                    <xsl:variable name="mebfCoeff" select="tokenize($nociroots[$innerIndex]//property[@dictRef='gr:mebfCoeff']/cml:array, '\|')"/>
                                                    '<xsl:value-of select="$mebfCoeff[$outerIndex]"/>',
                                                </xsl:for-each>
                                            ],
                                            </xsl:for-each>
                                            ],
                                            "aoColumns": [ { "sClass": "right"}, <xsl:for-each select="1 to count($nociroots)">{ "sClass": "right"},</xsl:for-each>],
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false                                      
                                        });   
                                    } );                                       
                                </script>
                            </div>

                            <div class="col-lg-8 col-sm-12">
                                Electronic coupling (<xsl:value-of select="helper:printUnitSymbol($coupling/@units)"/>)
                                <table id="couplingT-{generate-id($coupling)}"></table>                                
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#couplingT-<xsl:value-of select="generate-id($coupling)"/>').dataTable( {
                                            "aaData" : [
                                    <xsl:for-each select="1 to $coupling/@rows"><xsl:variable name="outerIndex" select="."/>['<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex],$outerIndex)"/>',<xsl:for-each select="1 to $coupling/@columns"><xsl:variable name="innerIndex" select="."/>
                                        '<xsl:value-of select="gr:printLowerDiagonal($cValues[($outerIndex - 1) * $coupling/@columns + ($innerIndex)], $innerIndex, $outerIndex)"/>',
                                    </xsl:for-each>],
                                                 </xsl:for-each>],
                                            "aoColumns": [ { "sClass": "left"}, <xsl:for-each select="1 to $coupling/@columns"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right", "sTitle": "<xsl:value-of select="gr:labelOrIndex($labels[$outerIndex], $outerIndex)"/>"},</xsl:for-each>],
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false                                      
                                        });   
                                    } );                                       
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Timing -->
    <xsl:template name="timing">  
        <xsl:variable name="taskSize" select="$environment/cml:parameter[@dictRef='gr:taskSize']" />
        <xsl:variable name="numProc" select="$environment/cml:parameter[@dictRef='cc:numProc']" />
        <xsl:variable name="numProcGPU" select="$environment/cml:parameter[@dictRef='gr:numProcGPU']" />
        <xsl:variable name="numRanks" select="$environment/cml:parameter[@dictRef='gr:numRanks']" />
        <xsl:variable name="numAccRanks" select="$environment/cml:parameter[@dictRef='gr:numAccRanks']" />
       
        <xsl:if test="exists($numProc)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#timing-{generate-id($numProc)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Timing                       
                    </h4>
                </div>
                <div id="timing-{generate-id($numProc)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6"> 
                                <table id="timingT-{generate-id($numProc)}">
                                    <thead>
                                        <tr>
                                            <th>Environment</th>
                                            <th> </th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Task size</td>
                                            <td><xsl:value-of select="$taskSize"/></td>
                                        </tr>
                                        <tr>
                                            <td>Number of processors</td>
                                            <td><xsl:value-of select="$numProc"/></td>                        
                                        </tr>                         
                                        <tr>
                                            <td>Number of GPU processors</td>
                                            <td><xsl:value-of select="$numProcGPU"/></td>                        
                                        </tr>                                             
                                        <tr>
                                            <td>Number of non-accelerated ranks</td>
                                            <td><xsl:value-of select="$numRanks"/></td>                        
                                        </tr>
                                        <tr>
                                            <td>Number of accelerated ranks</td>
                                            <td><xsl:value-of select="$numAccRanks"/></td>                        
                                        </tr>                                        
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#timingT-<xsl:value-of select="generate-id($numProc)"/>').dataTable( {
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 1 ] }]
                                        } );   
                                    } );                                       
                                </script>  
                            </div>
                        </div>             
                        
                        <xsl:if test="exists($wallTime)">
                        </xsl:if>
                        
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6"> 
                                <table id="timingT-{generate-id($wallTime)}">
                                    <thead>
                                        <tr>
                                            <th>Timing</th>
                                            <th> </th>
                                        </tr> 
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Start</td>
                                            <td><xsl:value-of select="$runDate"/></td>                        
                                        </tr>                         
                                        <tr>
                                            <td>End</td>
                                            <td><xsl:value-of select="$endDate"/></td>                        
                                        </tr>                                             
                                        <tr>
                                            <td>Wall time</td>
                                            <td><xsl:value-of select="$wallTime"/></td>                        
                                        </tr>                                             
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#timingT-<xsl:value-of select="generate-id($wallTime)"/>').dataTable( {
                                    "bFilter": false,                                 
                                    "bPaginate": false,                                    
                                    "bSort": false,
                                    "bInfo": false,
                                    "aoColumnDefs" : [                                    
                                    { "sClass": "text-right", "aTargets": [ 1 ] }
                                    ]
                                    } );   
                                    } );                                       
                                </script>  
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>                                  
        </xsl:if>                
    </xsl:template> 
    
    <xsl:function name="gr:escapeQuotes">
        <xsl:param name="value"/>
        <xsl:value-of select="replace(replace($value,$quote,$quote_escaped),$singlequote,$singlequote_escaped)"/>
    </xsl:function>
    
    <xsl:function name="gr:printLowerDiagonal">
        <xsl:param name="value"/>      
        <xsl:param name="col"/>
        <xsl:param name="row"/>
        <xsl:value-of select="
            if($row &gt; $col) then
                $value
            else
                ''
            ">            
        </xsl:value-of>
    </xsl:function>
    
    <xsl:function name="gr:labelOrIndex">
        <xsl:param name="label"/>
        <xsl:param name="index"/>
        
        <xsl:value-of select="
            if(exists($label)) then
                gr:escapeQuotes($label)
            else
                $index 
            "/>
    </xsl:function>
    
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
    
</xsl:stylesheet>