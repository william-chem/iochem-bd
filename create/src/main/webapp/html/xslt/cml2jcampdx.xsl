<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns="http://www.w3.org/1999/xhtml" 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema" 
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:math="http://www.exslt.org/math"
    xmlns:adf="http://www.scm.com/ADF/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd cml ckbk math"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Oct 1, 2013</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:include href="helper/chemistry/adf.xsl"/>
    <xsl:include href="helper/math/math.max.xslt"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:include href="helper/math/math.min.xslt"/>
    <xsl:output method="html" indent="no"/>
    <xsl:param name="title"/>
    <xsl:param name="index" />
    
    <xsl:variable name='newline'><xsl:text>
</xsl:text></xsl:variable>
    <xsl:variable name='blankspace'><xsl:text> </xsl:text></xsl:variable>
    
    <xsl:template match="/">
        <xsl:variable name="program" select="upper-case((//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']/scalar/text())[1])" />
        <xsl:choose>
            <xsl:when test="matches($program,'GAUSSIAN')">
                <xsl:call-template name="fromgaussian"/>                        
            </xsl:when>  
            <xsl:when test="matches($program,'ADF') or matches($program,'AMS')">
                <xsl:call-template name="fromadf"/>                        
            </xsl:when>
            <xsl:when test="matches($program,'MOPAC')">
                <xsl:call-template name="frommopac"/>            
            </xsl:when>
            <xsl:when test="matches($program,'VASP')">
                <xsl:call-template name="fromvasp"/>                        
            </xsl:when>  
            <xsl:when test="matches($program,'TURBOMOLE')">
                <xsl:call-template name="fromturbomole"/>                        
            </xsl:when> 
            <xsl:when test="matches($program,'ORCA')">
                <xsl:call-template name="fromorca"/>
            </xsl:when>
            <xsl:when test="matches($program,'MOLCAS')">
                <xsl:call-template name="frommolcas"/>
            </xsl:when>                           
        </xsl:choose>
    </xsl:template>
    
    <!-- JCAMPDX format from Gaussian formatted CML file -->
    <xsl:template name="fromgaussian">
        <xsl:variable name="index" select="if(exists($index) and $index != '') then xs:integer($index) else 1"/>
        <xsl:variable name="job" select="//module[@dictRef='cc:job'][descendant::property[@dictRef='cc:frequencies' or descendant::module[@cmlx:templateRef='l716.freq.chunkx']]][position() = $index]" />
                
      <xsl:variable name="molecule" select="
          if(exists($job/module[@dictRef='cc:calculation']//module[@cmlx:templateRef='l202.orient']/molecule)) then
            ($job/module[@dictRef='cc:calculation']//module[@cmlx:templateRef='l202.orient']/molecule)[last()]
          else
            $job/module[@dictRef='cc:finalization']/molecule"/>
       
      <xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
      <xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>
      <xsl:variable name="vibrations" select="if(exists($job//property//module[@cmlx:templateRef='l716.freq.high.precision.chunkx'])) then 
                                                  $job//property//module[@cmlx:templateRef='l716.freq.high.precision.chunkx']
                                              else
                                                  $job//property//module[@cmlx:templateRef='l716.freq.chunkx']"/>
      <xsl:variable name="serial" select="tokenize($vibrations/array[@dictRef='x:serial'],'[\s]')"/>
      <xsl:variable name="irrep" select="tokenize($vibrations/array[@dictRef='cc:irrep'],'[|]')"/>
      <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'[\s]')"/>
      <xsl:variable name="redmass" select="tokenize($vibrations/array[@dictRef='cc:redmass'],'[\s]')"/>
      <xsl:variable name="forceconst" select="tokenize($vibrations/array[@dictRef='cc:forceconst'],'[\s]')"/>
      <xsl:variable name="modelsys" select="tokenize($vibrations/array[@dictRef='cc:modelsys'],'[\s]')"/>
      <xsl:variable name="realsys" select="tokenize($vibrations/array[@dictRef='cc:realsys'],'[\s]')"/>
      <xsl:variable name="irintensity" select="tokenize($vibrations/array[@dictRef='cc:irintensity'],'[\s]')"/>
      <xsl:variable name="atomicNumber" select="tokenize($vibrations/array[@dictRef='cc:atomicNumber'],'[\s]')"/>
      <xsl:variable name="displacement" select="tokenize($vibrations/array[@dictRef='cc:displacement'],'[\s]')"/>          
      <xsl:variable name="formula" select="($molecule//formula/@concise)[1]"/>
        <xsl:variable name="frequencyNumber" as="xs:double*">      
            <xsl:for-each select="$frequency">
                <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>                                
            </xsl:for-each>        
        </xsl:variable>##TITLE= Compound file, contains several data records
##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bd.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
<xsl:value-of select="$newline"/>
<xsl:element name="ModelData">
<xsl:attribute name="id" select="$title"/>
<xsl:attribute name="type" select="'MOL'"/>
<xsl:value-of select="$newline"/>
<xsl:value-of select="$title"/>
DSViewer    3D   0

<xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
    <xsl:for-each select="1 to $atomcount">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
        <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
    </xsl:for-each> 
     <xsl:for-each select="1 to $bondcount">
        <xsl:variable name="outerIndex" select="."/>
         <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
         <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
         <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
     </xsl:for-each>M  END
</xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to $vibrations/array[@dictRef='x:serial']/@size">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>    
            <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
            <xsl:value-of select="concat($outerIndex,'  ','Energy=',$irintensity[$outerIndex],'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix,$newline)"/>
            <xsl:for-each select="1 to $atomcount">
                <xsl:variable name="innerIndex" select="."/>
                <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                <xsl:value-of select="ckbk:printVibrationLineWithAtomType($atom,$displacement, $outerIndex,$innerIndex,$atomcount)"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
   <xsl:attribute name="type" select="'IR'"/>
   <xsl:value-of select="$newline"/>
   <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
   <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
   <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>
   <xsl:for-each select="1 to $vibrations/array[@dictRef='x:serial']/@size">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:element name="PeakData">
            <xsl:attribute name="id" select="$outerIndex"/>            
            <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=',$irintensity[$outerIndex])"/>
            <xsl:attribute name="peakShape" select="'sharp'"/> 
            <xsl:attribute name="model" select="concat('1.',$outerIndex)"/>
            <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>
            <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="xMax" select="$xMaxValue"/>
            <xsl:attribute name="xMin" select="$xMinValue"/>
            <xsl:attribute name="yMax" select="1"/>
            <xsl:attribute name="yMin" select="0"/>            
        </xsl:element>
       <xsl:value-of select="$newline"/>        
    </xsl:for-each>
</xsl:element>  
<xsl:value-of select="ckbk:printPeakTable($irintensity,$frequency)"/>        
##END=
##END=$$ END of FILE</xsl:template>
    
    <!-- JCAMPDX format from ADF formatted CML file -->
    <xsl:template name="fromadf">        
        <xsl:variable name="molecule" select="
            if(exists(//property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule)) then
                //property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule
            else
                (//module[@dictRef='cc:initialization' and child::molecule])[last()]//molecule
        "/>
        <xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
        <xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>
        
        <xsl:variable name="vibrations" select="adf:getFrequencies(
                                                        //property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations'],
                                                        //module[@id='finalization']//property/module[@cmlx:templateRef='scanfreq']
                                                        )"/>
        <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'[\s+]')"/>        
        <xsl:variable name="elementType" select="tokenize($vibrations/array[@dictRef='cc:elementType'],'[\s]')"/>
        <xsl:variable name="displacement" select="tokenize($vibrations/array[@dictRef='cc:displacement'],'[\s]')"/>
        <xsl:variable name="irintensity" select="tokenize(
                                                        adf:getIntensities(
                                                                           //module[@dictRef='cc:finalization']//property[@dictRef='cc:intensities']/module[@cmlx:templateRef='intensities'],
                                                                           //module[@id='finalization']//property/module[@cmlx:templateRef='scanfreq']
                                                                           )
                                                        /array[@dictRef='cc:absortion']
                                                   ,'[\s]')"/>                
        <xsl:variable name="formula" select="$molecule/formula/@concise"/>##TITLE= Compound file, contains several data records
        <xsl:variable name="frequencyNumber" as="xs:double*">      <!-- Discard zero vibrations from frequencies -->
            <xsl:for-each select="$frequency">
                <xsl:if test="number(.) != 0">
                    <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>
                </xsl:if>                
            </xsl:for-each>        
        </xsl:variable>
        
##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bg.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="$title"/>
        <xsl:attribute name="type" select="'MOL'"/>
        <xsl:value-of select="$newline"/>
        <xsl:value-of select="$title"/>
        DSViewer    3D   0
        
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each> 
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to count($frequency)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
            <xsl:value-of select="concat($outerIndex,'  ','Energy=',$irintensity[$outerIndex],'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix ,$newline)"/>
            <xsl:for-each select="1 to $atomcount">
                <xsl:variable name="innerIndex" select="."/>
                <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                <xsl:value-of select="ckbk:printVibrationLineWithAtomType($atom,$displacement, $outerIndex,$innerIndex,$atomcount)"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
    <xsl:attribute name="type" select="'IR'"/>
    <xsl:value-of select="$newline"/>
    <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
    <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>   
    <xsl:for-each select="1 to count($frequency)">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:element name="PeakData">
            <xsl:attribute name="id" select="$outerIndex"/>            
            <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=',$irintensity[$outerIndex])"/>
            <xsl:attribute name="peakShape" select="'sharp'"/> 
            <xsl:attribute name="model" select="concat('1.',$outerIndex)"/>
            <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>
            <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="xMax" select="$xMaxValue"/>
            <xsl:attribute name="xMin" select="$xMinValue"/>
            <!--            <xsl:attribute name="xMax" select="number($frequency[$outerIndex]) + 1"/>
    <xsl:attribute name="xMin" select="number($frequency[$outerIndex]) - 1"/>
-->            <xsl:attribute name="yMax" select="1"/>
            <xsl:attribute name="yMin" select="0"/>            
        </xsl:element>
        <xsl:value-of select="$newline"/>        
    </xsl:for-each>
</xsl:element>  
<xsl:value-of select="ckbk:printPeakTable($irintensity,$frequency)"/>        
##END=
##END=$$ END of FILE</xsl:template>

<!-- JCAMPDX format from MOPAC formatted CML file -->
<xsl:template name="frommopac">        
<xsl:variable name="molecule" select="
    if(exists(//property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule)) then
        //property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule
    else
        (//module[@dictRef='cc:initialization' and child::molecule])[last()]//molecule
    "/>
<xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
<xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>

<xsl:variable name="vibrations" select="//property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations']"/>
<xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'[\s+]')"/>        
<xsl:variable name="elementType" select="tokenize($vibrations/array[@dictRef='cc:elementType'],'[\s]')"/>
<xsl:variable name="displacement" select="tokenize($vibrations/array[@dictRef='cc:displacement'],'[\s]')"/>
<xsl:variable name="powdipole">    
    <xsl:variable name="tdipole" select="tokenize(//property[@dictRef='cc:frequencies']//array[@dictRef='mp:transitiondipole'],'\s+')"/>
    <xsl:for-each select="$tdipole">
        <xsl:value-of select="abs(number(.))"/><xsl:text> </xsl:text>
    </xsl:for-each> 
</xsl:variable>
<xsl:variable name="irintensity" select="tokenize(helper:trim($powdipole),'\s')"/>
                
<xsl:variable name="formula" select="$molecule/formula/@concise"/>##TITLE= Compound file, contains several data records
<xsl:variable name="frequencyNumber" as="xs:double*">      <!-- Discard zero vibrations from frequencies -->
    <xsl:for-each select="$frequency">
        <xsl:if test="number(.) != 0">
            <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>
        </xsl:if>                
    </xsl:for-each>        
</xsl:variable>

##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bg.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="$title"/>
        <xsl:attribute name="type" select="'MOL'"/>
        <xsl:value-of select="$newline"/>
        <xsl:value-of select="$title"/>
        DSViewer    3D   0
        
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each> 
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to count($frequency)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
            <xsl:value-of select="concat($outerIndex,'  ','Energy=',$irintensity[$outerIndex],'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix ,$newline)"/>
            <xsl:for-each select="1 to $atomcount">
                <xsl:variable name="innerIndex" select="."/>
                <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                <xsl:value-of select="ckbk:printVibrationLineWithAtomType($atom,$displacement, $outerIndex,$innerIndex,$atomcount)"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
    <xsl:attribute name="type" select="'IR'"/>
    <xsl:value-of select="$newline"/>
    <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
    <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>   
    <xsl:for-each select="1 to count($frequency)">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:element name="PeakData">
            <xsl:attribute name="id" select="$outerIndex"/>            
            <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=',$irintensity[$outerIndex])"/>
            <xsl:attribute name="peakShape" select="'sharp'"/> 
            <xsl:attribute name="model" select="concat('1.',$outerIndex)"/>
            <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>
            <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="xMax" select="$xMaxValue"/>
            <xsl:attribute name="xMin" select="$xMinValue"/>
            <!--            <xsl:attribute name="xMax" select="number($frequency[$outerIndex]) + 1"/>
<xsl:attribute name="xMin" select="number($frequency[$outerIndex]) - 1"/>
-->            <xsl:attribute name="yMax" select="1"/>
            <xsl:attribute name="yMin" select="0"/>            
        </xsl:element>
        <xsl:value-of select="$newline"/>        
    </xsl:for-each>
</xsl:element>  
<xsl:value-of select="ckbk:printPeakTable($irintensity,$frequency)"/>        
##END=
##END=$$ END of FILE</xsl:template>


    <!-- JCAMPDX format from VASP formatted CML file -->
    <xsl:template name="fromvasp">        
        <xsl:variable name="molecule" select="//property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule"/>
        <xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
        <xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>
        
        <xsl:variable name="vibrations" select="(//property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations'])[last()]"/>
        <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'[\s+]')"/>        
        <xsl:variable name="elementType" select="$molecule/atomArray/atom/@elementType"/>
        <xsl:variable name="displacement" select="tokenize($vibrations/array[@dictRef='cc:displacement'],'[\s]')"/>

        <!-- To set correctly -->
        <xsl:variable name="irintensity" select="//property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations']"/>
        <!--<xsl:variable name="irintensity" select="tokenize(//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='intensities']/array[@dictRef='cc:absortion'],'[\s]')"/>-->                
        <xsl:variable name="formula" select="helper:calculateHillNotationFormula($molecule/atomArray)"/>##TITLE= Compound file, contains several data records
##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bd.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="$title"/>
        <xsl:attribute name="type" select="'MOL'"/>
        <xsl:value-of select="$newline"/>
        <xsl:value-of select="$title"/>
        DSViewer    3D   0
        
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each> 
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to count($frequency)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
            <xsl:value-of select="concat($outerIndex,'  ','Energy=',$irintensity[$outerIndex],'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix ,$newline)"/>
            <xsl:for-each select="1 to $atomcount">
                <xsl:variable name="innerIndex" select="."/>
                <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                <xsl:value-of select="ckbk:printVibrationLineWithAtomType($atom,$displacement, $outerIndex,$innerIndex, $atomcount)"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
    <xsl:attribute name="type" select="'IR'"/>
    <xsl:value-of select="$newline"/>
    <xsl:variable name="xOffset" select="0.05 * number($frequency[count($frequency)]) - number($frequency[1])"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="number($frequency[1]) - $xOffset"/>
    <xsl:variable name="lastX" select="number($frequency[count($frequency)]) + $xOffset"/>
    <xsl:for-each select="1 to count($frequency)">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:element name="PeakData">
            <xsl:attribute name="id" select="$outerIndex"/>            
            <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=',$irintensity[$outerIndex])"/>
            <xsl:attribute name="peakShape" select="'sharp'"/> 
            <xsl:attribute name="model" select="concat('1.',$outerIndex)"/>
            <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>
            <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="xMax" select="$xMaxValue"/>
            <xsl:attribute name="xMin" select="$xMinValue"/>
            <!--            <xsl:attribute name="xMax" select="number($frequency[$outerIndex]) + 1"/>
<xsl:attribute name="xMin" select="number($frequency[$outerIndex]) - 1"/>
-->            <xsl:attribute name="yMax" select="1"/>
            <xsl:attribute name="yMin" select="0"/>            
        </xsl:element>
        <xsl:value-of select="$newline"/>        
    </xsl:for-each>
</xsl:element>  
<xsl:value-of select="ckbk:printPeakTable($irintensity,$frequency)"/>        
##END=
##END=$$ END of FILE</xsl:template>
    
    <!-- JCAMPDX format from Orca formatted CML file -->
    <xsl:template name="fromorca">
    <xsl:variable name="molecule" select="
        if(exists(//property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule)) then
            //property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule
        else
            (//module[@dictRef='cc:initialization' and child::molecule])[last()]/molecule
        "/>
    <xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
    <xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>
    
    <xsl:variable name="vibrations" select="//property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations']"/>
    <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'[\s+]')"/>
    <xsl:variable name="elementType" select="$molecule/atomArray/atom/@elementType"/>
    <xsl:variable name="displacement" select="tokenize($vibrations/matrix[@dictRef='cc:displacement'],'[\s+]')"/>        
        
    <xsl:variable name="irintensity" select="tokenize(//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='irspectrum']/array[@dictRef='o:t2'],'[\s+]')"/>
    <xsl:variable name="firstIntensityIndex" select="tokenize(//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='irspectrum']/array[@dictRef='cc:serial'],'[\s+]')[1]"/>
    
    <!-- Add zero intensity values on first unnassigned vibrations -->    
    <xsl:variable name="fixedIrIntensity">
        <xsl:for-each select="1 to  xs:integer($firstIntensityIndex)">
            <xsl:value-of select="'0.0 '"/>                
        </xsl:for-each>
        <xsl:for-each select="$irintensity">            
            <xsl:value-of select="."/><xsl:if test="position() != count($irintensity)"><xsl:text> </xsl:text></xsl:if>
        </xsl:for-each>
    </xsl:variable>        
   
   <xsl:variable name="irinstensity2" select="tokenize($fixedIrIntensity,'\s+')"/>
        
        
    <xsl:variable name="formula" select="$molecule/formula/@concise"/>##TITLE= Compound file, contains several data records
    <xsl:variable name="frequencyNumber" as="xs:double*">      <!-- Discard zero vibrations from frequencies -->
        <xsl:for-each select="$frequency">
            <xsl:if test="number(.) != 0">
                <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>
            </xsl:if>
        </xsl:for-each>        
    </xsl:variable>
##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bg.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="$title"/>
        <xsl:attribute name="type" select="'MOL'"/>
        <xsl:value-of select="$newline"/>
        <xsl:value-of select="$title"/>
        DSViewer    3D   0
        
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each> 
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to count($frequency)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
            <xsl:value-of select="concat($outerIndex,'  ','Energy=',$irinstensity2[$outerIndex],'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix ,$newline)"/>
            <xsl:for-each select="1 to $atomcount">
                <xsl:variable name="innerIndex" select="."/>
                <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                <xsl:value-of select="ckbk:printVibrationLineWithAtomType($atom,$displacement, $outerIndex,$innerIndex,$atomcount)"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
    <xsl:attribute name="type" select="'IR'"/>
    <xsl:value-of select="$newline"/>
    <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
    <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>   
    <xsl:for-each select="1 to count($frequency)">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:element name="PeakData">
            <xsl:attribute name="id" select="$outerIndex"/>            
            <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=', $irinstensity2[$outerIndex])"/>
            <xsl:attribute name="peakShape" select="'sharp'"/> 
            <xsl:attribute name="model" select="concat('1.',$outerIndex)"/>
            <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>
            <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="xMax" select="$xMaxValue"/>
            <xsl:attribute name="xMin" select="$xMinValue"/>
            <!--            <xsl:attribute name="xMax" select="number($frequency[$outerIndex]) + 1"/>
<xsl:attribute name="xMin" select="number($frequency[$outerIndex]) - 1"/>
-->            <xsl:attribute name="yMax" select="1"/>
            <xsl:attribute name="yMin" select="0"/>            
        </xsl:element>
        <xsl:value-of select="$newline"/>        
    </xsl:for-each>
</xsl:element>  
<xsl:value-of select="ckbk:printPeakTable($irinstensity2,$frequency)"/>        
##END=
##END=$$ END of FILE</xsl:template>

    <!-- JCAMPDX format from TURBOMOLE formatted CML file -->
    <xsl:template name="fromturbomole">        
        <xsl:variable name="molecule" select="//property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule"/>
        <xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
        <xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>
        
        <xsl:variable name="vibrations" select="//property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations']"/>        
        <xsl:variable name="displacementx" select="tokenize($vibrations/array[@dictRef='t:coordx'],'[\s]')"/>
        <xsl:variable name="displacementy" select="tokenize($vibrations/array[@dictRef='t:coordy'],'[\s]')"/>
        <xsl:variable name="displacementz" select="tokenize($vibrations/array[@dictRef='t:coordz'],'[\s]')"/>
        
        <xsl:variable name="frequency" select="tokenize(//property[@dictRef='cc:frequencies']//cml:module[@cmlx:templateRef='spectrum']/array[@dictRef='cc:frequency'] ,'[\s+]')"/>
        <xsl:variable name="irintensity" select="tokenize(//property[@dictRef='cc:frequencies']//cml:module[@cmlx:templateRef='spectrum']//array[@dictRef='cc:irintensity'],'[\s]')"/>        
        
        <xsl:variable name="frequencyNumber" as="xs:double*">      <!-- Discard zero vibrations from frequencies -->
            <xsl:for-each select="$frequency">
                <xsl:if test="number(.) != 0">
                    <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>
                </xsl:if>                
            </xsl:for-each>        
        </xsl:variable>
        <xsl:variable name="intensityNumber" as="xs:double*">      <!-- Discard zero vibrations from intensities-->
            <xsl:for-each select="1 to count($frequency)">
                <xsl:variable name="outerIndex" select="."/>
                <xsl:if test="number($frequency[$outerIndex]) != 0">
                    <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="$irintensity[$outerIndex]"/></xsl:element>
                </xsl:if>                
            </xsl:for-each>        
        </xsl:variable>
        
        <xsl:variable name="discardedNumber" select="count($frequency) - count($frequencyNumber)"/>     
        <xsl:variable name="formula" select="$molecule/formula/@concise"/>##TITLE= Compound file, contains several data records
##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bg.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="$title"/>
        <xsl:attribute name="type" select="'MOL'"/>
        <xsl:value-of select="$newline"/>
        <xsl:value-of select="$title"/>
        DSViewer    3D   0
        
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each> 
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to count($frequency)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>                
            <xsl:if test="number($frequency[$outerIndex]) != 0">  <!-- Discard zero vibrations -->
                <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
                <xsl:value-of select="concat(ckbk:normalizeVibrationIndex($outerIndex,$frequency[$outerIndex],$discardedNumber),'  ','Energy=',$irintensity[$outerIndex],'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix,$newline)"/>
                <xsl:for-each select="1 to $atomcount">
                    <xsl:variable name="innerIndex" select="."/>
                    <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                    <xsl:value-of select="ckbk:printTurbomoleVibrationLineWithAtomType(count($frequency), $atom, $displacementx, $displacementy, $displacementz, $outerIndex, $innerIndex)"/>
                </xsl:for-each>
            </xsl:if>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
    <xsl:attribute name="type" select="'IR'"/>
    <xsl:value-of select="$newline"/>
    <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
    <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>
    <xsl:for-each select="1 to count($frequency)">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:if test="number($frequency[$outerIndex]) != 0">            
            <xsl:element name="PeakData">
                <xsl:attribute name="id" select="ckbk:normalizeVibrationIndex($outerIndex,$frequency[$outerIndex],$discardedNumber)"/>            
                <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=',$irintensity[$outerIndex])"/>
                <xsl:attribute name="peakShape" select="'sharp'"/> 
                <xsl:attribute name="model" select="concat('1.',ckbk:normalizeVibrationIndex($outerIndex,$frequency[$outerIndex],$discardedNumber))"/>
                <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>                
                <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                    <xsl:choose>
                        <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                            <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                    <xsl:choose>
                        <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                            <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>
                <xsl:attribute name="xMax" select="$xMaxValue"/>
                <xsl:attribute name="xMin" select="$xMinValue"/>
                <xsl:attribute name="yMax" select="1"/>
                <xsl:attribute name="yMin" select="0"/>            
            </xsl:element>
            <xsl:value-of select="$newline"/>    
        </xsl:if>    
    </xsl:for-each>
</xsl:element>  
<xsl:value-of select="ckbk:printTurbomolePeakTable($irintensity,$frequency, $frequencyNumber, $intensityNumber, $discardedNumber)"/>        
##END=
##END=$$ END of FILE</xsl:template>
  
    <!-- JCAMPDX format from Molcas formatted CML file -->
    <xsl:template name="frommolcas">
    <xsl:variable name="molecule" select="
        if(exists(//property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule)) then
        //property[@dictRef='cc:frequencies']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule
        else
        (//module[@dictRef='cc:initialization' and child::molecule])[last()]//molecule
        "/>
    <xsl:variable name="atomcount" select="count($molecule/atomArray/atom)"/>
    <xsl:variable name="bondcount" select="count($molecule/bondArray/bond)"/>
    
    <xsl:variable name="vibrations" select="//property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations']"/>
    <xsl:variable name="frequency" select="tokenize(replace($vibrations/array[@dictRef='cc:frequency'],'i','-'),'[\|\s+]')"/> <!-- Manage vibrations with complex numbers-->
    <xsl:variable name="elementType" select="$molecule/atomArray/atom/@elementType"/>
    <xsl:variable name="displacement" select="tokenize($vibrations/matrix[@dictRef='cc:displacement'],'[\s+]')"/>            
    <xsl:variable name="irintensity" select="
        if(exists(//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='vibrations']/array[@dictRef='cc:irintensity'])) 
        then
            tokenize(//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='vibrations']/array[@dictRef='cc:irintensity'],'\s+')
        else
            tokenize(replace(replace($vibrations/array[@dictRef='cc:frequency'],'i','-'), '[-.0-9]+', '0.0'), '\|')
           "/>
    <xsl:variable name="formula" select="$molecule/formula/@concise"/>##TITLE= Compound file, contains several data records
    <xsl:variable name="frequencyNumber" as="xs:double*">     
        <xsl:for-each select="$frequency">
            <xsl:if test="number(.) != 0">
                <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>
            </xsl:if>
        </xsl:for-each>        
    </xsl:variable>
##JCAMP-DX= 5.0
##DATA TYPE= LINK
##BLOCKS=1
##ORIGIN= ioChem-BD software
##OWNER= public domain
##TITLE= <xsl:choose><xsl:when test="exists($title) and $title != ''"><xsl:value-of select="$title"/></xsl:when><xsl:otherwise><xsl:text>N/A</xsl:text></xsl:otherwise></xsl:choose>
##JCAMP-DX= 5.00
##DATA TYPE= INFRARED SPECTRUM
##BLOCK_ID=1
##ORIGIN= ioChem-BD Quantum Repository software, more info at http://www.iochem-bg.org
##OWNER= Public domain
##DATE=<xsl:value-of  select="current-date()"/>
##TIME=<xsl:value-of  select="current-time()"/>
##SPECTROMETER/DATA SYSTEM= CMLv2.0 to JCAMP-MOL format conversion
##CAS REGISTRY NO=
##MOLFORM=<xsl:value-of select="$formula"/>
##$MODELS=
<xsl:element name="Models">
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="$title"/>
        <xsl:attribute name="type" select="'MOL'"/>
        <xsl:value-of select="$newline"/>
        <xsl:value-of select="$title"/>
        DSViewer    3D   0
        
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$molecule/atomArray/atom[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each> 
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($molecule/bondArray/bond[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>         
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END
    </xsl:element>
    <xsl:value-of select="$newline"/>
    <xsl:element name="ModelData">
        <xsl:attribute name="id" select="1"/>
        <xsl:attribute name="type" select="'XYZVIB'"/>
        <xsl:attribute name="baseModel" select="$title"/>
        <xsl:attribute name="vibrationScale" select="'1'"/>
        <xsl:value-of select="$newline"/>
        <xsl:for-each select="1 to count($frequency)">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), otherwise this fact will make jsmol fail to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="$outerIndex div 100"/>
                    </xsl:when>
                    <xsl:otherwise>0</xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:value-of select="$atomcount"/><xsl:value-of select="$newline"/>
            <xsl:value-of select="concat($outerIndex,'  ','Energy=',number($irintensity[$outerIndex]),'    ','Freq=',number($frequency[$outerIndex]) + $symmetrySuffix ,$newline)"/>
            <xsl:for-each select="1 to $atomcount">
                <xsl:variable name="innerIndex" select="."/>
                <xsl:variable name="atom" select="$molecule/atomArray/atom[$innerIndex]"/>
                <xsl:value-of select="ckbk:printVibrationLineWithAtomType($atom,$displacement, $outerIndex,$innerIndex,$atomcount)"/>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:element>    
    <xsl:value-of select="$newline"/>
</xsl:element> 
##$PEAKS=
<xsl:element name="Peaks">
    <xsl:attribute name="type" select="'IR'"/>
    <xsl:value-of select="$newline"/>
    <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
    <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>   
    <xsl:for-each select="1 to count($frequency)">
        <xsl:variable name="outerIndex" select="."/>
        <xsl:element name="PeakData">
            <xsl:attribute name="id" select="$outerIndex"/>            
            <xsl:attribute name="title" select="concat('X=',$frequency[$outerIndex],' Y=', number($irintensity[$outerIndex]))"/>
            <xsl:attribute name="peakShape" select="'sharp'"/> 
            <xsl:attribute name="model" select="concat('1.',$outerIndex)"/>
            <xsl:variable  name="xRange" select="ckbk:getPeakXGaps($outerIndex,$frequency,$firstX,$lastX)"/>
            <xsl:variable name="xMaxValue">  <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100 + 0.009 "/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) + $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:variable name="xMinValue"><!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
                <xsl:choose>
                    <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                        <xsl:value-of select="number($frequency[$outerIndex]) + $outerIndex div 100  - 0.01"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="format-number(number($frequency[$outerIndex]) - $xRange, '#0.000')"/>                    
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:variable>
            <xsl:attribute name="xMax" select="$xMaxValue"/>
            <xsl:attribute name="xMin" select="$xMinValue"/>
            <xsl:attribute name="yMax" select="1"/>
            <xsl:attribute name="yMin" select="0"/>            
        </xsl:element>
        <xsl:value-of select="$newline"/>        
    </xsl:for-each>
</xsl:element>  
    <xsl:value-of select="ckbk:printPeakTable($irintensity,$frequency)"/>        
##END=
##END=$$ END of FILE</xsl:template>


    <!-- Helper functions -->    
    
<xsl:function name="ckbk:justify">
        <xsl:param name="align" as="xs:string"/>
        <xsl:param name="length" as="xs:integer"/>
        <xsl:param name="value" as="xs:string"/>
        <xsl:variable name="blanks" select="'                              '"/><!--30 blank spaces will be enough by now-->        
        <xsl:if test="$align ='left'">
            <xsl:sequence select="substring(concat($value,$blanks),1,$length)"/>            
        </xsl:if>
        <xsl:if test="$align ='right'">
            <xsl:sequence select="substring(concat($blanks,$value), string-length($blanks) + string-length($value) - $length +1 ,$length)"/>
        </xsl:if>
    </xsl:function> 
    
<xsl:function name="ckbk:printAtom">
        <xsl:param name="atom" />
        <xsl:param name="index" as="xs:integer"/>
        
        <xsl:variable name="x3" select="ckbk:justify('right',10,string(format-number($atom/@x3,'#0.0000')))"/>
        <xsl:variable name="y3" select="ckbk:justify('right',10,string(format-number($atom/@y3,'#0.0000')))"/>
        <xsl:variable name="z3" select="ckbk:justify('right',10,string(format-number($atom/@z3,'#0.0000')))"/>
        <xsl:variable name="elementType" select="ckbk:justify('right',4,$atom/@elementType)"/>
        <xsl:variable name="others" select="string(' 0  0  0  0  0  0  0  0  0')" />
        <xsl:variable name="atomNumber" select="ckbk:justify('right',3,string($index))"/>
        <xsl:sequence select="concat($x3,$y3,$z3,$elementType,$others,$atomNumber,$newline)"/>                
    </xsl:function>

<xsl:function name="ckbk:printBond">
         <xsl:param name="firstAtom"/>
         <xsl:param name="secondAtom"/>
         <xsl:variable name="atom1" select="ckbk:justify('right',3,$firstAtom)"/>
         <xsl:variable name="atom2" select="ckbk:justify('right',3,$secondAtom)"/>
         <xsl:sequence select="concat($atom1,$atom2,'  1  0  0  0', $newline)"/>
     </xsl:function>

<xsl:function name="ckbk:printVibrationLineWithAtomType">
        <xsl:param name="atom"/>                
        <xsl:param name="displacement"/>
        <xsl:param name="outerIndex" as="xs:integer"/>
        <xsl:param name="innerIndex" as="xs:integer"/>
        <xsl:param name="atomCount"/>
        
        <xsl:variable name="atomType" select="ckbk:justify('left',2,$atom/@elementType)"/>
        <xsl:variable name="x3" select="ckbk:justify('right',12,string(format-number(number($atom/@x3),'#0.000000')))"/>
        <xsl:variable name="y3" select="ckbk:justify('right',12,string(format-number(number($atom/@y3),'#0.000000')))"/>
        <xsl:variable name="z3" select="ckbk:justify('right',12,string(format-number(number($atom/@z3),'#0.000000')))"/>
        
        <xsl:variable name="xi" select="ckbk:justify('right',12,string(format-number(number($displacement[ (($innerIndex - 1)* 3) + ($outerIndex -1 ) * ($atomCount * 3) + 1]),'#0.000000')))"/>
        <xsl:variable name="yi" select="ckbk:justify('right',12,string(format-number(number($displacement[ (($innerIndex - 1)* 3) + ($outerIndex -1 ) * ($atomCount * 3) + 2]),'#0.000000')))"/>
        <xsl:variable name="zi" select="ckbk:justify('right',12,string(format-number(number($displacement[ (($innerIndex - 1)* 3) + ($outerIndex -1 ) * ($atomCount * 3) + 3]),'#0.000000')))"/>        
        <xsl:value-of select="concat($atomType, $x3, $y3, $z3, $xi,$yi,$zi, $newline)"/>        
</xsl:function>

<xsl:function name="ckbk:printPeakTable">
        <xsl:param name="irintensity"/>
        <xsl:param name="frequency"/>
    
        <xsl:variable name="maxIntensity">
            <xsl:variable name="arrayMax">
                <xsl:call-template name="math:max">
                    <xsl:with-param name="nodes" select="$irintensity"/>            
                </xsl:call-template>
            </xsl:variable>
            <xsl:value-of select="if(number($arrayMax) = 0) then number('1.0') else $arrayMax"/>
            
        </xsl:variable>
        <xsl:variable name="xOffset" select="0.05 * number($frequency[count($frequency)]) - number($frequency[1])"/> <!-- 0.5% offset on X edges -->
        <xsl:variable name="firstX" select="number($frequency[1]) - $xOffset"/>
        <xsl:variable name="lastX" select="number($frequency[count($frequency)]) + $xOffset"/>                
##XUNITS= 1/CM
##YUNITS= TRANSMITTANCE
##XFACTOR=   1    
##YFACTOR=   1
##FIRSTX=  <xsl:value-of select="$firstX"/>    
##LASTX=   <xsl:value-of select="$lastX"/>    
##MAXY=   1
##MINY=   0
##NPOINTS=  <xsl:value-of select="count($frequency)"/>
##FIRSTY=   <xsl:value-of select="format-number(number($irintensity[1]) div number($maxIntensity),'0.000000')"/>
##PEAK TABLE= (X,Y..X,Y)
<xsl:for-each select="1 to count($frequency)">
    <xsl:variable name="outerIndex" select="."/>    
    <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
        <xsl:choose>
            <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                <xsl:value-of select="$outerIndex div 100"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>       
    <xsl:if test="(($outerIndex) mod 5 ) = 0"><xsl:value-of select="$newline"></xsl:value-of></xsl:if>
<xsl:value-of select="concat($blankspace,number($frequency[$outerIndex])+$symmetrySuffix,',',format-number(number($irintensity[$outerIndex]) div $maxIntensity,'0.000000'))"/>
</xsl:for-each>
</xsl:function>    

<xsl:function name="ckbk:getPeakXGaps">
    <xsl:param name="outerIndex" as="xs:integer"/>
    <xsl:param name="frequency"/>
    <xsl:param name="firstX" as="xs:double"/>
    <xsl:param name="lastX" as="xs:double"/>    
    <xsl:choose>
        <xsl:when test="$outerIndex = 1">            
            <xsl:variable name="array" as="element()*">
                <Item><xsl:value-of select="(number($frequency[$outerIndex]) - $firstX) div 2"/></Item>
                <xsl:if test="count($frequency) &gt; 1">
                    <Item><xsl:value-of select="(number($frequency[$outerIndex + 1]) - number($frequency[$outerIndex])) div 2"/></Item>
                </xsl:if>
            </xsl:variable>                       
            <xsl:sequence select="min($array)"/>
        </xsl:when>
        <xsl:otherwise>
            <xsl:choose>
                <xsl:when test="$outerIndex =  count($frequency)">
                    <xsl:variable name="array" as="element()*">
                        <Item><xsl:value-of select="(number($frequency[$outerIndex]) - number($frequency[$outerIndex - 1])) div 2"/></Item>
                        <Item><xsl:value-of select="($lastX - number($frequency[$outerIndex])) div 2"/></Item>                                     
                    </xsl:variable>                       
                    <xsl:sequence select="min($array)"/>                    
                </xsl:when>                                                    
                <xsl:otherwise>
                    <xsl:variable name="array" as="element()*">
                        <Item><xsl:value-of select="(number($frequency[$outerIndex]) - number($frequency[$outerIndex - 1])) div 2"/></Item>
                        <Item><xsl:value-of select="(number($frequency[$outerIndex + 1]) - number($frequency[$outerIndex])) div 2"/></Item>             
                    </xsl:variable>                       
                    <xsl:sequence select="min($array)"/>                    
                </xsl:otherwise>
            </xsl:choose>                    
        </xsl:otherwise>
    </xsl:choose>       
</xsl:function>

<xsl:function name="ckbk:printTurbomoleVibrationLineWithAtomType">
    <xsl:param name="freqcount"/>
    <xsl:param name="atom"/>                
    <xsl:param name="displacementx"/>
    <xsl:param name="displacementy"/>
    <xsl:param name="displacementz"/>
    <xsl:param name="outerIndex" as="xs:integer"/>
    <xsl:param name="innerIndex" as="xs:integer"/>
    
    <xsl:variable name="atomType" select="ckbk:justify('left',2,$atom/@elementType)"/>
    <xsl:variable name="x3" select="ckbk:justify('right',12,string(format-number(number($atom/@x3),'#0.000000')))"/>
    <xsl:variable name="y3" select="ckbk:justify('right',12,string(format-number(number($atom/@y3),'#0.000000')))"/>
    <xsl:variable name="z3" select="ckbk:justify('right',12,string(format-number(number($atom/@z3),'#0.000000')))"/>
    
    <xsl:variable name="displacementInx" select="$outerIndex + (($innerIndex -1) * $freqcount)"/>
    <xsl:variable name="xi" select="ckbk:justify('right',12,string(format-number( number($displacementx[$displacementInx]),'#0.000000')))"/>
    <xsl:variable name="yi" select="ckbk:justify('right',12,string(format-number( number($displacementy[$displacementInx]),'#0.000000')))"/>
    <xsl:variable name="zi" select="ckbk:justify('right',12,string(format-number( number($displacementz[$displacementInx]),'#0.000000')))"/>
    <xsl:value-of select="concat($atomType, $x3, $y3, $z3, $xi,$yi,$zi, $newline)"/>    
</xsl:function>
    
<xsl:function name="ckbk:printTurbomolePeakTable">
    <xsl:param name="irintensity"/>
    <xsl:param name="frequency"/>
    <xsl:param name="frequencyNumber"/>
    <xsl:param name="intensityNumber"/>
    <xsl:param name="numdiscarded"/>
    
    <xsl:variable name="maxIntensity" select="max($intensityNumber)"/>    
    <xsl:variable name="xOffset" select="abs(0.05 * max($frequencyNumber) - min($frequencyNumber))"/> <!-- 0.5% offset on X edges -->
    <xsl:variable name="firstX" select="min($frequencyNumber) - $xOffset"/>
    <xsl:variable name="lastX" select="max($frequencyNumber) + $xOffset"/>                
##XUNITS= 1/CM
##YUNITS= TRANSMITTANCE
##XFACTOR=   1    
##YFACTOR=   1
##FIRSTX=  <xsl:value-of select="$firstX"/>    
##LASTX=   <xsl:value-of select="$lastX"/>    
##MAXY=   1
##MINY=   0
##NPOINTS=  <xsl:value-of select="count($frequency) - $numdiscarded"/>
##FIRSTY=   <xsl:value-of select="format-number($intensityNumber[1] div $maxIntensity,'0.000000')"/>
##PEAK TABLE= (X,Y..X,Y)
<xsl:for-each select="1 to count($frequency)">
    <xsl:variable name="outerIndex" select="."/>
    <xsl:variable name="symmetrySuffix">    <!-- We'll append this numeric suffix on Freq label to avoid same value frequencies (due to symmetry), this would make jsmol fails to load the vibration-->
        <xsl:choose>
            <xsl:when test="$frequency[$outerIndex -1] = $frequency[$outerIndex] or $frequency[$outerIndex] = $frequency[$outerIndex + 1]">
                <xsl:value-of select="$outerIndex div 100"/>
            </xsl:when>
            <xsl:otherwise>0</xsl:otherwise>
        </xsl:choose>
    </xsl:variable>          
    <xsl:if test="number($frequency[$outerIndex]) != 0">
        <xsl:if test="(($outerIndex) mod 5 ) = 0"><xsl:value-of select="$newline"></xsl:value-of></xsl:if>
        <xsl:value-of select="concat($blankspace, number($frequency[$outerIndex]) + $symmetrySuffix,',',format-number(number($irintensity[$outerIndex]) div $maxIntensity,'0.000000'))"/>
    </xsl:if>    
</xsl:for-each>
</xsl:function>    
 
<xsl:function name="ckbk:normalizeVibrationIndex">
    <xsl:param name="currentIndex"/>
    <xsl:param name="currentFrequency"/>
    <xsl:param name="discardedIndexes"/>
    <xsl:if test="number($currentFrequency) &lt; 0"><xsl:value-of select="$currentIndex"/></xsl:if>
    <xsl:if test="number($currentFrequency) &gt; 0"><xsl:value-of select="$currentIndex - $discardedIndexes"/></xsl:if>
</xsl:function>
</xsl:stylesheet>
