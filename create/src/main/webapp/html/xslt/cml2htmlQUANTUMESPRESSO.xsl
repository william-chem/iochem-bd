<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:qex="http://www.iochem-bd.org/dictionary/qespresso/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs xd cml ckbk qex helper cmlx"
    version="2.0">

    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>February 07 , 2018</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>            
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:include href="helper/chemistry/qespresso.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>    
    <xsl:param name="jcampdxurl"/>    
    <xsl:param name="xyzurl"/>

    <!-- Environment module -->
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    
    <!-- Initialization module -->
    
    <xsl:variable name="modNames" select="//cml:module[@dictRef='cc:job']/module[@id='environment']/parameterList/parameter[@dictRef='cc:module']/scalar/text()"/>
    <xsl:variable name="calcParameter" select="replace(qex:getInputParameter(., 'CONTROL', 'calculation'), '&quot;','')"/>
    <xsl:variable name="calcType" select="qex:getCalcType($modNames,$calcParameter)"/>
  
    <xsl:variable name="otherComponents" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@id='otherComponents']"/>
    <xsl:variable name="userDefinedModules" select="$otherComponents/module[@dictRef='cc:userDefinedModule']"/>
    
    <xsl:variable name="species" select="tokenize(//cml:module[@cmlx:templateRef='species']/list[@cmlx:templateRef='species']/array[@dictRef='qex:specie'],'\s+')"/>
    <xsl:variable name="atomSpecies" select="tokenize((//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='qex:specie']/array)[1], '\s')"/>
    <xsl:variable name="specieToPseudopotential">
        <xsl:for-each select="$species">
            <xsl:variable name="specie" select="."/>            
            <xsl:variable name="outerIndex" select="position()"/>
            <xsl:element name="specie" namespace="http://www.xml-cml.org/schema">
                <xsl:attribute name="specie" select="$specie"/>
                <xsl:attribute name="basis" select="qex:getBasis($userDefinedModules/module[@cmlx:templateRef='pseudopotential' and child::scalar[@dictRef='cc:serial' and text() = $outerIndex]]/scalar[@dictRef='qex:pseudopotential'])"/>
                <xsl:attribute name="pseudofile" select="$userDefinedModules/module[@cmlx:templateRef='pseudopotential' and child::scalar[@dictRef='cc:serial' and text() = $outerIndex]]/scalar[@dictRef='qex:pseudofile']"/>
            </xsl:element>                        
        </xsl:for-each>
    </xsl:variable>       
    <xsl:variable name="pseudoFilenames" select="$userDefinedModules/module[@cmlx:templateRef='pseudopotential']/scalar[@dictRef='qex:pseudofile']/text()"/>
    
    
    <xsl:variable name="functionals" select="helper:trim(replace($otherComponents/module[@cmlx:templateRef='parameters']//scalar[@dictRef='cc:parameter' and text() = 'Exchange-correlation']/following-sibling::scalar[@dictRef='cc:value']/text(),'\(.*\)',''))"/>
    <xsl:variable name="functionalsFromFilenames" select="distinct-values(qex:getFunctionalsFromFilenames($pseudoFilenames))"/>    
    <xsl:variable name="functionalMethods" select="distinct-values(qex:getMethodsFromFilenames($pseudoFilenames))"/>
    <xsl:variable name="basis" select="distinct-values(qex:getBasis($userDefinedModules/module[@cmlx:templateRef='pseudopotential']/scalar[@dictRef='qex:pseudopotential']/text()))"/>
    
    <xsl:variable name="kpoints" select="$otherComponents/module[@cmlx:templateRef='kpoints']"/>
    <xsl:variable name="ldau" select="$otherComponents/module[@cmlx:templateRef='ldau']"/>    
    <xsl:variable name="parameters" select="$otherComponents/module[@cmlx:templateRef='parameters']/list"/>
    <xsl:variable name="pointGroup" select="$otherComponents/module[@cmlx:templateRef='point.group']/scalar[@dictRef='cc:pointgroup']" />
    
    <xsl:variable name="temperature" select="$otherComponents/module[@cmlx:templateRef='environ']//scalar[@dictRef='cc:parameter' and text() = 'static permittivity']/following-sibling::scalar[@dictRef='cc:value']"/>
    <xsl:variable name="pressure" select="$otherComponents/module[@cmlx:templateRef='environ']//scalar[@dictRef='cc:parameter' and text() = 'external pressure in input']/following-sibling::scalar[@dictRef='cc:value']"/>        

    <!-- Finalization module -->
    <xsl:variable name="frequencies" select="//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='frequencies']"/>
    <xsl:variable name="absorption" select="//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='absorptionspec']"/>

    
    <xsl:variable name="method" select="if(exists($absorption)) then 'TDDFT' else 'DFT'"/>
    

    <!-- Geometry -->    
    <xsl:variable name="energies" select="//module[@id='job']/module[@dictRef='cc:finalization']//property[@dictRef='cc:energies']/module[@cmlx:templateRef='energies']"/>
    <xsl:variable name="finalMolecule" select="//module[@dictRef='cc:finalization' and child::molecule]//molecule"/>

    <xsl:template match="/">    
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8"></meta>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>                                               
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>                        
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/exporting.js"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css"/>
                 
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">                   
                    $.blockUI();                    
                </script>
                <div id="container">                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Setttings section -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="settings"/>
                        </div>
                    </div>                    
                    
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                         
                            <div>
                               <xsl:call-template name="atomicCoordinates">
                                   <xsl:with-param name="molecule" select="$finalMolecule"/>
                               </xsl:call-template>
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->
                                        
                    <xsl:if test="exists($kpoints)">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <h3>MOLECULAR INFO</h3>
                                <xsl:call-template name="ldau"/>
                                <xsl:call-template name="pointGroup"/>
                                <xsl:call-template name="kpoints"/>                                
                            </div>
                        </div>    
                    </xsl:if>       
                    
                    <!-- Results -->
                    <div class="row bottom-buffer">
                        <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                            <xsl:variable name="job" select="."/>
                            <xsl:variable name="moduleName" select="$job/module[@id='environment']/parameterList/parameter[@dictRef='cc:module']"/>
                            <div class="col-md-12">                                                                                          
                                <div id="module-{generate-id($job)}">                                
                                    <h3>JOB <small><xsl:value-of select="$moduleName"/><xsl:text>   </xsl:text><a href="javascript:collapseModule('module-{generate-id(//module[@id='job'][1])}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(//module[@id='job'][1])}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                                    <xsl:variable name="finalOtherComponents" select="$job/module[@dictRef='cc:finalization']/module[@id='otherComponents']" />                                    
                                    <xsl:call-template name="force"> <xsl:with-param name="forces" select="$finalOtherComponents/module[@cmlx:templateRef='forces']"/> </xsl:call-template>                                    
                                    <xsl:call-template name="energy"> <xsl:with-param name="energy" select="$finalOtherComponents/module[@cmlx:templateRef='energies']"/> </xsl:call-template>
                                    <xsl:call-template name="magnetization"><xsl:with-param name="magnetizations" select="$finalOtherComponents/module[@cmlx:templateRef='magnetic']"/></xsl:call-template>
                                    <xsl:call-template name="eigenvalues"/>
                                    <xsl:call-template name="projwfc"><xsl:with-param name="section" select="$finalOtherComponents/module[@cmlx:templateRef='projwfc']"/></xsl:call-template>                                                                            
                                    <xsl:call-template name="frequencies"/>     
                                    <xsl:call-template name="absorptionspec" />
                                    <br/>
                                </div>
                                <xsl:call-template name="printLicense"/>
                            </div>
                        </xsl:for-each>
                    </div> 
                    <script type="text/javascript">
                        function expandModule(moduleID){                        
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });      
                            //Add download action to every dataTable                             
                            $('div.dataTables_wrapper').each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });
                            $.unblockUI();                             
                        });

                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }

                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                           $('.dataTable').each(function(index){
                             var oTable = $(this).dataTable();
                             $(oTable).css({ width: $(oTable).parent().width() });
                             oTable.fnAdjustColumnSizing();                           
                           });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
            </body>
        </html>
    </xsl:template>
    
    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="helper:calculateHillNotationFormula($finalMolecule/atomArray/atom)"/>
                </td>
            </tr>                               
            <tr>
                <td>Calculation type:</td>
                <td>
                    <xsl:value-of select="$calcType"/>                                        
                </td>
            </tr>
            <tr>
                <td>Method:</td>
                <td>
                    <xsl:value-of select="$method"/>                                        
                </td>
            </tr>
            <tr>
                <td>Functional:</td>
                <td><xsl:value-of select="if(exists($functionals)) then $functionals else $functionalsFromFilenames"/></td>
            </tr>
            <xsl:if test="exists($temperature)">                                  
                <tr>
                    <td>                                           
                        <xsl:text>Temperature:</xsl:text>
                    </td>                                   
                    <td>                                            
                        <xsl:value-of select="round-half-to-even($temperature/text(),2)"></xsl:value-of>   
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($temperature/@units)"/>
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="exists($pressure)">
                <tr>
                    <td>
                        <xsl:text>Pressure:</xsl:text>
                    </td>
                    <td>
                        <xsl:value-of select="round-half-to-even($pressure/text(),2)"></xsl:value-of>   
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($pressure/@units)"/>
                    </td>
                </tr>
            </xsl:if>
        </table>        
    </xsl:template>
    
    <!-- Settings -->
    <xsl:template name="settings">
        <h3>SETTINGS</h3>
        <div class="row bottom-buffer">
            <div class="col-md-5 col-sm-12">
                <table class="display compact" id="settings">
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th class="right">Value</th>
                            <th class="right">Units</th>
                        </tr>
                    </thead>
                    <tbody>
                        <xsl:for-each select="$parameters">
                            <tr>
                                <xsl:variable name="parameter" select="."/>
                                <td><xsl:value-of select="$parameter/scalar[@dictRef='cc:parameter']"/></td>
                                <td class="right"><xsl:value-of select="$parameter/scalar[@dictRef='cc:value']"/></td>
                                <td><xsl:value-of select="helper:printUnitSymbol($parameter/scalar[@dictRef='cc:value']/@units)"/></td>
                            </tr>
                        </xsl:for-each>
                    </tbody>
                </table>
                <script>
                    $(document).ready(function() {                                        
                    $('#settings').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    });
                </script>
            </div>                                 
        </div>
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">        
        <xsl:param name="molecule" />         
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">                        
                    Atomic coordinates [&#8491;]                                                 
                </h4>
            </div>
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row bottom-buffer">
                        <div class="col-lg-12 col-md-12 col-sm-12">                            
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h5>Cell parameters:</h5>
                                <table id="cellParameters" class="display">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>a = <xsl:value-of select="round-half-to-even($molecule/crystal/scalar[@title='a'],6)"/></td>
                                        </tr>
                                        <tr>
                                            <td>b = <xsl:value-of select="round-half-to-even($molecule/crystal/scalar[@title='b'],6)"/></td>
                                        </tr>
                                        <tr>
                                            <td>c = <xsl:value-of select="round-half-to-even($molecule/crystal/scalar[@title='c'],6)"/></td>
                                        </tr>
                                        <tr>
                                            <td>α = <xsl:value-of select="$molecule/crystal/scalar[@title='alpha']"/></td>
                                        </tr>
                                        <tr>
                                            <td>β = <xsl:value-of select="$molecule/crystal/scalar[@title='beta']"/></td>
                                        </tr>
                                        <tr>
                                            <td>γ = <xsl:value-of select="$molecule/crystal/scalar[@title='gamma']"/></td>                                  
                                        </tr>
                                    </tbody>
                                </table>    
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h5>Lattice vectors</h5>
                                <table id="latticeVectors" class="display">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:for-each select="(//module[@dictRef='cc:initialization']//module[@cmlx:templateRef='lattice'])[last()]/array">
                                            <tr>
                                                <xsl:for-each select="tokenize(.,'\s+')">
                                                    <td><xsl:value-of select="."/></td>                                                
                                                </xsl:for-each>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                </table>                                
                            </div>
                            <div class="col-md-12 col-sm-12">                                                                                    
                                <br/>
                                Coordinate type : 
                                <input type="radio" name="coordinateType" value="both" checked="checked" onclick="javascript:showCoordinatesTable()"/>Both 
                                <input type="radio" name="coordinateType" value="cartesian" onclick="javascript:showCoordinatesTable()"/>Cartesian 
                                <input type="radio" name="coordinateType" value="fractional" onclick="javascript:showCoordinatesTable()"/>Fractional
                                <div class="row">
                                    <div id="atomicCoordinatesDiv" class="col-md-12">
                                        <table id="atomicCoordinates" class="display">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2"/>
                                                    <th rowspan="2"/>
                                                    <th colspan="3" style="text-align:center">Cartesian coordinates</th>
                                                    <th colspan="3" style="text-align:center">Fractional coordinates</th>
                                                    <th rowspan="2" style="text-align:center">Pseudopotential</th>
                                                </tr>
                                                <tr>
                                                    <th style="text-align:right">x</th>
                                                    <th style="text-align:right">y</th>
                                                    <th style="text-align:right">z</th>
                                                    <th style="text-align:right">x</th>
                                                    <th style="text-align:right">y</th>
                                                    <th style="text-align:right">z</th>
                                                </tr>
                                            </thead>
                                        </table>                                                
                                    </div>
                                    <div id="cartesianCoordinatesTypeFilteredDiv" class="col-md-6 col-sm-12">
                                        <table id="cartesianCoordinatesTypeFiltered" class="display compact">
                                            <thead>                                         
                                                <tr>
                                                    <th/>
                                                    <th/>
                                                    <th style="text-align:right">x</th>
                                                    <th style="text-align:right">y</th>
                                                    <th style="text-align:right">z</th>
                                                    <th style="text-align:center">Basis</th>
                                                </tr>
                                            </thead>
                                        </table>      
                                    </div>
                                    <div id="crystalCoordinatesTypeFilteredDiv" class="col-md-6 col-sm-12">
                                        <table id="crystalCoordinatesTypeFiltered" class="display compact">
                                            <thead>                                         
                                                <tr>
                                                    <th/>
                                                    <th/>
                                                    <th style="text-align:right">x</th>
                                                    <th style="text-align:right">y</th>
                                                    <th style="text-align:right">z</th>
                                                    <th style="text-align:center">Basis</th>
                                                </tr>
                                            </thead>
                                        </table>      
                                    </div>
                                </div>
                                <style type="text/css">
                                    .cellpaddingleft {
                                        padding-left: 20px !important;                                   
                                    }                                
                                </style>    
                               
                                <script type="text/javascript">                                    
                                    var molecular_data = [ 
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">
                                        <xsl:variable name="elementType" select="./@elementType"/>
                                        <xsl:variable name="id" select="@id"/>
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="specie" select="$atomSpecies[$outerIndex]"/>
                                        <xsl:variable name="basis" select="$specieToPseudopotential/specie[@specie = $atomSpecies[$outerIndex]]/@basis"/>
                                        <xsl:variable name="pseudo" select="$specieToPseudopotential/specie[@specie = $atomSpecies[$outerIndex]]/@pseudofile"/>                                        
                                        [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3, '#0.0000')"/>","<xsl:value-of select="format-number(@y3, '#0.0000')"/>","<xsl:value-of select="format-number(@z3, '#0.0000')"/>","<xsl:value-of select="format-number(@xFract, '#0.0000')"/>","<xsl:value-of select="format-number(@yFract, '#0.0000')"/>","<xsl:value-of select="format-number(@zFract, '#0.0000')"/>","<xsl:value-of select="$specie"/> / <xsl:value-of select="$basis"/> / <xsl:value-of select="$pseudo"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if></xsl:for-each>];
                                    
                                    var molecular_data_cartesian = []; 
                                    var molecular_data_crystal = [];

                                    for (var i = 0; i &lt; molecular_data.length; i++){ 
                                        molecular_data_cartesian[i]= molecular_data[i].slice(0)
                                        molecular_data_cartesian[i].splice(5, 3);
                                        molecular_data_crystal[i]= molecular_data[i].slice(0)
                                        molecular_data_crystal[i].splice(2, 3);
                                    }
                                       
                                    $(document).ready(function() {                                        
                                        buildLatticeVectorsTable();
                                        buildCellParametersTable();                                      
                                        buildAtomicCoordinatesTable();
                                        buildCartesianCoordinatesTable();
                                        buildCrystalCoordinatesTable();
                                        showCoordinatesTable();
                                    });

                                    function buildLatticeVectorsTable() {
                                        $('#latticeVectors').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    }
                                    
                                    function buildCellParametersTable() {
                                        $('#cellParameters').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    }
                                                                       
                                    function buildAtomicCoordinatesTable(){
                                        $('table#atomicCoordinates').dataTable( {
                                            "aaData": molecular_data,
                                            "aoColumns": [                                          
                                                {"sTitle":""},
                                                {"sTitle":"Atom", "sClass": "left"},
                                                {"sTitle":"x", "sClass": "right"},
                                                {"sTitle":"y", "sClass": "right"},
                                                {"sTitle":"z", "sClass": "right"},                                         
                                                {"sTitle":"u", "sClass": "right"},
                                                {"sTitle":"v", "sClass": "right"},
                                                {"sTitle":"w", "sClass": "right"},                                          
                                                {"sTitle":"Specie / Basis / Pseudopotential", sClass: "left cellpaddingleft"}],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "bDestroy": true
                                        });
                                    }
                                    
                                    function buildCartesianCoordinatesTable(){
                                        $('table#cartesianCoordinatesTypeFiltered').dataTable( {
                                            "aaData": molecular_data_cartesian,
                                            "aoColumns": [                                          
                                                {"sTitle":""},
                                                {"sTitle":"Atom", "sClass": "left"},                                                                                      
                                                {"sTitle":"x", "sClass": "right"},
                                                {"sTitle":"y", "sClass": "right"},
                                                {"sTitle":"z", "sClass": "right"},                                          
                                                {"sTitle":"Specie / Basis / Pseudopotential", sClass: "left cellpaddingleft"}],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "bDestroy": true
                                        });
                                    }  
                                    
                                    function buildCrystalCoordinatesTable(){
                                        $('table#crystalCoordinatesTypeFiltered').dataTable( {
                                            "aaData": molecular_data_crystal,
                                            "aoColumns": [                                          
                                                {"sTitle":""},
                                                {"sTitle":"Atom", "sClass": "left"},                                                                                      
                                                {"sTitle":"u", "sClass": "right"},
                                                {"sTitle":"v", "sClass": "right"},
                                                {"sTitle":"w", "sClass": "right"},                                          
                                                {"sTitle":"Specie / Basis / Pseudopotential", sClass: "left cellpaddingleft"}],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "bDestroy": true
                                        });
                                    }  

                                    function showCoordinatesTable(){
                                        if($("input:radio[name ='coordinateType']:checked").val() == 'both'){                                    
                                            $("div#cartesianCoordinatesTypeFilteredDiv").hide();
                                            $("div#crystalCoordinatesTypeFilteredDiv").hide();
                                            $("div#atomicCoordinatesDiv").show();
                                            
                                            oTable = $('table#atomicCoordinates').dataTable();
                                            $(oTable).css({ width: $(oTable).parent().width() });
                                            oTable.fnAdjustColumnSizing();                                            
                                        }else if($("input:radio[name ='coordinateType']:checked").val() == 'cartesian'){
                                            $("div#atomicCoordinatesDiv").hide();                                            
                                            $("div#crystalCoordinatesTypeFilteredDiv").hide();
                                            $("div#cartesianCoordinatesTypeFilteredDiv").show();
                                            
                                            oTable = $('table#cartesianCoordinatesTypeFiltered').dataTable();
                                            $(oTable).css({ width: $(oTable).parent().width() });
                                            oTable.fnAdjustColumnSizing();
                                        }else if($("input:radio[name ='coordinateType']:checked").val() == 'fractional'){                                        
                                            $("div#atomicCoordinatesDiv").hide();
                                            $("div#cartesianCoordinatesTypeFilteredDiv").hide();
                                            $("div#crystalCoordinatesTypeFilteredDiv").show();
                                            
                                            oTable = $('table#crystalCoordinatesTypeFiltered').dataTable();
                                            $(oTable).css({ width: $(oTable).parent().width() });
                                            oTable.fnAdjustColumnSizing();
                                        }
                                    }
                            </script>  
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>  
    
    <!-- kpoint list -->       
    <xsl:template name="kpoints">
        <xsl:if test="exists($kpoints)">            
            <xsl:variable name="alat" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']//module[@id='otherComponents']/module[@cmlx:templateRef='parameters']/list/scalar[@dictRef='cc:parameter' and text() = 'lattice parameter (alat)']/following-sibling::scalar[@dictRef='cc:value']"/>       
            <xsl:variable name="twopi_alat2angstrom" select="(0.529177249) div (2 * 3.14159265359 * $alat)"/>
            <xsl:variable name="subdivisionN" select="tokenize($kpoints/array[@dictRef='qex:subdivisionN'],'\s+')"/>
            <xsl:variable name="shiftS" select="tokenize($kpoints/array[@dictRef='qex:shiftS'],'\s+')"/>
                        
            <xsl:variable name="kpointCrystalNumber" select="$kpoints/matrix[@dictRef='qex:kpointlist']/@rows"/>
            <xsl:variable name="kpointCartesian" select="tokenize($kpoints/matrix[@dictRef='qex:kpointlist.cartesian'],'\s+')"/>        
            
            <xsl:variable name="kpointCartesianNumber" select="$kpoints/matrix[@dictRef='qex:kpointlist.cartesian']/@rows"/>
            <xsl:variable name="kpointCrystal" select="tokenize($kpoints/matrix[@dictRef='qex:kpointlist'],'\s+')"/>
            
            <xsl:variable name="kpointWeight" select="tokenize($kpoints/array[@dictRef='qex:kpointweight'][last()],'\s+')"/>
            <div class="row bottom-buffer">
                <div class="col-md-12">
                    <h4>Kpoint list</h4>               
                    <p>Scheme - <xsl:value-of select="$kpoints/scalar[@dictRef='qex:meshScheme']"/></p>
                    <table  class="display" id="kpointList">
                        <tbody>
                            <tr>
                                <xsl:for-each select="1 to count($subdivisionN)"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$subdivisionN[$outerIndex]"/></td></xsl:for-each>                                                
                            </tr>
                            <tr>
                                <xsl:for-each select="1 to count($shiftS)"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$shiftS[$outerIndex]"/></td></xsl:for-each>
                            </tr>
                        </tbody>                                        
                    </table>            
                </div>
                
                <div class="col-md-6 col-sm-12">
                    <table  class="display" id="kpointListCartesian">
                        <thead>
                            <tr>
                                <th colspan="4">Cartesian coordinates</th>
                            </tr>
                            <tr>
                                <th class="right">x</th>
                                <th class="right">y</th>
                                <th class="right">z</th>
                                <th class="right">Weight</th>
                            </tr>
                        </thead>
                        <tbody>
                            <xsl:for-each select="1 to $kpointCartesianNumber">
                                <xsl:variable name="outerIndex" select="."/>
                                <tr>
                                    <td class="right"><xsl:value-of select="format-number($twopi_alat2angstrom * number($kpointCartesian[3 * ($outerIndex - 1) + 1]),'#0.0000000')"/></td>
                                    <td class="right"><xsl:value-of select="format-number($twopi_alat2angstrom * number($kpointCartesian[3 * ($outerIndex - 1) + 2]),'#0.0000000')"/></td>
                                    <td class="right"><xsl:value-of select="format-number($twopi_alat2angstrom * number($kpointCartesian[3 * ($outerIndex - 1) + 3]),'#0.0000000')"/></td>
                                    <td class="right"><xsl:value-of select="$kpointWeight[$outerIndex]"/></td>
                                </tr>
                            </xsl:for-each>                                                
                        </tbody>                                        
                    </table>
                </div>
                <div class="col-md-6 col-sm-12">
                    <table  class="display" id="kpointListCrystal">
                        <thead>
                            <tr>
                                <th colspan="4">Crystal coordinates</th>
                            </tr>
                            <tr>
                                <th class="right">x</th>
                                <th class="right">y</th>
                                <th class="right">z</th>
                                <th class="right">Weight</th>                                
                            </tr>                        
                        </thead>
                        <tbody>
                            <xsl:for-each select="1 to $kpointCrystalNumber">
                                <xsl:variable name="outerIndex" select="."/>
                                <tr>
                                    <td class="right"><xsl:value-of select="$kpointCrystal[3 * ($outerIndex - 1) + 1]"/></td>
                                    <td class="right"><xsl:value-of select="$kpointCartesian[3 * ($outerIndex - 1) + 2]"/></td>
                                    <td class="right"><xsl:value-of select="$kpointCartesian[3 * ($outerIndex - 1) + 3]"/></td>
                                    <td class="right"><xsl:value-of select="$kpointWeight[$outerIndex]"/></td>
                                </tr>
                            </xsl:for-each>                                                
                        </tbody>                                        
                    </table>
                </div>
                
                <script type="text/javascript">                
                    $(document).ready(function() {                                        
                    $('#kpointListCartesian').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    $('#kpointListCrystal').dataTable({
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    });
                    });
                </script>                
            </div>    
        </xsl:if>        
    </xsl:template>  
    
    <!-- LDA+U table -->
    <xsl:template name="ldau">
        <xsl:if test="exists($ldau)">
            <xsl:variable name="ldauSpecie" select="tokenize($ldau//array[@dictRef='qex:specie'],'\s+')"/>
            <xsl:variable name="ldauL" select="tokenize($ldau//array[@dictRef='qex:l'],'\s+')"/>
            <xsl:variable name="ldauU" select="tokenize($ldau//array[@dictRef='qex:u'],'\s+')"/>
            <xsl:variable name="ldauAlpha" select="tokenize($ldau//array[@dictRef='qex:alpha'],'\s+')"/>
            <xsl:variable name="ldauJ0" select="tokenize($ldau//array[@dictRef='qex:j0'],'\s+')"/>
            <xsl:variable name="ldauBeta" select="tokenize($ldau//array[@dictRef='qex:beta'],'\s+')"/>
            
            <div class="row bottom-buffer">
                <div class="col-md-6">
                    <h4>LDA+U calculation (eV)</h4>                                        
                    <table class="display" id="ldau">
                        <thead>
                            <tr>
                                <th>atomic species</th>
                                <th>L</th>
                                <th>U</th>
                                <th>Alpha</th>
                                <th>J0</th>
                                <th>Beta</th>
                            </tr>
                        </thead>                        
                        <tbody>
                            <xsl:for-each select="1 to count($ldauSpecie)">
                                <xsl:variable name="outerIndex" select="."/>
                                <tr>
                                    <td><xsl:value-of select="$ldauSpecie[$outerIndex]"/></td>
                                    <td><xsl:value-of select="$ldauL[$outerIndex]"/></td>
                                    <td><xsl:value-of select="$ldauU[$outerIndex]"/></td>
                                    <td><xsl:value-of select="$ldauAlpha[$outerIndex]"/></td>
                                    <td><xsl:value-of select="$ldauJ0[$outerIndex]"/></td>                                   
                                    <td><xsl:value-of select="$ldauBeta[$outerIndex]"/></td>
                                </tr>
                            </xsl:for-each>                               
                        </tbody>                                        
                    </table>   
                    <script type="text/javascript">                
                        $(document).ready(function() {                                        
                            $('#ldau').dataTable({
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                            });                        
                        });
                    </script>            
                </div>               
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Point group line -->
    <xsl:template name="pointGroup">
        <xsl:if test="exists($pointGroup)">                       
            <div class="row bottom-buffer">
                <div class="col-md-6">
                    <p>Point group <xsl:value-of select="$pointGroup"/></p>                                        
                </div>
            </div>    
        </xsl:if>
    </xsl:template>
    
    <!-- Forces section -->
    <xsl:template name="force">
        <xsl:param name="forces"/>        
        <xsl:if test="exists($forces)">
            <xsl:variable name="force" select="$forces/scalar[@dictRef='cc:force']"/>
            <xsl:variable name="scfCorrection" select="$forces/scalar[@dictRef='qex:scfCorrection']"/>
            <xsl:variable name="dispEnergy" select="$forces/scalar[@dictRef='qex:dispenergy']"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#forces-{generate-id($forces)}" style="cursor: pointer; cursor: hand;" >
                    <h4 class="panel-title">                        
                        Forces                           
                    </h4>
                </div>
                <div id="forces-{generate-id($forces)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-4 col-sm-12">

                                    <table class="display" id="forcesT-{generate-id($forces)}">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>                                                
                                            </tr>
                                        </thead>                        
                                        <tbody>
                                            <xsl:if test="exists($force)">
                                                <tr>
                                                    <td>Total force</td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even($force, 6)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($force/@units)"/></td>
                                                </tr>
                                            </xsl:if>                     
                                            <xsl:if test="exists($scfCorrection)">
                                                <tr>
                                                    <td>Total SCF correction</td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even($scfCorrection, 6)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($scfCorrection/@units)"/></td>
                                                </tr>
                                            </xsl:if>              
                                            <xsl:if test="exists($dispEnergy)">
                                                <tr>
                                                    <td>Total Dispersion Force</td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even($dispEnergy, 6)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($dispEnergy/@units)"/></td>
                                                </tr>
                                            </xsl:if>              
                                        </tbody>                                        
                                    </table>   
                                    <script type="text/javascript">                
                                        $(document).ready(function() {                                        
                                            $('#forcesT-<xsl:value-of select="generate-id($forces)"/>').dataTable({
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false
                                            });                        
                                        });
                                    </script>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>                
    </xsl:template>
    
    <!-- Final energy -->
    <xsl:template name="energy">      
        <xsl:param name="energy"/>
        <xsl:if test="exists($energy)">
            <xsl:variable name="energyLabels">
                <variable name="qex:fermiener" label="Fermi energy"/>
                <variable name="qex:totalener" label="Total energy"/>
                <variable name="qex:harrisfoulkes" label="Harris-Foulkes estimate"/>
                <variable name="qex:sscfaccuracy" label="Estimated scf accuracy"/>
                <variable name="qex:oneelec" label="One-electron contribution"/>
                <variable name="qex:hartee" label="Hartree contribution"/>
                <variable name="qex:xc" label="XC contribution"/>
                <variable name="qex:ewald" label="Ewald contribution"/>
                <variable name="qex:dispcorr" label="Dispersion Correction"/>
                <variable name="qex:hubbardener" label="Hubbard energy"/>
                <variable name="qex:smearing" label="Smearing contrib. (-TS)"/>
                <variable name="qex:totalmag" label="Total magnetization"/>
                <variable name="cc:absolutemag" label="Absolute magnetization"/>
            </xsl:variable>            
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#energies-{generate-id($energy)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Energies 
                    </h4>
                </div>
                <div id="energies-{generate-id($energy)}" class="panel-collapse collapse show">
                    <div class="panel-body">
                        <div class="row bottom-buffer">                           
                            <div class="col-md-4 col-sm-12">
                                <table class="display" id="energiesT-{generate-id($energy)}">                        
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>                                                           
                                    </thead>                       
                                    <tbody>
                                        <xsl:for-each select="$energy/scalar">
                                            <xsl:variable name="energyField" select="."/>
                                            <tr>
                                                <td><xsl:value-of select="$energyLabels/*:variable[@name=$energyField/@dictRef]/@label"/></td>
                                                <td class="right"><xsl:value-of select="$energyField"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($energyField/@units)"/></td>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                </table>
                                <script type="text/javascript">                
                                    $(document).ready(function() {                                        
                                        $('#energiesT-<xsl:value-of select="generate-id($energy)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });                        
                                    });
                                </script>
                            </div>   
                            <xsl:if test="$calcType = $qex:GeometryOptimization and count(//module[@cmlx:templateRef='energies'] &gt; 1) ">                                
                                <div class="col-md-8 col-sm-12">
                                    <div id="iterationEnergyContainer" />  
                                    <script type="text/javascript">
                                        <xsl:variable name="finalEnergies" select="//module[@cmlx:templateRef='energies']" />
                                        var energies = [<xsl:for-each select="1 to count($finalEnergies)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$finalEnergies[$outerIndex]/scalar[@dictRef='qex:totalener'] * 0.073498617649"/><xsl:if test="$outerIndex &lt; count($finalEnergies)">,</xsl:if></xsl:for-each>];
                                        var indexes = [<xsl:for-each select="1 to count($finalEnergies)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="$outerIndex"/><xsl:if test="$outerIndex &lt; count($finalEnergies)">,</xsl:if></xsl:for-each>];
                                        $(document).ready(function() {
                                        buildIterationEnergyGraph();
                                        });
                                        <xsl:text disable-output-escaping="yes">
                                                <![CDATA[
                                                    function buildIterationEnergyGraph() {
                                                        var data = new Array();
                                                        var serie = {
                                                               x : indexes,
                                                               y : energies,                                                       
                                                               mode: 'lines+markers'                                                      
                                                        };
                                                        data.push(serie); 
                                                    
                                                        var xOffset = 0.25;
                                                        var yOffset = (Math.max(...data[0].y) - Math.min(...data[0].y)) / 10;
                                                    
                                                        var layout = { 
                                                              height: 400,
                                                              margin: {
                                                                t: 40, r: 40, b: 40, pad: 0
                                                              },
                                                              modebar: {
                                                                orientation: 'v' 
                                                              },
                                                              title: 'Geometric Optimization',                                                             
                                                              xaxis: {
                                                                  title: {text: '# Iterations'},
                                                                  showgrid: true,
                                                                  showline: true,
                                                                  automargin: true,
                                                                  zeroline: true,
                                                                  tickmode: 'auto',
                                                                  range: [Math.min(...data[0].x) - xOffset, Math.max(...data[0].x) + xOffset],
                                                                  tickformat:',d',
                                                                  fixedrange : true
                                                              },                                                              
                                                              yaxis: {
                                                                   title: { text: 'Total energy / Ry'},
                                                                   showgrid: true,
                                                                   showline: true,
                                                                   automargin: true,
                                                                   range: [Math.min(...data[0].y) - yOffset, Math.max(...data[0].y) + yOffset],
                                                                   tickmode: 'auto',
                                                                   nticks : 10,
                                                                   separatethousands : false,
                                                                   fixedrange : true
                                                              },
                                                              separators: ','
                                                        };
                                                        Plotly.newPlot('iterationEnergyContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                                    }
                                                         
                                                ]]>
                                            </xsl:text>
                                    </script>                                      
                                </div>  
                            </xsl:if> 
                        </div>
                    </div>                    
                </div>
            </div>
            
        </xsl:if> 
    </xsl:template>
  
    <!-- Magnetization -->    
    <xsl:template name="magnetization">
        <xsl:param name="magnetizations"/>
        <xsl:if test="exists($magnetizations)">
            <xsl:variable name="serial" select="tokenize($magnetizations/array[@dictRef='cc:serial'],'\s+')"/>
            <xsl:variable name="charge" select="tokenize($magnetizations/array[@dictRef='qex:charge'],'\s+')"/>
            <xsl:variable name="magn" select="tokenize($magnetizations/array[@dictRef='qex:magn'],'\s+')"/>
            <xsl:variable name="constr" select="tokenize($magnetizations/array[@dictRef='qex:constr'],'\s+')"/>            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#magnetization-{generate-id($magnetizations)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        Magnetic moment per site
                    </h4>
                </div>
                <div id="magnetization-{generate-id($magnetizations)}" class="panel-collapse collapse">
                    <div class="panel-body">                   
                        <div class="row bottom-buffer">
                            <div class="col-md-5 col-sm-12">
                                <table class="display" id="magnetizationT-{generate-id($magnetizations)}">
                                    
                                </table>
                                
                                
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('#magnetizationT-<xsl:value-of select="generate-id($magnetizations)"/>').dataTable({
                                    "aaData":  [ <xsl:for-each select="1 to count($serial)"> <xsl:variable name="outerIndex" select="."/>[ <xsl:value-of select="$serial[$outerIndex]"/>,"<xsl:value-of select="$finalMolecule/atomArray/atom[$outerIndex]/@elementType"/>", "<xsl:value-of select="$charge[$outerIndex]"/>", "<xsl:value-of select="$magn[$outerIndex]"/>", "<xsl:value-of select="$constr[$outerIndex]"/>"  ]<xsl:if test="$outerIndex != count($serial)">,</xsl:if> </xsl:for-each> ],
                                    "aoColumns": [ { "sTitle": "Atom"},{"sTitle": "", "sClass": "left" },{"sTitle": "Charge", "sClass": "right" },{"sTitle": "Magn", "sClass": "right" },{"sTitle": "Constr", "sClass": "right" }], 
                                            "bFilter": false,
                                            "bPaginate": true,
                                            "bSort": false,
                                            "bInfo": true,
                                            "bDestroy": true
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Additional modules section -->
    <xsl:template name="projwfc">
        <xsl:param name="section"/>
        <xsl:if test="exists($section)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#projwfc-{generate-id($section)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Projects wavefunctions onto orthogonalized atomic wavefunctions
                    </h4>
                </div>
                <div id="projwfc-{generate-id($section)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                        <xsl:variable name="kpointNum" select="count($section/list[@cmlx:templateRef='kpoint'])"/>
                                        <div class="form-inline">
                                            <div class="form-group mb-6" style="width: 100%">
                                                <h4>Kpoints</h4>
                                                <label for="projwfcKpointsList">Choose one:</label>
                                                <select id="projwfcKpointsList" class="form-control" onchange="javascript:buildKpointEnergiesTable()" style="width: 80%;">
                                                    <option value="-">-</option>
                                                    <xsl:for-each select="1 to $kpointNum">
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <option value="{$outerIndex}"><xsl:value-of select="$outerIndex"/></option>
                                                    </xsl:for-each>
                                                </select>
                                            </div>
                                        </div>                                                                                                               
                                        <script type="text/javascript">
                                            <xsl:for-each select="$section/list[@cmlx:templateRef='kpoint']">
                                                <xsl:variable name="kpoint" select="."/>
                                                <xsl:variable name="outerIndex" select="position()"/>                                      
                                                <xsl:variable name="coords" select="tokenize($kpoint/array[@dictRef='cc:coord'], '\s+')" />
                                                <xsl:variable name="level" select="tokenize($kpoint/array[@dictRef='qex:level'], '\s+')" />
                                                <xsl:variable name="energies" select="tokenize($kpoint/array[@dictRef='cc:energy'], '\s+')" />
                                                var kpointCoord<xsl:value-of select="$outerIndex"/>= [ <xsl:value-of select="$coords[1]"/>, <xsl:value-of select="$coords[2]"/>, <xsl:value-of select="$coords[3]"/>]; 
                                                var kpoint<xsl:value-of select="$outerIndex"/>= [<xsl:for-each select="1 to count($level)"><xsl:variable name="innerIndex" select="."/>[<xsl:value-of select="$level[$innerIndex]"/>, <xsl:value-of select="$energies[$innerIndex]"/>]<xsl:if test="$innerIndex &lt; count($energies)">,</xsl:if></xsl:for-each>];
                                            </xsl:for-each>
                                        </script>
                                        <table class="display compact" id="kpointcoordsT-{generate-id($section)}">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                        </table>
                                        <br/>
                                        <table class="display compact" id="kpointenergiesT-{generate-id($section)}"></table>
                                    </div>                                    
                                    
                                <script type="text/javascript">
                                    var kpointTable;
                                    var kpointCoordsTable;
                                    function buildKpointEnergiesTable(){
                                         var index = $('#projwfcKpointsList').val();                                         
                                         var kpointEnergiesId = 'table#kpointenergiesT-<xsl:value-of select="generate-id($section)"/>';
                                         var kpointEnergiesTable = 'kpointenergiesT-<xsl:value-of select="generate-id($section)"/>';
                                         
                                         if(index == '-'){
                                            $('table#kpointcoordsT-<xsl:value-of select="generate-id($section)"/>').hide();
                                            $('table#kpointenergiesT-<xsl:value-of select="generate-id($section)"/>').hide();
                                            $('#downloadTable'+ kpointEnergiesTable).remove();
                                            kpointCoordsTable.destroy();
                                            kpointTable.destroy();
                                         }else{
                                            $('#downloadTable'+ kpointEnergiesTable).remove();
                                            $('table#kpointenergiesT-<xsl:value-of select="generate-id($section)"/>').show();
                                            kpointCoordsTable = $('table#kpointcoordsT-<xsl:value-of select="generate-id($section)"/>').DataTable({
                                                "aaData":  [ [((window['kpointCoord' + index])[0]).toFixed(10), ((window['kpointCoord' + index])[1]).toFixed(10), ((window['kpointCoord' + index])[2]).toFixed(10)]],
                                                "aoColumns": [ { "sTitle": "", "sClass": "right"},{"sTitle": "", "sClass": "right"},{"sTitle": "", "sClass": "right"}],
                                                "bFilter": false,                                            
                                                "bSort": false,
                                                "bPaginate": false,
                                                "bInfo": false,
                                                "bDestroy": true
                                            });
                                         
                                            $('table#kpointenergiesT-<xsl:value-of select="generate-id($section)"/>').show();  
                                            kpointTable = $(kpointEnergiesId).DataTable({
                                                "aaData":  window['kpoint' + index],
                                                "aoColumns": [ { "sTitle": "Level"},{"sTitle": "Energies", "sClass": "right" }], 
                                                "bFilter": false,
                                                "bPaginate": true,
                                                "bSort": false,
                                                "bInfo": true,
                                                "bDestroy": true
                                            });
                                            
                                            var jsTable = "javascript:showDownloadOptions('" + kpointEnergiesTable + "');"
                                            $('<div id="downloadTable'+ kpointEnergiesTable + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + kpointEnergiesTable +'_wrapper');
                                         }
                                         
                                    }
                                </script>
                                </div>
                                <!-- Print Lowdin charges -->
                                <xsl:variable name="lowdin" select="$section/list[@dictRef='lowdin']"/>
                                <xsl:if test="exists($lowdin)">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12">
                                            <h4>Lowdin charges</h4>
                                            <xsl:variable name="serial" select="tokenize($lowdin/array[@dictRef='cc:serial'],'\s+')" />
                                            <xsl:variable name="charge" select="tokenize($lowdin/array[@dictRef='cc:charge'],'\s+')" />
                                            <xsl:variable name="charges" select="tokenize($lowdin/array[@dictRef='cc:charges'],'\s+')" />
                                            <xsl:variable name="chargep" select="tokenize($lowdin/array[@dictRef='cc:chargep'],'\s+')" />
                                            <xsl:variable name="charged" select="tokenize($lowdin/array[@dictRef='cc:charged'],'\s+')" />
                                            <xsl:variable name="polarization" select="tokenize($lowdin/array[@dictRef='qex:polarization'],'\s+')" />
                                            <xsl:variable name="polarizations" select="tokenize($lowdin/array[@dictRef='qex:polarizations'],'\s+')" />
                                            <xsl:variable name="polarizationp" select="tokenize($lowdin/array[@dictRef='qex:polarizationp'],'\s+')" />
                                            <xsl:variable name="polarizationd" select="tokenize($lowdin/array[@dictRef='qex:polarizationd'],'\s+')" />

                                            <table class="display" id="lowdinchargesT-{generate-id($lowdin)}">
                                                <thead>                                                    
                                                    <tr>
                                                        <th rowspan="2">Atom #</th>
                                                        <th colspan="4" style="text-align:center">Charges</th>
                                                        <th colspan="4" style="text-align:center">Polarization</th>
                                                    </tr>
                                                    <tr>                                                        
                                                        <th style="text-align:right">Total</th>
                                                        <th style="text-align:right">s</th>
                                                        <th style="text-align:right">p</th>
                                                        <th style="text-align:right">d</th>
                                                        <th style="text-align:right">Total</th>
                                                        <th style="text-align:right">s</th>
                                                        <th style="text-align:right">p</th>
                                                        <th style="text-align:right">d</th>                                                        
                                                    </tr>
                                                </thead>
                                            </table>

                                            <script type="text/javascript">                                                
                                                var lowdin = [<xsl:for-each select="1 to count($serial)"><xsl:variable name="outerIndex" select="."/>[<xsl:value-of select="$serial[$outerIndex]"/>, <xsl:value-of select="$charge[$outerIndex]"/>, <xsl:value-of select="$charges[$outerIndex]"/>, <xsl:value-of select="$chargep[$outerIndex]"/>, <xsl:value-of select="$charged[$outerIndex]"/>, <xsl:value-of select="$polarization[$outerIndex]"/>, <xsl:value-of select="$polarizations[$outerIndex]"/>, <xsl:value-of select="$polarizationp[$outerIndex]"/>, <xsl:value-of select="$polarizationd[$outerIndex]"/>  ]<xsl:if test="$outerIndex &lt; count($serial)">,</xsl:if></xsl:for-each>];
                                                
                                                $(document).ready(function() {
                                                    $('#lowdinchargesT-<xsl:value-of select="generate-id($lowdin)"/>').dataTable({
                                                        "aaData":  lowdin,
                                                        "aoColumns": [                                          
                                                            {"sTitle":"Atom"},                                                            
                                                            {"sTitle":"Total", "sClass": "right"},
                                                            {"sTitle":"s", "sClass": "right"},
                                                            {"sTitle":"p", "sClass": "right"},                                         
                                                            {"sTitle":"d", "sClass": "right"},
                                                            {"sTitle":"Total", "sClass": "right"},
                                                            {"sTitle":"s", "sClass": "right"},
                                                            {"sTitle":"p", "sClass": "right"},
                                                            {"sTitle":"d", "sClass": "right"}],
                                                        "bFilter": false,
                                                        "bPaginate": true,
                                                        "bSort": false,
                                                        "bInfo": true,
                                                        "bDestroy": true
                                                    });
                                                });                                                
                                            </script>                                                                                        
                                        </div>
                                    </div>
                                </xsl:if>                                
                            </div>  
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    

    <xsl:template name="eigenvalues">
        <xsl:variable name="eigenvalues" select="//module[@dictRef='cc:calculation']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='eigenvalues']"/>        
        <xsl:if test="count($eigenvalues) = 1">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#eigenvalues-{generate-id($eigenvalues)}" style="cursor: pointer; cursor: hand;" >
                    <h4 class="panel-title">                        
                        Eigenvalues                           
                    </h4>
                </div>
                <div id="eigenvalues-{generate-id($eigenvalues)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div class="row">                                
                                    <xsl:for-each select="$eigenvalues/list">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <xsl:variable name="nodeId" select="generate-id(.)"/>
                                            <xsl:variable name="spin" select="./scalar[@dictRef='cc:spin']"></xsl:variable>
                                            <h4>Spin <xsl:value-of select="if(compare($spin,'1') = 0) then
                                                'alpha'
                                                else
                                                'beta'"/></h4>
                                            
                                            <div class="button-group">
                                                Kpoint
                                                <xsl:for-each select="tokenize(./array[@dictRef='cc:serial'],'\s+')">
                                                    <button class="btn btn-default" type="button" onclick="pushButton(this); buildEigenOccupTable('eigenvaluesspin-{$nodeId}','{$spin}','{.}');"><xsl:value-of select="."/></button>
                                                </xsl:for-each>
                                            </div>                                       
                                            <script type="text/javascript">
                                                <xsl:variable name="numberOfRows" select=" number(./array[@dictRef='cc:eigen']/@size) idiv number(./array[@dictRef='cc:serial']/@size)"/>
                                                <xsl:variable name="eigen" select="tokenize(./array[@dictRef='cc:eigen'],'\s+')"/>
                                                <xsl:variable name="occup" select="tokenize(./array[@dictRef='cc:occup'],'\s+')"/>
                                                <xsl:for-each select="tokenize(./array[@dictRef='cc:serial'],'\s+')">
                                                    <xsl:variable name="outerIndex" select="position()"/>                                       
                                                    var eigenValuesSpin<xsl:value-of select="$spin"/>Serial<xsl:value-of select="."/> = [<xsl:for-each select="1 to $numberOfRows"><xsl:variable name="innerIndex" select="."/>['<xsl:value-of select="$eigen[ ($outerIndex - 1) * $numberOfRows + $innerIndex ]"/>','<xsl:value-of select="$occup[ ($outerIndex - 1) * $numberOfRows + $innerIndex ]"/>']<xsl:if test="$innerIndex &lt; $numberOfRows">,</xsl:if></xsl:for-each>];
                                                </xsl:for-each>
                                            </script>
                                            <table class="display" id="eigenvaluesspin-{$nodeId}"></table>
                                        </div>                                    
                                    </xsl:for-each>
                                </div>
                                <script type="text/javascript">
                                    function pushButton(button){                                        
                                        $(button.parentElement).children('.btn-default').removeClass('active');
                                        $(button).addClass('active');
                                    }                                    
                                    
                                    function buildEigenOccupTable(tableId,spin,kpoint){
                                        $('div#downloadTable' + tableId).remove();                                    
                                        $('table#' + tableId).dataTable({
                                            "aaData":  window['eigenValuesSpin' + spin + 'Serial' + kpoint],
                                            "aoColumns": [ { "sTitle": "Eigenvalues"},{"sTitle": "Occupation"}], 
                                            "bFilter": false,
                                            "bPaginate": true,
                                            "bSort": false,
                                            "bInfo": true,
                                            "bDestroy": true
                                        });
                                                                                                                         
                                        var jsTable = "javascript:showDownloadOptions('" + tableId + "');";                                      
                                        $('<div id="downloadTable'+ tableId + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableId +'_wrapper'); 
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </xsl:if>              
    </xsl:template>


    <xsl:template name="frequencies">
        <xsl:param name="section"/>
        <xsl:if test="exists($frequencies)">
            <xsl:variable name="serial" select="tokenize($frequencies/array[@dictRef='cc:serial'],'\s+')"/>
            <xsl:variable name="values" select="tokenize($frequencies/array[@dictRef='cc:frequency'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies-{generate-id($frequencies)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Frequencies
                    </h4>
                </div>
                <div id="frequencies-{generate-id($frequencies)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-3 col-sm-12">
                                        <table class="display" id="frequenciesT-{generate-id($frequencies)}">
                                            <thead>
                                                <tr>                                                    
                                                    <th></th>
                                                    <th></th>                                                
                                                </tr>
                                            </thead>                        
                                            <tbody>
                                                <xsl:for-each select="1 to count($serial)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <tr>
                                                        <td><xsl:value-of select="$serial[$outerIndex]"/></td>
                                                        <td><xsl:value-of select="$values[$outerIndex]"/></td>
                                                    </tr>
                                                </xsl:for-each>
                                            </tbody>
                                        </table> 
                                        
                                        <script type="text/javascript">
                                            $(document).ready(function() {
                                                $('table#frequenciesT-<xsl:value-of select="generate-id($frequencies)"/>' ).dataTable({
                                                    "aoColumns": [ { "sClass": "left"},{"sClass": "right"}],
                                                    "bFilter": false,
                                                    "bPaginate": false,
                                                    "bSort": false,
                                                    "bInfo": false,
                                                    "bDestroy": true
                                                });
                                            });
                                        </script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="absorptionspec">
        <xsl:if test="exists($absorption)">            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#absorption-{generate-id($absorption)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Absorption Spectrum
                    </h4>
                </div>
                <div id="absorption-{generate-id($absorption)}" class="panel-collapse">
                    <div class="panel-body pt-0">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-7 col-sm-12 mt-2">
                                        <div id="spectraContainer" />
                                            <script type="text/javascript">
                                                var hbaromega = [<xsl:value-of select="replace($absorption/cml:array[@dictRef='q:hbaromega'], '\s+', ',')"/>];
                                                var oscillation = [<xsl:value-of select="replace($absorption/cml:array[@dictRef='cc:oscillator'], '\s+', ',')"/>];
                                                $(document).ready(function() {
                                                    buildSpectraGraph();
                                                    buildSpectraTable('<xsl:value-of select="concat('spectraT-',generate-id($absorption))"/>');
                                                });
                                                <xsl:text disable-output-escaping="yes">
                                                <![CDATA[
                                                    function buildSpectraGraph() {
                                                        var type = $('input[name="spectraUnits"]:checked').val();
                                                        var data = generateAbsorptionData(type);
                                                        var title;                                                
                                                        var range;
                                                        var maxEnergy = Math.max(...data[0].x); 
                                                        if(type == 'ev') {
                                                            title = 'Photon energy (eV)';
                                                            maxEnergy = maxEnergy > 100.0 ? 100 : maxEnergy;                                                    
                                                        } else {
                                                            title = 'Photon energy (nm)';
                                                            maxEnergy = maxEnergy > 700.0 ? 700 : maxEnergy;                                                   
                                                        }
                                                        range = [0.0, maxEnergy];
                                                       
                                                        var layout = { 
                                                              autosize: true,
                                                              height: 350,
                                                              margin: {
                                                                t: 40,
                                                                r: 40,
                                                                b: 40,
                                                                pad: 0
                                                              },
                                                              modebar: {
                                                                orientation: 'v' 
                                                              },
                                                              style: {
                                                                cursor: 'pointer'
                                                              },
                                                              title: 'Absorption Spectrum',                                                             
                                                              xaxis: {
                                                                  title: title,
                                                                  showgrid: true,
                                                                  zeroline: true,
                                                                  tickmode: 'auto',
                                                                  range: range,
                                                                  rangeslider: { }
                                                          },
                                                              hovermode: 'closest',
                                                              yaxis: {
                                                              title: 'Intensity'
                                                              }
                                                        };
                                                        Plotly.newPlot('spectraContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                                    }
        
                                                    function generateAbsorptionData(units) {
                                                        var energy;
                                                        var data = new Array();                                                
                                                        if(units == 'ev') {
                                                          energy = hbaromega.map(function toev(number){
                                                               return number * 13.6058;
                                                          });
                                                        }else if(units == 'nm') {
                                                          energy = hbaromega.map(function toev(number){
                                                                if(number == 0) 
                                                                    return 0
                                                               else
                                                                    return 1.24/(number*13.6058/1000.0 + 1.0E-12);
                                                          });
                                                        }                                         
                                                        var serie = {
                                                               x : energy,
                                                               y : oscillation,                                                       
                                                               mode: 'lines'                                                      
                                                        };
                                                        data.push(serie);                                                                           
                                                        return data;
                                                    }
                                                ]]>
                                            </xsl:text>
                                            </script>                                      
                                    </div>  
                                    <div class="col-md-5 d-sm-none d-md-block mt-2">
                                    </div>
                                    <div class="col-md-7 col-sm-12 mt-3">
                                        <div class="d-flex flex-column">                                            
                                            <div id="spectraControlsContainer" class="d-flex flex-row justify-content-end mb-3">
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" id="eV" name="spectraUnits" value="ev" checked="checked" onclick="buildSpectraGraph()" class="mr-2"></input>
                                                    <label for="eV" style="margin-bottom:0px">eV</label>                                                
                                                </div>
                                                <div class="form-check form-check-inline">
                                                    <input type="radio" id="nm" name="spectraUnits" value="nm" onclick="buildSpectraGraph()" class="mr-2"></input>
                                                    <label for="nm" style="margin-bottom:0px">nm</label>                                                        
                                                </div>                                            
                                                <div id="spectraTable" class="d-flex flex-row justify-content:flex-end ml-4">
                                                    <table class="display" id="spectraT-{generate-id($absorption)}"></table>
                                                    <script>
                                                        function buildSpectraTable(tableId){
                                                        var dataev = generateAbsorptionData("ev");
                                                        var datanm = generateAbsorptionData("nm");
                                                        
                                                        var data = new Array();
                                                        for(var inx = 0; inx &lt; dataev[0].x.length; inx++){
                                                        data.push( [dataev[0].x[inx], datanm[0].x[inx], datanm[0].y[inx]]);
                                                        }
                                                        
                                                        $('div#downloadTable' + tableId).remove();                                    
                                                        $('table#' + tableId).dataTable({
                                                        "aaData": data,
                                                        "aoColumns": [ { "sTitle": "Photon Energy (eV)"},{ "sTitle": "Photon Energy (nm)"},{"sTitle": "Intensity"}], 
                                                        "bFilter": false,
                                                        "bPaginate": true,
                                                        "bSort": false,
                                                        "bInfo": true,
                                                        "bDestroy": true
                                                        });
                                                        
                                                        $('#spectraTable div.dataTables_wrapper').first().hide();                                                
                                                        }
                                                    </script>
                                                </div>                                       
                                            </div>

                                        </div>
                                        
                                    </div>                                   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>
        </div>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
      
</xsl:stylesheet>
