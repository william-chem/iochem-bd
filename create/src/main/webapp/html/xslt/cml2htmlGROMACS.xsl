<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:gm="http://www.iochem-bd.org/dictionary/gromacs/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk gm helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Sep 03, 2021</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>       
    </xd:doc> 
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:include href="helper/chemistry/gromacs.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>

    <!-- Environment module -->
    <xsl:variable name="environment" select="//cml:module[@id='job'][1]/cml:module[@id='environment']/cml:parameterList"/>
    <xsl:variable name="programParameter" select="$environment/cml:parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="$environment/cml:parameter[@dictRef='cc:programVersion']"/>
        
    <!-- Geometry -->
    <xsl:variable name="molecule" select="(//cml:molecule)[last()]"/>    
    
    <!-- Initialization -->
    <xsl:variable name="initialization" select="//cml:module[@dictRef='cc:initialization']"/>
    <xsl:variable name="tcoupl" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gm:tcoupl']" />
    <xsl:variable name="pcoupl" select="$initialization/cml:parameterList/cml:parameter[@dictRef='gm:pcoupl']" />
    <xsl:variable name="method" select="helper:getMethod($tcoupl, $pcoupl)" />
    
    <!-- Finalization -->
    <xsl:variable name="finalization" select="//cml:module[@dictRef='cc:finalization']/cml:propertyList"/>               
    
    <xsl:template match="/">        
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Settings -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>SETTINGS</h3>                                                                                                                                               
                            <div>
                                <xsl:call-template name="settings"/>
                            </div>
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                                                                                                               
                            <div>
                                <xsl:apply-templates select="$molecule"/>                                                               
                            </div>
                        </div>
                    </div>
                    <!-- Results -->
                    <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                        <div id="module-{generate-id(.)}">                                
                            <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                            <div class="row bottom-buffer">                     
                                <div class="col-md-12">
                                    <xsl:call-template name="energies"/>
                                    <xsl:call-template name="timing"/>
                                </div>
                            </div>
                        </div>
                    </xsl:for-each>
                    <xsl:call-template name="printLicense"/>
                    
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {    
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                        
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                            $.unblockUI();                             
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                               var oTable = $(this).dataTable();
                               $(oTable).css({ width: $(oTable).parent().width() });
                               oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
        </body>
    </html>
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinatesFractional" match="cml:molecule[cml:crystal]">                
        <xsl:variable name="collapseAccordion" select="if(count($molecule/atomArray/atom) > 1000) then '' else 'in'"/>
        <xsl:variable name="isLargeMolecule" select="count($molecule/atomArray/atom) > 1000"/>
        <xsl:variable name="boxCoords" select="//cml:module[@dictRef='cc:finalization']/cml:molecule/cml:crystal"/>
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                                      
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        
                        <xsl:if test="exists($boxCoords)">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12 mb-4">
                                        <h5>Cell parameters:</h5>
                                        <table id="cellParameters" class="display">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>a = <xsl:value-of select="./crystal/scalar[@title='a']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>b = <xsl:value-of select="./crystal/scalar[@title='b']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>c = <xsl:value-of select="./crystal/scalar[@title='c']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>α = <xsl:value-of select="./crystal/scalar[@title='alpha']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>β = <xsl:value-of select="./crystal/scalar[@title='beta']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>γ = <xsl:value-of select="./crystal/scalar[@title='gamma']"/></td>
                                                </tr>
                                            </tbody>
                                        </table>    
                                    </div>
                                </div>
                            </div>
                        </xsl:if>

                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="coordinateTypeDiv">
                                <span>Coordinate type :</span>
                                <input type="radio" name="coordinateType" value="both" checked="checked" onclick="changeMoleculeColumnVisibility()" />Both
                                <input type="radio" name="coordinateType" value="cartesian" onclick="changeMoleculeColumnVisibility()"/>Cartesian
                                <input type="radio" name="coordinateType" value="fractional" onclick="changeMoleculeColumnVisibility()"/>Fractional
                            </div>
                            <div id="coordinatesXYZ-{generate-id($molecule)}" class="right">
                                <xsl:if test="$isLargeMolecule">
                                </xsl:if>
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>                             
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>                               
                            </table>
                                      
                                      
                            <table id="atomicCoordinates">
                                <thead>
                                    <tr>
                                        <th rowspan="2"/>
                                        <th rowspan="2"/>
                                        <th colspan="3" style="text-align:center">Cartesian coordinates</th>
                                        <th colspan="3" style="text-align:center">Fractional coordinates</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">x</th>
                                        <th style="text-align:right">y</th>
                                        <th style="text-align:right">z</th>
                                        <th style="text-align:right">u</th>
                                        <th style="text-align:right">v</th>
                                        <th style="text-align:right">w</th>
                                    </tr>
                                </thead>
                            </table>
                            <script type="text/javascript">                                
                                var coordinates = [<xsl:for-each select="$molecule/cml:atomArray/cml:atom"><xsl:variable name="outerIndex" select="position()"/>
                                    [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="@elementType"/>","<xsl:value-of select="format-number(@x3, '#0.0000')"/>","<xsl:value-of select="format-number(@y3, '#0.0000')"/>","<xsl:value-of select="format-number(@z3, '#0.0000')"/>","<xsl:value-of select="format-number(@xFract, '#0.0000')"/>","<xsl:value-of select="format-number(@yFract, '#0.0000')"/>","<xsl:value-of select="format-number(@zFract, '#0.0000')"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                    </xsl:for-each>];

                                $(document).ready(function() {
                                    buildXYZTable();
                                    buildMolecularInfoTable();
                                    <xsl:if test="not(exists($molecule/cml:atomArray/cml:atom/@xFract))">   
                                    viewCartesianCoordinatesOnly();
                                    </xsl:if> 
                                } );

                                function changeMoleculeColumnVisibility(){
                                    var mode = $('input[name=coordinateType]:checked').val();
                                    var dt = $('#atomicCoordinates').DataTable();
                                    if( mode == 'both'){
                                        dt.columns([2,3,4,5,6,7]).visible(true);
                                    }else if(mode == 'cartesian') {
                                        dt.columns([2,3,4]).visible(true); 
                                        dt.columns([5,6,7]).visible(false);
                                    }else {
                                        dt.columns([2,3,4]).visible(false); 
                                        dt.columns([5,6,7]).visible(true);
                                    }
                                    dt.columns.adjust().draw();
                                    // Adjust table width to its container
                                    dt = $('#atomicCoordinates').dataTable();
                                    $(dt).css({ width: $(dt).parent().width() });
                                    dt.fnAdjustColumnSizing();
                                }
                                
                                function buildMolecularInfoTable() {
                                    $('table#atomicCoordinates').dataTable( {
                                        "aaData": coordinates,
                                        "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" },
                                            { "sTitle": "u", "sClass": "right" },
                                            { "sTitle": "v", "sClass": "right" },
                                            { "sTitle": "w", "sClass": "right" }],
                                        "bFilter": false,
                                        "bPaginate": <xsl:value-of select="if($isLargeMolecule) then 'true' else 'false'" />,
                                        "bSort": false,
                                        "bInfo": false
                                    } );   
                                }  
                                
                                function buildXYZTable() {
                                    var coordinatesXYZ = coordinates.map((val) => val.slice(1,5));
                                    coordinatesXYZ.unshift(['<xsl:value-of select="$title"/>','','','']);
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "aaData" : coordinatesXYZ,
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "bDeferRender": true
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                }
                                
                                function getXYZ() {
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons(table, {
                                        <xsl:choose>
                                            <xsl:when test="$isLargeMolecule">
                                                buttons: [ {
                                                    extend: 'csv',
                                                    text: 'CSV'
                                                }]
                                            </xsl:when>
                                            <xsl:otherwise>
                                                buttons: [ {
                                                    extend: 'copyHtml5',
                                                    text: 'XYZ'
                                                }]
                                            </xsl:otherwise>
                                        </xsl:choose>
                                    });
                                                                
                                    table.buttons().container().appendTo($('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));
                                    $('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                
                                <xsl:if test="$isLargeMolecule">
                                    $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").on('mousedown', function(){
                                        $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").addClass('processing');
                                    });
                                </xsl:if>     
                                }
                                
                                function viewCartesianCoordinatesOnly(){
                                    $('input[name=coordinateType ][value=cartesian]').prop('checked', true); 
                                    changeMoleculeColumnVisibility();
                                    $('#coordinateTypeDiv').hide()
                                }                                
                            </script>                              
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>


    <xsl:template name="atomicCoordinates" match="cml:molecule[not(cml:crystal)]">
        <xsl:variable name="boxCoords" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='averages']/cml:vector3[@dictRef='gm:box.coords'])[last()]"/>
        <xsl:variable name="collapseAccordion" select="if(count($molecule/atomArray/atom) > 1000) then '' else 'in'"/>
        <xsl:variable name="isLargeMolecule" select="count($molecule/atomArray/atom) > 1000"/>
        
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                                      
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <xsl:if test="exists($boxCoords)">
                            <xsl:variable name="coords" select="tokenize($boxCoords, '\s+')"/>
                            <div class="col-md-12">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <h5>Cell parameters:</h5>
                                    <table id="cellParameters" class="display">
                                        <thead>
                                            <tr>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>X = <xsl:value-of select="$coords[1]"/></td>
                                            </tr>
                                            <tr>
                                                <td>Y = <xsl:value-of select="$coords[2]"/></td>
                                            </tr>
                                            <tr>
                                                <td>Z = <xsl:value-of select="$coords[3]"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </xsl:if>
                        <div class="col-lg-6 col-md-8 col-sm-12">
                            <div id="coordinatesXYZ-{generate-id($molecule)}" class="right">
                                <xsl:if test="$isLargeMolecule">
                                </xsl:if>
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>                             
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>                               
                            </table>
                            
                            <table id="atomicCoordinates"></table>
                            
                            <script type="text/javascript">                                
                                var atomInfo = [<xsl:for-each select="$molecule/atomArray/atom">[<xsl:variable name="outerIndex" select="position()"/><xsl:variable name="elementType" select="@elementType"/><xsl:value-of select="$outerIndex"/>,'<xsl:value-of select="$elementType"/>',<xsl:value-of select="format-number(@x3,'#0.000000')"/>,<xsl:value-of select="format-number(@y3,'#0.000000')"/>,<xsl:value-of select="format-number(@z3,'#0.000000')"/>]<xsl:if test="$outerIndex&lt;count($molecule/atomArray/atom)">,</xsl:if></xsl:for-each>];
                                
                                $(document).ready(function() {      
                                    setupXYZTable();
                                    setupMolecularInfoTable();                                                                                                            
                                } );
                                
                                function setupXYZTable() {
                                    var coordinatesXYZ = atomInfo.map((val) => val.slice(1,5));
                                    coordinatesXYZ.unshift(['<xsl:value-of select="$title"/>','','','']); 
                                    
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "aaData" : coordinatesXYZ,
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "bDeferRender": true
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                }
                                
                                function setupMolecularInfoTable() {
                                    $('table#atomicCoordinates').dataTable( {
                                        "aaData": atomInfo,
                                        "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" }],                                
                                        "bFilter": false,
                                        "bPaginate": <xsl:value-of select="if($isLargeMolecule) then 'true' else 'false'" />,
                                        "bSort": false,
                                        "bInfo": false
                                    });   
                                }                               
                                
                                function getXYZ() {
                                var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                new $.fn.dataTable.Buttons(table, {
                                <xsl:choose>
                                    <xsl:when test="$isLargeMolecule">
                                        buttons: [ {
                                        extend: 'csv',
                                        text: 'CSV'
                                        }]
                                    </xsl:when>
                                    <xsl:otherwise>
                                        buttons: [ {
                                        extend: 'copyHtml5',
                                        text: 'XYZ'
                                        }]
                                    </xsl:otherwise>
                                </xsl:choose>
                                });
                                
                                table.buttons().container().appendTo($('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));
                                $('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                
                                <xsl:if test="$isLargeMolecule">
                                $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").on('mousedown', function(){
                                    $("div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> div.dt-buttons button.buttons-csv").addClass('processing');
                                });
                                </xsl:if>     
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <!--
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>
            -->
            <tr>
                <td style="vertical-align: top;">Calculation type:</td>
                <td>Molecular Dynamics (<xsl:value-of select="$method"/>)             
                   <br/>
                   <xsl:copy-of select="helper:getCouplingMethods($tcoupl, $pcoupl)"/>
                </td>                                
            </tr>          
        </table>        
    </xsl:template>
    
    <!-- Settings -->
    <xsl:template name="settings">
        <div class="row bottom-buffer">
            <div class="col-md-3 col-sm-6">
                <table class="display" id="timeSettings">
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th>Value</th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <xsl:copy-of select="helper:printParameterRow('gm:tinit')"/>
                        <xsl:copy-of select="helper:printParameterRow('gm:dt')"/>
                        <xsl:copy-of select="helper:printParameterRow('gm:nsteps')"/>
                    </tbody>
                </table>
                <script type="text/javascript">
                    $(document).ready(function() {                        
                        $('table#timeSettings').dataTable( {                                                      
                            "bFilter": false,
                            "bPaginate": false,
                            "bSort": false,
                            "bInfo": false
                        } );   
                    } );                                       
                </script>
            </div>
            
            <xsl:variable name="tcgroups" select="tokenize($environment/cml:parameter[@dictRef='gm:tc.grps'], '[\s]+')" />
            <xsl:variable name="taut" select="tokenize($environment/cml:parameter[@dictRef='gm:tau.t'], '[\s]+')" />
            <xsl:if test="exists($tcgroups)">
                <div class="col-md-4 col-sm-8">
                    <table class="display" id="otherSettings">
                        <thead>
                            <tr>
                                <th>Parameter</th>
                                <th>Value</th>
                            </tr>                        
                        </thead>
                        <tbody>                        
                            <tr>
                                <td style="vertical-align: top;">tc-groups<br/>tau-t</td>
                                <td>
                                    <table class="display compact">
                                        <thead>
                                            <tr>
                                                <xsl:for-each select="$tcgroups"><th><xsl:value-of select="."/></th></xsl:for-each>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <xsl:for-each select="$taut"><td><xsl:value-of select="."/></td></xsl:for-each>
                                            </tr>
                                        </tbody>
                                    </table>                                   
                                </td>
                            </tr>    
                            <xsl:copy-of select="helper:printParameterRow('gm:tau.p')"/>
                            <xsl:copy-of select="helper:printParameterRow('gm:rvdw')"/>
                            <xsl:copy-of select="helper:printParameterRow('gm:rcoulomb')"/>                                                        
                        </tbody>
                    </table>
                    <script type="text/javascript">
                        $(document).ready(function() {                        
                        $('table#otherSettings').dataTable( {                                                      
                        "bFilter": false,
                        "bPaginate": false,
                        "bSort": false,
                        "bInfo": false
                        } );   
                        } );                                       
                    </script>
                </div>
            </xsl:if>
        </div>   
    </xsl:template>

    <xsl:function name="helper:printParameterRow">
        <xsl:param name="parameterName" as="xs:string"/>
        <xsl:variable name="param" select="$initialization/cml:parameterList/cml:parameter[@dictRef = $parameterName]" />
        <xsl:if test="exists($param)">
                <tr>          
                    <td><xsl:value-of select="replace(replace($parameterName,'gm:',''), '\.', '-')"/></td>
                    <td><xsl:value-of select="$param/cml:*/text()"/></td>
                </tr>
        </xsl:if>
    </xsl:function>

    <xsl:template name="energies">
        <xsl:variable name="averages" select="
            if(exists(.//cml:module[@dictRef='cc:finalization']//cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='averages']))
            then
                .//cml:module[@dictRef='cc:finalization']//cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='averages']
             else
             .//cml:module[@dictRef='cc:finalization']//cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='steep']/cml:module[@cmlx:templateRef='energies']            
            "/>
        <xsl:variable name="energies" select="$averages/cml:property[child::cml:scalar[@dictRef = 'x:label' and not(matches(./text(), '(Temperature|Pres.*)'))]]" />
        <xsl:variable name="properties" select="$averages/cml:property[child::cml:scalar[@dictRef = 'x:label' and matches(./text(), '(Temperature|Pres.*)')]]" />
        <xsl:if test="exists($averages)">
            <xsl:variable name="energies" select="$energies" />
            <xsl:variable name="value" select="tokenize($energies/cml:array[@dictRef='x:value'], '[\s]')" />
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#energies-{generate-id($averages)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Energies</h4>
                </div>            
                <div id="energies-{generate-id($averages)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="energiesT-{generate-id($averages)}"></table>
                                                                
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#energiesT-<xsl:value-of select="generate-id($averages)"/>").dataTable({
                                        "aaData" : [  
                                            <xsl:for-each select="1 to count($energies)">                                                               
                                                <xsl:variable name="outerIndex" select="."/>                                                                                                                                                                                
                                                [ '<xsl:value-of select="$energies[$outerIndex]/cml:scalar[@dictRef='x:label']"/>',<xsl:value-of select="$energies[$outerIndex]/cml:scalar[@dictRef='x:value']"/>]<xsl:if test="position()!=last()">,</xsl:if>
                                            </xsl:for-each>
                                        ],
                                        "aoColumns" : [
                                            {"sTitle" : "Type"},
                                            {"sTitle" : "kJ/mol"},
                                            ],
                                         "aoColumnDefs" : [
                                            { "sClass": "nowrap", "aTargets": [ 0 ] },
                                            { "sClass": "text-right", "aTargets": [ 1 ] }
                                            
                                            ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    }); 
                                </script>
                            </div>
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="propertiesT-{generate-id($averages)}">
                                    <thead>
                                        <tr>
                                            <th>Thermodynamic conditions</th>
                                            <th></th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Temperature</td>
                                            <td><xsl:value-of select="$properties[child::cml:scalar[@dictRef='x:label' and text() = 'Temperature']]/cml:scalar[@dictRef='x:value']"/></td>
                                        </tr>        
                                        <tr>
                                            <td>Pressure (bar)</td>
                                            <td><xsl:value-of select="$properties[child::cml:scalar[@dictRef='x:label' and text() = 'Pressure (bar)']]/cml:scalar[@dictRef='x:value']"/></td>
                                        </tr>        
                                        <tr>
                                            <td>Pres. DC (bar)</td>
                                            <td><xsl:value-of select="$properties[child::cml:scalar[@dictRef='x:label' and text() = 'Pres. DC (bar)']]/cml:scalar[@dictRef='x:value']"/></td>                                            
                                        </tr>        
                                    </tbody>
                                </table>
                                
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#propertiesT-<xsl:value-of select="generate-id($averages)"/>").dataTable({                                    
                                        "aoColumns" : [
                                            {"sTitle" : "Thermodynamic conditions"},
                                            {"sTitle" : ""},
                                        ],
                                        "aoColumnDefs" : [
                                            { "sClass": "nowrap", "aTargets": [ 0 ] },
                                            { "sClass": "text-right", "aTargets": [ 1 ] }
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                        
                                        });
                                    }); 
                                </script>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </xsl:if>
    </xsl:template>

    <!-- Timing -->
    <xsl:template name="timing">  
        <xsl:variable name="numProcGPU" select="$environment/cml:parameter[@dictRef='gm:numProcGPU']" />
        <xsl:variable name="numRanks" select="$environment/cml:parameter[@dictRef='hardware']/cml:list/cml:scalar[@dictRef='gm:numRanks']" />
        
        <xsl:variable name="runDate" select="$environment/cml:parameter[@dictRef='cc:runDate']" />
        <xsl:variable name="endDate" select="$finalization/cml:property[@dictRef='cc:jobdatetime.end']" />
        <xsl:variable name="cputime" select="$finalization/cml:property[@dictRef='cc:cputime']" />
        <xsl:variable name="wallTime" select="$finalization/cml:property[@dictRef='cc:wallTime']" />
        <xsl:variable name="totalTime" select="$finalization/cml:property[@dictRef='gm:totaltime']" />
        
        <xsl:variable name="nsperday" select="$finalization/cml:property[@dictRef='gm:nsperday']" />
        <xsl:variable name="hourperns" select="$finalization/cml:property[@dictRef='gm:hourperns']" />
        
        <xsl:variable name="times" select="$finalization/cml:property[@dictRef='gm:time.accounting']/cml:list" />
    
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#timing-{generate-id($numProcGPU)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Timing                       
                </h4>
            </div>
            <div id="timing-{generate-id($numProcGPU)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">                        
                        <div class="col-lg-6 col-sm-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <table id="timingT-{generate-id($numProcGPU)}">
                                        <thead>
                                            <tr>
                                                <th>Environment</th>
                                                <th> </th>
                                            </tr> 
                                        </thead>
                                        <tbody>                         
                                            <tr>
                                                <td>Number of GPU processors</td>
                                                <td><xsl:value-of select="$numProcGPU"/></td>                        
                                            </tr>                                             
                                            <tr>
                                                <td>Number of ranks</td>
                                                <td><xsl:value-of select="$numRanks"/></td>                        
                                            </tr>
                                        </tbody>
                                    </table>
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                            $('table#timingT-<xsl:value-of select="generate-id($numProcGPU)"/>').dataTable( {
                                                "bFilter": false,                                 
                                                "bPaginate": false,                                    
                                                "bSort": false,
                                                "bInfo": false,
                                                "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 1 ] }]
                                            } );
                                        } );
                                    </script>
                                </div>                           
                            <xsl:if test="exists($times)">
                                    <div class="col-md-12 mt-3">
                                    <xsl:variable name="work" select="tokenize($times/cml:array[@dictRef='gm:work'], $times/cml:array[@dictRef='gm:work']/@delimiter)" />
                                    <xsl:variable name="walltime" select="tokenize($times/cml:array[@dictRef='cc:wallTime'], '[\s]+')" />
                                    <xsl:variable name="gcyclessum" select="tokenize($times/cml:array[@dictRef='gm:gcyclessum'], '[\s]+')" />
                                    <xsl:variable name="gcyclespercent" select="tokenize($times/cml:array[@dictRef='gm:gcyclespercent'], '[\s]+')" />
                                    
                                    <xsl:variable name="totalwalltime" select="$times/cml:scalar[@dictRef='gm:totalwalltime']" />
                                    <xsl:variable name="totalgcyclessum" select="$times/cml:scalar[@dictRef='gm:totalgcyclessum']" />
                                    <xsl:variable name="totalgcyclespercent" select="$times/cml:scalar[@dictRef='gm:totalgcyclespercent']" />
                                    
                                   
                                        <table id="accountingT-{generate-id($times)}">
                                            <thead>
                                                <tr>
                                                    <th> </th>                                        
                                                    <th> </th>
                                                    <th style="text-align:center" colspan="2">Giga-cycles</th>
                                                </tr>                                        
                                                <tr>
                                                    <th>Computing</th>
                                                    <th>Wall time (s)</th>
                                                    <th>Sum</th>
                                                    <th>Percent</th>
                                                </tr>                                               
                                            </thead>
                                            <tbody>                                            
                                                <xsl:for-each select="1 to count($work)">
                                                    <xsl:variable name="outerIndex" select="."/>                                            
                                                    <tr>
                                                        <td><xsl:value-of select="$work[$outerIndex]"/></td>    
                                                        <td><xsl:value-of select="$walltime[$outerIndex]"/></td>
                                                        <td><xsl:value-of select="$gcyclessum[$outerIndex]"/></td>
                                                        <td><xsl:value-of select="$gcyclespercent[$outerIndex]"/></td>
                                                    </tr>   
                                                </xsl:for-each>
                                                <tr>
                                                    <td>TOTAL</td>
                                                    <td><xsl:value-of select="$totalwalltime"/></td>
                                                    <td><xsl:value-of select="$totalgcyclessum"/></td>
                                                    <td><xsl:value-of select="$totalgcyclespercent"/></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                        <script type="text/javascript">
                                            $(document).ready(function() {                        
                                            $('table#accountingT-<xsl:value-of select="generate-id($times)"/>').dataTable( {
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumns" : [
                                            { "sClass": ""},
                                            { "sClass": "right"},
                                            { "sClass": "right"},
                                            { "sClass": "right"}]
                                            } );   
                                            } );                                       
                                        </script>
                                    </div>
                            </xsl:if>
                            </div>
                    </div>                    
                    <xsl:if test="exists($wallTime) or exists($runDate)">
                         <div class="col-lg-6 col-sm-12"> 
                            <table id="timingT-{generate-id($wallTime)}">
                                <thead>
                                    <tr>
                                        <th>Timing</th>
                                        <th> </th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Start</td>
                                        <td><xsl:value-of select="$runDate"/></td>
                                    </tr>                         
                                    <tr>
                                        <td>End</td>
                                        <td><xsl:value-of select="$endDate"/></td>                        
                                    </tr>
                                    <xsl:if test="exists($cputime)">
                                    <tr>
                                        <td>Cpu</td>
                                        <td><xsl:value-of select="concat($cputime, ' ', helper:printUnitSymbol($cputime/cml:scalar/@units))"/></td>
                                    </tr>
                                    </xsl:if>
                                    <xsl:if test="exists($wallTime)">
                                    <tr>
                                        <td>Wall time</td>
                                        <td><xsl:value-of select="concat($wallTime, ' ', helper:printUnitSymbol($cputime/cml:scalar/@units))"/></td>                        
                                    </tr>       
                                        
                                    </xsl:if>
                                    <xsl:if test="exists($totalTime)">
                                    <tr>
                                        <td>Total time</td>
                                        <td><xsl:value-of select="$totalTime"/></td>                        
                                    </tr>
                                    </xsl:if>
                                    <xsl:if test="exists($nsperday)">
                                    <tr>
                                        <td>ns per day</td>
                                        <td><xsl:value-of select="$nsperday"/></td>                        
                                    </tr>    
                                    </xsl:if>
                                    <xsl:if test="exists($hourperns)">
                                    <tr>
                                        <td>Hour per ns</td>
                                        <td><xsl:value-of select="$hourperns"/></td>                        
                                    </tr>                                        
                                    </xsl:if>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#timingT-<xsl:value-of select="generate-id($wallTime)"/>').dataTable( {
                                "bFilter": false,                                 
                                "bPaginate": false,                                    
                                "bSort": false,
                                "bInfo": false,
                                "aoColumnDefs" : [                                    
                                { "sClass": "text-right", "aTargets": [ 1 ] }
                                ]
                                } );   
                                } );                                       
                            </script>  
                        </div>                                                      
                    </xsl:if>
                    </div>
                </div>
            </div>
        </div>                                  
                    
    </xsl:template> 
   
   
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
    
</xsl:stylesheet>
