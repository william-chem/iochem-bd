<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    exclude-result-prefixes="xs cml"
    version="2.0">
    
    <xsl:output encoding="UTF-8" media-type="text/plain" method="text" />
    
    <xsl:param name="source"/>
    <xsl:variable name="geometry" select="(//cml:module[@id='finalization']/cml:molecule)[last()]"/>
    <xsl:variable name="atoms" select="$geometry/cml:atomArray/cml:atom"/>
    <xsl:variable name="atomcount" select="count($atoms)"/>
    <xsl:variable name="bonds" select="$geometry/cml:bondArray/cml:bond"/>
    <xsl:variable name="bondcount" select="count($bonds)" />
    <xsl:variable name='newline'><xsl:text>
</xsl:text></xsl:variable>
    
    <xsl:template match="/">
<xsl:text>ioChem-BD file using: cml2mol templated</xsl:text><xsl:value-of select="$newline"/>  
<xsl:text>Original source: </xsl:text><xsl:value-of select="$source"/><xsl:value-of select="$newline"/>    
<xsl:text>Generation date: </xsl:text><xsl:value-of select="current-dateTime()"/><xsl:value-of select="$newline"/>
        <xsl:value-of select="concat(ckbk:justify('right',3,string($atomcount)),ckbk:justify('right',3,string($bondcount)),'  0  0  0  0  0  0  0  0999 V2000',$newline)"/>
        <xsl:for-each select="1 to $atomcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="atom" select="$atoms[$outerIndex]"/>
            <xsl:value-of select="ckbk:printAtom($atom,$outerIndex)"/>
        </xsl:for-each>
        <xsl:for-each select="1 to $bondcount">
            <xsl:variable name="outerIndex" select="."/>
            <xsl:variable name="firstAtom" select="substring(tokenize($bonds[$outerIndex]/@atomRefs2,'[\s+]')[1],2)"/>
            <xsl:variable name="secondAtom" select="substring(tokenize($bonds[$outerIndex]/@atomRefs2,'[\s+]')[2],2)"/>
            <xsl:value-of select="ckbk:printBond($firstAtom,$secondAtom)"/>
        </xsl:for-each>M  END</xsl:template>    
    
    <xsl:function name="ckbk:justify">
        <xsl:param name="align" as="xs:string"/>
        <xsl:param name="length" as="xs:integer"/>
        <xsl:param name="value" as="xs:string"/>
        <xsl:variable name="blanks" select="'                              '"/><!--30 blank spaces will be enough by now-->
        <xsl:if test="$align ='left'">
            <xsl:sequence select="substring(concat($value,$blanks),1,$length)"/>
        </xsl:if>
        <xsl:if test="$align ='right'">
            <xsl:sequence select="substring(concat($blanks,$value), string-length($blanks) + string-length($value) - $length +1 ,$length)"/>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="ckbk:printAtom">
        <xsl:param name="atom" />
        <xsl:param name="index" as="xs:integer"/>
        
        <xsl:variable name="x3" select="ckbk:justify('right',10,string(format-number($atom/@x3,'#0.0000')))"/>
        <xsl:variable name="y3" select="ckbk:justify('right',10,string(format-number($atom/@y3,'#0.0000')))"/>
        <xsl:variable name="z3" select="ckbk:justify('right',10,string(format-number($atom/@z3,'#0.0000')))"/>
        <xsl:variable name="elementType" select="ckbk:justify('right',4,$atom/@elementType)"/>
        <xsl:variable name="others" select="string(' 0  0  0  0  0  0  0  0  0')" />
        <xsl:variable name="atomNumber" select="ckbk:justify('right',3,string($index))"/>
        <xsl:sequence select="concat($x3,$y3,$z3,$elementType,$others,$atomNumber,$newline)"/>
    </xsl:function>
    
    <xsl:function name="ckbk:printBond">
        <xsl:param name="firstAtom"/>
        <xsl:param name="secondAtom"/>
        <xsl:variable name="atom1" select="ckbk:justify('right',3,$firstAtom)"/>
        <xsl:variable name="atom2" select="ckbk:justify('right',3,$secondAtom)"/>
        <xsl:sequence select="concat($atom1,$atom2,'  1  0  0  0', $newline)"/>
    </xsl:function>
    
    
</xsl:stylesheet>