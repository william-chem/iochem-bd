<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.xml-cml.org/schema"
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:fo="http://www.w3.org/1999/XSL/Format"
	xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
	xpath-default-namespace="http://www.xml-cml.org/schema"
    version="2.0">
	<xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
	<xsl:param name="reportTitle"/>
	
    <xsl:template match="/">    	
    	<fo:root>
	    	<fo:layout-master-set>    		
	    		<fo:simple-page-master master-name="calculations"
	    			page-height="29.7cm" 
	    			page-width="21cm"
	    			margin-top="1in"
	    			margin-bottom="1in"
	    			margin-left = "0.75in"
	    			margin-right="0.75in">	    			
	    			<fo:region-body 				
	    				margin-top="0.6in" 
	    				margin-bottom="0.6in"/>    			
	    			<fo:region-before 
	    				extent="0.5in"/>    			
	    			<fo:region-after  
	    				extent="0.5in"/>			
	    		</fo:simple-page-master>
	    		
	    	</fo:layout-master-set>
	    	
	    	<fo:page-sequence master-reference="calculations">	
	    		<!-- Header section -->
	    		<fo:static-content flow-name="xsl-region-before">
	    			<fo:list-block provisional-distance-between-starts="5in" provisional-label-separation="0in">
	    				<fo:list-item>
	    					<fo:list-item-label>
	    						<fo:block  text-align="start"><xsl:value-of select="$reportTitle"/></fo:block>
	    					</fo:list-item-label>
	    					<fo:list-item-body>
	    						<fo:block  text-align="end"></fo:block>
	    					</fo:list-item-body>
	    				</fo:list-item>
	    			</fo:list-block>
	    		</fo:static-content>
	    		<!-- Footer section -->
	    		<fo:static-content flow-name="xsl-region-after">
	    			<fo:list-block provisional-distance-between-starts="5in" provisional-label-separation="0in">
	    				<fo:list-item>
	    					<fo:list-item-label>
	    						<fo:block  text-align="start"></fo:block>
	    					</fo:list-item-label>
	    					<fo:list-item-body>
	    						<fo:block  text-align="end">Page <fo:page-number/></fo:block>
	    					</fo:list-item-body>
	    				</fo:list-item>
	    			</fo:list-block>
	    		</fo:static-content>	
	    		<!-- Body section -->
	    		<fo:flow flow-name="xsl-region-body" font-family="Courier">			
	    			<xsl:for-each select="/template/template[@id='calculation']">	    				
	    				<fo:block break-before="page">
	    					<xsl:if test="exists(./scalar[@id='title'])">
	    						<fo:block><fo:inline font-size="1em"><xsl:value-of select="./scalar[@id='title']"/></fo:inline></fo:block>
	    					</xsl:if>
	    					<xsl:if test="exists(./scalar[@name='energy'])">
	    						<fo:block>Energy (<xsl:value-of select="./scalar[@name='energy']/@type"/>) = <xsl:value-of select="./scalar[@name='energy']"/><xsl:text> </xsl:text><xsl:value-of select="helper:printUnitSymbol(./scalar[@name='energy']/@units)"/></fo:block>
	    					</xsl:if>
	    					<xsl:choose>
	    						<xsl:when test="exists(./molecule/crystal)">
	    							<fo:table>
	    								<fo:table-body>
	    									<fo:table-row>
	    										<fo:table-cell><fo:block>a = </fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block><xsl:value-of select="./molecule/crystal/scalar[@title='a']"/></fo:block></fo:table-cell>    										
	    									</fo:table-row>
	    									<fo:table-row>
	    										<fo:table-cell><fo:block>b = </fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block><xsl:value-of select="./molecule/crystal/scalar[@title='b']"/></fo:block></fo:table-cell>    										
	    									</fo:table-row>    									
	    									<fo:table-row>
	    										<fo:table-cell><fo:block>c = </fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block><xsl:value-of select="./molecule/crystal/scalar[@title='c']"/></fo:block></fo:table-cell>    										
	    									</fo:table-row>    									
	    									<fo:table-row>
	    										<fo:table-cell><fo:block>alpha = </fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block><xsl:value-of select="./molecule/crystal/scalar[@title='alpha']"/></fo:block></fo:table-cell>    										
	    									</fo:table-row>
	    									<fo:table-row>
	    										<fo:table-cell><fo:block>beta = </fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block><xsl:value-of select="./molecule/crystal/scalar[@title='beta']"/></fo:block></fo:table-cell>    										
	    									</fo:table-row>
	    									<fo:table-row>
	    										<fo:table-cell><fo:block>gamma = </fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block><xsl:value-of select="./molecule/crystal/scalar[@title='gamma']"/></fo:block></fo:table-cell>    										
	    									</fo:table-row>    									
	    								</fo:table-body>
	    							</fo:table>
	    							<fo:table>    								
	    								<fo:table-body>
	    									<fo:table-row>
	    										<fo:table-cell width="0.3in"><fo:block></fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Atom</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">X</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Y</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Z</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">X</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Y</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Z</fo:block></fo:table-cell>    								    										
	    									</fo:table-row>
	    									<xsl:for-each select="./molecule/atomArray/atom">
	    										<fo:table-row>
	    											<fo:table-cell width="0.3in"><fo:block><xsl:value-of select="position()"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block><xsl:value-of select="@elementType"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@x3,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@y3,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@z3,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@xFract,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@yFract,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@zFract,'#0.0000')"/></fo:block></fo:table-cell>    								    										
	    										</fo:table-row>
	    									</xsl:for-each>
	    								</fo:table-body>
	    							</fo:table>    					    							
	    						</xsl:when>
	    						<xsl:otherwise>
	    							<fo:table>
	    								<fo:table-body>
	    									<fo:table-row>
	    										<fo:table-cell width="0.3in"><fo:block></fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Atom</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">X</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Y</fo:block></fo:table-cell>
	    										<fo:table-cell><fo:block text-align="end">Z</fo:block></fo:table-cell>    								
	    									</fo:table-row>
	    									<xsl:for-each select="./molecule/atomArray/atom">
	    										<fo:table-row>
	    											<fo:table-cell width="0.3in"><fo:block><xsl:value-of select="position()"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="@elementType"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@x3,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@y3,'#0.0000')"/></fo:block></fo:table-cell>
	    											<fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(@z3,'#0.0000')"/></fo:block></fo:table-cell>	    											
	    										</fo:table-row>
	    									</xsl:for-each>
	    								</fo:table-body>
	    							</fo:table>    					
	    						</xsl:otherwise>
	    					</xsl:choose>
	    				</fo:block>
	    			</xsl:for-each>
	    		</fo:flow>
	    	</fo:page-sequence>
    	</fo:root>
    </xsl:template>
</xsl:stylesheet>