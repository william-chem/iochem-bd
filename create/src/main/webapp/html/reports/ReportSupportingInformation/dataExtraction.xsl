<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet   
    xmlns="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:gaussian="http://www.gaussian.com/"
    xmlns:v="http://www.iochem-bd.org/dictionary/vasp/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="#all"    
    version="2.0">
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/gaussian.xsl"/>
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:variable name="energyTypePotential" select="'POTENTIAL'"/>
    <xsl:variable name="energyTypeFree"      select="'FREE'"/>
    <xsl:variable name="energyTypeZeroPoint" select="'ZERO_POINT'"/>
    <xsl:variable name="energyTypeEnthalpy"  select="'ENTHALPY'"/>
    <xsl:variable name="energyTypeBonding"   select="'BONDING'"/> 
    <xsl:variable name="energyTypeNucRepulsion"   select="'NUCREPULSION'"/>
    
    <xsl:param name="title"/>
    <xsl:param name="energyType"/>
    <xsl:param name="energyUnits"/>

    <xsl:template match='/module'>
        <template id="calculation" name="{$title}">
            <xsl:call-template name="retrieveTitle"/>         
            <xsl:call-template name="retrieveEnergy"/>         
            <xsl:call-template name="retrieveFinalGeometry"/>
        </template>
    </xsl:template>
    
    <xsl:template name="retrieveTitle">
        <scalar id="title"><xsl:value-of select="$title"/></scalar>
    </xsl:template>
    
    <xsl:template name="retrieveEnergy">
        <xsl:if test="$energyType">
            <xsl:variable name="energyValue">
                <xsl:choose>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">
                                <xsl:value-of select="helper:getGaussianPotentialEnergy(., $energyUnits)"></xsl:value-of>                                                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:zpe.sumelectthermalfe'])[last()],$energyUnits)"/>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:zpe.sumelectzpe'])[last()],$energyUnits)"/>                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeEnthalpy) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:zpe.sumelectthermalent'])[last()],$energyUnits)"/>                        
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>
                        </xsl:choose>        
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ADF')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypeBonding) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='bonding.energy']//scalar[@dictRef='cc:total'])[last()],$energyUnits)"/>
                            </xsl:when>                           
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:variable name="bonding" select="number((//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='bonding.energy']/module[@cmlx:templateRef='summary']/scalar[@dictRef='cc:total'])[last()])"/>
                                <xsl:variable name="internal" select="number((//list[@cmlx:templateRef='internalEnergy']/scalar[@dictRef='cc:total'])[last()])" />
                                <xsl:variable name="entropy" select="number((//list[@cmlx:templateRef='entropy']/scalar[@dictRef='cc:total'])[last()])"/>
                                <xsl:variable name="thermochemistry" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]"/>
                                <xsl:variable name="temp" select="$thermochemistry/module[@cmlx:templateRef='energies']/scalar[@dictRef='cc:temp']"/>
                                <xsl:variable name="free" select="(($bonding * 96.48531 * 1000 +  $internal * 4184 ) + (8.31441 * number($temp)) - (number($temp) * $entropy * 4.184)) div 1000"/> <!-- Units in kJ/mol -->
                                
                                <xsl:variable name="freeEnergy">
                                    <xsl:element name="scalar">
                                        <xsl:attribute name="units"><xsl:text>nonsi:kj.mol-1</xsl:text></xsl:attribute>
                                        <xsl:value-of select="$free"/>
                                    </xsl:element>
                                </xsl:variable>                                
                                <xsl:value-of select="helper:convertEnergyUnits($freeEnergy/scalar,$energyUnits)"/>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">
                                <xsl:variable name="zeroPoint" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:zeropoint']/scalar)[last()]"/>                                
                                <xsl:value-of select="helper:convertEnergyUnits($zeroPoint,$energyUnits)"/>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeEnthalpy) = 0">
                                <xsl:variable name="bonding" select="number((//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='bonding.energy']/module[@cmlx:templateRef='summary']/scalar[@dictRef='cc:total'])[last()])"/>
                                <xsl:variable name="internal" select="number((//list[@cmlx:templateRef='internalEnergy']/scalar[@dictRef='cc:total'])[last()])" />
                                <xsl:variable name="entropy" select="number((//list[@cmlx:templateRef='entropy']/scalar[@dictRef='cc:total'])[last()])"/>
                                <xsl:variable name="thermochemistry" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]"/>
                                <xsl:variable name="temp" select="$thermochemistry/module[@cmlx:templateRef='energies']/scalar[@dictRef='cc:temp']"/>
                                <xsl:variable name="free" select="((($bonding * 96.48531 * 1000 +  $internal * 4184 ) + (8.31441 * number($temp)) - (number($temp) * $entropy * 4.184)) div 1000) div 4.184"/> <!-- Units in kcal.mol-1 -->
                                
                                <xsl:variable name="enthalpy">
                                    <xsl:element name="scalar">
                                        <xsl:attribute name="units"><xsl:text>nonsi2:kcal.mol-1</xsl:text></xsl:attribute>
                                        <xsl:value-of select="(($entropy div 1000) * $temp) + $free"/>
                                    </xsl:element>
                                </xsl:variable>                                
                                <xsl:value-of select="helper:convertEnergyUnits($enthalpy/scalar,$energyUnits)"/>
                                
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'TURBOMOLE')">
                        <xsl:variable name="energy" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:energy']/scalar)[last()]"/>
                        <xsl:variable name="energy2" select="(//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@id='energy.resume']/scalar[@dictRef='t:rhfEnergy'])[last()]"/>
                        <xsl:variable name="zeropoint" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:zeropoint']/scalar)[last()]" xpath-default-namespace="http://www.xml-cml.org/schema"></xsl:variable>
                        <xsl:variable name="nucrepulsion" select="(//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='nuclear.repulsion']/scalar[@dictRef='cc:nucrepener'])[last()]"/>                        
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">                                
                                <xsl:choose>
                                    <xsl:when test="exists($energy)">
                                        <xsl:value-of select="helper:convertEnergyUnits($energy, $energyUnits)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="helper:convertEnergyUnits($energy2, $energyUnits)"/>
                                    </xsl:otherwise>
                                </xsl:choose>                                
                            </xsl:when>                            
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">                
                                <xsl:choose>
                                    <xsl:when test="exists($energy)">
                                        <xsl:value-of select="number(helper:convertEnergyUnits($zeropoint,$energyUnits)) + number(helper:convertEnergyUnits($energy, $energyUnits))"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="number(helper:convertEnergyUnits($zeropoint,$energyUnits)) + number(helper:convertEnergyUnits($energy2, $energyUnits))"/>
                                    </xsl:otherwise>
                                </xsl:choose>                               
                            </xsl:when>                           
                            <xsl:when test="compare($energyType, $energyTypeNucRepulsion) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits($nucrepulsion, $energyUnits)"/>
                            </xsl:when>                            
                        </xsl:choose>
                    </xsl:when>                                       
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ORCA')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">                           
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:totalener'])[last()],$energyUnits)"/>                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='enthalpy']/scalar[@dictRef='o:totalfree'])[last()],$energyUnits)"/>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">                                
                                <xsl:variable name="value">
                                    <xsl:element name="scalar">
                                        <xsl:attribute name="units"><xsl:value-of select="(//module[@cmlx:templateRef='enthalpy'])[last()]/scalar[@dictRef='o:totalfree']/@units"/></xsl:attribute>                                    
                                        <xsl:value-of select="number((//module[@cmlx:templateRef='enthalpy'])[last()]/scalar[@dictRef='o:totalfree'])
                                                                +   
                                                              number((//module[@cmlx:templateRef='enthalpy'])[last()]/scalar[@dictRef='o:thermalcorrenthalpy'])"/>
                                    </xsl:element>
                                </xsl:variable>
                                <xsl:value-of select="helper:convertEnergyUnits($value/scalar,$energyUnits)"/>                        
                            </xsl:when>                          
                            <xsl:when test="compare($energyType, $energyTypeEnthalpy) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='gibbs']/scalar[@dictRef='o:gibbsenthalpy'])[last()],$energyUnits)"/>                                
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeNucRepulsion) = 0">                                
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:nucrepener'])[last()],$energyUnits)"/>                                
                            </xsl:when>
                            <xsl:otherwise>                                
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>                          
                        </xsl:choose>
                    </xsl:when>                
                    <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'VASP')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:e0'])[last()], $energyUnits)"/>                                                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:freeEnergy'])[last()],$energyUnits)"/>
                            </xsl:when>                           
                            <xsl:otherwise>
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <scalar name="energy" type="{$energyType}" units="{$energyUnits}"><xsl:value-of select="$energyValue"/></scalar>                
        </xsl:if>
    </xsl:template>
        
    <xsl:function name="helper:getGaussianPotentialEnergy">
        <xsl:param name="base"/>        
        <xsl:param name="units"/>        
        <xsl:variable name="archiveEnergies" select="$base//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@dictRef='g:archive.namevalue']"/>
        <xsl:variable name="method">
            <xsl:if test="exists($base//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])">
                <xsl:variable name="hf" select="($base//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])[last()]"/>                

                <xsl:element name="energy">
                    <xsl:attribute name="method"  select="'hf'" />
                    <xsl:attribute name="value" select="$hf/text()" />
                    <xsl:attribute name="units" select="$hf/@units" />
                    <xsl:value-of select="$hf/text()"/>
                </xsl:element>
            </xsl:if>

            <xsl:if test="$archiveEnergies != ''">
                <xsl:for-each select="$archiveEnergies/scalar">
                    <xsl:variable name="field" select="."/>
                    <xsl:if test="matches($field,'.*=.*')">
                        <xsl:variable name="splittedField" select="tokenize($field,'=')"/>
                        <xsl:if test="gaussian:isMethod($splittedField[1])">
                            <xsl:element name="energy">
                                <xsl:attribute name="method"  select="$splittedField[1]"/>
                                <xsl:attribute name="value" select="$splittedField[2]"/>
                                <xsl:attribute name="units" select="'nonsi:hartree'"/>
                                <xsl:value-of select="$splittedField[2]"/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
        </xsl:variable>
        
        <xsl:if test="exists($method/*:energy)">
            <xsl:variable name="finest" select="$method/*:energy[last()]"/>     <!-- Capture last (and more accurate) method energy -->
            <xsl:value-of select="helper:convertEnergyUnits($finest, $units)"/>            
        </xsl:if>        
    </xsl:function>

    <xsl:template name="retrieveFinalGeometry" >              
        <xsl:copy-of select="(.//molecule)[last()]" copy-namespaces="no"/>            
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>