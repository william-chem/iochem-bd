<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:a="http://www.xml-cml.org/dictionary/adf/" 
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:gaussian="http://www.gaussian.com/"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="#all"
    version="2.0">
    <xsl:output method="text" encoding="UTF-8" indent="no"/>
    <xsl:include href="../../xslt/helper/chemistry/gaussian.xsl"/>

    <xsl:template match="/">
        <xsl:variable name="energies">
            <xsl:choose>
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'GAUSSIAN')">                    
                    <xsl:if test="helper:hasPotentialEnergySection(.)">
                        <xsl:text>POTENTIAL|</xsl:text>                           
                    </xsl:if>
                    <xsl:if test="exists(//scalar[@dictRef='cc:zpe.sumelectthermalfe'])">
                        <xsl:text>FREE|</xsl:text>
                        <xsl:if test="
                            exists(//property[@dictRef='cc:thermochemistry']/list/scalar[@dictRef='cc:molmass'])
                            and 
                            exists(//module[@cmlx:templateRef='l9999.archive']/scalar[@dictRef='cc:hfenergy'])
                            and
                            exists(//module[@cmlx:templateRef='l202.stoich']/scalar[@dictRef='cc:pointgroup'])                           
                            ">
                            <xsl:text>FREE_CORRECTED|</xsl:text>
                        </xsl:if>
                    </xsl:if>
                    <xsl:if test="exists(//scalar[@dictRef='cc:zpe.sumelectzpe'])">
                        <xsl:text>ZERO_POINT|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists(//scalar[@dictRef='cc:zpe.sumelectthermalent'])">
                        <xsl:text>ENTHALPY|</xsl:text>
                    </xsl:if>            
                </xsl:when>
                
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ADF')">
                    <xsl:variable name="bonding" select="(//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='bonding.energy']/module[@cmlx:templateRef='summary']/scalar[@dictRef='cc:total'])[last()]"/>
                    <xsl:variable name="internal" select="(//list[@cmlx:templateRef='internalEnergy']/scalar[@dictRef='cc:total'])[last()]" />
                    <xsl:variable name="entropy" select="(//list[@cmlx:templateRef='entropy']/scalar[@dictRef='cc:total'])[last()]"/>                    
                    <xsl:variable name="thermochemistry" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]"/>                    
                    
                    <xsl:if test="exists(//module[@cmlx:templateRef='bonding.energy' and descendant::scalar[@dictRef='cc:total']])">
                        <xsl:text>BONDING|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists($bonding) and exists($internal) and exists($entropy) and exists($thermochemistry)">                                                                  
                        <xsl:text>ENTHALPY|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists(//module[@id='finalization']/propertyList/property[@dictRef='cc:zeropoint']/scalar)">
                        <xsl:text>ZERO_POINT|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists($bonding) and exists($internal) and exists($entropy)">                                                                  
                        <xsl:text>FREE|</xsl:text>                        
                        <xsl:if test="
                             exists(//module[@dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])
                             and
                             exists(//module[@cmlx:templateRef='adf.frequencyanalysis']//array[@dictRef='cc:atomicmass'])
                             and 
                             exists(//module[@cmlx:templateRef='bonding.energy']//scalar[@dictRef='cc:eener'])
                             and                             
                             exists(//property[@dictRef='cc:thermochemistry']/module/array[@dictRef='cc:moi'])
                             and
                             exists(//module[@cmlx:templateRef='symmetry']/scalar[@dictRef='a:symmetry'])                             
                             and
                             exists(//property[@dictRef='cc:thermochemistry']/module/scalar[@dictRef='cc:symmnumber'])                             
                            ">
                            <xsl:text>FREE_CORRECTED|</xsl:text>
                        </xsl:if>                        
                    </xsl:if>                    
                </xsl:when>
                
                
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'TURBOMOLE')">
                    <xsl:variable name="energy" select="//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:energy']/scalar"/>
                    <xsl:variable name="energy2" select="//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@id='energy.resume']/scalar[@dictRef='t:rhfEnergy']"/>
                    <xsl:variable name="repulsion" select="//module[@cmlx:templateRef='nuclear.repulsion']"/>
                    <xsl:variable name="zeroPoint" select="//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:zeropoint']/scalar"/>
                    
                    <xsl:if test="exists($energy) or exists($energy2)">
                        <xsl:text>POTENTIAL|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists($repulsion)">
                        <xsl:text>NUCREPULSION|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists($zeroPoint)">
                        <xsl:text>ZERO_POINT|</xsl:text>
                    </xsl:if>
                </xsl:when>
                
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'ORCA')">
                    <xsl:variable name="potential" select="//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:totalener']"/>
                    <xsl:variable name="free" select="//module[@cmlx:templateRef='enthalpy']/scalar[@dictRef='o:totalfree']"/>                    
                    <xsl:variable name="zeropoint" select="//module[@cmlx:templateRef='enthalpy']/scalar[@dictRef='o:totalfree']" />
                    <xsl:variable name="enthalpy" select="//module[@cmlx:templateRef='gibbs']/scalar[@dictRef='o:gibbsenthalpy']" />
                    <xsl:variable name="nucrepulsion" select="//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:nucrepener']"/>

                    <xsl:if test="exists($potential)">
                        <xsl:text>POTENTIAL|</xsl:text>    
                    </xsl:if>
                    <xsl:if test="exists($free)">
                        <xsl:text>FREE|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists($zeropoint)">
                        <xsl:text>ZERO_POINT|</xsl:text>
                    </xsl:if>
                    <xsl:if test="exists($enthalpy)">
                        <xsl:text>ENTHALPY|</xsl:text>
                    </xsl:if> 
                    <xsl:if test="exists($nucrepulsion)">
                        <xsl:text>NUCREPULSION|</xsl:text>
                    </xsl:if>
                </xsl:when>               
                
                <xsl:when test="matches(upper-case((//parameter[@dictRef='cc:program'])[last()]), 'VASP')">                     
                    <xsl:if test="exists(//scalar[@dictRef='cc:e0'])">
                        <xsl:text>POTENTIAL|</xsl:text>                           
                    </xsl:if>
                    <xsl:if test="exists(//scalar[@dictRef='cc:freeEnergy'])">
                        <xsl:text>FREE|</xsl:text>
                    </xsl:if>
                </xsl:when>
                
                <xsl:otherwise>
                    
                </xsl:otherwise>
            </xsl:choose>                        
        </xsl:variable>
        <xsl:variable name="energiesFiltered">
            <xsl:if test="ends-with($energies,'|')">
                <xsl:value-of select="substring($energies,0,string-length($energies))"/>        
            </xsl:if>            
        </xsl:variable>
        <xsl:copy-of select="$energiesFiltered"/>
    </xsl:template>

    <xsl:function name="helper:hasPotentialEnergySection" as="xs:boolean">
        <xsl:param name="base"/>
        
        <xsl:choose>
            <!-- HF energy available?-->
            <xsl:when test="exists($base//scalar[@dictRef='g:rbhflyp'])">
                <xsl:value-of select="true()"/>
            </xsl:when>            
            <!-- Alternative methods available? -->
            <xsl:otherwise>                
                <xsl:variable name="archiveEnergies" select="$base//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@dictRef='g:archive.namevalue']"/>
                <xsl:variable name="method">                                          
                    <xsl:if test="$archiveEnergies != ''">
                        <xsl:for-each select="$archiveEnergies/scalar">
                            <xsl:variable name="field" select="."/>
                            <xsl:if test="matches($field,'.*=.*')">
                                <xsl:variable name="splittedField" select="tokenize($field,'=')"/>
                                <xsl:if test="gaussian:isMethod($splittedField[1])">
                                    <xsl:element name="energy">
                                        <xsl:attribute name="method"  select="$splittedField[1]"/>
                                        <xsl:attribute name="value" select="$splittedField[2]"/>
                                    </xsl:element>
                                </xsl:if>
                            </xsl:if>
                        </xsl:for-each>
                    </xsl:if>
                </xsl:variable>                
                <xsl:choose>
                    <xsl:when test="exists($method/*:energy)">
                        <xsl:value-of select="true()"/>
                    </xsl:when>
                </xsl:choose>
                <xsl:value-of select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>   
</xsl:stylesheet>