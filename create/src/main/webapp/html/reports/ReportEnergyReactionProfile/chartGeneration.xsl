<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.w3.org/1999/xhtml"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"    
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="title"/>
    <xsl:param name="javascriptEnergyParameters"/>
    <xsl:param name="webrootpath"/>
    <xsl:param name="energyType"/>
    <xsl:param name="energyUnits"/>

    <xsl:variable  name="energyPotential" select="'POTENTIAL'"/>
    <xsl:variable  name="energyFree" select="'FREE'"/>
    <xsl:variable  name="energyZeroPoint" select="'ZERO_POINT'"/>    
    <xsl:variable  name="energyEnthalpy" select="'ENTHALPY'"/>
    <xsl:variable  name="energyBonding" select="'BONDING'"/>
    <xsl:variable  name="energyNucRepulsion" select="'NUCREPULSION'"/>

    <xsl:variable name="energyLabel">
        <xsl:choose>
            <xsl:when test="matches($energyType, $energyFree)">Gibbs Energy </xsl:when>
            <xsl:when test="matches($energyType, $energyBonding)">Bonding Energy </xsl:when>
            <xsl:when test="matches($energyType, $energyEnthalpy)">Enthalpy </xsl:when>
            <xsl:otherwise>Energy</xsl:otherwise>            
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="energyDiffLabel">
        <xsl:choose>
            <xsl:when test="matches($energyType, $energyFree)">Δ G</xsl:when>
            <xsl:when test="matches($energyType, $energyEnthalpy)">Δ H</xsl:when>
            <xsl:otherwise>Δ E</xsl:otherwise>            
        </xsl:choose>
    </xsl:variable>

    <xsl:template match="/">
        <html lang="en">
            <head>        
                <title>Energy reaction profile</title>
                <meta charset="utf-8"></meta>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>                                                              
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>                                               
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />                        
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/style.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />                
            </head>
            <body>
                
                <div id="container">                 
                    <!-- Reaction energy profile chart -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <div id="energyReactionProfileDiv" style="min-width: 600px; height: 600px; max-width: 1000px; margin: 0 auto">
                            </div>                            
                        </div>
                        <div class="col-md-12">
                            <p class="text-right" style="margin:10px" role="alert">
                                *Any modification done to this chart using Plotly Chart Studio won't be saved at ioChem-BD platform.
                            </p>
                        </div>
                        <div class="col-md-12">                                
                            <table id="energies"/>          
                        </div>                        
                    </div>                    
                </div>
                
                <script type="text/javascript">
                    $(document).ready(function(){
                        buildEnergyChart();                   
                        buildEnergyDataSeries();                              
                    });
                    var d3 = Plotly.d3;
                    
                    var graph_title = '<xsl:value-of select="$title"/>';
                    var delta_label = '<xsl:value-of select="$energyDiffLabel"/>';
                    var yaxis_label = '<xsl:value-of select="$energyLabel"/> (<xsl:value-of select="$energyUnits"/>)';                    
                    <xsl:value-of select="$javascriptEnergyParameters"/>
                    <xsl:text disable-output-escaping="yes" >               
                    <![CDATA[  
                    /* Energy chart build function */
                    var colors = ['#ff0029', '#377eb8', '#66a61e', '#984ea3', '#00d2d5', '#ff7f00', '#af8d00', '#7f80cd', '#b3e900', '#c42e60', '#a65628', '#f781bf', '#8dd3c7', '#bebada', '#fb8072', '#80b1d3', '#fdb462', '#fccde5', '#bc80bd', '#ffed6f', '#c4eaff', '#cf8c00', '#1b9e77', '#d95f02', '#e7298a', '#e6ab02', '#a6761d', '#0097ff', '#00d067', '#000000', '#252525', '#525252', '#737373', '#969696', '#bdbdbd', '#f43600', '#4ba93b', '#5779bb', '#927acc', '#97ee3f', '#bf3947', '#9f5b00', '#f48758', '#8caed6', '#f2b94f', '#eff26e', '#e43872', '#d9b100', '#9d7a00', '#698cff'];                
                    function buildEnergyChart() {
                        energyReactionProfileDiv = document.getElementById('energyReactionProfileDiv');
                        var interval_width = 1;                        
                        var traces = [];
                        var solid_lines = [];
                        var step_annotations = [];
                        for(let i = 0; i < serieNames.length; i++) {
                            var count = 0;
                            var x_values = [];
                            var y_values = [];
                            var text_labels = [];
                            
                            for(let j = 0; j < dataSeries[i].length; j++) {
                                energy = dataSeries[i][j];
                                // Add serie energy step (dotted)
                                x_values.push(interval_width * count++);
                                x_values.push(interval_width * count++);
                                y_values.push(energy);
                                y_values.push(energy);
                                text_labels.push(stepNames[i][j]);
                                text_labels.push(stepNames[i][j]);
                                // Add solid line for serie energy step (solid)
                                solid_lines.push({
                                    layer: 'above',
                                    type: 'line',
                                    xref: 'x',
                                    yref: 'y',
                                    x0: interval_width * (count - 2),
                                    y0: energy,
                                    x1: interval_width * (count -1),
                                    y1: energy,                                    
                                    opacity: 1,
                                    line: {
                                        color: colors[i],
                                        width: 3
                                    },
                                customdata: {"index": i}           //Custom property used to show/hide this element on legend click
                                });
                                
                                //Add step annotation label
                                step_annotations.push({
                                    x: interval_width * (count - 2) + interval_width / 2, 
                                    y: energy,
                                    yshift: 10,
                                    align: "center", 
                                    showarrow:false,
                                    borderpad: 3, 
                                    borderwidth: 0, 
                                    font: {size: 14}, 
                                    text: stepNames[i][j], 
                                    textangle: 0,
				                    customdata: {"index": i}           //Custom property used to show/hide this element on legend click
                                });
                            }
                            
                            // Add serie
                            traces.push({
                                x: x_values,
                                y: y_values,
                                text: text_labels,
                                mode: 'lines',
                                marker: {        
                                    color: colors[i] 
                                },
                                name: serieNames[i] + '(' + delta_label + ': ' + deltaEnergies[i] + ')',
                                opacity: 1,
                                line: {
                                    dash: 'dot',
                                    width: 2
                                }
				                //hoverinfo:'none'
                            });
                        }

                        //Additional elements, solid lines for each serie step and its annotations
                        var layout = {
                            shapes: solid_lines,
                            annotations: step_annotations,
                            title: graph_title,
                            xaxis : { 'showline' : false,
                                      'ticks' : '',
                                      'showticklabels' : false
                                    },
                            yaxis : { 'title': yaxis_label},
                            legend : {'orientation': 'h'}
                        };
	  		
            			
                        Plotly.plot(energyReactionProfileDiv, traces, layout, {editable: false, showSendToCloud: true, plotlyServerURL: 'https://chart-studio.plotly.com'});
                        
                        energyReactionProfileDiv.on('plotly_legendclick', 
                                function(data) { 
                                    index=data.curveNumber;     
                                    visible = data.data[index].visible == 'legendonly' ||  data.data[index].visible == 'false' ?  true : false;
                                    for(i = 0; i < layout.shapes.length; i++) {
                                        var shape = layout.shapes[i];
                                        if(shape.customdata.index == index)
                                            shape.visible = visible;
                                    }                                    
                                    for(i = 0; i < layout.annotations.length; i++) {
                                        var annotation = layout.annotations[i];
                                        if(annotation.customdata.index == index)
                                            annotation.visible = visible;                    
                                    }  
                                });
                    }

                    /* Energy table build function */
                    function buildEnergyDataSeries() {
                        var columns = null;
                        var rows = null;
                        //Get max step number
                        var maxStepNumber = 0;               
                        for(inx = 0; inx < dataSeries.length; inx++)
                            if(dataSeries[inx].length > maxStepNumber)
                                maxStepNumber = dataSeries[inx].length;
                         rows = new Array(dataSeries.length * 2);
                                         
                         for(inx = 0; inx < dataSeries.length; inx++){                    
                            rows[inx * 2] = new Array(maxStepNumber+2);                                                
                            rows[(inx * 2) + 1 ] = new Array(maxStepNumber+2);
                            
                            rows[inx * 2][0] = '';
                            rows[(inx * 2) + 1][0] = serieNames[inx];
                            
                            for(inx2 = 0; inx2 < maxStepNumber; inx2++){
                                if(typeof stepNames[inx][inx2] === 'undefined'){
                                      rows[inx * 2][inx2 + 1] = ''                                  
                                }else{
                                      rows[inx * 2][inx2 + 1] = stepNames[inx][inx2];
                                }
                                if(typeof dataSeries[inx][inx2] === 'undefined'){
                                    rows[(inx * 2) + 1][inx2 + 1] = ''
                                }else{
                                    rows[(inx * 2) + 1][inx2 + 1] = dataSeries[inx][inx2];
                                }
                            }
                            rows[inx * 2][maxStepNumber + 1] = '';
                            rows[(inx * 2) + 1 ][maxStepNumber + 1] = deltaEnergies[inx];                  
                         }
                          
                         columns = new Array(maxStepNumber + 2);
                         columns[0] = {"sTitle" : "Series"};
                         for(inx = 1; inx <= maxStepNumber; inx++){
                            columns[inx] = {"sTitle" : "Step", "sClass": "text-center"};
                         }
                         columns[maxStepNumber + 1] = {"sTitle" : "Delta"};

                        $("table#energies").dataTable( {
                            "aaData" : rows,
                            "aoColumns" : columns,
                            "bFilter": false,
                            "bPaginate": false,
                            "bSort": false,
                            "bInfo": false
                         } );
                     }

                     ]]>
                    </xsl:text>
                </script>
                
                <script type="text/javascript">
                    $(window).on('load', function(){
                        //Add custom styles to tables
                        $('div.dataTables_wrapper').each(function(){ 
                            $(this).children("table").addClass("compact");
                            $(this).children("table").addClass("display");
                        });          
                        //Add download action to every dataTable                         
                        $('div.dataTables_wrapper').each(function(){ 
                            var tableName = $(this).children("table").attr("id");
                            if(tableName != null){
                                var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');                                
                            }
                        });                                                                        
                    });
                    
                    function showDownloadOptions(tableName){                            
                        var table = $('#' + tableName).DataTable();                                                    
                        new $.fn.dataTable.Buttons( table, {
                            buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                        } );                            
                        table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                        $('div#downloadTable' + tableName + ' span.fa').hide();
                    }
                </script>
                
            </body>
        </html>       
    </xsl:template>
    
</xsl:stylesheet>
