<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet   
    xmlns="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:convention="http://www.xml-cml.org/convention/"
    xmlns:cc="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:compchem="http://www.xml-cml.org/dictionary/compchem/"
    xmlns:xsd="http://www.w3.org/2001/XMLSchema"
    xmlns:si="http://www.xml-cml.org/unit/si/" 
    xmlns:nonsi="http://www.xml-cml.org/unit/nonSi/"
    xmlns:nonsi2="http://www.xml-cml.org/unit/nonSi2/"
    xmlns:cml="http://www.xml-cml.org/dictionary/cml/"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:adf="http://www.scm.com/ADF/"
    xmlns:gaussian="http://www.gaussian.com/"  
    xmlns:v="http://www.iochem-bd.org/dictionary/vasp/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns:math="http://www.w3.org/2005/xpath-functions/math"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="#all"    
    version="3.0">
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/adf.xsl"/>
    <xsl:include href="../../xslt/helper/chemistry/gaussian.xsl"/>
    <xsl:include href="../../xslt/helper/math/fxsl/product.xsl"/>
    
    
    <xsl:output method="xml" indent="yes" omit-xml-declaration="yes"/>
    <xsl:strip-space elements="*"/>
    
    <xsl:variable name="energyTypePotential" select="'POTENTIAL'"/>
    <xsl:variable name="energyTypeFree"      select="'FREE'"/>
    <xsl:variable name="energyTypeZeroPoint" select="'ZERO_POINT'"/>
    <xsl:variable name="energyTypeEnthalpy"  select="'ENTHALPY'"/>
    <xsl:variable name="energyTypeBonding"   select="'BONDING'"/> 
    <xsl:variable name="energyTypeNucRepulsion"   select="'NUCREPULSION'"/>
    
    <xsl:param name="isGibbsCorrected" as="xsd:boolean"/>    
    <xsl:param name="pressure" select="number('298.15')" />
    <xsl:param name="temperature"  select="number('1.0')" />
    
    <xsl:param name="title"/>
    <xsl:param name="energyType"/>
    <xsl:param name="energyUnits"/>
    <xsl:param name="negativeFrequencyThreshold"/>

    <xsl:template match='/module'>
        <template id="calculation" name="{$title}">
            <xsl:call-template name="retrieveTitle"/>         
            <xsl:call-template name="retrieveEnergy"/>         
            <xsl:call-template name="retrieveFinalGeometry"/>
            <xsl:call-template name="isTransitionState" />
        </template>
    </xsl:template>
    
    <xsl:template name="retrieveTitle">
        <scalar id="title"><xsl:value-of select="$title"/></scalar>
    </xsl:template>
    
    <xsl:template name="retrieveEnergy">
        <xsl:variable name="program" select="upper-case((//parameter[@dictRef='cc:program'])[last()])" />         
        
        <xsl:if test="$energyType">
            <xsl:variable name="energyValue">
                <xsl:choose>
                    <xsl:when test="matches($program, 'GAUSSIAN')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">
                                <xsl:value-of select="helper:getGaussianPotentialEnergy(., $energyUnits)"></xsl:value-of>                                                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:variable name="freeEnergy" select="(//scalar[@dictRef='cc:zpe.sumelectthermalfe'])[last()]" />
                                <xsl:choose>
                                    <xsl:when test="$isGibbsCorrected">   
                                        <xsl:value-of select="helper:convertEnergyUnits(helper:gibbsCorrected($program, .), $energyUnits)"/>                                        
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="helper:convertEnergyUnits($freeEnergy,$energyUnits)"/>
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:zpe.sumelectzpe'])[last()],$energyUnits)"/>                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeEnthalpy) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:zpe.sumelectthermalent'])[last()],$energyUnits)"/>                        
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>
                        </xsl:choose>        
                    </xsl:when>
                    <xsl:when test="matches($program, 'ADF')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypeBonding) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='bonding.energy']//scalar[@dictRef='cc:total'])[last()],$energyUnits)"/>
                            </xsl:when>                           
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:choose>
                                    <xsl:when test="$isGibbsCorrected">
                                        <xsl:value-of select="helper:convertEnergyUnits(helper:gibbsCorrected($program, .), $energyUnits)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:variable name="bonding" select="number((//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='bonding.energy']/module[@cmlx:templateRef='summary']/scalar[@dictRef='cc:total'])[last()])"/>
                                        <xsl:variable name="internal" select="number((//list[@cmlx:templateRef='internalEnergy']/scalar[@dictRef='cc:total'])[last()])" />
                                        <xsl:variable name="entropy" select="number((//list[@cmlx:templateRef='entropy']/scalar[@dictRef='cc:total'])[last()])"/>
                                        <xsl:variable name="thermochemistry" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]"/>
                                        <xsl:variable name="temp" select="$thermochemistry/module[@cmlx:templateRef='energies']/scalar[@dictRef='cc:temp']"/>
                                        <xsl:variable name="free" select="(($bonding * 96.48531 * 1000 +  $internal * 4184 ) + (8.31441 * number($temp)) - (number($temp) * $entropy * 4.184)) div 1000"/> <!-- Units in kJ/mol -->
                                        <xsl:variable name="freeEnergy">
                                            <xsl:element name="scalar">
                                                <xsl:attribute name="units"><xsl:text>nonsi:kj.mol-1</xsl:text></xsl:attribute>
                                                <xsl:value-of select="$free"/>
                                            </xsl:element>
                                        </xsl:variable>   
                                        
                                        <xsl:value-of select="helper:convertEnergyUnits($freeEnergy/scalar,$energyUnits)"/>                                               
                                    </xsl:otherwise>
                                </xsl:choose>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">
                                <xsl:variable name="zeroPoint" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:zeropoint']/scalar)[last()]"/>                                
                                <xsl:value-of select="helper:convertEnergyUnits($zeroPoint,$energyUnits)"/>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeEnthalpy) = 0">
                                <xsl:variable name="bonding" select="number((//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='bonding.energy']/module[@cmlx:templateRef='summary']/scalar[@dictRef='cc:total'])[last()])"/>
                                <xsl:variable name="internal" select="number((//list[@cmlx:templateRef='internalEnergy']/scalar[@dictRef='cc:total'])[last()])" />
                                <xsl:variable name="entropy" select="number((//list[@cmlx:templateRef='entropy']/scalar[@dictRef='cc:total'])[last()])"/>
                                <xsl:variable name="thermochemistry" select="(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry'])[last()]"/>
                                <xsl:variable name="temp" select="$thermochemistry/module[@cmlx:templateRef='energies']/scalar[@dictRef='cc:temp']"/>
                                <xsl:variable name="free" select="((($bonding * 96.48531 * 1000 +  $internal * 4184 ) + (8.31441 * number($temp)) - (number($temp) * $entropy * 4.184)) div 1000) div 4.184"/> <!-- Units in kcal.mol-1 -->
                                
                                <xsl:variable name="enthalpy">
                                    <xsl:element name="scalar">
                                        <xsl:attribute name="units"><xsl:text>nonsi2:kcal.mol-1</xsl:text></xsl:attribute>
                                        <xsl:value-of select="(($entropy div 1000) * $temp) + $free"/>
                                    </xsl:element>
                                </xsl:variable>                                
                                <xsl:value-of select="helper:convertEnergyUnits($enthalpy/scalar,$energyUnits)"/>
                                
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                    <xsl:when test="matches($program, 'TURBOMOLE')">
                        <xsl:variable name="energy" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:energy']/scalar)[last()]"/>
                        <xsl:variable name="energy2" select="(//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@id='energy.resume']/scalar[@dictRef='t:rhfEnergy'])[last()]"/>
                        <xsl:variable name="zeropoint" select="(//module[@dictRef='cc:finalization']/propertyList/property[@dictRef='t:zeropoint']/scalar)[last()]" xpath-default-namespace="http://www.xml-cml.org/schema"></xsl:variable>
                        <xsl:variable name="nucrepulsion" select="(//module[@dictRef='cc:finalization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='nuclear.repulsion']/scalar[@dictRef='cc:nucrepener'])[last()]"/>                        
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">                                
                                <xsl:choose>
                                    <xsl:when test="exists($energy)">
                                        <xsl:value-of select="helper:convertEnergyUnits($energy, $energyUnits)"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="helper:convertEnergyUnits($energy2, $energyUnits)"/>
                                    </xsl:otherwise>
                                </xsl:choose>                                
                            </xsl:when>                            
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">                
                                <xsl:choose>
                                    <xsl:when test="exists($energy)">
                                        <xsl:value-of select="number(helper:convertEnergyUnits($zeropoint,$energyUnits)) + number(helper:convertEnergyUnits($energy, $energyUnits))"/>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <xsl:value-of select="number(helper:convertEnergyUnits($zeropoint,$energyUnits)) + number(helper:convertEnergyUnits($energy2, $energyUnits))"/>
                                    </xsl:otherwise>
                                </xsl:choose>                               
                            </xsl:when>                           
                            <xsl:when test="compare($energyType, $energyTypeNucRepulsion) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits($nucrepulsion, $energyUnits)"/>
                            </xsl:when>                            
                        </xsl:choose>
                    </xsl:when>                                       
                    <xsl:when test="matches($program, 'ORCA')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">                           
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:totalener'])[last()],$energyUnits)"/>                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='enthalpy']/scalar[@dictRef='o:totalfree'])[last()],$energyUnits)"/>
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeZeroPoint) = 0">                                
                                <xsl:variable name="value">
                                    <xsl:element name="scalar">
                                        <xsl:attribute name="units"><xsl:value-of select="(//module[@cmlx:templateRef='enthalpy'])[last()]/scalar[@dictRef='o:totalfree']/@units"/></xsl:attribute>                                    
                                        <xsl:value-of select="number((//module[@cmlx:templateRef='enthalpy'])[last()]/scalar[@dictRef='o:totalfree'])
                                                                +   
                                                              number((//module[@cmlx:templateRef='enthalpy'])[last()]/scalar[@dictRef='o:thermalcorrenthalpy'])"/>
                                    </xsl:element>
                                </xsl:variable>
                                <xsl:value-of select="helper:convertEnergyUnits($value/scalar,$energyUnits)"/>                        
                            </xsl:when>                          
                            <xsl:when test="compare($energyType, $energyTypeEnthalpy) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='gibbs']/scalar[@dictRef='o:gibbsenthalpy'])[last()],$energyUnits)"/>                                
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeNucRepulsion) = 0">                                
                                <xsl:value-of select="helper:convertEnergyUnits((//module[@cmlx:templateRef='totalenergy']/scalar[@dictRef='cc:nucrepener'])[last()],$energyUnits)"/>                                
                            </xsl:when>
                            <xsl:otherwise>                                
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>                          
                        </xsl:choose>
                    </xsl:when>                
                    <xsl:when test="matches($program, 'VASP')">
                        <xsl:choose>
                            <xsl:when test="compare($energyType, $energyTypePotential) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:e0'])[last()], $energyUnits)"/>                                                        
                            </xsl:when>
                            <xsl:when test="compare($energyType, $energyTypeFree) = 0">
                                <xsl:value-of select="helper:convertEnergyUnits((//scalar[@dictRef='cc:freeEnergy'])[last()],$energyUnits)"/>
                            </xsl:when>                           
                            <xsl:otherwise>
                                <xsl:value-of select="'N/A'"/>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:when>
                </xsl:choose>
            </xsl:variable>
            <scalar name="energy" type="{$energyType}" units="{$energyUnits}"><xsl:value-of select="$energyValue"/></scalar>                
        </xsl:if>
    </xsl:template>
     
    <xsl:function name="helper:getGaussianPotentialEnergy">
        <xsl:param name="base"/>        
        <xsl:param name="units"/>        
        <xsl:variable name="archiveEnergies" select="$base//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@dictRef='g:archive.namevalue']"/>
        <xsl:variable name="method">
                       
            <xsl:variable name="hf" select="helper:getGaussianHFEnergy($base)"/>
            <xsl:if test="$hf != ''">
                <xsl:element name="energy">
                    <xsl:attribute name="method"  select="'hf'" />
                    <xsl:attribute name="value" select="$hf/text()" />
                    <xsl:attribute name="units" select="$hf/@units" />
                    <xsl:value-of select="$hf/text()"/>
                </xsl:element>
            </xsl:if>            
                        
            <xsl:if test="$archiveEnergies != ''">
                <xsl:for-each select="$archiveEnergies/scalar">
                    <xsl:variable name="field" select="."/>
                    <xsl:if test="matches($field,'.*=.*')">
                        <xsl:variable name="splittedField" select="tokenize($field,'=')"/>
                        <xsl:if test="gaussian:isMethod($splittedField[1])">
                            <xsl:element name="energy">
                                <xsl:attribute name="method"  select="$splittedField[1]"/>
                                <xsl:attribute name="value" select="$splittedField[2]"/>
                                <xsl:attribute name="units" select="'nonsi:hartree'"/>
                                <xsl:value-of select="$splittedField[2]"/>
                            </xsl:element>
                        </xsl:if>
                    </xsl:if>
                </xsl:for-each>
            </xsl:if>
    </xsl:variable>
        <xsl:if test="exists($method/*:energy)">
            <xsl:variable name="finest" select="$method/*:energy[last()]"/>     <!-- Capture last (and more accurate) method energy -->
            <xsl:value-of select="helper:convertEnergyUnits($finest, $units)"/>            
        </xsl:if>        
    </xsl:function>

    <xsl:function name="helper:getGaussianHFEnergy">
        <xsl:param name="base" />
        <xsl:copy-of select="
                    if(exists($base//module[@cmlx:templateRef='l50X']/scalar[@dictRef='g:rbhflyp'])) then
                        ($base//module[@cmlx:templateRef='l50X']/scalar[@dictRef='g:rbhflyp'])[last()]
                    else if(exists($base//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])) then
                        ($base//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])[last()]
                    else if(exists($base//module[@cmlx:templateRef='l508']/scalar[@dictRef='g:rbhflyp'])) then
                        ($base//module[@cmlx:templateRef='l508']/scalar[@dictRef='g:rbhflyp'])[last()]
                    else ''"/>                              
    </xsl:function>

    <xsl:template name="retrieveFinalGeometry" >              
        <xsl:copy-of select="(.//molecule)[last()]" copy-namespaces="no"/>            
    </xsl:template>
            
    <xsl:template name="isTransitionState">
        <xsl:variable name="program" select="upper-case((//parameter[@dictRef='cc:program'])[last()])" />                 
        <xsl:variable name="threshold" select="number($negativeFrequencyThreshold)"/>
        <scalar name="isTransitionState" >            
    		<xsl:choose>
    		    <xsl:when test="exists(//module[@dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])">                
    		        <xsl:variable name="frequencyArray"> 
    		            <xsl:choose>
    		                <xsl:when test="matches($program, 'ADF')"> <!-- Specific ADF SCANFREQ correction -->
    		                    <xsl:variable name="correctedFrequencies" select="adf:getFrequencies(
                                                                                                        .//module[@id='finalization']/propertyList/property[@dictRef='cc:frequencies']/module[@dictRef='cc:vibrations'],
                                                                                                        .//module[@id='finalization']//property/module[@cmlx:templateRef='scanfreq']
                        		                                                                      )"/>
    		                    <xsl:copy-of select="helper:trim(replace(($correctedFrequencies//array[@dictRef='cc:frequency'])[position() = last()], '^(\d+\.\d+(?:[Ee][\-\+]\d+)?)|(\s+\d+\.\d+(?:[Ee][\-\+]\d+)?)', ''))"/>
    		                </xsl:when>
    		                <xsl:otherwise>
    		                    <xsl:copy-of select="helper:trim(replace((//module[@dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])[position() = last()], '^(\d+\.\d+(?:[Ee][\-\+]\d+)?)|(\s+\d+\.\d+(?:[Ee][\-\+]\d+)?)', ''))"/>                                          
    		                </xsl:otherwise>
    		            </xsl:choose>
    		        </xsl:variable>
                  
                    
                    <xsl:variable name="negativeFrequencies">
                        <xsl:for-each select="tokenize($frequencyArray, '\s+')">
                            <xsl:variable name="negativeFrequency" select="."/>
                            <xsl:if test="number($negativeFrequency) &lt; number($threshold)">
                                <xsl:element  namespace="http://www.xml-cml.org/schema" name="frequency"><xsl:value-of select="$negativeFrequency"/></xsl:element>
                            </xsl:if>    
                        </xsl:for-each>    
                    </xsl:variable>                  
    		                           
    		        <xsl:value-of select="count($negativeFrequencies/frequency) = 1"/>
    		    </xsl:when>
    		    <xsl:otherwise>
    		        <xsl:value-of select="'false'"/>
    		    </xsl:otherwise>
    		</xsl:choose> 
        </scalar>                                       
    </xsl:template>

    <!-- Corrected Gibbs energy section -->
    <xsl:variable name="h_joule" select="number('6.62607015E-34')" as="xsd:double"/>
    <xsl:variable name="kb" select="number('1.380649E-23')" as="xsd:double" />
    <xsl:variable name="h" select="$h_joule * $c * 100" />
    <xsl:variable name="c" select="number('299792458.0')" as="xsd:double"/> <!-- Units: m s^-1 -->
    <xsl:variable name="R_SI" select="number('8.314462618')" as="xsd:double" />
    <xsl:variable name="Navog" select="number('6.02214076E+23')" as="xsd:double" />
    <xsl:variable name="amu_to_kg" select="number('1.6605390666E-27')" as="xsd:double"/>
    <xsl:variable name="bohr_to_m" select="number('5.29177210903E-11')"  as="xsd:double"/>
    <xsl:variable name="hartree_to_j" select="number('4.3597447222071E-18')"  as="xsd:double"/>
    <xsl:variable name="eV_to_J" select="number('1.602176634E-19')"  as="xsd:double"/>
    <xsl:variable name="eV_to_au" select="number('0.03674930495')" as="xsd:double" />
    <xsl:variable name="R_kcal" select="0.0019872042586042064"  as="xsd:double"/>
    <xsl:variable name="R_hartree" select="number('3.166811563397242E-06')"  as="xsd:double"/>
    <xsl:variable name="kcalmol_to_hartree" select="4184 div ($hartree_to_j * $Navog)" as="xsd:double"/>
    <xsl:variable name="atm_to_Pa" select="number(101325)" as="xsd:double" /> 
    <xsl:variable name="adfFreqLimit" as="xsd:integer" select="20" />
    <xsl:variable name="gaussianFreqLimit" as="xsd:integer" select="0" />
    
    <xsl:variable name="CORR_TYPE_GRIMME" select="'Grimme'" />
    <xsl:variable name="CORR_TYPE_RRHO" select="'RRHO'" />
    
    <xsl:function name="helper:gibbsCorrected">
        <xsl:param name="program" />
        <xsl:param name="context" />  
                
        <!-- Retrieve parameters -->        
        <xsl:variable name="sigmaVal" select="if(matches($program, 'GAUSSIAN')) then ($context//property[@dictRef='cc:thermochemistry']/list[@id='l716.thermochemistry']/scalar[@dictRef='cc:symmnumber'])[position() = last()]
            else ($context//property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry']/scalar[@dictRef='cc:symmnumber'])[position() = last()]" />  
        
        <xsl:variable name="sigma" select="if(exists($sigmaVal)) then number($sigmaVal) else 999" />    
        
        <xsl:variable name="frequencies" select="if(matches($program, 'GAUSSIAN')) then tokenize(($context//module[@cmlx:templateRef='l716.freq.chunkx' and @dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])[position() = last()],'[\s]+')
            else helper:readADFFrequencies(($context//property[@dictRef ='cc:frequencies']/module[@dictRef='cc:vibrations']/array[@dictRef='cc:frequency'])[position() = last()],
                                              ($context//property/module[@cmlx:templateRef='scanfreq']/list[@cmlx:templateRef='scanfreq'])[position() = last()]) "/>
        
        <xsl:variable name="e_elec" select="
            if(matches($program, 'GAUSSIAN')) then
                helper:getGaussianFreqsHFEnergy($context) * $hartree_to_j                     
            else 
                ($context//module[@cmlx:templateRef='summary']/scalar[@dictRef='cc:total'])[last()] * $eV_to_au * $hartree_to_j"
            />
        
        <xsl:variable name="molmass" select="if(matches($program, 'GAUSSIAN')) then $context//property[@dictRef='cc:thermochemistry']/list[@id='l716.thermochemistry']/scalar[@dictRef='cc:molmass'] * $amu_to_kg
            else number(helper:sumStringArray($context//module[@cmlx:templateRef='adf.frequencyanalysis']/module[@cmlx:templateRef='masses']/array[@dictRef='cc:atomicmass'], '[\s]+')) * $amu_to_kg" />
        
        <!-- RRHO is set by default as correction type -->
        <xsl:variable name="S_corr_type" select="$CORR_TYPE_RRHO" />
                
        <xsl:variable name="rot_temps" select="tokenize(helper:rotation_reading($program, $context), '[\s+]')" />
        
        <xsl:variable name="thermal_energy" select="helper:getThermalEnergy($program, $sigma, $frequencies, $e_elec, $rot_temps)"/>
                
        <xsl:variable name="entropy" select="helper:getEntropy($program, $context, $e_elec, $rot_temps, $molmass, $sigma, $frequencies, $S_corr_type)" />
        
        <!-- Correction of thermal contribution for linear molecules: was considered as 3/2 RT but shall be RT -->
        <xsl:variable name="thermal_correction" select=" if(exists($rot_temps) and count($rot_temps) = 1) then 0.5 else 0.0" />
        
        
        <xsl:variable name="entropyTermCorrection" select="(number($thermal_energy/@e_total) - $thermal_correction) + 1 - number($entropy/@s_total)" />
                    
        <xsl:element name="scalar">
            <xsl:attribute name="dataType" select="'xsd:double'" />
            <xsl:attribute name="dictRef" select="'cc:correctedGibbs'" />
            <xsl:attribute name="units" select="'nonsi:hartree'" />
            <xsl:value-of select="$entropyTermCorrection * $R_kcal* $temperature* $kcalmol_to_hartree"/>                        
        </xsl:element>                    
    </xsl:function>
    
    <xsl:function name="helper:getGaussianFreqsHFEnergy">
        <xsl:param name="context" />
        <xsl:variable name="freqsJob" select="($context//property[@dictRef='cc:frequencies']/ancestor::module[@id='job'])[last()]" />
        <xsl:value-of select="helper:getGaussianHFEnergy($freqsJob)"/>
    </xsl:function>
    
    <xsl:function name="helper:getThermalEnergy">
        <xsl:param name="program" />
        <xsl:param name="sigma" />
        <xsl:param name="frequencies" />
        <xsl:param name="e_elec" />
        <xsl:param name="rot_temps" />

        <xsl:variable name="e_transl" select="1.5" />
        <xsl:variable name="e_rot" select="if($sigma = 999) then 0.0 else 1.5" />
        <xsl:variable name="e_vib" select="helper:evibr_rrho($program, $frequencies)" />   
        
       <xsl:variable name="energy">
            <xsl:element name="energy">
                <xsl:attribute name="e_thermal" select="$e_transl + $e_rot + $e_vib" />
                <xsl:attribute name="e_total" select="($e_transl + $e_rot + $e_vib) + $e_elec div ( $kb * $temperature)" />
            </xsl:element>
        </xsl:variable>
        <xsl:copy-of select="$energy/energy"/>                 
    </xsl:function> 
    
    <xsl:function name="helper:readADFFrequencies" as="xsd:string*">
        <xsl:param name="freqs" />
        <xsl:param name="scanfreq" />

        <xsl:variable name="freqArray" select="tokenize($freqs,'[\s]+')"/>      
        <xsl:choose>
            <xsl:when test="exists($scanfreq)">
                <!-- On SCANFREQ calculations will replace original frequency values for the new calculated ones -->               
                <xsl:variable name="oldValues" select="tokenize($scanfreq/array[@dictRef='a:oldfreq']/text(),'[\s+]')"/>
                <xsl:variable name="newValues" select="tokenize($scanfreq/array[@dictRef='a:newfreq']/text(),'[\s+]')"/>
                
                <xsl:variable name="translateMap">
                    <xsl:element name="scanfreqTranslate">
                        <xsl:for-each select="1 to count($oldValues)">
                            <xsl:variable name="outerIndex" select="."/>
                            <xsl:element name="translate">
                                <xsl:attribute name="old" select="$oldValues[$outerIndex]"/>
                                <xsl:attribute name="new" select="$newValues[$outerIndex]"/>
                            </xsl:element>
                        </xsl:for-each>                    
                    </xsl:element>                                      
                </xsl:variable>
                
                
                <xsl:for-each select="$freqArray">
                    <xsl:variable name="freq" select="."/>                    
                    <xsl:choose>
                        <xsl:when test="exists($translateMap/*:scanfreqTranslate/*:translate[@old = $freq])">
                            <xsl:value-of select="$translateMap/*:scanfreqTranslate/*:translate[@old = $freq]/@new" /> 
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$freq"/>                            
                        </xsl:otherwise>                                                      
                    </xsl:choose> 
                </xsl:for-each>
                
                
                
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of select="$freqArray"/>
            </xsl:otherwise>
        </xsl:choose>
        
        
    </xsl:function>
    
    <xsl:function name="helper:getEntropy">
        <xsl:param name="program" as="xsd:string" />               
        <xsl:param name="context" />
        <xsl:param name="energies" />
        <xsl:param name="rot_temps" />
        <xsl:param name="molmass" />
        <xsl:param name="sigma" />
        <xsl:param name="frequencies" />
        <xsl:param name="S_corr_type" />

        <xsl:variable name="pressurePa" select="$pressure * $atm_to_Pa"/>
        <xsl:variable name="St_1" select="2 * math:pi() * $molmass * $kb * $temperature div math:pow($h_joule,2)"/>
        <xsl:variable name="St_2" select="$kb * $temperature div $pressurePa* math:exp(5 div 2)" />
        <xsl:variable name="S_transl" select="3 div 2 * math:log($St_1) + math:log($St_2)" />
            
        
        <xsl:variable name="S_rot">
            <xsl:choose>
                <xsl:when test="count($rot_temps) = 1"> <!-- Linear molecule -->
                    <xsl:value-of select="math:log((math:exp(1) * $temperature) div ($sigma * number($rot_temps[1])))"/>
                    <!--
                        #Correction of thermal contribution for linear molecules: was considered as 3/2 RT but shall be RT
                        GibbsHandlerObj.ThermalEnergy=GibbsHandlerObj.ThermalEnergy-0.5
                        GibbsHandlerObj.TotalEnergy=GibbsHandlerObj.TotalEnergy-0.5
                    -->
                    <!--
                        if (print_now == True):
                        print("="*21)
                        print("Linear molecule identified")
                        print("Correcting energy: used 3/2 RT for rot. contribution, but for lin. mol. is RT")
                        print("Thermal*=  ",(GibbsHandlerObj.ThermalEnergy*R_kcal*temp))
                        print("Ecorr*=	",(GibbsHandlerObj.energy/(hartree_to_J*kcalmol_to_hartree)+GibbsHandlerObj.ThermalEnergy*R_kcal*temp)*kcalmol_to_hartree)
                        print("="*21)    
                    -->
                </xsl:when>
                <xsl:when test="count($rot_temps) = 0">  <!-- Monoatomic: no rotation at all -->
                    <xsl:value-of select="0.0"/>                    
                </xsl:when>
                <xsl:otherwise> <!-- General case: polyatomic molecule -->                    
                    <xsl:variable name="prod_rot_temps">
                        <xsl:call-template name="product">
                            <xsl:with-param name="pList" select="helper:nodesFromStringArray($rot_temps)/value"/>
                        </xsl:call-template>
                    </xsl:variable>                                             
                    <xsl:value-of select="0.5*math:log(math:pi() * math:exp(3) * math:pow($temperature, 3) div (math:pow($sigma, 2) * $prod_rot_temps))"/>
                </xsl:otherwise>                 
            </xsl:choose>        
        </xsl:variable>
        
        
        <xsl:variable name="S_vibr" select="if(matches($S_corr_type, $CORR_TYPE_GRIMME)) then helper:svibr_grimme($frequencies,$temperature,$program)
                                                                                         else helper:svibr_rrho($frequencies,$temperature,$program)" /> 
                 
        <xsl:variable name="S_total" select="$S_transl + $S_rot + number($S_vibr)"/>
        <xsl:variable name="S_terms" select="concat($S_transl, ' ', $S_rot, ' ', $S_vibr)" />       
        <!--
            #Conversion factor to get in cal/(mol K) to compare with G09 results
            if (print_now == True):
            print("S= \t %.4f kcal/mol" % (S_total*R_kcal*1000))
            print("Contributions to entropy (in cal mol-1 K-1): translation, rotation, vibration")
            print("%.4f    %.4f    %.4f" % (S_transl*R_kcal*1000,S_rot*R_kcal*1000,S_vibr*R_kcal*1000))
            
            return S_total,S_terms
        
        -->
        <xsl:variable name="energy">
            <xsl:element name="energy">
                <xsl:attribute name="s_total" select="$S_total" />
                <xsl:attribute name="s_terms" select="$S_terms" />
            </xsl:element>
        </xsl:variable>
        <xsl:copy-of select="$energy/energy" />      
    </xsl:function>
    
    <xsl:function name="helper:rotation_reading">
        <xsl:param name="program" />
        <xsl:param name="context" />        
        <xsl:choose>
            <xsl:when test="matches($program, 'GAUSSIAN')">
                <xsl:value-of select="tokenize(($context//property[@dictRef='cc:thermochemistry']/list[@id='l716.thermochemistry']/array[@dictRef='cc:rottemp'])[position() = last()],'[\s+]')"/>                                                            
            </xsl:when>
            <xsl:when test="matches($program, 'ADF')">                 
                <xsl:variable name="rot_temps">
                    <xsl:element name="temps">
                        <xsl:choose>
                            <xsl:when test="matches($context//property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry']/array[@dictRef='cc:moi'],'0\.0000')">
                                <xsl:variable name="moi_nozero" select="distinct-values(tokenize(replace($context//property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry']/array[@dictRef='cc:moi'],'\s*0\.0000\s*',''),'[\s+]'))" />
                                <!-- Transform to kg m² and get rot. temperatures in K -->
                                <xsl:variable name="Ivalue_SI" select="number($moi_nozero[1]) * $amu_to_kg * math:pow($bohr_to_m, 2)" />
                                <xsl:variable name="theta" select="math:pow($h_joule,2) div (8 * math:pow(math:pi(),2) * $kb * $Ivalue_SI)" />
                                <xsl:element name="rot_temps">
                                    <xsl:attribute name="rot_temps" select="$theta"/>                                    
                                </xsl:element>
                            </xsl:when>
                            <xsl:otherwise> 
                                <xsl:variable name="moi" select="tokenize($context//property[@dictRef='cc:thermochemistry']/module[@cmlx:templateRef='thermochemistry']/array[@dictRef='cc:moi'],'[\s+]')"/>
                                <xsl:for-each select="$moi">
                                    <xsl:variable name="Ivalue" select="."/>                     
                                    <!-- Transform to kg m² and get rot. temperatures in K -->
                                    <xsl:variable name="Ivalue_SI" select="number($Ivalue) * $amu_to_kg * math:pow($bohr_to_m, 2)" />
                                    <xsl:variable name="theta" select="math:pow($h_joule,2) div (8 * math:pow(math:pi(),2) * $kb * $Ivalue_SI)" />
                                    <xsl:element name="rot_temps">
                                        <xsl:attribute name="rot_temps" select="$theta"/>                                    
                                    </xsl:element>                                
                                </xsl:for-each>
                            </xsl:otherwise>
                        </xsl:choose>
                    </xsl:element>
                </xsl:variable>
                <xsl:value-of select="$rot_temps/temps/rot_temps/@rot_temps"/>
            </xsl:when>
        </xsl:choose>        
    </xsl:function>
    
    <xsl:function name="helper:evibr_rrho">
        <xsl:param name="program" />
        <xsl:param name="freqlist" />  
        
        <xsl:variable name="temp" select="$temperature"/>                        
        
        <xsl:variable name="correctedVibs">
            <xsl:for-each select="$freqlist">
                <xsl:variable name="freq" select="number(.)"/>
                <xsl:if test="(matches($program, 'ADF') and $freq &gt;$adfFreqLimit) or (matches($program,'GAUSSIAN') and $freq &gt; $gaussianFreqLimit)">
                    <xsl:element name="node">
                        <xsl:variable name="theta_v" select="$h * $freq div $kb" />
                        <xsl:variable name="term1" select="$theta_v div (2 * $temp)" />
                        <xsl:variable name="term2" select="($theta_v div $temp) div (math:exp($theta_v div $temp) - 1)" />
                        <xsl:variable name="term" select="$term1 + $term2" />
                        <xsl:value-of select="$term"/>                        
                    </xsl:element>                    
                </xsl:if>
            </xsl:for-each>                        
        </xsl:variable>       
        <xsl:value-of select="sum($correctedVibs/node/text())" />
    </xsl:function>

    <xsl:function name="helper:svibr_grimme">
        <xsl:param name="freqlist" />
        <xsl:param name="temp" />
        <xsl:param name="program" />
        
        <xsl:variable name="f_cutoff" select="100" />		 <!-- in cm-1, from Grimme Chem. Eur. J. 2012 -->
        <xsl:variable name="Bav" select="number('10E-44')" /> <!-- in kg·m2, from Grimme Chem. Eur. J. 2012 -->        
        <xsl:variable name="S_vibr">
            <xsl:for-each select="$freqlist">
                <xsl:variable name="freq" select="number(.)"/>
                <xsl:if test="(matches($program, 'ADF') and $freq &gt;$adfFreqLimit) or (matches($program,'GAUSSIAN') and $freq &gt; $gaussianFreqLimit)">
                    <!-- Both RRHO and pseudorrotational contributions must be computed, then weighted-->
                    <!-- Pseudorrotational approach (Grimme Chem. Eur. J. 2012) -->
                    <xsl:variable name="freq_hz" select="$freq * 100 * $c" />
                    <xsl:variable name="m_inertia" select="$h_joule div (8 * math:pow(math:pi(), 2) * $freq_hz)"/>
                    <xsl:variable name="m_inertia_eff" select="$m_inertia * $Bav div($m_inertia + $Bav)"/>
                    <xsl:variable name="logterm" select="8 * math:pow(math:pi(), 3) * $m_inertia_eff * $kb * $temp div math:pow($h_joule, 2)" />
                    <!-- RRHO approximation -->
                    <xsl:variable name="s_vibr_pseudorrot" select="0.5 *(1 + math:log($logterm))" />
                    <xsl:variable name="theta_v" select="$h * $freq div $kb" />  
                    <xsl:variable name="term1" select="($theta_v div $temp) div(math:exp($theta_v div $temp)-1)" />
                    <xsl:variable name="term2" select="math:log(1 - math:exp(-$theta_v div $temp))" />
                    <xsl:variable name="s_vibr_rrho" select="$term1 - $term2"/>                    
                    <!-- Weight according to the quotient against the cutoff freq. 100 cm-1 -->
                    <xsl:variable name="weight" select="1 div (1 + math:pow($f_cutoff div $freq, 2))"/>
                    <xsl:variable name="s_vibr_term" select="$weight * $s_vibr_rrho + (1 - $weight) * $s_vibr_pseudorrot" />
                    
                    <xsl:element name="node">
                        <xsl:value-of select="$s_vibr_term"/>    
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="sum($S_vibr/node/text())" />        
    </xsl:function>
    
    <xsl:function name="helper:svibr_rrho">
        <xsl:param name="freqlist" />
        <xsl:param name="temp" />
        <xsl:param name="program" />
                
        <xsl:variable name="S_vibr">
            <xsl:for-each select="$freqlist">
                <xsl:variable name="freq" select="number(.)"/>
                <!-- Calculation: consider the cutoff of small freqs. of different programs -->
                <xsl:if test="(matches($program, 'ADF') and $freq &gt;$adfFreqLimit) or (matches($program,'GAUSSIAN') and $freq &gt; $gaussianFreqLimit)">                    
                    <xsl:variable name="theta_v" select="$h * $freq div $kb"/>                    
                    <xsl:variable name="term1" select="($theta_v div $temp) div (math:exp($theta_v div $temp)-1)"/>
                    <xsl:variable name="term2" select="math:log(1 - math:exp(-$theta_v div $temp))"/>
                    <xsl:variable name="s_vibr_term" select="$term1 - $term2"/>                    
                    <xsl:element name="node">
                        <xsl:value-of select="$s_vibr_term"/>    
                    </xsl:element>
                </xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="sum($S_vibr/node/text())" />
    </xsl:function>

    <xsl:function name="helper:sumStringArray">
        <xsl:param name="value"/>
        <xsl:param name="delimiter" />
        
        <xsl:variable name="values" select="tokenize($value, $delimiter)" />
        <xsl:variable name="sum">        
            <xsl:for-each select="1 to count($values)">
                <xsl:variable name="outerIndex" select="."/>
                <xsl:element name="value">
                    <xsl:value-of select="$values[$outerIndex]"/>   
                </xsl:element>
            </xsl:for-each>                            
        </xsl:variable>
        <xsl:value-of select="sum($sum/*:value)"/>        
    </xsl:function>

    <xsl:function name="helper:nodesFromStringArray">
        <xsl:param name="values"/>
    
        <xsl:element name="array">
            <xsl:for-each select="$values">
                <xsl:variable name="value" select="."/>                
                <xsl:element name="value">
                    <xsl:value-of select="number($value)"/>
                </xsl:element>
            </xsl:for-each>                             
        </xsl:element>
                        
    </xsl:function>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>
