<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:fo="http://www.w3.org/1999/XSL/Format"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xpath-default-namespace="http://www.xml-cml.org/schema"
    version="2.0">
    <xsl:output encoding="UTF-8" method="xml" indent="yes"/>
    <xsl:param name="reportTitle"/>
    <xsl:param name="reportDescription"/>
    <xsl:include href="../../xslt/helper/chemistry/helper.xsl"/>

    <xsl:variable name="minimum">
        <xsl:variable name="matches" select="//template[child::multiplicity = min(//multiplicity)]"/>
        <xsl:choose>
            <xsl:when test="count($matches) > 1" xml:space="default">
                <xsl:copy-of copy-namespaces="no" select="$matches[child::finalenergy = min($matches//finalenergy)]/*"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy-of copy-namespaces="no" select="$matches/*" />
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:variable>    
    <xsl:template match="/">    	
        <fo:root>
            <fo:layout-master-set>    		
                <fo:simple-page-master master-name="calculations"
                    page-height="21cm" 
                    page-width="29.7cm"
                    margin-top="1in"
                    margin-bottom="1in"
                    margin-left = "0.75in"
                    margin-right="0.75in">	    			
                    <fo:region-body 				
                        margin-top="0.6in" 
                        margin-bottom="0.6in"/>    			
                    <fo:region-before 
                        extent="0.5in"/>    			
                    <fo:region-after  
                        extent="0.5in"/>			
                </fo:simple-page-master>
                
            </fo:layout-master-set>
            
            <fo:page-sequence master-reference="calculations">	
                <!-- Header section -->
                <fo:static-content flow-name="xsl-region-before">
                    <fo:list-block provisional-distance-between-starts="5in" provisional-label-separation="0in">
                        <fo:list-item>
                            <fo:list-item-label>
                                <fo:block  text-align="start"><!--<xsl:value-of select="$reportTitle"/>--></fo:block>
                            </fo:list-item-label>
                            <fo:list-item-body>
                                <fo:block  text-align="end"></fo:block>
                            </fo:list-item-body>
                        </fo:list-item>
                    </fo:list-block>
                </fo:static-content>
                <!-- Footer section -->
                <fo:static-content flow-name="xsl-region-after">
                    <fo:list-block provisional-distance-between-starts="5in" provisional-label-separation="0in">
                        <fo:list-item>
                            <fo:list-item-label>
                                <fo:block  text-align="start"></fo:block>
                            </fo:list-item-label>
                            <fo:list-item-body>
                                <fo:block  text-align="end">Page <fo:page-number/></fo:block>
                            </fo:list-item-body>
                        </fo:list-item>
                    </fo:list-block>
                </fo:static-content>	
                <!-- Body section -->
                <fo:flow flow-name="xsl-region-body" font-family="Courier">				    			
                        <fo:block break-before="page">                            
                            <fo:block><fo:inline font-size="1em">Report : Spin state energies for transition metal complexes</fo:inline></fo:block>                            
                            <xsl:if test="exists($reportDescription)">
                                <fo:block>Compound : <xsl:value-of select="$reportDescription"/></fo:block>
                            </xsl:if>
                            <fo:table>
                                <fo:table-column column-width="10%"/>
                                <fo:table-column column-width="40%"/>
                                <fo:table-column column-width="20%"/>
                                <fo:table-column column-width="15%"/>
                                <fo:table-column column-width="15%"/>
                                <fo:table-body>
                                    <fo:table-row>
                                        <fo:table-cell><fo:block>Spin</fo:block></fo:table-cell>                                        
                                        <fo:table-cell><fo:block>Calc. Type</fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block>Method</fo:block></fo:table-cell>
                                        <fo:table-cell><fo:block text-align="end">Energy [<xsl:value-of select="helper:printUnitSymbol($minimum/finalenergy/@units)"/>]</fo:block></fo:table-cell>    								    										
                                        <fo:table-cell><fo:block text-align="end">Energy+ZPVE [<xsl:value-of select="helper:printUnitSymbol($minimum/finalenergy/@units)"/>]</fo:block></fo:table-cell>
                                    </fo:table-row>
                                    <xsl:call-template name="first"/>
                                    <xsl:apply-templates mode="rest"  select="//template[@id='calculation' and not(./finalenergy = $minimum/finalenergy)]">
                                        <xsl:sort select="multiplicity"/>                    
                                    </xsl:apply-templates>                                       
                                </fo:table-body>
                            </fo:table>
                        </fo:block>                   
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
 
    <xsl:template name="first">
        <fo:table-row>
            <fo:table-cell><fo:block width="0.3in"><xsl:value-of select="$minimum/multiplicity"/></fo:block></fo:table-cell>
            <fo:table-cell><fo:block><xsl:value-of select="$minimum/calculationtype"/></fo:block></fo:table-cell>    										
            <fo:table-cell><fo:block><xsl:value-of select="$minimum/method"/></fo:block></fo:table-cell>
            <fo:table-cell><fo:block text-align="end"><xsl:value-of select="'0'"/></fo:block></fo:table-cell>
            <fo:table-cell><fo:block text-align="end"><xsl:value-of select="'0'"/></fo:block></fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <xsl:template mode="rest" match="template">
        <fo:table-row>
            <fo:table-cell><fo:block width="0.3in"><xsl:value-of select="./multiplicity"/></fo:block></fo:table-cell>
            <fo:table-cell><fo:block><xsl:value-of select="./calculationtype"/></fo:block></fo:table-cell>    										
            <fo:table-cell><fo:block><xsl:value-of select="./method"/></fo:block></fo:table-cell>
            <fo:table-cell><fo:block text-align="end"><xsl:value-of select="format-number(number(./finalenergy) - number($minimum/finalenergy),'#0.00')"/></fo:block></fo:table-cell>
            <fo:table-cell><fo:block text-align="end"> <xsl:value-of select="format-number(number(./finalenergy) + number(./zeropointenergy) - (number($minimum/finalenergy) + number($minimum/zeropointenergy)),'#0.00')"/></fo:block></fo:table-cell>
        </fo:table-row>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>