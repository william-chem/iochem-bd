<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<?page title="Report base template" contentType="text/html;charset=UTF-8"?>
<window id="customReportWindow" border="none" xmlns:n="native" apply="cat.iciq.tcg.labbook.zk.composers.reportmanager.ReportEnergyReactionProfile" hflex="1" vflex="1">		
	<script type="text/javascript">
	    <![CDATA[
	    function fireEventFromClient(eventName) {
		     zAu.send(new zk.Event(zk.Widget.$('$customReportWindow'), eventName, null, {toServer:true}));
	    }
		]]>
	</script>


	<vlayout hflex="1" vflex="1" style="padding-top:5px">		
		<n:h5 vflex="min">Energy reaction profile <a href="https://youtu.be/IRQ28Jd6_X0" target="_blank" tooltiptext="More about energy reaction profile reports"><n:i class="fas fa-lg fa-question-circle"></n:i></a></n:h5>
		<div class="form-group" hflex="1" vflex="min">
		   	<n:label>Energy type:</n:label>
			<div>
				<label id="unavailableEnergyLbl" value="N/A" sclass="ml-2"/>								
				<groupbox sclass="ml-2">
					<radiogroup id="energyTypeRdg">					
						<radio id="potentialEnergyRad" 		label="Potential Energy" 		visible="false" 	value="POTENTIAL"/>
						<radio id="freeEnergyRad" 			label="Gibbs Energy"  			visible="false"		value="FREE"/>
						<button id="freeCorrectedBtn" 		class="gibbs-corrected-btn btn btn-sm btn-secondary" visible="false" tooltiptext="Correct temperature and pressure" />						
						<radio id="zeroPointEnergyRad"		label="Zero Point Energy Corrected" visible="false" value="ZERO_POINT"/>
						<radio id="enthalpyEnergyRad"		label="Enthalpy"		 		visible="false"		value="ENTHALPY"/>
						<radio id="bondingEnergyRad"		label="Bonding Energy" 			visible="false"		value="BONDING"/>
						<radio id="nucRepulsionEnergyRad"	label="Nuc. Repulsion Energy" 	visible="false"		value="NUCREPULSION"/>					
					</radiogroup>
				</groupbox>				
			</div>								   
		 </div>
		<div class="form-group" hflex="1" vflex="min">
		    <n:label>Default Units:</n:label>
			<div>
				<groupbox sclass="ml-2">
					<radiogroup id="energyUnitsRdg">
						<radio id="energyUnitsKcalMolRad" 	label="kcal/mol"	value="nonsi2:kcal.mol-1"	sclass="energy-rd" />
						<radio id="energyUnitsKJMolRad" 	label="kJ/mol" 		value="nonsi:kj.mol-1"	sclass="energy-rd" />
						<radio id="energyUnitsEVRad" 		label="eV"  		value="nonsi:electronvolt"	sclass="energy-rd"/>
						<radio id="energyUnitsHartreeRad"   label="Eh"			value="nonsi:hartree"	sclass="energy-rd" />
					</radiogroup>
				</groupbox>
			</div>    
		 </div>
		<div class="form-group" hflex="1" vflex="min">
			<n:label>File format:</n:label>
			<div id="fileFormatLbl" width="100%" height="40px">
				<borderlayout>
					<center>
						<groupbox sclass="energy-rd energy-type-rd ml-2">
							<radiogroup id="fileFormatRgp"/>
						</groupbox>
					</center>
					<east>
						<div sclass="reaction-graph-div">
							<n:label sclass="mb-0 ml-5" style="white-space: nowrap;">Build reaction network graph </n:label>
							<button id="generateReactionNetworkBtn" sclass="btn btn-primary reaction-network-btn ml-2" label="" tooltiptext="Display reaction network" />
						</div>					
					</east>
				</borderlayout>
		 	</div>
		 </div>	 	
	 	<tabbox id="seriesTabbox" sclass="series-tabbox" hflex="1" vflex="1">
			<tabs id="seriesTabs" hflex="1" vflex="min">		
				<tab label=" " id="addSerieTab" sclass="add-serie-tab" tooltiptext="Add a new serie" />				
			</tabs>			
			<tabpanels id="seriesTabpanels" sclass="series-tabpanels" hflex="1" vflex="2">
				<tabpanel></tabpanel>		
			</tabpanels>			
		</tabbox>
	</vlayout>
		
	
	<window xmlns:n="native" xmlns:w="client" id="reportInformation" closable="true" border="normal" title="Step definition help" 
	 			onClose="self.visible = false; event.stopPropagation();" sclass="z-window-modal-large" visible="false">
	 			
		<n:style type="text/css">
			.carousel-control-next, .carousel-control-prev {
				width: 25px;
			}
		
			.carousel-control-prev-icon {
			    width: 30px;
			    height: 30px;
			    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23444444' width='6' height='6' viewBox='0 0 8 8'%3e%3cpath d='M5.25 0l-4 4 4 4 1.5-1.5L4.25 4l2.5-2.5L5.25 0z'/%3e%3c/svg%3e");
			}
			 
			.carousel-control-next-icon {
			    width: 30px;
			    height: 30px;
			    background-image: url("data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' fill='%23444444' width='6' height='6' viewBox='0 0 8 8'%3e%3cpath d='M2.75 0l-1.5 1.5L3.75 4l-2.5 2.5L2.75 8l4-4-4-4z'/%3e%3c/svg%3e");
			}
			
			.carousel-caption h5, .carousel-caption p {
				color: #212529;
			}
			
			ol.carousel-indicators li {
				background-color: #CCC;
			}
			
			ol.carousel-indicators li.active {
				background-color: #000;
			}
		</n:style>				
		<n:div>
			<n:div id="carouselReactionEnergySteps" class="carousel slide" data-ride="carousel">
			  <n:ol class="carousel-indicators">
			    <n:li data-target="#carouselReactionEnergySteps" data-slide-to="0" class="active"></n:li>
			    <n:li data-target="#carouselReactionEnergySteps" data-slide-to="1"></n:li>
			    <n:li data-target="#carouselReactionEnergySteps" data-slide-to="2"></n:li>
			     <n:li data-target="#carouselReactionEnergySteps" data-slide-to="3"></n:li>
			  </n:ol>
			
			  <n:div class="carousel-inner">
			    <n:div class="carousel-item active">
			      <n:img class="d-block w-100" src="../images/reportEnergyReactionProfileHelpStep1.png"></n:img>
			       <n:div class="carousel-caption d-none d-md-block">
				    <n:h5>Calculation name assignation</n:h5>
				    <n:p>Refer to each calculation in the formulas using its selection order index and prepending a "C" letter. </n:p>
				  </n:div>
			    </n:div>
			    <n:div class="carousel-item">
			      <n:img class="d-block w-100" src="../images/reportEnergyReactionProfileHelpStep2.png"></n:img>
			      <n:div class="carousel-caption d-none d-md-block">
				    <n:h5>Closing series 1/2</n:h5>
				    <n:p>If the last step of the serie is also the first one, it will close the graph.</n:p>
				  </n:div>
			    </n:div>
			    <n:div class="carousel-item">
			      <n:img class="d-block w-100" src="../images/reportEnergyReactionProfileHelpStep3.png"></n:img>
			      <n:div class="carousel-caption d-none d-md-block">
				    <n:h5>Closing series 2/2</n:h5>
				    <n:p>If the closing step doesn't have a previous TS, a generic one called "Closing" will be generated.</n:p>
				  </n:div>
			    </n:div>
			    <n:div class="carousel-item">
			      <n:img class="d-block w-100" src="../images/reportEnergyReactionProfileHelpStep4.png"></n:img>
			      <n:div class="carousel-caption d-none d-md-block">
				    <n:h5>Setup additional variables</n:h5>
				    <n:p>Define helper variables to avoid repeating elements and simplify formula definition.</n:p>
				  </n:div>
			    </n:div>			    
			  </n:div>
			  <n:a class="carousel-control-prev" href="#carouselReactionEnergySteps" role="button" data-slide="prev">
			    <n:span class="carousel-control-prev-icon" aria-hidden="true"></n:span>
			    <n:span class="sr-only">Previous</n:span>
			  </n:a>
			  <n:a class="carousel-control-next" href="#carouselReactionEnergySteps" role="button" data-slide="next">
			    <n:span class="carousel-control-next-icon" aria-hidden="true"></n:span>
			    <n:span class="sr-only">Next</n:span>
			  </n:a>
			</n:div>			
		</n:div>
	</window>

</window>	
