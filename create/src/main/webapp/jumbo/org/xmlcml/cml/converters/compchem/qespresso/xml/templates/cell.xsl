<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"    
    version="2.0">
    
    <!-- <xsl:include href="helper.xsl"/> -->
    <xsl:template match="CELL" >
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="cell">
                <CELL>
                    <NON-PERIODIC_CELL_CORRECTION type="character" size="1" len="11">
                        Makov-Payne
                    </NON-PERIODIC_CELL_CORRECTION>
                    <BRAVAIS_LATTICE type="character" size="1" len="12">
                        cubic P (sc)
                    </BRAVAIS_LATTICE>
                    <LATTICE_PARAMETER type="real" size="1" UNITS="Bohr">
                        2.000000000000000E+001
                    </LATTICE_PARAMETER>
                    <CELL_DIMENSIONS type="real" size="6">
                        2.000000000000000E+001
                        0.000000000000000E+000
                        0.000000000000000E+000
                        0.000000000000000E+000
                        0.000000000000000E+000
                        0.000000000000000E+000
                    </CELL_DIMENSIONS>
                    <DIRECT_LATTICE_VECTORS>
                        <UNITS_FOR_DIRECT_LATTICE_VECTORS UNITS="Bohr"/>
                        <a1 type="real" size="3" columns="3">
                            2.000000000000000E+001  0.000000000000000E+000  0.000000000000000E+000
                        </a1>
                        <a2 type="real" size="3" columns="3">
                            0.000000000000000E+000  2.000000000000000E+001  0.000000000000000E+000
                        </a2>
                        <a3 type="real" size="3" columns="3">
                            0.000000000000000E+000  0.000000000000000E+000  2.000000000000000E+001
                        </a3>                        
                    </DIRECT_LATTICE_VECTORS>
                    <RECIPROCAL_LATTICE_VECTORS>
                        <UNITS_FOR_RECIPROCAL_LATTICE_VECTORS UNITS="2 pi / a"/>
                        <b1 type="real" size="3" columns="3">
                            1.000000000000000E+000  0.000000000000000E+000  0.000000000000000E+000
                        </b1>
                        <b2 type="real" size="3" columns="3">
                            0.000000000000000E+000  1.000000000000000E+000  0.000000000000000E+000
                        </b2>
                        <b3 type="real" size="3" columns="3">
                            0.000000000000000E+000  0.000000000000000E+000  1.000000000000000E+000
                        </b3>
                    </RECIPROCAL_LATTICE_VECTORS>
                </CELL>                
            </comment>
        </xsl:if>

        <module id="cell" cmlx:templateRef="cell">

            <xsl:variable name="lattice">
                <xsl:call-template name="lattice"/>                
            </xsl:variable>
           
            <xsl:variable name="latticeA" select="tokenize($lattice//*:array[@dictRef='cc:lattice'][1],'\s+')"/>
            <xsl:variable name="latticeB" select="tokenize($lattice//*:array[@dictRef='cc:lattice'][2],'\s+')"/>
            <xsl:variable name="latticeC" select="tokenize($lattice//*:array[@dictRef='cc:lattice'][3],'\s+')"/>
            
            
            <xsl:variable name="alpha" select="helper:calcAxesAngle($latticeB,$latticeC)"/>
            <xsl:variable name="beta" select="helper:calcAxesAngle($latticeC, $latticeA)"/>
            <xsl:variable name="gamma" select="helper:calcAxesAngle($latticeA, $latticeB)"/>
            <crystal>
                <scalar units="units:ang" title="a" id="sc1"><xsl:value-of select="helper:calcAxisLength($latticeA)"/></scalar>
                <scalar units="units:ang" title="b" id="sc2"><xsl:value-of select="helper:calcAxisLength($latticeB)"/></scalar>
                <scalar units="units:ang" title="c" id="sc3"><xsl:value-of select="helper:calcAxisLength($latticeC)"/></scalar>
                <scalar units="units:deg" title="alpha" id="sc4"><xsl:value-of select="$alpha"/></scalar>
                <scalar units="units:deg" title="beta" id="sc5"><xsl:value-of select="$beta"/></scalar>
                <scalar units="units:deg" title="gamma" id="sc6"><xsl:value-of select="$gamma"/></scalar>
            </crystal>

            <xsl:copy-of select="$lattice/node()"/>
            <xsl:call-template name="dimensions"/>            
                        
        </module>

        <xsl:if test="$isVerbose">
            <comment class="example.output" id="cell">
                <module id="cell" cmlx:templateRef="cell">
                    <crystal>
                        <scalar units="units:ang" title="a" id="sc1">10.63020706</scalar>
                        <scalar units="units:ang" title="b" id="sc2">10.63020706</scalar>
                        <scalar units="units:ang" title="c" id="sc3">10.63020706</scalar>
                        <scalar units="units:deg" title="alpha" id="sc4">90.00</scalar>
                        <scalar units="units:deg" title="beta" id="sc5">90.00</scalar>
                        <scalar units="units:deg" title="gamma" id="sc6">90.00</scalar>
                    </crystal>
                    <array dataType="xsd:double" dictRef="qex:cellDimensions" size="6" units="nonsi:angstrom">10.58360000 0.00000000 0.00000000 0.00000000 0.00000000 0.00000000</array>
                    <array dataType="xsd:double" dictRef="cc:lattice" size="3" units="nonsi:angstrom">10.58360000 0.00000000 0.00000000</array>
                    <array dataType="xsd:double" dictRef="cc:lattice" size="3" units="nonsi:angstrom">0.00000000 10.58360000 0.00000000</array>
                    <array dataType="xsd:double" dictRef="cc:lattice" size="3" units="nonsi:angstrom">0.00000000 0.00000000 10.58360000</array>
                    <array dataType="xsd:string" dictRef="qex:cellDimensions" size="" units="nonsi:angstrom"/>
                </module>             
            </comment>
        </xsl:if>  
    </xsl:template>

    <xsl:template match="DIRECT_LATTICE_VECTORS" name="lattice">                
            <xsl:apply-templates/>        
    </xsl:template>
    
    <xsl:template match="node()[starts-with(name(.),'a')]">
        <xsl:variable name="currentUnits" select="./preceding-sibling::UNITS_FOR_DIRECT_LATTICE_VECTORS/@UNITS"/>
        <xsl:copy-of select="helper:toArray('cc:lattice',., $currentUnits,'nonsi:angstrom', '#0.00000000')"></xsl:copy-of>                
    </xsl:template> 

    <xsl:template match="CELL_DIMENSIONS" name="dimensions">        
        <xsl:copy-of select="helper:toArray('qex:cellDimensions',., 'nonsi:bohr','nonsi:angstrom', '#0.00000000')"></xsl:copy-of>
    </xsl:template>
</xsl:stylesheet>