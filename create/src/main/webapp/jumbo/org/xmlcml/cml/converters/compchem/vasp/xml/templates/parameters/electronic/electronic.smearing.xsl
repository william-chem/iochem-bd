<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <xsl:template match="./separator[@name='electronic smearing']" mode="parameters.electronic">
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="electronic.smearing">
                 <separator name="electronic smearing" >
				    <i type="int" name="ISMEAR">     0</i>
				    <i name="SIGMA">      0.05000000</i>
				    <i name="KSPACING">      0.50000000</i>
				    <i type="logical" name="KGAMMA"> T  </i>
				   </separator>
            </comment>            
        </xsl:if>
        
        <module id="electronic.smearing" cmlx:templateRef="electronic.smearing">
            <xsl:if test="exists(./i[@name='ISMEAR'])">
                <scalar dataType="xsd:integer" dictRef="v:ismear"><xsl:value-of select="helper:trim(./i[@name='ISMEAR'])"/></scalar>                
            </xsl:if>
        	<xsl:if test="exists(./i[@name='SIGMA'])">
        		<scalar dataType="xsd:double" dictRef="v:sigma"><xsl:value-of select="helper:trim(./i[@name='SIGMA'])"/></scalar>                
        	</xsl:if>                     
        </module>
           
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="electronic.smearing">
            	      
            </comment>
        </xsl:if>
            
    </xsl:template>
    
</xsl:stylesheet>