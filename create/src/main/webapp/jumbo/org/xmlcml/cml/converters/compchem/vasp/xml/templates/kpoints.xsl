<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    
 <!--<xsl:include href="helper.xsl"/>--> 
    
    <xsl:template match="./kpoints" >        
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="kpoints">
                <kpoints>
                    <generation param="Monkhorst-Pack">
                        <v type="int" name="divisions">       3       3       1</v>
                        <v name="usershift">      0.00000000      0.00000000      0.00000000</v>
                        <v name="genvec1">      0.33333333      0.00000000      0.00000000</v>
                        <v name="genvec2">      0.00000000      0.33333333      0.00000000</v>
                        <v name="genvec3">      0.00000000      0.00000000      1.00000000</v>
                        <v name="shift">      0.00000000      0.00000000      0.00000000</v>
                    </generation>
                    <varray name="kpointlist" >
                        <v>       0.00000000      0.00000000      0.00000000</v>
                        <v>       0.33333333      0.00000000      0.00000000</v>
                        <v>       0.00000000      0.33333333      0.00000000</v>
                        <v>       0.33333333      0.33333333      0.00000000</v>
                        <v>      -0.33333333      0.33333333      0.00000000</v>
                    </varray>
                    <varray name="weights" >
                        <v>       0.11111111</v>
                        <v>       0.22222222</v>
                        <v>       0.22222222</v>
                        <v>       0.22222222</v>
                        <v>       0.22222222</v>
                    </varray>
                </kpoints>                
            </comment>
        </xsl:if>  
        
        <module id="kpoints" cmlx:templateRef="kpoints">
            <xsl:variable name="generation" select="./generation"/>
            <xsl:if test="exists($generation)">                            
                <module id="generation" cmlx:templateRef="generation">
                    <scalar dataType="xsd:string" dictRef="v:generation"><xsl:value-of select="$generation/@param"/></scalar>                    
                    <xsl:copy-of select="helper:arrayFromStringNodes ('v:usershift','xsd:double',$generation/v[@name='usershift'])"/>
                    <xsl:copy-of select="helper:arrayFromStringNodes ('v:divisions','xsd:double',$generation/v[@name='divisions'])"/>
                    <xsl:copy-of select="helper:matrixFromStringNodes('v:genvects','xsd:double',$generation/v[matches(@name,'genvec.*')], -1 , 3)"/>
                    <xsl:copy-of select="helper:arrayFromStringNodes ('v:shift','xsd:double',$generation/v[@name='shift'])"/>                   
                </module>
                <xsl:copy-of select="helper:matrixFromStringNodes('v:kpointlist','xsd:double',./varray[@name='kpointlist'], -1 , 3)"/>
            </xsl:if>                                    
        </module>
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="kpoints">
                <module id="kpoints" cmlx:templateRef="kpoints">
                    <module id="generation" cmlx:templateRef="generation">
                        <scalar dataType="xsd:string" dictRef="v:generation">Monkhorst-Pack</scalar>
                        <array dataType="xsd:double" dictRef="v:usershift" size="3">0.00000000 0.00000000 0.00000000</array>
                        <array dataType="xsd:double" dictRef="v:divisions" size="3">3 3 1</array>
                        <matrix dataType="xsd:double" dictRef="v:genvects" rows="3" cols="3">0.33333333 0.00000000 0.00000000 0.00000000 0.33333333 0.00000000 0.00000000 0.00000000 1.00000000</matrix>
                        <array dataType="xsd:double" dictRef="v:shift" size="3">0.00000000 0.00000000 0.00000000</array>
                    </module>
                    <matrix dataType="xsd:double" dictRef="v:kpointlist" rows="5" cols="3">0.00000000 0.00000000 0.00000000 0.33333333 0.00000000 0.00000000 0.00000000 0.33333333 0.00000000 0.33333333 0.33333333 0.00000000 -0.33333333 0.33333333 0.00000000</matrix>
                </module>
            </comment>
        </xsl:if>  

    </xsl:template>
    
   

    
</xsl:stylesheet>