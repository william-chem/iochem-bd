 <xsl:stylesheet 
 	xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
	xmlns:xs="http://www.w3.org/2001/XMLSchema"
	xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
	xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
	xmlns="http://www.xml-cml.org/schema"
	exclude-result-prefixes="xs helper"
	version="2.0"> 	
 	 
 	<!-- This template will work either for initial and final position coordinates --> 	
 	<xsl:template match="./structure[matches(@name,'(initialpos)|(finalpos)')]">
 		<xsl:param name="isVerbose" tunnel="yes"/>
 		
 		<xsl:if test="$isVerbose">
 			<comment class="example.input" id="initialpos">
 			
 			
 			</comment>
 			<comment class="example.input" id="finalpos">
 				<structure name="finalpos" >
 					<crystal>
 						<varray name="basis" >
 							<v>       7.77260017       0.00000000       0.00000000 </v>
 							<v>      -3.88630009       6.73126920       0.00000000 </v>
 							<v>       0.00000000       0.00000000      22.93280029 </v>
 						</varray>
 					</crystal>
 					<varray name="positions" >
 						<v>       0.16666500       0.33333501       0.03459000 </v>
 						<v>       0.16666500       0.83333501       0.03459000 </v>
 						<v>       0.66666500       0.33333501       0.03459000 </v>
 						<v>       0.66666500       0.83333501       0.03459000 </v>
 						<v>       0.33345702       0.16678011       0.17244178 </v>
 						<v>       0.33374085       0.66724218       0.17237917 </v>
 						<v>       0.83355744       0.16699794       0.17241040 </v>
 						<v>       0.83368393       0.66718511       0.17247860 </v>
 						<v>       0.00065777       0.00165975       0.31030428 </v>
 						<v>       0.00129612       0.50125835       0.30981799 </v>
 						<v>       0.50111614       0.00121935       0.31025131 </v>
 						<v>       0.50050736       0.50095261       0.30988947 </v>
 						<v>       0.00000000       0.00000000       0.00000000 </v>
 						<v>       0.00000000       0.50000000       0.00000000 </v>
 						<v>       0.50000000       0.00000000       0.00000000 </v>
 						<v>       0.50000000       0.50000000       0.00000000 </v>
 						<v>       0.33333501       0.16666500       0.06918000 </v>
 						<v>       0.33333501       0.66666500       0.06918000 </v>
 						<v>       0.83333501       0.16666500       0.06918000 </v>
 						<v>       0.83333501       0.66666500       0.06918000 </v>
 						<v>       0.16666500       0.33333501       0.13837001 </v>
 						<v>       0.16666500       0.83333501       0.13837001 </v>
 						<v>       0.66666500       0.33333501       0.13837001 </v>
 						<v>       0.66666500       0.83333501       0.13837001 </v>
 						<v>       0.00079204       0.00089568       0.20592797 </v>
 						<v>       0.00070866       0.50078874       0.20586618 </v>
 						<v>       0.50025876       0.00080272       0.20582948 </v>
 						<v>       0.50009211       0.49992056       0.20580172 </v>
 						<v>       0.33359908       0.16673571       0.27906386 </v>
 						<v>       0.33479508       0.66867669       0.27848829 </v>
 						<v>       0.83362089       0.16748637       0.27847920 </v>
 						<v>       0.83419157       0.66821061       0.27857077 </v>
 						<v>       0.16664213       0.33927392       0.36571296 </v>
 						<v>       0.16685883       0.83294966       0.36959934 </v>
 						<v>       0.67289486       0.33692034       0.36702799 </v>
 						<v>       0.66726971       0.83517413       0.36838300 </v>
 						<v>       0.34343403       0.19853492       0.47545825 </v>
 						<v>       0.42041623       0.38579751       0.49052487 </v>
 						<v>       0.54644985       0.59300076       0.50149967 </v>
 						<v>       0.17561704       0.11203177       0.47398625 </v>
 						<v>       0.18242851       0.33304824       0.40771598 </v>
 						<v>       0.06213697       0.20253998       0.47794839 </v>
 						<v>       0.66281836       0.34328083       0.40903848 </v>
 						<v>       0.15118296       0.80431941       0.41107872 </v>
 						<v>       0.68790807       0.85950849       0.40999307 </v>
 						<v>       0.42127028       0.12286014       0.46013990 </v>
 						<v>       0.69951639       0.64541922       0.48593729 </v>
 						<v>       0.49194210       0.68267252       0.47912874 </v>
 						<v>       0.55238075       0.62546183       0.54837617 </v>
 					</varray>
 				</structure>
 				<atominfo>
 					<atoms>      49 </atoms>
 					<types>       4 </types>
 					<array name="atoms" >
 						<dimension dim="1">ion</dimension>
 						<field type="string">element</field>
 						<field type="int">atomtype</field>
 						<set>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>Ce</c><c>   1</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>O </c><c>   2</c></rc>
 							<rc><c>C </c><c>   3</c></rc>
 							<rc><c>C </c><c>   3</c></rc>
 							<rc><c>C </c><c>   3</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 							<rc><c>H </c><c>   4</c></rc>
 						</set>
 					</array> 					
 				</atominfo>
 			</comment> 			
 		</xsl:if>

 		<xsl:variable name="latticeVectors" select="./crystal/varray[@name='basis']/v"/> 				 		 
 		<xsl:variable name="atomTypes" select="/modeling/atominfo/array[@name='atoms']/set/rc/c[1]"/>
 		<xsl:variable name="atomPoints" select="./varray[@name='positions']/v"/>
 		<xsl:copy-of select="helper:arrayFromStringNodes('cc:positions', 'xsd:string',./varray[@name='selective'])"/>
 		
 		<molecule id="{@name}" cmlx:templateRef="{@name}">
 			<crystal>
 				<xsl:variable name="latticeA" select="tokenize(helper:trim($latticeVectors[1]/text()),'\s+')"/>
 				<xsl:variable name="latticeB" select="tokenize(helper:trim($latticeVectors[2]/text()),'\s+')"/>
 				<xsl:variable name="latticeC" select="tokenize(helper:trim($latticeVectors[3]/text()),'\s+')"/>

 				<xsl:variable name="alpha" select="helper:calcAxesAngle($latticeB,$latticeC)"/>
				<xsl:variable name="beta" select="helper:calcAxesAngle($latticeC, $latticeA)"/>
				<xsl:variable name="gamma" select="helper:calcAxesAngle($latticeA, $latticeB)"/>
 				
 				<scalar units="units:ang" title="a" id="sc1"><xsl:value-of select="helper:calcAxisLength($latticeA)"/></scalar>
 				<scalar units="units:ang" title="b" id="sc2"><xsl:value-of select="helper:calcAxisLength($latticeB)"/></scalar>
 				<scalar units="units:ang" title="c" id="sc3"><xsl:value-of select="helper:calcAxisLength($latticeC)"/></scalar>
 				<scalar units="units:deg" title="alpha" id="sc4"><xsl:value-of select="$alpha"/></scalar>
 				<scalar units="units:deg" title="beta" id="sc5"><xsl:value-of select="$beta"/></scalar>
 				<scalar units="units:deg" title="gamma" id="sc6"><xsl:value-of select="$gamma"/></scalar>
 				
 			</crystal>
			<atomArray>
				<xsl:for-each select="$atomPoints">
					<xsl:variable name="outerIndex" select="position()"/>
					<xsl:variable name="atomType" select="helper:trim($atomTypes[$outerIndex])"/>					
					<xsl:variable name="a" select="tokenize(helper:trim($latticeVectors[1]/text()),'\s+')"/>
					<xsl:variable name="b" select="tokenize(helper:trim($latticeVectors[2]/text()),'\s+')"/>
					<xsl:variable name="c" select="tokenize(helper:trim($latticeVectors[3]/text()),'\s+')"/>
					<xsl:variable name="frac" select="tokenize(helper:trim($atomPoints[$outerIndex]/text()),'\s+')"/>
		    	    <xsl:variable name="coords" select="helper:fractionalToCartesian($a,$b,$c,$frac)"/>										 
					<atom elementType='{normalize-space($atomType)}' x3='{$coords/@x3}' y3='{$coords/@y3}' z3='{$coords/@z3}' xFract='{$frac[1]}' yFract='{$frac[2]}' zFract='{$frac[3]}' id='{concat("a",string($outerIndex))}'/>					
				</xsl:for-each>							
			</atomArray>
 		</molecule>	
 		
 		<xsl:if test="$isVerbose">
 			<comment class="example.output" id="initialpos">
 				
 			</comment>
 			
 			<comment class="example.output" id="finalpos">
 				<molecule id="finalpos" xmlns="http://www.xml-cml.org/schema"  xmlns:cmlx="http://www.xml-cml.org/schema/cmlx" cmlx:templateRef="vasprun">
	 				<crystal>
	 					<scalar units="units:ang" title="a" id="sc1">7.77601582</scalar>
	 					<scalar units="units:ang" title="b" id="sc2">7.77601582</scalar>
	 					<scalar units="units:ang" title="c" id="sc3">22.93343284</scalar>
	 					<scalar units="units:deg" title="alpha" id="sc4">90.00</scalar>
	 					<scalar units="units:deg" title="beta" id="sc5">90.00</scalar>
	 					<scalar units="units:deg" title="gamma" id="sc6">119.97</scalar>
	 				</crystal>
	 				<atomArray>
	 					<atom elementType="Ce" x3="-0.00001947" y3="2.24376769" z3="0.79324556" xFract="0.16666500"	yFract="0.33333501" zFract="0.03459000" id="a1"/>
	 					<atom elementType="Ce" x3="-1.94316952" y3="5.60940229" z3="0.79324556" xFract="0.16666500" yFract="0.83333501" zFract="0.03459000" id="a2"/>
	 					<atom elementType="Ce" x3="3.88628061" y3="2.24376769" z3="0.79324556" xFract="0.66666500" yFract="0.33333501" zFract="0.03459000" id="a3"/>
	 					<atom elementType="Ce" x3="1.94313057" y3="5.60940229" z3="0.79324556" xFract="0.66666500" yFract="0.83333501" zFract="0.03459000" id="a4"/>
	 					<atom elementType="Ce" x3="1.94367053" y3="1.12264182" z3="3.95457290" xFract="0.33345702" yFract="0.16678011" zFract="0.17244178" id="a5"/>
	 					<atom elementType="Ce" x3="0.00093084" y3="4.49138674" z3="3.95313708" xFract="0.33374085" yFract="0.66724218" zFract="0.17237917" id="a6"/>
	 					<atom elementType="Ce" x3="5.82990459" y3="1.12410809" z3="3.95385327" xFract="0.83355744" yFract="0.16699794" zFract="0.17241040" id="a7"/>
	 					<atom elementType="Ce" x3="3.88701030" y3="4.49100258" z3="3.95541729" xFract="0.83368393" yFract="0.66718511" zFract="0.17247860" id="a8"/>
	 					<atom elementType="Ce" x3="-0.00133770" y3="0.01117222" z3="7.11614608" xFract="0.00065777" yFract="0.00165975" zFract="0.31030428" id="a9"/>
	 					<atom elementType="Ce" x3="-1.93796615" y3="3.37410489" z3="7.10499409" xFract="0.00129612" yFract="0.50125835" zFract="0.30981799" id="a10"/>
	 					<atom elementType="Ce" x3="3.89023663" y3="0.00820777" z3="7.11493133" xFract="0.50111614" yFract="0.00121935" zFract="0.31025131" id="a11"/>
	 					<atom elementType="Ce" x3="1.94339142" y3="3.37204687" z3="7.10663333" xFract="0.50050736" yFract="0.50095261" zFract="0.30988947" id="a12"/>
	 					<atom elementType="O" x3="0.00000000" y3="0.00000000" z3="0.00000000" xFract="0.00000000" yFract="0.00000000" zFract="0.00000000" id="a13"/>
	 					<atom elementType="O" x3="-1.94315004" y3="3.36563460" z3="0.00000000" xFract="0.00000000" yFract="0.50000000" zFract="0.00000000" id="a14"/>
	 					<atom elementType="O" x3="3.88630008" y3="0.00000000" z3="0.00000000" xFract="0.50000000" yFract="0.00000000" zFract="0.00000000" id="a15"/>
	 					<atom elementType="O" x3="1.94315004" y3="3.36563460" z3="0.00000000" xFract="0.50000000" yFract="0.50000000" zFract="0.00000000" id="a16"/>
	 					<atom elementType="O" x3="1.94316955" y3="1.12186698" z3="1.58649112" xFract="0.33333501" yFract="0.16666500" zFract="0.06918000" id="a17"/>
	 					<atom elementType="O" x3="0.00001951" y3="4.48750158" z3="1.58649112" xFract="0.33333501" yFract="0.66666500" zFract="0.06918000" id="a18"/>
	 					<atom elementType="O" x3="5.82946964" y3="1.12186698" z3="1.58649112" xFract="0.83333501" yFract="0.16666500" zFract="0.06918000" id="a19"/>
	 					<atom elementType="O" x3="3.88631959" y3="4.48750158" z3="1.58649112" xFract="0.83333501" yFract="0.66666500" zFract="0.06918000" id="a20"/>
	 					<atom elementType="O" x3="-0.00001947" y3="2.24376769" z3="3.17321181" xFract="0.16666500" yFract="0.33333501" zFract="0.13837001" id="a21"/>
	 					<atom elementType="O" x3="-1.94316952" y3="5.60940229" z3="3.17321181" xFract="0.16666500" yFract="0.83333501" zFract="0.13837001" id="a22"/>
	 					<atom elementType="O" x3="3.88628061" y3="2.24376769" z3="3.17321181" xFract="0.66666500" yFract="0.33333501" zFract="0.13837001" id="a23"/>
	 					<atom elementType="O" x3="1.94313057" y3="5.60940229" z3="3.17321181" xFract="0.66666500" yFract="0.83333501" zFract="0.13837001" id="a24"/>
	 					<atom elementType="O" x3="0.00267533" y3="0.00602906" z3="4.72250501" xFract="0.00079204" yFract="0.00089568" zFract="0.20592797" id="a25"/>
	 					<atom elementType="O" x3="-1.94070719" y3="3.37094382" z3="4.72108799" xFract="0.00070866" yFract="0.50078874" zFract="0.20586618" id="a26"/>
	 					<atom elementType="O" x3="3.88519171" y3="0.00540332" z3="4.72024636" xFract="0.50025876" yFract="0.00080272" zFract="0.20582948" id="a27"/>
	 					<atom elementType="O" x3="1.94417470" y3="3.36509987" z3="4.71960974" xFract="0.50009211" yFract="0.49992056" zFract="0.20580172" id="a28"/>
	 					<atom elementType="O" x3="1.94494726" y3="1.12234295" z3="6.39971577" xFract="0.33359908" yFract="0.16673571" zFract="0.27906386" id="a29"/>
	 					<atom elementType="O" x3="0.00355002" y3="4.50104281" z3="6.38651634" xFract="0.33479508" yFract="0.66867669" zFract="0.27848829" id="a30"/>
	 					<atom elementType="O" x3="5.82849958" y3="1.12739584" z3="6.38630788" xFract="0.83362089" yFract="0.16748637" zFract="0.27847920" id="a31"/>
	 					<atom elementType="O" x3="3.88697059" y3="4.49790550" z3="6.38840784" xFract="0.83419157" yFract="0.66821061" zFract="0.27857077" id="a32"/>
	 					<atom elementType="O" x3="-0.02327762" y3="2.28374409" z3="8.38682228" xFract="0.16664213" yFract="0.33927392" zFract="0.36571296" id="a33"/>
	 					<atom elementType="O" x3="-1.94016537" y3="5.60680839" z3="8.47594785" xFract="0.16685883" yFract="0.83294966" zFract="0.36959934" id="a34"/>
	 					<atom elementType="O" x3="3.92076916" y3="2.26790151" z3="8.41697960" xFract="0.67289486" yFract="0.33692034" zFract="0.36702799" id="a35"/>
	 					<atom elementType="O" x3="1.94068336" y3="5.62178190" z3="8.44805377" xFract="0.66726971" yFract="0.83517413" zFract="0.36838300" id="a36"/>
	 					<atom elementType="C" x3="1.89780912" y3="1.33639199" z3="10.90358909" xFract="0.34343403" yFract="0.19853492" zFract="0.47545825" id="a37"/>
	 					<atom elementType="C" x3="1.76840236" y3="2.59690690" z3="11.24910888" xFract="0.42041623" yFract="0.38579751" zFract="0.49052487" id="a38"/>
	 					<atom elementType="C" x3="1.94275729" y3="3.99164775" z3="11.50079178" xFract="0.54644985" yFract="0.59300076" zFract="0.50149967" id="a39"/>
	 					<atom elementType="H" x3="0.92961196" y3="0.75411600" z3="10.86983201" xFract="0.17561704" yFract="0.11203177" zFract="0.47398625" id="a40"/>
	 					<atom elementType="H" x3="0.12361846" y3="2.24183736" z3="9.35006914" xFract="0.18242851" yFract="0.33304824" zFract="0.40771598" id="a41"/>
	 					<atom elementType="H" x3="-0.30416532" y3="1.36335113" z3="10.96069498" xFract="0.06213697" yFract="0.20253998" zFract="0.47794839" id="a42"/>
	 					<atom elementType="H" x3="3.81772978" y3="2.31071568" z3="9.38039777" xFract="0.66281836" yFract="0.34328083" zFract="0.40903848" id="a43"/>
	 					<atom elementType="H" x3="-1.95074189" y3="5.41409047" z3="9.42718619" xFract="0.15118296" yFract="0.80431941" zFract="0.41107872" id="a44"/>
	 					<atom elementType="H" x3="2.00652646" y3="5.78558303" z3="9.40228919" xFract="0.68790807" yFract="0.85950849" zFract="0.40999307" id="a45"/>
	 					<atom elementType="H" x3="2.79689408" y3="0.82700468" z3="10.55229643" xFract="0.42127028" yFract="0.12286014" zFract="0.46013990" id="a46"/>
	 					<atom elementType="H" x3="2.92876844" y3="4.34449052" z3="11.14390283" xFract="0.69951639" yFract="0.64541922" zFract="0.48593729" id="a47"/>
	 					<atom elementType="H" x3="1.17059897" y3="4.59525251" z3="10.98776371" xFract="0.49194210" yFract="0.68267252" zFract="0.47912874" id="a48"/>
	 					<atom elementType="H" x3="1.86270235" y3="4.21015195" z3="12.57580119" xFract="0.55238075" yFract="0.62546183" zFract="0.54837617" id="a49"/>
	 				</atomArray>
	 			</molecule>
 			</comment>
 		</xsl:if>
 	</xsl:template>
</xsl:stylesheet>
 