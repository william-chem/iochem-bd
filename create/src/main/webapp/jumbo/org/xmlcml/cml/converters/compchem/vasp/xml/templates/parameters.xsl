<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0">   
    
    <xsl:template match="./parameters">        
        <xsl:apply-templates mode="parameters"/>        
    </xsl:template>
    <xsl:include href="parameters/electronic.xsl"/>  
    <xsl:include href="parameters/electronic.exchange.correlation.xsl"/>
    <xsl:include href="parameters/vdw.dft.xsl"/>
    
    <!-- Override default templates -->
    <xsl:template match="text()" mode="parameters"/>
    <xsl:template match="*" mode="parameters"/>
</xsl:stylesheet>