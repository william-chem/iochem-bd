<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
    <xsl:template match="./generator" >
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="generator">
               <generator>
                   <i name="program" type="string">vasp</i>
                   <i name="version" type="string">4.6.3 </i>
                   <i name="subversion" type="string">06Feb03 complex  parallel</i>
                   <i name="platform" type="string">Marenostrum</i>
                   <i name="date" type="string">2012 03 21</i>
                   <i name="time" type="string">13:00:50</i>
               </generator>
            </comment>
        </xsl:if>  
        
        <module id="generator" cmlx:templateRef="generator">
            <scalar dataType="xsd:string" dictRef="cc:program"><xsl:value-of select="helper:trim(./i[@name='program'])"/></scalar>
            <scalar dataType="xsd:string" dictRef="cc:programVersion"><xsl:value-of select="helper:trim(./i[@name='version'])"/></scalar>
            <scalar dataType="xsd:string" dictRef="cc:subversion"><xsl:value-of select="helper:trim(./i[@name='subversion'])"/></scalar>
           <scalar dataType="xsd:string" dictRef="v:platform"><xsl:value-of select="helper:trim(./i[@name='platform'])"/></scalar>
           <scalar dataType="xsd:date" dictRef="cc:rundate"><xsl:value-of select="helper:format_date(helper:trim(./i[@name='date']),helper:trim(./i[@name='time']))"/></scalar>
        </module>
              
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="generator">            
                <module id="generator" cmlx:templateRef="generator">
                    <scalar dataType="xsd:string" dictRef="cc:program">vasp</scalar>
                    <scalar dataType="xsd:string" dictRef="cc:programVersion">4.6.3</scalar>
                    <scalar dataType="xsd:string" dictRef="cc:subversion">06Feb03 complex  parallel</scalar>
                    <scalar dataType="xsd:string" dictRef="v:platform">Marenostrum</scalar>
                    <scalar dataType="xsd:date" dictRef="cc:rundate">2012-03-21T13:00:50</scalar>
                </module>    
            </comment>                          
        </xsl:if>        
    </xsl:template>
    
    <xsl:function name="helper:format_date">
        <xsl:param name="date" />
        <xsl:param name="time" />
        
        <xsl:analyze-string select="$date" regex="\s*(\d+)\s+(\d+)\s+(\d+)\s*">
            <xsl:matching-substring>
                <xsl:value-of select="concat(regex-group(1),'-',regex-group(2),'-',regex-group(3))"/>              
            </xsl:matching-substring>
        </xsl:analyze-string>
        <xsl:text>T</xsl:text>
        <xsl:value-of select="$time"/>
        <xsl:text>Z</xsl:text>
    </xsl:function>
    
</xsl:stylesheet>