<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"    
    exclude-result-prefixes="xs helper"
    version="2.0">
    
    <!-- <xsl:include href="helper.xsl"/> -->
    <xsl:template match="IONS" name="ions">
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="ions">
                <IONS>
                    <NUMBER_OF_ATOMS type="integer" size="1">
                        3
                    </NUMBER_OF_ATOMS>
                    <NUMBER_OF_SPECIES type="integer" size="1">
                        2
                    </NUMBER_OF_SPECIES>
                    <UNITS_FOR_ATOMIC_MASSES UNITS="a.m.u."/>
                    <SPECIE.1>
                        <ATOM_TYPE type="character" size="1" len="3">
                            H  
                        </ATOM_TYPE>
                        <MASS type="real" size="1">
                            1.000000000000000E+000
                        </MASS>
                        <PSEUDO type="character" size="1" len="16">
                            H.pbe-rrkjus.UPF
                        </PSEUDO>
                    </SPECIE.1>
                    <SPECIE.2>
                        <ATOM_TYPE type="character" size="1" len="3">
                            O  
                        </ATOM_TYPE>
                        <MASS type="real" size="1">
                            1.600000000000000E+001
                        </MASS>
                        <PSEUDO type="character" size="1" len="16">
                            O.pbe-rrkjus.UPF
                        </PSEUDO>
                    </SPECIE.2>
                    <PSEUDO_DIR type="character" size="1" len="30">
                        /home/hnguyen/espresso_pseudo/
                    </PSEUDO_DIR>
                    <UNITS_FOR_ATOMIC_POSITIONS UNITS="Bohr"/>
                    <ATOM.1 SPECIES="O  " INDEX="2" tau="1.179089434422790E+001 1.204595806777958E+001 1.149999995913596E+001" if_pos="1 1 1"/>
                    <ATOM.2 SPECIES="H  " INDEX="1" tau="1.344046535769723E+001 1.122010404526686E+001 1.150000002487639E+001" if_pos="1 1 1"/>
                    <ATOM.3 SPECIES="H  " INDEX="1" tau="1.056864029807491E+001 1.066393788695357E+001 1.150000001598764E+001" if_pos="1 1 1"/>
                </IONS> 
            </comment>
        </xsl:if>  
        
        <module id="ions" cmlx:templateRef="ions">           
            <xsl:variable name="species">
                <xsl:apply-templates select="node()[starts-with(name(), 'SPECIE.')]" />    
            </xsl:variable>
            <xsl:copy-of select="$species/node()"/>

            <!-- Retrieve cell parameters to calculate coordinates -->
            <xsl:variable name="cell">
                <xsl:apply-templates select="preceding::node()[starts-with(name(), 'CELL')]" />
            </xsl:variable>
                        
                       
            <list cmlx:templateRef="atoms">
                <xsl:variable name="currentUnits" select="helper:trim(UNITS_FOR_ATOMIC_POSITIONS/@UNITS)"/>
                
                <xsl:variable name="a" select="$cell//*:scalar[@title='a']"/>
                <xsl:variable name="b" select="$cell//*:scalar[@title='b']"/>
                <xsl:variable name="c" select="$cell//*:scalar[@title='c']"/>
                <xsl:variable name="alpha" select="$cell//*:scalar[@title='alpha']"/>
                <xsl:variable name="beta" select="$cell//*:scalar[@title='beta']"/>
                <xsl:variable name="gamma" select="$cell//*:scalar[@title='gamma']"/>
                    
                <xsl:variable name="lattice1" select="tokenize($cell//*:array[@dictRef='cc:lattice'][1], '\s+')"/>
                <xsl:variable name="lattice2" select="tokenize($cell//*:array[@dictRef='cc:lattice'][2], '\s+')"/>
                <xsl:variable name="lattice3" select="tokenize($cell//*:array[@dictRef='cc:lattice'][3], '\s+')"/>
                                    
                <xsl:for-each select="node()[starts-with(name(), 'ATOM.')]">                    
                    <xsl:variable name="atom" select="."/>        
                    <xsl:variable name="positions" select="helper:convert2Angstroms(tokenize(helper:trim($atom/@tau),'\s+'), $currentUnits)"/>                
                    <xsl:variable name="specie" select="helper:trim($atom/@SPECIES)"/>
                    <xsl:variable name="atomType" select="$species//*:scalar[@dictRef='qex:specie' and text() = $specie]/preceding-sibling::*[@dictRef='cc:atomType']"/>                
                    <xsl:variable name="fractional" select="helper:cartesian2fractional($a, $b, $c, $alpha, $beta, $gamma, $positions)"/>                    
                    <atom id="atom" elementType="{$atomType}" x3="{$positions[1]}" y3="{$positions[2]}" z3="{$positions[3]}" xFract="{$fractional[1]}" yFract="{$fractional[2]}" zFract="{$fractional[3]}" units="'nonsi:angstrom'">    
                        <scalar dataType="xsd:string" dictRef="qex:specie"><xsl:value-of select="$specie"/></scalar>
                    </atom>                    
                </xsl:for-each>                    
            </list>            
        </module>
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="ions">
                <module id="ions" cmlx:templateRef="ions">
                    <list cmlx:templateRef="elementType">
                        <scalar dataType="xsd:string" dictRef="cc:atomType">H</scalar>
                        <scalar dataType="xsd:string" dictRef="qex:specie">H</scalar>
                        <scalar dataType="xsd:double" dictRef="cc:atomicmass">1.000000000000000E+000</scalar>
                        <scalar dataType="xsd:string" dictRef="qex:pseudo">H.pbe-rrkjus.UPF</scalar>
                    </list>
                    <list cmlx:templateRef="elementType">
                        <scalar dataType="xsd:string" dictRef="cc:atomType">O</scalar>
                        <scalar dataType="xsd:string" dictRef="qex:specie">O</scalar>
                        <scalar dataType="xsd:double" dictRef="cc:atomicmass">1.600000000000000E+001</scalar>
                        <scalar dataType="xsd:string" dictRef="qex:pseudo">O.pbe-rrkjus.UPF</scalar>
                    </list>
                    <list cmlx:templateRef="atoms">
                        <atom id="atom" xFract="6.239505" yFract="6.374480" zFract="6.085570" units="'nonsi:angstrom'" atomType="O">
                            <scalar dataType="xsd:string" dictRef="qex:specie">O</scalar>
                        </atom>
                        <atom id="atom" xFract="7.112425" yFract="5.937455" zFract="6.085570" units="'nonsi:angstrom'" atomType="H">
                            <scalar dataType="xsd:string" dictRef="qex:specie">H</scalar>
                        </atom>
                        <atom id="atom" xFract="5.592713" yFract="5.643143" zFract="6.085570" units="'nonsi:angstrom'" atomType="H">
                            <scalar dataType="xsd:string" dictRef="qex:specie">H</scalar>
                        </atom>
                    </list>
                </module>
            </comment>
        </xsl:if>  
    </xsl:template>
        
    <xsl:template match="node()[starts-with(name(), 'SPECIE.')]" name="species">
        <list cmlx:templateRef="elementType">
            <scalar dataType="xsd:string" dictRef="cc:atomType"><xsl:value-of select="(tokenize(helper:trim(./PSEUDO/text()), '[\._]+'))[1]"/></scalar>
            <scalar dataType="xsd:string" dictRef="qex:specie"><xsl:value-of select="helper:trim(./ATOM_TYPE/text())"/></scalar>
            <scalar dataType="xsd:double" dictRef="cc:atomicmass"><xsl:value-of select="helper:trim(./MASS/text())"/></scalar>
            <scalar dataType="xsd:string" dictRef="qex:pseudo"><xsl:value-of select="helper:trim(./PSEUDO/text())"/></scalar>            
        </list>        
    </xsl:template>
    
    <xsl:template match="node()[starts-with(name(), 'ATOM.')]">
        <xsl:variable name="currentUnits" select="preceding::UNITS_FOR_ATOMIC_POSITIONS/@UNITS"/>
        <xsl:variable name="atom" select="."/>        
        <xsl:variable name="positions" as="xs:string*" select="tokenize(helper:trim($atom/@tau),'\s+')"/>
             
        <atom id="atom" 
            xFract="{helper:toText($positions[1], $currentUnits, 'nonsi:angstrom','#0.000000')}" 
            yFract="{helper:toText($positions[2], $currentUnits, 'nonsi:angstrom','#0.000000')}" 
            zFract="{helper:toText($positions[3], $currentUnits, 'nonsi:angstrom','#0.000000')}" 
            units="'nonsi:angstrom'">
            <scalar datatype="xsd:string" dictRef="qex:specie"><xsl:value-of select="helper:trim($atom/@SPECIES)"/></scalar>            
        </atom>               
    </xsl:template>

    <xsl:function name="helper:convert2Angstroms" as="xs:string*">
        <xsl:param name="positions" as="xs:string*"/>                
        <xsl:param name="currentUnits"/>
        <xsl:value-of select="helper:toText($positions[1], $currentUnits, 'nonsi:angstrom','#0.000000')"/>
        <xsl:value-of select="helper:toText($positions[2], $currentUnits, 'nonsi:angstrom','#0.000000')"/>
        <xsl:value-of select="helper:toText($positions[3], $currentUnits, 'nonsi:angstrom','#0.000000')"/>
    </xsl:function>
    
</xsl:stylesheet>