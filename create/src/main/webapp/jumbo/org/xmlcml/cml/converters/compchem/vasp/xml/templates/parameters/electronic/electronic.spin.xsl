<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    xmlns="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs helper"
    version="2.0">
           
    <xsl:template match="./separator[@name='electronic spin']"  mode="parameters.electronic">     
        <xsl:param name="isVerbose" tunnel="yes"/>
        
        <xsl:if test="$isVerbose">
            <comment class="example.input" id="electronic.spin">
                <separator name="electronic spin" >
                    <i type="int" name="ISPIN">     1</i>
                    <i type="logical" name="LNONCOLLINEAR"> F  </i>
                    <v name="MAGMOM">      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000      1.00000000</v>
                    <i name="NUPDOWN">     -1.00000000</i>
                    <i type="logical" name="LSORBIT"> F  </i>
                    <v name="SAXIS">      0.00000000      0.00000000      1.00000000</v>
                    <i type="logical" name="LSPIRAL"> F  </i>
                    <v name="QSPIRAL">      0.00000000      0.00000000      0.00000000</v>
                    <i type="logical" name="LZEROZ"> F  </i>
                </separator>
            </comment>
        </xsl:if>  
        
        <module id="electronic.spin" cmlx:templateRef="electronic.spin">
            <xsl:if test="exists(./i[@name='NUPDOWN'])">
                <scalar type="xsd:double" dictRef="v:nupdown"><xsl:value-of select="helper:trim(./i[@name='NUPDOWN'])"/></scalar>                
            </xsl:if>
        </module>               
        
        <xsl:if test="$isVerbose">
            <comment class="example.output" id="electronic.spin">
                <module id="electronic.spin" cmlx:templateRef="electronic.spin">
                    <scalar type="xsd:double" dictRef="v:nupdown">-1.00000000</scalar>
                </module>
            </comment>
        </xsl:if>  
    </xsl:template>
  
</xsl:stylesheet>