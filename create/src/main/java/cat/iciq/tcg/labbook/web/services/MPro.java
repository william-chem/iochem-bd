/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.zk.composers.Main;

/**
 * Servlet implementation class CPro
 */
public class MPro extends Services {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(MPro.class.getName());
       
    /**
     * @see Services#Services()
     */
    public MPro() {
        super();
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String,String> params ,String username,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {    
    	try	{						
			String path = Paths.isAbsolute(params.get("newPath")) ? 
								params.get("newPath") : 
								Paths.getFullPath(params.get("path"), params.get("newPath"));		

			String destinationPath = params.get("newParentPath");
			if(PermissionService.hasPermissionOnProject(path, Permissions.WRITE)) {
				Project project = ProjectService.getByPath(path);
				if (params.get("newGroup") != null) {
					String group = params.get("newGroup");
					if(belongsToGroup(group))
						project.setGroup(ShiroManager.getCurrent().getGroupIdByName(group));
					else
						throw new Exception("Current user doesn't belong to the provided user group.");
				}
				if (params.get("newPerm") != null) {
					String permissions = params.get("newPerm").trim();
					if(!Pattern.matches("11[01][01][01][01]", permissions))
						throw new Exception("Invalid permissions, please use a string of six bits, always starting with 11, ex. 110000.");
					project.setPermissions(permissions);
				}
				if (params.get("newDesc") != null) 
					project.setDescription(truncate(params.get("newDesc"), 304));
				if (params.get("newCG") != null) 
					project.setConceptGroup(truncate(params.get("newCG"), 64));
				if (params.get("newName") != null)
					project.setName(truncate(Main.normalizeField(params.get("newName")), 64));
				if (destinationPath != null) {
					if(destinationPath.length()> 512)
						throw new Exception("Project path exceeds maximum length (256)");
					if(isValidMovePath(username, destinationPath) && PermissionService.hasPermissionOnProject(destinationPath, Permissions.WRITE)) {						
						if(isValidMove(project, destinationPath)) {
							project.setParentPath(destinationPath);
							ProjectService.update(project, destinationPath);
						} else {
							returnKO(ostream,CommandsMessages.PRO_MOD_RES_ACCESS.TITLE(),CommandsMessages.PRO_MOD_RES_ACCESS.MESSAGE());	
						}
					} else
						returnKO(ostream,CommandsMessages.PRO_MOD_RES_ACCESS.TITLE(),CommandsMessages.PRO_MOD_RES_ACCESS.MESSAGE());
				} else
					ProjectService.update(project);
				
				returnOK(ostream);
			}else {
				returnKO(ostream,CommandsMessages.PRO_MOD_RES_ACCESS.TITLE(),CommandsMessages.PRO_MOD_RES_ACCESS.MESSAGE());				
			}
    	} catch(Exception e) {
    		returnKO(ostream,CommandsMessages.PROJECT_MODIFICATION_ERROR.TITLE(), e.getMessage());
    	}    
    }

	private boolean belongsToGroup(String group) throws Exception {
		if(group.equals(""))
			return false;		
		try {			
			int groupId = ShiroManager.getCurrent().getGroupIdByName(group);
			for(String userGroup: ShiroManager.getCurrent().getUserGroups()) 
				if(Integer.parseInt(userGroup) == groupId)
					return true;	
		}catch(Exception e) {
			throw new Exception(CommandsMessages.PRO_MOD_GROUP_DONT_EXIST.MESSAGE());			
		}
		return false;
	}

	private boolean isValidMovePath(String username, String destinationPath) {
		return destinationPath.startsWith(GeneralConstants.DB_ROOT + username) ;
	}

	private boolean isValidMove(Project source, String destinationPath){
		return !(isMovingToSameParentOrItself(source, destinationPath) ||
 				isDestinationChildOfSelection(source, destinationPath) ||
 				existsCollisionsOnDestinationPath(source, destinationPath));
	}

	private boolean isMovingToSameParentOrItself(Project source, String destinationPath){
		return source.getPath().equals(destinationPath) || 
				source.getPath().equals(Paths.getFullPath(destinationPath, source.getName()));
	}
 
	private boolean isDestinationChildOfSelection(Project source, String destinationPath){		
		return Paths.isDescendant(source.getPath(), destinationPath);			
	}

	private boolean existsCollisionsOnDestinationPath(Project source, String destinationPath){		
		try {
			return ProjectService.projectExists(destinationPath, source.getName());
		} catch (Exception e) {
			return true;
		} 											
	}
}
