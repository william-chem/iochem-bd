/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.zk.composers.Main;

/**
 * Servlet implementation class CPro
 */
public class MCalc extends Services {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see Services#Services()
     */
    public MCalc() {
        super();      
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService( Connection conn, OutputStream ostream,HashMap<String,String> params, String username,
								Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
    	try {
			String path = Paths.isAbsolute(params.get("newPath")) ? 
					params.get("newPath") : 
					Paths.getFullPath(params.get("path"), params.get("newPath"));	
			String destinationPath = params.get("newParentPath"); 

			if(PermissionService.hasPermissionOnProject(Paths.getParent(path), Permissions.WRITE)) {
				Calculation calculation = CalculationService.getByPath(path);
				if (params.get("newDesc") != null) 
					calculation.setDescription(truncate(params.get("newDesc"),304));
				if (params.get("newName") != null)	
					calculation.setName(truncate(Main.normalizeField(params.get("newName")),64));				
				if (destinationPath != null) {
					if(destinationPath.length()> 256)
						throw new Exception("Project path exceeds maximum length (256)");
					calculation.setParentPath(destinationPath);
					if(isValidMovePath(calculation.getParentPath()) && PermissionService.hasPermissionOnProject(calculation.getParentPath(), Permissions.WRITE))
						CalculationService.update(calculation, destinationPath);
					else
						returnKO(ostream,CommandsMessages.PROJECT_ERROR_WRITE_PERM.TITLE(),CommandsMessages.PROJECT_ERROR_WRITE_PERM.MESSAGE());
				} else 
					CalculationService.update(calculation);
				returnOK(ostream);
			} else {
				returnKO(ostream,CommandsMessages.PRO_MOD_RES_ACCESS.TITLE(),CommandsMessages.PRO_MOD_RES_ACCESS.MESSAGE());
			}
    	} catch(Exception e) {
    		returnKO(ostream,CommandsMessages.PRO_MOD_RES_ACCESS.TITLE(),e.getMessage());
    	} finally {
    		DatabaseHandler.closeStatementResult(psql, res);
    	}
    }
    
	private boolean isValidMovePath(String destinationPath) throws BrowseCredentialsException {
		return 	destinationPath.startsWith(GeneralConstants.DB_ROOT + ShiroManager.getCurrent().getUserName() + GeneralConstants.PARENT_SEPARATOR);
	}
    
}