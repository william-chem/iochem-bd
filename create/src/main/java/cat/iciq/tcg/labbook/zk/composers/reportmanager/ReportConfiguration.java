/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReportConfiguration {

	private static Logger logger  = LogManager.getLogger(ReportConfiguration.class.getName());
	private static DocumentBuilder docBuilder;
	private static DocumentBuilderFactory bFactory = DocumentBuilderFactory.newInstance();
	private static TransformerFactory tFactory 	= new net.sf.saxon.TransformerFactoryImpl();
	private Document document = null;
	private String xml	= "";				//Used for lazy content parsing, we'll parse xml only if a query() is requested

	static{		
		bFactory.setNamespaceAware(true);
		bFactory.setXIncludeAware(true);
		try {
			docBuilder = bFactory.newDocumentBuilder();
		} catch (ParserConfigurationException e) {
			
		}		
	}
	
	public ReportConfiguration(String xml) throws SAXException, IOException{		
		this.document = null;
		this.xml = xml;
	}
	
	public String toString(){
		//If content manipulation, it returns initial value
		if(document == null)
			return this.xml;				
		try {
			Transformer transformer = tFactory.newTransformer();
			StringWriter buffer = new StringWriter();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
			transformer.transform(new DOMSource(document), new StreamResult(buffer));
			return buffer.toString();		
		} catch (TransformerConfigurationException e) {
			logger.error(e.getMessage());
		} catch (TransformerException e) {
			logger.error(e.getMessage());
		}
		return "";
	}
	
	public NodeList query(String string) throws SAXException, IOException {
		//If we haven't parsed xml string we do it now
		if(document == null && !xml.equals("")){
			InputStream is = new ByteArrayInputStream(xml.getBytes());
			this.document =  docBuilder.parse(is);	
		}		
	
		XPath xPath = XPathFactory.newInstance().newXPath();		
		try {
			return (NodeList)xPath.evaluate(string, document, XPathConstants.NODESET);			
		} catch (XPathExpressionException e1) {
			e1.printStackTrace();
		}
		return null;
	}
}
