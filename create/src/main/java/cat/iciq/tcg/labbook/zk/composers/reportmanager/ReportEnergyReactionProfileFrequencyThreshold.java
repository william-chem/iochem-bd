
/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2020 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.util.HashMap;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Decimalbox;
import org.zkoss.zul.Window;

public class ReportEnergyReactionProfileFrequencyThreshold extends SelectorComposer<Window> {

	private static final long serialVersionUID = 1L;
	
	protected Double baseThreshold;
	
	@Wire
	Window frequencyParameters;
	
	@Wire
	Decimalbox thresholdDb;
	
	@Listen("onClick=#closeBtn")
	public void onCloseBtnClick() {
		frequencyParameters.detach();
	}
	
	@Listen("onClick=#resetBtn")
	public void onResetBtnClick() {
		thresholdDb.setText(String.valueOf(baseThreshold));
	}
	
	@Listen("onClick=#generateBtn")
	public void onBtnClose() {
		HashMap<String, Double> params = new HashMap<String, Double>();
		params.put("threshold", thresholdDb.doubleValue());
		Events.postEvent("onClosing", frequencyParameters, params); 
		frequencyParameters.detach();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void doAfterCompose(Window comp) throws Exception {	
		super.doAfterCompose(comp);	
		HashMap<String, Double> params = (HashMap<String, Double>) Executions.getCurrent().getArg();	
		baseThreshold = params.get("threshold");
		thresholdDb.setText(String.valueOf(baseThreshold));
	}

	public ReportEnergyReactionProfileFrequencyThreshold() {
		super();
	}
	
	
}

