/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.Services;

/**
 * Servlet implementation class DPro
 */
public class DPro extends Services {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(DPro.class.getName());
    
	public DPro() {
        super();
    }

    @Override
    public void executeService( Connection conn, OutputStream ostream,HashMap<String, String> params,String userName,
								Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
    	try {			
    		String path = Paths.isAbsolute(params.get("newPath")) ? 
								params.get("newPath") : 
								Paths.getFullPath(params.get("path"), params.get("newPath"));		
    		
			if (path.equals(GeneralConstants.DB_ROOT + userName)) 
				returnKO(ostream,CommandsMessages.PROJECT_ERROR_WRITE_PERM.TITLE(),CommandsMessages.PROJECT_ERROR_WRITE_PERM.MESSAGE());				
			else {	
				Project project = ProjectService.getByPath(path);
				if(project != null) {
					ProjectService.deleteProject(project.getId(), true);
					returnOK(ostream);
				}else
					returnKO(ostream,CommandsMessages.PROJECT_ERROR.TITLE(),CommandsMessages.PRO_MOD_RES_ACCESS.MESSAGE());
			}											
		} catch(Exception e) {
    		returnKO(ostream,CommandsMessages.PROJECT_ERROR.TITLE(), e.getMessage());
    	}  
    }
}
