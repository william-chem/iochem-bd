package cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph;

import java.util.HashMap;

public class Step {
	
	private String label = "";
	private double energy = 0.0d;
	private String formula = "";
	private String splittedFormula = "";	
	private boolean isTransitionState = false;
	private String serie = "";
	private HashMap <String, String> properties = new HashMap<String, String>();
	
	public Step(String label, Double energy, String formula, String splittedFormula, boolean isTransitionState, int serie) {
		if(label == null || label.isEmpty())
			setLabel(formula);
		else
			setLabel(label);
		setEnergy(energy);
		setFormula(formula);
		setSplittedFormula(splittedFormula);
		setTransitionState(isTransitionState);
		addSerie(String.valueOf(serie));
	}
	
	public String getLabel() {
		return label;
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public double getEnergy() {
		return energy;
	}

	public void setEnergy(double energy) {
		this.energy = energy;
	}

	public String getFormula() {
		return formula;
	}

	public void setFormula(String formula) {
		this.formula = formula;
	}

	public String getSplittedFormula() {
		return splittedFormula;
	}

	public void setSplittedFormula(String splittedFormula) {
		this.splittedFormula = splittedFormula;
	}

	public boolean isTransitionState() {
		return isTransitionState;
	}

	public void setTransitionState(boolean isTransitionState) {
		this.isTransitionState = isTransitionState;
	}

	public void setProperty(String key, String value) {
		this.properties.put(key, value);
	}
	
	public String getProperty(String key) {
		return this.properties.containsKey(key)? this.properties.get(key): null;
	}
	
	public String getSerie() {				
		return this.serie;
	}

	public void addSerie(String serie) {
		this.serie = serie;
	}

	public String toString() {
		if(label.equals(getFormula()))
			return getFormula();
		else {			
			String formulas[] = getSplittedFormula().split("\t<->\t");
			if(formulas.length > 1)
				return label + "\\n" + formulas[1];
			else
				return label + "\\n" + getSplittedFormula();		
		}
	}
}
