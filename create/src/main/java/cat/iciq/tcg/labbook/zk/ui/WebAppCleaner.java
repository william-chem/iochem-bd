/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.ui;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.util.WebAppCleanup;

import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.ConversionService;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.InsertionService;

public class WebAppCleaner implements WebAppCleanup {
	private static final Logger log = LogManager.getLogger(WebAppCleaner.class.getName());
	
	@Override
	public void cleanup(WebApp wapp) throws Exception {
		//Clear user temporal folders
		File tmpFile = new File(wapp.getRealPath("/") + "tmp/");
		try{
			if(tmpFile.exists())
				for(File childFolder : tmpFile.listFiles())
					FileUtils.deleteQuietly(childFolder);
		}catch(Exception e){
			log.error(e.getMessage());
		}	
		//Send termination signal to open queues in services
		try{
			ConversionService.closeConversionQueue();
		}catch(Exception e){
			log.error(e.getMessage());
		}
		try{
			InsertionService.closeInsertionQueue();	
		}catch(Exception e){
			log.error(e.getMessage());
		}
		try {
	        if(WebAppInitiator.contentService != null)
	            WebAppInitiator.contentService.shutdown();		    
		}catch(Exception e) {
		    log.error(e.getMessage());
		}
	}
}
