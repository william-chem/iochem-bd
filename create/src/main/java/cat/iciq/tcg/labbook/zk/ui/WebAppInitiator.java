/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.ui;

import java.io.File;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApp;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.util.WebAppInit;
import org.zkoss.zul.Fileupload;
import org.zkoss.zul.Messagebox;

import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.RestDoiDaemonManager;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.ConversionService;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.InsertionService;

public class WebAppInitiator implements WebAppInit {
	
	private static final Logger log = LogManager.getLogger(WebAppInitiator.class.getName());	 	 	    
	
	protected static ScheduledExecutorService contentService;
	
	@Override
	public void init(WebApp wapp) throws Exception {
		setDoiDaemonStatus();
		setGlobalParameters();
		setDefaultTemplates();
		startConversionThreads();
		startDeferredContentThread();
	}

	public static void setGlobalParameters() {
	    CustomProperties.reloadProperties();
	    WebApps.getCurrent().setAttribute("uploadMaxFileSizeSoft", CustomProperties.getLongProperty("upload.max.file.size.soft", -1L));
	    WebApps.getCurrent().setAttribute("uploadMaxFileSizeHard", CustomProperties.getLongProperty("upload.max.file.size.hard", -1L));
	    WebApps.getCurrent().setAttribute("uploadMaxOutputFileSizeSoft", CustomProperties.getLongProperty("upload.max.output.file.size.soft", -1L));
	    WebApps.getCurrent().setAttribute("uploadMaxOutputFileSizeHard", CustomProperties.getLongProperty("upload.max.output.file.size.hard", -1L));
	    WebApps.getCurrent().setAttribute("uploadMaxFileSizeMessage", CustomProperties.getProperty("upload.max.file.size.message", ""));
	    WebApps.getCurrent().setAttribute("uploadRestrictionUsersHard", CustomProperties.getMultivaluedProperty("upload.restriction.users.hard"));
	    WebApps.getCurrent().setAttribute("deferredContentThreshold", CustomProperties.getLongProperty("deferred.content.disposition.threshold", -1L));	    
	    WebApps.getCurrent().setAttribute("deferredContentServer", CustomProperties.getProperty("deferred.content.server.url"));
	    int maxFileSize = WebApps.getCurrent().getConfiguration().getMaxUploadSize();
	    WebApps.getCurrent().setAttribute("uploadMaxFileSize",  maxFileSize > -1? maxFileSize * 1024L: -1L);  // All limits are stored as bytes
	    
	    
	    //Set rkf2cml executable permissions
	    String rkf2cmlPath = WebApps.getCurrent().getRealPath("/WEB-INF/rkf2cml"); 
	    new File(rkf2cmlPath).setExecutable(true);
	    System.setProperty("RKF2CML_FILE_PATH", rkf2cmlPath);
    }

	public static void setDoiDaemonStatus(){
		int status = Constants.INSTITUTION_STATUS_DISABLED;
		try {
			RestDoiDaemonManager doiManager = new RestDoiDaemonManager();
			status = doiManager.getInstitutionStatus();			
		} catch (Exception e) {
			log.error("Error retrieving DOI daemon status, please check your server can communicate with DOI daemon URL. %s", e.getMessage());
		}		
		WebApps.getCurrent().setAttribute("doiDaemonStatus", status);
		log.info("DOI daemon status set to : %s/nPossible values /n/t0/2: Test server/n/t1/3: Real server/n/t4: Disabled", status);		
	}

	private void setDefaultTemplates() {
		try {
			Messagebox.setTemplate("templates/messagebox.zul");
			Fileupload.setTemplate("templates/fileuploaddlg.zul");
		}catch(Exception e) {
			log.error("Error raised whle loading Messagebox template");
		}			
	}	

	private void startConversionThreads() {
 		ConversionService.init();
		InsertionService.init();		
	}
	
	private void startDeferredContentThread() {
	    if((Long)WebApps.getCurrent().getAttribute("deferredContentThreshold") > -1L) {
	        DeferredContentDaemon content = new DeferredContentDaemon();
	        contentService = Executors.newSingleThreadScheduledExecutor();
	        contentService.scheduleAtFixedRate(content, 0, 1, TimeUnit.DAYS);
	    }
	}
}
