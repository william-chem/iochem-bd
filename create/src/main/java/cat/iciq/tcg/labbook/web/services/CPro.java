/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;

/**
 * Servlet implementation class CPro
 */
public class CPro extends Services {
		
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(CPro.class.getName());
       
    /**
     * @see Services#Services()
     */
    public CPro() {
        super();        
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService( Connection conn, OutputStream ostream, HashMap<String,String> params,String userName,
								Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {    	    	
    	try {	     		
    		String path = params.get("path");    		
    		if(path.length() > 512)
				throw new Exception("Project path exceeds maximum length (256)");

    		Project p = new Project();
    		p.setParentPath(path);
    		p.setName(truncate(Main.normalizeField(params.get("name")),64));
    		p.setDescription(truncate(params.get("description"),304));
    		p.setConceptGroup(truncate(params.get("cg") + "",64));
    		p.setPermissions(buildPermissions(path));    		
    		p.setOwner(ShiroManager.getCurrent().getUserId());
       		p.setGroup(ShiroManager.getCurrent().getMainGroupId());
    		ProjectService.add(p);
    		returnOK(ostream);								
    	}catch(Exception e) {
    		returnKO(ostream, CommandsMessages.PROJECT_ERROR.TITLE(), e.getMessage());
    	}
    }

	private String buildPermissions(String path) throws BrowseCredentialsException {
		if(path.equals(Paths.getFullPath(Constants.BASE_PATH, ShiroManager.getCurrent().getUserName())))	//Base path
			return "111000";
		else {
			return ProjectService.getByPath(path).getPermissions();
		}
	}

    
}
