/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

public class CalculationTypeFile {

	public static enum Use {INPUT, OUTPUT, ADDITIONAL, APPEND};
    
	private int id;
	
	private int calculationTypeId;
	
	private boolean defaultTypeSelection;
	
	private String abbreviation; 
	
	private String fileName;
	
	private String url;
	
	private String description;
	
	private String jumboConverterClass;
	
	private String jumboConverterInType;
	
	private String jumboConverterOutType;
	
	private String mimetype;
	
	private String use;
	
	private String label;
	
	private String behaviour;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getCalculationTypeId() {
		return calculationTypeId;
	}
	public void setCalculationTypeId(int calculationTypeId) {
		this.calculationTypeId = calculationTypeId;
	}
	public boolean isDefaultTypeSelection() {
		return defaultTypeSelection;
	}
	public void setDefaultTypeSelection(boolean defaultTypeSelection) {
		this.defaultTypeSelection = defaultTypeSelection;
	}
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getJumboConverterClass() {
		return jumboConverterClass;
	}
	public void setJumboConverterClass(String jumboConverterClass) {
		this.jumboConverterClass = jumboConverterClass;
	}
	public String getJumboConverterInType() {
		return jumboConverterInType;
	}
	public void setJumboConverterInType(String jumboConverterInType) {
		this.jumboConverterInType = jumboConverterInType;
	}
	public String getJumboConverterOutType() {
		return jumboConverterOutType;
	}
	public void setJumboConverterOutType(String jumboConverterOutType) {
		this.jumboConverterOutType = jumboConverterOutType;
	}
	public String getMimetype() {
		return mimetype;
	}
	public void setMimetype(String mimetype) {
		this.mimetype = mimetype;
	}
	public String getUse() {
		return use;
	}
	public void setUse(String use) {
		this.use = use;
	}
	public String getLabel() {
		return label;
	}
	public void setLabel(String label) {
		this.label = label;
	}
	public String getBehaviour() {
		return behaviour;
	}
	public void setBehaviour(String behaviour) {
		this.behaviour = behaviour;
	}
	
}
