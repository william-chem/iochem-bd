/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.Queries;

/**
 * Servlet implementation class Get Calculation Types
 */
public class GetCalTypes extends Services {
	private static final long serialVersionUID = 1L;
	private MatrixDH data = null;
       
	
	public IDataHolder getData()
	{
		return data;
	}
    /**
     * @see Services#Services()
     */
    public GetCalTypes() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String,String> params, String userName,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException {

    	try
    	{
			psql = conn.prepareStatement(Queries.GET_CAL_TYPES);
			res = psql.executeQuery();
			data = new MatrixDH();

			Vector<String> calFile		= new Vector<String>();
			Vector<String> calType		= new Vector<String>();
			Vector<String> url			= new Vector<String>();
			Vector<String> defType		= new Vector<String>();
			Vector<String> abr			= new Vector<String>();
			Vector<String> jumClass		= new Vector<String>();
			Vector<String> jumInType	= new Vector<String>();
			Vector<String> jumOutType	= new Vector<String>();
			Vector<String> mimetype		= new Vector<String>();
			Vector<String> use			= new Vector<String>();
			Vector<String> label		= new Vector<String>();
			Vector<String> behaviour	= new Vector<String>();
			Vector<String> requires		= new Vector<String>();
			Vector<String> renameto		= new Vector<String>();
			
			while (res.next()){
				calFile.add		(res.getString("cal_file"));
				calType.add		(res.getString("cal_type"));
				url.add			(res.getString("url"));
				defType.add		(res.getString("def_type"));
				abr.add			(res.getString("abbreviation"));
				jumClass.add	(res.getString("jumbo_class"));
				jumInType.add 	(res.getString("jumbo_in_type"));
				jumOutType.add 	(res.getString("jumbo_out_type"));
				mimetype.add	(res.getString("mimetype"));
				use.add 		(res.getString("use"));
				label.add 		(res.getString("label"));
				behaviour.add 	(res.getString("behaviour"));				
				requires.add 	(res.getString("requires"));
				renameto.add 	(res.getString("renameto"));
			}
			data.setStringColumn(0, calFile   );
			data.setStringColumn(1, calType	  );
			data.setStringColumn(2, url		  );
			data.setStringColumn(3, defType	  );
			data.setStringColumn(4, abr		  );
			data.setStringColumn(5, jumClass  );
			data.setStringColumn(6, jumInType );
			data.setStringColumn(7, jumOutType);
			data.setStringColumn(8, mimetype  );
			data.setStringColumn(9, use       );
			data.setStringColumn(10,label     );
			data.setStringColumn(11,behaviour );
			data.setStringColumn(12,requires  );
			data.setStringColumn(13,renameto  );
			
			if (ostream != null) sendObj(data, ostream);
    	}
    	finally
    	{
    		DatabaseHandler.closeStatementResult(psql, res);
    	}
    	
    }
}
