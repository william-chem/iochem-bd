/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.Part;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.CalculationInsertion;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.ConversionService;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.InsertionService;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;

public class UploadService {

    private static final String fileSizeErrorMessage = "The request was rejected because its size (%d MB) exceeds the configured maximum (%d MB). %s";
    
    private static long softMaxFileSize = -1;
    private static long hardMaxFileSize = -1;
    private static long systemMaxFileSize = -1;
    
    private static String maxFileSizeMessage = "";    
    private static HashSet <String> hardQuotaUsers = new HashSet<>();

    static {
        setupUploadRestrictions();
    }

    public static void setupUploadRestrictions() {
        softMaxFileSize = (Long)WebApps.getCurrent().getAttribute("uploadMaxFileSizeSoft");
        hardMaxFileSize = (Long)WebApps.getCurrent().getAttribute("uploadMaxFileSizeHard");
        maxFileSizeMessage = (String)WebApps.getCurrent().getAttribute("uploadMaxFileSizeMessage");
        systemMaxFileSize = (Long)WebApps.getCurrent().getAttribute("uploadMaxFileSize");
        String[] users =  (String[])WebApps.getCurrent().getAttribute("uploadRestrictionUsersHard");
        hardQuotaUsers.clear();
        Collections.addAll(hardQuotaUsers, users);        
    }

    public static void loadCalculationViaWeb(Map<String, String> params) throws Exception {
        params.put(CalculationInsertion.PARAM_USERNAME, ShiroManager.getCurrent().getUserName());       
        params.put(CalculationInsertion.PARAM_USERID, String.valueOf(ShiroManager.getCurrent().getUserId()));
        params.put(CalculationInsertion.PARAM_USERGROUPS_SQL, ShiroManager.getCurrent().getUserGroupsSQL());
        ConversionService.convertFiles(params);
    }

    public static void insertCalculations(Map<String,String> params) {
        //Asynchronous call inside web interface upload
        List<File> fileItems = getFileItemsFromDir(params.get(Constants.LOCAL_DIR));            
        InsertionService.insertFiles(params, fileItems);
    }

    private static List<File> getFileItemsFromDir(String dir) {
        List<File> fileList = new ArrayList<>();        
        File dirF = new File(dir);
        for (File area:dirF.listFiles()) 
            fileList.add(area);     
        return fileList;        
    }

    public static String loadCalculationsViaShell(HttpServletRequest request, Map<String,String> params) throws Exception {     
        //Synchronous call via shell (done this for errors calling Eventqueues with no active Execution,l see : http://forum.zkoss.org/question/56395/eventqueue-not-in-an-execution/
        List<File> fileItems = getItemsFromRequest(request, params);
        params.put(CalculationInsertion.PARAM_USERNAME, ShiroManager.getCurrent().getUserName());       
        params.put(CalculationInsertion.PARAM_USERID, String.valueOf(ShiroManager.getCurrent().getUserId()));
        params.put(CalculationInsertion.PARAM_USERGROUPS_SQL, ShiroManager.getCurrent().getUserGroupsSQL());
        CalculationInsertion calculationInsertion = new CalculationInsertion(params, fileItems);
        String id = calculationInsertion.call().get(CalculationInsertion.PARAM_CALC_ID);                
        TreeEvent.sendEventToUserQueue(new Event("calculationAdded", null, id));
        return id;
    }

    private static List<File> getItemsFromRequest(HttpServletRequest request, Map<String,String> params) throws Exception {
        List<File> files = new ArrayList<>();
        Long maxFileSize = getMaxFileSize();
        File tmpdir = null;
        try {
            tmpdir = FileService.createTemporalDirectory();
            params.put(Constants.LOAD_CALC_TMP_DIR, tmpdir.getCanonicalPath());
            for(Part filePart : request.getParts()) { 
                if(isFile(filePart)){                   
                    if((maxFileSize != -1 && filePart.getSize() > maxFileSize) || (systemMaxFileSize > -1 && filePart.getSize() >  systemMaxFileSize))
                        throw new IOException(
                                String.format(
                                        fileSizeErrorMessage, 
                                        filePart.getSize() / 1048576,  
                                        (maxFileSize != -1 ? maxFileSize / 1048576: systemMaxFileSize / 1048576), 
                                        maxFileSizeMessage));
                    if(filePart.getSize() == 0)
                        throw new IOException("Empty file found on shell uploaded calculation");                                       
                    String fileName = FilenameUtils.getName(FilenameUtils.normalize(filePart.getName()));
                    if(fileName == null || fileName.isEmpty())
                        throw new IOException("Invalid file name used.");
                    File f = new File(Paths.get(tmpdir.getCanonicalPath(), fileName).toAbsolutePath().toString());                    
                    InputStream is = filePart.getInputStream();
                    Files.copy(is, f.toPath(), StandardCopyOption.REPLACE_EXISTING);
                    files.add(f);
                    IOUtils.closeQuietly(is);
                }else{                                              
                    params.put(
                            filePart.getName(), 
                            IOUtils.toString(filePart.getInputStream(), StandardCharsets.UTF_8.name()));
                }
            }            
        } catch(Exception e) {           
            FileUtils.deleteDirectory(tmpdir);
            throw e;
        } finally {
            // Remove temporary disk files
            for(Part filePart : request.getParts()) {
                if(isFile(filePart)) {
                    filePart.delete();
                }
            }
        }       
        return files;
    }    
    
    
    private static boolean isFile(Part filePart){       
        return filePart.getHeader("content-disposition").contains("filename=");        
    }
    
    private static Long getMaxFileSize() {        
        try {
            return hardQuotaUsers.contains(ShiroManager.getCurrent().getUserName())? hardMaxFileSize: softMaxFileSize;    
        }catch(Exception e) {            
            return softMaxFileSize;            
        }                
    }    
         
}