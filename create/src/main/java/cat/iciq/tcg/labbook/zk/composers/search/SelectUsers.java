/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;

public class SelectUsers extends SelectorComposer<Window> {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(SelectUsers.class.getName());
	private static ArrayList<String> users = new ArrayList<String>(); 
	private AddButtonEventListener addButtonEventListener = null;
	private EventQueue<Event> addUsersQueue = null;
	
	@Wire 
	Window selectUser;
	
	@Wire
	Textbox txtName;
	
	@Wire
	Grid grdUsers;

	@Listen("onChanging=#txtName")
	public void onTxtNameChange(InputEvent event){
		clearNamesGrid();				
		loadNamesGrid(event.getValue());		
	}	
	
	@Listen("onClick=#btnClear")
	public void onBtnClearClick(){
		txtName.setValue("");
		clearNamesGrid();
		loadNamesGrid("");
	}
	
	@Listen("onClick=#btnClose; onClick=#btnClose2")
	public void onBtnCloseClick(){
		selectUser.detach();
	}
	
	static{
		try {
			users = new ArrayList<String>(Arrays.asList(ShiroManager.getCurrent().getOwners()));
			Collections.sort(users);
		} catch (BrowseCredentialsException e) {
			log.error(e.getMessage());
		}
	}

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);		
		addUsersQueue = EventQueues.lookup("searchaddusers", 	EventQueues.DESKTOP, true);
		addButtonEventListener = new AddButtonEventListener(addUsersQueue);
		onBtnClearClick();
	}
	
	private void clearNamesGrid(){
		while(grdUsers.getRows().getFirstChild() != null)
			grdUsers.getRows().getFirstChild().detach();			
	}
	
	@SuppressWarnings("unchecked")
	private Collection<String> filterUsernames(String nameHint){
		nameHint = nameHint.trim();
		return CollectionUtils.select(users, new ContainsPredicate(nameHint));		
	}
	
	private void loadNamesGrid(String hint){
		Collection<String> results = filterUsernames(hint);		
		for(String result : results)			
			grdUsers.getRows().appendChild(buildUserRow(result));
		grdUsers.invalidate();
	}	
	
	private Row buildUserRow(String username){
		Row row = new Row();		

		Cell cellBtnAdd = new Cell();
		cellBtnAdd.setAttribute("username", username);

		Button btnAdd = new Button();
		btnAdd.setAttribute("username", username);
		btnAdd.setSclass("btn btn-outline-secondary add-user-button ml-2");
		btnAdd.addEventListener(Events.ON_CLICK, addButtonEventListener);
		cellBtnAdd.appendChild(btnAdd);

		Cell cellTxtName = new Cell();
		Label lblName = new Label(username);
		cellTxtName.appendChild(lblName);

		row.appendChild(cellBtnAdd);
		row.appendChild(cellTxtName);

		return row;
	}
	
	class ContainsPredicate implements Predicate{

		Pattern namePattern = null;
		
		public ContainsPredicate(String hint){
			if(hint == null || hint.equals(""))
				namePattern = Pattern.compile(".*");
			
			try{
				namePattern = Pattern.compile(".*" + hint + ".*");
			}catch(Exception e){
				namePattern = Pattern.compile(".*");
			}		
		}
		
		@Override
		public boolean evaluate(Object object) {
			String candidate = (String) object;  
			return namePattern.matcher(candidate).matches(); 
		}
	}

	class AddButtonEventListener implements EventListener<Event>{
		EventQueue<Event> addUsersQueue = null;  
		public AddButtonEventListener(EventQueue<Event> addUsersQueue){
			this.addUsersQueue = addUsersQueue;				
		}
		
		@Override
		public void onEvent(Event event) throws Exception {
			Button selected = (Button)event.getTarget();
			selected.setSclass("btn btn-secondary add-group-button ml-2");
			
			String username = (String)(selected.getAttribute("username"));
			addUsersQueue.publish(new Event("useradded", null, username));
		}	
	}
	
}




