/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.owasp.esapi.util.CollectionsUtil;

import cat.iciq.tcg.labbook.datatype.CalculationType;
import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.web.definitions.Queries;

public class CalculationTypeService {
    
    private static final Logger log = LogManager.getLogger(CalculationTypeService.class);
    
    private static HashMap<Integer, CalculationType> calculationTypeById;
    private static HashMap<String, CalculationType> calculationTypeByName;
    
    static {
        calculationTypeById = new HashMap<>();
        calculationTypeByName = new HashMap<>();
        loadCalculationTypes();
    }
    
    private static void loadCalculationTypes() {
        Connection conn = null;
        PreparedStatement psql = null;
        ResultSet rs = null;
        try {
            conn = DatabaseHandler.getCreateConnection();
            psql = conn.prepareStatement(Queries.GET_CALCULATION_TYPES);                                  
            rs = psql.executeQuery();           
            while(rs.next()) {
                CalculationType type = new CalculationType();               
                type.setId(rs.getInt("id"));
                type.setCreationTime(rs.getTimestamp("creation_time"));
                type.setAbbreviation(rs.getString("abr"));
                type.setName(rs.getString("name"));
                type.setDescription(rs.getString("description"));
                type.setMetadataTemplate(rs.getString("metadata_template"));                
                //Load cache of CalculationTypes
                calculationTypeById.put(type.getId(), type);
                calculationTypeByName.put(type.getName(), type);                
            }
        } catch (Exception e) {
            log.error("Exception raised loading calculation types.");
            log.error(e.getMessage());
        } finally{
            DatabaseHandler.closeStatementResult(psql, rs);
            DatabaseHandler.closeConnection(conn);
        }                       
    }
    
    public static CalculationType getCalculationTypeByName(String name) throws SQLException{
        if(calculationTypeByName.containsKey(name))
            return calculationTypeByName.get(name);
        if(name == null || name.equals(""))
            return null;
        
        //Not cached. retrieve it from database 
        Connection conn = null;
        PreparedStatement psql = null;
        ResultSet rs = null;
        try {
            conn = DatabaseHandler.getCreateConnection();
            psql = conn.prepareStatement(Queries.GET_CALCULATION_TYPE_BY_NAME);
            psql.setString(1,name);                    
            rs = psql.executeQuery();
            if(rs.next()) {
                CalculationType calcType = new CalculationType();
                calcType.setId(rs.getInt(1));
                calcType.setCreationTime(rs.getTimestamp(2));
                calcType.setAbbreviation(rs.getString(3));
                calcType.setName(rs.getString(4));
                calcType.setDescription(rs.getString(5));
                calcType.setMetadataTemplate(rs.getString(6));
                //Load cache of calculationTypes
                calculationTypeByName.put(calcType.getName(), calcType);
                calculationTypeById.put(calcType.getId(), calcType);
                return calcType;
            }
        } catch (Exception e) {
            throw new SQLException(e);          
        } finally{
            DatabaseHandler.closeStatementResult(psql, rs);
            DatabaseHandler.closeConnection(conn);
        }       
        return null;        
    }

    public static CalculationType getCalculationTypeById(int id) throws SQLException {
        if(calculationTypeById.containsKey(id))
            return calculationTypeById.get(id);     
        //Not cached. retrieve it from database 
        Connection conn = null;
        PreparedStatement psql = null;
        ResultSet rs = null;
        try {
            conn = DatabaseHandler.getCreateConnection();
            psql = conn.prepareStatement(Queries.GET_CALCULATION_TYPE_BY_ID);
            psql.setInt(1,id);                     
            rs = psql.executeQuery();
            if(rs.next()) {
                CalculationType calcType = new CalculationType();
                calcType.setId(rs.getInt(1));
                calcType.setCreationTime(rs.getTimestamp(2));
                calcType.setAbbreviation(rs.getString(3));
                calcType.setName(rs.getString(4));
                calcType.setDescription(rs.getString(5));
                calcType.setMetadataTemplate(rs.getString(6));
                //Load cache of calculationTypes
                calculationTypeByName.put(calcType.getName(), calcType);
                calculationTypeById.put(calcType.getId(), calcType);
                return calcType;
            }
        } catch (Exception e) {
            throw new SQLException(e);          
        } finally{
            DatabaseHandler.closeStatementResult(psql, rs);
            DatabaseHandler.closeConnection(conn);
        }       
        return null;        
    }

    public static List<CalculationType> getAll() {
        List<CalculationType> calculationTypes = new ArrayList<CalculationType>(calculationTypeById.values()); 
        calculationTypes.sort(new CalculationTypeSorter());
        return calculationTypes;
    }
    
}

class CalculationTypeSorter implements Comparator<CalculationType> {

    @Override
    public int compare(CalculationType o1, CalculationType o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
