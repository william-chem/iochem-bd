/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.sql.Timestamp;

import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;

import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.TokenManager;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class Project extends PublicableEntity {

	private int owner;

	private int group;

	private String permissions;

	private String conceptGroup;

	private Timestamp modificationDate;

	private Timestamp certificationDate;

	private String state;

	public int getOwner() {
		return owner;
	}

	public void setOwner(int owner) {
		this.owner = owner;
	}

	public int getGroup() {
		return group;
	}

	public void setGroup(int group) {
		this.group = group;
	}

	public String getFormattedPermissions() {
		return binaryPermissionToLinux(this.getPermissions());
	}

	public String getPermissions() {
		return permissions;
	}

	public void setPermissions(String permissions) {
		this.permissions = permissions;
	}

	public String getConceptGroup() {
		return conceptGroup;
	}

	public void setConceptGroup(String conceptGroup) {
		this.conceptGroup = conceptGroup;
	}

	public Timestamp getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Timestamp modificationDate) {
		this.modificationDate = modificationDate;
	}

	public Timestamp getCertificationDate() {
		return certificationDate;
	}

	public void setCertificationDate(Timestamp certificationDate) {
		this.certificationDate = certificationDate;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public static String binaryPermissionToLinux(String perm) {
		if (perm.equals("111111"))
			return "rwrwrw";
		else if (perm.equals("111110"))
			return "rwrwr-";
		else if (perm.equals("111100"))
			return "rwrw--";
		else if (perm.equals("111000"))
			return "rwr---";
		else if (perm.equals("110000"))
			return "rw----";
		else if (perm.equals("100000"))
			return "r-----";
		else
			return "------";
	}

	public static String linuxPermissionToBinary(String perm) {
		if (perm.equals("rwrwrw"))
			return "111111";
		else if (perm.equals("rwrwr-"))
			return "111110";
		else if (perm.equals("rwrw--"))
			return "111100";
		else if (perm.equals("rwr---"))
			return "111000";
		else if (perm.equals("rw----"))
			return "110000";
		else if (perm.equals("r-----"))
			return "100000";
		else
			return "000000";
	}

	public boolean hasGroupReadPermission() {
		return permissions != null &&  permissions.matches("..1...");	
	}
	
	public boolean hasGroupWritePermission() {
		return permissions != null &&  permissions.matches("...1..");	
	}
	
	public boolean hasOthersReadPermission() {
		return permissions != null &&  permissions.matches("....1.");	
	}
	
	public boolean hasOthersWritePermission() {
		return permissions != null &&  permissions.matches(".....1");	
	}
	
	public String getEditHash() {
		if(isPublished() && getHandle() != null) {			
			String salt = CustomProperties.getProperty("module.communication.secret");
			String secret = Main.getBrowseBaseUrl() + "#" + this.getHandle() + "#" + "edit-collection";
			
			String editHash = TokenManager.encode(salt, secret);
			editHash = editHash.substring(editHash.length() -24, editHash.length());
			return editHash;			
		} else {
			return null;
		}
	}
	
}
