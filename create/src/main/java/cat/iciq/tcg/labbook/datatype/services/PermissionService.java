/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import org.apache.commons.lang.ArrayUtils;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;

public class PermissionService {

	public static enum Permissions { READ, WRITE, DELETE};   

	public static boolean hasPermissionOnProject(String path, Permissions permission) {
		if(isUserRootPath(path))
			return true;
		
		Project project = ProjectService.getByPath(path);
		if(project == null)
			return false;
		return hasPermissionOnProject(project.getId(), permission);
	}
	
	public static boolean hasPermissionOnProject(int projectId, Permissions permission) {
		Project project = ProjectService.getById(projectId);
		if(project == null)
			return false;
		return hasPermission(project, permission);	
	}
		
	public static boolean hasPermissionOnCalculation(long calculationId, Permissions permission) {
		Calculation calculation = CalculationService.getById(calculationId);
		Project project = ProjectService.getByPath(calculation.getParentPath());
		if(calculation == null || project == null)
			return false;
		return hasPermission(project, permission);		
	}
	
	private static boolean hasPermission(Project project, Permissions permission) {		
		try {
			boolean belongsToGroup = false;	
			if(project.getOwner() == ShiroManager.getCurrent().getUserId()) 
				return true;
			else if(permission == Permissions.DELETE)					//DELETE permission is only allowed to the owner of the entity
				return false;			
			// Check others first
			if((permission == Permissions.WRITE && project.hasOthersWritePermission()) || 
					(permission == Permissions.READ && project.hasOthersReadPermission()))
				return true;
			// Check group at last			
			String[] groups = ShiroManager.getCurrent().getUserGroups();
			belongsToGroup = ArrayUtils.contains(groups, String.valueOf(project.getGroup()));			
			if( belongsToGroup && (   
					(permission == Permissions.WRITE && project.hasGroupWritePermission()) || 
					(permission == Permissions.READ && project.hasGroupReadPermission())))
				return true;
		}catch(Exception e) {
			
		}
		return false;	
	}
	
	private static boolean isUserRootPath(String path) {
		try {
			return path.equals(GeneralConstants.DB_ROOT + ShiroManager.getCurrent().getUserName());	
		}catch(Exception e) {
			return false;
		}		
	}
}
