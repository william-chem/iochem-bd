package cat.iciq.tcg.labbook.zk.ui;

import javax.naming.NamingException;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.db.DatabaseHandler;

public class WebappServletListener implements ServletContextListener {

	private static final Logger log = LogManager.getLogger(WebappServletListener.class.getName());
	
	@Override
	public void contextInitialized(ServletContextEvent sce) {
	
	}

	@Override
	public void contextDestroyed(ServletContextEvent sce) {
		try {
			DatabaseHandler.deregisterDriver();
		} catch (NamingException e) {
			log.error("Could not deregister JDBC PostgreSQL drivers.");			
		}
	}
}
