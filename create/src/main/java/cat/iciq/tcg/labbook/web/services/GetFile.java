/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cat.iciq.tcg.labbook.datatype.services.AssetstoreService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;

public class GetFile extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {					
		try {
			long calculationId = Long.parseLong(request.getParameter("id"));
			String fileName = request.getParameter("file");
			
			if(fileName != null && 
					!fileName.equals("undefined") && 
					PermissionService.hasPermissionOnCalculation(calculationId, Permissions.READ)) {
				File file = AssetstoreService.getCalculationFileByName(calculationId, fileName);
				if(file.exists()){
					response.setContentLength((int)file.length());		   		    		       
					response.getOutputStream().write(Files.readAllBytes(file.toPath()));
				}else{
					response.sendError(HttpServletResponse.SC_GONE);
				}
			}else{
				response.sendError(HttpServletResponse.SC_FORBIDDEN);
			}			
		}catch(Exception e) {
			response.sendError(HttpServletResponse.SC_FORBIDDEN);
		}
	}
	
	

}
