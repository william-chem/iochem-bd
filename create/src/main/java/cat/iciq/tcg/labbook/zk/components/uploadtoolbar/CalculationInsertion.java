/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.uploadtoolbar;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.security.cert.X509Certificate;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import javax.naming.InvalidNameException;
import javax.naming.ldap.LdapName;
import javax.naming.ldap.Rdn;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swordapp.client.ProtocolViolationException;
import org.swordapp.client.SWORDClientException;
import org.swordapp.client.SWORDCollection;
import org.swordapp.client.ServiceDocument;
import org.xml.sax.SAXException;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.CalculationType;
import cat.iciq.tcg.labbook.datatype.services.AssetstoreService;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.CalculationTypeService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.exceptions.ExtractionException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.web.definitions.Queries;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.InchiGenerator;
import cat.iciq.tcg.labbook.web.utils.RestManager;
import cat.iciq.tcg.labbook.web.utils.SwordManager;

public class CalculationInsertion implements Callable<Map<String, String>> {

	private static final Logger logger = LogManager.getLogger(CalculationInsertion.class.getName());

	public static Templates geometryExtractionTemplate;
	
	private Map<String,String> params = null;
	private List<File> fileItems = null;	
	
	public static final String VASP_CALCULATION_TYPE= "Vasp"; 
	public static final String METS_FILE_NAME		= "mets.xml";
	public static final String THUMBNAIL_FILE_NAME 	= "thumbnail.jpeg";
	public static final String GEOMETRY_FILE_NAME 	= "geometry.cml";
	public static final String NO_THUMBNAIL_LOCAL_PATH 	="/images/thumbnail.jpeg";
	public static final String JMOLDATA_LOCAL_PATH 	="/html/xslt/jmol/JmolData.jar";	
	public static final String THUMBNAIL_SCRIPT_LOCAL_PATH 	= "/html/xslt/jmol/thumbnail.spt";
	public static final String GEOMETRY_SCRIPT_LOCAL_PATH 	= "/html/xslt/jmol/geometry.spt";	
	
	public static final String PARAM_CALC_ID = "calcId";
	public static final String PARAM_NAME = "name";
    public static final String PARAM_DESC = "description";
    public static final String PARAM_PATH = "path";
    public static final String PARAM_TYPE = "type";
    public static final String PARAM_AUTOPUBLISH = "autopublish";
    public static final String PARAM_USERNAME = "userLogin";
    public static final String PARAM_USERID = "userID";
    public static final String PARAM_USERGROUPS_SQL = "userGroupsSQL";    
    public static final String PARAM_UUID = "UUID";
    public static final String PARAM_EXCEPTION_TITLE = "exceptionTitle";
    public static final String PARAM_EXCEPTION_MESSAGE = "exceptionMessage";
	
    private static String COVERAGE_SPATIAL_LOCATION = null;
    
	static{		
		try {
			FileInputStream fis = new FileInputStream(WebApps.getCurrent().getRealPath("/WEB-INF/extract-geometry.xsl"));
			geometryExtractionTemplate = new net.sf.saxon.TransformerFactoryImpl().newTemplates(new StreamSource(fis));
		} catch (TransformerConfigurationException e) {
			e.printStackTrace();
		} catch (TransformerFactoryConfigurationError e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
	}
    
	public CalculationInsertion(Map<String,String> params, List<File> fileItems){	
		this.params = params;
		this.fileItems = fileItems;
	}
	
	@Override
	public Map<String, String> call() throws Exception {
		insertCalculation();					
		return params;
	}

    private void insertCalculation() throws Exception {
    	Connection conn = DatabaseHandler.getCreateConnection();    	
    	String userName = params.get(PARAM_USERNAME);
    	int userId = Integer.parseInt(params.get(PARAM_USERID));
    	String userGroupsSQL = params.get(PARAM_USERGROUPS_SQL);
    	long calcId = 0;
    	String directoryPath = null;
    	
    	try{    	
    		if(canWriteInPath(conn, params, userName, userId, userGroupsSQL)){
    			try{	
    				calcId = saveCalculation(params);  
    				params.put(PARAM_CALC_ID, String.valueOf(calcId));
    				directoryPath = createDirectory(calcId);
    				saveFiles(conn, fileItems, directoryPath, calcId);
    				setInchiFormulaOnOutputFile(calcId);
    				extractGeometry(calcId);
    				buildThumbnail(calcId);		       		        	
    			}catch(Exception e){
	    			if(calcId != 0){
	    				deleteCalculationDirectory(directoryPath);		    		
	    				removeCalculationFromCreate(conn, calcId);
	    			}						
    				throw e;
    			}
    		}
        }catch(Exception e){
            FileUtils.deleteDirectory(new File(params.get(Constants.LOAD_CALC_TMP_DIR)));
        	if(params.containsKey(Constants.LOAD_CALC_WEB)){   // Is web upload
            	ExtractionException extExc = new ExtractionException();
    			extExc.setMessage(e.getMessage());
    			extExc.setThreadUUID(params.get("UUID"));
    			extExc.setUser(params.get("userLogin"));
    			throw extExc;
        	}else {
        	    throw e;
        	}
    	}finally{
    		DatabaseHandler.closeConnection(conn);
    	}
    	if(params.containsKey(Constants.LOAD_CALC_TMP_DIR))
    		FileUtils.deleteDirectory(new File(params.get(Constants.LOAD_CALC_TMP_DIR)));
    }

	private boolean canWriteInPath(Connection conn, Map<String,String> vars, String userName, Integer userId, String userGroupsSQL) throws IOException, SQLException, BrowseCredentialsException{
    	PreparedStatement psql = null;
    	ResultSet res = null;    	
    	if ((vars.get(PARAM_PATH).equals(GeneralConstants.DB_ROOT + userName))){
    		logger.error(CommandsMessages.CALC_PATH_ERROR.TITLE() + " " + CommandsMessages.CALC_PATH_ERROR.MESSAGE());
    		return false;     	
    	}
		if(vars.get(PARAM_PATH).length() > 256){
    		logger.error(CommandsMessages.CALC_PATH_ERROR.TITLE() + " " + CommandsMessages.CALC_LONG_PATH_ERROR.MESSAGE());
			return false;		
		}
    	psql = conn.prepareStatement(Queries.CHECK_WRITE_PERMISSIONS.replaceAll("@USER_GROUPS", userGroupsSQL));
		psql.setInt(1, userId);
		psql.setString(2, Paths.getParent(vars.get(PARAM_PATH)));
		psql.setString(3, Paths.getTail(vars.get(PARAM_PATH)));		
		res = psql.executeQuery();
		if (!(res.next()))  // if there is no next. The user cannot write the project on the location
		{
			logger.error(CommandsMessages.PROJECT_ERROR_WRITE_PERM.TITLE() + " " + CommandsMessages.PROJECT_ERROR_WRITE_PERM.MESSAGE());
			return false;
		}
		DatabaseHandler.closeStatementResult(psql, res);
		return true;
    }
    
    private void setInchiFormulaOnOutputFile(long id) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException{    	    
    	File outputFile = new File(CalculationService.getOutputFilePath(id));
    	new InchiGenerator().addInchi(outputFile);
    }
    
	public void extractGeometry(long calcId) throws Exception {				
		String geometryFile = FileService.getCalculationPath(calcId, CalculationInsertion.GEOMETRY_FILE_NAME);		
		Transformer transformer;
		try {
			transformer = geometryExtractionTemplate.newTransformer();
			StreamSource xml = new StreamSource(new File(CalculationService.getOutputFilePath(calcId)));
			StringWriter writer = new StringWriter();
	        StreamResult result = new StreamResult(writer);
	        transformer.transform(xml, result);
	        FileUtils.writeStringToFile(new File(geometryFile), writer.toString());
		} catch (TransformerConfigurationException e) {
			throw new Exception("Bad geometry extraction configuration setup. Please contact your ioChem-BD administrator.");			
		} catch (IOException e) {
			throw new Exception("Could not store extracted geometry file. Please contact your ioChem-BD administrator.");
		} catch (TransformerException e) {
			throw new Exception("Could not extract geometry from uploaded calculation.");
		}		
	}
        
    private void buildThumbnail(long id) throws Exception  {    	
      	String jmolDataJar = FileService.getCreateWebapp(JMOLDATA_LOCAL_PATH);
    	String thumbnailFile = FileService.getCalculationPath(id, THUMBNAIL_FILE_NAME);    	    	
    	String geometryFile = FileService.getCalculationPath(id, GEOMETRY_FILE_NAME);    	
    	String thumbnailScriptFilePath 	= FileService.getCreateWebapp(THUMBNAIL_SCRIPT_LOCAL_PATH);  
    	
        ArrayList<String> commandList = new ArrayList<String>();
    	String commands[];	    	           
		commandList.add("java");
		commandList.add("-Djava.awt.headless=true");          
		commandList.add("-jar");
		commandList.add(jmolDataJar);
		commandList.add("-i");		  
		commandList.add("-L");
		commandList.add("-n");   
		commandList.add("-s");
		commandList.add(thumbnailScriptFilePath);
		commandList.add("-g500x500");
		commandList.add(geometryFile);
		commandList.add("-w");
		commandList.add("JPG:" + thumbnailFile);
		commands = new String[commandList.size()];
		commandList.toArray(commands);
		try{
		    Process child = Runtime.getRuntime().exec(commands);
		    InputStream is = child.getInputStream();
		    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    String aux = br.readLine();
		    logger.info("CML thumbnail creation command:" + commandList.toString());              
		    while (aux != null) 			                  
		        aux = br.readLine();			
		}catch (Exception e){
			logger.error("Error parsing CML document to extract thumbnail." + e.getMessage());
		}   
		//If thumbnail was not properly generated, we'll use "thumbnail not available" placeholder
		File thumbnail = new File(thumbnailFile);
		if(!thumbnail.exists())
			try {			    
				FileUtils.copyFile(new File(FileService.getCreateWebapp(NO_THUMBNAIL_LOCAL_PATH)), thumbnail);
			} catch (IOException e) {
				logger.error("Error copying noThumbnail image into calculation folder. " + e.getMessage());
    	}    	    
    }
    
    private void appendItemMetadata(HashMap<String,String> publishParams) throws BrowseCredentialsException{
    	publishParams.put("metadata1#dc:type", "dataset");
    	publishParams.put("metadata2#dc:contributor.dcterms:author", ShiroManager.getCurrent().getUserFullName()); 		
		publishParams.put("metadata3#dc:publisher", CustomProperties.getProperty("mets.institution.name"));				
		publishParams.put("metadata4#dc:rights", "CC BY 4.0 (c) " +  CustomProperties.getProperty("mets.institution.name") + ", " +   Calendar.getInstance().get(Calendar.YEAR));
		publishParams.put("metadata5#dc:rights.dcterms:URI", "http://creativecommons.org/licenses/by/4.0/");
		publishParams.put("metadata6#dcterms:accessRights", "info:eu-repo/semantics/openAccess");

		if(COVERAGE_SPATIAL_LOCATION == null)
			COVERAGE_SPATIAL_LOCATION = getLocationFromHttpsCertificate();
		
		if(!COVERAGE_SPATIAL_LOCATION.equals(""))
			publishParams.put("metadata" + publishParams.size() +  "#dcterms:spatial", COVERAGE_SPATIAL_LOCATION);		
    }
    
    private String getLocationFromHttpsCertificate(){
		StringBuilder sb = new StringBuilder();
		try {
			X509Certificate cert = (X509Certificate) RestManager.getCurrentCertificate();		
			String dn = cert.getSubjectX500Principal().getName();
			LdapName ldapDN = new LdapName(dn);
			for(Rdn rdn: ldapDN.getRdns()) {
				if(rdn.getType().matches("L|ST|C") && !((String)rdn.getValue()).trim().equals("") && !((String)rdn.getValue()).trim().equals("Unknown"))
					sb.append((String)rdn.getValue() + " ");
			}			
		} catch (InvalidNameException e) {
			logger.error(e.getMessage());			
		}		
		return sb.toString();
	}
 
    private SWORDCollection getSWORDCollectionForCalculation(SwordManager swordManager, String communityHandle, String collectionHandle) throws SWORDClientException, ProtocolViolationException{    	
		ServiceDocument serviceDocument= swordManager.getServiceDocument("/" + communityHandle);
		for(SWORDCollection collection : serviceDocument.getWorkspaces().get(0).getCollections()){
			if(collection.getHref().getPath().endsWith(collectionHandle))
				return collection;
		}	
		return null;
    }

    private long saveCalculation(Map<String,String> vars) throws Exception{
    	String name = Services.truncate(vars.get(PARAM_NAME),64);
    	String path = vars.get(PARAM_PATH);
    	String description = Services.truncate(vars.get(PARAM_DESC),304);    	
    	CalculationType calcType = CalculationTypeService.getCalculationTypeByName(vars.get(PARAM_TYPE));    	
    	return CalculationService.addCalculation(path, name, description, calcType.getId());    
    }
    
    private String createDirectory(long calcId) throws Exception {        
        String path = FileService.getCalculationPath(calcId);
		new File(path).mkdir();
		return path;
    }

    private void saveFiles(Connection conn, List<File> fileItems, String filesStorage, long calcId) throws Exception {        
		try {
		    Iterator<File> i = fileItems.iterator();
		    while(i.hasNext()){              
	            saveFile(conn, calcId, i.next());
	        }   
		}catch(SQLException e){
		    logger.error(e.getMessage() + Services.getStackTrace(e));
            throw new Exception(CommandsMessages.CALC_ALREADY_EXIST_ERROR.MESSAGE());          
		}catch(Exception e) {
		    logger.error(e.getMessage() + Services.getStackTrace(e));
            throw new Exception(CommandsMessages.CALC_IO_ERROR.MESSAGE());
		}
    }
    
    private void saveFile(Connection conn, long calcId, File file) throws Exception {                       
        String filename = file.getName(); 
        if(filename != null) {           
            if(filename.equals(METS_FILE_NAME)) {
                storeMetsMetadata(conn, calcId, file);
            } else {
                moveCalculationFile(calcId, filename, file);                                 
                storeCalculationFile(conn, calcId, filename);          
            }
        }
    }
    
    private void moveCalculationFile(long calcId, String filename, File file) throws Exception {
       File outFile = AssetstoreService.getCalculationFileByName(calcId, filename);
       Files.move(file.toPath(), outFile.toPath(), StandardCopyOption.REPLACE_EXISTING);
    }

    /**
     * METS metadata file is store in database after assigning calculation ID inside its content
     * @param obj Object that holds the METS file, can be a File or a FileItem either
     * @throws IOException 
     * @throws SQLException 
     */
    private void storeMetsMetadata(Connection conn, long calcId, File file) throws IOException, SQLException {
        PreparedStatement psql = null;
        MetsFileHandler metsFile = null;
        try {
            metsFile = new MetsFileHandler(Files.readAllBytes(java.nio.file.Paths.get(file.getAbsolutePath())));
            metsFile.setCalcID(calcId);
            psql = conn.prepareStatement(Queries.SET_CALCULATION_METS);
            psql.setString(1,metsFile.toString());
            psql.setLong(2,calcId);
            psql.executeUpdate();
        }finally{
            DatabaseHandler.closeStatementResult(psql, null);
        }
    }

    private void storeCalculationFile(Connection conn, long calcId, String filename) throws Exception {
        PreparedStatement psql = null;
        try {            
            psql = conn.prepareStatement(Queries.CREATE_AREA_FILE_REF);
            psql.setLong(1,calcId);
            psql.setString(2,filename);
            psql.setString(3,AssetstoreService.getCalculationFilePartialPath(calcId, filename));
            psql.executeUpdate();       
        }finally{
            DatabaseHandler.closeStatementResult(psql, null);
        }     
    }
   
    private void deleteCalculationDirectory(String directoryPath){
    	if(directoryPath != null)
    		FileUtils.deleteQuietly(new File(directoryPath));		
    }

	private void removeCalculationFromCreate(Connection conn, long calcId){
		if(calcId <= 0)
			return;
		PreparedStatement psql = null;
		try {
			psql = conn.prepareStatement(Queries.getREMOVE_CALC(Long.toString(calcId)));
			psql.executeUpdate();
		} catch (SQLException e) {			
			logger.error(e.getMessage());
		}
	}		

}
