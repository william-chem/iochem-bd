/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.web.definitions.Queries;

public class ReportTypeService {
	
	private static final Logger log = LogManager.getLogger(ReportTypeService.class);	
	private static HashMap<Integer, ReportType> reportTypeById;
	
	static {		
		loadReportTypes();
	}
	
	private static void loadReportTypes() {
		reportTypeById = new HashMap<>();
		
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_REPORT_TYPES);	        	        	      
	        rs = psql.executeQuery();	        
	        while(rs.next()) {
	        	ReportType reportType = new ReportType();
	        	reportType.setId(rs.getInt(1));
	        	reportType.setName(rs.getString(2));
	        	reportType.setOutputType(rs.getString(3));
	        	reportType.setXmlStylesheet(rs.getString(4));
	        	reportType.setAssociatedZul(rs.getString(5));
	        	reportType.setEnabled(rs.getBoolean(6));
	        	//Load cache of ReportTypes	        	
	        	reportTypeById.put(reportType.getId(), reportType);
	        }
		} catch (Exception e) {
			log.error("Exception raised loading report types.");
			log.error(e.getMessage());
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}		
	}
	
	public static List<ReportType> getActiveReportTypes() {		
		ArrayList<ReportType> activeReportTypes = new ArrayList<ReportType>();
		for(ReportType reportType: reportTypeById.values())
			if(reportType.isEnabled())
				activeReportTypes.add(reportType);			
		return activeReportTypes;		
	}
	
	public static List<ReportType> getAllReportTypes() {
		return new ArrayList<ReportType>(reportTypeById.values());				
	}
	
	public static ReportType getReportTypeById(int id) throws Exception{		
		if(reportTypeById.containsKey(id))
			return reportTypeById.get(id);		
		//Not cached. retrieve it from database 
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_REPORT_TYPE_BY_ID);
	        psql.setInt(1,id);	        	       
	        rs = psql.executeQuery();
	        if(rs.next()) {
	        	ReportType reportType = new ReportType();
	        	reportType.setId(rs.getInt(1));
	        	reportType.setName(rs.getString(2));
	        	reportType.setOutputType(rs.getString(3));
	        	reportType.setXmlStylesheet(rs.getString(4));
	        	reportType.setAssociatedZul(rs.getString(5));
	        	reportType.setEnabled(rs.getBoolean(6));
	        	//Load cache of ReportTypes	        	
	        	reportTypeById.put(reportType.getId(), reportType);
	        	return reportType;
	        }
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}		
		return null;		
	}

}