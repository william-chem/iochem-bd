/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.Queries;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;

public class JCampDX extends Services {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(JCampDX.class.getName());
		
	private static final String XSLT_PATH 				= "html/xslt/cml2jcampdx.xsl";
	private static final String JCAMPDX_MIMETYPE 		= "chemical/x-jcamp-dx";
	private static final String JCAMPDX_FILENAME		= "cml2jcampdx.jdx";
	private static TransformerFactory tFactory 			= null; 
	private static Templates template					= null;
	
	static {
		tFactory	=  new net.sf.saxon.TransformerFactoryImpl();
	}
	
	public static void flushXsltTemplates(){
		template = null;		
	}
	
	public JCampDX(){
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection 		conn 		= null;	
		OutputStream    ostream 	= response.getOutputStream();
	   	PreparedStatement	psql 	= null;
    	ResultSet 			res	 	= null;
    	
    	if(template == null){		//First call to servlet will load conversion template
			StreamSource xslSource =  new StreamSource(new File(this.getServletContext().getRealPath("/") + XSLT_PATH));
			try {
				template = tFactory.newTemplates(xslSource);
			} catch (TransformerConfigurationException e) {
				
			}
		}
    	
		try	{
			conn =  DatabaseHandler.getCreateConnection();		
			// real execution:
			try{
					int calculationId = Integer.parseInt(request.getParameter("id"));
					String index = request.getParameter("index") == null ? "" : request.getParameter("index");
					
					if(isUserAllowedToAccessFile(calculationId)){
						String jcampFileContent = convertOutputToJCampDX(calculationId, index);								    
						response.setContentType(JCAMPDX_MIMETYPE);
				        response.setContentLength((int)jcampFileContent.length());
				        response.setHeader("Content-Disposition", "attachment; filename=\"" + JCAMPDX_FILENAME + "\"");
				        ostream.write(jcampFileContent.getBytes());							
					}else{
						response.sendError(HttpServletResponse.SC_FORBIDDEN);	
					}					
			}finally{			
				DatabaseHandler.closeStatementResult(psql, res);
			}			
		}catch (Exception e) {
			logger.error(e.getMessage() + getStackTrace(e));		
			returnSystemError(SERVER_ERROR,e,ostream);
		}finally {			
			try {		
				if (ostream != null) ostream.close();
				if (conn != null) conn.close();				
			} catch (Exception e) {
				logger.error(e.getMessage() + getStackTrace(e));
				returnSystemError(SERVER_ERROR,e,ostream);
			}
		}
	}
	
	private String convertOutputToJCampDX(int calculationId, String index) throws SQLException, IOException, SAXException, ParserConfigurationException{		
        try {
        	String realFilePath = CalculationService.getOutputFilePath(calculationId);
    		StreamSource xml = new StreamSource(new File(realFilePath));	    
    	    StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            Transformer transformer = template.newTransformer();
            transformer.setParameter("index", index);
            transformer.transform(xml, result);            
            return writer.toString();
        }
        catch (TransformerConfigurationException e)
        {
            return "";
        }
        catch (TransformerException e)
        {
            return "";        
        }
        catch (Exception e) {
        	return "";
        }
	}

	@Override
	public void executeService(Connection conn, OutputStream ostream,
			HashMap<String, String> params, String userName, Integer userId,
			PreparedStatement psql, ResultSet res) throws SQLException,
			IOException, BrowseCredentialsException {		
	}

}
