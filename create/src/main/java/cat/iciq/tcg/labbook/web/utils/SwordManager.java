/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swordapp.client.AuthCredentials;
import org.swordapp.client.Deposit;
import org.swordapp.client.DepositReceipt;
import org.swordapp.client.ProtocolViolationException;
import org.swordapp.client.SWORDClient;
import org.swordapp.client.SWORDClientException;
import org.swordapp.client.SWORDCollection;
import org.swordapp.client.ServiceDocument;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;


public class SwordManager {

	private static final Logger logger = LogManager.getLogger(SwordManager.class.getName());
	private static final int MAX_UPLOAD_RETRIES = 100;
	public static String DSPACEMETSSIP_PACKAGE_URL = "http://purl.org/net/sword/package/METSDSpaceSIP";
	private String swordBaseUrl = null;
	private SWORDClient swordClient = null;
	private AuthCredentials auth = null; 
	
	public SwordManager() throws BrowseCredentialsException
	{
		swordClient = new SWORDClient();
		swordBaseUrl = Main.getBaseUrl().concat(Constants.BROWSE_SWORD_SERVICEDOCUMENT_ENDPOINT);
		auth = buildCredentials();
	}
	
	private AuthCredentials buildCredentials() throws BrowseCredentialsException {
		String salt = CustomProperties.getProperty("module.communication.secret");
		String email = ShiroManager.getCurrent().getUserEmail();
		String timestamp = new SimpleDateFormat("yyyy/MM/dd 'at' hh:mm:ss z", Locale.getDefault()).format(new Date(System.currentTimeMillis()));		
		String secret =  email + "#" + timestamp;		
		String token = TokenManager.encode(salt, secret);			
		return new AuthCredentials(email, timestamp + "#" + token);		
	}
	
	
	public ServiceDocument getServiceDocument(String relativePath) throws SWORDClientException, ProtocolViolationException
	{
		return swordClient.getServiceDocument(swordBaseUrl.concat(relativePath), auth);
	}
	
	public String upload(SWORDCollection collection, String filePath) throws FileNotFoundException
	{				
		
		InputStream is = null;
		for(int inx = 0; inx < MAX_UPLOAD_RETRIES; inx++){
			try {								
				Thread.sleep(1000);					
				is= new FileInputStream(filePath);
				Deposit deposit = new Deposit();
				DepositReceipt receipt = null;
				deposit.setFile(is);
				deposit.setMimeType("application/zip");
				deposit.setFilename(new File(filePath).getName());
				deposit.setPackaging(DSPACEMETSSIP_PACKAGE_URL);
				deposit.setInProgress(false);
				receipt = swordClient.deposit(collection, deposit, auth);
				String handle = receipt.getSplashPageLink().getHref(); 
				Pattern handlePattern = Pattern.compile("..*\\/(([\\s\\S]+)\\/([\\s\\S]+))");			
				Matcher matcher = handlePattern.matcher(handle);
				if(matcher.matches())
					return matcher.group(1);
				else
					try{
						handle = handle.split("handle/")[1];
					}catch(Exception e){				
						return null;
					}
				return handle;
			} catch (Exception e) {	
				logger.error(e.getMessage());
				try {
					is.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}									
		}				
		return null;
	}
	
}
