/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.ui;

import java.net.MalformedURLException;
import java.net.URL;

import org.apache.shiro.cas.CasFilter;
import org.apache.shiro.cas.CasRealm;
import org.apache.shiro.cas.CasSubjectFactory;
import org.apache.shiro.realm.Realm;
import org.apache.shiro.util.Destroyable;
import org.apache.shiro.util.Initializable;
import org.apache.shiro.web.env.DefaultWebEnvironment;
import org.apache.shiro.web.filter.authc.AnonymousFilter;
import org.apache.shiro.web.filter.authc.LogoutFilter;
import org.apache.shiro.web.filter.authz.RolesAuthorizationFilter;
import org.apache.shiro.web.filter.mgt.DefaultFilterChainManager;
import org.apache.shiro.web.filter.mgt.FilterChainResolver;
import org.apache.shiro.web.filter.mgt.PathMatchingFilterChainResolver;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.session.mgt.ServletContainerSessionManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import cat.iciq.tcg.labbook.web.utils.CustomProperties;

@SuppressWarnings("deprecation")
public class ShiroWebEnvironment extends DefaultWebEnvironment implements Initializable, Destroyable{
	
	public static final String CREATE_CAS_ENDPOINT ="/shiro-cas";
	public static final String CAS_FAILURE_ENDPOINT ="/errorLogin.zul";	
    public static final String FILTER_CHAIN_RESOLVER_NAME = "filterChainResolver";
	
    private static final Logger log = LoggerFactory.getLogger(ShiroWebEnvironment.class);

	public void init() {
        configure();   
    }
		
    protected void configure() {
        this.objects.clear();

        WebSecurityManager securityManager = createWebSecurityManager();
        setWebSecurityManager(securityManager);

        FilterChainResolver resolver = createFilterChainResolver();
        if (resolver != null) {
            setFilterChainResolver(resolver);
        }
    }
 
    protected WebSecurityManager createWebSecurityManager() {
    	DefaultWebSecurityManager wsm =  new DefaultWebSecurityManager(buildCASRealm());
    	wsm.setSubjectFactory(new CasSubjectFactory());
    	wsm.setSessionManager(new ServletContainerSessionManager());
        return wsm;
    }
    
    private Realm buildCASRealm()  {
    	try {
    		CasRealm realm = new CasRealm();
    		realm.setDefaultRoles("ROLE_USER");
    		realm.setCasServerUrlPrefix(ShiroWebEnvironment.getCasServerUrlPrefix().toString());
    		realm.setCasService(ShiroWebEnvironment.getCasService().toString());
    		realm.setValidationProtocol("SAML");
    		return realm;			
    	}catch(Exception e) {
    		log.error("Error building CAS reaml, bad cas server enviroment properties" + e.getMessage());
    		return null;
    	}		
	}
    
    protected FilterChainResolver createFilterChainResolver() {
		CasFilter casFilter = buildCASFilter(); 		
		AnonymousFilter anonFilter = new AnonymousFilter();
		RolesAuthorizationFilter rolesFilter = buildRolesFilter();	
		
		LogoutFilter logoutFilter = new LogoutFilter();
		logoutFilter.setName("logout");
		logoutFilter.setRedirectUrl("/../browse/logout");
		
		DefaultFilterChainManager filterChainManager = new DefaultFilterChainManager();
		filterChainManager.addFilter("casFilter", casFilter);
		filterChainManager.addFilter("anon", anonFilter);
		filterChainManager.addFilter("roles", rolesFilter);
		filterChainManager.addFilter("logout", logoutFilter);
		
		filterChainManager.addToChain("/logout", "logout");
		filterChainManager.addToChain(CREATE_CAS_ENDPOINT, "casFilter");
		filterChainManager.addToChain(CAS_FAILURE_ENDPOINT, "anon");
		filterChainManager.addToChain("/timeout.zul", "anon");
		filterChainManager.addToChain("/zkau/**", "anon");
		filterChainManager.addToChain("/zul/errors/**", "anon");
		filterChainManager.addToChain("/index.zul", "roles", "ROLE_USER");
		filterChainManager.addToChain("/admintools/fileretrieval", "anon");
		filterChainManager.addToChain("/admintools/**", "roles", "ROLE_USER");	
		filterChainManager.addToChain("/zul/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/png/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/template/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/tmp/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/utils/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/userCommands/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/systemUserCommands/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/innerServices/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/html/css/**", "anon");
		filterChainManager.addToChain("/html/xslt/css/**", "anon");
		filterChainManager.addToChain("/html/xslt/js/**", "anon");
		filterChainManager.addToChain("/html/**", "roles", "ROLE_USER");
		filterChainManager.addToChain("/**", "anon");		
		
		PathMatchingFilterChainResolver chainResolver = new PathMatchingFilterChainResolver();
		chainResolver.setFilterChainManager(filterChainManager);
		return chainResolver;
    }
    
	private CasFilter buildCASFilter() {
		CasFilter casFilter = new CasFilter();
		casFilter.setFailureUrl(CAS_FAILURE_ENDPOINT);
		return casFilter;
	}
	
    private RolesAuthorizationFilter buildRolesFilter() {
    	RolesAuthorizationFilter rolesFilter = new RolesAuthorizationFilter(); 
		rolesFilter.setLoginUrl(getRolesFilterLoginUrl());
		rolesFilter.setUnauthorizedUrl("/timeout.zul");
		return rolesFilter;
	}
	
	private String getRolesFilterLoginUrl() {
		try {
			return getCasServerUrlPrefix().toString() 
							+ "/login?service=" 
							+ getCasService().toString();	
		}catch(MalformedURLException e) {
			log.error(e.getMessage());
			return null;
		}		
	}
	
	public static URL getCasServerUrlPrefix() throws MalformedURLException {
		String casHostname = CustomProperties.getProperty("cas.server.hostname");
		int casPort = CustomProperties.getProperty("cas.server.port").equals("")? 443 : Integer.parseInt(CustomProperties.getProperty("cas.server.port")); 
		String casServerApp = "/" + CustomProperties.getProperty("cas.server.app"); 
		return new URL("https",casHostname, casPort, casServerApp);
	}
	
	public static URL getCasService() throws MalformedURLException {
		String createHostname = CustomProperties.getProperty("create.server.hostname");
		int createPort = CustomProperties.getProperty("create.server.port").equals("")? 443 : Integer.parseInt(CustomProperties.getProperty("create.server.port"));
		String createCasServerApp = "/" + CustomProperties.getProperty("create.server.app") + CREATE_CAS_ENDPOINT; 
		return new URL("https", createHostname, createPort, createCasServerApp);
	}
}
