/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.manager.actions;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.web.definitions.Constants.ScreenSize;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;

/**
 * Abstract class that defines an action taken to a file or group of files.
 * Every action will be rendered in a zkoss tab component, all will be gathered 
 * inside a tabbox and then placed on Content Manager panel.
 * 
 * It's a requisite that we define tab id, tab label and tab internal order values,
 * it's necessary to allow rendering elements without error. So Content Manager class
 * will be in charge of this work. 
 * 
 * @author malvarez
 *
 */
public abstract class Action {

	protected static final Logger log = LogManager.getLogger(Action.class);
	
	protected EventQueue actionQueue = null;	//Communication with parent component				
	protected String tabID 		= "";			//Tab id inside zk
	protected String tabLabel 	= "";			//Tab text label
	protected int internalOrder = -1; 			//Tab order inside tabbox

	protected Tab tab 				= null;
	protected Tabpanel tabpanel 	= null;		
	protected Calculation calculation 	= null;
	protected HashMap<String,String> parameters 			= null;		//Parameters attached to an specific mimetype and jumbo_format
	protected HashMap<String,HashSet<String>> filesByFormat	= null;		//Mimetype - format => Files representation (dynamic part of this class)
	protected List<CalculationFile> calculationFiles		= null;       
	protected XMLFileManager metsFileManager				= null;	
	
	@SuppressWarnings("unchecked")
	public Action(){				
		parameters 		= new HashMap<String,String>();
		filesByFormat 	= new HashMap<String,HashSet<String>>();
		actionQueue		= EventQueues.lookup("dcmactions", EventQueues.DESKTOP, true);
		actionQueue.subscribe(new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {
				if(event.getName().equals("actionsMaximize")){
					boolean maximized = (Boolean)event.getData();
					resizeContent(maximized);
					
				}				
			}			
		});		
	}
	
	public void setTabID(String tabID) {
		this.tabID = tabID;
	}

	public String getTabID() {
		return tabID;
	}

	public void setTabLabel(String tabLabel) {
		this.tabLabel = tabLabel;
	}

	public String getTabLabel() {
		return tabLabel;
	}
	
	public int getInternalOrder() {
		return this.internalOrder;
	}
	
	public void setInternalOrder(int internalOrder){
		this.internalOrder = internalOrder;
	}
	
	public Tab getTab(){
		return this.tab;
	}
	
	public Tabpanel getTabpanel(){
		return this.tabpanel;
	}
	
	public Calculation getCalculation() {
		return calculation;
	}

	public void setCalculation(Calculation calculation) {
		this.calculation = calculation;
	}

	public void setParameters(String mimetype, String jumbo_format, String parameters) {
		this.parameters.put(mimetype + "$" + jumbo_format,parameters);		
	}

	public String getParameters(String mimetype, String jumbo_format) {
		return this.parameters.get(mimetype + "$" + jumbo_format);
	}
	
	public void addFile(String mimetype, String jumbo_format, String fileName){
		String key = mimetype + "$" + jumbo_format;
		if(filesByFormat.containsKey(key)){
			filesByFormat.get(key).add(fileName);
		}else{
			HashSet<String> fileNames = new HashSet<String>();
			fileNames.add(fileName);
			filesByFormat.put(key, fileNames);
		}		
	}
	
	public void setCalculationFiles(List<CalculationFile> calculationFiles){
		this.calculationFiles = calculationFiles;
	}
	
	public void setMetsFileManager(XMLFileManager metsFileManager){
		this.metsFileManager = metsFileManager;		
	}
	
	public XMLFileManager geMetsFileManager(){		
		return this.metsFileManager;
	}
	
	public abstract void init();
	public abstract void render();
	public abstract void resizeContent(boolean maximize);
	
	public void reset(){
		setVisible(false);
		filesByFormat.clear();		//Remove loaded files		
		resetZkControls();					//Clear current zk controls (specific for every Action subclass)
	}
	
	public void setVisible(boolean visible) {
		if(tab != null && tabpanel != null) {
			tab.setVisible(visible);
			tabpanel.setVisible(visible);
		}
	}
	
	abstract void resetZkControls();
	
	public boolean isEmpty(){
		return this.tab == null;
	}
	
	protected String findFileRealPathByName(String name) throws Exception{
		for(CalculationFile file : calculationFiles)	{
			String filePath = file.getFile();
			if(filePath.substring(filePath.lastIndexOf(File.separatorChar) + 1).equals(name))
				return FileService.getCreatePath() + File.separatorChar + filePath;
		}
		throw new IOException();		
	}
		
	protected boolean isSmallLayout() {
		ScreenSize layout = getCurrentLayout(); 
		return layout == ScreenSize.SMALL || layout == ScreenSize.X_SMALL;
	}
	
	protected ScreenSize getCurrentLayout() {
		ScreenSize layout = (ScreenSize) Executions.getCurrent().getDesktop().getAttribute("display");
		if(layout == null)
			return ScreenSize.X_LARGE;
		else
			return layout;
	}
	
}
