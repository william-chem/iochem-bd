/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;

import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;

public class ReportSupportingInformation extends ReportCustomBase {
	
	public static final long serialVersionUID = 1L; 
	
	public static final String AVAILABLE_ENERGIES_XSLT_TEMPLATE_PATH   = "/html/reports/ReportSupportingInformation/getAvailableEnergies.xsl";
	
	@Wire 
	Radiogroup energyTypeRdg;
	
	@Wire
	Radio potentialEnergyRad;
	
	@Wire
	Radio freeEnergyRad; 
	
	@Wire 
	Radio zeroPointEnergyRad;
	
	@Wire
	Radio enthalpyEnergyRad;
	
	@Wire 
	Radio bondingEnergyRad; 
	
	@Wire
	Radio nucRepulsionEnergyRad;
	
	@Wire
	Label unavailableEnergyLbl;
	
	@Wire
	Radiogroup energyUnitsRdg;
	
	@Wire 
	Radio energyUnitsKcalMolRad;
	
	@Wire
	Radio energyUnitsKJMolRad;
	
	@Wire
	Radio energyUnitsEVRad;
	
	@Wire
	Radio energyUnitsHartreeRad;
	
	@Listen("onCheck=#energyTypeRdg")
	public void onEnergyTypeRdgCheck() throws SAXException, IOException {
		saveConfiguration();
	}

	@Listen("onCheck=#energyUnitsRdg")
	public void onEnergyUnitsRdgCheck() throws SAXException, IOException{
		saveConfiguration();
	}
	
	@Override
	protected  void loadReport() throws SAXException, IOException, TransformerConfigurationException, BrowseCredentialsException, InterruptedException, ParserConfigurationException{				
		if(report.hasNoConfiguration())
			setDefaultConfiguration();
		loadConfiguration();
	}

	@Override
	protected void onCalculationsChanged() {
		potentialEnergyRad.setSelected(false);
		freeEnergyRad.setSelected(false);
		zeroPointEnergyRad.setSelected(false);
		enthalpyEnergyRad.setSelected(false);
		bondingEnergyRad.setSelected(false);
		nucRepulsionEnergyRad.setSelected(false);
		
		potentialEnergyRad.setVisible(false);
		freeEnergyRad.setVisible(false);
		zeroPointEnergyRad.setVisible(false);
		enthalpyEnergyRad.setVisible(false);
		bondingEnergyRad.setVisible(false);
		nucRepulsionEnergyRad.setVisible(false);
		
		try {
			enableAvailableEnergies();
			saveConfiguration();
		} catch (SAXException e) {		
			logger.error(e.getMessage());
		} catch (IOException e) {		
			logger.error(e.getMessage());
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}

	private void setDefaultConfiguration() throws SAXException, IOException{		
		String defaultConfiguration = 	"<configuration>" +
										"	<parameters>" +
										"		<energyUnits>" + CONFIG_PARAMETER_ENERGY_UNITS_EV + "</energyUnits>" +		
										"		<outputType>" + CONFIG_PARAMETER_OUTPUT_TYPE_PDF + "</outputType>" +    
										"	</parameters>" +
										"</configuration>";		
		report.setReportConfiguration(defaultConfiguration);		
	}
	
	protected void saveConfiguration() throws SAXException, IOException {
		parentSelector.addChanges();
		StringBuilder configuration = new StringBuilder();
		configuration.append("<configuration>" +
								"	<parameters>");
		
		if(energyTypeRdg.getSelectedItem()!= null)
			configuration.append("		<energyType>"  + energyTypeRdg.getSelectedItem().getValue()  + "</energyType>");
		if(energyUnitsRdg.getSelectedItem()!= null)
			configuration.append("		<energyUnits>" + energyUnitsRdg.getSelectedItem().getValue() + "</energyUnits>");
		if(fileFormatRgp.getSelectedItem()!= null)
			configuration.append("		<outputType>"  + fileFormatRgp.getSelectedItem().getValue()  + "</outputType>");    
								
		configuration.append("	</parameters>" +
								"</configuration>");
		report.setReportConfiguration(configuration.toString());
	}
	
	@SuppressWarnings("unused")
	private void loadConfiguration() throws TransformerConfigurationException, BrowseCredentialsException, InterruptedException, ParserConfigurationException {
		try {
			//Set energy type
			enableAvailableEnergies();
			NodeList energyType = report.queryConfiguration(QUERY_GET_ENERGY_TYPE);			
			if(energyType.item(0) != null){
				String energy =  energyType.item(0).getNodeValue();				
				if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL))
					potentialEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_FREE))
					freeEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT))
					zeroPointEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY))
					enthalpyEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_BONDING))
					bondingEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION))
					nucRepulsionEnergyRad.setSelected(true);
			}
			//Set energy units
			NodeList unitNodes = report.queryConfiguration(QUERY_GET_ENERGY_UNITS);
			String units = unitNodes.item(0).getNodeValue(); 
			if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_KCAL_MOL))
				energyUnitsKcalMolRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITs_KJ_MOL))
				energyUnitsKJMolRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_EV))
				energyUnitsEVRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_HARTREE))
				energyUnitsHartreeRad.setSelected(true);
			//Set output type
			NodeList outputType = report.queryConfiguration(QUERY_GET_OUTPUT_TYPE);
			String outType = outputType.item(0).getNodeValue();			
			for(Component child : fileFormatRgp.getChildren()){
				Radio typeRad = (Radio)child;
				if(typeRad.getValue().equals(outType)){
					typeRad.setSelected(true);
					break;
				}					
			}									
		} catch (SAXException e) {
			logger.error(e);			
		} catch (Exception e) {
			logger.error(e);
		}		
	}
	
	private void enableAvailableEnergies() throws Exception {
		ArrayList<String> calculationEnergies = new ArrayList<String>(); 
		Templates template = loadedXsltTemplates.get(AVAILABLE_ENERGIES_XSLT_TEMPLATE_PATH);
		Transformer transformer = template.newTransformer();
		//We'll query all calculations for it's containing energy types and enable energy types that exist in all report calculations		
		for(ReportCalculation calculation : report.getCalculations()){
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
       	    StringWriter writer = new StringWriter();
       	    StreamResult result = new StreamResult(writer);                  	    
       	    try{	              
       	    	transformer.transform(xml, result);
       	    	for(String existingEnergies : writer.getBuffer().toString().split("\\|")) //TODO:Code stylesheet and code real exisiting energies logic
       	    		calculationEnergies.add(existingEnergies);       	    	
       	    }catch (TransformerConfigurationException e){
       	    	logger.error(e.getMessage());
       	    	continue;
       	    }catch (TransformerException e){
       	    	logger.error(e.getMessage());
       	    	continue;               
       	    }  
		}
		int totalCalculations = report.getCalculations().size();
		int sumPotentialEnergyCalculations = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL);
		int sumFreeEnergyCalculations 	   = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_FREE);
		int sumZeroPointEnergyCalculations = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT);		
		int sumEnthalpyEnergyCalculations  = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY);
		int sumBondingEnergyCalculations   = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_BONDING);
		int sumNucRepEnergyCalculations    = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION);
			
		unavailableEnergyLbl.setVisible(totalCalculations == 0);
		potentialEnergyRad.setVisible(sumPotentialEnergyCalculations == totalCalculations && totalCalculations != 0);
		freeEnergyRad.setVisible(sumFreeEnergyCalculations == totalCalculations && totalCalculations != 0);
		zeroPointEnergyRad.setVisible(sumZeroPointEnergyCalculations == totalCalculations && totalCalculations != 0);
		enthalpyEnergyRad.setVisible(sumEnthalpyEnergyCalculations == totalCalculations && totalCalculations != 0);
		bondingEnergyRad.setVisible(sumBondingEnergyCalculations == totalCalculations && totalCalculations != 0);
		nucRepulsionEnergyRad.setVisible(sumNucRepEnergyCalculations == totalCalculations && totalCalculations != 0);
	}
	
	@Override
	protected void generateReport() throws Exception {
		//Check form validity
		if(!formFieldsAreValid())
			return;
		//Extract content from calculations and join them into a same root template element
		StringBuilder filteredCalculations = new StringBuilder();
		Transformer transformer = null;
				
		String templates[] = report.getType().getXmlStylesheet().split("\\|");
		Templates template = loadedXsltTemplates.get(templates[1]);
		
		filteredCalculations.append("<template id='reportData' xmlns='http://www.xml-cml.org/schema'>");
		for(ReportCalculation calculation : report.getCalculations()){
			transformer = template.newTransformer();
			transformer.setParameter("title", calculation.getTitle());
			if(energyTypeRdg.getSelectedItem() != null){
				transformer.setParameter("energyUnits", energyUnitsRdg.getSelectedItem().getValue());
				transformer.setParameter("energyType",  energyTypeRdg.getSelectedItem().getValue());
			}
				
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
       	    StringWriter writer = new StringWriter();
       	    StreamResult result = new StreamResult(writer);
       	    try {
				transformer.transform(xml, result);
				filteredCalculations.append(writer.getBuffer().toString());				
			} catch (TransformerException e) {
				logger.error(e.getMessage());
				continue;
			}
		}
		filteredCalculations.append("</template>");		

		//Build result .fo file
		String foOutput = "";
		template = loadedXsltTemplates.get(templates[2]);
		transformer = template.newTransformer();
		StreamSource xml = new StreamSource(new StringReader(filteredCalculations.toString()));	            	    
   	    StringWriter writer = new StringWriter();
   	    StreamResult result = new StreamResult(writer);
   	    try {
   	    	transformer.setParameter("reportTitle", report.getTitle());
			transformer.transform(xml, result);
			foOutput = writer.getBuffer().toString();				
		} catch (TransformerException e) {
			logger.error(e.getMessage());
		}   	       	    
   	    //Convert fo document to specific type and prompt user for it's download   	    
   	    convertFoDocumentToSpecificFormat(foOutput);   	    
	}
	

	
	private boolean formFieldsAreValid(){
		if(energyTypeRdg.getSelectedItem() != null && energyUnitsRdg.getSelectedItem() == null){
			Messagebox.show("Please select the energy units", "Warning", Messagebox.OK , Messagebox.EXCLAMATION);
			return false;
		}					
		if(fileFormatRgp.getSelectedItem() == null){
			Messagebox.show("Please select a file format", "Warning", Messagebox.OK , Messagebox.EXCLAMATION);
			return false;
		}
		return true;
	}
	
}
