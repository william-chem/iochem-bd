/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.data.RowDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;

/**
 * Servlet implementation class CPro
 */
public class ViewCalc extends Services {
	
	private static final long serialVersionUID = 1L;
       
    /**
     * @see Services#Services()
     */
    public ViewCalc() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String,String> params,String userName,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
    	String path = Paths.isAbsolute(params.get("newPath")) ? 
				params.get("newPath") : 
				Paths.getFullPath(params.get("path"), params.get("newPath"));
    	String fullView = params.get("full");
	
    	Calculation c = CalculationService.getByPath(path);
    	if(c == null) {
    		returnKO(ostream,CommandsMessages.CALC_PATH_ERROR.TITLE(),CommandsMessages.CALC_PATH_ERROR.MESSAGE());
    		return;
    	} else if(!PermissionService.hasPermissionOnCalculation(c.getId(), Permissions.READ)){
    		returnKO(ostream,CommandsMessages.CALC_READ_ERROR.TITLE(),CommandsMessages.CALC_READ_ERROR.MESSAGE());
    		return;
    	}
    	
    	Project p = ProjectService.getByPath(c.getParentPath());
    	
    	RowDH data = new RowDH();
    	data.setElement(c.getName());
    	data.setElement(c.getDescription());
    	data.setElement(p.getPermissions());
    	data.setElement(ShiroManager.getCurrent().getUserNameById(p.getOwner()));
    	data.setElement(ShiroManager.getCurrent().getGroupNameById(p.getGroup()));
    	data.setElement(c.getType().getAbbreviation());
    	data.setElement(c.getPath());
    	data.setElement(p.getConceptGroup());
    	data.setElement(c.getCreationDate() + "");
    	data.setElement("");
    	data.setElement(String.valueOf(c.isPublished()));
    	data.setElement(c.getHandle());
    	data.setElement(c.getPublished_name() + "");
    	data.setElement(c.getPublicationDate() + "");
    	
    	
    	if (fullView != null) {
    		List<CalculationFile> files = CalculationService.getCalculationFiles(c.getId());
    		for(CalculationFile f : files) {
    			data.setHiddenData("assetstore" + File.separatorChar + c.getId()+ File.separatorChar + f.getName());
    		}    		
    	}    	
		sendObj(data, ostream);	
    }

}
