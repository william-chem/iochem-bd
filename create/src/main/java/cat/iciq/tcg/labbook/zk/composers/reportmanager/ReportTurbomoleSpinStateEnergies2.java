/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;

import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;

public class ReportTurbomoleSpinStateEnergies2 extends ReportCustomBase {

	private static final long serialVersionUID = 1L;

	@Wire
	Radiogroup energyUnitsRdg;
	
	@Wire 
	Radio energyUnitsCm1;
	
	@Wire 
	Radio energyUnitsKcalMolRad;
	
	@Wire
	Radio energyUnitsKJMolRad;
	
	@Wire
	Radio energyUnitsEVRad;
	
	@Wire
	Radio energyUnitsHartreeRad;
	
	@Listen("onCheck=#energyUnitsRdg")
	public void onEnergyUnitsRdgCheck() throws SAXException, IOException{
		saveConfiguration();
	}
	
	@Override
	protected void onCalculationsChanged() {
		//Do nothing
	}

	@Override
	protected void loadReport() throws Exception {
		if(report.hasNoConfiguration())
			setDefaultConfiguration();
		loadConfiguration();		
	}

	private void setDefaultConfiguration() throws SAXException, IOException{		
		String defaultConfiguration = 	"<configuration>" +
										"	<parameters>" +
										"		<energyUnits>" + CONFIG_PARAMETER_ENERGY_UNITS_CM_1 + "</energyUnits>" + 
										"		<outputType>" + CONFIG_PARAMETER_OUTPUT_TYPE_PDF + "</outputType>" +    
										"	</parameters>" +
										"</configuration>";		
		report.setReportConfiguration(defaultConfiguration);		
	}
	
	@SuppressWarnings("unused")
	private void loadConfiguration() throws TransformerConfigurationException, BrowseCredentialsException, InterruptedException, ParserConfigurationException {
		try {
			//Set energy units
			NodeList unitNodes = report.queryConfiguration(QUERY_GET_ENERGY_UNITS);
			String units = unitNodes.item(0).getNodeValue();
			if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_CM_1))
				energyUnitsCm1.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_KCAL_MOL))
				energyUnitsKcalMolRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITs_KJ_MOL))
				energyUnitsKJMolRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_EV))
				energyUnitsEVRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_HARTREE))
				energyUnitsHartreeRad.setSelected(true);
			//Set output type
			NodeList outputType = report.queryConfiguration(QUERY_GET_OUTPUT_TYPE);
			String outType = outputType.item(0).getNodeValue();			
			for(Component child : fileFormatRgp.getChildren()){
				Radio typeRad = (Radio)child;
				if(typeRad.getValue().equals(outType)){
					typeRad.setSelected(true);
					break;
				}					
			}									
		} catch (SAXException e) {
			logger.error(e);			
		} catch (IOException e) {
			logger.error(e);
		}		
	}
	
	@Override
	protected void generateReport() throws Exception {
		//Extract content from calculations and join them into a same root template element
		StringBuilder filteredCalculations = new StringBuilder();
		Transformer transformer = null;
				
		String templates[] = report.getType().getXmlStylesheet().split("\\|");
		Templates template = loadedXsltTemplates.get(templates[0]);
		
		filteredCalculations.append("<template id='reportData' xmlns='http://www.xml-cml.org/schema'>");
		for(ReportCalculation calculation : report.getCalculations()){
			transformer = template.newTransformer();			
			transformer.setParameter("title", calculation.getTitle());
			transformer.setParameter("energyUnits", energyUnitsRdg.getSelectedItem().getValue());
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
       	    StringWriter writer = new StringWriter();
       	    StreamResult result = new StreamResult(writer);
       	    try {
				transformer.transform(xml, result);
				filteredCalculations.append(writer.getBuffer().toString());				
			} catch (TransformerException e) {
				logger.error(e.getMessage());
				continue;
			}
		}
		filteredCalculations.append("</template>");		

		//Build result .fo file
		String foOutput = "";
		template = loadedXsltTemplates.get(templates[1]);
		transformer = template.newTransformer();
		StreamSource xml = new StreamSource(new StringReader(filteredCalculations.toString()));	            	    
   	    StringWriter writer = new StringWriter();
   	    StreamResult result = new StreamResult(writer);
   	    try {
   	    	transformer.setParameter("reportTitle", report.getTitle());
   	    	transformer.setParameter("reportDescription", report.getDescription());
			transformer.transform(xml, result);
			foOutput = writer.getBuffer().toString();				
		} catch (TransformerException e) {
			logger.error(e.getMessage());
		}   	       	    
   	    //Convert fo document to specific type and prompt user for it's download   	    
   	    convertFoDocumentToSpecificFormat(foOutput);   	    

		
	}

	@Override
	protected void saveConfiguration() throws SAXException, IOException {
		parentSelector.addChanges();
		StringBuilder configuration = new StringBuilder();
		configuration.append("<configuration>" +
								"	<parameters>");		
		if(energyUnitsRdg.getSelectedItem()!= null)
			configuration.append("		<energyUnits>" + energyUnitsRdg.getSelectedItem().getValue() + "</energyUnits>");
		if(fileFormatRgp.getSelectedItem()!= null)
			configuration.append("		<outputType>"  + fileFormatRgp.getSelectedItem().getValue()  + "</outputType>");    
								
		configuration.append("	</parameters>" +
								"</configuration>");
		report.setReportConfiguration(configuration.toString());
		
	}
	
	

}
	