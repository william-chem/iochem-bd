/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.main.navigation;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zul.A;
import org.zkoss.zul.Image;
import org.zkoss.zul.Tree;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.Treecell;
import org.zkoss.zul.Treeitem;
import org.zkoss.zul.TreeitemRenderer;
import org.zkoss.zul.Treerow;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.PublicableEntity;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class TreeItemNavigationRenderer implements TreeitemRenderer<TreeNode<Entity>> {
	
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm"); 
	
	private boolean isTactileDevice = false;
	
	public TreeItemNavigationRenderer(boolean isTactileDevice){
		this.isTactileDevice = isTactileDevice;
	}
	
	@Override
	public void render(Treeitem item, TreeNode<Entity> node, int index) throws Exception {
		PublicableEntity element = (PublicableEntity)node.getData();
		Treerow row = new Treerow();			
		Treecell name = new Treecell(element.getName());		
		Treecell type = new Treecell(getType(element));
		Treecell desc = new Treecell(element.getDescription());
		Treecell owner = new Treecell();
		Treecell group = new Treecell();
		Treecell permissions = new Treecell();
		Treecell creationdate = new Treecell(DATE_FORMAT.format(element.getCreationDate()));
		Treecell conceptgroup = new Treecell();
		Treecell state = new Treecell();
		Treecell handle = buildHandleTreeCell(element);
		Treecell published = buildPublishedTreeCell(element);
		Treecell editable = buildEditableTreeCell(element);
		Treecell order = new Treecell(String.valueOf(element.getElementOrder()));
		Treecell id = new Treecell(String.valueOf(element.getId()));
				
		row.appendChild(name);
		row.appendChild(type);		
		row.appendChild(desc);
		row.appendChild(owner);		
		row.appendChild(group);
		row.appendChild(permissions);
		row.appendChild(creationdate);
		row.appendChild(conceptgroup);
		row.appendChild(state);
		row.appendChild(handle);		
		row.appendChild(published);
		row.appendChild(editable);
		row.appendChild(order);
		row.appendChild(id);		
		
		item.appendChild(row);
		item.setAttribute("entity", element);
        if(!type.getLabel().equals("PRO"))
        	item.setTooltip("thumbnailPopup, position=after_center, delay=700");

        if(isTactileDevice){ //Functionality disabled on tactile devices
        	item.setDraggable("false");
        	item.setDroppable("false");
        }else{
        	//First time we render the element we will setup its drag and drop properties and event listeners
        	if(!item.getEventListeners("onDrop").iterator().hasNext()) {
        		if(element.isPublished())
            		item.setDraggable("false");
            	else         		
            		item.setDraggable(element.isProject()? "project" : "calculation");
        		
        		item.setDroppable(element.isProject()? "project, calculation" : "calculation");
        		
        		
        		item.addEventListener("onDrop", new EventListener<Event>(){
        			@Override
        			public void onEvent(Event arg0) throws Exception {					
        				DropEvent drop = (DropEvent)arg0;
        				Treeitem sourceItem = (Treeitem)drop.getDragged();
        				Treeitem destinationItem = (Treeitem)drop.getTarget();					 
        				moveElement(sourceItem, destinationItem);									
        			}				
        		});        	
        	}    		
        }
	}
	
	private String getType(PublicableEntity entity) {
		if(entity instanceof Project)
			return "PRO";
		else if(entity instanceof Calculation) 
			return ((Calculation)entity).getType().getAbbreviation();
		return "";
	}
	
	private Treecell buildHandleTreeCell(PublicableEntity element){
		Treecell handleCell = new Treecell();
		if(!element.isPublished())
			return handleCell;
	    A a = new A();
	    String handleHref = Main.getHandleBaseUrl() + "/" + element.getHandle();
	    a.setHref(handleHref);
	    a.setLabel(element.getHandle());
	    a.setTarget("_blank");
	    handleCell.getChildren().add(a);
	    return handleCell;
	}
	
	private Treecell buildPublishedTreeCell(PublicableEntity element){
		Treecell publishedCell = new Treecell();
		if(element.isPublished()){			
			Image imgPublished = new Image("../images/published.png");       
        	publishedCell.getChildren().add(imgPublished);
		}
        return publishedCell;
	}
	
	private Treecell buildEditableTreeCell(PublicableEntity element){
		Treecell editableCell = new Treecell();		
		if(element.isProject()) {
			Project project = (Project)element;
	        if(project.isPublished()){
				A a = new A();
				String editHref = Main.getBrowseBaseUrl() + Constants.BROWSE_HANDLE_EDIT_ENDPOINT + "/" + project.getHandle() + "/" + project.getEditHash();
				a.setHref(editHref);			
				a.setTarget("_blank");
				a.setImage("../images/editable.png");
				a.setTooltiptext("Edit published content");
				editableCell.getChildren().add(a); 
	        }			
		}
		return editableCell;		
	}
	
	private void moveElement(Treeitem sourceItem, Treeitem destinationTreeitem) {
		Entity destination = (Entity) destinationTreeitem.getAttribute("entity");
		if(destination.isProject())
			moveToProject(sourceItem, destinationTreeitem);
		else
			moveToCalculation(sourceItem, destinationTreeitem);	
	}
	
	private void moveToProject(Treeitem sourceItem, Treeitem destinationItem){
		HashMap<String, Object> parameters = new HashMap<String, Object>();	
		Entity dc = (Entity) destinationItem.getAttribute("entity");
		
		Tree tree = (Tree)destinationItem.query("tree#tree");
		Set<Object> sourceItems = ((CustomTreeModel)tree.getModel()).getSelection();		
		if(sourceItem.equals(destinationItem)){
			return;
		}else if(sourceItems.size() > 1){		//Coming from multiple selection
			parameters.put("sourceItems", sourceItems);			
		}else{									//Coming from single selection (or a drag and drop)
			HashSet<Object> singleSourceItem = new HashSet<Object>();
			singleSourceItem.add(sourceItem.getAttribute("entity"));
			parameters.put("sourceItems", singleSourceItem);
		}		
		parameters.put("destinationItem", dc);		
		EventQueue<Event> queue = EventQueues.lookup("navigation", EventQueues.DESKTOP, true);
		queue.publish(new Event("movetoproject", null, parameters));
	}
	 
	public void moveToCalculation(Treeitem sourceTreeitem, Treeitem destinationTreeitem){
		HashMap<String, Object> parameters = new HashMap<String, Object>();	
		Entity destination = (Entity) destinationTreeitem.getAttribute("entity");
		Tree tree = (Tree)destinationTreeitem.query("tree#tree");
		Set<Object> sourceItems = ((CustomTreeModel)tree.getModel()).getSelection();		
		if(sourceTreeitem.equals(destinationTreeitem))
			return;
		if(sourceItems.size() > 1)		//Coming from multiple selection, discard it
			return;					
		
		Entity source = (Entity)sourceTreeitem.getAttribute("entity");
		if(source.isProject())				//Can't drop a project onto a calculation
			return;
		parameters.put("sourceItems", source);
		parameters.put("destinationItem", destination);		
		EventQueue<Event> queue = EventQueues.lookup("navigation", EventQueues.DESKTOP, true);
		queue.publish(new Event("movetocalculation", null, parameters));			
	}
	 
}
