/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.util.HashMap;

import javax.net.ssl.SSLContext;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.services.FileService;

public class RestManager {
	private static final Logger log = LogManager.getLogger(RestManager.class.getName());

	private static PoolingHttpClientConnectionManager connManager;
	private static RequestConfig config;
	private static ConnectionKeepAliveStrategy myStrategy;
	
	private static final String KEYSTORE_FILE_LOCAL_PATH = "ssl/keystore"; 
	private static final String KEYSTORE_PASSWORD = "changeit";
	private static final String KEYSTORE_ALIAS = "iochem-bd";

	//Code adapted from http://stackoverflow.com/questions/30070745/closeablehttpclient-execute-freezes-once-every-few-weeks-despite-timeouts
    static {    	
    	try{
    	    int readTimeout = 10;
    	    int connectionTimeout = 10;
    	    int connectionFetchTimeout =10;    	 
    	    int poolSize = 200;
    	    int routeSize = 50;

    	    //HTTP protocol connection socket factory
    	    PlainConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
    	    //HTTPS protocol connection socket factory    	    
    	    String keystorePath = FileService.getBasePath(File.separatorChar + KEYSTORE_FILE_LOCAL_PATH);
    		FileInputStream is = new FileInputStream(keystorePath);
    	    
    		KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
    		keystore.load(is, KEYSTORE_PASSWORD.toCharArray());
    	    is.close();
    	   
    	    SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(keystore, new TrustSelfSignedStrategy()).build();
    	    SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext,NoopHostnameVerifier.INSTANCE);
    	    Registry<ConnectionSocketFactory> r = RegistryBuilder.<ConnectionSocketFactory>create()
    	            .register("http", plainsf)
    	            .register("https", sslsf)
    	            .build();
    	    connManager = new PoolingHttpClientConnectionManager(r);
    	    // Connection pool size and number of routes to cache
    	    connManager.setMaxTotal(poolSize);
    	    connManager.setDefaultMaxPerRoute(routeSize);    	      
    	    // ConnectTimeout : time to establish connection with GSA
    	    // ConnectionRequestTimeout : time to get connection from pool
    	    // SocketTimeout : waiting for packets form GSA
    	    config = RequestConfig.custom()
    	    		.setConnectTimeout(connectionTimeout * 1000)
    	    		.setConnectionRequestTimeout(connectionFetchTimeout * 1000)
    	    		.setSocketTimeout(readTimeout * 1000).build();

    		// Keep alive for 5 seconds if server does not have keep alive header
    		myStrategy = new ConnectionKeepAliveStrategy() {
    		@Override
    		public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
    			HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
    			while (it.hasNext()) {
    				HeaderElement he = it.nextElement();
    		        String param = he.getName();
    		        String value = he.getValue();
    		        if (value != null && param.equalsIgnoreCase("timeout")) {
    		        	return Long.parseLong(value) * 1000;
    		        }
    		    }
    		    return 5 * 1000;
    			}
    		  };	    		
    	}catch(Exception e){
    		connManager = null;
    		myStrategy = null;
    		config = null;
    		log.error("Unable to setup ClosableHttpClient configuration. " + e.getMessage());
    	}    	
	}     

	public static CloseableHttpClient buildAcceptAllHttpClient() throws Exception {
		if(connManager == null || myStrategy == null || config == null)
			throw new Exception("Unable to setup ClosableHttpClient configuration");
	    return HttpClients.custom().setDefaultRequestConfig(config).setKeepAliveStrategy(myStrategy).setConnectionManager(connManager).build();
	}
	
	public static XMLFileManager getRequestAsXml(CloseableHttpClient client, String url, HashMap<String, String> headers) throws ClientProtocolException, IOException, SAXException, ParserConfigurationException{
		XMLFileManager obj = null;
		HttpGet httpGet = new HttpGet(url);		
		httpGet.setHeader("Accept", "application/xml");
		if(headers != null)
			for(String key : headers.keySet())
				httpGet.setHeader(key, headers.get(key));		
		CloseableHttpResponse response = client.execute(httpGet);		
		try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :
							if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);
								String responseString = new String( baos.toByteArray(), StandardCharsets.UTF_8);
								obj = new XMLFileManager(null,null, responseString);	
							}							
							break;
				default  : 	
							String result = EntityUtils.toString(entity);
							log.error(result);								
			}
		    EntityUtils.consume(entity);
		} finally {						
		    response.close();
		}				
		return obj;				
	} 
	
	public static String getRequestAsText(CloseableHttpClient client, String url, HashMap<String, String> headers) throws ParseException, Exception{
		HttpGet httpGet = new HttpGet(url);		
		httpGet.setHeader("Accept", "text/plain");
		if(headers != null)
			for(String key : headers.keySet())
				httpGet.setHeader(key, headers.get(key));		
		CloseableHttpResponse response = client.execute(httpGet);		
		try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :  if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);								
								return new String( baos.toByteArray(), StandardCharsets.UTF_8); 	
							}else
								return "";							
				default  : 	throw new Exception(EntityUtils.toString(entity));																				
			}
		} finally {
			EntityUtils.consume(response.getEntity());
		    response.close();
		}		
	} 
	
	public static String postRequest(CloseableHttpClient client, String url, HttpEntity postEntity, HashMap<String, String> headers) throws ParseException, Exception{
		HttpPost httpPost = new HttpPost(url);
		if(headers != null)
			for(String key : headers.keySet())
				httpPost.setHeader(key, headers.get(key));
		httpPost.setEntity(postEntity);
		CloseableHttpResponse response = client.execute(httpPost);
	    try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :  if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);								
								return new String( baos.toByteArray(), StandardCharsets.UTF_8); 	
							}else
								return "";							
				default  : 	throw new Exception(EntityUtils.toString(entity));																				
			}
		} finally {
			EntityUtils.consume(response.getEntity());
		    response.close();
		}	  
	}
	
	public static String putRequest(CloseableHttpClient client, String url, HttpEntity putEntity, HashMap<String, String> headers) throws ParseException, Exception{
        HttpPut httpPut = new HttpPut(url);
        if(headers != null)
            for(String key : headers.keySet())
                httpPut.setHeader(key, headers.get(key));
        httpPut.setEntity(putEntity);
        CloseableHttpResponse response = client.execute(httpPut);
        try {
            HttpEntity entity = response.getEntity();           
            switch(response.getStatusLine().getStatusCode()){
                case 200 :  if (entity != null) {
                                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                                entity.writeTo(baos);                               
                                return new String( baos.toByteArray(), StandardCharsets.UTF_8);     
                            }else
                                return "";                          
                default  :  throw new Exception(EntityUtils.toString(entity));                                                                              
            }
        } finally {
            EntityUtils.consume(response.getEntity());
            response.close();
        }     
    }
	
	
	public static Certificate getCurrentCertificate() {
		FileInputStream is = null;;
		try {
			String keystorePath = FileService.getBasePath(File.separatorChar + KEYSTORE_FILE_LOCAL_PATH);			
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
	        is = new FileInputStream(new File(keystorePath));	        
	        keystore.load(is, KEYSTORE_PASSWORD.toCharArray());	        
	        Certificate cert = keystore.getCertificate(KEYSTORE_ALIAS);
	        return cert;
		} catch (Exception e) {
			log.error(e.getMessage());			
		} finally{
			try {
				is.close();
			} catch (Exception e) {}
		}
        return null;
	}	
}
