/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

public class ReportType {
	private int id;
	private String name;
	private String xmlStylesheet;
	private String outputType;
	private String associatedZul;
	private boolean enabled;
	
	public ReportType(){
		
	}
	
	public ReportType(int id, String name, String xmlStylesheet, String outputType, String associatedZul, boolean enabled){
		this.id = id;
		this.name = name;
		this.xmlStylesheet = xmlStylesheet;
		this.outputType = outputType;
		this.associatedZul = associatedZul;
		this.enabled = enabled;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getXmlStylesheet() {
		return xmlStylesheet;
	}
	public void setXmlStylesheet(String xmlStylesheet) {
		this.xmlStylesheet = xmlStylesheet;
	}
	public String getOutputType() {
		return outputType;
	}
	public void setOutputType(String outputType) {
		this.outputType = outputType;
	}
	public String getAssociatedZul() {
		return associatedZul;
	}
	public void setAssociatedZul(String associatedZul) {
		this.associatedZul = associatedZul;
	}
	public boolean isEnabled() {
		return enabled;
	}
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public ReportType clone(){
		return new ReportType(this.id, this.name, this.xmlStylesheet, this.outputType, this.associatedZul, this.enabled);
	}
}
