/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.CalculationTypeFile;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.web.definitions.Queries;

public class CalculationTypeFileService {

	private static final Logger log = LogManager.getLogger(CalculationTypeFileService.class);
	
	private static HashMap<Integer, CalculationTypeFile> calculationTypeFileById = null;
	private static HashMap<Integer, List<CalculationTypeFile>> calculationTypeFileByCalculationTypeId = null;

	static {
		loadCalculationTypeFiles();
	}
	
	public static List<CalculationTypeFile> getByCalculationTypeId(int calculationTypeId) {
		//Using lazy load, CalculationTypes must be loaded (static init) before loading this dependent table
		if(calculationTypeFileById  == null || calculationTypeFileByCalculationTypeId == null) {
			loadCalculationTypeFiles();
		}
		return calculationTypeFileByCalculationTypeId.get(calculationTypeId);		
	}
 	
	public static void loadCalculationTypeFiles() {		
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		try {
			calculationTypeFileById = new HashMap<Integer, CalculationTypeFile>();
			calculationTypeFileByCalculationTypeId = new HashMap<Integer, List<CalculationTypeFile>> ();
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_CALCULATION_TYPE_FILES);	        	        	      
	        rs = psql.executeQuery();	        
	        while(rs.next()) {
	        	CalculationTypeFile file = new CalculationTypeFile();
	        	file.setId(rs.getInt("id"));
	        	file.setCalculationTypeId(rs.getInt("calculation_type_id"));
	        	file.setDefaultTypeSelection(rs.getBoolean("default_type_sel"));
	        	file.setAbbreviation(rs.getString("abbreviation"));
	        	file.setFileName(rs.getString("file_name"));
	        	file.setUrl(rs.getString("url"));
	        	file.setDescription(rs.getString("description"));
	        	file.setJumboConverterClass(rs.getString("jumbo_converter_class"));
	        	file.setJumboConverterInType(rs.getString("jumbo_converter_in_type"));
	        	file.setJumboConverterOutType(rs.getString("jumbo_converter_out_type"));
	        	file.setMimetype(rs.getString("mimetype"));
	        	file.setUse(rs.getString("use"));
	        	file.setLabel(rs.getString("label"));
	        	file.setBehaviour(rs.getString("behaviour"));
	        	calculationTypeFileById.put(file.getId(), file);

	        	if(!calculationTypeFileByCalculationTypeId.containsKey(file.getCalculationTypeId())) 
	        		calculationTypeFileByCalculationTypeId.put(file.getCalculationTypeId(), new ArrayList<CalculationTypeFile>());
	        	calculationTypeFileByCalculationTypeId.get(file.getCalculationTypeId()).add(file);
	        }			
		}catch(Exception e) {
			log.error("Error loading CalculationTypeFiles values.");
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}
	}
}
