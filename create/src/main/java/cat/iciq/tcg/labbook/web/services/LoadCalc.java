/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.services.UploadService;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.web.core.MultiPartServices;
import cat.iciq.tcg.labbook.web.core.Services;

@MultipartConfig()
public class LoadCalc extends MultiPartServices {
	
	private static final long   serialVersionUID 	= 1L;
	private static final Logger logger = LogManager.getLogger(LoadCalc.class.getName());
    
    @Override
    public void executeService( Connection conn, OutputStream ostream, HashMap<String,String> params,String userName,
			Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException {
    }
    
    @Override
    public void executeService(HttpServletRequest request , HttpServletResponse response, HashMap<String,String> params) {
    	OutputStream ostream = null;
		try {
			ostream = response.getOutputStream();
			if(request != null) {
				ostream.write(UploadService.loadCalculationsViaShell(request, params).getBytes());	
			}else{
				throw new Exception("This should never be called");
			}						
		} catch (Exception e) {
			logger.error(e.getMessage());
			try {
				Services.returnKO(ostream, CommandsMessages.TITLE_MESSAGE, e.getMessage());
			} catch (IOException e1){}
		}
    }    
}
