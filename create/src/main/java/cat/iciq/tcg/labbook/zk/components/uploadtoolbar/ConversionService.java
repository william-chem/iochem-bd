/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.uploadtoolbar;

import java.io.File;
import java.sql.Connection;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;

import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.UploadService;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.extraction.CalExtractionJumbo;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.web.services.GetCalTypes;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;

public class ConversionService  {
	
	private static final Logger logger = LogManager.getLogger(ConversionService.class.getName());  
			
	private static ExecutorService conversionExecutorService = null;				
	private static ExecutorCompletionService<Map<String,String>> conversionCompletionService = null;
	private static EventQueue<Event>  conversionEventQueue = null;
	private static IDataHolder calTypes = null; 
	   
	public static void init() {		
		conversionExecutorService = buildExecutorService();		
		conversionEventQueue = initConversionEventQueue();
		conversionCompletionService = new ExecutorCompletionService<>(conversionExecutorService, new ConversionQueue());		
	}	
	
	private static ExecutorService buildExecutorService(){				
		int numberOfProcessors = Runtime.getRuntime().availableProcessors();	
		if(numberOfProcessors > 1)
			numberOfProcessors = numberOfProcessors - 1;		
		return Executors.newFixedThreadPool(numberOfProcessors);					
	}
	
	private static EventQueue<Event> initConversionEventQueue(){		
		return EventQueues.lookup("calculationloading",	WebApps.getCurrent(), true);	//Asynchronous event queue		
	}

	private static IDataHolder retrieveCalculationTypes(){
		try {
			Connection conn = DatabaseHandler.getCreateConnection();
			GetCalTypes getCalcTypes = new GetCalTypes();
			getCalcTypes.executeService(conn, null, null, null, null, null, null);
			return getCalcTypes.getData();
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
		return null;
	}
	
	public static void convertFiles(Map<String, String> params) throws Exception{
		if(calTypes == null)
			calTypes = retrieveCalculationTypes();		
		String templateHostname = CustomProperties.getProperty("jumbo.template.base.url");
		String institutionName  = CustomProperties.getProperty("mets.institution.name");	
		String dir = params.get(Constants.LOAD_CALC_TMP_DIR);		
		String processUUID = buildRandomUUID();		
		
		params.put(Constants.LOCAL_APP_DIR, FileService.getCreatePath());
		params.put(Constants.LOCAL_DIR, dir + File.separatorChar + CalExtractionJumbo.EXTRACTION_FOLDER);
		params.put(CalculationInsertion.PARAM_UUID,processUUID);
		ConcurrentHashMap<String, String> files = getFilesToProcess(params);
		
		CalExtractionJumbo calExt = new CalExtractionJumbo((MatrixDH) calTypes,files,dir, params);
		calExt.setEnvironmentProperties(templateHostname, institutionName);			
		try{
			conversionEventQueue.publish(new Event("filesQueued",null, params));
			conversionCompletionService.submit(calExt);		
			logger.info("submitted calculation");
		}catch(Exception e){
			logger.error(e.getMessage() + Services.getStackTrace(e));
		}				
	}
	
	private static ConcurrentHashMap<String, String> getFilesToProcess(Map<String,String> params) 
	{
		ConcurrentHashMap<String, String> files = new ConcurrentHashMap<>();
		for(String key: params.keySet())
			if(key.startsWith("-"))
				files.put(key, params.get(key));
		return files;					
	}
	
	public static void insertConvertedFilesIntoDatabase(Map<String, String > params){		
		try {
			UploadService.insertCalculations(params);				
		} catch (Exception e) {			
			params.put(CalculationInsertion.PARAM_EXCEPTION_TITLE,  "title");
			params.put(CalculationInsertion.PARAM_EXCEPTION_MESSAGE,  e.getMessage());
			conversionEventQueue.publish(new Event("conversionFailed",null, params));			
			logger.error(e.getMessage());
		}
	}
	
	public static void closeConversionQueue(){
		conversionExecutorService.shutdown(); // Disable new tasks from being submitted
		try {
			if (!conversionExecutorService.awaitTermination(20, TimeUnit.SECONDS)) {
				conversionExecutorService.shutdownNow(); // Cancel currently executing tasks
				if (!conversionExecutorService.awaitTermination(20, TimeUnit.SECONDS))
					logger.error("loadCalculationPool did not terminate in defined time frame");
			}
		}catch (InterruptedException ie) {
			conversionExecutorService.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}	
	
	private static String buildRandomUUID(){		
		return UUID.randomUUID().toString();						
	}
}
