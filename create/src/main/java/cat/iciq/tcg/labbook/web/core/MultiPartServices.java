/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.core;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.web.definitions.Constants;

public abstract class MultiPartServices extends Services {
	
	private static final Logger logger = LogManager.getLogger(MultiPartServices.class.getClass());
       
    public MultiPartServices() {
        super();
    }

    @Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
		try{
			HashMap<String,String> params = buildParameterMap(request);
			params.put(Constants.LOCAL_APP_DIR, FileService.getCreatePath() + File.separatorChar);
			this.executeService(request, response, params);
		}catch(Exception e){
			OutputStream ostream = response.getOutputStream();
    		returnSystemError(SERVER_ERROR,e,ostream);			
		}	
	}

	public abstract void executeService(HttpServletRequest request, HttpServletResponse response, HashMap<String,String> params);
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		this.doGet(request,response);
	}
	
}
