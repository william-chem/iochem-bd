/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.postgresql.ds.PGPoolingDataSource;

import cat.iciq.tcg.labbook.web.utils.CustomProperties;

public class DatabaseHandler {
	public static final Logger logger = LogManager.getLogger(DatabaseHandler.class.getName());
	
	public static final String DDBB = "java:/comp/env/jdbc/create";	
	public static final String NO_CONTEXT = "No context on server";
	public static final String NO_DDBB = "Server cannot locate database";	
	
	public static InitialContext cxt = null;
	
	static{
		try {
			cxt = new InitialContext();
			cxt.addToEnvironment(DDBB, setupDatasource());			
		} catch (NamingException e) {
			logger.error(e.getMessage());
		}
	}
	
	public static void deregisterDriver() throws NamingException {
		try {
		    java.sql.Driver postgresqlDriver = DriverManager.getDriver(buildUrl());
		    DriverManager.deregisterDriver(postgresqlDriver);
		} catch (SQLException ex) {
		    logger.info("Could not deregister driver:".concat(ex.getMessage()));
		}
	}
	
	public static Connection getCreateConnection() throws Exception {	
		Connection conn = null;
		DataSource ds = (DataSource) cxt.getEnvironment().get(DDBB);
		if ( ds == null ) 
			throw new SQLException(NO_DDBB);		
		conn =  ds.getConnection();
		return conn;		
	}	

	private static DataSource setupDatasource() {
		PGPoolingDataSource source = new PGPoolingDataSource();
		source.setDataSourceName("ioChem-BD Create data source");		
		source.setUrl(buildUrl());				
		source.setUser(CustomProperties.getProperty("create.database.user"));
		source.setPassword(CustomProperties.getProperty("create.database.pwd"));
		source.setMaxConnections(0);
		return source;
	}

	private static String buildUrl() {
		StringBuilder url = new StringBuilder();
		url.append("jdbc:postgresql://");
		url.append(CustomProperties.getProperty("create.database.host").trim());
		url.append(":");
		url.append(CustomProperties.getProperty("create.database.port").trim());
		url.append("/");
		url.append(CustomProperties.getProperty("create.database.name").trim());
		
		String extras = CustomProperties.getProperty("database.extra.parameters");
	    if(extras != null && !extras.isEmpty())
	    	url.append(extras);		
	    return url.toString();
	}

	public static void resetAutocommit(Connection conn){
		try{
			if (conn.getAutoCommit() == false) 
				conn.setAutoCommit(true);	
		}catch(Exception e){
			logger.error(e.getMessage());
		}		
	}
	
	public static void closeStatementResult(PreparedStatement psql, ResultSet res){		
		try{
			if (res  != null) 
				res.close();  res  = null; 	
		}catch(SQLException e){
			logger.error(e.getMessage());
		}		
		try{
			if (psql != null)
				psql.close(); psql = null;  	
		}catch(SQLException e){
			logger.error(e.getMessage());
		}
	}
	
	public static void closeConnection(Connection conn){
		try {
			if(conn != null)
				conn.close();
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
	}
	
	
}
