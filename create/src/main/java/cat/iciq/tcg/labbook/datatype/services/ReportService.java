/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.web.definitions.Queries;

public class ReportService {

	private static final Logger log = LogManager.getLogger(ReportService.class);

	public static List<Report> getUserReports(int userId) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		List<Report> reports = new ArrayList<Report>();
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_USER_REPORTS);
			psql.setInt(1, userId);
			rs = psql.executeQuery();
			while (rs.next()) {
				Report report = new Report();
				report.setId(rs.getInt(1));
	        	report.setName(rs.getString(2));
	        	report.setTitle(rs.getString(3));
	        	report.setDescription(rs.getString(4));
	        	report.setCreationDate(rs.getDate(5));
	        	report.setReportConfiguration(rs.getString(6));
	        	report.setType(ReportTypeService.getReportTypeById(rs.getInt(7)));
	        	report.setPublished(rs.getBoolean(8));
	        	reports.add(report);
	        }
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}	
		return reports;       
	}
	
	public static Report getReport(int id) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		Report report = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_REPORT_BY_ID);
			psql.setInt(1, id);
			rs = psql.executeQuery();
			if(rs.next()) {
				report = new Report();
				report.setId(rs.getInt(1));
				report.setName(rs.getString(2));
				report.setTitle(rs.getString(3));
				report.setDescription(rs.getString(4));
				report.setCreationDate(rs.getDate(5));
				report.setOwnerId(rs.getInt(6));
				report.setReportConfiguration(rs.getString(7));
				report.setPublished(rs.getBoolean(8));
				report.setType(ReportTypeService.getReportTypeById(rs.getInt(9)));
				report.setCalculations(getReportCalculations(conn, id));
			}									
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}	
		return report;
	}
	
	private static LinkedList<ReportCalculation> getReportCalculations(Connection conn, int id) throws Exception {		
		PreparedStatement psql = null;
		ResultSet rs = null;
		LinkedList<ReportCalculation> calculations = new LinkedList<>();
		try {
			psql = conn.prepareStatement(Queries.GET_REPORT_CALCULATIONS);
			psql.setInt(1, id);
			rs = psql.executeQuery();
			while(rs.next()) {
				ReportCalculation reportCalculation = new ReportCalculation();
				reportCalculation.setId(rs.getInt(1));
				reportCalculation.setCalcOrder(rs.getInt(2));
				reportCalculation.setTitle(rs.getString(3));
				reportCalculation.setCalcId(rs.getInt(4));
				reportCalculation.setCalcPath(rs.getString(5));
				reportCalculation.setReportId(rs.getInt(6));
				calculations.add(reportCalculation);				
			}
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);			
		}	
		return calculations;
	}	
	
	public static void deleteReport(int id) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.DELETE_REPORT_CALCULATIONS);
			psql.setInt(1, id);
			psql.executeUpdate();
			psql.close();
			psql = conn.prepareStatement(Queries.DELETE_REPORT);
			psql.setInt(1, id);
			psql.executeUpdate();			
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}
	}	
	
	public static void deleteReportCalculations(int reportId) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.DELETE_REPORT_CALCULATIONS);
			psql.setInt(1, reportId);
			psql.executeUpdate();						
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}		
	}
	
	public static void saveReport(Report report) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.UPDATE_REPORT);
			psql.setString(1, report.getName());
			psql.setString(2, report.getTitle());
			psql.setString(3,  report.getDescription());
			psql.setString(4, report.getReportConfiguration().toString());
			psql.setBoolean(5,  report.isPublished());
			psql.setInt(6, report.getId());			
			psql.executeUpdate();			
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}	
	}
	
	
	public static void saveReportCalculation(ReportCalculation reportCalculation) throws Exception {				
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.CREATE_REPORT_CALCULATION);
			psql.setInt(1, reportCalculation.getReportId());
			psql.setInt(2, reportCalculation.getCalcOrder());
			psql.setString(3, reportCalculation.getTitle());
			psql.setInt(4, reportCalculation.getCalcId());		
			rs = psql.executeQuery();
			rs.next();
			int id = rs.getInt(1);
			reportCalculation.setId(id);
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}
	}

	
	public static int createReport(int reportType, int ownerId) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			int id = getNextReportId(conn);
			psql = conn.prepareStatement(Queries.CREATE_REPORT);
			psql.setInt(1, id);
			psql.setDate(2, new java.sql.Date(System.currentTimeMillis()));
			psql.setInt(3, ownerId);
			psql.setString(4, "");
			psql.setString(5, "");
			psql.setString(6, "");
			psql.setString(7, null);
			psql.setInt(8, reportType);
			psql.setBoolean(9, false);		
			psql.executeUpdate();
			return id;
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}	
	}
	
	
	private synchronized static int getNextReportId(Connection conn) throws Exception {
		PreparedStatement psql = null;
		ResultSet res = null;		
		try {			
			psql = conn.prepareStatement(Queries.GET_REPORT_NEXT_ID);
			res = psql.executeQuery();
			res.next();
			return res.getInt(1);				
		}catch(Exception e) {			
			throw e;
		} finally{
			DatabaseHandler.closeStatementResult(psql, res);			
		}		
	}	
}
