/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedList;

import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cat.iciq.tcg.labbook.zk.composers.reportmanager.ReportConfiguration;


public class Report {
	private int id;
	private String name;
	private String title;
	private String description;
	private Date creationDate;
	private int ownerId;
	private boolean published;
	
	private ReportType type;
	
	private LinkedList<ReportCalculation> calculations;
	private ReportConfiguration configuration;
	
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	
	public int getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(int ownerId) {
		this.ownerId = ownerId;
	}

	public ReportType getType() {
		return type;
	}

	public void setType(ReportType type) {
		this.type = type;
	}

	public boolean isPublished() {
		return published;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public void setReportConfiguration(String xml) throws SAXException, IOException{
		this.configuration = new ReportConfiguration(xml);		
	}
	
	public ReportConfiguration getReportConfiguration(){
		return this.configuration;
	}
	
	public NodeList queryConfiguration(String xpath) throws SAXException, IOException{
		return this.configuration.query(xpath);
	}
	
	public LinkedList<ReportCalculation> getCalculations() {
		return calculations;
	}
	
	public ReportCalculation getCalculationByCalculationId(int calculationId){
		for(ReportCalculation reportCalculation : this.getCalculations())
			if(reportCalculation.getId() == calculationId)
				return reportCalculation;			
		return null;
	}
	
	public void setCalculations(LinkedList<ReportCalculation> calculations) {
		this.calculations = calculations;
	}	
	
	public int findNewReportCalculationIndex(){
		if(calculations == null || calculations.size() == 0)		//New ReportCalculations will have negative (incremental) values as id 
			return -1;
		ArrayList<Integer> ids = new ArrayList<Integer>();
		for(ReportCalculation calculation : calculations)
			ids.add(calculation.getId());
		return Collections.min(ids) > 0 ? -1 : Collections.min(ids) - 1;
	}
	
	public boolean containsCalculationPath(String path){
		if(calculations == null || calculations.size() == 0)		//New ReportCalculations will have negative (incremental) values as id 
			return false;
		for(ReportCalculation calculation : calculations)
			if(calculation.getCalcPath().equals(path))
				return true;
		return false;
	}
		
	public void reorderCalculations(){
		Collections.sort(calculations, new Comparator<ReportCalculation>(){
			@Override
			public int compare(ReportCalculation o1, ReportCalculation o2) {
				return o1.getCalcOrder() - o2.getCalcOrder();
			}			
		});
		
	}	
	
	public boolean hasNoConfiguration() {
		return configuration.toString() == null;
	}
	
}
