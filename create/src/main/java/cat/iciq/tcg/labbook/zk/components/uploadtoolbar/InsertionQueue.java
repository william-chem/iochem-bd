/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.uploadtoolbar;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;

import cat.iciq.tcg.labbook.shell.exceptions.ExtractionException;

public class InsertionQueue extends LinkedBlockingQueue<Future<Map<String, String>>>{
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(InsertionQueue.class.getName());
	private static EventQueue<Event> conversionEventQueue = null;
	
	static{
		conversionEventQueue = EventQueues.lookup("calculationloading",	WebApps.getCurrent(), true);
	}
	
	@Override
	public boolean add(Future<Map<String, String>> o) {
	    boolean succeeded = super.add(o);
	    if(succeeded)
			try {
		    	notifyListeners(o.get());				
			} catch (InterruptedException e) {				
				logger.error(e.getMessage());
				return true;
			} catch (Exception e) {					
				logger.error(e.getMessage());
				if(e.getCause() instanceof ExtractionException){
					ExtractionException ext = (ExtractionException)e.getCause();
					HashMap<String,String> params = new HashMap<>();
					params.put(CalculationInsertion.PARAM_USERNAME, ext.getUser());
					params.put(CalculationInsertion.PARAM_UUID,  ext.getThreadUUID());
					params.put(CalculationInsertion.PARAM_EXCEPTION_TITLE,  ext.getTitle());
					params.put(CalculationInsertion.PARAM_EXCEPTION_MESSAGE,  ext.getMessage());
					conversionEventQueue.publish(new Event("conversionFailed", null, params));
				}
				return true;
			}
	    return succeeded;
	}

	private void notifyListeners(Map<String, String> params){
		conversionEventQueue.publish(new Event("filesSaved",null, params));		
	}
	
}
