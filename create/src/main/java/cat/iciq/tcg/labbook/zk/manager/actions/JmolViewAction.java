/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.manager.actions;

import java.util.List;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Div;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Listbox;
import org.zkoss.zul.Listitem;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Window;

import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.services.Molden;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.CalculationInsertion;

/**
 * This action class is used to load calculation files into JSmol html5 viewer  
 * @author malvarez
 */
public class JmolViewAction extends Action {
	
	private static final String JSMOL_IFRAME_URL = "../html/xslt/jsmol/jsmol.html";
	private static final String JSMOL_WINDOW_URL = "main/actions/jsmol.zul";
	
	private Window jsmolWindow = null;
	private Iframe jsmolSmallIframe = null;
	private Listbox geometryFileSelector = null;
	private Listbox orbitalFileSelector = null;
	private Listbox orbitalNumberSelector = null; 	

	private enum DisplayMode { FULL, SINGLE, MINIMAL};
	
	@Override
	public void init(){
		this.setInternalOrder(1);
		this.setTabID		("jmolViewActionTab");
		this.setTabLabel	("3D Structure");		
		initTab();
		initIframe();		//Small screen size
		initTabpanel();		//Large screen size		
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initTab(){
		tab = new Tab();
		tab.setId(this.tabID);
		tab.setLabel(this.tabLabel);		
		//Maximize functionality			
		tab.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {		
				actionQueue.publish(new Event("maximizeRequested",null,null));						
			}			
		});				
	}

	private void initTabpanel(){
		tabpanel = new Tabpanel();
		Div div = new Div();
		div.setHflex("1");
		div.setVflex("1");				
		jsmolWindow = (Window) Executions.createComponents(JSMOL_WINDOW_URL, null, null);		
		div.appendChild(jsmolWindow);
		div.appendChild(jsmolSmallIframe);
		tabpanel.appendChild(div);
		addEventListeners();
	}

	private void initIframe() {
		jsmolSmallIframe  = new Iframe("");
		jsmolSmallIframe.setVflex("1");
		jsmolSmallIframe.setHflex("1");
		jsmolSmallIframe.setSrc(JSMOL_IFRAME_URL);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void addEventListeners() {
	    geometryFileSelector = ((Listbox)jsmolWindow.query("#geometryFileSelector"));	    
		orbitalFileSelector = (Listbox)jsmolWindow.query("#orbitalFileSelector");
		orbitalNumberSelector = (Listbox)jsmolWindow.query("#orbitalNumberSelector");

		geometryFileSelector.addEventListener(Events.ON_SELECT, new EventListener() {
            @Override
            public void onEvent(Event event) throws Exception {
                Listitem item = geometryFileSelector.getSelectedItem();
                if(item != null){
                    String calcId = item.getValue();
                    String filename = (String) item.getAttribute("filename");
                    loadGeometryFile(calcId, filename, filename.equals(CalculationInsertion.GEOMETRY_FILE_NAME));
                }
            }
        });

		orbitalFileSelector.addEventListener(Events.ON_SELECT, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				Listitem item = orbitalFileSelector.getSelectedItem();
				if(item != null){
					int calcId = Integer.parseInt(item.getValue());
					String filename = item.getLabel();
					loadOrbitalNumbers(calcId, filename);
				}
			}
		});
		orbitalNumberSelector.addEventListener(Events.ON_SELECT, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				int calcId = Integer.parseInt(orbitalFileSelector.getSelectedItem().getValue());
				String filename = orbitalFileSelector.getSelectedItem().getLabel();								
				int index = (Integer)orbitalNumberSelector.getSelectedItem().getValue();				
				if(index != 0)				
					Clients.evalJavaScript("loadMoldenOrbital(" + calcId + ",'" + filename + "', " + index +");");			
			}
		});
	}	
	
	@Override
	public void render() {
		if(filesByFormat.size() == 0)
			clearMolecule();
		else		
			displayMolecule();
		tab.setSelected(true);
	}	

	private void clearMolecule() {
		Clients.evalJavaScript("clearMolecule()");
	}
	
	private void displayMolecule() {		
		String calcId = this.metsFileManager.getStringFromQuery(XpathQueries.GET_CALCULATION_INTERNAL_ID);		
		if(calcId == null) {
			clearMolecule();
			return;
		}						
	    	    
	    setVisible(true);
	    switch (getDisplayMode()) { 
	        case MINIMAL:  jsmolSmallIframe.setClientDataAttribute("calcId", calcId);
                           jsmolSmallIframe.setClientDataAttribute("file", CalculationInsertion.GEOMETRY_FILE_NAME);
                           jsmolWindow.setVisible(false);
                           jsmolSmallIframe.setVisible(true);
                           break;

	        case SINGLE:   jsmolSmallIframe.setVisible(false);
                           jsmolWindow.setVisible(true);
                           displaySingleMolecule(calcId);
                           break;
	            
	        case FULL:     jsmolSmallIframe.setVisible(false);
                           jsmolWindow.setVisible(true);
                           displayFullMolecule(calcId);
	                       break;
	    }
	}

	private DisplayMode getDisplayMode() {
        if(isSmallLayout()) {
            return DisplayMode.MINIMAL;
        }else if(!isDesktop()) {
            return DisplayMode.SINGLE;
        }else {
            return DisplayMode.FULL;
        }
    }   

	private void displaySingleMolecule (String calcId) {	    	    
	    ((Tabs)jsmolWindow.query("#moleculeViewTabs")).setVisible(false);
	    loadGeometryFile(calcId, CalculationInsertion.GEOMETRY_FILE_NAME);
    }
	
	private void displayFullMolecule(String calcId) {
	    // Show visualisation tabs only if there exist alternative content to display
	    ((Tabs)jsmolWindow.query("#moleculeViewTabs")).setVisible(hasOrbitalFiles());
	    jsmolWindow.setVisible(true);		
		Tab geometryTab = (Tab)jsmolWindow.query("#geometryTab");
		geometryTab.setSelected(true);
		
		Clients.evalJavaScript("clearMoldenOrbital();");
		loadGeometryFile(calcId, CalculationInsertion.GEOMETRY_FILE_NAME);
		
		loadGeometryFiles(calcId);
		
		
		if(hasOrbitalFiles()) {		    			
		    loadOrbitalFiles(calcId);
		}		
	}

    private void loadGeometryFile(String calcId, String filename) {
        Clients.evalJavaScript("loadMolecule(" + calcId + ",'" + filename + "');");
    }

	private void loadGeometryFile(String calcId, String filename, boolean isXml) {
	    Clients.evalJavaScript("loadMolecule(" + calcId + ",'" + filename + "', " + isXml + ");");
	}	
	
	private void loadOrbitalFiles(String calcId) {	    	    
        orbitalFileSelector.clearSelection();        
        //Clear previous molden file values
        while(orbitalFileSelector.getFirstChild() != null) 
            orbitalFileSelector.getFirstChild().detach();       
        while(orbitalNumberSelector.getFirstChild() != null) 
            orbitalNumberSelector.getFirstChild().detach();	    
	    
		NodeList nodes = this.metsFileManager.getItemIteratorQuery(XpathQueries.GET_ORBITAL_FILE_ELEMENTS);		
		for(int inx = 0; inx < nodes.getLength();inx++){
			Element element = (Element)nodes.item(inx);
			String fileName = element.getAttribute("xlink:href");
			Listitem listItem = new Listitem();
			listItem.setLabel(fileName);
			listItem.setValue(calcId);
			orbitalFileSelector.appendChild(listItem);
		}
	}

	private void loadOrbitalNumbers(int calcId, String filename) throws Exception {
		int inx = 0;
		//Clear previous orbital numbers 
		while(orbitalNumberSelector.getFirstChild() != null) 
			orbitalNumberSelector.getFirstChild().detach();					
		//Add first dummy entry		
		List<String> orbitalHeaders = Molden.buildOrbitalHeadersList(calcId, filename);
		Listitem listItem = new Listitem();
		listItem.setLabel("-");
		listItem.setValue(inx++);
		orbitalNumberSelector.appendChild(listItem);
		//Then the other valid ones
		for(String header : orbitalHeaders) {
			listItem = new Listitem();
			listItem.setLabel(header);
			listItem.setValue(inx++);
			orbitalNumberSelector.appendChild(listItem);
		}					
	}

	private boolean hasOrbitalFiles(){
		String hasOrbitals = this.metsFileManager.getStringFromQuery(XpathQueries.HAS_MOLECULAR_ORBITALS_FILE);
		return Boolean.valueOf(hasOrbitals);
	}

    private boolean hasGeometryFiles(){
        String hasOrbitals = this.metsFileManager.getStringFromQuery(XpathQueries.HAS_ADDITIONAL_GEOMETRIES_FILE);
        return Boolean.valueOf(hasOrbitals);
    }
   
    private void loadGeometryFiles(String calcId) {
        Div geometrySelectDiv = (Div)jsmolWindow.query("#geometrySelect");
        if(hasGeometryFiles()) {
            geometrySelectDiv.setVisible(true);
            geometryFileSelector.clearSelection();        
            //Clear previous molden file values
            while(geometryFileSelector.getFirstChild() != null) 
                geometryFileSelector.getFirstChild().detach();       
               
            Listitem listItem = new Listitem();
            
            listItem.setLabel("geometry.cml");
            listItem.setValue(calcId);
            listItem.setAttribute("filename", CalculationInsertion.GEOMETRY_FILE_NAME);
            geometryFileSelector.appendChild(listItem);
            
            NodeList nodes = this.metsFileManager.getItemIteratorQuery(XpathQueries.GET_GEOMETRY_FILE_ELEMENTS);     
            for(int inx = 0; inx < nodes.getLength();inx++){
                Element element = (Element)nodes.item(inx);
                String fileName = element.getAttribute("xlink:href");
                listItem = new Listitem();
                listItem.setLabel(fileName + " (slower)");
                listItem.setValue(calcId);
                listItem.setAttribute("filename", fileName);
                geometryFileSelector.appendChild(listItem);
            }                
        }else {
            geometrySelectDiv.setVisible(false);
        }        
    }
   

	private boolean isDesktop(){
		return Executions.getCurrent().getSession().getAttribute("displayDevice").equals("desktop");
	}
		
	@Override
	public void resizeContent(boolean maximize) {
		
	}

	@Override
	void resetZkControls() {
		clearMolecule();
	}
}