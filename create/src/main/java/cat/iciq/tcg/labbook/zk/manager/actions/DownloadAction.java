/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.manager.actions;

import java.io.File;
import java.util.Optional;
import java.util.regex.Pattern;

import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Div;
import org.zkoss.zul.Filedownload;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.zk.ui.DeferredContentDaemon;


/**
 * This action class allows us to download to client computer a single file from a calculation entity  
 * @author malvarez
 */
public class DownloadAction extends Action {

	public static final String NOT_DOWNLOADABLE_USES_REGEX = "(input|output)";
	private static final Pattern p = Pattern.compile(NOT_DOWNLOADABLE_USES_REGEX);
	private static final Long THRESHOLD = (Long)WebApps.getCurrent().getAttribute("deferredContentThreshold");	

	@SuppressWarnings("unchecked")
	@Override
	public void init() {		
		this.setInternalOrder(3);
		this.setTabID("downloadActionTab");
		this.setTabLabel("Download");		
		
		tab = new Tab();
		tab.setId(this.tabID);
		tab.setLabel(this.tabLabel);
		//Maximize functionality			
		tab.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {		
				actionQueue.publish(new Event("maximizeRequested",null,null));						
			}			
		});
		tabpanel = new Tabpanel();		
				
	}

	@Override
	public void render() {
		if(filesByFormat.size() == 0)
			return;		
	
		boolean isSmallLayout = isSmallLayout();
		
		
		Grid grid = new Grid();
		Rows rows = new Rows();
				
		Columns columns 		= new Columns();
		Column columnButton 	= new Column("");
		Column columnDownload   = new Column("");
		Column columnName		= new Column("File name");		
		columnButton.setWidth("15%");
		columnDownload.setWidth("5%");
		columnName.setWidth("80%");
		columns.appendChild(columnButton);
		columns.appendChild(columnDownload);
		columns.appendChild(columnName);

		if(!isSmallLayout) {
			Column columnMimetype	= new Column("Mimetype");
			Column columnSize		= new Column("Size (kB)");
			columnButton.setWidth("15%");
			columnName.setWidth("30%");
			columnMimetype.setWidth("30%");
			columnSize.setWidth("20%");
			columns.appendChild(columnMimetype);
			columns.appendChild(columnSize);
		}
		
		grid.appendChild(columns);
		grid.appendChild(rows);
		grid.setSclass("downloadGrid");
						
		tabpanel.appendChild(grid);
		for(String key : filesByFormat.keySet()){
			for(String fileName : filesByFormat.get(key)){
				Button downloadButton = new Button();
				downloadButton.setLabel("Download");
				downloadButton.setSclass("btn btn-sm btn-secondary btn-block mb-2 mt-2");
				try{
					String filePath = findFileRealPathByName(fileName);
					String fileSize 	= metsFileManager.getSingleAttributeValueQuery(XpathQueries.GET_FILESIZE_FROM_FILENAME.replace("?", fileName));
					String fileMimeType = metsFileManager.getSingleAttributeValueQuery(XpathQueries.GET_MIMETYPE_FROM_FILENAME.replace("?", fileName));
					Row row = new Row();
					row.setSclass("downloadFileName");

					Div loading = new Div();					
					if(THRESHOLD == -1L || new File(filePath).length() < THRESHOLD)
					    downloadButton.addEventListener(Events.ON_CLICK, new DownloadFileListener(filePath));
					else {
					    Optional<CalculationFile> optCalcFile = calculationFiles.stream().filter(calculation -> calculation.getName().equals(fileName)).findFirst();
					    if(optCalcFile.isPresent()) {					        
                            CalculationFile calculationFile = optCalcFile.get();
                            
                            loading.setWidth("100%");  
                            loading.setSclass("text-center");
                            loading.setTooltiptext("Retrieving large files from tape storage can take some time.\nYou will be notified when they are available, you can continue browsing meanwhile.");                            
                            Div loadIcon = new Div();
                            loadIcon.setId("download-spin-" + calculationFile.getId());
                            loadIcon.setSclass("mx-auto");
                            loading.appendChild(loadIcon);                          					        
	                        downloadButton.addEventListener(Events.ON_CLICK, new DownloadDeferredFileListener(filePath, fileName, calculation.getId(), calculationFile.getId(), loadIcon));
					    }
					}

					Label fileNameLabel	= new Label(fileName);
					fileNameLabel.setTooltiptext(fileName);
					row.appendChild(downloadButton);
                    row.appendChild(loading);
					row.appendChild(fileNameLabel);
					if(!isSmallLayout) {
						Label fileMimeTypeLabel	= new Label(fileMimeType);
						Label fileSizeLabel		= new Label(String.format("%1$,.2f",(Float.valueOf(fileSize) / 1024)));
						row.appendChild(fileMimeTypeLabel);
						row.appendChild(fileSizeLabel);
					}
					rows.appendChild(row);
				}catch(Exception e){
				    
				}
			}
		}

		tab.setSelected(false);
		setVisible(true);
	}
	
	@Override
	public void resizeContent(boolean maximize) {	
		
	}
	
	@Override
	void resetZkControls() {
		while(!tabpanel.getChildren().isEmpty())
			tabpanel.getFirstChild().detach();
	}
}


class DownloadFileListener implements EventListener<Event> {
	
	private String file;
	public DownloadFileListener(String file)
	{
		this.file = file;
	}
	
	@Override
	public void onEvent(Event event) 
	{
		try {
			Filedownload.save(new File(file), null);
		} catch (Exception e) {
     		Messagebox.show("The file you're trying to download doesn't exist.", "File error", Messagebox.OK, Messagebox.INFORMATION);
		}	
	}
}

class DownloadDeferredFileListener implements EventListener<Event> {
    private String file;
    private String name;
    private int calculationId;
    private int calculationFileId;
    private Div loading;
    
    public DownloadDeferredFileListener(String file, String name, int calculationId, int calculationFileId, Div loading) {
        this.file = file;
        this.name = name;
        this.calculationId = calculationId;
        this.calculationFileId = calculationFileId;
        this.loading = loading;
    }
    
    @Override
    public void onEvent(Event event) throws Exception {
        if(DeferredContentDaemon.isUnderMaintenance()) {
            Messagebox.show("The file retrieval service is currently under maintenance. Please try again later.", "File service maintenance", Messagebox.OK, Messagebox.INFORMATION);
        }else if(DeferredContentDaemon.isAccessible(calculationFileId)) {
            try {
                loading.setSclass("mx-auto");
                Filedownload.save(new File(file), null);
            } catch (Exception e) {
                Messagebox.show("The file you're trying to download doesn't exist.", "File error", Messagebox.OK, Messagebox.INFORMATION);
            }
        }else {
            if(loading.getSclass().contains("spin")) {
                Messagebox.show("The file you're trying to download is being retrieved.", "File retrieval in process", Messagebox.OK, Messagebox.INFORMATION);
            }else {               
                try {
                    DeferredContentDaemon.requestAccess(ShiroManager.getCurrent().getUserName(), calculationId, calculationFileId, name, file);
                    loading.setSclass("spin mx-auto");
                    Messagebox.show("Retrieving large files from its storage can take some time.\nYou will be notified when they are available, you can continue browsing meanwhile.", "File retrieval started", Messagebox.OK, Messagebox.INFORMATION);
                }catch(Exception e) {
                    Messagebox.show("The file retrieval service had an internal error. Please notify system administrator.", "File service error", Messagebox.OK, Messagebox.INFORMATION);       
                }
               
            }
        }
    }
}
