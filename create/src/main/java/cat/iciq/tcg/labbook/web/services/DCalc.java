/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.MultiPartServices;

public class DCalc extends MultiPartServices {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(DCalc.class.getName()); 
  
    public DCalc() {
        super();
    }

    @Override
    public void executeService( Connection conn, OutputStream ostream,HashMap<String,String> params, String userName, Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
    }

    @Override
    public void executeService(HttpServletRequest request, HttpServletResponse response, HashMap<String,String> params) {
    	String path = Paths.isAbsolute(params.get("newPath")) ?
				params.get("newPath") :
				Paths.getFullPath(params.get("path"), params.get("newPath"));
    	OutputStream ostream = null;
    	
		try {		
	    	ostream = response.getOutputStream();
			Calculation calculation = CalculationService.getByPath(path);
			if(calculation != null) 
				CalculationService.deleteCalculation(calculation.getId(), true);
			returnOK(ostream);
		}catch(Exception e) {
			try {
				returnKO(ostream, CommandsMessages.CALC_ERROR.TITLE(), e.getMessage());
			} catch (IOException e1) {			
			}
		}
		
    }
}
