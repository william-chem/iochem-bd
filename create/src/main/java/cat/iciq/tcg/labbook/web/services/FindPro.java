/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Vector;

import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.Queries;

/**
 * Servlet implementation class CPro
 */
public class FindPro extends Services {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see Services#Services()
     */
    public FindPro() {
        super();
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String,String> params,String userName,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException {
    	try	{			
			StringBuffer 	sql 	= new StringBuffer("");
			Vector<String> 	sqlVar	= new Vector<String>();
			sql.append(Queries.START_FIND_PRO.replaceAll("@USER_GROUPS", ShiroManager.getCurrent().getUserGroupsSQL()));
			
			if (params.get("nameSearch") != null)  // name search
			{
				sql.append(Queries.NAME_SEARCH);
				sqlVar.add(params.get("nameSearch"));
			}
			
			if (params.get("pathSearch") != null)  // path search
			{
				sql.append(Queries.PATH_SEARCH);
				sqlVar.add(params.get("pathSearch"));
			}
			else // else is added the current path as default value.
			{
				sql.append(Queries.PATH_SEARCH);
				sqlVar.add("^"+params.get("path"));
			}
			
			if (params.get("descSearch") != null)  // description search
			{
				sql.append(Queries.DESC_SEARCH);
				sqlVar.add(params.get("descSearch"));
			}
			
			if (sqlVar.size()>0) // if there is some kind of search
			{
				int pos = 1;				
				psql = conn.prepareStatement(sql.toString());
				psql.setInt(pos++, userId);							
				for (String var:sqlVar)				
					psql.setString(pos++, var);
				
				res = psql.executeQuery();
				MatrixDH data = new MatrixDH();
				// the list for the moment is the path and the name of the project that matches the find pattern.
				Vector<String> nameFound			= new Vector<String>();
				Vector<String> pathFound			= new Vector<String>();
				while (res.next())
				{
					nameFound.add		(res.getString(1));
					pathFound.add		(res.getString(2));
				}
				data.setStringColumn(0, nameFound		);
				data.setStringColumn(1, pathFound		);
				sendObj(data, ostream);
			
			}
			else		// no results for this search. Just returning an OK
			{
				returnOK(ostream);
			}			
    	}catch(Exception e) {
    		returnKO(ostream,CommandsMessages.PROJECT_ERROR.TITLE(),CommandsMessages.PROJECT_ERROR_READ_PERM.MESSAGE());
    	}finally
    	{
    		DatabaseHandler.closeStatementResult(psql, res);
    	}
    	
    }

}
