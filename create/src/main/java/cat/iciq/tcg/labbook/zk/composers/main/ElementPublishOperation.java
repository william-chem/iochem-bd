/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.swordapp.client.ProtocolViolationException;
import org.swordapp.client.SWORDClientException;
import org.swordapp.client.SWORDCollection;
import org.swordapp.client.SWORDError;
import org.swordapp.client.ServiceDocument;
import org.zkoss.zul.TreeNode;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.datatype.services.PublishService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.utils.Rest50ApiManager;
import cat.iciq.tcg.labbook.web.utils.SwordManager;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;
import cat.iciq.tcg.labbook.zk.components.publishtree.Element;
import cat.iciq.tcg.labbook.zk.components.publishtree.Element.ElementType;
import cat.iciq.tcg.labbook.zk.components.publishtree.PublishTreeModel;
import cat.iciq.tcg.labbook.zk.ui.LongOperation;

public class ElementPublishOperation extends LongOperation {
	
	private static final Logger log = LogManager.getLogger(ElementPublishOperation.class);
	private Rest50ApiManager restManager = null;
	private PublishTreeModel elementTreeModel = null;
	private int publishedTotalCount = 0;
	private int publishedProjectCount = 0;
	private int maxCalculations = 0;
	private int publishedCalculationCount = 0;
	
	
	private HashMap<String, String> additionalMetadata;
	private ArrayList<Element>doiRequestCollection;
	private ArrayList<Element>createCollectionRequests;
	private ArrayList<Element>updateCollectionRequests;
	private boolean generateDOIs = false;
	private boolean appendDescription = false;
	
	 
	
	private static final String REST_QUERY_GET_HANDLE = "/collection/handle/text()";
	
	public ElementPublishOperation(PublishTreeModel elementTreeModel) throws Exception {
		this.elementTreeModel = elementTreeModel;
		
		maxCalculations = countCalculationsToPublish(0, elementTreeModel.getRoot());
		updateCollectionRequests = new ArrayList<Element>();
		doiRequestCollection = new ArrayList<Element>();	
		createCollectionRequests = new ArrayList<Element>();		
	}
	
	private int countCalculationsToPublish(int count, TreeNode<Element> element) {
		List<TreeNode<Element>> children = element.getChildren();		
		for(TreeNode<Element> child : children)
			if(child.getData().isItem()) {
				if(!child.getData().isPublished())					
					count++;
			} else {
				count=+ countCalculationsToPublish(count, child);
			}
		return count;
	}
	
	@Override
	protected void execute() throws InterruptedException {		
		try {
			init();
			publishTreeElements();
		}catch(Exception e) {
			log.error(e.getMessage());
		}
	}
		
	@Override
    protected void onFinish() {
		try {
			this.clearStatus();
		} catch (InterruptedException e) {
			log.error(e.getMessage());			
		}
    };	 
	
	private void init() throws Exception {
		restManager = new Rest50ApiManager();
	}
	
	public HashMap<String, String> getAdditionalMetadata() {
		return additionalMetadata;
	}

	public void setAdditionalMetadata(HashMap<String, String> additionalMetadata) {
		this.additionalMetadata = additionalMetadata;
	}

	public boolean isGenerateDOIs() {
		return generateDOIs;
	}

	public void setGenerateDOIs(boolean generateDOIs) {
		this.generateDOIs = generateDOIs;
	}

	public boolean shouldAppendDescription() {
		return appendDescription;
	}

	public void setAppendDescription(boolean appendDescription) {
		this.appendDescription = appendDescription;
	}

    public int getPublishedTotals() {
    	return publishedTotalCount;
    }
    
    public int getPublishedProjectCount() {
    	return publishedProjectCount;
    }
    
    public int getPublishedCalculationCount() {
    	return publishedCalculationCount;
    }

    public ArrayList<Element> getDoiRequestCollection() {
		return doiRequestCollection;
	}

	public ArrayList<Element> getCreateCollectionRequests() {
		return createCollectionRequests;
	}

	public ArrayList<Element> getUpdateCollectionRequests() {
		return updateCollectionRequests;
	}

	private void publishTreeElements() throws Exception{		
		List<TreeNode<Element>> communities = elementTreeModel.getRoot().getChildren();
		for(TreeNode<Element> community : communities)
			publishChildCollections(community);
	}
    
	private void publishChildCollections(TreeNode<Element> parentNode) throws Exception {
		for(TreeNode<Element>collection : parentNode.getChildren())		
			publishCollection(parentNode.getData().getBrowseId(), parentNode.getData().getHandle(), collection);	
	}
	
	private void publishCollection(String communityId, String communityHandle, TreeNode<Element> collectionNode) throws Exception {
		Element collection = collectionNode.getData();
		if(!collection.isPublished()){
			createBrowseCollection(communityId, collection);					//Top level projects are generated as collections on Browse module, subprojects get same handle as parent project but aren't generated. This is done to avoid generating too much structures on Browse side			
			publishedProjectCount++;
			publishedTotalCount++;
			this.showStatus(
					String.format("<p class='notification-header'>Current publication progress</p>" + 
								  "<p class='notification-line'>Projects: %d</p>" +
								  "<p class='notification-line'>Calculations: %d of %d</p>", 
									publishedProjectCount, 
									publishedCalculationCount,
									maxCalculations )
					);			
		}else{
			updateCollectionRequests.add(collection);
		}
		SWORDCollection swordCollection = findSWORDCollection(communityHandle, collection.getHandle());
		for(TreeNode<Element> item: collectionNode.getChildren())
			if(item.getData().getType() == ElementType.COLLECTION){
				publishSubcollection(swordCollection, collectionNode, item);
			}else
				publishItem(swordCollection, item);	
	}
	
	private void createBrowseCollection(String communityId, Element collection) throws Exception {	
		String handle = createCollectionWithRest(communityId, collection);
		setProjectPublicationData(collection, handle, true);		
		savePublishedProject(collection);
		if(generateDOIs)
			doiRequestCollection.add(collection);
		createCollectionRequests.add(collection);
	}
	
	
	private void publishSubcollection(SWORDCollection swordCollection, TreeNode<Element> parentNode, TreeNode<Element> collectionNode) throws Exception{
		Element parent = parentNode.getData();
		Element collection = collectionNode.getData();
		if(!collection.isPublished()){
			setProjectPublicationData(collection, parent.getHandle(), false);											//Subprojects are not generated inside Browse structure, they just inherit parents information	
			savePublishedProject(collection);
		}
		for(TreeNode<Element> item: collectionNode.getChildren())
			if(item.getData().getType() == ElementType.COLLECTION){
				publishSubcollection(swordCollection, collectionNode, item);
			}else
				publishItem(swordCollection, item);	
	}
	
	private SWORDCollection findSWORDCollection(String communityHandle, String collectionHandle) throws SWORDClientException, ProtocolViolationException, BrowseCredentialsException {		
		ServiceDocument serviceDocument= new SwordManager().getServiceDocument("/" + communityHandle);
		for(SWORDCollection collection : serviceDocument.getWorkspaces().get(0).getCollections()){
			if(collection.getHref().getPath().endsWith(collectionHandle))
				return collection;
		}	
		return null;
	}
	
	private void publishItem(SWORDCollection collection, TreeNode<Element> itemNode) throws InterruptedException, SWORDClientException, SWORDError, ProtocolViolationException, IOException, BrowseCredentialsException {
		Element item = itemNode.getData();
		try {
			if(!item.isPublished()) {					
			
					HashMap<String,String> params = new HashMap<String,String>();
					params.put("id",String.valueOf(item.getCreateId()));
					params.put("title",item.getPath());
					if(shouldAppendDescription())
						params.put("description", item.getDescription());
					params.put("author", ShiroManager.getCurrent().getUserFullName());						
					params.putAll(additionalMetadata);			
					params.put("metadata#dc:type", "dataset");
					
					
					
					String zipFilePath = PublishService.buildMetsPackage(params);
					String handle = new SwordManager().upload(collection, zipFilePath);
					if(handle == null)
						return;				
					new File(zipFilePath).delete();			
					//Set tree node as published
					item.setPublished(true);
					item.setHandle(handle);		
					item.setPublishedName(item.getPath());	
					//Set CREATE calculation as published					
					CalculationService.publishCalculation(item.getCreateId(), item.getPublishedName(), item.getHandle());					
					publishedCalculationCount++;
					publishedTotalCount++;				
				
					this.showStatus(
							String.format("<p class='notification-header'>Current publication progress</p>" + 
										  "<p class='notification-line'>Projects: %d</p>" +
										  "<p class='notification-line'>Calculations: %d of %d</p>", 
											publishedProjectCount, 
											publishedCalculationCount,
											maxCalculations ));
			}
		}catch(Exception e) {
			log.error("Error raised while publishing item: " + item.toString());				
		}		
	}

	private String createCollectionWithRest(String communityId, Element collection) throws Exception{
		int collectionId = restManager.postCollection(communityId, collection.getName());					//Create a new collection and get BROWSE internal id
		XMLFileManager xml = restManager.getCollection(collectionId);
		return xml.getStringFromQuery(REST_QUERY_GET_HANDLE);		
	}

	private void setProjectPublicationData(Element collection, String handle, boolean isEditable) throws Exception{			
		collection.setPublished(true);		
		collection.setHandle(handle);
		collection.setPublishedName(collection.getPath());
	}
	
	private void savePublishedProject(Element collection) throws InterruptedException{	
		try {
			Project project = ProjectService.getById(collection.getCreateId());
			project.setPublished_name(collection.getPublishedName());
			project.setHandle(collection.getHandle());			
			ProjectService.publishProject(project);			
		}catch(Exception e) {
			log.error("Error saving project publication information. "  + e.getMessage());
		}		
	}	

}
