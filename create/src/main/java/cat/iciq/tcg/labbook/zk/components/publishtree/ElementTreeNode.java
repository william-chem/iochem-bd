/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree;

import org.zkoss.zul.DefaultTreeNode;


public class ElementTreeNode extends DefaultTreeNode<Element> {
	private static final long serialVersionUID = -7012663776755277499L;
	
	private boolean open = false;

	public ElementTreeNode(Element data, DefaultTreeNode<Element>[] children) {
		super(data, children);
	}

	public ElementTreeNode(Element data, DefaultTreeNode<Element>[] children, boolean open) {
		super(data, children);
		setOpen(open);
	}

	public ElementTreeNode(Element data) {
		super(data);

	}

	public boolean isOpen() {
		return open;
	}

	public void setOpen(boolean open) {
		this.open = open;
	}

}
