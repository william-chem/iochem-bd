/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.IOException;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Session;

import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.components.publishtree.doi.DoiRequest;
import cat.iciq.tcg.labbook.zk.components.publishtree.doi.Author;

public class RestDoiDaemonManager {
	
	private static final Logger log = LogManager.getLogger(RestDoiDaemonManager.class.getName());	
	
	private CloseableHttpClient client;
	private String doiDaemonUrl = null;
	
	public RestDoiDaemonManager() throws Exception{		
		doiDaemonUrl = CustomProperties.getProperty(Constants.DOI_URL_PROPERTY);
		client = RestManager.buildAcceptAllHttpClient();
	}
	
	public int getInstitutionStatus(){		
		String restUrl = null;
		String response = null;
		try {
			String baseUrl = URLEncoder.encode(buildDoiDaemonBaseUrl(), "UTF-8");
			restUrl = doiDaemonUrl + Constants.REST_INSTITUTION_STATUS_ENDPOINT + baseUrl;
			response = RestManager.getRequestAsText(client, restUrl, null);
			return Integer.parseInt(response);
		} catch (Exception e) {
			log.error("Error raised retrieving institution status for url " + restUrl + " "  + e.getMessage());
			return Constants.INSTITUTION_STATUS_DISABLED;
		}
	}
	
	public void requestDoi(String url, String email, String depositorName, String title , ArrayList<String> authorNames, ArrayList<String>authorSurnames ) throws Exception{		
		String salt = CustomProperties.getProperty("external.communication.secret");
		String token = TokenManager.encode(salt, url + "#" + email);
		
		DoiRequest request = buildRequest(url, email, depositorName, title, authorNames, authorSurnames);		
		String json = request.getJson();				
		
		HashMap<String, String> headers = new HashMap<String, String>();
		headers.put("Content-Type", "application/json");
		try {			
			String restUrl = doiDaemonUrl + Constants.REST_DOIREQUEST_ENDPOINT + "/" + token;			
			HttpEntity entity = new StringEntity(json, ContentType.create("application/json", "UTF-8"));			
			String response = RestManager.postRequest(client, restUrl, entity, headers);
			log.info(response);
		} catch (Exception e) {
			log.error("Error raised requesting DOI to daemon with json :" + json + " Exception :" + e.getMessage());
			throw new Exception(json);
		}
	}

	private DoiRequest buildRequest(String url, String email, String depositorName, String title , ArrayList<String> authorNames, ArrayList<String>authorSurnames ){
		DoiRequest doiRequest = new DoiRequest();
		doiRequest.setDoiBatchId(url);
		doiRequest.setDepositorEmailAddress(email);
		doiRequest.setDepositorName(depositorName);
		doiRequest.setTitle(title);
		
		ArrayList<Author> authors = new ArrayList<Author>();
		for(int inx = 0; inx < authorNames.size(); inx++){
			String name = authorNames.get(inx);
			String surname = authorSurnames.get(inx);
			String sequence = inx== 0? "first": "additional";
			String role = "author";
			Author author = new Author(name, surname, role, sequence);
			authors.add(author);
		}		
		doiRequest.setAuthors(authors);
		return doiRequest;
	}
	
	
	private static String buildDoiDaemonBaseUrl(){    	
    	String baseHostName = CustomProperties.getProperty("create.server.hostname");
    	String basePort = CustomProperties.getProperty("create.server.port");
    	String portDelimiter = basePort.equals("")?"":":";
    	String baseUrl = "https://" + baseHostName + portDelimiter + basePort;
    	return baseUrl;
	}

	public void close(){
		try {
			client.close();
		} catch (IOException e) {
			log.error("Exception raised while closing RestDoiDaemonManager connection. " + e.getMessage());
		}
	}
}
