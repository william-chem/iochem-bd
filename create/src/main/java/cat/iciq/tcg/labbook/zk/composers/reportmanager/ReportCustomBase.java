/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.io.FileUtils;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Attr;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zkoss.zhtml.Filedownload;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.CreateEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Window;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.Report;
import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.datatype.ReportType;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.ReportTypeService;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.web.definitions.Constants.ScreenSize;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;

public abstract class ReportCustomBase extends SelectorComposer<Window> {
	
	private static final long serialVersionUID = 1L;
	protected static final Logger logger = LogManager.getLogger(ReportCustomBase.class.getName());
	
	protected static final String CONFIG_PARAMETER_ENERGY_UNITS_KCAL_MOL 	= "nonsi2:kcal.mol-1";
	protected static final String CONFIG_PARAMETER_ENERGY_UNITs_KJ_MOL 		= "nonsi:kj.mol-1";
	protected static final String CONFIG_PARAMETER_ENERGY_UNITS_EV 			= "nonsi:electronvolt";
	protected static final String CONFIG_PARAMETER_ENERGY_UNITS_HARTREE 	= "nonsi:hartree";
	protected static final String CONFIG_PARAMETER_ENERGY_UNITS_CM_1 		= "nonsi:cm-1";
	
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL	= "POTENTIAL";
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_FREE		= "FREE";
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_FREE_CORRECTED	= "FREE_CORRECTED";
	
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT	= "ZERO_POINT";
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY	= "ENTHALPY";
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_BONDING		= "BONDING";
	protected static final String CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION = "NUCREPULSION";
	
	protected static final String ALL_ENERGY_TYPES_REGEX = "(" + CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL + "|" + CONFIG_PARAMETER_ENERGY_TYPE_FREE + "|" + CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT + "|" + CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY + "|" + CONFIG_PARAMETER_ENERGY_TYPE_BONDING + "|" + CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION +")";	
	
	protected static final String CONFIG_PARAMETER_OUTPUT_TYPE_TXT 		= "TXT";
	protected static final String CONFIG_PARAMETER_OUTPUT_TYPE_PDF 		= "PDF";
	protected static final String CONFIG_PARAMETER_OUTPUT_TYPE_CHART 	= "CHART";
	
	protected static final String QUERY_GET_ENERGY_UNITS = "/configuration/parameters/energyUnits/text()";
	protected static final String QUERY_GET_OUTPUT_TYPE  = "/configuration/parameters/outputType/text()";
	protected static final String QUERY_GET_ENERGY_TYPE  = "/configuration/parameters/energyType/text()";
	protected static final String QUERY_GET_ENERGY_REACTION_PROFILE_SERIES  = "/configuration/parameters/series/serie";
	protected static final String QUERY_GET_ENERGY_REACTION_PROFILE_GIBBS_TEMPERATURE  = "/configuration/parameters/energy/gibbs/temperature/text()";
	protected static final String QUERY_GET_ENERGY_REACTION_PROFILE_GIBBS_PRESSION = "/configuration/parameters/energy/gibbs/pression/text()";
	
	protected Window parent;
	protected ReportBase parentSelector;
	protected Report report;
	protected LinkedHashMap<String,String> calculationsContent;

	protected static TransformerFactory tFactory;
	protected static HashMap<String,Templates> loadedXsltTemplates;
	protected static FopFactory fopFactory;

	protected EventQueue<Event> reportManagementQueue = null;
	
	@Wire
	Window customReportWindow;
	
	@Wire 
	Radiogroup fileFormatRgp;
	
	@Listen("onCheck=#fileFormatRgp")
	public void onFileFormatRdgCheck() throws SAXException, IOException{
		saveConfiguration();
	}
	
	@SuppressWarnings("unchecked")
	@Listen("onCreate=#customReportWindow")
	public void onReportBaseWindowCreate(CreateEvent event){	
		//Get report from parent window (passed as argument)
		parent = (Window)event.getArg().get("parent");
		parentSelector = (ReportBase) parent.getAttribute("reportBaseWindow$composer");
		report = parentSelector.getReport();
		reportManagementQueue = EventQueues.lookup("reportmanagement", EventQueues.DESKTOP,true);
		reportManagementQueue.subscribe(new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {
				if(event.getName().equals("reportChanged")){
					int reportId = (Integer)event.getData();
					if(reportId == report.getId())	{		//Only process events with same reportId as our internal report object
						onCalculationsChanged();
						parentSelector.addChanges();
					}
				}else if(event.getName().equals("reportCalculationDragged")) {
					int reportId = (Integer)event.getData();
					if(reportId == report.getId())
						onReportCalculationDragged();
				}else if(event.getName().equals("reportTextFieldChanged")) {
					int reportId = (Integer)event.getData();
					if(reportId == report.getId())
						onReportTextFieldChanged();
				}else if(event.getName().equals("generateReport")){
					int reportId = (Integer)event.getData();
					if(reportId == report.getId())
						generateReport();
				}else if(event.getName().equals("closeReport")){
					int reportId = (Integer)event.getData();
					if(reportId == report.getId())
						reportManagementQueue.unsubscribe(this);
				}
			}			
		});
		try {
			addOutputTypeRadiobuttons();
			loadReport();			
		} catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	static {		
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
		loadedXsltTemplates = new HashMap<String,Templates>();		
		for(ReportType reportType : ReportTypeService.getAllReportTypes()) 	//We'll load all templates inside a HashMap so we can access them statically
			for(String templatePath :reportType.getXmlStylesheet().split("\\|")){
				String xsltPath = Sessions.getCurrent().getWebApp().getRealPath(templatePath);
				StreamSource xslSource = new StreamSource(new File(xsltPath));
				try {
					Templates template = tFactory.newTemplates(xslSource);
					loadedXsltTemplates.put(templatePath, template);
				} catch (TransformerConfigurationException e) {
					continue;
				}
		}
		try {			
			fopFactory = FopFactory.newInstance(new File(Sessions.getCurrent().getWebApp().getRealPath("/WEB-INF/fop.xconf")));
		} catch (SAXException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		} catch (IOException e) {
			logger.error(e.getMessage());
			e.printStackTrace();
		}
	}
	
	protected String getCalculationContent(String path) throws Exception{			     	
		//Iterate report associated calculations
    	calculationsContent = new LinkedHashMap<String,String>();
		Iterator<ReportCalculation> iter = report.getCalculations().iterator();
		while(iter.hasNext()){
			ReportCalculation reportCalculation = iter.next();
			if(reportCalculation.getCalcPath().equals(path)){
				return getOutputFileContentFromPath(path);						
			}
		}
		return null;		
	}
	
	protected boolean isSmallLayout() {
		ScreenSize layout = getCurrentLayout(); 
		return layout == ScreenSize.SMALL || layout == ScreenSize.X_SMALL;
	}
	
	protected ScreenSize getCurrentLayout() {
		ScreenSize layout = (ScreenSize) Executions.getCurrent().getDesktop().getAttribute("display");
		if(layout == null)
			return ScreenSize.X_LARGE;
		else
			return layout;
	}
	
	private String getOutputFileContentFromPath(String path) throws Exception{		
		Calculation calculation = CalculationService.getByPath(path);
		
		XMLFileManager metsFileManager = new XMLFileManager(MetsFileHandler.METS_NAMESPACE, MetsFileHandler.METS_ADDITIONAL_NAMESPACES, calculation.getMetsXml());
		NodeList iter2 = metsFileManager.getItemIteratorQuery(XpathQueries.GET_CALCULATION_FILES_FILEID);
		
		List<CalculationFile> files = CalculationService.getCalculationFiles(calculation.getId());
		for(int inx = 0; inx < iter2.getLength(); inx++){			
			String fileID 			= ((Attr) iter2.item(inx)).getTextContent();				
			String mimeType			= metsFileManager.getSingleAttributeValueQuery(XpathQueries.GET_MIMETYPE_FROM_FILEID.replace("?", fileID));
			String fileName			= metsFileManager.getSingleAttributeValueQuery(XpathQueries.GET_FILENAME_FROM_FILEID.replace("?", fileID));
			if(mimeType.equals("chemical/x-cml")){		//Output type			
				for(CalculationFile file : files) {
					String filePath = file.getFile();
					if(filePath.endsWith(fileName)){							
						String realPath = FileService.getCreatePath() + File.separatorChar + filePath;
						return FileUtils.readFileToString(new File(realPath));													
					}
				}
			}										
		}
		return null;
	}
	

	
	private void addOutputTypeRadiobuttons() throws Exception{		
		ReportType reportType = ReportTypeService.getReportTypeById(report.getType().getId());
		String types[] = reportType.getOutputType().trim().split("\\|");
		for(String type : types){
			Radio typeRadio = new Radio(type);
			typeRadio.setValue(type);
			fileFormatRgp.appendChild(typeRadio);			
		}				
	}
	
	
	protected void onReportCalculationDragged() {
		try {			
			saveConfiguration();
		} catch (SAXException e) {		
			logger.error(e.getMessage());
		} catch (IOException e) {		
			logger.error(e.getMessage());
		}		
	}
	
	protected void onReportTextFieldChanged() {
		try {			
			saveConfiguration();
		} catch (SAXException e) {		
			logger.error(e.getMessage());
		} catch (IOException e) {		
			logger.error(e.getMessage());
		}		
	}	
	
	protected abstract void onCalculationsChanged();
	
	protected abstract void loadReport() throws Exception;

	protected abstract void generateReport() throws Exception;
	
	protected abstract void saveConfiguration() throws SAXException, IOException;  

	protected void convertFoDocumentToSpecificFormat(String documentContent){
		String format = fileFormatRgp.getSelectedItem().getValue();
		String formatMime = getOutputFormat(format);
		String fileName = report.getTitle() == null || report.getTitle().equals("")?  UUID.randomUUID().toString() : report.getTitle();		
		try {
			ByteArrayOutputStream out = new ByteArrayOutputStream();
		    Fop fop = fopFactory.newFop(formatMime,out);
		    TransformerFactory factory = TransformerFactory.newInstance();
		    Transformer transformer = factory.newTransformer(); // identity transformer
		    Source src = new StreamSource(new StringReader(documentContent));
		    Result res = new SAXResult(fop.getDefaultHandler());
		    transformer.transform(src, res);
		    Filedownload.save(out.toByteArray(), formatMime, fileName + "." + format.toLowerCase());
		} catch (Exception e) {
			
		}		
	}
	
	private String getOutputFormat(String selectedFormat){
		if(selectedFormat.equals("PDF"))
			return "application/pdf";
		else if(selectedFormat.equals("TXT"))
			return "text/plain";
		else if(selectedFormat.equals("RTF"))
			return "text/rtf";
		else 
			return "text/plain";
		
//	    /** PostScript */
//	    String MIME_POSTSCRIPT      = "application/postscript";
//	    /** Encapsulated PostScript (same MIME type as PostScript) */
//	    String MIME_EPS             = MIME_POSTSCRIPT;
//
//	    /** HP's PCL */
//	    String MIME_PCL             = "application/x-pcl";
//	    /** HP's PCL (alternative MIME type) */
//	    String MIME_PCL_ALT         = "application/vnd.hp-PCL";
//
//	    /** IBM's AFP */
//	    String MIME_AFP             = "application/x-afp";
//
//	    /** IBM's AFP (alternative MIME type) */
//	    String MIME_AFP_ALT         = "application/vnd.ibm.modcap";
//
//	    /** IBM's AFP IOCA subset for bilevel raster image */
//	    String MIME_AFP_IOCA_FS10   = "image/x-afp+fs10";
//
//	    /** IBM's AFP IOCA subset for grayscale and color raster image */
//	    String MIME_AFP_IOCA_FS11   = "image/x-afp+fs11";
//
//	    /** IBM's AFP IOCA subset for grayscale and color tiled raster image */
//	    String MIME_AFP_IOCA_FS45   = "image/x-afp+fs45";
//
//	    /** IBM's AFP GOCA subset for graphical objects */
//	    String MIME_AFP_GOCA        = "image/x-afp+goca";
//
//
//	    /** Rich text format */
//	    String MIME_RTF             = "application/rtf";
//	    /** Rich text format (alternative 1) */
//	    String MIME_RTF_ALT1        = "text/richtext";
//	    /** Rich text format (alternative 2) */
//	    String MIME_RTF_ALT2        = "text/rtf";
//
//	    /** FrameMaker's MIF */
//	    String MIME_MIF             = "application/mif";
//
//	    /** Structured Vector Graphics */
//	    String MIME_SVG             = "image/svg+xml";
//
//	    /** GIF images */
//	    String MIME_GIF             = "image/gif";
//	    /** PNG images */
//	    String MIME_PNG             = "image/png";
//	    /** JPEG images */
//	    String MIME_JPEG            = "image/jpeg";
//	    /** TIFF images */
//	    String MIME_TIFF            = "image/tiff";
//
//	    /** Proposed but non-registered MIME type for XSL-FO */
//	    String MIME_XSL_FO          = "text/xsl";
//
//	    /** Microsoft's Enhanced Metafile */
//	    String MIME_EMF             = "image/x-emf";
	}
}
