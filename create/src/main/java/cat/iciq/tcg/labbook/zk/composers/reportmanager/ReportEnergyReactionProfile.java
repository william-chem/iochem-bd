/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.reportmanager;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.RoundingMode;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.xerces.dom.DeferredTextImpl;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.zkoss.util.media.AMedia;
import org.zkoss.zhtml.Div;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.DropEvent;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Button;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Column;
import org.zkoss.zul.Columns;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Hlayout;
import org.zkoss.zul.Html;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Label;
import org.zkoss.zul.Layout;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;
import org.zkoss.zul.Row;
import org.zkoss.zul.Rows;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Vlayout;
import org.zkoss.zul.Window;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Iterables;

import cat.iciq.tcg.labbook.datatype.ReportCalculation;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;
import cat.iciq.tcg.labbook.zk.composers.main.ErrorDialog;
import cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.ReactionNetwork;
import net.sourceforge.jeval.Evaluator;
import net.sourceforge.jeval.VariableResolver;
import net.sourceforge.jeval.function.FunctionException;

public class ReportEnergyReactionProfile extends ReportCustomBase {

	private static final long serialVersionUID = 1L;
	
	protected static final int NUMBER_OF_STEPS = 5;
	protected static final int NUMBER_OF_VARIABLES = 5;
	
	protected int numberOfSeries = 0;
	
	protected double temperature = -1.0;
	protected double pressure = -1.0;
	protected double correctedGibbsTemperature = -1.0;
	protected double correctedGibbsPressure = -1.0;
	protected double frequencyThreshold = -5.0;
	
	protected boolean isGibbsCorrected = false;
	
	public static final String AVAILABLE_ENERGIES_XSLT_TEMPLATE_PATH   = "/html/reports/ReportEnergyReactionProfile/getAvailableEnergies.xsl";
	public static final String TEMPERATURE_PRESSION_XSLT_TEMPLATE_PATH = "/html/reports/ReportEnergyReactionProfile/getTempPressure.xsl";
	public static final String DATA_EXTRACTION_XSLT_TEMPLATE_PATH      = "/html/reports/ReportEnergyReactionProfile/dataExtraction.xsl";
	public static final String CHART_GENERATION_XSL_TEMPLATE_PATH      = "/html/reports/ReportEnergyReactionProfile/chartGeneration.xsl";
	

	@Wire 
	Window customReportWindow;
	
	@Wire 
	Radiogroup energyTypeRdg;
	
	@Wire
	Label unavailableEnergyLbl;
	
	@Wire
	Radio potentialEnergyRad;
	
	@Wire
	Radio freeEnergyRad; 
	
	@Wire
	Button freeCorrectedBtn;
	
	@Wire 
	Radio zeroPointEnergyRad;
	
	@Wire
	Radio enthalpyEnergyRad;
	
	@Wire 
	Radio bondingEnergyRad; 
	
	@Wire
	Radio nucRepulsionEnergyRad;
	
	@Wire
	Radiogroup energyUnitsRdg;
	
	@Wire 
	Radio energyUnitsKcalMolRad;
	
	@Wire
	Radio energyUnitsKJMolRad;
	
	@Wire
	Radio energyUnitsEVRad;
	
	@Wire
	Radio energyUnitsHartreeRad;
	
	@Wire
	Tabs seriesTabs;

	@Wire
	Tabpanels seriesTabpanels;
	
	@Wire
	Window reportInformation;
	
	@Listen("onDisplayInformation=#customReportWindow")
	public void displayInformation() {
		reportInformation.doModal();		
	}
	
	@Listen("onClick=#generateReactionNetworkBtn")
	public void onGenerateReactionNetworkBtnClick() {
		try {
			generateReactionNetwork();
		} catch (Exception e) {
			showErrorMessage("Error raised while building reaction network", e.getMessage(), null, null);
			logger.error(e.getMessage());
		}	
	}
	
	private void showErrorMessage(String title, String error, String cause, String solution) {
        Window window = (Window) Executions.createComponents("errors/errorDialog.zul", customReportWindow, null);
        window.setWidth("600px");
        ErrorDialog errorDialog = (ErrorDialog) window.getAttribute("$composer");
        errorDialog.setTitle(title);
        errorDialog.setError(error != null? new Html(error): null);
        errorDialog.setCause(cause != null? new Html(cause): null);        
        errorDialog.setSolution(solution != null? new Html(solution): null);        
        window.doModal();
	}
	
	@SuppressWarnings("unchecked")
	@Listen("onClick=#freeCorrectedBtn")
	public void onFreeCorrectedBtnClick() {
		try {		
			freeEnergyRad.setSelected(true);
			HashMap<String, Double> params = new HashMap<String, Double>();
			params.put("basePressure", pressure);
			params.put("baseTemperature", temperature);			
			params.put("pressure", correctedGibbsPressure);
			params.put("temperature", correctedGibbsTemperature);
			Window window = (Window)Executions.createComponents("/zul/reports/reportEnergyReactionProfileGibbsParameters.zul", null, params);
			window.addEventListener("onClosing", new EventListener() {
				@Override
				public void onEvent(Event event) throws Exception {
					HashMap<String, Double> params = (HashMap<String, Double>) event.getData();					
					correctedGibbsPressure = params.get("pressure");
			      	correctedGibbsTemperature = params.get("temperature");
			      	
			      	isGibbsCorrected = temperature != correctedGibbsTemperature || pressure != correctedGibbsPressure;
			      	freeEnergyRad.setLabel(isGibbsCorrected? "Gibbs Energy (corrected)" : "Gibbs Energy");
				}				
			});
	      	window.doModal();
		}catch (Exception e) {
			logger.error(e.getMessage());
		}
	}
	
	@Listen("onCheck=#energyTypeRdg")
	public void onEnergyTypeRdgCheck() throws SAXException, IOException {
		saveConfiguration();
	}

	@Listen("onCheck=#energyUnitsRdg")
	public void onEnergyUnitsRdgCheck() throws SAXException, IOException{
		saveConfiguration();
	}
	
	@Listen("onClick=#addSerieTab")
	public void onAddSerieTabClick(){
		addSerie(null, null, null, null, null);
		fixCssClasses();
		focusOnLastSerie();		
	}
	
	@Override
	protected void generateReport() throws Exception {
		if(!formFieldsAreValid())
			return;				
		try {     	    
   	    	displayChart(generateChartHtml(retrieveCalculationsFields()));			
		} catch (Exception e) {
			Messagebox.show(e.getMessage());
			return;
		}
	}

	private void displayChart(String html) {
		Iframe iframe = new Iframe();		            		
		iframe.setWidth("100%");
		iframe.setHeight("100%");
		iframe.setStyle("scrolling:yes");
		AMedia media = new AMedia("Reaction energy profile", "html", "text/html", html);            	                       
        iframe.setContent(media);
        
        Window window = new Window();            
        window.appendChild(iframe);
        window.setMode(Window.OVERLAPPED);
        window.setBorder(true);
        window.setClosable(true);
        window.setTitle(report.getTitle());
        window.setPosition("center");
        window.setWidth("100%");
        window.setHeight("100%");
        window.setParent(customReportWindow);    
        window.setSizable(true);
	}
	
	/**
	 * Build chart html content
	 * @throws Exception
	 */
	private String generateChartHtml(String extractedData) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException, Exception {		
		Templates template = loadedXsltTemplates.get(CHART_GENERATION_XSL_TEMPLATE_PATH);		
		Transformer transformer = template.newTransformer();
		
		StreamSource xml = new StreamSource(new StringReader(extractedData));	            	    
   	    StringWriter writer = new StringWriter();
   	    StreamResult result = new StreamResult(writer);
		
   	    URL reconstructedURL = new URL(Executions.getCurrent().getScheme(),Executions.getCurrent().getServerName(),Executions.getCurrent().getServerPort() ,Executions.getCurrent().getContextPath());
   	    
   	    transformer.setParameter("reportTitle", report.getTitle());
    	transformer.setParameter("webrootpath", reconstructedURL.toString() + "/html");
    	transformer.setParameter("energyUnits", energyUnitsRdg.getSelectedItem().getLabel());
    	transformer.setParameter("energyType", energyTypeRdg.getSelectedItem().getValue());   	    
    	transformer.setParameter("javascriptEnergyParameters", buildJavascriptMatrixes(buildEnergyVariableResolver(extractedData)));   	    	
		transformer.transform(xml, result);
		return writer.getBuffer().toString();
	}

	/**
	 * Build reaction network html file
	 * @throws Exception
	 */
	protected void generateReactionNetwork() throws Exception {
		if(!formFieldsAreValid())
			return;	
		displayFrequencyThreshold();
	}
	
	
	@SuppressWarnings("unchecked")
	protected void displayFrequencyThreshold() {
		HashMap<String, Double> params = new HashMap<String, Double>();
		params.put("threshold", frequencyThreshold);
		Window window = (Window)Executions.createComponents("/zul/reports/reportEnergyReactionProfileFrequencyThreshold.zul", null, params);
		window.addEventListener("onClosing", new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				HashMap<String, Double> params = (HashMap<String, Double>) event.getData();					
				frequencyThreshold = params.get("threshold");
				
				String calculationFields = retrieveCalculationsFields();				
				ReactionNetwork network = new ReactionNetwork();
				addTransitionStatesToNetwork(network, calculationFields);
				addSeriesToNetwork(network, calculationFields);		
				displayGraphPage(network.buildGraph(), convertToJson(report.getCalculations()), convertToJson(network.getTS()));

				
			}				
		});
	  	window.doModal();
	}
	
	private String convertToJson(Object object) {		
		ByteArrayOutputStream bos = new ByteArrayOutputStream(); 
		ObjectMapper objectMapper = new ObjectMapper();		
		try {
			objectMapper.writeValue(bos, object);			
		} catch (Exception e) {			
			logger.error(e.getMessage());
			return "[]"; 
		}		
		return new String(bos.toByteArray());
	}	

	private void displayGraphPage(String dotGraph, String calcInfo, String tsInfo) {
		HashMap <String, String> parameters = new HashMap<>();
		parameters.put("dotGraph", dotGraph);
		
		Iframe iframe = new Iframe();		            		
		iframe.setWidth("100%");
		iframe.setHeight("100%");
		iframe.setStyle("scrolling:yes");	     
        iframe.setSrc("/html/reports/ReportEnergyReactionProfile/graphviz_d3.html");
        iframe.setClientDataAttribute("dotgraph", dotGraph);
        iframe.setClientDataAttribute("calc-info", calcInfo);
        iframe.setClientDataAttribute("transition-states", tsInfo);
        Window window = new Window();            
        window.appendChild(iframe);
        window.setMode(Window.OVERLAPPED);
        window.setBorder(true);
        window.setClosable(true);
        window.setTitle(report.getTitle());
        window.setPosition("center");
        window.setWidth("100%");
        window.setHeight("100%");
        window.setParent(customReportWindow);    
        window.setSizable(true);               
	}

	private void addTransitionStatesToNetwork(ReactionNetwork network, String xmlEnergyTags) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException{		
		List<Boolean> tsList = new ArrayList<Boolean>();
		String transitionStateQuery = "//*:template/*:scalar[@name='isTransitionState']";
		XMLFileManager xml = new XMLFileManager("http://www.xml-cml.org/schema", null, xmlEnergyTags);
		NodeList list = xml.getItemIteratorQuery(transitionStateQuery);
		for(int inx = 1; inx <= list.getLength(); inx++){
			Element isTs = (Element)list.item(inx-1);
			tsList.add(Boolean.valueOf(isTs.getTextContent().trim()));
			
		}	
		network.addTS(tsList);		
	}
	
	private void addSeriesToNetwork(ReactionNetwork network, String calculationFields) throws Exception {
		MockVariableResolver resolver = buildEnergyVariableResolver(calculationFields);
		DecimalFormat df = getEnergyFormatter();
		Evaluator evaluator = new Evaluator();

		for(Component serieTab : seriesTabpanels.getChildren()){			
			if(serieTab.equals(seriesTabpanels.getFirstChild()))	//Discard + tab
				continue;
			if(isSerieEmpty(serieTab))
				continue;
			
			Vlayout stepVlayout = (Vlayout) serieTab.query("vlayout.stepLayout");
			HashMap<String, String> userVariables = getUserDefinedVariables(serieTab);			
			Textbox serieTxt = (Textbox)serieTab.query(".serie-name-txt");
			
			String serieName = escapeString(serieTxt.getText());
			
			List<List<String>> formulas = new ArrayList<>();
			List<Double> energies = new ArrayList<Double>();
			
			for(Component component: stepVlayout.getChildren()){
				String label = ((Textbox)component.query(".stepLabel")).getValue();
				String formula = ((Textbox)component.query(".stepFormula")).getValue().replaceAll("\\s+", "");
				String formulaSimplified = replaceUserVariablesForValue(formula, userVariables);				
				
				if(formula.trim().equals(""))
					continue;

				ArrayList<String> formulaArray = new ArrayList<>();
				formulaArray.add(label.isEmpty()? formula : label);
				formulaArray.add(formulaSimplified);
				formulas.add(formulaArray);
			
				
				try {
					evaluator.setVariableResolver(resolver);
					String result = df.format(Double.valueOf(evaluator.evaluate(formulaSimplified.replace("C", "c").replaceAll("(c[0-9]+)", "#{$1}")))); //Surround cXX with #{ } so JEval library knows that it is a variable
					energies.add(Double.valueOf(result));
				} catch (Exception e) {
					throw new Exception("Error processing formula " + formulaSimplified);
				}
			}
			network.addSerie(serieName, formulas, energies);
		}				
		
	}

	/**
	 * Extract content from all the calculations and join them into a same root template element.
	 * @return An XML string with the report calculations required fields: geometry, energy, isTS, etc.
	 * @throws Exception 
	 */
	private String retrieveCalculationsFields() throws Exception {
		StringBuilder filteredCalculations = new StringBuilder();
		Transformer transformer = null;	
		Templates template = loadedXsltTemplates.get(DATA_EXTRACTION_XSLT_TEMPLATE_PATH);
		
		filteredCalculations.append("<template id='reportData' xmlns='http://www.xml-cml.org/schema'>");
		for(ReportCalculation calculation : report.getCalculations()){
			transformer = template.newTransformer();
			transformer.setParameter("title", calculation.getTitle());
			transformer.setParameter("energyUnits", energyUnitsRdg.getSelectedItem().getValue());
			transformer.setParameter("energyType",  energyTypeRdg.getSelectedItem().getValue());
			transformer.setParameter("isGibbsCorrected", isGibbsCorrected);
			transformer.setParameter("negativeFrequencyThreshold", frequencyThreshold);
		 	if(isGibbsCorrected) {			 		
		 		transformer.setParameter("pressure", correctedGibbsPressure);
		 		transformer.setParameter("temperature", correctedGibbsTemperature);
   	    	}							
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
       	    StringWriter writer = new StringWriter();
       	    StreamResult result = new StreamResult(writer);
       	    try {
				transformer.transform(xml, result);
				filteredCalculations.append(writer.getBuffer().toString());				
			} catch (TransformerException e) {				
				throw new Exception("Could not extract information from calculation c" + calculation.getCalcOrder());
			}
		}
		filteredCalculations.append("</template>");
		return filteredCalculations.toString(); 
	}

	private void addSerie(String serieName, ArrayList<String> formulas, ArrayList<String> labels, ArrayList<String> varNames, ArrayList<String> varLabels){
		numberOfSeries++;
		//Create tab
		
		if(serieName == null || serieName.equals("")) 
			serieName = "Serie";		
		Tab serieTab = new Tab(serieName);
		serieTab.setSclass("serie-tab");
		serieTab.setClosable(true);
		serieTab.setDraggable("true");
		serieTab.setDroppable("true");
		serieTab.addEventListener("onClick", new EventListener<Event>() {
			@Override
			public void onEvent(Event event) throws Exception {
				setSerieCopyBtnVisibility();				
			}			
		});			
		serieTab.addEventListener("onClose", new EventListener<Event>(){
			@Override
			public void onEvent(Event event) throws Exception {				
				saveConfiguration(event.getTarget());			
				setSerieCopyBtnVisibility();
			}
			
		});
		serieTab.addEventListener("onDrop", new EventListener<Event>(){
			@Override
			public void onEvent(Event event) throws Exception {
				DropEvent dropEvent = (DropEvent)event;		
				Tab draggedTab = (Tab)dropEvent.getDragged();
				Tab droppedTab = (Tab)dropEvent.getTarget();					
				Tabpanel draggedTabpanel = (Tabpanel)draggedTab.getAttribute("tabpanel");
				Tabpanel droppedTabpanel = (Tabpanel)droppedTab.getAttribute("tabpanel");										
				draggedTab.getParent().insertBefore(draggedTab, droppedTab);					
				draggedTabpanel.getParent().insertBefore(draggedTabpanel, droppedTabpanel);
				fixCssClasses();	
				setSerieCopyBtnVisibility();
				saveConfiguration();
			}
		});
				
		seriesTabs.appendChild(serieTab);
		//Create tabpanel
		Tabpanel serieTabpanel = new Tabpanel();
		serieTabpanel.setAttribute("tab", serieTab);
		//serieTabpanel.setVflex("1");		
		Grid stepGrid = buildSerieGrid(serieTab, serieTabpanel, serieName, formulas, labels);
		Grid variableGrid = buildVariableGrid(varNames, varLabels);
		Layout layout = isSmallLayout()? new Vlayout() : new Hlayout();
		layout.setVflex("1");
		layout.setHflex("1");
		layout.setSclass("report-series-layout");
		layout.appendChild(stepGrid);
		layout.appendChild(variableGrid);		
		serieTabpanel.appendChild(layout);
		seriesTabpanels.appendChild(serieTabpanel);
		serieTab.setAttribute("tabpanel", serieTabpanel);
	}
	
	private void setSerieCopyBtnVisibility() {
		Iterator<Component> tabPanelIter = seriesTabpanels.getChildren().iterator();
		seriesTabpanels.query(".report-series-layout");
		int inx = 0;
		while(tabPanelIter.hasNext()) {
			Tabpanel tabPanel = (Tabpanel) tabPanelIter.next();
			Button btn = (Button) tabPanel.query(".copy-last-serie-btn");
			if(btn != null)
				btn.setVisible(inx > 1);
			inx++;
		}		
	}
	
	@SuppressWarnings("unchecked")
	private Grid buildSerieGrid(Tab serieTab, Tabpanel serieTabpanel, String name, ArrayList<String> formulas, ArrayList<String> labels){
		Grid grid = new Grid();
		grid.setHflex("1");
		grid.setSclass("tblWithoutHover");
		grid.setStyle("margin-left: 5px");
		grid.setOddRowSclass("unexisting");
		Columns columns = new Columns();		
		Column column = new Column();
		//Formula + label columns
		column.setHflex("min");
		columns.appendChild(column);		
		column = new Column();
		column.setHflex("1");
		columns.appendChild(column);	
		grid.appendChild(columns);
		Rows rows = new Rows();
		rows.appendChild(buildNameRow(serieTab, serieTabpanel, name)); 		//First row, serie name
		rows.appendChild(buildSerieRow(formulas, labels));		//Second row, serie steps
		grid.appendChild(rows);
		return grid;
	}
	
	private Row buildNameRow(Tab serieTab, Tabpanel serieTabpanel, String name) {		
		Row row = new Row();
		row.setSclass("mt-2");
		Cell cell = new Cell();
		cell.appendChild(new Label("Name:"));
		row.appendChild(cell);
		cell = new Cell();		
		cell.setSclass("d-flex flex-nowrap");
		Textbox serieName = new Textbox(name);
		serieName.setHflex("1");
		serieName.setAttribute("tabpanel", serieTab);
		serieName.setSclass("serie-name-txt form-control form-control-sm col-sm-8");
		serieName.addEventListener("onChange", new EventListener<Event>(){
			@Override
			public void onEvent(Event event) throws Exception {				
				Textbox serieName = (Textbox)event.getTarget();
				Tab serieTab = (Tab) serieName.getAttribute("tabpanel");				
				serieTab.setLabel(serieName.getValue().equals("")?"Serie":escapeString(serieName.getValue()));
				saveConfiguration();				
			}			
		});
				
		cell.appendChild(serieName);

		Button copyLastSerieBtn = new Button("Copy");
		copyLastSerieBtn.setVisible(numberOfSeries > 1);
		copyLastSerieBtn.setHflex("min");
		copyLastSerieBtn.setTooltiptext("Copy from previous serie");
		copyLastSerieBtn.setSclass("copy-last-serie-btn btn btn-secondary btn-sm col-sm-4");			
		copyLastSerieBtn.addEventListener("onClick", new CopyFromPreviousSerie(serieTabpanel));
		cell.appendChild(copyLastSerieBtn);
		
		row.appendChild(cell);
		return row;
	}
		
	private Row buildSerieRow(ArrayList<String> formulas, ArrayList<String> labels) {				
		Row row = new Row();		
		Cell cell = new Cell();
		cell.setVflex("1");
		cell.appendChild(new Label("Steps:"));
		Html html = new Html("<n:button type='button' class='btn btn-link' onClick=\"fireEventFromClient('onDisplayInformation')\"><n:i class='fas fa-lg fa-question-circle'></n:i></n:button>");
		cell.appendChild(html);
		cell.setValign("top");
		row.appendChild(cell);		
		cell = new Cell();
		
		Vlayout stepLayout = new Vlayout();
		stepLayout.setSclass("stepLayout");
		stepLayout.setHflex("1");
		if(formulas != null)
			for(int inx = 1; inx <= formulas.size(); inx++)		
				appendNewStep(stepLayout, numberOfSeries, inx, formulas.get(inx-1), labels.get(inx-1));		
		else
			for(int inx = 1; inx <= NUMBER_OF_STEPS; inx++)		
				appendNewStep(stepLayout, numberOfSeries, inx, null, null);		
		cell.appendChild(stepLayout);
		row.appendChild(cell);
		return row;
	}

	private Grid buildVariableGrid(ArrayList<String> varNames, ArrayList<String> varValues){
		Grid grid = new Grid();
		grid.setHflex("1");
		grid.setSclass("tblWithoutHover");
		grid.setOddRowSclass("unexisting");
		Columns columns = new Columns();		
		Column column = new Column();
		//Formula + label columns
		column.setHflex("min");
		columns.appendChild(column);		
		column = new Column();
		column.setHflex("1");
		columns.appendChild(column);
		grid.appendChild(columns);
		//First row is empty, to match same height than series 
		Rows rows = new Rows();
		Row row = new Row();		
		Cell cell = new Cell();
		cell.appendChild(new Label("-"));
		row.appendChild(cell);
		cell = new Cell();
		row.appendChild(cell);
		rows.appendChild(row);
		//Second row, variables
		row = new Row();
		cell = new Cell();
		cell.appendChild(new Label("Variables:"));
		cell.setValign("top");
		row.appendChild(cell);		
		cell = new Cell();

		Vlayout variableLayout = new Vlayout();
		variableLayout.setSclass("variableLayout");
		if(varNames != null)
			for(int inx = 1; inx <= varNames.size(); inx++)		
				appendNewVariable(variableLayout, numberOfSeries, inx, varNames.get(inx-1), varValues.get(inx-1));		
		else
			for(int inx = 1; inx <= NUMBER_OF_STEPS; inx++)		
				appendNewVariable(variableLayout, numberOfSeries, inx, null, null);		
		cell.appendChild(variableLayout);
		row.appendChild(cell);
		rows.appendChild(row);
		grid.appendChild(rows);
		return grid;
	}
	
	@SuppressWarnings("unchecked")
	protected void appendNewStep(Vlayout stepLayout, int serieNumber, int stepNumber, String formula, String label){
		Hlayout hlayout = new Hlayout();			
		hlayout.setDraggable("stepformula");
		hlayout.setDroppable("stepformula");
		hlayout.addEventListener("onDrop", new FormulaDropped(this));
		Div dragDiv = new Div();
		dragDiv.setSclass("z-icon-drag");
		Textbox stepFormulaTxt = new Textbox();
		stepFormulaTxt.setId("serie" + serieNumber + "step" + stepNumber + "Txt");
		stepFormulaTxt.setPlaceholder("Formula");
		stepFormulaTxt.setSclass("stepFormula form-control form-control-sm");
		stepFormulaTxt.setAttribute("parentLayout", stepLayout);
		stepFormulaTxt.setAttribute("serie", serieNumber);
		stepFormulaTxt.setAttribute("step", stepNumber);		
		if(formula != null)
			stepFormulaTxt.setValue(formula);
		stepFormulaTxt.addEventListener("onChange", new StepTextboxChanged(this));
		
		Textbox stepLabelTxt = new Textbox();		
		if(label != null)
			stepLabelTxt.setValue(label);
		stepLabelTxt.setPlaceholder("Label");
		stepLabelTxt.setSclass("stepLabel form-control form-control-sm");
		stepLabelTxt.setAttribute("parentLayout", stepLayout);
		stepLabelTxt.setAttribute("serie", serieNumber);
		stepLabelTxt.setAttribute("step", stepNumber);
		stepLabelTxt.addEventListener("onChange", new StepTextboxChanged(this));
		hlayout.appendChild(dragDiv);
		hlayout.appendChild(stepFormulaTxt);
		hlayout.appendChild(stepLabelTxt);
		stepLayout.appendChild(hlayout);
	}
	
	protected void appendNewVariable(Vlayout variableLayout, int serieNumber, int varNumber, String varName, String varValue){
		Hlayout hlayout = new Hlayout();			
		
		Textbox varNameTxt = new Textbox();
		varNameTxt.setId("serie" + serieNumber + "variable" + varNumber + "Txt");
		varNameTxt.setPlaceholder("Variable");
		varNameTxt.setSclass("variableName form-control form-control-sm");
		varNameTxt.setAttribute("parentLayout", variableLayout);
		varNameTxt.setAttribute("serie", serieNumber);
		varNameTxt.setAttribute("variable", varNumber);		
		if(varName != null)
			varNameTxt.setValue(varName);
		varNameTxt.addEventListener("onChange", new ValueTextboxChanged(this));
		
		Textbox varValueTxt = new Textbox();		
		if(varValue != null)
			varValueTxt.setValue(varValue);
		varValueTxt.setPlaceholder("Formula");
		varValueTxt.setSclass("variableValue form-control form-control-sm");
		varValueTxt.setAttribute("parentLayout", variableLayout);
		varValueTxt.setAttribute("serie", serieNumber);
		varValueTxt.setAttribute("variable", varNumber);
		varValueTxt.addEventListener("onChange", new ValueTextboxChanged(this));		
		hlayout.appendChild(varNameTxt);
		hlayout.appendChild(varValueTxt);
		variableLayout.appendChild(hlayout);
	}
	
	private void focusOnFirstSerie(){
		if(seriesTabs.getChildren().size() > 1) {
			Tab firstSerie = (Tab)seriesTabs.getChildren().get(1);
			firstSerie.setSelected(true);	
		}				
	}
		
	private void focusOnLastSerie(){
		Tab lastSerie = (Tab)seriesTabs.getChildren().get(seriesTabs.getChildren().size()-1);
		lastSerie.setSelected(true);		
	}
	
	@Override
	protected  void loadReport() throws SAXException, IOException, TransformerConfigurationException, BrowseCredentialsException, InterruptedException, ParserConfigurationException{				
		if(report.hasNoConfiguration())
			setDefaultConfiguration();
		loadConfiguration();		
	}

	@Override
	protected void onCalculationsChanged() {
		potentialEnergyRad.setSelected(false);
		freeEnergyRad.setSelected(false);
		zeroPointEnergyRad.setSelected(false);
		enthalpyEnergyRad.setSelected(false);
		bondingEnergyRad.setSelected(false);
		nucRepulsionEnergyRad.setSelected(false);
		
		potentialEnergyRad.setVisible(false);
		freeEnergyRad.setVisible(false);
		freeCorrectedBtn.setVisible(false);
		zeroPointEnergyRad.setVisible(false);
		enthalpyEnergyRad.setVisible(false);
		bondingEnergyRad.setVisible(false);
		nucRepulsionEnergyRad.setVisible(false);
		
		try {
			enableAvailableEnergies();
			saveConfiguration();
		} catch (Exception e) {		
			logger.error(e.getMessage());		
		}
	}
	
	private void fixCssClasses() {
		Clients.evalJavaScript("addBootstrapClasses(" + (isSmallLayout()?"'small'":"'large'") + " );");
	}
	
	private void setDefaultConfiguration() throws SAXException, IOException{		
		String defaultConfiguration = 	"<configuration>" +
											"<parameters>" +
												"<energyUnits>" + CONFIG_PARAMETER_ENERGY_UNITS_EV + "</energyUnits>" +		
												"<outputType>" + CONFIG_PARAMETER_OUTPUT_TYPE_CHART + "</outputType>" +
												"<series>" +
												"<serie name=''>" +
														"<step></step>" +
														"<step></step>" +
														"<step></step>" +
														"<step></step>" +
														"<step></step>" +
														"<variables>" +
															"<variable></variable>" +
															"<variable></variable>" +
															"<variable></variable>" +
															"<variable></variable>" +
															"<variable></variable>" +
														"</variables>" +
													"</serie>" +
												"</series>" +
										  "</parameters>" +
										"</configuration>";
		report.setReportConfiguration(defaultConfiguration);
	}
	
	protected void saveConfiguration() throws SAXException, IOException{		
		saveConfiguration(null);
		fixCssClasses();		
	}
	
	protected void saveConfiguration(Component discardTab) throws SAXException, IOException {
		parentSelector.addChanges();
		
		StringBuilder configuration = new StringBuilder();
		configuration.append("<configuration>");
		configuration.append("<parameters>");
		if(energyTypeRdg.getSelectedItem()!= null)
			configuration.append("		<energyType>"  + energyTypeRdg.getSelectedItem().getValue()  + "</energyType>");
		if(energyUnitsRdg.getSelectedItem()!= null)
			configuration.append("		<energyUnits>" + energyUnitsRdg.getSelectedItem().getValue() + "</energyUnits>");
		if(fileFormatRgp.getSelectedItem()!= null)
			configuration.append("		<outputType>"  + fileFormatRgp.getSelectedItem().getValue()  + "</outputType>");		
		//Series information
		configuration.append("		<series>");
		for(Component serieTabpanel : seriesTabpanels.getChildren()){
			Vlayout stepVlayout = (Vlayout) serieTabpanel.query("vlayout.stepLayout");
			if(stepVlayout == null)																	//Plus tab
				continue;
			if(serieTabpanel.getAttribute("tab") == null)
				continue;
			if(discardTab != null && serieTabpanel.getAttribute("tab").equals(discardTab))		//We are deleting this tab, so don't include it in saved configuration
				continue;		
			String name = escapeString(((Textbox)serieTabpanel.query(".serie-name-txt")).getText());
			configuration.append("		<serie name='" + name  + "' >");
			for(Component component: stepVlayout.getChildren()){				
				String formula = escapeString(((Textbox)component.query(".stepFormula")).getValue());
				String label =  escapeString(((Textbox)component.query(".stepLabel")).getValue());
				if(!formula.trim().equals(""))
					configuration.append("				<step label='" + label +"'>" + formula + "</step>");
			}			
			
			Vlayout variableVlayout = (Vlayout) serieTabpanel.query("vlayout.variableLayout");
			if(variableVlayout == null)																	//Plus tab
				continue;		
			configuration.append("<variables>");
			for(Component component: variableVlayout.getChildren()){				
				String variableName = escapeString(((Textbox)component.query(".variableName")).getValue());
				String variableValue =  escapeString(((Textbox)component.query(".variableValue")).getValue());
				if(!variableName.trim().equals(""))
					configuration.append("<variable name='" + variableName +"'>" + variableValue + "</variable>");
			}
			configuration.append("</variables>");
			configuration.append("</serie>");
			
		}
		configuration.append("		</series>");				
		configuration.append("	</parameters>");
	    configuration.append("</configuration>");
		report.setReportConfiguration(configuration.toString());
	}
	

	private void loadConfiguration() throws TransformerConfigurationException, BrowseCredentialsException, InterruptedException, ParserConfigurationException {
		try {
			//Set energy type
			enableAvailableEnergies();
			NodeList energyType = report.queryConfiguration(QUERY_GET_ENERGY_TYPE);			
			if(energyType.item(0) != null){
				String energy =  energyType.item(0).getNodeValue();				
				if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL))
					potentialEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_FREE))
					freeEnergyRad.setSelected(true);			
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT))
					zeroPointEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY))
					enthalpyEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_BONDING))
					bondingEnergyRad.setSelected(true);
				else if(energy.equals(CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION))
					nucRepulsionEnergyRad.setSelected(true);
			}
			//Set energy units
			NodeList unitNodes = report.queryConfiguration(QUERY_GET_ENERGY_UNITS);
			String units = unitNodes.item(0).getNodeValue(); 
			if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_KCAL_MOL))
				energyUnitsKcalMolRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITs_KJ_MOL))
				energyUnitsKJMolRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_EV))
				energyUnitsEVRad.setSelected(true);
			else if(units.equals(CONFIG_PARAMETER_ENERGY_UNITS_HARTREE))
				energyUnitsHartreeRad.setSelected(true);
			//Set output type
			NodeList outputType = report.queryConfiguration(QUERY_GET_OUTPUT_TYPE);
			String outType = outputType.item(0).getNodeValue();			
			for(Component child : fileFormatRgp.getChildren()){
				Radio typeRad = (Radio)child;
				if(typeRad.getValue().equals(outType)){
					typeRad.setSelected(true);
					break;
				}					
			}
			//Set series
			NodeList series = report.queryConfiguration(QUERY_GET_ENERGY_REACTION_PROFILE_SERIES);
			for(int inx = 0; inx < series.getLength(); inx++){
				Element element = (Element)series.item(inx);
				String serieName = element.getAttribute("name");
				NodeList steps = element.getChildNodes();
				ArrayList<String> stepFormulas = new ArrayList<String>();
				ArrayList<String> stepLabels = new ArrayList<String>();
				
				ArrayList<String> variableName = new ArrayList<String>();
				ArrayList<String> variableValue = new ArrayList<String>();
				
				for(int inx2 = 0; inx2 < steps.getLength(); inx2++){	//Add valued steps
					if(!(steps.item(inx2) instanceof DeferredTextImpl)){
						Element childElement = (Element)steps.item(inx2);
						if(childElement.getLocalName().equals("step")){
							stepFormulas.add(childElement.getTextContent());
							stepLabels.add(childElement.getAttribute("label"));
						}else if(childElement.getLocalName().equals("variables")){
							NodeList variables = childElement.getChildNodes();
							for(int inx3 = 0; inx3 < variables.getLength(); inx3++){	//Add defined variables
								Element variable = (Element) variables.item(inx3);
								variableName.add(variable.getAttribute("name"));
								variableValue.add(variable.getTextContent());	
							}							
						}
					}
				}
				int currentStepFormulas = stepFormulas.size();
				int currentVariables = variableName.size(); 
				for(int inx2 = currentStepFormulas; inx2 < Math.max(NUMBER_OF_STEPS, currentStepFormulas + 2) ; inx2++){				
					stepFormulas.add("");	
					stepLabels.add("");
				}
				for(int inx2 = currentVariables; inx2 < Math.max(NUMBER_OF_VARIABLES, currentVariables + 2) ; inx2++){				
					variableName.add("");	
					variableValue.add("");
				}				
				addSerie(serieName, stepFormulas, stepLabels, variableName, variableValue);
				
			}			
			
			focusOnFirstSerie();
		} catch (SAXException e) {
			logger.error(e);			
		} catch (Exception e) {
			logger.error(e);
		}		
	}
	
	private void enableAvailableEnergies() throws Exception {
		ArrayList<String> calculationEnergies = new ArrayList<String>(); 
		Templates template = loadedXsltTemplates.get(AVAILABLE_ENERGIES_XSLT_TEMPLATE_PATH);
		Transformer transformer = template.newTransformer();
		//We'll query all calculations for it's containing energy types and enable energy types that exist in all report calculations		
		for(ReportCalculation calculation : report.getCalculations()){
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
       	    StringWriter writer = new StringWriter();
       	    StreamResult result = new StreamResult(writer);                  	    
       	    try{	              
       	    	transformer.transform(xml, result);
       	    	for(String existingEnergies : writer.getBuffer().toString().split("\\|")) //TODO:Code stylesheet and code real exisiting energies logic
       	    		calculationEnergies.add(existingEnergies);       	    	
       	    }catch (TransformerConfigurationException e){
       	    	logger.error(e.getMessage());
       	    	continue;
       	    }catch (TransformerException e){
       	    	logger.error(e.getMessage());
       	    	continue;               
       	    }  
		}
		int totalCalculations = report.getCalculations().size();
		int sumPotentialEnergyCalculations = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_POTENTIAL);
		int sumFreeEnergyCalculations 	   = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_FREE);
		boolean hasValidFieldsForEnergyCorrection  = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_FREE_CORRECTED) == totalCalculations && totalCalculations != 0;
		int sumZeroPointEnergyCalculations = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_ZERO_POINT);		
		int sumEnthalpyEnergyCalculations  = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_ENTHALPY);
		int sumBondingEnergyCalculations   = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_BONDING);
		int sumNucRepEnergyCalculations    = Collections.frequency(calculationEnergies, CONFIG_PARAMETER_ENERGY_TYPE_NUCREPULSION);
		if(hasValidFieldsForEnergyCorrection) 
			hasValidFieldsForEnergyCorrection = readTemperatureAndPressure();

		unavailableEnergyLbl.setVisible(totalCalculations == 0);
		potentialEnergyRad.setVisible(sumPotentialEnergyCalculations == totalCalculations && totalCalculations != 0);
		freeEnergyRad.setVisible(sumFreeEnergyCalculations == totalCalculations && totalCalculations != 0);		
		zeroPointEnergyRad.setVisible(sumZeroPointEnergyCalculations == totalCalculations && totalCalculations != 0);
		enthalpyEnergyRad.setVisible(sumEnthalpyEnergyCalculations == totalCalculations && totalCalculations != 0);
		bondingEnergyRad.setVisible(sumBondingEnergyCalculations == totalCalculations && totalCalculations != 0);
		nucRepulsionEnergyRad.setVisible(sumNucRepEnergyCalculations == totalCalculations && totalCalculations != 0);
		freeCorrectedBtn.setVisible(hasValidFieldsForEnergyCorrection);	
	}
	
	private boolean readTemperatureAndPressure() throws Exception {	
		Templates template = loadedXsltTemplates.get(TEMPERATURE_PRESSION_XSLT_TEMPLATE_PATH);
		Transformer transformer = template.newTransformer();		
		for(ReportCalculation calculation : report.getCalculations()){
			String fileContent = getCalculationContent(calculation.getCalcPath());
			StreamSource xml = new StreamSource(new StringReader(fileContent));	            	    
       	    StringWriter writer = new StringWriter();
       	    StreamResult result = new StreamResult(writer);                  	    
       	    try{	              
       	    	transformer.transform(xml, result);
       	    	Double readedPression = Double.valueOf(writer.getBuffer().toString().split("\\n")[0].split("\\|")[0]);
       	    	Double readedTemperature = Double.valueOf((writer.getBuffer().toString().split("\\n")[1]).split("\\|")[0]);
       	    	if(pressure < 0)
       	    		pressure = readedPression;
       	    	else if(readedPression != pressure)
       	    		return false;			// Pressures doesn't match in all calculations
       	    	
       	    	if(temperature < 0)
       	    		temperature = readedTemperature;
       	    	else if(readedTemperature != temperature)
       	    		return false;			// Temperatures doesn't match in all calculations       	    	
       	    	       	    	
       	    }catch (TransformerConfigurationException e){
       	    	logger.error(e.getMessage());
       	    	continue;
       	    }catch (TransformerException e){
       	    	logger.error(e.getMessage());
       	    	continue;               
       	    }  
		}	
		return true;
	}

	private MockVariableResolver buildEnergyVariableResolver(String xmlEnergyTags) throws UnsupportedEncodingException, SAXException, IOException, ParserConfigurationException{		
		DecimalFormat df = getEnergyFormatter();
		
		MockVariableResolver resolver = new MockVariableResolver();
		String energyQuery = "//*:template/*:scalar[@name='energy']";
		
		XMLFileManager xml = new XMLFileManager("http://www.xml-cml.org/schema", null, xmlEnergyTags);
		NodeList list = xml.getItemIteratorQuery(energyQuery);
		for(int inx = 1; inx <= list.getLength(); inx++){
			Element energy = (Element)list.item(inx-1);
			Double energyValue = Double.valueOf(energy.getTextContent().trim());
			resolver.addVariable("c" + inx, df.format(energyValue));
		}		
		return resolver;		
	}
			
	private DecimalFormat getEnergyFormatter(){		
		String energyUnits = energyUnitsRdg.getSelectedItem().getValue();
		String formatString = "#.#";		
		switch(energyUnits){
			case "nonsi2:kcal.mol-1":
			case "nonsi:kj.mol-1":		formatString = "#.0";
										break;					
			case "nonsi:electronvolt":	formatString = "#.00";
										break;
			case "nonsi:hartree": 		formatString = "#.000000";
										break;
		}

		DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.ENGLISH);
		symbols.setDecimalSeparator('.');
		DecimalFormat df = new DecimalFormat(formatString, symbols);
		df.setRoundingMode(RoundingMode.HALF_EVEN);	
		return df;
	}

	private boolean formFieldsAreValid(){
		if(energyTypeRdg.getSelectedItem() == null || energyUnitsRdg.getSelectedItem() == null ||
				!energyTypeRdg.getSelectedItem().isVisible() || !energyUnitsRdg.getSelectedItem().isVisible()){
			Messagebox.show("Please select the energy type and units", "Warning", Messagebox.OK , Messagebox.EXCLAMATION);
			return false; 
		}					
		if(fileFormatRgp.getSelectedItem() == null || !fileFormatRgp.getSelectedItem().isVisible()){
			Messagebox.show("Please select a file format", "Warning", Messagebox.OK , Messagebox.EXCLAMATION);
			return false;
		}
		return true;
	}	

	private String buildJavascriptMatrixes(MockVariableResolver resolver) throws Exception{
		DecimalFormat df = getEnergyFormatter();
		Evaluator evaluator = new Evaluator();
		StringBuilder serieNames = new StringBuilder("[");
		StringBuilder deltaEnergies = new StringBuilder("[");		
		StringBuilder valueMatrix = new StringBuilder("[");
		StringBuilder formulaMatrix = new StringBuilder("[");
		
		for(Component serieTab : seriesTabpanels.getChildren()){
			if(serieTab.equals(seriesTabpanels.getFirstChild()))	//Discard + tab
				continue;
			if(isSerieEmpty(serieTab))
				continue;
			Vlayout stepVlayout = (Vlayout) serieTab.query("vlayout.stepLayout");
			HashMap<String, String> userVariables = getUserDefinedVariables(serieTab);
			
			Textbox serieTxt = (Textbox)serieTab.query(".serie-name-txt");
			serieNames.append("\"" + escapeString(serieTxt.getText()) + "\",");
			
			StringBuilder valueRow = new StringBuilder();
			StringBuilder formulaRow = new StringBuilder();
			valueRow.append("[");
			formulaRow.append("[");
			
			ArrayList<Double> deltas = new ArrayList<Double>();
			int valueCount = 0;
			for(Component component: stepVlayout.getChildren()){
				String label = ((Textbox)component.query(".stepLabel")).getValue();
				String formula = ((Textbox)component.query(".stepFormula")).getValue();
				formula = replaceUserVariablesForValue(formula, userVariables);				
				String formulaWithTokens;
				if(formula.trim().equals(""))
					continue;
				if(valueCount> 0){
					valueRow.append(",");
					formulaRow.append(",");
				}							
				formulaRow.append("'" + (label.equals("")?formula:label) + "'");
				formulaWithTokens = formula.replace("C", "c").replaceAll("(c[0-9]+)", "#{$1}");		//Surround cXX with #{ } so JEval library knows that it is a variable
				try {			
					evaluator.setVariableResolver(resolver);
					String result = df.format(Double.valueOf(evaluator.evaluate(formulaWithTokens)));
					Double value = Double.valueOf(result);
					deltas.add(value);					
					valueRow.append(result);
					valueCount++;
				} catch (Exception e) {
					throw new Exception("Error processing formula " + formula);
				}			
			}
			
			valueRow.append("]");
			formulaRow.append("]");
						
			deltaEnergies.append( df.format(calculateDeltaEnergy(deltas)) + ",");			
			if(valueCount > 0){
				valueMatrix.append(valueRow);
				formulaMatrix.append(formulaRow);
			}
		}
		
		serieNames.append("]");
		deltaEnergies.append("]");
		valueMatrix.append("]");		
		formulaMatrix.append("]");
		
		StringBuilder javascriptCode = new StringBuilder();
		javascriptCode.append(" var serieNames = ");
		javascriptCode.append(serieNames.toString().replace(",]","]"));
		javascriptCode.append(";\n\n");
		javascriptCode.append(" var deltaEnergies = ");
		javascriptCode.append(deltaEnergies.toString().replace(",]","]"));
		javascriptCode.append("\n\n");
		javascriptCode.append(" var dataSeries =");
		javascriptCode.append(valueMatrix.toString().replace("][", "],["));
		javascriptCode.append(";\n\n");
		javascriptCode.append(" var stepNames = ");
		javascriptCode.append(formulaMatrix.toString().replace("][", "],["));
		javascriptCode.append(";\n\n");
		return javascriptCode.toString();		
	}

	private boolean isSerieEmpty(Component serieTab) {
		Iterator<Component> iter = serieTab.queryAll(".stepFormula").iterator();
		while(iter.hasNext()) {
			if(!((Textbox)iter.next()).getValue().isEmpty())
				return false;			
		}			
		return true;
	}
	
	private Double calculateDeltaEnergy(ArrayList<Double> deltas){
		Double currentMax = -1.0d;		
		for(int inx = deltas.size()-1; inx >=1 ; inx--){
			for(int inx2 = inx-1; inx2 >= 0; inx2--){
				Double partialMax = deltas.get(inx) - deltas.get(inx2);
				if(partialMax > currentMax)
					currentMax = partialMax;				
			}
		}
		return currentMax;
	}
	
	private String escapeString(String value){
		return value.replaceAll("[\"']+",  "_");
	}
	
	private HashMap<String,String> getUserDefinedVariables(Component serieTab){
		HashMap<String, String> variables = new HashMap<String,String>();	
		Vlayout variableVlayout = (Vlayout) serieTab.query("vlayout.variableLayout");
		for(Component component: variableVlayout.getChildren()){
			String variable= ((Textbox)component.query(".variableName")).getValue();
			String value = ((Textbox)component.query(".variableValue")).getValue();
			if(!value.trim().equals("") && ! variable.trim().equals(""))				
				variables.put(variable, "(" + value + ")");		//Append  ( and ) to variable definition to avoid math processing errors like c9 - c1 + c2 instead of c9 - (c1 + c2)    			
		}		
		return variables;
	}	
	
	private String replaceUserVariablesForValue(String formula, HashMap<String, String> userVariables){
		for(String variable : userVariables.keySet()){
			formula = formula.replaceAll("\\b" + variable +"\\b", userVariables.get(variable));
		}
		return formula;
	}	
}


class MockVariableResolver implements VariableResolver {

	HashMap<String, String> variables = null;
	
	public MockVariableResolver(){
		variables = new HashMap<String, String>();
	}
	
	public void addVariable(String key, String value){
		variables.put(key, value);
	}

	public String resolveVariable(String variableName) throws FunctionException {
		if (variables.containsKey(variableName)) 
			return variables.get(variableName);
		else
			throw new FunctionException("Invalid mock variable name.");
	}
}

class StepTextboxChanged implements EventListener<Event>{
	
	private ReportEnergyReactionProfile report;
	
	public StepTextboxChanged (ReportEnergyReactionProfile report){
		this.report = report;		
	}
	
	@Override
	public void onEvent(Event event) throws Exception {				
		Textbox stepTxt = (Textbox)event.getTarget();		
		Vlayout vlayout = (Vlayout)stepTxt.getAttribute("parentLayout");					
		int numberOfSteps = Selectors.find(vlayout, ".stepFormula").size();
		int serie = (Integer)stepTxt.getAttribute("serie");
		int step =  (Integer)stepTxt.getAttribute("step");		
		if(numberOfSteps == step)
			report.appendNewStep(vlayout, serie, step + 1, null, null);
		report.saveConfiguration();
	}
}

class ValueTextboxChanged implements EventListener<Event>{
	
	private ReportEnergyReactionProfile report;
	
	public ValueTextboxChanged (ReportEnergyReactionProfile report){
		this.report = report;		
	}
	
	@Override
	public void onEvent(Event event) throws Exception {				
		Textbox variableNameTxt = (Textbox)event.getTarget();		
		Vlayout vlayout = (Vlayout)variableNameTxt.getAttribute("parentLayout");					
		int numberOfVariables = Selectors.find(vlayout, ".variableName").size();
		int serie = (Integer)variableNameTxt.getAttribute("serie");
		int variables =  (Integer)variableNameTxt.getAttribute("variable");
		if(numberOfVariables == variables)
			report.appendNewVariable(vlayout, serie, numberOfVariables + 1, null, null);
		report.saveConfiguration();
	}
}

class FormulaDropped implements EventListener<Event> {
	
	private ReportEnergyReactionProfile report;
	
	public FormulaDropped (ReportEnergyReactionProfile report){
		this.report = report;		
	}
	
	@Override
	public void onEvent(Event event) throws Exception {				
		Component dragged = ((DropEvent)event).getDragged();
		Component droped = ((DropEvent)event).getTarget();
		Vlayout parent = (Vlayout)dragged.getParent();				
		int from = parent.getChildren().indexOf(dragged);
		int to = parent.getChildren().indexOf(droped);
		if(from > to)
			droped.getParent().insertBefore(dragged, droped);
		else{
			if(droped.getNextSibling() != null)						
				droped.getParent().insertBefore(dragged, droped.getNextSibling());
			else
				droped.getParent().appendChild(dragged);
		}
		report.saveConfiguration();
	}	
}

class CopyFromPreviousSerie implements EventListener{
	
	Tabpanel currentTabpanel = null;
	public CopyFromPreviousSerie(Tabpanel currentTabpanel){
		this.currentTabpanel = currentTabpanel;
	}
	
	@Override
	public void onEvent(Event event) throws Exception {
		Tabpanel previousTabpanel = getPreviousTabpanel(); 
		copySerie(previousTabpanel, currentTabpanel);
	}
		
	private Tabpanel getPreviousTabpanel(){
		Tabpanels parentTabpanels = (Tabpanels)currentTabpanel.getParent();
		int inx = 2;
		while(inx < parentTabpanels.getChildren().size()){
			if((Tabpanel)parentTabpanels.getChildren().get(inx) == currentTabpanel)		//Find current tabpanel
				break;										
			inx++;
		}		
		return (Tabpanel)parentTabpanels.getChildren().get(inx-1);
	}
	
	private void copySerie(Tabpanel sourceTabpanel, Tabpanel destTabpanel){		
		int numOfSourceSteps = Iterables.size(sourceTabpanel.queryAll(".stepFormula"));		
		int numOfSourceVariables = Iterables.size(sourceTabpanel.queryAll(".variableName"));
		
		int numOfDestSteps = Iterables.size(destTabpanel.queryAll(".stepFormula"));		
		int numOfDestVariables = Iterables.size(destTabpanel.queryAll(".variableName")); 
		
		Iterator<Component> iter = destTabpanel.queryAll("textbox").iterator();
		while(iter.hasNext())
			((Textbox)iter.next()).setValue("");
		
		//((Textbox)destTabpanel.query(".serie-name-txt")).setValue(((Textbox)sourceTabpanel.query(".serie-name-txt")).getValue());
		
		//Add same number of steps and variables (if current serie has less than copied one)
		try{
			for(int inx = numOfDestSteps; inx < numOfSourceSteps; inx++){
				Textbox stepTxt = (Textbox)Iterables.getLast(destTabpanel.queryAll(".stepFormula"));
				((EventListener<Event>)Iterables.getLast(stepTxt.getEventListeners(Events.ON_CHANGE))).onEvent(new Event(Events.ON_CHANGE, stepTxt));									
			}
			for(int inx = numOfDestVariables; inx < numOfSourceVariables; inx++){
				Textbox variableTxt = (Textbox)Iterables.getLast(destTabpanel.queryAll(".variableName"));
				((EventListener<Event>)Iterables.getLast(variableTxt.getEventListeners(Events.ON_CHANGE))).onEvent(new Event(Events.ON_CHANGE, variableTxt));			
			}	
		}catch(Exception e){
			
		}
		
		//Copy values from source to destination 
		try{
			Iterator<Component> iterSourceFormula = sourceTabpanel.queryAll(".stepFormula").iterator();	
			Iterator<Component> iterDestFormula = destTabpanel.queryAll(".stepFormula").iterator();
			while(iterSourceFormula.hasNext())
				((Textbox)iterDestFormula.next()).setValue(((Textbox)iterSourceFormula.next()).getValue());		
			iterSourceFormula = sourceTabpanel.queryAll(".stepLabel").iterator();
			iterDestFormula = destTabpanel.queryAll(".stepLabel").iterator();
			while(iterSourceFormula.hasNext())
				((Textbox)iterDestFormula.next()).setValue(((Textbox)iterSourceFormula.next()).getValue());	
		}catch(Exception e){
			System.out.println(e.getMessage());
		}
		
		try{
			Iterator<Component> iterSourceVariable = sourceTabpanel.queryAll(".variableName").iterator();
			Iterator<Component> iterDestVariable = destTabpanel.queryAll(".variableName").iterator();
			while(iterSourceVariable.hasNext())
				((Textbox)iterDestVariable.next()).setValue(((Textbox)iterSourceVariable.next()).getValue());
			iterSourceVariable = sourceTabpanel.queryAll(".variableValue").iterator();
			iterDestVariable = destTabpanel.queryAll(".variableValue").iterator();
			while(iterSourceVariable.hasNext())
				((Textbox)iterDestVariable.next()).setValue(((Textbox)iterSourceVariable.next()).getValue());
			
		}catch(Exception e){
			
		}
		
	}
}

