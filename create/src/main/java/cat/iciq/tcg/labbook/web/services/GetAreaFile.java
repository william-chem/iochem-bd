/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.core.Services;

/**
 * Servlet implementation class CPro
 */
public class GetAreaFile extends Services {
	private static final long serialVersionUID = 1L;    
	
    /**
     * @see Services#Services()
     */
    public GetAreaFile() {
        super();       
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String, String> params,String userName,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
    	String path = Paths.isAbsolute(params.get("newPath")) ?
				params.get("newPath") :
				Paths.getFullPath(params.get("path"), params.get("newPath"));
    	
    	Calculation calculation = CalculationService.getByPath(path);
    	if(calculation == null) {
			returnKO(ostream,CommandsMessages.CALC_NON_EXIST_ERROR.TITLE(),CommandsMessages.CALC_NON_EXIST_ERROR.MESSAGE());
			return;
		}							
		if(!PermissionService.hasPermissionOnCalculation(calculation.getId(), Permissions.READ)) {
			returnKO(ostream,CommandsMessages.CALC_READ_ERROR.TITLE(),CommandsMessages.CALC_READ_ERROR.MESSAGE());
			return;
		}    		
    			
		List<CalculationFile> files = CalculationService.getCalculationFiles(calculation.getId());
		String calcFile	= params.get("calcFile").replaceAll("assetstore/[0-9]+/", "");			
		try {
			for(CalculationFile file: files) {
				if(file.getName().equals(calcFile)) {
					sendFile(FileService.getCreatePath("/" + file.getFile()), ostream);
					break;
				}
			}											
		} catch(IOException e) {
		    returnKO(ostream, "title", "File exceeds automated download size. Please download current file using the web interface.");
		} catch (Exception e) {
		    returnKO(ostream,"title","Could not retrieve calculation file.");				
		}
    }  
}
