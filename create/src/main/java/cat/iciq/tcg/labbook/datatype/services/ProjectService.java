/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Queries;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;

public class ProjectService {

	private static final Logger log = LogManager.getLogger(ProjectService.class);

	public static boolean projectExists(String path, String name) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.existsProjectWithPathAndName);
			psql.setString(1, path);
			psql.setString(2, name);
			rs = psql.executeQuery();
			rs.next();
			return rs.getInt(1) > 0;
		} catch (Exception e) {
			throw e;
		} finally {
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}
	}
	
	public static long add(Project project) throws Exception {
		if(!PermissionService.hasPermissionOnProject(project.getParentPath(), Permissions.WRITE))
			throw new Exception("Can't create project, invalid path or not enough permissions.");
		if(projectExists(project.getParentPath(), project.getName()))
			throw new Exception("A project with this name already exists.");
		if(project.getParentPath().length()> 512)
			throw new SQLException("Project path exceeds maximum length (256)");

		Connection conn = null;
		PreparedStatement psql = null;
		BigDecimal id = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			id = getNextProjectId(conn);			
			psql = conn.prepareStatement(Queries.CREATE_PROJECT);			
			psql.setBigDecimal(1, id);
			psql.setString(2, project.getName());
			psql.setString(3, project.getParentPath());
			psql.setString(4, project.getDescription());
			psql.setInt(5, project.getOwner());
			psql.setInt(6, project.getGroup());
			psql.setString(7, project.getConceptGroup());
			psql.setInt(8, OrderService.getNewProjectOrderIndex(project.getParentPath()));	
			psql.setString(9, project.getPermissions());
			psql.executeUpdate();		
			TreeEvent.sendEventToUserQueue(new Event("projectAdded", null, id.toString()));
			return id.longValueExact();
		} catch (Exception e) {
			throw e;
		} finally {
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}
		
	}
	
	private synchronized static BigDecimal getNextProjectId(Connection conn) throws Exception {				
		PreparedStatement psql = null;
		ResultSet res = null;		
		try {			
			psql = conn.prepareStatement(Queries.GET_NEXT_PROJECT_ID);
			res = psql.executeQuery();
			res.next();
			return new BigDecimal(res.getInt(1));				
		}catch(Exception e) {
			log.error(e.getMessage());
			throw e;
		} finally{
			DatabaseHandler.closeStatementResult(psql, res);			
		}
	}

	public static Project getById(int projectId) {
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet res = null;
		Project project = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_PROJECT_BY_ID);
			psql.setInt(1, projectId);
			res = psql.executeQuery();
			if (res.next()) {
				project = readProjectFromResultSet(res);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, res);
			DatabaseHandler.closeConnection(conn);
		}
		return project;
	}

	public static Project getByPath(String projectPath) {
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet res = null;
		Project project = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_PROJECT_BY_PATH);
			psql.setString(1, Paths.getParent(projectPath));
			psql.setString(2, Paths.getTail(projectPath));
			res = psql.executeQuery();
			if (res.next())
				project = readProjectFromResultSet(res);
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, res);
			DatabaseHandler.closeConnection(conn);
		}
		return project;
	}

	public static List<Project> getChildProjects(String parentPath) {
		return getChildProjects(parentPath, true);
	}

	public static List<Project> getChildProjects(String parentPath, boolean ordered) {
		ArrayList<Project> children = new ArrayList<Project>();
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet res = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(
					Queries.GET_CHILD_PROJECTS_BY_PARENT_PATH + (ordered ? " order by element_order" : ""));
			psql.setString(1, parentPath);
			res = psql.executeQuery();
			while (res.next())
				children.add(readProjectFromResultSet(res));
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, res);
			DatabaseHandler.closeConnection(conn);
		}
		return children;
	}

	public static List<Calculation> getChildCalculations(String parentPath) {
		return getChildCalculations(parentPath, true);
	}

	public static List<Calculation> getChildCalculations(String parentPath, boolean ordered) {
		ArrayList<Calculation> children = new ArrayList<Calculation>();
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet res = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(
					Queries.GET_CHILD_CALCULATIONS_BY_PARENT_PATH + (ordered ? " order by element_order" : ""));
			psql.setString(1, parentPath);
			res = psql.executeQuery();
			while (res.next()) {
				children.add(CalculationService.readCalculationFromResultSet(res));
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, res);
			DatabaseHandler.closeConnection(conn);
		}
		return children;
	}

	public static List<Integer> getAllChildCalculationsIds(String parentPath) {
		ArrayList<Integer> values = new ArrayList<Integer>();
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet res = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_PROJECT_ALL_CHILD_CALCULATIONS_IDS);
			psql.setString(1, parentPath);			//Same level calculations path
	    	psql.setString(2, parentPath + "/%");	//Subproject calculations path
	    	res = psql.executeQuery();
	    	while(res.next())
	    		values.add(res.getInt(1));
		} catch (Exception e) {
			log.error("Exception raised while deleting projects from DPro. " + e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, res);
			DatabaseHandler.closeConnection(conn);
		}
		return values;
	}

	protected static Project readProjectFromResultSet(ResultSet res) throws SQLException {
		Project project = new Project();
		project.setId(res.getBigDecimal("id").intValue());
		project.setPermissions(res.getString("permissions"));
		project.setOwner(res.getInt("owner_user_id"));
		project.setGroup(res.getInt("owner_group_id"));
		project.setCreationDate(res.getTimestamp("creation_time"));
		project.setConceptGroup(res.getString("concept_group"));
		project.setName(res.getString("name"));
		project.setParentPath(res.getString("path"));
		project.setDescription(res.getString("description"));
		project.setModificationDate(res.getTimestamp("modification_time"));
		project.setCertificationDate(res.getTimestamp("certification_time"));
		project.setPublished(res.getBoolean("published"));
		project.setHandle(res.getString("handle"));
		project.setPublished_name(res.getString("published_name"));
		project.setPublicationDate(res.getTimestamp("publication_time"));
		project.setState(res.getString("state"));
		project.setElementOrder(res.getInt("element_order"));
		return project;
	}

	public static void deleteProject(int id) throws Exception {
		deleteProject(id, true);
	}
 
	public static void deleteProject(int id, boolean checkPermissions) throws Exception {
		if(checkPermissions && !PermissionService.hasPermissionOnProject(id, Permissions.DELETE))
			throw new Exception("Not enough permissions to delete project.");
		Project project = ProjectService.getById(id);				
		deleteChildCalculations(project.getPath());
		deleteChildProjectsAndItself(project);
		//Reduce sibling projects order on database
		OrderService.reduceProjectSiblingsOrder(project.getElementOrder(), project.getParentPath());	
		TreeEvent.sendEventToUserQueue(new Event("projectDeleted", null, project.getPath()));
    }

	private static void deleteChildCalculations(String projectPath) {
		List<Calculation> calculations = ProjectService.getChildCalculations(projectPath);
		for(Calculation calculation : calculations) { 
			try {
				CalculationService.deleteCalculation(calculation.getId(), false);	//Delete without checking permissions, we have already done this on root project
			}catch(Exception e) {
				log.error("Error raised while removing calculation: " + calculation.toString());
			}
		}
	}
	
	private static void deleteChildProjectsAndItself(Project project) {		
		List<Project> children = ProjectService.getChildProjects(project.getPath());
		Connection conn = null;
		PreparedStatement psql = null;
		//First remove all its children projects
		for(Project child: children) {
			try {
				ProjectService.deleteProject(child.getId(), false);
			}catch(Exception e) {
				log.error("Error raised while removing project: " + child.toString());
			}
		}		
		//Then remove itself
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.DELETE_PROJECT_BY_ID);
			psql.setInt(1, project.getId());
			psql.executeUpdate();			
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}			
	}
	
	public static void moveOverProject(Project source, Project destination) throws BrowseCredentialsException, Exception {		
		update(source, destination.getPath());
	}
	
	public static void update(Project modifiedProject) throws BrowseCredentialsException, Exception {
		update(modifiedProject, modifiedProject.getParentPath());
	}

	public static void update(Project modifiedProject, String newParentPath) throws BrowseCredentialsException, Exception {		
		String newPath = Paths.getFullPath(newParentPath, modifiedProject.getName()); 
		
		boolean pathUpdated = false;
		Project originalProject = getById(modifiedProject.getId());
		if(!isValidPath(newPath) || !PermissionService.hasPermissionOnProject(modifiedProject.getParentPath(), Permissions.WRITE))			
			throw new Exception("Can't move project, not a valid destination path");					
		
		Connection conn = null;
		PreparedStatement psql = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			conn.setAutoCommit(false);
			//Update project
			psql = conn.prepareStatement(Queries.UPDATE_PROJECT_BY_ID);
			psql.setInt(1, modifiedProject.getOwner());
			psql.setInt(2, modifiedProject.getGroup());
			psql.setString(3, modifiedProject.getDescription());
			psql.setString(4, modifiedProject.getName());
			psql.setString(5, newParentPath);
			psql.setString(6, modifiedProject.getConceptGroup());
			psql.setString(7, modifiedProject.getPermissions());			
			psql.setInt(8, modifiedProject.getId());			
			psql.executeUpdate();
			psql.close();
			//If path or name is modified, then its children must be also updated 
			if(!modifiedProject.getName().equals(originalProject.getName()) ||
					!newParentPath.equals(originalProject.getParentPath())) {
				//Update child projects
				psql = conn.prepareStatement(Queries.UPDATE_PROJECT_PATHS);
				String escapedPath = escape(originalProject.getPath());
				psql.setString(1, "^" + escapedPath);
				psql.setString(2, newPath);
				psql.setString(3, originalProject.getPath() + "/%");
				psql.setString(4, originalProject.getPath());
				psql.executeUpdate();
				psql.close();
				//Update child calculations
				psql = conn.prepareStatement(Queries.UPDATE_CALCULATION_PATHS);
				psql.setString(1, "^" + escapedPath);
				psql.setString(2, newPath);
				psql.setString(3, originalProject.getPath() + "/%");
				psql.setString(4, originalProject.getPath());
				psql.executeUpdate();
				psql.close();
				pathUpdated = true; 			
			}
			conn.commit();			
			//Send modification events to the rest of the UI components 
			HashMap<String, Object> params = new HashMap<>();			
			params.put("id", Integer.toString(originalProject.getId()));
			params.put("oldPath", originalProject.getPath());
			params.put("newPath", newPath);
			TreeEvent.sendEventToUserQueue(new Event("projectModified", null, params));
			//Reorder element according to the new path
			if(pathUpdated)
				OrderService.moveProject(modifiedProject.getId(), originalProject.getParentPath(), newParentPath);
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally {
			DatabaseHandler.closeStatementResult(psql, null);
    		DatabaseHandler.resetAutocommit(conn);
			DatabaseHandler.closeConnection(conn);
		}	
	}
	
	private static boolean isValidPath(String destinationPath) throws BrowseCredentialsException {				
		return destinationPath.startsWith(GeneralConstants.DB_ROOT + ShiroManager.getCurrent().getUserName()) ;
	}
		
	private static String escape(String inString) {
	    StringBuilder builder = new StringBuilder(inString.length() * 2);
	    String toBeEscaped = "\\{}()[]*+?.|^$";
	    for (int i = 0; i < inString.length(); i++){
	        char c = inString.charAt(i);
	        if (toBeEscaped.contains(Character.toString(c))){
	            builder.append('\\');
	        }
	        builder.append(c);
	    }
	    return builder.toString();
	}

	public static void publishProject(Project project) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;
		try {
			conn = DatabaseHandler.getCreateConnection();						
			psql = conn.prepareStatement(Queries.PUBLISH_PROJECT);
			psql.setString(1, project.getHandle());
			psql.setString(2, project.getPublished_name());		
			psql.setInt(3, project.getId());		
			psql.executeUpdate();		
		}catch(Exception e) {
			throw e;
		}finally {
			DatabaseHandler.closeStatementResult(psql, null);
    		DatabaseHandler.resetAutocommit(conn);
    		DatabaseHandler.closeConnection(conn);
		}
	}

    public static void cascadePermissionsToChildren(Project project) throws Exception {
        Connection conn = null;
        PreparedStatement psql = null;
        try {
            conn = DatabaseHandler.getCreateConnection();                       
            psql = conn.prepareStatement(Queries.UPDATE_SUBPROJECTS_PERMISSIONS);            
            psql.setInt(1, project.getGroup());
            psql.setString(2, project.getPermissions());
            
            psql.setString(3, project.getPath());
            psql.setString(4, Paths.getFullPath(project.getPath(),"%"));            
            
            psql.executeUpdate();       
        }catch(Exception e) {
            throw e;
        }finally {
            DatabaseHandler.closeStatementResult(psql, null);
            DatabaseHandler.resetAutocommit(conn);
            DatabaseHandler.closeConnection(conn);
        }   
        
    }
}
