/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.services.AssetstoreService;
import cat.iciq.tcg.labbook.datatype.services.CalculationService;
import cat.iciq.tcg.labbook.datatype.services.FileService;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.web.core.Services;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;


public class Molden extends Services {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(Molden.class.getName());
	
	private static final String TEXT_MIMETYPE = "text/plain";
	private static final String JSON_MIMETYPE = "application/json";	
	private static final Pattern symmPattern = Pattern.compile("\\s*Sym=\\s*(\\S*)\\s*");

	private static final String ID_PARAMETER = "id";
	private static final String FILE_PARAMETER = "file";
	private static final String LABEL_PARAMETER = "label";
	private static final String INDEX_PARAMETER = "index";
	
	public Molden(){
		super();
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		Connection conn = null;	
		OutputStream ostream = response.getOutputStream();
	   	PreparedStatement psql = null;
    	ResultSet res = null;      	
    	try{
			conn =  DatabaseHandler.getCreateConnection();		
			try{
				int calculationId = Integer.parseInt(request.getParameter(ID_PARAMETER));
				if(isUserAllowedToAccessFile(calculationId)){
					String moldenfile = request.getParameter(FILE_PARAMETER);
					String label = request.getParameter(LABEL_PARAMETER);								
					int index = request.getParameterMap().containsKey(INDEX_PARAMETER)? Integer.parseInt(request.getParameter(INDEX_PARAMETER)):-1;
					String content = "";
					
					if(moldenfile == null) {
						content = new Gson().toJson(getMoldenFilesInfo(calculationId));
						response.setContentType(JSON_MIMETYPE + ";charset=UTF-8");
						response.setContentLength(content.length());
					}else {						
						content = getMoldenFile(calculationId, moldenfile, label, index);
						content = content.replace("MOLDEN FORMAT", "Molden Format");	 //Action performed to avoid a bug of Jsmol that fails to load molden orbital files with its header in capital letters
						response.setContentType(TEXT_MIMETYPE  + ";charset=UTF-8");
					    response.setContentLength(content.length());
					}
					ostream.write(content.getBytes(Charset.forName("UTF-8")));							
				}else{
					response.sendError(HttpServletResponse.SC_FORBIDDEN);
				}
			}catch(Exception e) {
				response.sendError(HttpServletResponse.SC_BAD_REQUEST);				
			}finally{			
				DatabaseHandler.closeStatementResult(psql, res);
			}		
		}
		catch (Exception e)
		{
			logger.error(e.getMessage() + getStackTrace(e));		
			returnSystemError(SERVER_ERROR,e,ostream);
		}
		finally
		{
			try {		
				if (ostream != null) 	ostream.close();
				if (conn 	!= null) 	conn.close();				
			}
			catch (Exception e)
			{
				logger.error(e.getMessage() + getStackTrace(e));			
				returnSystemError(SERVER_ERROR,e,ostream);
			}
		}
	}
		
	private List<MoldenFile> getMoldenFilesInfo(int calcId) throws Exception {
		ArrayList<MoldenFile> moldenFiles = new ArrayList<MoldenFile>();
		Calculation calc = CalculationService.getById(calcId);
		XMLFileManager metsFileManager = new XMLFileManager(MetsFileHandler.METS_NAMESPACE, MetsFileHandler.METS_ADDITIONAL_NAMESPACES, calc.getMetsXml());
		NodeList orbitalFiles = metsFileManager.getItemIteratorQuery(XpathQueries.GET_ORBITAL_FILE_ELEMENTS);
		
		for(int inx = 0; inx < orbitalFiles.getLength(); inx++) {
			Element element = (Element)orbitalFiles.item(inx);
			String filename = element.getAttribute("xlink:href");			
			ArrayList<String> headers = getOrbitalHeaders(calcId, filename);
						
			MoldenFile molden = new MoldenFile();
			molden.setFilename(filename);
			molden.setOrbitals(headers);
			moldenFiles.add(molden);			
		}		
		return moldenFiles;
	}
	
	private String getMoldenFile(int calcId, String moldenFile, String label, int index) throws Exception {
		if(label != null && !label.equals("")) 
			index = getOrbitalIndex(calcId, moldenFile, label);
		
		return 	buildMoldenOrbitalFile(calcId, moldenFile, index);			
	}

	private static ArrayList<String> getOrbitalHeaders(int calcId, String moldenfile) throws Exception{
		ArrayList<String> orbitalHeaders = new ArrayList<String>();		
		BufferedReader bf = AssetstoreService.getCalculationFileReaderByName(calcId, moldenfile);
		
		String line = null;		
		int spinAlpha = 0;
		int spinBeta = 0;
		while( (line = bf.readLine()) != null){
			if(symmPattern.matcher(line).matches()){
				String symmetry = line.replaceAll("\\s*Sym=\\s*","");				
				String energy = bf.readLine().replaceAll("\\s*Ene=\\s*", "");				
				String spin = bf.readLine().replaceAll("\\s*Spin=\\s*", "");
				String occup = bf.readLine().replaceAll("\\s*Occup=\\s*", "");
				occup = formatDouble(occup);
				energy = formatDouble(energy);
				orbitalHeaders.add(String.format("%1$-4s %2$-6s %3$-4s %4$11s %5$11s", 
						spin.equals("Alpha")? spinAlpha++ : spinBeta++ , 
								spin, 
								symmetry, 
								occup, 
								energy));
			}
		}			
		bf.close();		
		return orbitalHeaders;
	}

	private int getOrbitalIndex(int calculationId, String moldenfile, String label) throws Exception {	
		String path = FileService.getCalculationPath(calculationId, moldenfile);
		String line = null;
		BufferedReader bf = null;
		int spinAlpha = 0;
		int spinBeta = 0;
		int index = 0;		
		try {
			
			int labelIndex = Integer.parseInt(label.replaceAll("(?![0-9]+).*", ""));
			String labelSymmetry = label.replaceAll("^[0-9]+", "").toUpperCase();		
			
			
			bf = new BufferedReader(new FileReader(path));		
			while((line = bf.readLine()) != null){
				if(symmPattern.matcher(line).matches()){					
					String symmetry = line.replaceAll("\\s*Sym=\\s*","").trim().toUpperCase();				
					bf.readLine();				
					String spin = bf.readLine().replaceAll("\\s*Spin=\\s*", "").trim().toUpperCase();
					// Exact match by symmetry label
					if(spin.equals("ALPHA")) {
						if(symmetry.equals(label.toUpperCase()))
							return spinAlpha + spinBeta + 1;
						spinAlpha++;						
					} else {
						if(symmetry.equals(label.toUpperCase()))
							return spinAlpha + spinBeta + 1;
						spinBeta++;
					}	
					
					// Alternative match by current index and label, starts on 0
					try {
						symmetry = symmetry.replaceAll("^[0-9]+", "");
						if(!labelSymmetry.equals("")) {
							if(labelSymmetry.startsWith("A") && spin.equals("ALPHA") && (spinAlpha -1) == labelIndex)
								index = spinAlpha + spinBeta;
							else if(labelSymmetry.startsWith("B") && spin.equals("BETA") && (spinBeta -1) == labelIndex)
								index = spinAlpha + spinBeta;																					
						}
					}catch(Exception e) {
						index = 0;
					}
				}
			}				
		}catch(IOException e) {
			
		}finally {
			try {
				bf.close();
			} catch (IOException e) {
				
			}	
		}
		return index;
	}	
	
	public static List<String> buildOrbitalHeadersList(int calculationId, String moldenfile) throws Exception {
		List<String> headers = new ArrayList<String>();
		String path = FileService.getCalculationPath(calculationId, moldenfile);		
		String line = null;
		BufferedReader bf = null;
		int spinAlpha = 0;
		int spinBeta = 0;		
		try {
			bf = new BufferedReader(new FileReader(path));		
			while( (line = bf.readLine()) != null){
				if(symmPattern.matcher(line).matches()){
					String symmetry = line.replaceAll("\\s*Sym=\\s*","");				
					String energy = bf.readLine().replaceAll("\\s*Ene=\\s*", "");				
					String spin = bf.readLine().replaceAll("\\s*Spin=\\s*", "");
					String occup = bf.readLine().replaceAll("\\s*Occup=\\s*", "");
					occup = formatDouble(occup);
					energy = formatDouble(energy);
					if(spin.equals("Alpha"))
						headers.add(String.format("%1$-4s %2$-6s %3$-4s %4$11s %5$11s",spinAlpha++, spin, symmetry, occup, energy));
					else
						headers.add(String.format("%1$-4s %2$-6s %3$-4s %4$11s %5$11s",spinBeta++, spin, symmetry, occup, energy));
				}			
			}				
		}catch(IOException e) {
			
		}finally {
			try {
				bf.close();
			} catch (IOException e) {
				
			}	
		}
		return headers;
	}


	private static String formatDouble(String val){
		try{
			double d = Double.parseDouble(val);					
			DecimalFormat df = new DecimalFormat("#0.000");
			return df.format(d);				
		}catch(Exception e){
			logger.error("Exception raised while formatting orbital values from :" + val);
			return val;
		}			
	}
	
	private String buildMoldenOrbitalFile(int calculationId, String moldenfile, int index) throws Exception{
		BufferedReader bf = AssetstoreService.getCalculationFileReaderByName(calculationId, moldenfile);
		StringBuilder sb = new StringBuilder();

		String line = null;		
		int inx = 0;
		while((line = bf.readLine()) != null && !line.contains("[MO]")){ //Copy content until we get to molecular orbital section 
			sb.append(line + "\n");
		}
		sb.append(line + "\n");
		while( (line = bf.readLine()) != null && !line.startsWith("[")){  //Discard other sections
			if(line.toUpperCase().contains("SYM=") && (++inx == index)){			
				do{
					sb.append(line + "\n");					
					line = bf.readLine();
				}while(line != null && !line.startsWith("[") && !line.toUpperCase().contains("SYM="));
				break;
			}		
		}			
		bf.close();		
		return sb.toString();		
	}

	@Override
	public void executeService(Connection conn, OutputStream ostream,
		HashMap<String, String> params, String userName, Integer userId,
		PreparedStatement psql, ResultSet res) throws SQLException,
		IOException, BrowseCredentialsException {		
	}	
}


class MoldenFile {
	String filename;
	Collection<String> orbitals;
	
	public MoldenFile() {
		filename = "";
		orbitals = new ArrayList<String>();		
	}
	
	public MoldenFile(String filename, Collection<String> orbitals) {
		this.filename = filename;
		this.orbitals = orbitals;		
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public Collection<String> getOrbitals() {
		return orbitals;
	}
	
	public void setOrbitals(Collection<String> orbitals) {
		this.orbitals = orbitals;
	}

	public void addOrbital(String orbital) {
		this.orbitals.add(orbital);
	}	
}
