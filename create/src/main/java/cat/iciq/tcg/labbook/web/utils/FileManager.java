/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;

import cat.iciq.tcg.labbook.shell.utils.Paths;

public class FileManager {
	
	public static String generateFile(String filePath, String content) throws IOException 
	{
		new File(filePath).delete();
		BufferedWriter out = new BufferedWriter(new FileWriter(filePath));
		out.write(content);		
		out.close();
		return filePath;
	}	
	
	public static void deleteFile(String filePath) throws IOException
	{
		if(!new File(filePath).delete())
			throw new IOException("There has been an error removing " + filePath + " from filesystem.");
	}
}
