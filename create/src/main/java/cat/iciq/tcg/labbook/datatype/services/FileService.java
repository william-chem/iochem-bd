/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class FileService {

	/**
	 * Function that returns the full path of the project root  
	 * @return
	 */
	public static String getBasePath() throws Exception {
		String path = System.getenv(Constants.IOCHEMBD_DIR_SYS_VAR);
		if(path == null || path.isEmpty())
			throw new Exception("Undefined IOCHEMBD_DIR system variable.");		
		return path;		
	}
	
	/**
	 * Helper function to get the full path of a file/folder inside the project root.
	 * @param endPath Relative path to file/folder inside project root folder, provided path must start with file separator
	 * @return String with the full path in the file system to the file
	 * @throws Exception if endPath doesn't start with the file separator char
	 */	
	public static String getBasePath(String endPath) throws Exception {		
		if(endPath == null)
			throw new Exception("Invalid path provided.");
		
		if(endPath.startsWith("" + File.separatorChar) || endPath.equals(""))
			return  getBasePath() + endPath;
		else
			return getBasePath() + File.separatorChar + endPath;
	}

	/**
	 * Function that returns the full path of the create folder (not the web service).
	 * @return
	 * @throws Exception 
	 */
	public static String getCreatePath() throws Exception {
		String path = System.getenv(Constants.IOCHEMBD_DIR_SYS_VAR);
		if(path == null || path.isEmpty())
			throw new Exception("Undefined IOCHEMBD_DIR system variable.");		
		path += File.separatorChar + Constants.CREATE_DIR;				
		return path;
	}

	/**
	 * Helper function to get the full path of a file/folder inside the create folder (not the web service).
	 * @param endPath Relative path to file/folder inside Create, provided path must start with file separator
	 * @return String with the full path in the file system to the file
	 * @throws Exception if endPath doesn't start with the file separator char 
	 */
	public static String getCreatePath(String endPath) throws Exception {		
		if(endPath == null || !endPath.startsWith("" + File.separatorChar) || endPath.equals("" + File.separatorChar))
			throw new Exception("Invalid path provided");
		return getCreatePath() + endPath; 						
	}

	
	/**
	 * Function that returns the folder path of a calculation inside Create module
	 * @param calculationId Id of the calculation
	 * @return	The full path in the file system to calculation folder
	 * @throws Exception 
	 */
	public static String getCalculationPath(long calculationId) throws Exception {
		if(calculationId < 0)
			throw new Exception("Invalid calculation path parameters");		
		return getCreatePath() + 
				File.separatorChar + Constants.CREATE_ASSETSTORE_DIR +
					File.separatorChar + String.valueOf(calculationId);
	}
	
	/**
	 * Function that returns the path of a calculation file inside Create module 
	 * @param calculationId Id of the calculation
	 * @param filename Name of the file to get its path
	 * @return The full path in the file system to the calculation file
	 * @throws Exception 
	 */
	public static String getCalculationPath(long calculationId, String filename) throws Exception {
		if(calculationId < 0 || filename == null || filename.equals(""))
			throw new Exception("Invalid calculation path parameters");	
		filename = FilenameUtils.getName(filename);
		return getCalculationPath(calculationId) + 
				File.separatorChar + filename;
	}	
	
	/**
	 * Function that builds the path of the Create webapp folder base folder or a file inside  
	 * @param endPath Additional path to append to the generated path, if null or empty it will return the base Create webapp folder. 
	 * @return
	 * @throws Exception 
	 */
	public static String getCreateWebapp(String endPath) throws Exception {
	    StringBuilder sb = new StringBuilder();
	    sb.append(getBasePath());
	    sb.append(File.separatorChar);
	    sb.append(Constants.WEBAPPS_DIR);
	    sb.append(File.separatorChar);
	    sb.append(Constants.CREATE_DIR);
	    
	    if(endPath == null || endPath.equals("")) {
	        return sb.toString();
	    } else if(!endPath.startsWith("" + File.separatorChar) || endPath.equals("" + File.separatorChar)) {
            throw new Exception("Invalid path provided");	    
	    } else {
	        sb.append(endPath);
	        return sb.toString(); 
	    }
	}
	
	/**
	 * Function that creates a temporal folder for the current user inside webapp tmp/ folder
	 * @return A File object that points that specific temporal folder. 
	 * @throws IOException
	 * @throws BrowseCredentialsException
	 */
    public static File createTemporalDirectory() throws IOException, BrowseCredentialsException{
        if(!new File(Main.TMP_ROOT_DIR).exists())
            new File(Main.TMP_ROOT_DIR).mkdir();     
        
        String tmpDir = Main.TMP_ROOT_DIR + ShiroManager.getCurrent().getUserName();
        File dir = new File(tmpDir);
        
        if (!dir.isDirectory()) {
            if (!dir.mkdir()) {
                throw new IOException("Unable to create upload temporal folder.");
            }
        }   
        tmpDir = tmpDir + "/"  + UUID.randomUUID().toString();
        dir = new File(tmpDir);
        
        if (dir.isDirectory()) 
            FileUtils.deleteDirectory(dir);
        if (!dir.mkdir()) 
            throw new IOException("Unable to create upload temporal folder.");
        
        return dir;        
    }	
}