/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.manager.actions;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URL;
import java.util.HashMap;

import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.zkoss.util.media.AMedia;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Iframe;
import org.zkoss.zul.Tab;
import org.zkoss.zul.Tabbox;
import org.zkoss.zul.Tabpanel;
import org.zkoss.zul.Tabpanels;
import org.zkoss.zul.Tabs;

import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.zk.composers.Main;

/**
 * This action class is used to transform CML files into other output formats like 
 * HTML, etc using XSLT transformation sheets.
 * NOTE: Conversion relies on SAXON HE package. Do not update SAXON HE 9.1.0.8 package (this will break current functionality, it's the only free version that allows namespaces on xslt stylesheet pages) 
 * @author malvarez
 *
 */

@SuppressWarnings("unchecked")
public class XsltTransformAction extends Action {
	
	
	//Expandable list of parameters
	private static String TEMPLATE_OPTION						= "template";
	private static String EXTENSION_OPTION						= "extension";
	
	//Saxon static elements
	private static TransformerFactory tFactory 					= null; 		//This object will generate all conversion templates
	private static HashMap<String,Templates> loadedTemplates	= null;			//Templates will be class variables
	
	private static String  optionNames[] 			= {TEMPLATE_OPTION,	EXTENSION_OPTION};
	private static boolean optionArgumentRequired[] = {true,			true};
	private static String  optionDescriptions[]		= {"Template path",	"Output file extension"};
	
	private static final String JCAMPDX_URL	= "/innerServices/jcampdx?id=";
	private static final String XYZ_URL		= "/innerServices/xyz?id=";
	private static final String MOLDEN_URL		= "/innerServices/molden?id=";
	
	private Tabs childrenTabs;
	private Tabpanels childrenTabPanels;
	private boolean hasRenderedContent = false;
		
	static{
		tFactory 		= new net.sf.saxon.TransformerFactoryImpl();
		loadedTemplates = new HashMap<String, Templates>();			
	}
	
	public static void flushXsltTemplates(){
		loadedTemplates = new HashMap<String, Templates>();					
	}
	
	@Override
	public void init() {		
		this.setInternalOrder(2);
		this.setTabID("XsltTransformActionTab");
		this.setTabLabel("View Results");	
		
		tab = new Tab();
	    tab.setId(this.tabID);
	    tab.setLabel(this.tabLabel);	    
	    tab.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener(){
			@Override
			public void onEvent(Event event) throws Exception {		
				actionQueue.publish(new Event("maximizeRequested",null,null));						
			}			
		});
	    tab.addEventListener(Events.ON_CLICK, new EventListener() {
			@Override
			public void onEvent(Event event) throws Exception {
				if(!hasRenderedContent) {
					generateTabs();					
					hasRenderedContent = true;
				}					
			}
	    	
	    });
	    
	    tabpanel = new Tabpanel();	    
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void render() {
		if(filesByFormat.size() == 0)
			return;
		//Lazy load reports
		hasRenderedContent = false;
		tab.setSelected(false);
	    setVisible(true);
	}
	
	private void generateTabs() {
				
		Tabbox tabbox = new Tabbox();
	    tabbox.setId("xsltTransformActionTabbox");
	    tabbox.setHeight("100%");
	    childrenTabs = new Tabs();
	    childrenTabPanels = new Tabpanels();	    
	    tabbox.appendChild(childrenTabs);
	    tabbox.appendChild(childrenTabPanels);
	    
	    int inx = 1;	    	    	   
	    for(String key : filesByFormat.keySet()){
	            for(String fileName : filesByFormat.get(key)){
	            	try{
	            		CommandLineParser parser = new BasicParser();
	            		CommandLine cmd = parser.parse(buildOptions(), parameters.get(key).trim().split("[ \t\r\n]+"));	   

	        			String xsltPath 	= cmd.getOptionValue(TEMPLATE_OPTION);
						String realFilePath = findFileRealPathByName(fileName);
						int calculationId = Integer.parseInt(metsFileManager.getStringFromQuery(XpathQueries.GET_CALCULATION_INTERNAL_ID));
						
						//Add new tab 
						Tab childrenTab = new Tab();
	            		childrenTab.setId("xsltTransformActionTabboxTab" + inx);
	            		childrenTab.setLabel(fileName.concat(cmd.getOptionValue(EXTENSION_OPTION)));
	            		childrenTab.addEventListener(Events.ON_DOUBLE_CLICK, new EventListener(){
	            			@Override
	            			public void onEvent(Event event) throws Exception {		
	            				actionQueue.publish(new Event("maximizeRequested",null,null));						
	            			}	            				            			
	            		});

	            		Tabpanel childrenTabPanel = new Tabpanel();
	            		childrenTabPanel.setId("xsltTransformActionTabboxTabPanel" + inx);
	            		Iframe iframe = new Iframe();		            		
	            		iframe.setWidth("100%");
	            		iframe.setHeight("100%");
	            		iframe.setStyle("scrolling:yes");

	            		StreamSource xml = new StreamSource(new File(realFilePath));	            	    
	            	    StringWriter writer = new StringWriter();
	                    StreamResult result = new StreamResult(writer);	                    	 
	                    Transformer transformer = null;
	                    try
	                    {	   	                 			                    	
	                        transformer = getTemplate(xsltPath).newTransformer();	
	                        Execution exec = Executions.getCurrent();
	                    	URL reconstructedURL = new URL(exec.getScheme(), exec.getServerName(), exec.getServerPort(), exec.getContextPath());
	                        transformer.setParameter("webrootpath", reconstructedURL.toString() + "/html");	                        	                        
	                    	if(calculation.isPublished())
	                    		transformer.setParameter("title", calculation.getPublished_name());
	                    	else 
	                    		transformer.setParameter("title",calculation.getName());
	                    	if(hasOrbitalFiles())
	                        	transformer.setParameter("moldenurl", reconstructedURL.toString() + MOLDEN_URL + calculationId);
	                    	
	                        transformer.setParameter("jcampdxurl", reconstructedURL.toString() + JCAMPDX_URL + calculationId );
	                        transformer.setParameter("xyzurl", reconstructedURL.toString() + XYZ_URL + calculationId);	                        	                       
	                        transformer.transform(xml, result);
	                        
	                        AMedia media = new AMedia(fileName.concat(cmd.getOptionValue(EXTENSION_OPTION)), cmd.getOptionValue(EXTENSION_OPTION).replace(".", ""), "text/html", writer.toString());
	                        iframe.setContent(media);	                        
	                    }
	                    catch (TransformerConfigurationException e)
	                    {
	                        continue;
	                    }
	                    catch (TransformerException e)
	                    {
	                        continue;               
	                    }  
	            		childrenTabPanel.appendChild(iframe);
	            		childrenTabs.appendChild(childrenTab);
	            		childrenTabPanels.appendChild(childrenTabPanel);
					} catch (IOException e) {						
						continue;
					} catch (Exception e) {
						continue;
					}
	                 inx++;
	            }	            
	    }		    
	    tabpanel.appendChild(tabbox);	    
	    tabpanel.invalidate();	    
	    //Add new tab classes	    
	    Clients.evalJavaScript("addBootstrapClasses(" + (isSmallLayout()? "'small'" : "'large'") + ")");	    
	}
	 
	@Override
	public void resizeContent(boolean maximize) {
		// TODO Auto-generated method stub
		
	}
	
	private Options buildOptions(){		
		Options options = new Options();				
		for(int i = 0; i< optionNames.length; i++){
			Option option = new Option(optionNames[i], optionArgumentRequired[i], optionDescriptions[i]);
			options.addOption(option);
		}
		return options;		
	}
	
	private Templates getTemplate(String templatePath) throws TransformerConfigurationException{		
		if(!loadedTemplates.containsKey(templatePath)){	
			StreamSource xslSource = new StreamSource(new File(Sessions.getCurrent().getWebApp().getRealPath(templatePath)));
			loadedTemplates.put(templatePath, tFactory.newTemplates(xslSource));
		}
		return loadedTemplates.get(templatePath);
	}

	private boolean hasOrbitalFiles(){
		String hasOrbitals = this.metsFileManager.getStringFromQuery(XpathQueries.HAS_MOLECULAR_ORBITALS_FILE);
		return Boolean.valueOf(hasOrbitals);
	}
	
	@Override
	void resetZkControls() {
		while(!tabpanel.getChildren().isEmpty())
			tabpanel.getFirstChild().detach();		
	}
}
