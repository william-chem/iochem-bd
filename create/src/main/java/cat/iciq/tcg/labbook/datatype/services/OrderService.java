/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;

public class OrderService {
	
	private static final int PROJECT_ORDER_START_INDEX = 1;
	private static final int CALCULATION_ORDER_START_INDEX = 10000;
	
	private static final Logger log = LogManager.getLogger(OrderService.class);
	
	public static void moveProject(int projectId, String oldParentPath, String newParentPath) {
		Connection conn = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			Project project = ProjectService.getById(projectId);
			int order = project.getElementOrder();						
			reduceProjectSiblingsOrder(order, oldParentPath);			
			int newOrder = getLastProjectOrderNumberExcludingId(conn, newParentPath, projectId) + 1;
			setProjectOrder(conn, project.getId(), newOrder);			
			sendMoveProjectNavigationEvent(projectId, order, newOrder, oldParentPath, newParentPath);
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally{
			DatabaseHandler.closeConnection(conn);
		}
	}

	public static void reduceProjectSiblingsOrder(int projectOrder, String parentProjectPath) {
		Connection conn = null;
		PreparedStatement ps = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			ps = conn.prepareStatement("update projects set element_order = element_order-1 where element_order > ? and path = ?");
			ps.setInt(1, projectOrder);
			ps.setString(2,  parentProjectPath);
			ps.executeUpdate();		
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeStatementResult(ps, null);
			DatabaseHandler.closeConnection(conn);
		}		
	}
	
	private static void sendMoveProjectNavigationEvent(int projectId, int oldOrder, int newOrder, String oldParentPath, String newParentPath) {
		HashMap<String, Object> params = new HashMap<String, Object>();		
		params.put("id", projectId);
		params.put("oldOrder", oldOrder);
		params.put("newOrder", newOrder);
		params.put("oldParentPath", oldParentPath);
		params.put("newParentPath", newParentPath);
		TreeEvent.sendEventToUserQueue(new Event("projectReorder", null, params));
	}
	
	private static void setProjectOrder(Connection conn, int projectId, int order) {
		PreparedStatement ps = null; 		
		try {
			ps = conn.prepareStatement("update projects set element_order = ? where id = ?");
			ps.setInt(1, order);
			ps.setInt(2, projectId);
			ps.executeUpdate();			
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeStatementResult(ps, null);
		}		
	}

	public static int getNewProjectOrderIndex(String parentPath) {
		Connection conn = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			return getLastProjectOrderNumber(conn, parentPath) + 1;
		}catch (Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeConnection(conn);
		}
		return PROJECT_ORDER_START_INDEX;
	}
	
	private static int getLastProjectOrderNumber(Connection conn, String projectPath) {
		PreparedStatement ps = null; 
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("select coalesce(max(element_order), " + (PROJECT_ORDER_START_INDEX - 1) + ") from projects where path = ?");
			ps.setString(1, projectPath);
			rs = ps.executeQuery();
			if(rs.next())
				return rs.getInt(1); 
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {			
			DatabaseHandler.closeStatementResult(ps, rs);			
		}
		return PROJECT_ORDER_START_INDEX-1;
	}
	
	public static int getNewCalculationOrderIndex(String parentPath) {
		Connection conn = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			return getLastCalculationOrderNumber(conn, parentPath) + 1;
		}catch (Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeConnection(conn);
		}
		return CALCULATION_ORDER_START_INDEX;
	}
	
	private static int getLastCalculationOrderNumber(Connection conn, String projectPath) {
		PreparedStatement ps = null; 
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("select coalesce(max(element_order), " + (CALCULATION_ORDER_START_INDEX - 1) + ") from calculations where path = ?");
			ps.setString(1, projectPath);
			rs = ps.executeQuery();
			if(rs.next())
				return rs.getInt(1); 
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {			
			DatabaseHandler.closeStatementResult(ps, rs);			
		}
		return CALCULATION_ORDER_START_INDEX-1;			
	}
	
	private static int getLastProjectOrderNumberExcludingId(Connection conn, String projectPath, int id) {
		PreparedStatement ps = null; 
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("select coalesce(max(element_order), " + (PROJECT_ORDER_START_INDEX - 1) + ") from projects where path = ? and id != ?");
			ps.setString(1, projectPath);
			ps.setInt(2, id);
			rs = ps.executeQuery();
			if(rs.next())
				return rs.getInt(1); 
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {			
			DatabaseHandler.closeStatementResult(ps, rs);			
		}
		return PROJECT_ORDER_START_INDEX-1;
	}
	
	public static void moveCalculation(Calculation calculation, Calculation targetCalculation, String oldParentPath, String newParentPath) {
		int oldOrder = calculation.getElementOrder();
		int newOrder = 0;
		Connection conn = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			if(targetCalculation == null) {		// Dropped from multiple selection, no targetCalculationId => move to last position
				reduceCalculationSiblingsOrder(conn, calculation.getElementOrder(), oldParentPath);
				newOrder = getLastCalculationOrderNumberExcludingId(conn, calculation.getId(), newParentPath) + 1;
				setCalculationOrder(conn, calculation.getId(), newOrder);				
			}else {
				if(oldParentPath.equals(newParentPath)) {	// Dropped on same project, insert before targetCalculation
					moveCalculationWithinProject(conn, oldParentPath, calculation.getElementOrder(), targetCalculation.getElementOrder());									
				}else {										// Dropped on different project, insert before targetCalculation
					reduceCalculationSiblingsOrder(conn, calculation.getElementOrder(), oldParentPath);
					moveCalculationIntoProject(conn, newParentPath, targetCalculation.getElementOrder());
				}
				newOrder = targetCalculation.getElementOrder();
				setCalculationOrder(conn, calculation.getId(), newOrder);
			}
			sendMoveCalculationNavigationEvent(calculation.getId(), targetCalculation.getId(), newOrder, oldOrder, oldParentPath, newParentPath);
		}catch (Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeConnection(conn);
		}				
	}
	
	private static void sendMoveCalculationNavigationEvent(int calculationId, int targetCalculationId,  int newOrder, int oldOrder, String oldParentPath, String newParentPath) {
		HashMap<String, Object> params = new HashMap<String, Object>();		
		params.put("id", calculationId);
		params.put("targetId", targetCalculationId);
		params.put("oldOrder", oldOrder);
		params.put("newOrder", newOrder);
		params.put("oldParentPath", oldParentPath);
		params.put("newParentPath", newParentPath);
		TreeEvent.sendEventToUserQueue(new Event("calculationReorder", null, params));		
	}

	protected static void reduceCalculationSiblingsOrder(Connection conn, int order, String path) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("update calculations set element_order = element_order-1 where element_order > ? and path = ?");
			ps.setInt(1, order);
			ps.setString(2, path);
			ps.executeUpdate();
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeStatementResult(ps, null);
		}
	}

	private static int getLastCalculationOrderNumberExcludingId(Connection conn, int calculationId, String projectPath) {
		PreparedStatement ps = null; 
		ResultSet rs = null;
		try {
			ps = conn.prepareStatement("select coalesce(max(element_order)," + (CALCULATION_ORDER_START_INDEX - 1) + ") from calculations where path = ? and id != ?");
			ps.setString(1, projectPath);
			ps.setInt(2, calculationId);
			rs = ps.executeQuery();
			if(rs.next())
				return rs.getInt(1); 
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {			
			DatabaseHandler.closeStatementResult(ps, rs);			
		}
		return CALCULATION_ORDER_START_INDEX-1;			//Calculation order indexes start from 10000
	}
	
	private static void setCalculationOrder(Connection conn, int calculationId, int order) {
		PreparedStatement ps = null; 		
		try {
			ps = conn.prepareStatement("update calculations set element_order = ? where id = ?");
			ps.setInt(1, order);
			ps.setInt(2, calculationId);
			ps.executeUpdate();			
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeStatementResult(ps, null);
		}		
	}
	
	private static void moveCalculationWithinProject(Connection conn, String parentPath, int calculationOrder, int targetCalculationOrder) {
		PreparedStatement ps = null;
		if(calculationOrder > targetCalculationOrder) {
			try {
				ps = conn.prepareStatement("update calculations set element_order = element_order + 1 where element_order >= ? and element_order <= ? and path = ?");
				ps.setInt(1, targetCalculationOrder);
				ps.setInt(2, calculationOrder);
				ps.setString(3, parentPath);
				ps.executeUpdate();			
			}catch(Exception e) {
				log.error(e.getMessage());
			}finally {
				DatabaseHandler.closeStatementResult(ps, null);
			}			
		}else if(calculationOrder < targetCalculationOrder) {
			try {
				ps = conn.prepareStatement("update calculations set element_order = element_order - 1 where element_order >= ? and element_order <= ? and path = ?");
				ps.setInt(1, calculationOrder);
				ps.setInt(2, targetCalculationOrder);
				ps.setString(3, parentPath);
				ps.executeUpdate();			
			}catch(Exception e) {
				log.error(e.getMessage());
			}finally {
				DatabaseHandler.closeStatementResult(ps, null);
			}			
		}else {
			log.error("Can't swap calculations with the same order field value.");
		}
	}
	
	private static void moveCalculationIntoProject(Connection conn, String parentPath, int calculationOrder) {
		PreparedStatement ps = null;
		try {
			ps = conn.prepareStatement("update calculations set element_order = element_order + 1 where element_order >= ? and path = ?");
			ps.setInt(1, calculationOrder);
			ps.setString(2, parentPath);
			ps.executeUpdate();
		}catch(Exception e) {
			log.error(e.getMessage());
		}finally {
			DatabaseHandler.closeStatementResult(ps, null);
		}
	}
	
}