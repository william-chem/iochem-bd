package cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph;


public class ReactionNetworkException extends Exception {

	public ReactionNetworkException(String message) {
		super(message);
	}
}

