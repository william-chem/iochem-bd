/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;

import org.swordapp.client.ProtocolViolationException;
import org.swordapp.client.SWORDClientException;
import org.swordapp.client.SWORDCollection;
import org.swordapp.client.ServiceDocument;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.web.utils.Rest50ApiManager;
import cat.iciq.tcg.labbook.web.utils.SwordManager;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;
import cat.iciq.tcg.labbook.zk.components.publishtree.Element.ElementType;

@SuppressWarnings("deprecation")
public class ElementListBuilder {
	
	private ElementTreeNode root;
	private boolean defaultOpen = true;

	private HashMap<String,String> collectionCommunity = null;
	private Set<Entity> elements = null;
	private Rest50ApiManager restManager = null;		//Helper classes
	private SwordManager swordManager  = null;
	
	private ArrayList<Integer> managedCommunities = null;
	 
	public ElementListBuilder(Set<Entity> elements) throws UnsupportedEncodingException, Exception {	
		this.elements 		= elements;  
		restManager 		= new Rest50ApiManager();	
		swordManager 		= new SwordManager();
		collectionCommunity	= new HashMap<String, String> ();
		
		ElementTreeNode[] collections = loadCreateProjects();
		ElementTreeNode[] communities = loadBrowseCommunities();
		
		root = associate(communities, collections);
	}
	
	/**
	 * Publication presentation form will behave this way to conform information hierarchy. 
	 * 1.Separate projects from calculations by it's (unique) path. 
	 * 2.Find parent projects for selected calculations
	 * 	a. If they're already added inside in selection we do nothing
	 *  b. If not then we add them to projects selection
	 * 3. We add projects one by one, we look into calculations if there are child calculations
	 * 	a. If there are child calculations we add them to current project
	 *  b. Else we do nothing
	 * 4. Here we have all structure done at project level (at collection level), now we check if projects are published:
	 * 	a. If they are published we add it to it's community (must exist)
	 *  b. If no we'll add them to user's default community  
	 * 5. Nest projects by its path
	 */
	@SuppressWarnings("unchecked")
	private ElementTreeNode[] loadCreateProjects() throws Exception {		
		Vector<String> projectPaths					= new Vector<String>();
		
		Vector<Entity> calculations			= new Vector<Entity>();
		Vector<Entity> projects 				= new Vector<Entity>();
		
		HashMap<String, ElementTreeNode> pathToProjectTreeNode = new HashMap<String, ElementTreeNode>();
		
		//First we separate elements
		for(Entity element : elements){
			if(element instanceof Project){
				projects.add(element);
				projectPaths.add(element.getPath());
			}else{
				calculations.add(element);
			}			
		}
		
		//TODO:check this call to ProjectService
		//Now we check that calculation parent project is added
		for(Entity calculation : calculations){					
			String parentPath = calculation.getParentPath();
			if(!projectPaths.contains(parentPath)){				
				Entity project  = ProjectService.getByPath(parentPath);				
				projects.add(project);
				projectPaths.add(project.getPath());
			}			
		}
		//Now we attach calculations to it's parents
		ArrayList<ElementTreeNode> projectVector = new ArrayList<ElementTreeNode>();
		for(Entity project : projects){
			ElementTreeNode projectNode = buildProjectNode(project, calculations);
			pathToProjectTreeNode.put(project.getPath(), projectNode);			
		}
		
		//Nest by path
		for(Entity project : projects){			
			String projectPath = project.getPath();			
			ElementTreeNode projectNode = pathToProjectTreeNode.get(projectPath);
			if(pathToProjectTreeNode.containsKey(Paths.getParent(projectPath))){			//If exists parent project append it as a child
				ElementTreeNode parentNode = pathToProjectTreeNode.get(Paths.getParent(projectPath));
				parentNode.add(projectNode);				
			}else
				projectVector.add(projectNode);											//Otherwise add it to root
		}
		return projectVector.toArray(new ElementTreeNode[projectVector.size()]);
	}
	
	private ElementTreeNode buildProjectNode(Entity entity, Vector<Entity> calculations) throws BrowseCredentialsException{
		Project project = (Project)entity;		
		Element projectElement = new Element(null, project.getId(), project.getName(), project.getDescription(), ElementType.COLLECTION, project.isPublished(), project.getPublished_name(), project.getHandle(), project.getElementOrder(), null);
		ElementTreeNode[] projectChildren = getChildNodes(project.getPath(), calculations);						
		return new ElementTreeNode(projectElement, projectChildren, defaultOpen);
	}
	
	private ElementTreeNode[] getChildNodes(String parentPath, Vector<Entity> calculations){
		ArrayList<ElementTreeNode> childItems = new ArrayList<ElementTreeNode>();
		for(Entity calculation : calculations)
		{			
			String path = calculation.getParentPath();
			if(path.equals(parentPath)){				
				ElementTreeNode child = buildCalculationNode(calculation);
				childItems.add(child);
			}
		}
		return childItems.toArray(new ElementTreeNode[childItems.size()]);
	}
	
	private ElementTreeNode buildCalculationNode(Entity entity){
		Calculation calculation = (Calculation)entity;		
		Element calculationElement = new Element(null, calculation.getId(), calculation.getName(), calculation.getDescription(), ElementType.ITEM, calculation.isPublished(), calculation.getPublished_name(), calculation.getHandle(), calculation.getElementOrder(), null);
		return new ElementTreeNode(calculationElement);		
	}
	
	
	public ElementTreeNode getRoot(){
		return root;
	}
	
	public ElementTreeNode[] loadBrowseCommunities() throws UnsupportedEncodingException, Exception {			
		ArrayList<ElementTreeNode> communityNodes = new ArrayList<ElementTreeNode>();
		XMLFileManager xml = restManager.getCommunities();
		NodeList communities = xml.getItemIteratorQuery("//community");
		for(int inx = 0; inx < communities.getLength(); inx++){
			Node community = (Node)communities.item(inx);
			ElementTreeNode communityTreeNode = loadCommunity(community);
			if(communityTreeNode != null)
				communityNodes.add(communityTreeNode);
		}					
		return communityNodes.toArray(new ElementTreeNode[communityNodes.size()]);
	}
	
	public ElementTreeNode loadCommunity(Node community) throws BrowseCredentialsException, Exception {	
		org.w3c.dom.Element node = (org.w3c.dom.Element)community;					
		String id 		= node.getElementsByTagName("id").item(0).getFirstChild().getNodeValue();
		String handle 	= node.getElementsByTagName("handle").item(0).getFirstChild().getNodeValue();
		String name 	= node.getElementsByTagName("name").item(0).getFirstChild().getNodeValue();		
		if(!isUserCommunityAdministator(community))
			return null;		
		loadCollections(handle);
		return new ElementTreeNode(new Element(id, -1, name, null, ElementType.COMMUNITY,true , name, handle, 0, null), null, defaultOpen);
	}
	
	
	public boolean isUserCommunityAdministator(Node community) throws Exception {
		getManagedCommunitiesId();
		org.w3c.dom.Element node = (org.w3c.dom.Element)community;				
		Integer id 		= Integer.valueOf(node.getElementsByTagName("id").item(0).getFirstChild().getNodeValue());		
		return managedCommunities.contains(id);		
	}
	
	private void getManagedCommunitiesId(){
		if(managedCommunities != null) //Values already loaded
			return;
		managedCommunities = new ArrayList<Integer>();				
		try {
			XMLFileManager obj = restManager.getAdministratorCommunities();
			if(obj != null){		
				NodeList communityIds = obj.getItemIteratorQuery("//administrator/communities");
				for(int inx = 0; inx < communityIds.getLength(); inx++)
					managedCommunities.add(Integer.valueOf(communityIds.item(inx).getTextContent()));								
			}			
		} catch (IOException | SAXException | ParserConfigurationException e) {			
		}		
	}
	
	public void loadCollections(String communityHandle) throws SWORDClientException, ProtocolViolationException {
		ServiceDocument sd = swordManager.getServiceDocument("/" + communityHandle);
		List<SWORDCollection> collections = sd.getWorkspaces().get(0).getCollections();		
		for(SWORDCollection collection: collections){
			if(!acceptsMETSDSpaceSIP(collection))	//Here we'll discard communties and collections that didn't work with DSpaceMETSSIP package format
				continue;
			String handle = collection.getHref().toString().substring(collection.getHref().toString().lastIndexOf("collection/")+11);				
			collectionCommunity.put(handle, communityHandle);
		}			
	}	
		
	public ElementTreeNode associate(ElementTreeNode[] communities, ElementTreeNode[] collections) throws Exception {	
		if(communities.length == 0)
			throw new Exception("There has been an error while associating Create projects to Browse communities. Report ioChem-BD administrator");
		ElementTreeNode rootNode = new ElementTreeNode(null, communities, defaultOpen);
		
		for(ElementTreeNode collectionNode : collections) {
			if(collectionNode.getData().isPublished()) {
				String collectionHandle = collectionNode.getData().getHandle();
				String communityHandle  = collectionCommunity.get(collectionHandle);
				if(communityHandle == null)				
					throw new Exception("There has been an error retrieving parent community for collection " + collectionHandle + ". Please check that current user has rights to publish on such community.");				
				ElementTreeNode community = findCommunityByHandle(communities, communityHandle);
				community.add(collectionNode);
			}else 																				//By now we will assign unpublished projects to first loaded community (this is not correct, but by now it will work)			
				communities[0].add(collectionNode);			
		}		
		return rootNode;		
	}
	
	private ElementTreeNode findCommunityByHandle(ElementTreeNode[] communities, String communityHandle) {
		for(ElementTreeNode community : communities)
			if(community.getData().getHandle().equals(communityHandle))
				return community;
		return null;
	}
	
	/**
	 * This function checks if passed SWORDCollection object contains DSpaceMETSSIP package between it's accepted packages
	 * @param collection
	 * @return
	 */
	public boolean acceptsMETSDSpaceSIP(SWORDCollection collection){
		for(String packaging : collection.getAcceptPackaging())
			if(packaging.equals(SwordManager.DSPACEMETSSIP_PACKAGE_URL))
				return true;
		return false;
	}
	
}
