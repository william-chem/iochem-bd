/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.definitions;

import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;

public class XpathQueries {

	//METS file specific XPath Queries
	public static final String GET_CALCULATION_INTERNAL_ID 		= "substring(/*:mets/@ID,2)";
	public static final String GET_CALCULATION_OUTPUT_FILENAME 	= "//*:fileSec/*:fileGrp[@USE='" + MetsFileHandler.BUNDLE_ORIGINAL +"']/*:file[@USE='output']/*:FLocat/@xlink:href";
	public static final String GET_CALCULATION_FILES_FILEID		= "//*:structMap[@TYPE='" + MetsFileHandler.STRUCTMAP_TYPE + "']/*:div[@LABEL='" + MetsFileHandler.STRUCTMAP_DIV_LABEL +"']//*:fptr/@FILEID";
	public static final String GET_JUMBOTYPE_FROM_FILEID		= "//*:mdWrap[@OTHERMDTYPE='jumbo']/*:xmlData/jumbo:file[@FILEID='?']/@jumbo:output";
	public static final String GET_MIMETYPE_FROM_FILEID			= "//*:fileGrp/*:file[@ID='?']/@MIMETYPE";
	public static final String GET_FILENAME_FROM_FILEID			= "//*:fileGrp/*:file[@ID='?']/*:FLocat/@xlink:href";
	public static final String GET_FILESIZE_FROM_FILENAME		= "//*:fileGrp/*:file/*:FLocat[@xlink:href='?']/ancestor-or-self::*:file/@SIZE";
	public static final String GET_MIMETYPE_FROM_FILENAME		= "//*:fileGrp/*:file/*:FLocat[@xlink:href='?']/ancestor-or-self::*:file/@MIMETYPE";
	public static final String GET_USE_FROM_FILENAME			= "//*:fileGrp/*:file/*:FLocat[@xlink:href='?']/ancestor-or-self::*:file/@USE";
	
	public static final String HAS_MOLECULAR_ORBITALS_FILE		= "exists(//*:fileSec/*:fileGrp/*:file[@MIMETYPE='chemical/x-molden'])";
	public static final String GET_ORBITAL_FILE_ELEMENTS		= "//*:fileSec/*:fileGrp/*:file[@MIMETYPE='chemical/x-molden']/*:FLocat";
	
	public static final String HAS_ADDITIONAL_GEOMETRIES_FILE   = "exists(//*:fileSec/*:fileGrp/*:file[@MIMETYPE='chemical/x-gromos87'])";
	public static final String GET_GEOMETRY_FILE_ELEMENTS       = "//*:fileSec/*:fileGrp/*:file[@MIMETYPE='chemical/x-gromos87']/*:FLocat";
		
}
