/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Vector;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.data.MatrixDH;
import cat.iciq.tcg.labbook.shell.definitions.CommandsMessages;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;

/**
 * Servlet implementation class CPro
 */
public class LSPro extends Services {
	
	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");   
       
    /**
     * @see Services#Services()
     */
    public LSPro() {
        super();
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String, String> params, String userName,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
		String path = Paths.isAbsolute(params.get("newPath")) ? 
				params.get("newPath") : 
				Paths.getFullPath(params.get("path"), params.get("newPath"));
	
		if(!PermissionService.hasPermissionOnProject(path, Permissions.READ)) {
			returnKO(ostream,CommandsMessages.PROJECT_ERROR_READ_PERM.TITLE(),CommandsMessages.PROJECT_ERROR_READ_PERM.MESSAGE());
			return;
		}
		
		List<Project> projects = ProjectService.getChildProjects(path);
		List<Calculation> calculations = ProjectService.getChildCalculations(path);

		String orderby = params.get("orderby");
		if (orderby != null) {
			sortProjects(projects, orderby);
			sortCalculations(calculations, orderby);
		}

		Vector<String> type  = new Vector<String>();
		Vector<String> name = new Vector<String>();
		Vector<String> description = new Vector<String>();
		Vector<String> permissions = new Vector<String>();
		Vector<String> owner = new Vector<String>();
		Vector<String> group = new Vector<String>(); 
		Vector<String> time = new Vector<String>();
		Vector<String> ca = new Vector<String>();
		Vector<String> state = new Vector<String>();
		
		for(Project p: projects) {
			type.add("PRO");
			name.add(p.getName());
			description.add(p.getDescription());
			permissions.add(p.getPermissions());
			owner.add(ShiroManager.getCurrent().getUserNameById(Integer.valueOf(p.getOwner())));
			group.add(ShiroManager.getCurrent().getGroupNameById(Integer.valueOf(p.getGroup())));
			time.add(DATE_FORMAT.format(p.getCreationDate()));
			ca.add(p.getConceptGroup());
			state.add(p.getState());
		}
		 
		if(!calculations.isEmpty()) {
			Project p = ProjectService.getByPath(path);	
			String permissionsCalc = p.getPermissions();
			String ownerCalc = ShiroManager.getCurrent().getUserNameById(Integer.valueOf(p.getOwner()));
			String groupCalc = ShiroManager.getCurrent().getGroupNameById(Integer.valueOf(p.getGroup()));		
			
			for(Calculation c: calculations) {				
				type.add(c.getType().getAbbreviation());
				name.add(c.getName());
				description.add(c.getDescription());
				permissions.add(permissionsCalc);
				owner.add(ownerCalc);
				group.add(groupCalc);
				time.add(DATE_FORMAT.format(c.getCreationDate()));
				ca.add("");
				state.add("");
			}
		}

		MatrixDH data = new MatrixDH();
		data.setStringColumn(0, type);
		data.setStringColumn(1, name);
		data.setStringColumn(2, description);
		data.setStringColumn(3, permissions);
		data.setStringColumn(4, owner);
		data.setStringColumn(5, group);
		data.setStringColumn(6, time);
		data.setStringColumn(7, ca);
		data.setStringColumn(8, state);
		sendObj(data, ostream);
    }

	private void sortCalculations(List<Calculation> calculations, String order) {
		Comparator<Calculation> comparator = null;

		switch(order) {
			case "t":	comparator = (Calculation c1, Calculation c2) -> c1.getCreationDate().compareTo(c2.getCreationDate());
						break;
			case "n":
			default:	comparator = (Calculation c1, Calculation c2) -> c1.getName().compareTo(c2.getName());
						break;
		}
		Collections.sort(calculations, comparator);
	}

	private void sortProjects(List<Project> projects, String order) {
		Comparator<Project> comparator = null;

		switch(order) {
			case "t": 	comparator = (Project p1, Project p2) -> p1.getCreationDate().compareTo(p2.getCreationDate());
						break;
			case "o":	comparator = (Project p1, Project p2) -> p1.getOwner() - p2.getOwner();
						break;	
			case "g":	comparator = (Project p1, Project p2) -> p1.getGroup() - p2.getGroup();
						break;
			case "c":	comparator = (Project p1, Project p2) -> p1.getConceptGroup().compareTo(p2.getConceptGroup());
						break;
			case "s": 	comparator = (Project p1, Project p2) -> p1.getState().compareTo(p2.getState());
						break;						
			case "n":
			default:	comparator = (Project p1, Project p2) -> p1.getName().compareTo(p2.getName());
						break;
		
		}
		Collections.sort(projects, comparator);	
	}
}
