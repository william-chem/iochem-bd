/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.xml.transform.Source;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.zk.components.uploadtoolbar.CalculationInsertion;
import cat.iciq.tcg.labbook.zk.composers.Main;
import edu.harvard.hul.ois.mets.helper.MetsException;

public class PublishService {
	
	private static final Logger log = LogManager.getLogger(PublishService.class);
	
	private static TransformerFactory tFactory 					= null; 		//Saxon static elements. This object will generate all conversion templates 	
	private static HashMap<Integer,Templates> loadedTemplates	= null;			//Templates will be class variables
	
	private static String ccLicenseRdfFilename = "license.xml";
	private static String ccLicenseTextRdf =
			"<result>" + 
				"<license-uri>http://creativecommons.org/licenses/by/4.0/</license-uri>" + 
				"<license-name>Attribution 4.0 International</license-name>" + 												 
				"<rdf>" + 
					"<rdf:RDF xmlns='http://creativecommons.org/ns#' xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>" + 
						"<Work xmlns:dc='http://purl.org/dc/elements/1.1/' rdf:about=''>" + 
							"<license rdf:resource='http://creativecommons.org/licenses/by/4.0/'/>" + 
						"</Work>" + 
						"<License rdf:about='http://creativecommons.org/licenses/by/4.0/'>" + 
							"<permits rdf:resource='http://creativecommons.org/ns#DerivativeWorks'/>" + 
							"<permits rdf:resource='http://creativecommons.org/ns#Distribution'/>" + 
							"<permits rdf:resource='http://creativecommons.org/ns#Reproduction'/>" + 
							"<requires rdf:resource='http://creativecommons.org/ns#Attribution'/>" + 
							"<requires rdf:resource='http://creativecommons.org/ns#Notice'/>" + 
						"</License>" + 
					"</rdf:RDF>" + 
				"</rdf>" + 
				"<licenserdf>" + 
					"<rdf:RDF xmlns='http://creativecommons.org/ns#' xmlns:rdf='http://www.w3.org/1999/02/22-rdf-syntax-ns#'>" + 
						"<License rdf:about='http://creativecommons.org/licenses/by/4.0/'>" + 
							"<permits rdf:resource='http://creativecommons.org/ns#DerivativeWorks'/>" + 
							"<permits rdf:resource='http://creativecommons.org/ns#Distribution'/>" + 
							"<permits rdf:resource='http://creativecommons.org/ns#Reproduction'/>" + 
							"<requires rdf:resource='http://creativecommons.org/ns#Attribution'/>" + 
							"<requires rdf:resource='http://creativecommons.org/ns#Notice'/>" + 
						"</License>" + 
					"</rdf:RDF>" + 
				"</licenserdf>" + 
				"<html>" + 
					"<a rel='license' href='http://creativecommons.org/licenses/by/4.0/'>" + 
						"<img alt='Creative Commons License' style='border-width:0' src='http://i.creativecommons.org/l/by/4.0/88x31.png'/>" + 
					"</a>" + 
					"<br/>" + 
					"This work is licensed under a" + 
					"<a rel='license' href='http://creativecommons.org/licenses/by/4.0/'>" + 
						"Creative Commons Attribution 4.0 International License" + 
					"</a>" + 
					"." + 
				"</html>" + 
			"</result>";
	
	static{
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
		loadedTemplates = new HashMap<Integer, Templates>();	
	}
	
	public static String buildMetsPackage(HashMap<String,String> param) throws Exception, SQLException, MetsException, TransformerException {		
		int calculationId = Integer.valueOf(param.get("id"));
		HashMap<String, byte[]> fileNameContent = AssetstoreService.getCalculationFilesContent(calculationId);		
		fileNameContent.put("mets.xml", getCalculationNormalizedMETS(param, fileNameContent).getBytes());		
		fileNameContent.put(ccLicenseRdfFilename, ccLicenseTextRdf.getBytes());
        String zipFile = buildZipFile(calculationId, fileNameContent);
        return zipFile;		
	}
	
	
	private static String getCalculationNormalizedMETS(HashMap<String,String> param, HashMap<String, byte[]> fileNameContent) throws Exception
	{
		int calculationId = Integer.valueOf(param.get("id"));		
		Calculation calculation = CalculationService.getById(calculationId);
		
		String md5sum = DigestUtils.md5Hex(IOUtils.toInputStream(ccLicenseTextRdf, "UTF-8"));
		int size = ccLicenseTextRdf.getBytes().length;
		MetsFileHandler metsFileHandler = new MetsFileHandler(calculation.getMetsXml());		
		metsFileHandler.attachLicenseFile(ccLicenseRdfFilename, md5sum, size);
		attachThumbnailFile(metsFileHandler, calculationId);	
		attachGeometryFile(metsFileHandler, calculationId);
		attachBrowseMetadata(calculation, param, metsFileHandler, fileNameContent);		
		return metsFileHandler.toString();	
	}
	
	private static void attachThumbnailFile(MetsFileHandler metsFileHandler, int calculationId) throws Exception{		
		String fullPath   = FileService.getCalculationPath(calculationId , CalculationInsertion.THUMBNAIL_FILE_NAME);
		String md5sum     = DigestUtils.md5Hex(new FileInputStream(fullPath));
		double size       = (new File(fullPath)).length();
		metsFileHandler.addFile(null, null, null, "image/jpeg", md5sum, "thumbnail", CalculationInsertion.THUMBNAIL_FILE_NAME, "thumbnail file", size);		
	}
	
	private static void attachGeometryFile(MetsFileHandler metsFileHandler, int calculationId) throws Exception{		
		String fullPath   = FileService.getCalculationPath(calculationId , CalculationInsertion.GEOMETRY_FILE_NAME); 
		String md5sum     = DigestUtils.md5Hex(new FileInputStream(fullPath));
		double size       = (new File(fullPath)).length();
		metsFileHandler.addFile(null, null, null, "chemical/x-cml", md5sum, "geometry", CalculationInsertion.GEOMETRY_FILE_NAME, "geometry file", size);
	}
	
	
	private static void attachBrowseMetadata(Calculation calculation, HashMap<String,String> param, MetsFileHandler metsFileHandler, HashMap<String, byte[]> fileNameContent ) throws SQLException, TransformerException {			
		String author = param.get("author");
		String title  = param.get("title"); 
		String description = param.get("description");
		
		synchronized(loadedTemplates){
			if(!loadedTemplates.containsKey(calculation.getType().getId())){
				StreamSource xslSource = new StreamSource(new File(WebApps.getCurrent().getRealPath(calculation.getType().getMetadataTemplate())));
				loadedTemplates.put(calculation.getType().getId(), tFactory.newTemplates(xslSource));
			}			
		}
		Transformer transformer = loadedTemplates.get(calculation.getType().getId()).newTransformer();
		//Set metadata fields captured from Create system and not from outputfiles 
		transformer.setParameter("author", author);
		transformer.setParameter("title", title);
		transformer.setParameter("description", description);
		transformer.setParameter("hasmolecularorbitals", metsFileHandler.containsMoldenFile());		
		Source source = new StreamSource(new ByteArrayInputStream(fileNameContent.get(metsFileHandler.getOutputFileName())));
		StringWriter writer = new StringWriter();
        StreamResult result = new StreamResult(writer);
        transformer.transform(source, result);
        //Set metadata extracted from calculation
		metsFileHandler.insertMetadataCsvFields(writer.toString());
		//Set metadata provided by user on publication form
		for(String key : param.keySet()){
			if(key.startsWith("metadata")){
				String value = param.get(key);
				key = key.split("#")[1];
				metsFileHandler.appendMetadataField(key, value);
			}
		}
		try{
			writer.close();
		}catch(Exception e){
			log.error(e.getMessage());
		}
	}
	
	private static String buildZipFile(int calculationId, HashMap<String,byte[]> fileContents) throws Exception {
		String zipFile = Main.TMP_ROOT_DIR + calculationId + ".zip";
        new File(zipFile).delete();
        FileOutputStream fout = new FileOutputStream(zipFile);
		ZipOutputStream out = new ZipOutputStream(fout);
		out.setMethod(ZipOutputStream.DEFLATED);
		out.setLevel(Deflater.NO_COMPRESSION);
		//Add text files content to current zip
	    for(String filename : fileContents.keySet()){		    	
	        ZipEntry entry = new ZipEntry(filename);	        
	        byte[] fileBytes = fileContents.get(filename);
	        entry.setSize(fileBytes.length);
	        out.putNextEntry(entry);      
	        out.write(fileBytes,0,fileBytes.length);
	        out.closeEntry();
	    }
	    
	    //Add thumbnail file 
	    ZipEntry entry = new ZipEntry(CalculationInsertion.THUMBNAIL_FILE_NAME);
	    File file = new File(FileService.getCalculationPath(calculationId,CalculationInsertion.THUMBNAIL_FILE_NAME));		
	    byte[] fileBytes = FileUtils.readFileToByteArray(file);
        entry.setSize(fileBytes.length);
        out.putNextEntry(entry);      
        out.write(fileBytes,0,fileBytes.length);
        out.closeEntry();
        
        //Add geometry file
        entry = new ZipEntry(CalculationInsertion.GEOMETRY_FILE_NAME);
		file = new File(FileService.getCalculationPath(calculationId, CalculationInsertion.GEOMETRY_FILE_NAME));		
	    fileBytes = FileUtils.readFileToByteArray(file);
        entry.setSize(fileBytes.length);
        out.putNextEntry(entry);      
        out.write(fileBytes,0,fileBytes.length);
        out.closeEntry();
        
		out.close();
		return zipFile;
	}
}
