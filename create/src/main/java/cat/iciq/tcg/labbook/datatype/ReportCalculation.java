/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

public class ReportCalculation {
	private int id;
	private int reportId;
	private int calcOrder;
	private String title;
	private int calcId;
	private String calcPath;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public int getReportId() {
		return reportId;
	}
	public void setReportId(int reportId) {
		this.reportId = reportId;
	}
	
	public int getCalcOrder() {
		return calcOrder;
	}
	public void setCalcOrder(int calcOrder) {
		this.calcOrder = calcOrder;
	}	
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	
	public int getCalcId() {
		return calcId;
	}
	public void setCalcId(int calcId) {
		this.calcId = calcId;
	}
	
	public String getCalcPath(){
		return this.calcPath;				
	}
	public void setCalcPath(String calcPath){
		this.calcPath = calcPath;
	}
	
}
