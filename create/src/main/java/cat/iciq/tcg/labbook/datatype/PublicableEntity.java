/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.sql.Timestamp;

public abstract class PublicableEntity extends Entity{
	
    private Timestamp publicationDate;
	
    private boolean published;	
	
    private String handle;
	
    private String published_name;

	public Timestamp getPublicationDate() {
		return publicationDate;
	}
	public void setPublicationDate(Timestamp publicationDate) {
		this.publicationDate = publicationDate;
	}
	public boolean isPublished() {
		return published;
	}
	public void setPublished(boolean published) {
		this.published = published;
	}
	public String getHandle() {
		return handle;
	}
	public void setHandle(String handle) {
		this.handle = handle;
	}
	public String getPublished_name() {
		return published_name;
	}
	public void setPublished_name(String published_name) {
		this.published_name = published_name;
	}
}
