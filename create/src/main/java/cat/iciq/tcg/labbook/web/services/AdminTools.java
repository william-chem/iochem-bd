/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.SortedSet;
import java.util.TreeSet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.datatype.services.UploadService;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.TokenManager;
import cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction;
import cat.iciq.tcg.labbook.zk.ui.DeferredContentDaemon;
import cat.iciq.tcg.labbook.zk.ui.WebAppInitiator;

public class AdminTools extends HttpServlet {

	private static final Logger logger = LogManager.getLogger(AdminTools.class.getName()); 	
	
	public static final String SERVLET_ENDPOINT = "/admintools";
	public static final String FLUSHXSLT_ENDPOINT = "/flushxslt";
	public static final String USAGESTATISTICS_ENDPOINT = "/usagestatistics";
	public static final String DOIDAEMONSTATUS_ENDPOINT = "/doidaemonrefresh";
	public static final String LOADLIMITS_ENDPOINT = "/uploadlimits";
	public static final String FILE_RETRIEVAL_ENDPOINT = "/fileretrieval";

	private static final int CATEGORIES_INDEX	= 0;
	private static final int PROJECTS_INDEX 	= 1;
	private static final int CALCULATIONS_INDEX = 2;
	private static final int PUBLISHED_INDEX 	= 3;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {		
		String endPoint = req.getRequestURI().substring(req.getContextPath().length());
		if(endPoint.equals(SERVLET_ENDPOINT + FLUSHXSLT_ENDPOINT )){
			flushXsltTemplates();
			resp.setContentType("text/html");
			resp.getOutputStream().write("<html><body>XSLT templates flushed!</body></html>".getBytes());
		}else if(endPoint.equals(SERVLET_ENDPOINT + USAGESTATISTICS_ENDPOINT)){
			resp.setContentType("application/json");			
			String period = req.getParameter("period");
			if(period == null){
				resp.getOutputStream().write(getUsageJSON(null).getBytes());
			}else if(period.equals("lastmonth")){
				Date current = new Date(System.currentTimeMillis());
				Date dateBefore = new Date(current.getTime() - 30 * 24 * 3600 * 1000l );
				String formatedDate = new SimpleDateFormat("yyyy/MM/dd").format(dateBefore);
				resp.getOutputStream().write(getUsageJSON(formatedDate).getBytes());
			}else if(period.equals("lastweek")){
				Date current = new Date(System.currentTimeMillis());
				Date dateBefore = new Date(current.getTime() - 7 * 24 * 3600 * 1000l );
				String formatedDate = new SimpleDateFormat("yyyy/MM/dd").format(dateBefore);
				resp.getOutputStream().write(getUsageJSON(formatedDate).getBytes());				
			}		
		}else if(endPoint.equals(SERVLET_ENDPOINT + DOIDAEMONSTATUS_ENDPOINT)){
			WebAppInitiator.setDoiDaemonStatus();
			String response = "Daemon status set to " + (int) WebApps.getCurrent().getAttribute("doiDaemonStatus"); 
			resp.getOutputStream().write(response.getBytes());
		}else if(endPoint.equals(SERVLET_ENDPOINT + LOADLIMITS_ENDPOINT)){
		    WebAppInitiator.setGlobalParameters();
		    UploadService.setupUploadRestrictions();
		    String response = "Global parameters updated successfully.";
		    resp.getOutputStream().write(response.getBytes());
		}else if(endPoint.equals(SERVLET_ENDPOINT + FILE_RETRIEVAL_ENDPOINT)) {
		    processFileRetrievalRequest(req);
		    resp.setCharacterEncoding("text/html");
		    resp.getOutputStream().write("File retrieval notified!".getBytes());		   
		}
		resp.flushBuffer();
	}
	
	private void flushXsltTemplates(){
		XsltTransformAction.flushXsltTemplates();
		JCampDX.flushXsltTemplates();
		ToXYZ.flushXsltTemplates();		
	}
	
	private String getUsageJSON(String from) {
		StringBuilder json = new StringBuilder();	
		try {
			ArrayList<String> values =  getUserInfoAndStatistics(from);			
			json.append("{ ");			
			json.append("\"usernumber\" : [" + getNumberOfUsers() + "] ,");						
			json.append("\"categories\" : [" + values.get(CATEGORIES_INDEX) + "] ,");
			json.append("\"projects\" :   [" + values.get(PROJECTS_INDEX) + "] ,");			
			json.append("\"calculations\" : [" + values.get(CALCULATIONS_INDEX) + "] ,");
			json.append("\"published\" :  [" + values.get(PUBLISHED_INDEX) + "]");
			json.append("}");
		} catch (Exception e) {
			json.setLength(0);
			logger.error(e.getMessage());
		}						
		return json.toString();
	}
	
	
	private ArrayList<String> getUserInfoAndStatistics(String from) {		
		Connection conn = null;
		ArrayList<String> values = new ArrayList<String>();	
		values.add("");
		values.add("");
		values.add("");
		values.add("");					

		try {
			conn = DatabaseHandler.getCreateConnection();
			SortedSet<String> usernames = new TreeSet<String>(ShiroManager.getCurrent().userIdByName.keySet());
			for(String username : usernames){
				int calculations = getUploadedCalculationsByUsername(conn, username, from);
				int projects 	 = getCreatedProjectsByUsername(conn, username, from);
				int published	 = getPublishedCalculationsByusername(conn, username, from); 
				addNewUser(values, username, calculations, projects, published);
			}
		} catch (Exception e) {
			logger.error(e.getMessage());
			try {
				conn.close();
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}
		
		return values;		
	}
	
	private int getNumberOfUsers() throws BrowseCredentialsException{
		return ShiroManager.getCurrent().getNumberOfUsers();		
	}
	
	private int getUploadedCalculationsByUsername(Connection conn, String username, String from){
		String queryCalculations = "SELECT count(*) AS total FROM calculations WHERE path LIKE ?";
		if(from != null)
			queryCalculations +=" and creation_time > \'" + from + "\'";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(queryCalculations);
			st.setString(1, "/db/" + username + "/%");
			ResultSet rs = st.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}
		return 0;
	}
	
	private int getCreatedProjectsByUsername(Connection conn, String username, String from){		
		String queryCalculations = "SELECT count(*) FROM projects WHERE owner_user_id = ?";
		if(from != null)
			queryCalculations +=" and creation_time > \'" + from + "\'";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(queryCalculations);
			st.setInt(1, ShiroManager.getCurrent().getUserIdByName(username));
			ResultSet rs = st.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			logger.error(e.getMessage());			
		}
		return 0;
	}
	
	private int getPublishedCalculationsByusername(Connection conn, String username, String from){		
		String queryCalculations = "SELECT count(*) FROM calculations WHERE path LIKE ? and publication_time is not null";
		if(from != null)
			queryCalculations +=" and creation_time > \'" + from + "\'";
		PreparedStatement st;
		try {
			st = conn.prepareStatement(queryCalculations);
			st.setString(1, "/db/" + username + "/%");
			ResultSet rs = st.executeQuery();
			rs.next();
			return rs.getInt(1);
		} catch (Exception e) {
			logger.error(e.getMessage());			
		}
		return 0;
	}
	
	private ArrayList<String> addNewUser(ArrayList<String> data, String username, int calculations, int projects, int published){
		if(!data.get(CATEGORIES_INDEX).equals("")){
			data.set(CATEGORIES_INDEX, data.get(CATEGORIES_INDEX) + ", ");
			data.set(PROJECTS_INDEX, data.get(PROJECTS_INDEX) + ", ");
			data.set(CALCULATIONS_INDEX, data.get(CALCULATIONS_INDEX) + ", ");
			data.set(PUBLISHED_INDEX, data.get(PUBLISHED_INDEX) + ", ");			
		}		
		data.set(CATEGORIES_INDEX, data.get(CATEGORIES_INDEX) + " \"" + username + "\"");
		data.set(PROJECTS_INDEX, data.get(PROJECTS_INDEX) + " " + projects);
		data.set(CALCULATIONS_INDEX, data.get(CALCULATIONS_INDEX) + " " + calculations);
		data.set(PUBLISHED_INDEX, data.get(PUBLISHED_INDEX) + " " + published);						
		return data;
	}
	
	private void processFileRetrievalRequest(HttpServletRequest request) {	     
	    Long requestMillis = Long.valueOf(request.getParameter("date"));
        String requestHash = request.getParameter("hash");
        String parentId = request.getParameter("parentId");
        String id = request.getParameter("id");
        String username = request.getParameter("username");
        if(isRetrievalRequestValid(requestMillis, requestHash, id)) {
            DeferredContentDaemon.addAccessibleFile(username, Integer.valueOf(parentId), Integer.valueOf(id), requestMillis);            
        }
    }

    private boolean isRetrievalRequestValid(Long requestMillis, String requestHash, String id) {
        if(requestMillis == null || requestHash == null)    // Bad request
            return false;
        
        String salt =  CustomProperties.getProperty("external.communication.secret");
        String calculatedHash = TokenManager.encode(requestMillis + "#" + id , salt).substring(0,10);
        if(!requestHash.equals(calculatedHash))
            return false;
        // If request is more than 24h old, file is deferred again, need to request it again.
        Long currentMillis = System.currentTimeMillis();
        if(currentMillis < requestMillis || currentMillis > requestMillis + (24*60*60*1000)  ) 
            return false;
        return true;
    }
}
