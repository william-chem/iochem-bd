package cat.iciq.tcg.labbook.zk.ui;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.zkoss.zk.ui.WebApps;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;

import com.google.gson.Gson;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.services.AdminTools;
import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.web.utils.RestManager;
import cat.iciq.tcg.labbook.web.utils.TokenManager;
import cat.iciq.tcg.labbook.zk.composers.Main;

public class DeferredContentDaemon implements Runnable {

    private static Map<Long, Integer> accessedFiles = null;
    private static Map<Integer, Long> accessedIds = null;
    
    private static String requestServerUrl = (String)WebApps.getCurrent().getAttribute("deferredContentServer");
    private static final String REST_OAI_ENDPOINT = "/rest/oai";
    private static final String OAI_BITSTREAM_ENDPOINT = "/bitstream";
    private static final String OAI_BITSTREAM_RETRIEVE_ENDPOINT = "/retrieve";
    private static final Long ONE_DAY_MILLIS = 86400000L;

    static {
        accessedFiles = (SortedMap<Long, Integer>)WebApps.getCurrent().getAttribute("accessedFiles");
        if(accessedFiles == null) {
            accessedFiles = new TreeMap<>();
            WebApps.getCurrent().setAttribute("accessedFiles", accessedFiles);
        }
        accessedIds = (Map<Integer, Long>)WebApps.getCurrent().getAttribute("accessedIds");
        if(accessedIds == null) {
            accessedIds = new HashMap<>();
            WebApps.getCurrent().setAttribute("accessedIds", accessedIds);
        }
        requestServerUrl = requestServerUrl + REST_OAI_ENDPOINT + OAI_BITSTREAM_ENDPOINT + OAI_BITSTREAM_RETRIEVE_ENDPOINT;
    }

    @Override
    public void run() {
        long lastDayMillis = System.currentTimeMillis() - (ONE_DAY_MILLIS);
        ((SortedMap<Long, Integer>) accessedFiles).subMap(0L, lastDayMillis).forEach((k,v) -> {
            accessedFiles.remove(k);
            accessedIds.remove(v);
        });
    }

    public static boolean isAccessible(int calcFileId) {
        return accessedIds.containsKey(calcFileId) &&
                accessedIds.get(calcFileId)> System.currentTimeMillis() - (ONE_DAY_MILLIS);
    }

    public static synchronized void addAccessibleFile(String username, int calculationId, int calculationFileId, long firstAccessMillis) {        
        accessedFiles.put(firstAccessMillis, calculationFileId);
        accessedIds.put(calculationFileId, firstAccessMillis);

        //Notify file is downloaded
        EventQueue<Event> userEventsQueue = EventQueues.lookup(username + "userevents", WebApps.getCurrent(), true);
        userEventsQueue.publish(new Event("fileRetrieved", null, String.valueOf(calculationId) + "#" + String.valueOf(calculationFileId)));
    }

    public static synchronized void requestAccess(String username, int calculationId, int calculationFileId, String name, String path) throws ParseException, Exception {
        String baseUrl = Main.getBaseUrl();
        String salt =  CustomProperties.getProperty("external.communication.secret");
        String secret = "PUT" + "#" + baseUrl + "#BITSTREAM#" + calculationId + "#" + calculationFileId;        
        String hash = TokenManager.encode(salt, secret);            
        
        HttpEntity putEntity = buildBitstreamRetrieveEntity(username, baseUrl, calculationId, calculationFileId, name, path, hash);
        RestManager.putRequest(RestManager.buildAcceptAllHttpClient(), requestServerUrl, putEntity, null);    
    }    
    
    private static HttpEntity buildBitstreamRetrieveEntity(String username, String baseUrl, int calculationId, int calculationFileId, String name, String path, String hash) throws URISyntaxException, BrowseCredentialsException {
        Map<String, Object> params = new HashMap<>();
        params.put("institutionUrl", baseUrl);
        params.put("objectType", "BITSTREAM");
        params.put("handle", calculationId);
        params.put("id", calculationFileId);
        params.put("hash", hash);
       
        
        URIBuilder retrievalUrl = new URIBuilder(baseUrl + "/create" + AdminTools.SERVLET_ENDPOINT + AdminTools.FILE_RETRIEVAL_ENDPOINT);
        retrievalUrl.addParameter("parentId", String.valueOf(calculationId));
        retrievalUrl.addParameter("id", String.valueOf(calculationFileId));
        retrievalUrl.addParameter("username", username);
        List<JsonDataPair> metadata = new ArrayList<>();
        metadata.add(new JsonDataPair("file", path));
        metadata.add(new JsonDataPair("name", name));
        metadata.add(new JsonDataPair("url", retrievalUrl.toString()));
        metadata.add(new JsonDataPair("email", ShiroManager.getCurrent().getUserEmail()));
        params.put("metadata", metadata);
        return new StringEntity(new Gson().toJson(params, HashMap.class), ContentType.create("application/json"));
    }

    public static boolean isUnderMaintenance() {        
        String maintenance = CustomProperties.getProperty("deferred.under.maintenance");
        if(maintenance == null)
            return false;
        else
            return Boolean.parseBoolean(maintenance);
    }    
}

class JsonDataPair {
    String element = null;
    String value = null;
    
    public JsonDataPair(String element, String value) {
        this.element = element; 
        this.value = value;
    }
    
    public String getElement() {
        return element;
    }

    public void setElement(String element) {
        this.element = element;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
