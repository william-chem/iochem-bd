/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.main.navigation;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zul.TreeNode;
import org.zkoss.zul.event.TreeDataEvent;
import org.zkoss.zul.ext.SelectionControl;
import org.zkoss.zul.ext.Sortable;
import org.zkoss.zul.ext.TreeSelectableModel;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.CustomTreeNode.TreeNodeChildrenList;

public class TreeNavigationModel extends CustomTreeModel<TreeNode<Entity>> implements TreeSelectableModel, Sortable {

	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(TreeNavigationModel.class.getName());
		
	private HashSet<TreeNode<Entity>> childrenCache;
	private HashSet<TreeNode<Entity>> childrenLoadedCache;		// Used to remember if a parent's children has been retrieved 
	private boolean refreshRequired = false;
	
	public TreeNavigationModel(TreeNode<Entity> root) {
		super(root);
		childrenCache = new HashSet<>();
		childrenLoadedCache = new HashSet<>();
		
		//Load root element and its children
		childrenCache.add(root);
		childrenLoadedCache.add(root);
		for(TreeNode<Entity> childNode : loadChildElements(root)) {
			root.add(childNode);
			childrenCache.add(childNode);			
		}
	}	
	
	public boolean needsRefresh() {
		return refreshRequired;
	}
	
	private List<TreeNode<Entity>> loadChildElements(TreeNode<Entity> node){				
		if(node.getChildCount() != 0)
			return new ArrayList<>();
		String parentPath = Paths.getFullPath(node.getData().getParentPath(), node.getData().getName());		
		childrenLoadedCache.add(node);
		return getAllChildren(parentPath);
	}
	
	private List<TreeNode<Entity>> getAllChildren(String parentPath) {
		List<TreeNode<Entity>> children = new ArrayList<>();
		for(Project project : ProjectService.getChildProjects(parentPath, true)) 
			children.add(new CustomTreeNode<Entity>(project, null, false));
		for(Calculation calculation : ProjectService.getChildCalculations(parentPath, true))
			children.add(new CustomTreeNode<Entity>(calculation));						
		return children;
	}

	@Override
	public String getSortDirection(Comparator arg0) {
		CustomTreeitemComparator comparator = (CustomTreeitemComparator)arg0;
		if(comparator == null || comparator.isAscending())
			return "ascending";
		else
			return "descending";
	}
	
	@Override
	public void sort(Comparator arg0, boolean ascending) {			
		CustomTreeitemComparator comparator = (CustomTreeitemComparator)arg0;
		Object[] nodes = childrenCache.toArray(new Object[childrenCache.size()]);
		for(int inx = 0; inx < nodes.length; inx++) {
			TreeNode<Entity> node = (TreeNode<Entity>)nodes[inx];			
			((TreeNodeChildrenList) node.getChildren()).treeSort(comparator);
			this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(node),  0, node.getChildCount());
		}
	}

	public void sort(Comparator arg0, boolean ascending, TreeNode<Entity> node) {
		if(node == null)
			return;				
		if(childrenCache.contains(node)) {
			CustomTreeitemComparator comparator = (CustomTreeitemComparator)arg0;
			((TreeNodeChildrenList) node.getChildren()).treeSort(comparator);
			this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(node),  0, node.getChildCount());
		}
	}
	
	@Override
	public void setSelectionControl(SelectionControl ctrl) {
		
	}

	@Override
	public SelectionControl getSelectionControl() {
		return null;
	}

	@Override
	public boolean isLeaf(TreeNode<Entity> node) {
		Entity dc = (Entity) node.getData();
		if(dc.isCalculation())	
			return true;
		else 
			return false;		
	}

	@Override
	public TreeNode<Entity> getChild(TreeNode<Entity> node, int index) {
		Entity dc = (Entity) node.getData();
		if(dc.isProject()) {			
			if(!childrenCache.contains(node)){
				childrenCache.add(node);
				childrenLoadedCache.add(node);
				for(TreeNode<Entity> childNode : loadChildElements(node))
					node.add(childNode);							
			}
			return node.getChildAt(index);			
		}			
		return null;
	}

	@Override
	public int getChildCount(TreeNode<Entity> node) {
		if(!childrenCache.contains(node)) {
			childrenCache.add(node);
			childrenLoadedCache.add(node);
		}
		for(TreeNode<Entity> childNode : loadChildElements(node))
			node.add(childNode);
		return node.getChildCount();
	}


	/*
	 * External event related operations 
	 */
	public void appendProject(Entity project) {
		String parentPath  = project.getParentPath();
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(parentPath);
		CustomTreeNode<Entity> newNode = new CustomTreeNode<Entity>(project, null, false);
		if(isProjectLoaded(parentNode) && isProjectChildrenLoaded(parentNode)) {						
			parentNode.add(newNode);
			int index = parentNode.getIndex(newNode);
			this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(parentNode),  index, index);
//			reorder();			
		}		
	}	

	public void appendCalculation(Entity calculation) {
		String parentPath  = calculation.getParentPath();
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(parentPath);
		CustomTreeNode<Entity> newNode = new CustomTreeNode<Entity>(calculation);		
		//Parent node exists and calculation has not been loaded (used to discard adding multiple times same calculation if multiple browser tabs are open) 
		if(isProjectLoaded(parentNode) && isProjectChildrenLoaded(parentNode) && findCalculationNodeById(parentNode, calculation.getId()) == null) { 						
			parentNode.add(newNode);
			int index = parentNode.getIndex(newNode);
			this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(parentNode),  index, index);
//			reorder();
		}				
	}
	
	public void updateCalculation(String oldPath, Entity calculation) {		
		String newPath = calculation.getParentPath();		
		// New path defined -> Move element
		if(newPath != null && !Paths.getParent(oldPath).equals(newPath))	 
			moveCalculation(oldPath, newPath);		
		// Update fields
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(calculation.getParentPath());
		CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findCalculationNodeById(parentNode, calculation.getId());		
		if(node != null && node.getData().isCalculation()) {
			node.setData(calculation);
			int index = parentNode.getIndex(node);			
			this.fireEvent(TreeDataEvent.CONTENTS_CHANGED, this.getPath(parentNode),  index, index);
		}
	}
	
	public void updateProject(Entity project, String oldPath, String newPath) {			
		if(!newPath.equals(oldPath)) 							
			moveProject(oldPath, newPath);
			
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(Paths.getParent(newPath));
		CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findAllProjectNodeById(project.getId());				
		if(parentNode != null && node != null && node.getData().isProject()) {									
			int index = parentNode.getIndex(node);
			node.setData(project);
			this.fireEvent(TreeDataEvent.CONTENTS_CHANGED, this.getPath(parentNode),  index, index);
		}	
	}
	
	private void updateChildrenPath(TreeNode<Entity> node, String oldPath, String newPath) {
		//Iterate all child elements changing their path for the new one
		for(TreeNode<Entity> child : node.getChildren()) {
			if(child.getData().isProject())
				updateChildrenPath(child, oldPath, newPath);
			else
				child.getData().setParentPath(child.getData().getParentPath().replace(oldPath, newPath));
		}
		node.getData().setParentPath(node.getData().getParentPath().replace(oldPath, newPath));		
	}

	public void deleteProject(String projectPath) {
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(Paths.getParent(projectPath)); 	
		CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findChildProjectNodeByPath(projectPath);			
		if(node == null)
			return;
		deleteProjectChildren(node);
		int order = node.getData().getElementOrder();
		if(parentNode == null)    				  
			return;		
		//Reduce sibling projects order on tree
		for(TreeNode<Entity> child : parentNode.getChildren()) {
			if(child.getData().isProject() && child.getData().getElementOrder() > order) 
				child.getData().setElementOrder(child.getData().getElementOrder()-1);									
		}
		this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(parentNode),  -1, -1);
	}
	
	
	private void deleteProjectChildren(TreeNode<Entity> node) {
		if(node == null)
			return;
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(node.getData().getParentPath());				
		while(node.getChildCount() > 0) {			
			TreeNode<Entity> child = (TreeNode<Entity>) node.getChildAt(0);
			if(child.getData().isProject())
				deleteProjectChildren(child);
			else 
				node.remove(child);
		}
		childrenCache.remove(node);
		childrenLoadedCache.remove(node);
		parentNode.remove(node);
	}
	
	public void deleteCalculation(String calculationPath) {	
		if(!calculationPath.contains("/"))		
			return;
		CustomTreeNode<Entity> parentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(Paths.getParent(calculationPath));
		if(parentNode == null)    // If parent project is now null, it has been already deleted 
			return;
		String calculationName = Paths.getTail(calculationPath);
		
		// Find element and remove it
		int calculationOrder = Integer.MAX_VALUE; 		
		for(TreeNode<Entity> child : parentNode.getChildren()) { 
			if(child.getData().getName().equals(calculationName)) {
				calculationOrder = child.getData().getElementOrder();
				parentNode.remove(child);
				break;
			}
		}
		//Decrease the order of the next sibling calculations 
		for(TreeNode<Entity> child : parentNode.getChildren()) {
			if(child.getData().isCalculation() && child.getData().getElementOrder() > calculationOrder) 
				child.getData().setElementOrder(child.getData().getElementOrder()-1);							
			
		}
		this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(parentNode),  -1, -1);		
	}
	
	/**
	 * This function moves a Calculation TreeNode from its base path to another path
	 * @param elementPath Calculation path composed of the parent path and calculation name
	 * @param newPath	 New parent path to assign 
	 */
	private void moveCalculation(String elementPath, String newPath) {
		String oldPath = Paths.getParent(elementPath);
		try {
			CustomTreeNode<Entity> oldParentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(oldPath);		
			CustomTreeNode<Entity> newParentNode = (CustomTreeNode<Entity>) findChildProjectNodeByPath(newPath);
			if(oldParentNode != null && newParentNode != null)  {		// Both endpoints are available
				CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findCalculationNodeByPath(oldParentNode, elementPath);
				node.getData().setParentPath(node.getData().getParentPath().replace(oldPath, newPath));
				oldParentNode.remove(node);
				newParentNode.add(node);								
				this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(oldParentNode),  -1, -1);
				this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(newParentNode),  -1, -1);
//				reorder();
			}else if(oldParentNode == null) {	// Can't retrieve moved data from origin node, just set as "refresh-needed"
				refreshRequired = true;
			}else if(newParentNode == null) {	// No end node available, just remove node and its children
				CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findCalculationNodeByPath(oldParentNode, elementPath);
				if(node == null)			// Moved element not loaded yet
					return;
				node.getData().setParentPath(node.getData().getParentPath().replace(oldPath, newPath));
				oldParentNode.remove(node);
				this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(oldParentNode),  -1, -1);
//				reorder();
			}									
		}catch(Exception e) { 			 
			logger.error(e.getMessage());
			refreshRequired = true;				// Something failed, set the treemodel to be refreshed
		}
	}
	
	/**
	 * This function moves a Project TreeNode from its base path to another path
	 * @param elementPath Calculation path composed of the parent path and calculation name
	 * @param newPath	 New parent path to assign 
	 */	
	private void moveProject(String elementPath, String newPath) {		
		String oldPath = Paths.getParent(elementPath);
		try {
			CustomTreeNode<Entity> oldParentNode = (CustomTreeNode<Entity>) findProjectNodeByPath(oldPath);
			CustomTreeNode<Entity> newParentNode = (CustomTreeNode<Entity>) findChildProjectNodeByPath(Paths.getParent(newPath));
			if(oldParentNode != null && newParentNode != null)  {		// Both endpoints are available and different			
				CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findChildProjectNodeByPath(elementPath);
				updateChildrenPath(node, elementPath, newPath);
				oldParentNode.remove(node);
				if(isProjectLoaded(newParentNode))
					newParentNode.add(node);
				this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(oldParentNode),  -1, -1);
				this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(newParentNode),  -1, -1);			
			}else if(oldParentNode == null) {	// Can't retrieve moved data from origin node, just set as "refresh-needed"
				refreshRequired = true;
			}else if(newParentNode == null) {	// No end node available, just remove node and its children				
				CustomTreeNode<Entity> node = (CustomTreeNode<Entity>) findChildProjectNodeByPath(elementPath);
				if(node == null)			// Moved element not loaded yet
					return;
				removeAllChildren(node);				
				oldParentNode.remove(node);													
				this.fireEvent(TreeDataEvent.STRUCTURE_CHANGED, this.getPath(oldParentNode),  -1, -1);			
			}									
		}catch(Exception e) { 			 
			logger.error(e.getMessage());
			refreshRequired = true;				// Something failed, set the treemodel to be refreshed
		}
	}
	
	public TreeNode<Entity> findProjectNodeByPath(String path) {
		Iterator<TreeNode<Entity>> iter = childrenCache.iterator();
		while(iter.hasNext()) {
			TreeNode<Entity> node = iter.next();
			Entity dc = node.getData();
			if(dc.isProject() && dc.getPath().equals(path))				
				return node;
		}					
		return null;
	}

	public TreeNode<Entity> findProjectNodeById(int id) {
		Iterator<TreeNode<Entity>> iter = childrenCache.iterator();
		while(iter.hasNext()) {
			TreeNode<Entity> node = iter.next();
			Entity dc = node.getData();
			if(dc.getId() == id && dc.isProject())
				return node;
		}				
		return null;
	}
	
	
	private TreeNode<Entity> findAllProjectNodeById(int id) {
		Iterator<TreeNode<Entity>> iter = childrenLoadedCache.iterator();
		while(iter.hasNext()) {
			TreeNode<Entity> node = iter.next();
			Entity dc = node.getData();
			if(dc.getId() == id)
				return node;
			else {
				Iterator<TreeNode<Entity>> iter2 = node.getChildren().iterator();
				while(iter2.hasNext()) {
					TreeNode<Entity> child = iter2.next();
					Entity dc2 = child.getData(); 
					if(dc2.getId() == id && dc.isProject())
						return child;
				}
			}
		}
		return null;	
	}

	private TreeNode<Entity> findCalculationNodeById(CustomTreeNode<Entity> parentNode, int id) {
		if(parentNode == null)
			return null;
		for(int inx = 0; inx < getChildCount(parentNode); inx++){
			TreeNode<Entity> childNode = getChild(parentNode, inx);
			if(childNode.getData().isCalculation() && childNode.getData().getId() == id)
				return childNode;
		}
		return null;
	}

	private TreeNode<Entity> findChildProjectNodeByPath(String path) {
		TreeNode<Entity> parent = findProjectNodeByPath(path);
		if(parent == null) {			
			Iterator<TreeNode<Entity>> iter = childrenCache.iterator();
			while(iter.hasNext()) {
				TreeNode<Entity> node = iter.next();
				Iterator<TreeNode<Entity>> iter2 = node.getChildren().iterator();
				while(iter2.hasNext()) {
					TreeNode<Entity> childNode = iter2.next();
					if(childNode.getData().isProject() && childNode.getData().getPath().equals(path))
						return childNode;					
				}
			}
		}
		return parent;		
	}
	
	private boolean isProjectLoaded(TreeNode<Entity> node) {
		return childrenCache.contains(node);
	}
	
	private boolean isProjectChildrenLoaded(TreeNode<Entity> node) {
		return childrenLoadedCache.contains(node);		
	}

	private void removeAllChildren(TreeNode<Entity> node) {
		for(TreeNode<Entity> child :node.getChildren()) {
			if(child.getData().isProject()) {				
				removeAllChildren(node);					
				childrenCache.remove(node);		
				childrenLoadedCache.remove(node);
			}
			node.remove(child);
		}
	}

	private TreeNode<Entity> findCalculationNodeByPath(CustomTreeNode<Entity> parentNode, String elementPath) {
		for(TreeNode<Entity> child : parentNode.getChildren()) 
			if(child.getData().getPath().equals(elementPath)) 
				return child;		
		return null;
	}

	/**
	 * Order related function, received when a project has been moved to another one
	 * @param params
	 */
	public void reorderProjectsOnMove(HashMap<String, Object> params) {
		int projectId = (Integer)params.get("id");
		int oldOrder = (Integer)params.get("oldOrder");
		int newOrder = (Integer)params.get("newOrder");
		String oldParentPath = (String)params.get("oldParentPath");
		//Update moved project order value
		TreeNode<Entity> projectTreeNode = findProjectNodeById(projectId);
		if(projectTreeNode == null) 
			return;
		projectTreeNode.getData().setElementOrder(newOrder);
		
		//Update oldParent order
		TreeNode<Entity> oldParentTreeNode = findProjectNodeByPath(oldParentPath);
		if(oldParentTreeNode == null || !childrenLoadedCache.contains(oldParentTreeNode))
			return;
		for(TreeNode<Entity> child : oldParentTreeNode.getChildren()) {
			Entity element = child.getData();
			if(!element.isProject())
				break;		
			if(element.getElementOrder() > oldOrder)
				element.setElementOrder(element.getElementOrder() - 1);			
		}
	}
	
	/**
	 * Order related function, received when a calculation has been moved over another one
	 * @param params
	 */

	public void reorderCalculationsOnMove(HashMap<String, Object> params) {			
		int id = (Integer)params.get("id");					//Moved calculation id
		int targetId = (Integer)params.get("targetId");		//Dropped calculation id
		int oldOrder = (Integer)params.get("oldOrder");		//Moved calculation order on old path
		int newOrder = (Integer)params.get("newOrder"); 	//Moved calculation in new path
		String oldParentPath = (String)params.get("oldParentPath");	
		String newParentPath = (String)params.get("newParentPath");
		
		TreeNode<Entity> oldParentProject = findProjectNodeByPath(oldParentPath);
		TreeNode<Entity> newParentProject = findProjectNodeByPath(newParentPath);
		
		if(targetId == -1) {							// Dropped from multiple selection, no targetCalculationId => move to last position
			removeCalculationFromProject(oldOrder, oldParentProject);		
			setCalculationOrder(id, newOrder, newParentProject);
		}else {											// Dropped into another calculation
			if(oldParentPath.equals(newParentPath)) {	// Dropped on same project, insert before targetCalculation
				moveCalculationWithinProject(oldParentProject, oldOrder, newOrder);									
			}else {										// Dropped on different project, insert before targetCalculation
				removeCalculationFromProject(oldOrder, oldParentProject);
				moveCalculationIntoProject(newParentProject, newOrder);
			}			
			setCalculationOrder(id, newOrder, newParentProject);
		}		
	}
	
	private void removeCalculationFromProject(int removedCalculationOrder, TreeNode<Entity> projectNode) {
		if(projectNode == null)
			return;
		for(TreeNode<Entity> child : projectNode.getChildren()) {
			Entity element = child.getData();
			if(!element.isCalculation())
				continue;		
			if(element.getElementOrder() > removedCalculationOrder)
				element.setElementOrder(element.getElementOrder() - 1);			
		}
	}
	
	private void moveCalculationWithinProject(TreeNode<Entity >projectNode, int oldOrder, int newOrder) {
		if(projectNode == null)
			return;
		if(oldOrder > newOrder) {
			for(TreeNode<Entity> child : projectNode.getChildren()) {
				Entity element = child.getData();
				if(!element.isCalculation())
					continue;
				int order = Integer.valueOf(element.getElementOrder());
				if(order >= newOrder && order <= oldOrder )
					element.setElementOrder(element.getElementOrder() + 1);				
			}
		}else if(oldOrder < newOrder) {
			for(TreeNode<Entity> child : projectNode.getChildren()) {
				Entity element = child.getData();
				if(!element.isCalculation())
					continue;
				int order = Integer.valueOf(element.getElementOrder());
				if(order >= oldOrder && order <= newOrder )
					element.setElementOrder(element.getElementOrder() - 1);				
			}
		}
	}
	
	private void moveCalculationIntoProject(TreeNode<Entity> projectNode, int newOrder) {
		if(projectNode == null)
			return;
		for(TreeNode<Entity> child : projectNode.getChildren()) {
			Entity element = child.getData();
			if(!element.isCalculation())
				continue;
			int order = Integer.valueOf(element.getElementOrder());
			if(order >= newOrder)
				element.setElementOrder(element.getElementOrder() + 1);		
		}
	}
	
	private void setCalculationOrder(int calculationId, int order, TreeNode<Entity> projectNode) {
		if(projectNode == null)
			return;
		TreeNode<Entity> calculation = findCalculationNodeById((CustomTreeNode<Entity>)projectNode, calculationId);		
		calculation.getData().setElementOrder(order);						
	}
	
}