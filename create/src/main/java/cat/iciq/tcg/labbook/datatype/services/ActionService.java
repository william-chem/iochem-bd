/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Action;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.web.definitions.Queries;

public class ActionService {
	
	private static final Logger log = LogManager.getLogger(ActionService.class);
	
	private static HashMap<Integer, Action> actionById;
	
	static {		
		loadActions();		
	}
	
	private static void loadActions() {
		actionById = new HashMap<>();
					
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_ACTIONS);	        	        	      
	        rs = psql.executeQuery();	        
	        while(rs.next()) {
	        	Action action = new Action();
	        	action.setId(rs.getInt("id"));
	        	action.setMimetype(rs.getString("mimetype"));
	        	action.setJumboFormat(rs.getString("jumbo_format"));
	        	action.setAction(rs.getString("action"));
	        	action.setParameters(rs.getString("parameters"));
	        	//Load cache of Actions
	        	actionById.put(action.getId(), action);	        	
	        }
		} catch (Exception e) {
			log.error("Exception raised loading actions.");
			log.error(e.getMessage());
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}		
	}
	
	public static Action getById(int id) {
		return actionById.get(id);
	}	

	public static List<Action> getAll(){
		return new ArrayList<Action>(actionById.values());
	}
	
}
