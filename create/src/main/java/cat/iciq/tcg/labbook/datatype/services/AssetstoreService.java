/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.HashMap;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.web.definitions.Constants;

public class AssetstoreService {
	
	private static final Logger log = LogManager.getLogger(AssetstoreService.class);
	
	
	public static void deleteCalculation(int calcId){				
    	try {
    		String folder = FileService.getCalculationPath(calcId);
    		FileUtils.forceDelete(new File(folder));
		} catch (Exception e) {			
			log.error(e.getMessage());
		}   
	}
	
    public static HashMap<String,byte[]> getCalculationFilesContent(int id) throws Exception {
    	HashMap<String, byte[]> fileContents = new HashMap<>();    	
    	List<CalculationFile> calculationFiles = CalculationService.getCalculationFiles(id);
    	
    	for(CalculationFile calculationFile : calculationFiles) {
    		File file = new File(FileService.getCreatePath(File.separatorChar + calculationFile.getFile()));
			fileContents.put(file.getName(), FileUtils.readFileToByteArray(file));
    	}
    	return fileContents;
    }

	public static File getCalculationFileByName(long calcId, String name) throws Exception {		
    	return new File(FileService.getCalculationPath(calcId, name));
	}
	
	public static BufferedReader getCalculationFileReaderByName(long calcId, String name) throws Exception {		
		return new BufferedReader(new FileReader(getCalculationFileByName(calcId, name))); 		
	}
	
	public static String getCalculationFilePartialPath(long calcId, String filename) {
	    StringBuilder sb = new StringBuilder();
	    sb.append(Constants.CREATE_ASSETSTORE_DIR);
	    sb.append(File.separatorChar);
	    sb.append(String.valueOf(calcId));
	    sb.append(File.separatorChar);
	    sb.append(filename);
	    return sb.toString();
	}

}
