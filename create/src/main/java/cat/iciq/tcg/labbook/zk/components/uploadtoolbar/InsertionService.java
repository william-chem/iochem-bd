/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.uploadtoolbar;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class InsertionService {
	private static final Logger logger = LogManager.getLogger(InsertionService.class.getName());
	
	private static ExecutorService insertionsService = null;
	private static ExecutorCompletionService<Map<String, String>> insertionCompletionService = null;	
	
	private InsertionService() {
	    throw new IllegalStateException("This is an utility class");
	}	
	
	public static void init() {
		insertionsService = buildExecutorService();		
		insertionCompletionService = new ExecutorCompletionService<>(insertionsService, new InsertionQueue());		
	}

	private static ExecutorService buildExecutorService() {
		int numberOfProcessors = Runtime.getRuntime().availableProcessors();
		if (numberOfProcessors > 1)
			numberOfProcessors = numberOfProcessors - 1;
		return Executors.newFixedThreadPool(numberOfProcessors);
	}

	public static void insertFiles(Map<String,String> params, List<File> fileItems) {
		CalculationInsertion calculationInsertion = new CalculationInsertion(params, fileItems);
		insertionCompletionService.submit(calculationInsertion);
	}

	public static void closeInsertionQueue() {
		insertionsService.shutdown(); // Disable new tasks from being submitted
		try {
			if (!insertionsService.awaitTermination(20, TimeUnit.SECONDS)) {
				insertionsService.shutdownNow(); // Cancel currently executing tasks
				if (!insertionsService.awaitTermination(20, TimeUnit.SECONDS))
					logger.error("loadCalculationPool did not terminate in defined time frame");
			}
		} catch (InterruptedException ie) {
			insertionsService.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
}
