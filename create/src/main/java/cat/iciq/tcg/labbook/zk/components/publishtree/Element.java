/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.components.publishtree;
/**
 * This class holds a generic Browse element, it can be a community, collection or item  
 * @author malvarez
 *
 */
public class Element {
	public enum ElementType { COMMUNITY, COLLECTION, ITEM };
	
	private String browseId;
	private int createId;
	private final ElementType type;
	private String name;	
	private String description;
	private String handle;	
	private String publishedName;
	private boolean published;
	private int order;
	private String path;
	
	public Element(String browseId, int createId, String name, String description, ElementType type, boolean published, String publishedName, String handle, int order, String path) {
		this.browseId = browseId;
		this.createId  = createId;
		this.name = name;	
		this.description = description;
		this.type = type;
		this.published = published;
		this.publishedName = publishedName;
		this.handle = handle;
		this.order = order;
		this.path = path; 
	}

	public String getBrowseId() {
		return browseId;
	}

	public void setBrowseId(String browseId) {
		this.browseId = browseId;
	}

	public int getCreateId() {
		return createId;
	}

	public void setCreateId(int createId) {
		this.createId = createId;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public ElementType getType() {
		return type;
	}

	public boolean isPublished() {
		return published;
	}

	public String getName() {
		return name;
	}

	public String getHandle() {
		return handle;
	}

	public void setHandle(String handle) {
		this.handle = handle;
	}

	public String getPublishedName() {
		return publishedName;
	}

	public void setPublishedName(String publishedName) {
		this.publishedName = publishedName;
	}

	public void setPublished(boolean published) {
		this.published = published;
	}

	public boolean isItem() {
		return this.getType() == ElementType.ITEM;
	}
	
	public boolean isCollection() {
		return this.getType() == ElementType.COLLECTION;
	}
	
	public boolean isCommunity() {
		return this.getType() == ElementType.COMMUNITY;
	}

	public int getOrder() {
		return order;
	}

	public void setOrder(int order) {
		this.order = order;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}	
}
