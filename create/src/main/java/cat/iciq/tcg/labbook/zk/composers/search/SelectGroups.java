/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.search;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.regex.Pattern;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.EventQueue;
import org.zkoss.zk.ui.event.EventQueues;
import org.zkoss.zk.ui.event.Events;
import org.zkoss.zk.ui.event.InputEvent;
import org.zkoss.zk.ui.select.SelectorComposer;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zul.Button;
import org.zkoss.zul.Cell;
import org.zkoss.zul.Grid;
import org.zkoss.zul.Label;
import org.zkoss.zul.Row;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;

public class SelectGroups extends SelectorComposer<Window> {
	private static final long serialVersionUID = 1L;
	private static final Logger log = LogManager.getLogger(SelectGroups.class.getName());
	private static ArrayList<String> groups = new ArrayList<String>(); 
	private AddButtonEventListener addButtonEventListener = null;
	private EventQueue<Event> addGroupsQueue = null;
	
	@Wire 
	Window selectGroup;
	
	@Wire
	Textbox txtName;
	
	@Wire
	Grid grdGroups;

	@Listen("onChanging=#txtName")
	public void onTxtNameChange(InputEvent event){
		clearGroupsGrid();		
		loadGroupsOnGrid(event.getValue());		
	}	
	
	@Listen("onClick=#btnClear")
	public void onBtnClearClick(){
		txtName.setValue("");
		clearGroupsGrid();
		loadGroupsOnGrid("");
	}
	
	@Listen("onClick=#btnClose; onClick=#btnClose2")
	public void onBtnCloseClick(){
		selectGroup.detach();
	}
	
	static{
		try {									
			groups = new ArrayList<String>(Arrays.asList(ShiroManager.getCurrent().getGroups()));
			groups.remove("Administrator");	//Browse administration groups are discarded
			groups.remove("Anonymous");
			Collections.sort(groups);
		} catch (BrowseCredentialsException e) {
			log.error(e.getMessage());
		}
	}

	@Override
	public void doAfterCompose(Window comp) throws Exception {
		super.doAfterCompose(comp);		
		addGroupsQueue = EventQueues.lookup("searchaddgroups", 	EventQueues.DESKTOP, true);
		addButtonEventListener = new AddButtonEventListener(addGroupsQueue);
		onBtnClearClick();
	}
	
	private void clearGroupsGrid(){
		while(grdGroups.getRows().getFirstChild() != null)
			grdGroups.getRows().getFirstChild().detach();			
	}
	
	@SuppressWarnings("unchecked")
	private Collection<String> filterGroupNames(String nameHint){
		nameHint = nameHint.trim();
		return CollectionUtils.select(groups, new ContainsPredicate(nameHint));		
	}
	
	private void loadGroupsOnGrid(String hint){
		Collection<String> results = filterGroupNames(hint);
		for(String result : results)			
			grdGroups.getRows().appendChild(buildGroupRow(result));
		grdGroups.invalidate();
	}	
	
	private Row buildGroupRow(String groupname){
		Row row = new Row();		

		Cell cellBtnAdd = new Cell();
		cellBtnAdd.setAttribute("groupname", groupname);

		Button btnAdd = new Button();
		btnAdd.setAttribute("groupname", groupname);
		btnAdd.setSclass("btn btn-outline-secondary add-group-button ml-2");
		btnAdd.addEventListener(Events.ON_CLICK, addButtonEventListener);
		cellBtnAdd.appendChild(btnAdd);

		Cell cellTxtName = new Cell();
		Label lblName = new Label(groupname);
		cellTxtName.appendChild(lblName);

		row.appendChild(cellBtnAdd);
		row.appendChild(cellTxtName);

		return row;
	}
	
	class ContainsPredicate implements Predicate{
		Pattern namePattern = null;		
		public ContainsPredicate(String hint){
			try{
				namePattern = Pattern.compile(".*" + hint + ".*");
			}catch(Exception e){
				namePattern = Pattern.compile(".*");
			}		
		}		
		@Override
		public boolean evaluate(Object object) {
			String candidate = (String) object;  
			return namePattern.matcher(candidate).matches(); 
		}
	}

	class AddButtonEventListener implements EventListener<Event>{
		EventQueue<Event> addGroupsQueue = null;  
		public AddButtonEventListener(EventQueue<Event> addGroupsQueue){
			this.addGroupsQueue = addGroupsQueue;				
		}
		
		@Override
		public void onEvent(Event event) throws Exception {
			Button selected = (Button)event.getTarget();
			selected.setSclass("btn btn-secondary add-group-button ml-2");
			
			String groupname = (String)(selected.getAttribute("groupname"));
			addGroupsQueue.publish(new Event("groupadded", null, groupname));
		}	
	}
	
}



