package cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph;

import java.util.ArrayList;
import java.util.List;

public class Serie {
	
	private String label;
	private List<Step> steps;
	
	public Serie(String title, List<List<String>> formulas, List<Double> energies, List<Boolean> hasTs, int key) {		
		this.label = title;
		this.steps = new ArrayList<Step>();
		for(int inx = 0; inx < formulas.size(); inx++) {
			Step step = new Step(
					formulas.get(inx).get(0),
					energies.get(inx),
					formulas.get(inx).get(1),
					formulas.get(inx).get(2),
					hasTs.get(inx),
					key);
			steps.add(step);
		}		
	}
	
	public String getLabel() {
		return label;
	}

	public String getLabel(int stepIndex) {		
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			return null;
		return steps.get(stepIndex).getLabel();
	}
	
	public void setLabel(int stepIndex, String label) throws ReactionNetworkException {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			throw new ReactionNetworkException("Step index " + stepIndex + " out of range.");	
		steps.get(stepIndex).setLabel(label);
	}
	
	public String getFormula(int stepIndex) {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			return null;
		return steps.get(stepIndex).getFormula();
	}
	
	public void setFormula(int stepIndex, String formula) throws ReactionNetworkException {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			throw new ReactionNetworkException("Step index " + stepIndex + " out of range.");	
		steps.get(stepIndex).setFormula(formula);
	}
		
	public String getSplittedFormula(int stepIndex) {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			return null;
		return steps.get(stepIndex).getSplittedFormula();
	}
	
	public void setSplittedFormula(int stepIndex, String splittedFormula) throws ReactionNetworkException {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			throw new ReactionNetworkException("Step index " + stepIndex + " out of range.");	
		steps.get(stepIndex).setSplittedFormula(splittedFormula);
	}
	
	public boolean isTransitionState(int stepIndex) {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			return false;
		return steps.get(stepIndex).isTransitionState();
	}	
	
	public void setTransitionState(int stepIndex, boolean isTs) throws ReactionNetworkException {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			throw new ReactionNetworkException("Step index " + stepIndex + " out of range.");	
		steps.get(stepIndex).setTransitionState(isTs);
	}
	
	public int getNumberOfSteps() {
		return steps.size();
	}	
	
	public Step getStep(int stepIndex) {
		if(stepIndex < 0 || stepIndex >= getNumberOfSteps())
			return null;
		return steps.get(stepIndex);
	}
	
	public void addRandomStep(int serie, int position, boolean isTs) {
		String label = "*missing_" + serie + "_" + (position + 1);
		Step step = new Step(label, 0.0d, label, label, isTs, serie);
		steps.add(position, step);	
	}
}
