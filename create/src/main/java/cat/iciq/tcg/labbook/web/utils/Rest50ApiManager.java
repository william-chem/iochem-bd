/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Constants;
import cat.iciq.tcg.labbook.zk.composers.Main;

//DSpace v5.0 
//DSpace-REST communication helper class.
//Documentation sources:
//	https://wiki.duraspace.org/display/DSDOC5x/REST+API
//	https://hc.apache.org/httpcomponents-client-4.4.x/tutorial/html/fundamentals.html#d5e49

public class Rest50ApiManager {

	private static final Logger log = LogManager.getLogger(Rest50ApiManager.class.getName());
	
	private static String url 			= null;
	private String dspaceRestToken;
	private CloseableHttpClient httpclient 	= null;
	private HashMap<String, String> headers = new HashMap<String,String>();

	static{
		url = Main.getBaseUrl() + Constants.REST_ENDPOINT;		
	}
	
	public Rest50ApiManager() throws Exception{
		httpclient = RestManager.buildAcceptAllHttpClient();
		dspaceRestToken = loginPost();
		headers.put("rest-dspace-token", dspaceRestToken);
		headers.put("Content-Type", "application/xml");
	}
	
	public boolean areValidUserCredentials() {
		return dspaceRestToken != null;
	}
	
	public void close() throws ClientProtocolException, IOException{
		try {
			logoutPost();
		} catch (Exception e) {
			log.error("Exception raised during REST Api logout. " + e.getMessage());
		}
	}
	
	private String loginPost() throws ClientProtocolException, IOException, BrowseCredentialsException {
		String salt = CustomProperties.getProperty("module.communication.secret");
		String email = ShiroManager.getCurrent().getUserEmail();
		String timestamp = new SimpleDateFormat("yyyy/MM/dd 'at' hh:mm:ss z", Locale.getDefault()).format(new Date(System.currentTimeMillis()));		
		String secret =  email + "#" + timestamp;		
		String token = TokenManager.encode(salt, secret);
		
		HttpPost httpPost = new HttpPost(url.concat(Constants.REST_LOGIN_ENDPOINT + "/" + token));
		httpPost.setEntity(new StringEntity(buildLoginJsonEntity(email, timestamp), ContentType.create("application/json")));	
		HttpResponse response = httpclient.execute(httpPost);
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		
		switch(response.getStatusLine().getStatusCode()){
	            case 200:	return rd.readLine();
	        				        		
		    default:	String line = "";
	        		while ((line = rd.readLine()) != null) 
	        			log.error(line);	       
	        		return null;
	    }
	}
	
	private String buildLoginJsonEntity(String email, String timestamp){
		StringBuilder entity = new StringBuilder();
		entity.append("{");
		entity.append("  \"user\":\"" + email + "\",");
		entity.append("  \"timestamp\":\"" +  timestamp + "\"");		
		entity.append("}");
		return entity.toString();		
	}
	
	
	private void logoutPost() throws ParseException, Exception{
		HttpEntity entity = new StringEntity("", ContentType.create("application/json"));		
		String result = RestManager.postRequest(httpclient,url.concat(Constants.REST_LOGOUT_ENDPOINT), entity , headers);
		log.info(result);		
	}
	
	
	public XMLFileManager getTopCommunities() throws ClientProtocolException, IOException, SAXException, ParserConfigurationException{
		return RestManager.getRequestAsXml(httpclient, url.concat(Constants.REST_GET_TOPCOMMUNITIES_ENDPOINT), headers);		
	}
	
	public XMLFileManager getCommunities() throws ClientProtocolException, IOException, SAXException, ParserConfigurationException{
		return RestManager.getRequestAsXml(httpclient, url.concat(Constants.REST_GET_COMMUNITIES_ENDPOINT), headers);
	}
	
	public XMLFileManager getCommunityCommunities(String parentCommunityId) throws Exception{
		String endPoint = Constants.REST_GET_COMMUNITY_COMMUNITIES_ENDPOINT.replace("{communityId}", parentCommunityId); 
		return RestManager.getRequestAsXml(httpclient, url.concat(endPoint), headers);	
	}
	
	public XMLFileManager getCommunityCollections(String parentCommunityId) throws Exception{
		String endPoint = Constants.REST_GET_COMMUNITY_COLLECTIONS_ENDPOINT.replace("{communityId}", parentCommunityId);
		return RestManager.getRequestAsXml(httpclient, url.concat(endPoint), headers);
	}
	
	public XMLFileManager getAdministratorCommunities() throws ClientProtocolException, IOException, SAXException, ParserConfigurationException {	
		String endPoint = Constants.REST_ADMINISTRATOR_COMMUNITIES;
		return RestManager.getRequestAsXml(httpclient, url.concat(endPoint), headers);
	}
	
	public XMLFileManager getCollection(int collectionId) throws Exception {	
		String endPoint = Constants.REST_GET_COLLECTION_ENDPOINT.replace("{collectionId}", String.valueOf(collectionId));
		return RestManager.getRequestAsXml(httpclient, url.concat(endPoint), headers);	
	}	
	
	public int postCollection(String communityId, String collectionName) throws Exception{
		String endPoint = Constants.REST_POST_COLLECTION_ENDPOINT.replace("{communityId}", communityId);
		HttpPost httpPost = new HttpPost(url.concat(endPoint));
		httpPost.setHeader("rest-dspace-token",dspaceRestToken);
		httpPost.setHeader("Accept", "application/json");
		httpPost.setEntity(new StringEntity(buildCreateCollectionEntity(collectionName), ContentType.create("application/xml", "UTF-8")));
		HttpResponse r = httpclient.execute(httpPost);
		String response = EntityUtils.toString(r.getEntity());
		if(r.getStatusLine().getStatusCode() == 200){			
			ObjectMapper mapper = new ObjectMapper();
			Map<String,Object> map = mapper.readValue(response, Map.class);
			return (Integer)map.get("id");		    		    
		}
		return -1;
	}
	
	private String buildCreateCollectionEntity(String collectionName) throws ParserConfigurationException, TransformerException {
	    // Sanitise collection name    
	    collectionName = StringUtils.replaceEach(collectionName, new String[]{"&", "\"", "<", ">", "\'"}, new String[]{"&amp;", "&quot;", "&lt;", "&gt;", "&#x27;"});
	    
	    DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
	    Document newDoc = builder.newDocument();
	    Element root = newDoc.createElement("collection");
	    newDoc.appendChild(root);
	    
	    Element name = newDoc.createElement("name");
	    name.setTextContent(collectionName);	    
	    root.appendChild(name);
	    
	    Element desc = newDoc.createElement("shortDescription");
        desc.setTextContent(collectionName);        
        root.appendChild(desc);
	    
	    Element type = newDoc.createElement("type");
        type.setTextContent("CREATED BY XML (POST)");        
        root.appendChild(type);
       
	    Element copyright = newDoc.createElement("copyrightText");
	    copyright.setTextContent("Share - copy and redistribute the material in any medium or format. Adapt -  remix, transform, and build upon the material for any purpose, even commercially. The licensor cannot revoke these freedoms as long as you follow the license terms. ");	    
	    root.appendChild(copyright);	    

	    TransformerFactory tf = TransformerFactory.newInstance();
	    Transformer transformer = tf.newTransformer();
	    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
	    StringWriter writer = new StringWriter();
	    transformer.transform(new DOMSource(newDoc), new StreamResult(writer));
	    
	    return writer.getBuffer().toString().replaceAll("\n|\r", "");
    }

    public void postCreateCollection(String collectionHandle) throws Exception{
		String endPoint = Constants.REST_OAI_COLLECTION_ENDPOINT.replace("{collectionHandle}",collectionHandle.replace("/", "_"));
		HttpPost httpPost = new HttpPost(url.concat(endPoint));
		httpPost.setHeader("rest-dspace-token",dspaceRestToken);		
		HttpResponse r = httpclient.execute(httpPost);
		if(r.getStatusLine().getStatusCode() != 200){
			throw new Exception("Error raised sending collection creation request for handle :" + collectionHandle);
		}		
	}
	
	public void putUpdateCollection(String collectionHandle) throws Exception{
		String endPoint = Constants.REST_OAI_COLLECTION_ENDPOINT.replace("{collectionHandle}",collectionHandle.replace("/", "_"));
		HttpPut httpPut = new HttpPut(url.concat(endPoint));
		httpPut.setHeader("rest-dspace-token",dspaceRestToken);		
		HttpResponse r = httpclient.execute(httpPut);
		if(r.getStatusLine().getStatusCode() != 200){
			throw new Exception("Error raised sending collection update request for handle :" + collectionHandle);
		}
	}
	
}
