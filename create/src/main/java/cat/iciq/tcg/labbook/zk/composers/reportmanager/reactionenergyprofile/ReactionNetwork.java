package cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile;

import java.io.StringWriter;
import java.io.Writer;
import java.rmi.server.ExportException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultUndirectedGraph;
import org.jgrapht.graph.Multigraph;
import org.jgrapht.nio.Attribute;
import org.jgrapht.nio.DefaultAttribute;
import org.jgrapht.nio.dot.DOTExporter;

import cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph.ReactionNetworkException;
import cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph.Serie;
import cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph.Step;
import cat.iciq.tcg.labbook.zk.composers.reportmanager.reactionenergyprofile.graph.StepEdge;

public class ReactionNetwork {
	
	private final String[] SERIE_COLORS = {"#ff0029", "#377eb8", "#66a61e", "#984ea3", "#00d2d5", "#ff7f00", "#af8d00", "#7f80cd", "#b3e900", "#c42e60", "#a65628", "#f781bf", "#8dd3c7", "#bebada", "#fb8072", "#80b1d3", "#fdb462", "#fccde5", "#bc80bd", "#ffed6f", "#c4eaff", "#cf8c00", "#1b9e77", "#d95f02", "#e7298a", "#e6ab02", "#a6761d", "#0097ff", "#00d067", "#000000", "#252525", "#525252", "#737373", "#969696", "#bdbdbd", "#f43600", "#4ba93b", "#5779bb", "#927acc", "#97ee3f", "#bf3947", "#9f5b00", "#f48758", "#8caed6", "#f2b94f", "#eff26e", "#e43872", "#d9b100", "#9d7a00", "#698cff"};
	private final static int LEGEND_SERIE_PER_LINE = 4;
	private Graph<Step, StepEdge> g; 
	private ShuntingYard sy;	
	private List<Serie> series;
	private HashMap<String, List<Step>> steps;
	private HashMap<String, Step> stepsReduced;

	private List<Boolean> transitionStates;			
	private final Pattern CALCULATION_INDEX_PATTERN = Pattern.compile("(?:[cC]([0-9]+))");

	public ReactionNetwork() {
		 sy = new ShuntingYard();
		 series = new ArrayList<Serie>();
		 steps = new HashMap<String, List<Step>>();
		 transitionStates = new ArrayList<Boolean>();
	}

	public void addTS(List<Boolean> transitionStates) {
		this.transitionStates = transitionStates;
	}
	
	public List<Boolean> getTS() {
		return transitionStates;
	}
 	
	public void addSerie(String title, List<List<String>> serieFormulas, List<Double> energies) {
		List<Boolean> hasTs = new ArrayList<Boolean>();
		int inx = 0;
		for(List<String> stepFormulas : serieFormulas) {
			String splittedFormula = splitElements(stepFormulas.get(1)); 		//Set formula splitting negatives from positives : from -> to
			stepFormulas.add(splittedFormula);
			hasTs.add((inx++ < serieFormulas.size()-1) && hasTransitionState(splittedFormula));  //Define also if it contains calculations with TS, last step is considered not a TS
		}					
		series.add(new Serie(title, serieFormulas, energies, hasTs, series.size()));		
	}

	private boolean hasTransitionState(String formula) {		
		boolean hasTs = false;
		
		String calculations[] = formula.split("\t<->\t");
		if(calculations.length<=1)
			return false;
		Matcher matcher = CALCULATION_INDEX_PATTERN.matcher(calculations[1]);		
		while(matcher.find()) {
			String calcIndexStr = matcher.group(1);
			if(calcIndexStr != null) {
				int calcIndex = Integer.valueOf(calcIndexStr);
				if(transitionStates.size()< calcIndex-1)	
					continue;
				hasTs = hasTs || transitionStates.get(calcIndex -1);	
			}			  
		}		
		return hasTs;
	}

	private String splitElements(String formula) {
		String postfix = sy.postfix(prepareLine(formula));			
		return processExpression(tokenizeIntoStack(postfix));
	}

	private String prepareLine(String line) {
		line = line.replaceAll("\\s\\t","");
		StringBuilder sb = new StringBuilder();

		boolean lastCharIsOperator = true;
		for(int inx = 0; inx < line.length(); inx++) {
			Character c = line.charAt(inx);
			if(Character.isDigit(c) && lastCharIsOperator) 
					sb.append("_");
			sb.append(c);
			lastCharIsOperator = (c == '+' || c == '-' || c == '*' || c == '/' );			
		}

		return sb.toString();
	}

	private List<String> tokenizeIntoStack(String formula){
		String regex = "([\\w]?[0-9\\.]+|\\+|\\-|\\/|\\*|\\%)?";
		List<String> elements = new ArrayList<String>();
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(formula);
		while(matcher.find()) {
			String group = matcher.group();
			elements.add(group.replace("_", ""));
		}
		return elements;
	}

	private String processExpression(List<String> tokens) {
		Pattern numberPattern = Pattern.compile("[0-9\\.]+");
		
		List<Expression> expressions = new ArrayList<Expression>();
		for(String token : tokens) {
			if(token.matches("[\\+\\-\\/\\*\\%]")) {				
				Expression first = expressions.get(expressions.size()-1);
				Expression last = expressions.get(expressions.size()-2);
				expressions.remove(expressions.size()-1);
				expressions.remove(expressions.size()-1);
				if(token.equals("+")) {					
					first = last.plus(first);
				}else if(token.equals("-")) {					
					first = last.minus(first);
				}else if(token.equals("*")) {					
					first = last.times(first);					
				}else if(token.equals("/")) {				
					first = last.div(first);					
				}
				expressions.add(first);
			} else {
				if(numberPattern.matcher(token).matches()) {
					if(!token.contains("."))
						expressions.add(new Expression(Integer.parseInt(token)));
					else
						expressions.add(new Expression(getFraction(token)));
				}else
					expressions.add(new Expression(token));
			}
		}
		String normalizedFormula = calculateDecimals(expressions.get(0).toString());
		return groupBySign(normalizedFormula);
	}

	private String calculateDecimals(String formula) {
		Pattern fraction = Pattern.compile("([0-9]+)\\/([0-9]+)");		
		Matcher matcher = fraction.matcher(formula);
		if(matcher.find()) {			
			Double value = Double.valueOf(matcher.group(1)) / Double.valueOf(matcher.group(2));
			return formula.replaceAll("([0-9]+\\/[0-9]+)", value.toString());
		}else
			return formula;
	}

	private String groupBySign(String formula) {
		StringBuilder sb = new StringBuilder();
		List<String> negativesList = new ArrayList<String>();
		List<String >positivesList = new ArrayList<String>();
		
		Pattern positives = Pattern.compile("(\\+\\s?[0-9]?[A-Za-z]+[0-9]+|^\\s*[0-9]?[A-Za-z]+[0-9]+)");
		Pattern negatives = Pattern.compile("(\\-\\s?[0-9]?[A-Za-z]+[0-9]+)");
		Matcher matcherNegatives = negatives.matcher(formula);
		
		while(matcherNegatives.find()) 
			negativesList.add(matcherNegatives.group(1).replaceAll("\\s*\\-\\s*", ""));
		
		Matcher matcherPositives = positives.matcher(formula);
		while(matcherPositives.find()) 
			positivesList.add(matcherPositives.group(1).replaceAll("\\s*\\+\\s*", ""));
		
		sb.append(String.join(" + ", negativesList));
		sb.append("\t<->\t");
		sb.append(String.join(" + ", positivesList));
		return sb.toString();
	}

	private Fraction getFraction(String token) {
		try {
			long numerator = Long.parseLong(token.replaceAll("\\.", ""));
			int position = token.replaceAll("^[0-9]+\\.", "").length();
			long denominator = ((Double)Math.pow(10, position)).longValue();
			return new Fraction(numerator, denominator);  			
		}catch(Exception e) {
			System.out.println(e.getMessage());
			return null;
		}		
	}

	/* 
	 * Reaction Network generation functions
	 */
	
	public String buildGraph() throws Exception {
		fixSeries();
		buildStepMap();
		createGraph();
		return renderGraphAsDOT(g);		
	}
	
	private void fixSeries() throws ReactionNetworkException {
		int serieInx = 0;
		for(Serie serie: series) {
			int stepInx = 0;
			fixReactants(serie);						
			do{					
				if(!serie.isTransitionState(stepInx) && stepInx < serie.getNumberOfSteps() -1  && !serie.isTransitionState(stepInx+1)) 		// Two Intermediates, need a TS in the middle  
					serie.addRandomStep(serieInx, stepInx + 1, true);				
				else if(serie.isTransitionState(stepInx) && (stepInx == serie.getNumberOfSteps() -1 || serie.isTransitionState(stepInx+1))) 	// Last element is TS , or have two TS, need an node in the end/middle
					serie.addRandomStep(serieInx, stepInx + 1, false);					
				stepInx++;
			}while(stepInx < serie.getNumberOfSteps());			
			serieInx++;
		}
	}
	
	private void fixReactants(Serie serie) throws ReactionNetworkException {
		String reactants = "";		
		String splittedFormula = serie.getSplittedFormula(0);
		if (splittedFormula.equals("\t<->\t")) {
			if(serie.getNumberOfSteps() > 1) {			// First step is empty: retrieve reactants from next step
				reactants = serie.getSplittedFormula(1).replaceAll("\t<->\t.*", "");
				serie.setSplittedFormula(0, reactants);				
				if(serie.getSplittedFormula(serie.getNumberOfSteps()-1).equals("\t<->\t"))		// Last series step is also empty (trying to close cycle)
					serie.setSplittedFormula(serie.getNumberOfSteps()-1, reactants);				
			} else
				throw new ReactionNetworkException("Can't determine serie reactants.");
		}
	}

	private void buildStepMap() {
		for(Serie serie: series) 				
			for(int step = 0; step < serie.getNumberOfSteps(); step++)
				addStep(serie.getSplittedFormula(step), serie.getStep(step));	
			
		stepsReduced = new HashMap<>();
		for(String formula: steps.keySet()) {
			stepsReduced.put(formula, buildStep(steps.get(formula)));			
		}		
	}
	
	private void addStep(String formula, Step step) {
		if(!steps.containsKey(formula)){
			List<Step> stepArray = new ArrayList<Step>(); 
			stepArray.add(step);
			steps.put(formula,  stepArray);
		} else {
			steps.get(formula).add(step);
		}		
	}
	
	private void createGraph() throws Exception {
		g = new Multigraph<>(StepEdge.class);
		int serieIndex = 0;
		for(Serie serie: series) {
			for(int stepIndex = 0; stepIndex < serie.getNumberOfSteps(); stepIndex++)  // The out of range step will close the graph 
				addNodeOrEdge(serie, serieIndex, stepIndex, SERIE_COLORS[serieIndex % (SERIE_COLORS.length -1)]);
			serieIndex++;			
		}
		
		int inx = 1;
		for(Step step : g.vertexSet())
			step.setProperty("id", "node" + inx++);
			
	}

	private void addNodeOrEdge(Serie serie, int serieIndex, int stepIndex, String color) throws Exception {
		if(stepIndex == serie.getNumberOfSteps()-1) {			// Last step
			if(serie.getSplittedFormula(stepIndex).equals(serie.getSplittedFormula(0))) {	// Its a cycle
				if(serie.getStep(stepIndex-1).getLabel().contains("*missing_")) {					
					addEdge(stepsReduced.get(serie.getSplittedFormula(stepIndex-2)), stepsReduced.get(serie.getSplittedFormula(0)), new Step("Closing", 0.0, "" , "", true, serieIndex+1), color, serieIndex);					
				}else {
					addEdge(stepsReduced.get(serie.getSplittedFormula(stepIndex-2)), stepsReduced.get(serie.getSplittedFormula(stepIndex)), serie.getStep(stepIndex-1), color, serieIndex);
				}
			}else {
				g.addVertex(stepsReduced.get(serie.getSplittedFormula(stepIndex)));
				addEdge(stepsReduced.get(serie.getSplittedFormula(stepIndex-2)), stepsReduced.get(serie.getSplittedFormula(stepIndex)), serie.getStep(stepIndex-1), color, serieIndex);			
			}
		}else if(stepIndex % 2 == 0) {							// Is first node 
			g.addVertex(stepsReduced.get(serie.getSplittedFormula(stepIndex)));
			if(stepIndex > 0) 									// Is not 
				addEdge(stepsReduced.get(serie.getSplittedFormula(stepIndex-2)), stepsReduced.get(serie.getSplittedFormula(stepIndex)), serie.getStep(stepIndex-1), color, serieIndex);			
		}
	}
	
	private Step buildStep(List<Step> stepArray){
		StringBuilder tooltipText = new StringBuilder();
		Step step = stepArray.get(0); 
		if(stepArray.size() == 1) {
			tooltipText.append(series.get(Integer.valueOf(step.getSerie())).getLabel() + ":\t\t" + step.getEnergy());			
			step.setProperty("tooltip", tooltipText.toString());
			return step;
		} else {
			String key[] = new String[stepArray.size()];
			String energies[] = new String[stepArray.size()];
			for(int inx=0; inx < stepArray.size(); inx++) {
				key[inx] = stepArray.get(inx).getSerie();
				energies[inx] = String.valueOf(stepArray.get(inx).getEnergy());
				tooltipText.append(series.get(Integer.valueOf(stepArray.get(inx).getSerie())).getLabel() + ":\t\t");
				tooltipText.append(stepArray.get(inx).getEnergy() + "\\n");
			}
			
			step.setProperty("key", String.join(",", key));
			step.setProperty("energies", String.join(",", energies));
			step.setProperty("tooltip", tooltipText.toString());			
			return step;
		}
	}
	
	private void addEdge(Step source, Step dest, Step current, String color, int serieIndex) throws Exception {		
		StepEdge edge = (StepEdge) g.addEdge(source, dest);		
		current.setProperty("color", color);
		current.setProperty("key", String.valueOf(serieIndex));
		edge.setStep(current);		
	}
	
	private String renderGraphAsDOT(Graph<Step, StepEdge> graph) throws ExportException {
		Writer writer = new StringWriter();
		Graph<Step, StepEdge> legend = createLegendGraph();
		getLegendDOTExporter().exportGraph(legend, writer);
		String legendString = writer.toString();		
		legendString = legendString.replace("strict graph G {", " rankdir=LR \n  nodesep=0.05 \nsubgraph legend {");				
		writer = new StringWriter();
		getDOTExporter().exportGraph(graph, writer);
		String graphString = writer.toString();
		graphString = graphString.replace("graph G {", "graph G {\n" + legendString + "\n");		
		
		return graphString;
	}

	
	private Graph<Step, StepEdge> createLegendGraph() {	
		Graph<Step, StepEdge> legend = new DefaultUndirectedGraph<>(StepEdge.class);
		HashMap<String, Step> vertexs = new HashMap<>();		
		int serieIndex = 0;
		for(int steps = 0; steps <= (series.size() + (series.size() / LEGEND_SERIE_PER_LINE)); steps++) {
			Step step = new Step("", 0.0d, "","", false, serieIndex);
			step.setProperty("style", "invis");
			step.setProperty("shape", "point");
			step.setProperty("id", "a" + steps);
			legend.addVertex(step);	
			vertexs.put(step.getProperty("id"), step);
		}
		
		int stepIndex = 0;
		for(Serie serie: series) {
			if( (stepIndex + 1) % (LEGEND_SERIE_PER_LINE + 1) == 0 && stepIndex != 0) 
				stepIndex++;
			StepEdge edge = legend.addEdge(vertexs.get("a" + stepIndex++), vertexs.get("a" + stepIndex));
			Step vertex = new Step(serie.getLabel(), 0.0d, "","", false, legend.edgeSet().size() -1);
			vertex.setProperty("color", SERIE_COLORS[serieIndex % (SERIE_COLORS.length -1)]);
			vertex.setProperty("style", "solid");
			vertex.setProperty("class", "serie");
			edge.setStep(vertex);				
			serieIndex++;
		}
		return legend;
	}
	
	public DOTExporter<Step, StepEdge> getLegendDOTExporter() {
		DOTExporter<Step, StepEdge> exporter = new DOTExporter<>();
				
		exporter.setVertexIdProvider((e) ->{
			return e.getProperty("id");			
		});
		
		exporter.setVertexAttributeProvider((v) -> {
			Map<String, Attribute> map = new LinkedHashMap<>();			
			map.put("style", DefaultAttribute.createAttribute(v.getProperty("style")));
			map.put("shape", DefaultAttribute.createAttribute(v.getProperty("shape")));			
			return map;
		});
		exporter.setEdgeAttributeProvider((e) -> {			
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("label", DefaultAttribute.createAttribute(e.getStep().getLabel()));			
			map.put("key", DefaultAttribute.createAttribute(e.getStep().getSerie()));			
			map.put("color", DefaultAttribute.createAttribute(e.getStep().getProperty("color")));
			map.put("style", DefaultAttribute.createAttribute(e.getStep().getProperty("style")));
			map.put("class", DefaultAttribute.createAttribute(e.getStep().getProperty("class")));	
			
			return map;
		});
		return exporter;
	}

	public DOTExporter<Step, StepEdge> getDOTExporter() {
		DOTExporter<Step, StepEdge> exporter = new DOTExporter<>();

		exporter.setVertexAttributeProvider((v) -> {
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("id", DefaultAttribute.createAttribute(v.getProperty("id")));
			map.put("label",  DefaultAttribute.createAttribute(v.toString()));						
			map.put("energy", DefaultAttribute.createAttribute(v.getProperty("energies") != null ? v.getProperty("energies"): String.valueOf(v.getEnergy())));
			map.put("key", 	  DefaultAttribute.createAttribute(v.getProperty("key") != null      ? v.getProperty("key")     : v.getSerie()));
			map.put("calculations", DefaultAttribute.createAttribute(v.getSplittedFormula()));
			if(v.getProperty("tooltip") != null)
				map.put("tooltip", DefaultAttribute.createAttribute(v.getProperty("tooltip")));						
			return map;
		});
		exporter.setEdgeAttributeProvider((e) -> {			
			Map<String, Attribute> map = new LinkedHashMap<>();
			map.put("label", DefaultAttribute.createAttribute(e.getStep().toString()));			
			map.put("calculations", DefaultAttribute.createAttribute(e.getStep().getSplittedFormula()));
			map.put("energy", DefaultAttribute.createAttribute(e.getStep().getEnergy()));
			map.put("color", DefaultAttribute.createAttribute(e.getStep().getProperty("color")));
			map.put("key", DefaultAttribute.createAttribute(e.getStep().getProperty("key")));
			map.put("labeltooltip", DefaultAttribute.createAttribute(
											series.get(Integer.valueOf(e.getStep().getProperty("key"))).getLabel() +
											":\t\t" + 
											e.getStep().getEnergy()));
			return map;
		});
		return exporter;
	}	
}
