/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.event.Event;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.CalculationFile;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.definitions.GeneralConstants;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.MetsFileHandler;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Queries;
import cat.iciq.tcg.labbook.web.definitions.XpathQueries;
import cat.iciq.tcg.labbook.web.utils.XMLFileManager;
import cat.iciq.tcg.labbook.zk.composers.main.navigation.TreeEvent;

public class CalculationService {

	private static final Logger log = LogManager.getLogger(CalculationService.class);
	
	public static boolean calculationExists(String path, String name) throws Exception{
		Connection conn = null;
		try {
			conn = DatabaseHandler.getCreateConnection();
			PreparedStatement psql = conn.prepareStatement(Queries.existsCalculationWithPathAndName);
	        psql.setString(1,path);
	        psql.setString(2, name);	        
	        ResultSet rs = psql.executeQuery();
	        rs.next();
	        return rs.getInt(1) > 0; 
		} catch (Exception e) {
			throw e;			
		} finally{
			DatabaseHandler.closeConnection(conn);
		}
	}
	
	public static Calculation getById(long calculationId) {
		Connection conn = null;		
		PreparedStatement psql = null;
		ResultSet rs = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();	        
	    	psql = conn.prepareStatement(Queries.GET_CALCULATION_BY_ID);
			psql.setBigDecimal(1,new BigDecimal(calculationId));	        
	        rs = psql.executeQuery();
	        if(rs.next())
	        	return readCalculationFromResultSet(rs);
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}
		return null;
	}	
	
	
	public static Calculation getByPath(String path) {
		Connection conn = null;		
		PreparedStatement psql = null;
		ResultSet rs = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();	        
	    	psql = conn.prepareStatement(Queries.GET_CALCULATION_BY_PATH);
			psql.setString(1, Paths.getParent(path));	        
			psql.setString(2, Paths.getTail(path));
	        rs = psql.executeQuery();
	        if(rs.next())
	        	return readCalculationFromResultSet(rs);
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}
		return null;		
	}		
	
	protected static Calculation readCalculationFromResultSet(ResultSet rs) throws SQLException {
		Calculation calculation = new Calculation();
		calculation.setId(rs.getBigDecimal("id").intValue());
		calculation.setType(CalculationTypeService.getCalculationTypeById(rs.getInt("type_id")));
		calculation.setCreationDate(rs.getTimestamp("creation_time"));
		calculation.setName(rs.getString("name"));
		calculation.setParentPath(rs.getString("path"));
		calculation.setDescription(rs.getString("description"));
		calculation.setMetsXml(rs.getString("mets_xml"));
		calculation.setPublished(rs.getBoolean("published"));
		calculation.setHandle(rs.getString("handle"));
		calculation.setPublished_name(rs.getString("published_name"));
		calculation.setPublicationDate(rs.getTimestamp("publication_time"));
		calculation.setElementOrder(rs.getInt("element_order"));
		return calculation;		
	}	
	
	private synchronized static long getNextCalculationId(Connection conn) throws Exception {				
		PreparedStatement psql = null;
		ResultSet rs = null;		
		try {			
			psql = conn.prepareStatement(Queries.GET_CALCULATION_NEXT_ID);
			rs = psql.executeQuery();
			rs.next();
			return rs.getLong(1);				
		}catch(Exception e) {
			log.error(e.getMessage());
			throw e;
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);			
		}
	}
	
	public static long addCalculation(String path, String name, String description, int calculationType) throws Exception {
	    Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;
		long id = 0;
        if(getByPath(Paths.getFullPath(path, name)) != null) {
            throw new Exception("A calculation with such name already exists on destination project.");
        }

		try {
			conn = DatabaseHandler.getCreateConnection();
			id = getNextCalculationId(conn);			
			psql = conn.prepareStatement(Queries.CREATE_CALC);
			psql.setLong(1, id);
			psql.setInt(2, calculationType);
			psql.setString(3, name);
			psql.setString(4, path);
			psql.setString(5, description);			
			psql.setInt(6, OrderService.getNewCalculationOrderIndex(path));
			psql.execute();			
			return id; 				
		}catch(Exception e) {
			log.error(e.getMessage());
			throw e;
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}			
	}

	public static void deleteCalculation(int id) throws Exception {
		deleteCalculation(id, true);
	}
	
	public static void deleteCalculation(int id, boolean checkPermissions) throws Exception {
    	Connection conn = null;
    	PreparedStatement psql = null;
    	ResultSet rs = null;
    	Calculation calculation = CalculationService.getById(id);
    	if(calculation == null)
    		return;
    	try	{    		
    		conn = DatabaseHandler.getCreateConnection();    		
    		conn.setAutoCommit(false);
    		if(checkPermissions && 
    				!PermissionService.hasPermissionOnCalculation(id, Permissions.DELETE))
    			throw new Exception("Not enough permissions to delete calculation.");    			
    		    		
    		//Delete areas_ref rows
			psql = conn.prepareStatement(Queries.DELETE_CALCULATION_AREAS_REF_BY_CALC_ID);
			psql.setInt(1, id);
			psql.executeUpdate();	
			psql.close();
			//Delete calculation row
			psql = conn.prepareStatement(Queries.DELETE_CALCULATION_BY_ID);
			psql.setInt(1, id);
			psql.executeUpdate();
			
			AssetstoreService.deleteCalculation(id);
			
			OrderService.reduceCalculationSiblingsOrder(conn, calculation.getElementOrder(), calculation.getParentPath());
			TreeEvent.sendEventToUserQueue(new Event("calculationDeleted", null, calculation.getPath()));
    	}catch(Exception e){
    		log.error(e.getMessage());
    		throw e;
    	}finally{
    		DatabaseHandler.resetAutocommit(conn);
    		DatabaseHandler.closeStatementResult(psql, rs);
    		DatabaseHandler.closeConnection(conn);
    	}
    }

    public static List<CalculationFile> getCalculationFiles(int id){
    	List<CalculationFile> files = new ArrayList<CalculationFile>();
    	
    	Connection conn = null;		
		PreparedStatement psql = null;
		ResultSet rs = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();	        
	    	psql = conn.prepareStatement(Queries.GET_CALCULATION_FILES_BY_CALC_ID);	    	
			psql.setBigDecimal(1,new BigDecimal(id));	        
	        rs = psql.executeQuery();
	        while(rs.next()) {
	        	CalculationFile calcFile = new CalculationFile();
	        	calcFile.setId(rs.getInt("id"));
	        	calcFile.setCalculationId(rs.getInt("calculation_id"));
	        	calcFile.setName(rs.getString("name"));
	        	calcFile.setFile(rs.getString("file"));
	        	files.add(calcFile);	        
	        }	        
		} catch (Exception e) {
			log.error(e.getMessage());
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);
		}
		return files;
    } 
    
    
    public static void moveOverProject(Calculation calculation, Project project) throws Exception {    	    	    	
    	update(calculation, project.getPath());
    	//Path has been changed, reorder its surrounding siblings
    	OrderService.moveCalculation(calculation, 
										null,
										calculation.getParentPath(),
										project.getPath());
    }      
    
    
    public static void moveOverCalculation(Calculation sourceCalculation, Calculation destinationCalculation) throws Exception {    	    	    
    	update(sourceCalculation, destinationCalculation.getParentPath());
    	//Path has been changed, reorder its surrounding siblings
    	OrderService.moveCalculation(sourceCalculation, 
										destinationCalculation,
										sourceCalculation.getParentPath(),
										destinationCalculation.getParentPath());
    }

    public static void update(Calculation modifiedCalculation) throws Exception {    	
    	update(modifiedCalculation, modifiedCalculation.getParentPath());
    }
    
	public static void update(Calculation modifiedCalculation, String newPath) throws Exception {		
		if(!isValidPath(modifiedCalculation.getParentPath()) || !PermissionService.hasPermissionOnProject(modifiedCalculation.getParentPath(), Permissions.WRITE))			
			throw new Exception("Can't update calculation, not a valid destination path");
		
		Calculation oldCalculation = getById(modifiedCalculation.getId());		
		if(!modifiedCalculation.getPath().equals(oldCalculation.getPath()) &&
				CalculationService.getByPath(modifiedCalculation.getPath()) != null)
			throw new Exception("A calculation with such name already exists on destination project.");		
		
		Connection conn = null;
		PreparedStatement psql = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();	        
	    	psql = conn.prepareStatement(Queries.UPDATE_CALCULATION);
	    	psql.setString(1, modifiedCalculation.getName());
	    	psql.setString(2, modifiedCalculation.getDescription());
	    	psql.setString(3, newPath);
	    	psql.setInt(4, modifiedCalculation.getId());
	    	psql.executeUpdate();
	    	
	    	//Send refresh UI event 
	    	HashMap<String, String> params = new HashMap<>();
	    	params.put("id", String.valueOf(oldCalculation.getId()));
	    	params.put("oldPath", oldCalculation.getPath());	    	
			TreeEvent.sendEventToUserQueue(new Event("calculationModified", null, params));
		}catch(Exception e) {
			throw new Exception("An error raised while updating the calculation.");			
		}finally {
			DatabaseHandler.closeStatementResult(psql, null);
			DatabaseHandler.closeConnection(conn);
		}
	}

	private static boolean isValidPath(String destinationPath) throws BrowseCredentialsException {		
		String userRoot = GeneralConstants.DB_ROOT + ShiroManager.getCurrent().getUserName();
		return destinationPath.startsWith(userRoot) && !destinationPath.equals(userRoot);		//Can't work with calculation on users' root 		
	}

	public static void publishCalculation(long id, String publishedName, String handle) throws Exception {
		Connection conn = null;
		PreparedStatement psql = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.PUBLISH_CALCULATION);
			psql.setString(1, handle);
			psql.setString(2, publishedName);
			psql.setLong(3, id);	
			psql.executeUpdate();		
		}catch(Exception e) {
			throw e;
		}finally {
			DatabaseHandler.closeStatementResult(psql, null);
    		DatabaseHandler.resetAutocommit(conn);
    		DatabaseHandler.closeConnection(conn);
		}	
	}
	
	public static String getOutputFilePath(long calculationId) {
		try {
			Calculation calculation = getById(calculationId);
			if(calculation == null)
				return null;			
			XMLFileManager xmlFileManager = new XMLFileManager(MetsFileHandler.METS_NAMESPACE, MetsFileHandler.METS_ADDITIONAL_NAMESPACES, calculation.getMetsXml());
			String outputFileName = xmlFileManager.getSingleAttributeValueQuery(XpathQueries.GET_CALCULATION_OUTPUT_FILENAME);		
			return FileService.getCalculationPath(calculationId, outputFileName);	
		}catch(Exception e) {
			log.error(e.getMessage());
		}
		return null;
	}	
}
