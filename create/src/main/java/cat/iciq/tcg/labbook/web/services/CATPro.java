/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.services;

import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.HashMap;

import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.datatype.services.PermissionService;
import cat.iciq.tcg.labbook.datatype.services.PermissionService.Permissions;
import cat.iciq.tcg.labbook.datatype.services.ProjectService;
import cat.iciq.tcg.labbook.shell.data.RowDH;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.core.Services;

/**
 * Servlet implementation class CATPro
 */
public class CATPro extends Services {
	
	private static final long serialVersionUID = 1L;
	private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");   
	
    /**
     * @see Services#Services()
     */
    public CATPro() {
        super();
    }

	/**
     * @throws SQLException 
	 * @throws IOException 
	 * @throws BrowseCredentialsException 
	 * @see IService#executeService(Connection, OutputStream)
     */
    @Override
    public void executeService(Connection conn, OutputStream ostream,HashMap<String,String> params, String userName,Integer userId,PreparedStatement psql,ResultSet res) throws SQLException, IOException, BrowseCredentialsException {
    		String path = params.get("path");
			String newPath = params.get("newPath");
						
    		path = Paths.isAbsolute(newPath)? newPath: Paths.getFullPath(path, newPath);
			
    		if(PermissionService.hasPermissionOnProject(path, Permissions.READ)) {
    			Project project = ProjectService.getByPath(path);
    			RowDH data = new RowDH();
    			data.setElement(project.getName());
				data.setElement(project.getDescription());
				data.setElement(project.getPermissions());
				data.setElement(ShiroManager.getCurrent().getUserNameById(project.getOwner()));
				data.setElement(ShiroManager.getCurrent().getGroupNameById(project.getGroup()));
				data.setElement(project.getPath());
				data.setElement(project.getConceptGroup());
				data.setElement(project.getCreationDate() != null ? DATE_FORMAT.format(project.getCreationDate()): "");
				data.setElement(project.getModificationDate() != null ? DATE_FORMAT.format(project.getModificationDate()): "");
				data.setElement(project.getCertificationDate() != null ? DATE_FORMAT.format(project.getCertificationDate()): "");
				data.setElement(project.getState());
				sendObj(data, ostream);
    		}else {
    			returnKO(ostream,"Permissions Error","Your user is not allowed to read the requested path.");	
    		}
    }
    
    

}
