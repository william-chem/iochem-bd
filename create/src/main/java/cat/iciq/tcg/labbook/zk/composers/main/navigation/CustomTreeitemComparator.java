/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.zk.composers.main.navigation;

import java.io.Serializable;
import java.util.Comparator;
import org.zkoss.zul.TreeNode;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.PublicableEntity;

public class CustomTreeitemComparator implements Comparator, Serializable {

	public static enum Order {ID, NAME, DESCRIPTION, DATE, HANDLE, USER_DEFINED};
	private static final long serialVersionUID = -1833887605002582778L;

	private boolean asc = true;
    private Order type = Order.NAME;
 
    public CustomTreeitemComparator(boolean asc, Order type) {
        this.asc = asc;
        this.type = type;
    }
  
    public Order getType() {
        return type;
    }
 
    public void setType(Order type) {
        this.type = type;
    }
    
    public boolean isAscending() {
    	return this.asc;
    }
    
    
    @Override
    public int compare(Object o1, Object o2) {
        Entity dc1 = ((TreeNode<Entity>) o1).getData();
        Entity dc2 = ((TreeNode<Entity>) o2).getData();
        switch (type) {
        case ID: // Compare Id
            return compareId(dc1, dc2) * (asc ? 1 : -1);
        case NAME: // Compare Name
            return compareName(dc1, dc2) * (asc ? 1 : -1);        
        case DESCRIPTION: // Compare Description
            return compareDescription(dc1, dc2) * (asc ? 1 : -1);      
        case DATE: // Compare Date
        	return compareDate(dc1, dc2) * (asc ? 1 : -1);
        case HANDLE: // Compare Handle
        	return compareHandle(dc1, dc2) * (asc ? 1 : -1);
        case USER_DEFINED:
        	return compareUserDefinedOrder(dc1, dc2) * (asc ? 1 : -1);
        default: // Compare Owner
            return compareName(dc1, dc2) * (asc ? 1 : -1);
        } 
    }

	public int compareId(Entity dc1, Entity dc2) {
		return Integer.valueOf(dc1.getId()) - Integer.valueOf(dc2.getId());
	}

	public int compareName(Entity dc1, Entity dc2) {
		return dc1.getName().toLowerCase().compareTo(dc2.getName().toLowerCase());		
	}

	public int compareDescription(Entity dc1, Entity dc2) {
		return dc1.getDescription().toLowerCase().compareTo(dc2.getDescription().toLowerCase());		
	}
	
	
	public int compareDate(Entity  dc1, Entity  dc2) {
		return dc1.getCreationDate().compareTo(dc2.getCreationDate());			
	}

	public int compareHandle(Entity  dc1, Entity  dc2) {
		return ((PublicableEntity)dc1).getHandle().compareTo(((PublicableEntity)dc2).getHandle());		
	}
	
	public int compareUserDefinedOrder(Entity dc1, Entity dc2) {
		 int order1 = dc1.getElementOrder() <= 0 ? 0 : dc1.getElementOrder();
		 int order2 = dc2.getElementOrder() <= 0 ? 0 : dc2.getElementOrder();			 
		return order1 - order2; 
	}
}
