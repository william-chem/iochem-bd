/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.FileSystem;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.spi.FileSystemProvider;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;

import org.apache.commons.io.FileUtils;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zul.Filedownload;

import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;

public class ShellApplicationDownload {
	
	private final static String SHELL_PACKAGE_NAME = "shell.zip";
	private final static String SHELL_PACKAGE_PATH = "/html/" + SHELL_PACKAGE_NAME;	
	
	public void downloadShellApp() throws Exception{		
		InputStream properties = buildPropertiesFile();		
		ByteArrayInputStream bundle = generateBundle(properties);
		downloadShellAppFile(bundle);
	}
	
	private InputStream buildPropertiesFile() throws BrowseCredentialsException, InvalidKeyException, UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, IllegalBlockSizeException, BadPaddingException{		
		Properties props = new Properties();
		props.setProperty("cas.server.hostname", CustomProperties.getProperty("cas.server.hostname"));
		props.setProperty("cas.server.port", CustomProperties.getProperty("cas.server.port"));
		props.setProperty("cas.server.app", CustomProperties.getProperty("cas.server.app"));				
		props.setProperty("create.server.hostname", CustomProperties.getProperty("create.server.hostname"));
		props.setProperty("create.server.port", CustomProperties.getProperty("create.server.port"));
		props.setProperty("create.server.app", CustomProperties.getProperty("create.server.app"));				
		props.setProperty("cas.username", ShiroManager.getCurrent().getUserEmail());
		props.setProperty("jumbo.template.base.url", CustomProperties.getProperty("jumbo.template.base.url"));
		props.setProperty("mets.institution.name", CustomProperties.getProperty("mets.institution.name"));				
		props.setProperty("debug.mode", "false");
		props.setProperty("allow.selfsigned.certs", "true");				
		try {
			StringWriter sw = new StringWriter();
			props.store(sw, null);
			return new ByteArrayInputStream(sw.getBuffer().toString().getBytes());
		} catch (IOException e) {
			return null;
		}				
	}
	
	private ByteArrayInputStream generateBundle(InputStream properties) throws Exception{
		ByteArrayInputStream result = null;
		Map<String, Object> env = new HashMap<String, Object>();
		//Make a duplicate of original shell package 
		String zipFilePath = Sessions.getCurrent().getWebApp().getRealPath(SHELL_PACKAGE_PATH);
		Path zipPath = Paths.get(zipFilePath);
		File tmpFile = File.createTempFile("shell" , ".zip");		
		Path zipTmpPath = Paths.get(tmpFile.getAbsolutePath());		
	    Files.copy(zipPath, zipTmpPath,StandardCopyOption.REPLACE_EXISTING);		
	    //Save properties to temporal file	
		File resourcesFile = File.createTempFile("resources.properties", ".tmp");
		String resourcesFilePath = resourcesFile.getAbsolutePath();
		saveInputstreamToFile(resourcesFile.getAbsolutePath(), properties);		
	    //Append properties file to build a customized shell package
		String filePathInsideZip = "shell/resources/resources.properties";
	    FileSystemProvider provider = getZipFSProvider();
		try (FileSystem fs = provider.newFileSystem(zipTmpPath, env)) {
			Path src, dst;			
		    src = Paths.get(resourcesFilePath);
            dst = fs.getPath(filePathInsideZip);
            Files.copy(src, dst);                               
		}catch(IOException e){
			throw e;
		}

	    result = new ByteArrayInputStream(FileUtils.readFileToByteArray(tmpFile));
        FileUtils.deleteQuietly(resourcesFile);
        FileUtils.deleteQuietly(tmpFile);
		return result; 
	}
	
	private static void saveInputstreamToFile(String filePath, InputStream is) throws IOException{
		byte[] buffer = new byte[is.available()];
		is.read(buffer);
	    File targetFile = new File(filePath);
	    OutputStream outStream = new FileOutputStream(targetFile);
	    outStream.write(buffer);		
	    outStream.close();
	}
	
    private static FileSystemProvider getZipFSProvider() {
        for (FileSystemProvider provider : FileSystemProvider.installedProviders()) {
            if ("jar".equals(provider.getScheme()))
                return provider;
        }
        return null;
    }

	private void downloadShellAppFile(ByteArrayInputStream bundle){	
		Filedownload.save(bundle, "application/zip", SHELL_PACKAGE_NAME);	
	}
}
