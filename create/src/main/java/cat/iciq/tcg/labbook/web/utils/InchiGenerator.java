/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.utils;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import jumbo2.chemistry.Atom;
import jumbo2.chemistry.Bond;
import jumbo2.chemistry.ConnectionHelper;
import jumbo2.chemistry.Molecule;
import jumbo2.util.Constants;
import jumbo2.util.UniversalNamespaceCache;
import net.sf.jniinchi.INCHI_BOND_TYPE;
import net.sf.jniinchi.JniInchiAtom;
import net.sf.jniinchi.JniInchiBond;
import net.sf.jniinchi.JniInchiException;
import net.sf.jniinchi.JniInchiInput;
import net.sf.jniinchi.JniInchiOutput;
import net.sf.jniinchi.JniInchiWrapper;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class InchiGenerator {
	
	private static final int MAX_ATOM_NUMBER = 1024;
    
	public static final String CONVENTION = "iupac:inchi";
	private static final Logger log = LogManager.getLogger(InchiGenerator.class.getName());
	private static DocumentBuilderFactory dbFactory = null;
	private static XPathFactory xpFactory = null;
	private static DocumentBuilder dBuilder = null;	
		
	private UniversalNamespaceCache namespaceContext;	
	private XPath xPath;
	
	static{		
		try {
			dbFactory = DocumentBuilderFactory.newInstance();
			dbFactory.setNamespaceAware(true);
			dbFactory.setXIncludeAware(true);
			dBuilder = dbFactory.newDocumentBuilder();
			xpFactory = new net.sf.saxon.xpath.XPathFactoryImpl();
		} catch (ParserConfigurationException e) {
			dbFactory = null;
			xpFactory = null;
			dBuilder = null;
			log.error(e.getMessage());
		}							
	}
	
	public void addInchi(File outputFile){	
		if(dbFactory == null)
			return;
		try {
			Document doc = dBuilder.parse(outputFile);
			namespaceContext = new UniversalNamespaceCache(doc, true);
			xPath = xpFactory.newXPath();
			xPath.setNamespaceContext(namespaceContext);
			
			if(!hasInchi(doc)){				
				NodeList moleculeNodes = (NodeList)xPath.evaluate(".//*:molecule", doc, XPathConstants.NODESET);
				for(int inx = 0; inx < moleculeNodes.getLength(); inx++){					
					Node moleculeNode = moleculeNodes.item(inx);
					try{
						Node inchi =  buildInchiNode(doc, moleculeNode);
						moleculeNode.appendChild(inchi);
					}catch(Exception e){
						log.error("Exception raised while generating INCHI formula for calculation file " + outputFile.getAbsolutePath() + ". " + e.getMessage());
					}
				}				
				writeOutputFile(outputFile, doc);	
			}
		} catch (Exception e) {
			log.error(e.getMessage());			
		}		
	}

	private Node buildInchiNode(Document doc, Node moleculeNode) throws Exception{
		Molecule molecule = rebuildMolecule(moleculeNode);
		return createInchiNode(doc, molecule);		
	}
	
	private Molecule rebuildMolecule(Node moleculeNode) throws Exception{
		Molecule molecule = new Molecule( "tmp");				
		NodeList atomNodes = (NodeList)xPath.evaluate(".//*:atom", moleculeNode, XPathConstants.NODESET);
		if(atomNodes.getLength() > MAX_ATOM_NUMBER)
		    throw new Exception("Molecule exceeds maximum number of atoms :" + MAX_ATOM_NUMBER);
		for(int inx2 = 0; inx2 < atomNodes.getLength(); inx2++){
			Node atomNode = atomNodes.item(inx2);					
			Atom atom = new Atom(atomNode);
			molecule.addAtom(atom);
		}
		ConnectionHelper.connectTheDots(molecule);
		return molecule;
	}
	
	private Node createInchiNode(Document doc, Molecule molecule) throws JniInchiException{		
		Element formula = (Element)doc.createElementNS(Constants.cmlNamespace, "formula");
		Element auxInfo = (Element)doc.createElementNS(Constants.cmlNamespace, "scalar");
		formula.setAttribute("convention", CONVENTION);		
		
		JniInchiOutput inchi = getInchiOutput(molecule);
		formula.setAttribute("inline", inchi.getInchi());			
		auxInfo.setAttribute("id", "auxInfo");
		auxInfo.setAttribute("dataType", "xsd:integer");
		auxInfo.setTextContent(inchi.getAuxInfo());
		formula.appendChild(auxInfo);
		return formula;					  			
	}

	private static JniInchiOutput getInchiOutput(Molecule molecule) throws JniInchiException{		
		JniInchiInput input = new JniInchiInput();		
		HashMap <String, JniInchiAtom> atomsMap = new HashMap<String, JniInchiAtom>();		
		Iterator<Atom> atomIterator = molecule.getAtomIterator();
		//Add JniInchiAtom's
		while(atomIterator.hasNext()){
			Atom atom = atomIterator.next();
	    	String atomType = atom.getElementType();
	    	//double atomX3 = atom.getX3();			//Configurations that affect INCHI generation, we omit coordinates if we want to compare structures one-to-one, otherwise AuxInfo will never match
	    	//double atomY3 = atom.getY3();
	    	//double atomZ3 = atom.getZ3();	    	
	    	//JniInchiAtom inchiAtom = input.addAtom(new JniInchiAtom(atomX3, atomY3, atomZ3, atomType));	    	
	    	JniInchiAtom inchiAtom = input.addAtom(new JniInchiAtom(0.0, 0.0, 0.0, atomType));
	    	inchiAtom.setImplicitH(0);
	    	atomsMap.put(atom.getId(), inchiAtom);	    				
	    	
		}
		//Add JniInchiBond's
	    Iterator <Bond> bondIterator = molecule.getBondIterator();
	    while(bondIterator.hasNext()){
	    	Bond bond = bondIterator.next();	 
	    	JniInchiBond inchiBond = null;
	    	JniInchiAtom bondFirstAtom = atomsMap.get(bond.getFromAtomId());
	    	JniInchiAtom bondSecondAtom = atomsMap.get(bond.getToAtomId());
	    	switch(bond.getBondType()){
	    			case SINGLE:    inchiBond = new JniInchiBond(bondFirstAtom, bondSecondAtom, INCHI_BOND_TYPE.SINGLE); 
	    							break;
	    			case DOUBLE: 	inchiBond = new JniInchiBond(bondFirstAtom, bondSecondAtom, INCHI_BOND_TYPE.DOUBLE);
	    							break;
	    			case TRIPLE:	inchiBond = new JniInchiBond(bondFirstAtom, bondSecondAtom, INCHI_BOND_TYPE.TRIPLE);
	    							break;	    							
	    		}
	    	input.addBond(inchiBond);
	    }	    
	    JniInchiOutput output = JniInchiWrapper.getInchi(input);	   
		return output;
	}	
	
	private boolean hasInchi(Document doc) throws XPathExpressionException{
		String hasInchi ="count(//*:molecule/*:formula[@convention='iupac:inchi']) > 0";
		return (Boolean)xPath.evaluate(hasInchi, doc, XPathConstants.BOOLEAN);
	}
	
	private void writeOutputFile(File file, Document doc){
		try {
			StreamResult result = new StreamResult(file);
		    // Use a Transformer for output
		    TransformerFactory tFactory = TransformerFactory.newInstance();
		    Transformer transformer;
			transformer = tFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
			transformer.setOutputProperty(OutputKeys.METHOD, "xml");
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");
			transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    DOMSource source = new DOMSource(doc);		  
		    transformer.transform(source, result);	
		} catch (TransformerConfigurationException e) {		
			log.error(e.getMessage());
		} catch (TransformerException e) {	
			log.error(e.getMessage());
		}		
	}
}
