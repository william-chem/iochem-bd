/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype;

import java.sql.Timestamp;

import cat.iciq.tcg.labbook.shell.utils.Paths;

public abstract class Entity {
    private int id;
    private String path;
	private String name;		
	private String description;
	private int elementOrder;
	private Timestamp creationDate;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Timestamp getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Timestamp creationDate) {
		this.creationDate = creationDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getParentPath() {
		return path;
	}

	public void setParentPath(String path) {
		this.path = path;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getElementOrder() {
		return elementOrder;
	}

	public void setElementOrder(int elementOrder) {
		this.elementOrder = elementOrder;
	}

	public boolean isProject() {
		return this instanceof Project;
	}
	
	public boolean isCalculation() {
		return this instanceof Calculation;
	}

	public String getPath() {
		return Paths.getFullPath(this.getParentPath(), this.getName());
	}
}
