/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.web.core;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;

import javax.naming.InitialContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.zkoss.zk.ui.WebApps;

import cat.iciq.tcg.labbook.db.DatabaseHandler;
import cat.iciq.tcg.labbook.shell.data.IDataHolder;
import cat.iciq.tcg.labbook.shell.data.MessageDH;
import cat.iciq.tcg.labbook.shell.exceptions.BrowseCredentialsException;
import cat.iciq.tcg.labbook.shell.utils.Paths;
import cat.iciq.tcg.labbook.shell.utils.ShiroManager;
import cat.iciq.tcg.labbook.web.definitions.Queries;

/**
 * Servlet implementation class Services
 */
public abstract class Services extends HttpServlet{
	
	protected static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger(Services.class.getName());
	
	public static final String PARAM_REQUEST = "PARAM_REQUEST";
	public static final String OK = "OK";
	public static final String SERVER_ERROR = "System error on server";
	public static final int  BUFFER_SIZE = 1024;
	
	private static final Long threshold = (Long)WebApps.getCurrent().getAttribute("deferredContentThreshold"); 
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Services() {
        super();
    }

	public abstract void executeService(Connection conn, OutputStream    ostream,
			HashMap<String, String> params, String userName, Integer userId,
			PreparedStatement psql, ResultSet res) throws SQLException,
			IOException, BrowseCredentialsException;
    
    /**
     * Rolling back if necessary.
     * @param conn
     * @throws SQLException 
     */
    public static void rollBack(Connection conn) throws SQLException
    {
		conn.rollback();
    	
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Connection 		conn 		= null;
		InitialContext 	cxt 		= null;
		OutputStream    ostream 	= response.getOutputStream();
	   	PreparedStatement	psql 	= null;
    	ResultSet 			res	 	= null;
    	Integer				userId  = null;
    	String 				userName= null;
		
		try
		{
			conn =  DatabaseHandler.getCreateConnection();
			// connection established.
			// some variable setting:
			userName 	= ShiroManager.getCurrent().getUserName();
		    userId 		= ShiroManager.getCurrent().getUserId();
			// real execution:
			this.executeService(conn,ostream, buildParameterMap(request),userName,userId,psql,res);
			
		}
		catch (Exception e)
		{
			logger.error(e.getMessage() + getStackTrace(e));
			try {
				rollBack(conn);
			} catch (SQLException e1) {
				logger.error(e1.getMessage() + getStackTrace(e1));
			}
			returnSystemError(SERVER_ERROR,e,ostream);
		}
		finally
		{
			try {
				DatabaseHandler.resetAutocommit(conn);
				if (ostream != null) 	ostream.close();
				if (conn 	!= null) 	conn.close();
				if (cxt  	!= null) 	cxt.close();
			}
			catch (Exception e)
			{
				logger.error(e.getMessage() + getStackTrace(e));
				try {
					rollBack(conn);
				} catch (SQLException e1) {
					logger.error(e1.getMessage() + getStackTrace(e1));
				}
				finally
				{					
					DatabaseHandler.resetAutocommit(conn);					
				}
				returnSystemError(SERVER_ERROR,e,ostream);
			}
		}
	
	}
	
	protected HashMap<String,String> buildParameterMap(HttpServletRequest request){
	    //retrieve request parameters and build a map with them
		HashMap<String, String> params = new HashMap<String, String>();
		Enumeration<String> parameters = request.getParameterNames();
		while(parameters.hasMoreElements()){
			String key = (String) parameters.nextElement();
			String value = request.getParameter(key);
			params.put(key, value);
		}
		return params;
	}
	
	public static String getStackTrace(Exception e) {
	    Writer result = null;
	    PrintWriter printWriter = null;
	    try
	    {
		    result = new StringWriter();
		    printWriter = new PrintWriter(result);
		    e.printStackTrace(printWriter);
		    return "\n" + result.toString();
	    }
	    finally
	    {
	    	if (printWriter != null) printWriter.close();
			try {
				if (result 		!= null) result.close();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
	    }
		
	}

	protected void returnSystemError(String title, String message,OutputStream ostream) throws IOException {
		
		MessageDH error = new MessageDH(title,message,true);
		sendObj(error,ostream);
		logger.error(title + "\n" + message);
	}

	public static void sendObj(IDataHolder obj, OutputStream ostream) throws IOException {

        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        try
        {
	        bos = new ByteArrayOutputStream();
	        oos = new ObjectOutputStream(bos);
	        oos.writeObject(obj);
	        oos.flush();
        }
        finally
        {
            oos.close();
            bos.close();
            byte[] buffer = bos.toByteArray(); 
            ostream.write(buffer,0,buffer.length);
        }
		
	}
	
	protected void sendFile(String file,OutputStream ostream) throws IOException
	{
  	    if(threshold > -1L && new File(file).length() > threshold)
  	        throw new IOException("Maximum file size reached. This file must be retrieved using the web interface.");
  	    
  	    try( FileInputStream instream = new FileInputStream(file);
  	         BufferedInputStream reader = new BufferedInputStream(instream);
  	         BufferedOutputStream writer = new BufferedOutputStream(ostream);){
  	      	        
  	        int read = 0;          
  	        byte[] buffer = new byte[BUFFER_SIZE];
	  	    while (-1 != (read = reader.read(buffer, 0, BUFFER_SIZE))) {
	  	    	writer.write(buffer, 0, read);
	  	    }  	    
  	    } finally {
  	        ostream.close();
  	    }
	}

	public static void returnSystemError(String title, Exception e,OutputStream ostream) throws IOException {
		String message = getStackTrace(e);
		MessageDH error = new MessageDH(title,message,true);
		sendObj(error,ostream);
		logger.error(title + "\n" + message);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		this.doGet(request,response);
		
	}
	
	public static void returnOK(OutputStream ostream) throws IOException
	{
		byte[] OK = "OK".getBytes();
		ostream.write(OK, 0, OK.length);
	}

	
	public static void returnUNKNOWN(OutputStream ostream) throws IOException
	{
		byte[] UNKNOWN = "UNKNOWN".getBytes();
		ostream.write(UNKNOWN, 0, UNKNOWN.length);
	}

	
	public static void returnKO(OutputStream ostream,String title,String message) throws IOException
	{
		MessageDH error = new MessageDH(title,message,true);
		sendObj(error,ostream);
		
	}
	
	/**  
	 * Helper function used to truncate strings to certain length
	 * @param value
	 * @param length
	 * @return
	 */
	public static String truncate(String value, int length)
	{
	  if (value != null && value.length() > length)
	    value = value.substring(0, length);
	  return value;
	}
	
	@SuppressWarnings("resource")
	public boolean isUserAllowedToAccessFile(int calculationId){
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet res = null;		
		try{
			//Check user is owner
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(Queries.GET_CALCULATION_BY_ID);
	        psql.setBigDecimal(1,new BigDecimal(calculationId));	        	       
	        res = psql.executeQuery();
	        res.next();	 	        
	        String path = res.getString("path");   	        
	        if(path.startsWith("/db/" + ShiroManager.getCurrent().getUserName() + "/"))
	        	return true;	        
	        //Check user is in an allowed group
	        psql = conn.prepareStatement(Queries.GET_PROJECT_OWNERS_AND_PERMISSSIONS_BY_PATH);
	        psql.setString(1, Paths.getParent(path));
	        psql.setString(2, Paths.getTail(path));
	        res = psql.executeQuery();
	        res.next();
	        String ownerGroup = String.valueOf(res.getInt("owner_group_id"));
	        String permissions = res.getString("permissions");	        
	        if(permissions.matches("....(10|11)"))	//Other enabled
	        	return true;
	        if(permissions.matches("..(10|11)..") && Arrays.asList(ShiroManager.getCurrent().getUserGroups()).contains(ownerGroup)) //Group enabled
	        	return true;
	    }catch(Exception e){
			return false;			
		}finally{
			DatabaseHandler.closeStatementResult(psql, res);
			DatabaseHandler.closeConnection(conn);
		}
		return false;
	}
	
}
