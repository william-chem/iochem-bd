/**
 * Create module - Create module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package cat.iciq.tcg.labbook.datatype.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.Arrays;
import java.util.List;
import java.util.TreeMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import cat.iciq.tcg.labbook.datatype.Calculation;
import cat.iciq.tcg.labbook.datatype.Entity;
import cat.iciq.tcg.labbook.datatype.Project;
import cat.iciq.tcg.labbook.db.DatabaseHandler;

public class SearchService {
	
	private static final Logger log = LogManager.getLogger(SearchService.class);
			
	public static List<TreeMap<String, Entity>> search(String sql){		
		TreeMap<String, Entity> projects = new TreeMap<String, Entity>();
		TreeMap<String, Entity> calculations = new TreeMap<String, Entity>();
		Connection conn = null;
		PreparedStatement psql = null;
		ResultSet rs = null;		
		try {
			conn = DatabaseHandler.getCreateConnection();
			psql = conn.prepareStatement(sql);
	        rs = psql.executeQuery();
	        while(rs.next()){
	        	Entity entity;	        
	        	int id = rs.getInt(1);	        		 
	        	String projectPath = rs.getString(2);
	        	String projectName = rs.getString(3);
	        	String projectDesc = rs.getString(4);
	        	
	        	int calcId = rs.getInt(5);
	        	String calculationPath = rs.getString(6);
	        	String calculationName = rs.getString(7);
	        	String calculationDesc = rs.getString(8);
	        	
	        	if(calcId == 0) {		//Search only projects or empty projects
	        		entity = new Project();
	        		entity.setId(id);
		        	entity.setParentPath(projectPath);
	        		entity.setName(projectName);
	        		entity.setDescription(projectDesc);
	        		projects.put(entity.getPath(), entity);
	        	}else{	        		
	        		entity = new Calculation();
	        		entity.setId(calcId);
	        		entity.setParentPath(calculationPath);	        		
	        		entity.setName(calculationName);
	        		entity.setDescription(calculationDesc);
	        		calculations.put(entity.getPath(), entity);
	        		if(!projects.containsKey(entity.getParentPath())) {
	        			entity = new Project();
		        		entity.setId(id);
			        	entity.setParentPath(projectPath);
		        		entity.setName(projectName);
		        		entity.setDescription(projectDesc);
		        		projects.put(entity.getPath(), entity);
	        		}
	        	}	        	
	        }
		} catch (Exception e) {
			
		} finally{
			DatabaseHandler.closeStatementResult(psql, rs);
			DatabaseHandler.closeConnection(conn);			
		}
		return Arrays.asList(projects, calculations);	
	}

}
