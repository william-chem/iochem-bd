package org.jasig.cas.client.validation;

import java.net.MalformedURLException;
import java.net.URL;

import javax.servlet.FilterConfig;

import org.jasig.cas.client.Protocol;
import org.jasig.cas.client.configuration.ConfigurationKeys;
import org.jasig.cas.client.ssl.HttpURLConnectionFactory;
import org.jasig.cas.client.ssl.HttpsURLConnectionFactory;
import org.jasig.cas.client.util.CommonUtils;

import cat.iciq.tcg.labbook.web.utils.CustomProperties;
import cat.iciq.tcg.labbook.zk.ui.ShiroWebEnvironment;

public class Saml11CustomTicketValidationFilter extends AbstractTicketValidationFilter {

	
    public Saml11CustomTicketValidationFilter() {
        super(Protocol.SAML11);     
    }
    
    @Override
    public void init() {     
        this.setServerName(getBaseService().toString());
        super.init();
    }
	
    @Override
    protected final TicketValidator getTicketValidator(final FilterConfig filterConfig) {
    	String casServerUrlPrefix = getString(ConfigurationKeys.CAS_SERVER_URL_PREFIX);
    	try {
    		casServerUrlPrefix = ShiroWebEnvironment.getCasServerUrlPrefix().toString();
    	}catch(MalformedURLException ex) {
    		logger.error("Wrong URL setting caseServeUrlPrefix on " + this.getClass().getName());
    		logger.error(ex.getMessage());
    	}        
        final Saml11TicketValidator validator = new Saml11TicketValidator(casServerUrlPrefix);        
        final long tolerance = getLong(ConfigurationKeys.TOLERANCE);
        validator.setTolerance(tolerance);
        validator.setRenew(getBoolean(ConfigurationKeys.RENEW));

        final HttpURLConnectionFactory factory = new HttpsURLConnectionFactory(getHostnameVerifier(), getSSLConfig());
        validator.setURLConnectionFactory(factory);

        validator.setEncoding(getString(ConfigurationKeys.ENCODING));
        return validator;
    }
    

	public static String getBaseService() {
		String createHostname = CustomProperties.getProperty("create.server.hostname");
		int createPort = CustomProperties.getProperty("create.server.port").equals("")? 443 : Integer.parseInt(CustomProperties.getProperty("create.server.port"));		
		try {
			return new URL("https", createHostname, createPort, "").toString();
		} catch (MalformedURLException e) {			
			return "";
		}
	}
}
