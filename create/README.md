# Create module 

The Create module is a private single-page web application aimed at managing input and output files from computational chemistry research work and to generate derived content from it. Create module allows uploading, organising, visualising and curing your data, and share it with your project mates. You can read more about it on the [software documentation page](https://docs.iochem-bd.org/en/latest/guides/usage/create-module-walktrough.html#create-module-walkthrough).

## Frameworks used inside this module

The module uses the **[ZK framework](https://www.zkoss.org/product/zk)** to render and display the web pages. It handles frontend <-> backend communications following the MVC pattern and configures a series of [EventQueue](https://www.zkoss.org/wiki/ZK_Developer's_Reference/Event_Handling/Event_Queues) object to manage intra-component communications.

With this framework, a series of .zk webpages are configured along with its logic controllers inside .java files. User interaction and server response messages are passed by the use of AJAX calls (handled entirely by the ZK framework).

The default CSS style of the ZK framework has been removed and replaced by a custom one and also the **[Bootstrap](https://getbootstrap.com/)** HTML framework and **[jQuery](https://jquery.com/)** javascript library.

## Chemical data conversion

Computational Chemistry results are uploaded into Create module after they are converted from text files into CML ([Chemical Markup Language](http://www.xml-cml.org/)) files, a custom set of XML tags for chemistry. This process is handled entirely by the *jumbo-saxon* project which generates .cml output files that are later formatted to HTML5 using [XSLT stylesheets](https://en.wikipedia.org/wiki/XSLT) and [XPaht](https://en.wikipedia.org/wiki/XPath) queries.

Data can be uploaded to Create module:
  * Via web upload 
  * Via shell client, a custom shell client is provided to communicate the web service, read more at the *createshell* project

### From CML to HTML, XYZ, Jcamp-DX ...

An extensive use of **XSLT** (eXtensible Stylesheet Language Transformations) is done to convert CML output files into plain HTML5 summary pages, The stylesheets are located on the *src/main/webapp/html/xslt* folder. 

Along with this transformations, a series of metadata fields will also be extracted from calculations during publication process. Such process will use the XSLT files from the *src/main/webapp/html/xslt* folder.

All XSLT conversions are performed using Saxon-HE (Home edition) library and most of them are located at the *src/main/java/cat/iciq/tcg/labbook/web/services* package and the *cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction* class.  

## Communication among modules

**Create** module communicates with **Browse** module during the *publication* process. In it, users share their datasets with the general public by marking them as Open Data, you can read more about the publication process on [this page](https://docs.iochem-bd.org/en/latest/guides/usage/publishing-calculations/publish-process.html#publishing-calculations-into-browse).

## Communication with external services

### Requesting a DOI

Published datasets can request DOI identifiers so to their URL can be easily linked and managed. 

### Synchronising to the central server

ioChem-BD team has released the **[Find](https://www.iochem-bd.org)** central service in the [Barcelona Supercomputing Center]() (BSC) to gather all public datasets from the different ioChem-BD instances running around the world.
Connecting your instance to the Find service gives your published data the following advantages:  
   * It is indexed in a larger dataset of curated results, so more people can reach your data
   * It is accessible 24/7 on HPC managed servers
   * In case you instance goes down, remove your server or your data gets corrupted a copy of your data   

 You must contact ioChem-BD team at contact@iochem-bd.org in order to request access to the DOI identifier generation process.   
A running ioChem-BD instance can also synchronise its content with the ioChem-BD Find server to   
