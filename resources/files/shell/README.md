# ioChem-BD shell client

The ioChem-BD shell client is intented to be deployed into a Linux box, where your calculations are generated and reside, or on any \*nix machine where calculations are visible. It allows to manage the content of ioChem-BD software from this utility. 

You can also script commands to automate the upload process using the following [guide](https://docs.iochem-bd.org/en/latest/guides/usage/uploading-content-to-create/using-shell-client/shell-automated-scripts.html).

## How to run the shell client

After extracting the .zip file, you must add execution permissions to *start-rep-shell*

```shell
$   chmod +x start-rep-shell
```

Then just start the shell client with this command:

```shell
$   . start-rep-shell   
$   source start-rep-shell  # Alternative to dot command
```

After connecting to the ioChem-BD server, a series of specific commands will be available in the current shell.
Please review them at the [official documentation](https://docs.iochem-bd.org/en/latest/guides/usage/uploading-content-to-create/shell-commands.html)

## Using it on Mac OS platforms

The shell client bundles a Java JRE 8 suited for \*nix machines contained in the *jre* folder.

If you want to run this tool on a Mac OS machine, you must replace the *jre* folder with the proper version from [this site](https://java.com/en/download/manual.jsp). Check the path to *java* binary inside the shell folder is *jre/bin/java*.




