--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;



SET search_path = public, pg_catalog;

--
-- Name: getnextid(character varying); Type: FUNCTION; Schema: public; Owner: iochembd
--

CREATE FUNCTION getnextid(character varying) RETURNS integer
    LANGUAGE sql
    AS $_$SELECT CAST (nextval($1 || '_seq') AS INTEGER) AS RESULT;$_$;


ALTER FUNCTION public.getnextid(character varying) OWNER TO iochembd;

--
-- Name: getuserfullname(integer); Type: FUNCTION; Schema: public; Owner: iochembd
--


CREATE OR REPLACE FUNCTION getuserfullname(user_id integer)
  RETURNS text AS
$BODY$ SELECT 
	(select text_value from metadatavalue where resource_type_id = 7 and resource_id= $1 and metadata_field_id = 125)
        || ', ' ||
	(select text_value from metadatavalue where resource_type_id = 7 and resource_id= $1 and metadata_field_id = 124); $BODY$
  LANGUAGE sql VOLATILE
  COST 100;
ALTER FUNCTION getuserfullname(integer)
  OWNER TO iochembd;



ALTER FUNCTION public.getuserfullname(user_id integer) OWNER TO iochembd;

--
-- Name: FUNCTION getuserfullname(user_id integer); Type: COMMENT; Schema: public; Owner: iochembd
--

COMMENT ON FUNCTION getuserfullname(user_id integer) IS 'Function dedicated to return user fullname in the form : Lastname, Firstname It accepts user_id as input parameter.';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bitstream; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE bitstream (
    bitstream_id integer NOT NULL,
    bitstream_format_id integer,
    checksum character varying(64),
    checksum_algorithm character varying(32),
    internal_id character varying(256),
    deleted boolean,
    store_number integer,
    sequence_id integer,
    size_bytes bigint
);


ALTER TABLE public.bitstream OWNER TO iochembd;

--
-- Name: bitstream_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE bitstream_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bitstream_seq OWNER TO iochembd;

--
-- Name: bitstreamformatregistry; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE bitstreamformatregistry (
    bitstream_format_id integer NOT NULL,
    mimetype character varying(256),
    short_description character varying(128),
    description text,
    support_level integer,
    internal boolean
);


ALTER TABLE public.bitstreamformatregistry OWNER TO iochembd;

--
-- Name: bitstreamformatregistry_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE bitstreamformatregistry_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bitstreamformatregistry_seq OWNER TO iochembd;

--
-- Name: bundle; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE bundle (
    bundle_id integer NOT NULL,
    primary_bitstream_id integer
);


ALTER TABLE public.bundle OWNER TO iochembd;

--
-- Name: bundle2bitstream; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE bundle2bitstream (
    id integer NOT NULL,
    bundle_id integer,
    bitstream_id integer,
    bitstream_order integer
);


ALTER TABLE public.bundle2bitstream OWNER TO iochembd;

--
-- Name: bundle2bitstream_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE bundle2bitstream_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bundle2bitstream_seq OWNER TO iochembd;

--
-- Name: bundle_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE bundle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.bundle_seq OWNER TO iochembd;

--
-- Name: checksum_history; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE checksum_history (
    check_id bigint NOT NULL,
    bitstream_id integer,
    process_start_date timestamp without time zone,
    process_end_date timestamp without time zone,
    checksum_expected character varying,
    checksum_calculated character varying,
    result character varying
);


ALTER TABLE public.checksum_history OWNER TO iochembd;

--
-- Name: checksum_history_check_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE checksum_history_check_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.checksum_history_check_id_seq OWNER TO iochembd;

--
-- Name: checksum_history_check_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE checksum_history_check_id_seq OWNED BY checksum_history.check_id;


--
-- Name: checksum_results; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE checksum_results (
    result_code character varying NOT NULL,
    result_description character varying
);


ALTER TABLE public.checksum_results OWNER TO iochembd;

--
-- Name: collection; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE collection (
    collection_id integer NOT NULL,
    logo_bitstream_id integer,
    template_item_id integer,
    workflow_step_1 integer,
    workflow_step_2 integer,
    workflow_step_3 integer,
    submitter integer,
    admin integer
);


ALTER TABLE public.collection OWNER TO iochembd;

--
-- Name: collection2item; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE collection2item (
    id integer NOT NULL,
    collection_id integer,
    item_id integer
);


ALTER TABLE public.collection2item OWNER TO iochembd;

--
-- Name: collection2item_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE collection2item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.collection2item_seq OWNER TO iochembd;

--
-- Name: collection_item_count; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE collection_item_count (
    collection_id integer NOT NULL,
    count integer
);


ALTER TABLE public.collection_item_count OWNER TO iochembd;

--
-- Name: collection_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE collection_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.collection_seq OWNER TO iochembd;

--
-- Name: communities2item; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE communities2item (
    id integer NOT NULL,
    community_id integer,
    item_id integer
);


ALTER TABLE public.communities2item OWNER TO iochembd;

--
-- Name: communities2item_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE communities2item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.communities2item_seq OWNER TO iochembd;

--
-- Name: community; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE community (
    community_id integer NOT NULL,
    logo_bitstream_id integer,
    admin integer
);


ALTER TABLE public.community OWNER TO iochembd;

--
-- Name: community2collection; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE community2collection (
    id integer NOT NULL,
    community_id integer,
    collection_id integer
);


ALTER TABLE public.community2collection OWNER TO iochembd;

--
-- Name: community2collection_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE community2collection_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.community2collection_seq OWNER TO iochembd;

--
-- Name: community2community; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE community2community (
    id integer NOT NULL,
    parent_comm_id integer,
    child_comm_id integer
);


ALTER TABLE public.community2community OWNER TO iochembd;

--
-- Name: community2community_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE community2community_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.community2community_seq OWNER TO iochembd;

--
-- Name: community2item; Type: VIEW; Schema: public; Owner: iochembd
--

CREATE VIEW community2item AS
 SELECT community2collection.community_id,
    collection2item.item_id
   FROM community2collection,
    collection2item
  WHERE (collection2item.collection_id = community2collection.collection_id);


ALTER TABLE public.community2item OWNER TO iochembd;

--
-- Name: community_item_count; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE community_item_count (
    community_id integer NOT NULL,
    count integer
);


ALTER TABLE public.community_item_count OWNER TO iochembd;

--
-- Name: community_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE community_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.community_seq OWNER TO iochembd;

--
-- Name: metadatafieldregistry_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE metadatafieldregistry_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metadatafieldregistry_seq OWNER TO iochembd;

--
-- Name: metadatafieldregistry; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE metadatafieldregistry (
    metadata_field_id integer DEFAULT nextval('metadatafieldregistry_seq'::regclass) NOT NULL,
    metadata_schema_id integer NOT NULL,
    element character varying(64),
    qualifier character varying(64),
    scope_note text
);


ALTER TABLE public.metadatafieldregistry OWNER TO iochembd;

--
-- Name: metadatavalue_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE metadatavalue_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metadatavalue_seq OWNER TO iochembd;

--
-- Name: metadatavalue; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE metadatavalue (
    metadata_value_id integer DEFAULT nextval('metadatavalue_seq'::regclass) NOT NULL,
    resource_id integer NOT NULL,
    metadata_field_id integer,
    text_value text,
    text_lang character varying(24),
    place integer,
    authority character varying(100),
    confidence integer DEFAULT (-1),
    resource_type_id integer NOT NULL
);


ALTER TABLE public.metadatavalue OWNER TO iochembd;

--
-- Name: dcvalue; Type: VIEW; Schema: public; Owner: iochembd
--

CREATE VIEW dcvalue AS
 SELECT metadatavalue.metadata_value_id AS dc_value_id,
    metadatavalue.resource_id,
    metadatavalue.metadata_field_id AS dc_type_id,
    metadatavalue.text_value,
    metadatavalue.text_lang,
    metadatavalue.place
   FROM metadatavalue,
    metadatafieldregistry
  WHERE (((metadatavalue.metadata_field_id = metadatafieldregistry.metadata_field_id) AND (metadatafieldregistry.metadata_schema_id = 1)) AND (metadatavalue.resource_type_id = 2));


ALTER TABLE public.dcvalue OWNER TO iochembd;

--
-- Name: dcvalue_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE dcvalue_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dcvalue_seq OWNER TO iochembd;

--
-- Name: doi; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE doi (
    doi_id integer NOT NULL,
    doi character varying(256),
    resource_type_id integer,
    resource_id integer,
    status integer
);


ALTER TABLE public.doi OWNER TO iochembd;

--
-- Name: doi_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE doi_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.doi_seq OWNER TO iochembd;

--
-- Name: eperson; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE eperson (
    eperson_id integer NOT NULL,
    email character varying(64),
    password character varying(128),
    can_log_in boolean,
    require_certificate boolean,
    self_registered boolean,
    last_active timestamp without time zone,
    sub_frequency integer,
    netid character varying(64),
    salt character varying(32),
    digest_algorithm character varying(16),
    username character varying(64),
    main_group_id character varying(12)
);


ALTER TABLE public.eperson OWNER TO iochembd;

--
-- Name: eperson_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE eperson_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.eperson_seq OWNER TO iochembd;

--
-- Name: epersongroup; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE epersongroup (
    eperson_group_id integer NOT NULL
);


ALTER TABLE public.epersongroup OWNER TO iochembd;

--
-- Name: epersongroup2eperson; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE epersongroup2eperson (
    id integer NOT NULL,
    eperson_group_id integer,
    eperson_id integer
);


ALTER TABLE public.epersongroup2eperson OWNER TO iochembd;

--
-- Name: epersongroup2eperson_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE epersongroup2eperson_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.epersongroup2eperson_seq OWNER TO iochembd;

--
-- Name: epersongroup2workspaceitem_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE epersongroup2workspaceitem_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.epersongroup2workspaceitem_seq OWNER TO iochembd;

--
-- Name: epersongroup2workspaceitem; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE epersongroup2workspaceitem (
    id integer DEFAULT nextval('epersongroup2workspaceitem_seq'::regclass) NOT NULL,
    eperson_group_id integer,
    workspace_item_id integer
);


ALTER TABLE public.epersongroup2workspaceitem OWNER TO iochembd;

--
-- Name: epersongroup_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE epersongroup_seq
    START WITH 0
    INCREMENT BY 1
    MINVALUE 0
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.epersongroup_seq OWNER TO iochembd;

--
-- Name: fileextension; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE fileextension (
    file_extension_id integer NOT NULL,
    bitstream_format_id integer,
    extension character varying(16)
);


ALTER TABLE public.fileextension OWNER TO iochembd;

--
-- Name: fileextension_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE fileextension_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.fileextension_seq OWNER TO iochembd;

--
-- Name: group2group; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE group2group (
    id integer NOT NULL,
    parent_id integer,
    child_id integer
);


ALTER TABLE public.group2group OWNER TO iochembd;

--
-- Name: group2group_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE group2group_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group2group_seq OWNER TO iochembd;

--
-- Name: group2groupcache; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE group2groupcache (
    id integer NOT NULL,
    parent_id integer,
    child_id integer
);


ALTER TABLE public.group2groupcache OWNER TO iochembd;

--
-- Name: group2groupcache_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE group2groupcache_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.group2groupcache_seq OWNER TO iochembd;

--
-- Name: handle; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE handle (
    handle_id integer NOT NULL,
    handle character varying(256),
    resource_type_id integer,
    resource_id integer
);


ALTER TABLE public.handle OWNER TO iochembd;

--
-- Name: handle_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE handle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.handle_seq OWNER TO iochembd;

--
-- Name: harvested_collection; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE harvested_collection (
    collection_id integer,
    harvest_type integer,
    oai_source character varying,
    oai_set_id character varying,
    harvest_message character varying,
    metadata_config_id character varying,
    harvest_status integer,
    harvest_start_time timestamp with time zone,
    last_harvested timestamp with time zone,
    id integer NOT NULL
);


ALTER TABLE public.harvested_collection OWNER TO iochembd;

--
-- Name: harvested_collection_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE harvested_collection_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.harvested_collection_seq OWNER TO iochembd;

--
-- Name: harvested_item; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE harvested_item (
    item_id integer,
    last_harvested timestamp with time zone,
    oai_id character varying,
    id integer NOT NULL
);


ALTER TABLE public.harvested_item OWNER TO iochembd;

--
-- Name: harvested_item_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE harvested_item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.harvested_item_seq OWNER TO iochembd;

--
-- Name: history_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE history_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.history_seq OWNER TO iochembd;

--
-- Name: historystate_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE historystate_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.historystate_seq OWNER TO iochembd;

--
-- Name: item; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE item (
    item_id integer NOT NULL,
    submitter_id integer,
    in_archive boolean,
    withdrawn boolean,
    owning_collection integer,
    last_modified timestamp with time zone,
    discoverable boolean
);


ALTER TABLE public.item OWNER TO iochembd;

--
-- Name: item2bundle; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE item2bundle (
    id integer NOT NULL,
    item_id integer,
    bundle_id integer
);


ALTER TABLE public.item2bundle OWNER TO iochembd;

--
-- Name: item2bundle_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE item2bundle_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item2bundle_seq OWNER TO iochembd;

--
-- Name: item_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE item_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.item_seq OWNER TO iochembd;

--
-- Name: metadataschemaregistry_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE metadataschemaregistry_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.metadataschemaregistry_seq OWNER TO iochembd;

--
-- Name: metadataschemaregistry; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE metadataschemaregistry (
    metadata_schema_id integer DEFAULT nextval('metadataschemaregistry_seq'::regclass) NOT NULL,
    namespace character varying(256),
    short_id character varying(32)
);


ALTER TABLE public.metadataschemaregistry OWNER TO iochembd;

--
-- Name: most_recent_checksum; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE most_recent_checksum (
    bitstream_id integer NOT NULL,
    to_be_processed boolean NOT NULL,
    expected_checksum character varying NOT NULL,
    current_checksum character varying NOT NULL,
    last_process_start_date timestamp without time zone NOT NULL,
    last_process_end_date timestamp without time zone NOT NULL,
    checksum_algorithm character varying NOT NULL,
    matched_prev_checksum boolean NOT NULL,
    result character varying
);


ALTER TABLE public.most_recent_checksum OWNER TO iochembd;

--
-- Name: registrationdata; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE registrationdata (
    registrationdata_id integer NOT NULL,
    email character varying(64),
    token character varying(48),
    expires timestamp without time zone
);


ALTER TABLE public.registrationdata OWNER TO iochembd;

--
-- Name: registrationdata_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE registrationdata_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.registrationdata_seq OWNER TO iochembd;

--
-- Name: requestitem; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE requestitem (
    requestitem_id integer NOT NULL,
    token character varying(48),
    item_id integer,
    bitstream_id integer,
    allfiles boolean,
    request_email character varying(64),
    request_name character varying(64),
    request_date timestamp without time zone,
    accept_request boolean,
    decision_date timestamp without time zone,
    expires timestamp without time zone,
    request_message text
);


ALTER TABLE public.requestitem OWNER TO iochembd;

--
-- Name: requestitem_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE requestitem_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.requestitem_seq OWNER TO iochembd;

--
-- Name: resourcepolicy; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE resourcepolicy (
    policy_id integer NOT NULL,
    resource_type_id integer,
    resource_id integer,
    action_id integer,
    eperson_id integer,
    epersongroup_id integer,
    start_date date,
    end_date date,
    rpname character varying(30),
    rptype character varying(30),
    rpdescription character varying(100)
);


ALTER TABLE public.resourcepolicy OWNER TO iochembd;

--
-- Name: resourcepolicy_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE resourcepolicy_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.resourcepolicy_seq OWNER TO iochembd;

--
-- Name: schema_version; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE schema_version (
    version_rank integer NOT NULL,
    installed_rank integer NOT NULL,
    version character varying(50) NOT NULL,
    description character varying(200) NOT NULL,
    type character varying(20) NOT NULL,
    script character varying(1000) NOT NULL,
    checksum integer,
    installed_by character varying(100) NOT NULL,
    installed_on timestamp without time zone DEFAULT now() NOT NULL,
    execution_time integer NOT NULL,
    success boolean NOT NULL
);


ALTER TABLE public.schema_version OWNER TO iochembd;

--
-- Name: subscription; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE subscription (
    subscription_id integer NOT NULL,
    eperson_id integer,
    collection_id integer
);


ALTER TABLE public.subscription OWNER TO iochembd;

--
-- Name: subscription_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE subscription_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.subscription_seq OWNER TO iochembd;

--
-- Name: tasklistitem; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE tasklistitem (
    tasklist_id integer NOT NULL,
    eperson_id integer,
    workflow_id integer
);


ALTER TABLE public.tasklistitem OWNER TO iochembd;

--
-- Name: tasklistitem_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE tasklistitem_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tasklistitem_seq OWNER TO iochembd;

--
-- Name: versionhistory; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE versionhistory (
    versionhistory_id integer NOT NULL
);


ALTER TABLE public.versionhistory OWNER TO iochembd;

--
-- Name: versionhistory_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE versionhistory_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.versionhistory_seq OWNER TO iochembd;

--
-- Name: versionitem; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE versionitem (
    versionitem_id integer NOT NULL,
    item_id integer,
    version_number integer,
    eperson_id integer,
    version_date timestamp without time zone,
    version_summary character varying(255),
    versionhistory_id integer
);


ALTER TABLE public.versionitem OWNER TO iochembd;

--
-- Name: versionitem_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE versionitem_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.versionitem_seq OWNER TO iochembd;

--
-- Name: webapp; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE webapp (
    webapp_id integer NOT NULL,
    appname character varying(32),
    url character varying,
    started timestamp without time zone,
    isui integer
);


ALTER TABLE public.webapp OWNER TO iochembd;

--
-- Name: webapp_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE webapp_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.webapp_seq OWNER TO iochembd;

--
-- Name: workflowitem; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE workflowitem (
    workflow_id integer NOT NULL,
    item_id integer,
    collection_id integer,
    state integer,
    owner integer,
    multiple_titles boolean,
    published_before boolean,
    multiple_files boolean
);


ALTER TABLE public.workflowitem OWNER TO iochembd;

--
-- Name: workflowitem_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE workflowitem_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workflowitem_seq OWNER TO iochembd;

--
-- Name: workspaceitem; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE workspaceitem (
    workspace_item_id integer NOT NULL,
    item_id integer,
    collection_id integer,
    multiple_titles boolean,
    published_before boolean,
    multiple_files boolean,
    stage_reached integer,
    page_reached integer
);


ALTER TABLE public.workspaceitem OWNER TO iochembd;

--
-- Name: workspaceitem_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE workspaceitem_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.workspaceitem_seq OWNER TO iochembd;

--
-- Name: check_id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY checksum_history ALTER COLUMN check_id SET DEFAULT nextval('checksum_history_check_id_seq'::regclass);


--
-- Data for Name: bitstream; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: bitstream_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('bitstream_seq', 1, false);


--
-- Data for Name: bitstreamformatregistry; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO bitstreamformatregistry VALUES (1, 'application/octet-stream', 'Unknown', 'Unknown data format', 0, false);
INSERT INTO bitstreamformatregistry VALUES (2, 'text/plain; charset=utf-8', 'License', 'Item-specific license agreed upon to submission', 1, true);
INSERT INTO bitstreamformatregistry VALUES (3, 'text/html; charset=utf-8', 'CC License', 'Item-specific Creative Commons license agreed upon to submission', 1, true);
INSERT INTO bitstreamformatregistry VALUES (4, 'application/pdf', 'Adobe PDF', 'Adobe Portable Document Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (5, 'text/xml', 'XML', 'Extensible Markup Language', 1, false);
INSERT INTO bitstreamformatregistry VALUES (6, 'text/plain', 'Text', 'Plain Text', 1, false);
INSERT INTO bitstreamformatregistry VALUES (7, 'text/html', 'HTML', 'Hypertext Markup Language', 1, false);
INSERT INTO bitstreamformatregistry VALUES (8, 'text/css', 'CSS', 'Cascading Style Sheets', 1, false);
INSERT INTO bitstreamformatregistry VALUES (9, 'application/msword', 'Microsoft Word', 'Microsoft Word', 1, false);
INSERT INTO bitstreamformatregistry VALUES (10, 'application/vnd.openxmlformats-officedocument.wordprocessingml.document', 'Microsoft Word XML', 'Microsoft Word XML', 1, false);
INSERT INTO bitstreamformatregistry VALUES (11, 'application/vnd.ms-powerpoint', 'Microsoft Powerpoint', 'Microsoft Powerpoint', 1, false);
INSERT INTO bitstreamformatregistry VALUES (12, 'application/vnd.openxmlformats-officedocument.presentationml.presentation', 'Microsoft Powerpoint XML', 'Microsoft Powerpoint XML', 1, false);
INSERT INTO bitstreamformatregistry VALUES (13, 'application/vnd.ms-excel', 'Microsoft Excel', 'Microsoft Excel', 1, false);
INSERT INTO bitstreamformatregistry VALUES (14, 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'Microsoft Excel XML', 'Microsoft Excel XML', 1, false);
INSERT INTO bitstreamformatregistry VALUES (15, 'application/marc', 'MARC', 'Machine-Readable Cataloging records', 1, false);
INSERT INTO bitstreamformatregistry VALUES (16, 'image/jpeg', 'JPEG', 'Joint Photographic Experts Group/JPEG File Interchange Format (JFIF)', 1, false);
INSERT INTO bitstreamformatregistry VALUES (17, 'image/gif', 'GIF', 'Graphics Interchange Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (18, 'image/png', 'image/png', 'Portable Network Graphics', 1, false);
INSERT INTO bitstreamformatregistry VALUES (19, 'image/tiff', 'TIFF', 'Tag Image File Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (20, 'audio/x-aiff', 'AIFF', 'Audio Interchange File Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (21, 'audio/basic', 'audio/basic', 'Basic Audio', 1, false);
INSERT INTO bitstreamformatregistry VALUES (22, 'audio/x-wav', 'WAV', 'Broadcase Wave Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (23, 'video/mpeg', 'MPEG', 'Moving Picture Experts Group', 1, false);
INSERT INTO bitstreamformatregistry VALUES (24, 'text/richtext', 'RTF', 'Rich Text Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (25, 'application/vnd.visio', 'Microsoft Visio', 'Microsoft Visio', 1, false);
INSERT INTO bitstreamformatregistry VALUES (26, 'application/x-filemaker', 'FMP3', 'Filemaker Pro', 1, false);
INSERT INTO bitstreamformatregistry VALUES (27, 'image/x-ms-bmp', 'BMP', 'Microsoft Windows bitmap', 1, false);
INSERT INTO bitstreamformatregistry VALUES (28, 'application/x-photoshop', 'Photoshop', 'Photoshop', 1, false);
INSERT INTO bitstreamformatregistry VALUES (29, 'application/postscript', 'Postscript', 'Postscript Files', 1, false);
INSERT INTO bitstreamformatregistry VALUES (30, 'video/quicktime', 'Video Quicktime', 'Video Quicktime', 1, false);
INSERT INTO bitstreamformatregistry VALUES (31, 'audio/x-mpeg', 'MPEG Audio', 'MPEG Audio', 1, false);
INSERT INTO bitstreamformatregistry VALUES (32, 'application/vnd.ms-project', 'Microsoft Project', 'Microsoft Project', 1, false);
INSERT INTO bitstreamformatregistry VALUES (33, 'application/mathematica', 'Mathematica', 'Mathematica Notebook', 1, false);
INSERT INTO bitstreamformatregistry VALUES (34, 'application/x-latex', 'LateX', 'LaTeX document', 1, false);
INSERT INTO bitstreamformatregistry VALUES (35, 'application/x-tex', 'TeX', 'Tex/LateX document', 1, false);
INSERT INTO bitstreamformatregistry VALUES (36, 'application/x-dvi', 'TeX dvi', 'TeX dvi format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (37, 'application/sgml', 'SGML', 'SGML application (RFC 1874)', 1, false);
INSERT INTO bitstreamformatregistry VALUES (38, 'application/wordperfect5.1', 'WordPerfect', 'WordPerfect 5.1 document', 1, false);
INSERT INTO bitstreamformatregistry VALUES (39, 'audio/x-pn-realaudio', 'RealAudio', 'RealAudio file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (40, 'image/x-photo-cd', 'Photo CD', 'Kodak Photo CD image', 1, false);
INSERT INTO bitstreamformatregistry VALUES (41, 'application/vnd.oasis.opendocument.text', 'OpenDocument Text', 'OpenDocument Text', 1, false);
INSERT INTO bitstreamformatregistry VALUES (42, 'application/vnd.oasis.opendocument.text-template', 'OpenDocument Text Template', 'OpenDocument Text Template', 1, false);
INSERT INTO bitstreamformatregistry VALUES (43, 'application/vnd.oasis.opendocument.text-web', 'OpenDocument HTML Template', 'OpenDocument HTML Template', 1, false);
INSERT INTO bitstreamformatregistry VALUES (44, 'application/vnd.oasis.opendocument.text-master', 'OpenDocument Master Document', 'OpenDocument Master Document', 1, false);
INSERT INTO bitstreamformatregistry VALUES (45, 'application/vnd.oasis.opendocument.graphics', 'OpenDocument Drawing', 'OpenDocument Drawing', 1, false);
INSERT INTO bitstreamformatregistry VALUES (46, 'application/vnd.oasis.opendocument.graphics-template', 'OpenDocument Drawing Template', 'OpenDocument Drawing Template', 1, false);
INSERT INTO bitstreamformatregistry VALUES (47, 'application/vnd.oasis.opendocument.presentation', 'OpenDocument Presentation', 'OpenDocument Presentation', 1, false);
INSERT INTO bitstreamformatregistry VALUES (48, 'application/vnd.oasis.opendocument.presentation-template', 'OpenDocument Presentation Template', 'OpenDocument Presentation Template', 1, false);
INSERT INTO bitstreamformatregistry VALUES (49, 'application/vnd.oasis.opendocument.spreadsheet', 'OpenDocument Spreadsheet', 'OpenDocument Spreadsheet', 1, false);
INSERT INTO bitstreamformatregistry VALUES (50, 'application/vnd.oasis.opendocument.spreadsheet-template', 'OpenDocument Spreadsheet Template', 'OpenDocument Spreadsheet Template', 1, false);
INSERT INTO bitstreamformatregistry VALUES (51, 'application/vnd.oasis.opendocument.chart', 'OpenDocument Chart', 'OpenDocument Chart', 1, false);
INSERT INTO bitstreamformatregistry VALUES (52, 'application/vnd.oasis.opendocument.formula', 'OpenDocument Formula', 'OpenDocument Formula', 1, false);
INSERT INTO bitstreamformatregistry VALUES (53, 'application/vnd.oasis.opendocument.database', 'OpenDocument Database', 'OpenDocument Database', 1, false);
INSERT INTO bitstreamformatregistry VALUES (54, 'application/vnd.oasis.opendocument.image', 'OpenDocument Image', 'OpenDocument Image', 1, false);
INSERT INTO bitstreamformatregistry VALUES (55, 'application/vnd.openofficeorg.extension', 'OpenOffice.org extension', 'OpenOffice.org extension (since OOo 2.1)', 1, false);
INSERT INTO bitstreamformatregistry VALUES (56, 'application/vnd.sun.xml.writer', 'Writer 6.0 documents', 'Writer 6.0 documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (57, 'application/vnd.sun.xml.writer.template', 'Writer 6.0 templates', 'Writer 6.0 templates', 1, false);
INSERT INTO bitstreamformatregistry VALUES (58, 'application/vnd.sun.xml.calc', 'Calc 6.0 spreadsheets', 'Calc 6.0 spreadsheets', 1, false);
INSERT INTO bitstreamformatregistry VALUES (59, 'application/vnd.sun.xml.calc.template', 'Calc 6.0 templates', 'Calc 6.0 templates', 1, false);
INSERT INTO bitstreamformatregistry VALUES (60, 'application/vnd.sun.xml.draw', 'Draw 6.0 documents', 'Draw 6.0 documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (61, 'application/vnd.sun.xml.draw.template', 'Draw 6.0 templates', 'Draw 6.0 templates', 1, false);
INSERT INTO bitstreamformatregistry VALUES (62, 'application/vnd.sun.xml.impress', 'Impress 6.0 presentations', 'Impress 6.0 presentations', 1, false);
INSERT INTO bitstreamformatregistry VALUES (63, 'application/vnd.sun.xml.impress.template', 'Impress 6.0 templates', 'Impress 6.0 templates', 1, false);
INSERT INTO bitstreamformatregistry VALUES (64, 'application/vnd.sun.xml.writer.global', 'Writer 6.0 global documents', 'Writer 6.0 global documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (65, 'application/vnd.sun.xml.math', 'Math 6.0 documents', 'Math 6.0 documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (66, 'application/vnd.stardivision.writer', 'StarWriter 5.x documents', 'StarWriter 5.x documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (67, 'application/vnd.stardivision.writer-global', 'StarWriter 5.x global documents', 'StarWriter 5.x global documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (68, 'application/vnd.stardivision.calc', 'StarCalc 5.x spreadsheets', 'StarCalc 5.x spreadsheets', 1, false);
INSERT INTO bitstreamformatregistry VALUES (69, 'application/vnd.stardivision.draw', 'StarDraw 5.x documents', 'StarDraw 5.x documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (70, 'application/vnd.stardivision.impress', 'StarImpress 5.x presentations', 'StarImpress 5.x presentations', 1, false);
INSERT INTO bitstreamformatregistry VALUES (71, 'application/vnd.stardivision.impress-packed', 'StarImpress Packed 5.x files', 'StarImpress Packed 5.x files', 1, false);
INSERT INTO bitstreamformatregistry VALUES (72, 'application/vnd.stardivision.math', 'StarMath 5.x documents', 'StarMath 5.x documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (73, 'application/vnd.stardivision.chart', 'StarChart 5.x documents', 'StarChart 5.x documents', 1, false);
INSERT INTO bitstreamformatregistry VALUES (74, 'application/vnd.stardivision.mail', 'StarMail 5.x mail files', 'StarMail 5.x mail files', 1, false);
INSERT INTO bitstreamformatregistry VALUES (75, 'application/rdf+xml; charset=utf-8', 'RDF XML', 'RDF serialized in XML', 1, false);
INSERT INTO bitstreamformatregistry VALUES (76, 'chemical/x-alchemy', 'Alchemy format', 'Alchemy format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (77, 'chemical/x-cache-csf', 'cache-csf', 'cache-csf', 1, false);
INSERT INTO bitstreamformatregistry VALUES (78, 'chemical/x-cactvs-binary', 'CACTVS binary format', 'CACTVS binary format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (79, 'chemical/x-cactvs-cascii', 'CACTVS ascii format', 'CACTVS ascii format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (80, 'chemical/x-cactvs-table', 'CACTVS table format', 'CACTVS table format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (81, 'chemical/x-cdx', 'ChemDraw eXchange file', 'ChemDraw eXchange file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (82, 'chemical/x-cerius', 'MSI Cerius II format.', 'MSI Cerius II format.', 1, false);
INSERT INTO bitstreamformatregistry VALUES (83, 'chemical/x-chem3d', 'Chem3D Format', 'Chem3D Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (84, 'chemical/x-chemdraw', 'ChemDraw file', 'ChemDraw file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (85, 'chemical/x-cif', 'Crystallographic Interchange Format', 'Crystallographic Interchange Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (86, 'chemical/x-mmcif', 'mmcif', 'mmcif', 1, false);
INSERT INTO bitstreamformatregistry VALUES (87, 'chemical/x-cmdf', 'CrystalMaker Data format', 'CrystalMaker Data format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (88, 'chemical/x-compass', 'Compass program of the Takahashi', 'Compass program of the Takahashi', 1, false);
INSERT INTO bitstreamformatregistry VALUES (89, 'chemical/x-crossfire', 'Crossfire file', 'Crossfire file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (90, 'chemical/x-csml', 'Chemical Style Markup Language', 'Chemical Style Markup Language', 1, false);
INSERT INTO bitstreamformatregistry VALUES (91, 'chemical/x-ctx', 'Gasteiger group CTX file format', 'Gasteiger group CTX file format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (92, 'chemical/x-cxf', 'cxf', 'cxf', 1, false);
INSERT INTO bitstreamformatregistry VALUES (93, 'chemical/x-daylight-smiles', 'Smiles Format', 'Smiles Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (94, 'chemical/x-embl-dl-nucleotide', 'EMBL nucleotide format', 'EMBL nucleotide format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (95, 'chemical/x-galactic-spc', 'SPC format for spectral and chromatographic data.', 'SPC format for spectral and chromatographic data.', 1, false);
INSERT INTO bitstreamformatregistry VALUES (96, 'chemical/x-gamess-input', 'MACGAMESS', 'MACGAMESS', 1, false);
INSERT INTO bitstreamformatregistry VALUES (97, 'chemical/x-gaussian-checkpoint', 'Gaussian Checkpoint format', 'Gaussian Checkpoint format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (98, 'chemical/x-gaussian-cube', 'Gaussian Cube (Wavefunction) format', 'Gaussian Cube (Wavefunction) format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (99, 'chemical/x-gaussian-input', 'Gaussian Input format', 'Gaussian Input format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (100, 'chemical/x-gcg8-sequence', 'gcg8-sequence', 'gcg8-sequence', 1, false);
INSERT INTO bitstreamformatregistry VALUES (101, 'chemical/x-genbank', 'ToGenBank format', 'ToGenBank format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (102, 'chemical/x-isostar', 'IsoStar Library of intermolecular interactions', 'IsoStar Library of intermolecular interactions', 1, false);
INSERT INTO bitstreamformatregistry VALUES (103, 'chemical/x-jcamp-dx', 'JCAMP Spectroscopic Data Exchange Format', 'JCAMP Spectroscopic Data Exchange Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (104, 'chemical/x-jjc-review-surface', 'Re_View3 Orbital Contour files', 'Re_View3 Orbital Contour files', 1, false);
INSERT INTO bitstreamformatregistry VALUES (105, 'chemical/x-jjc-review-vib', 'Re_View3 Vibration files', 'Re_View3 Vibration files', 1, false);
INSERT INTO bitstreamformatregistry VALUES (106, 'chemical/x-jjc-review-xyz', 'Re_View3 Animation files', 'Re_View3 Animation files', 1, false);
INSERT INTO bitstreamformatregistry VALUES (107, 'chemical/x-kinemage', 'Kinetic (Protein Structure) Images', 'Kinetic (Protein Structure) Images', 1, false);
INSERT INTO bitstreamformatregistry VALUES (108, 'chemical/x-macmolecule', 'MacMolecule File Format', 'MacMolecule File Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (109, 'chemical/x-macromodel-input', 'MacroModel Molecular Mechanics', 'MacroModel Molecular Mechanics', 1, false);
INSERT INTO bitstreamformatregistry VALUES (110, 'chemical/x-mol2', 'Portable representation of a SYBYL molecule', 'Portable representation of a SYBYL molecule', 1, false);
INSERT INTO bitstreamformatregistry VALUES (111, 'chemical/x-mdl-molfile', 'MDL Molfile', 'MDL Molfile', 1, false);
INSERT INTO bitstreamformatregistry VALUES (112, 'chemical/x-mdl-rdfile', 'Reaction-data file', 'Reaction-data file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (113, 'chemical/x-mdl-rxnfile', 'MDL Reaction format', 'MDL Reaction format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (114, 'chemical/x-mdl-sdfile', 'MDL Structure-data file', 'MDL Structure-data file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (115, 'chemical/x-mdl-tgf', 'MDL Transportable Graphics Format', 'MDL Transportable Graphics Format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (116, 'chemical/x-mif', 'mif', 'mif', 1, false);
INSERT INTO bitstreamformatregistry VALUES (117, 'chemical/x-molconn-Z', 'Molconn-Z format', 'Molconn-Z format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (118, 'chemical/x-mopac-graph', 'MOPAC Graph format', 'MOPAC Graph format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (119, 'chemical/x-mopac-input', 'MOPAC Input format', 'MOPAC Input format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (120, 'chemical/x-ncbi-asn1', 'ncbi-asn1', 'ncbi-asn1', 1, false);
INSERT INTO bitstreamformatregistry VALUES (121, 'chemical/x-ncbi-asn1-binary', 'ncbi-asn1-binary', 'ncbi-asn1-binary', 1, false);
INSERT INTO bitstreamformatregistry VALUES (122, 'chemical/x-pdb', 'Protein DataBank', 'Protein DataBank', 1, false);
INSERT INTO bitstreamformatregistry VALUES (123, 'chemical/x-swissprot', 'SWISS-PROT protein sequence database', 'SWISS-PROT protein sequence database', 1, false);
INSERT INTO bitstreamformatregistry VALUES (124, 'chemical/x-vamas-iso14976', 'JISO - a Java Applet for ISO', 'JISO - a Java Applet for ISO', 1, false);
INSERT INTO bitstreamformatregistry VALUES (125, 'chemical/x-vmd', 'Visual Molecular Dynamics', 'Visual Molecular Dynamics', 1, false);
INSERT INTO bitstreamformatregistry VALUES (126, 'chemical/x-xtel', 'Xtelplot file format', 'Xtelplot file format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (127, 'chemical/x-xyz', 'Co-ordinate Animation format', 'Co-ordinate Animation format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (128, 'model/x-vrml', 'Virtual Reality Modeling Language (VRML)', 'Virtual Reality Modeling Language (VRML)', 1, false);
INSERT INTO bitstreamformatregistry VALUES (129, 'chemical/x-cml', 'Chemical Markup Language', 'Chemical Markup Language', 1, false);
INSERT INTO bitstreamformatregistry VALUES (130, 'chemical/x-output', 'Output file', 'Quantum Chemistry output file format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (131, 'chemical/x-adf-input', 'ADF file', 'ADF input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (132, 'chemical/x-molden', 'Molden file', 'Molden Format file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (133, 'chemical/x-turbomole-input', 'Turbomole file', 'Turbomole input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (134, 'chemical/x-vasp-input', 'VASP input', 'VASP input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (135, 'chemical/x-orca-input', 'Orca input file', 'Orca input', 1, false);
INSERT INTO bitstreamformatregistry VALUES (136, 'chemical/x-molcas-input', 'Molcas input', 'Molcas input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (137, 'chemical/x-qespresso-input', 'QuantumEspresso input', 'QuantumEspresso input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (138, 'chemical/x-mopac-input', 'Mopac input', 'Mopac input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (139, 'chemical/x-gronor-input', 'GronOR input', 'GronOR input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (140, 'chemical/x-gromacs-input', 'GROMACS input', 'GROMACS input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (141, 'chemical/x-gromacs-trajectory', 'GROMACS trajectory', 'GROMACS binary trajectory file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (142, 'chemical/x-gromos87', 'Gromos87 geometry', 'Geometry file in Gromos87 format', 1, false);
INSERT INTO bitstreamformatregistry VALUES (143, 'chemical/x-amber-input', 'Amber input', 'Amber input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (144, 'chemical/x-amber-topology', 'Amber topology', 'Amber parameter/trajectory file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (145, 'chemical/x-amber-trajectory', 'Amber trajectory', 'Amber trajectory in NetCDF format', 1, false);


--
-- Name: bitstreamformatregistry_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('bitstreamformatregistry_seq', 145, true);


--
-- Data for Name: bundle; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: bundle2bitstream; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: bundle2bitstream_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('bundle2bitstream_seq', 1, false);


--
-- Name: bundle_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('bundle_seq', 1, false);


--
-- Data for Name: checksum_history; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: checksum_history_check_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('checksum_history_check_id_seq', 1, false);


--
-- Data for Name: checksum_results; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO checksum_results VALUES ('INVALID_HISTORY', 'Install of the cheksum checking code do not consider this history as valid');
INSERT INTO checksum_results VALUES ('BITSTREAM_NOT_FOUND', 'The bitstream could not be found');
INSERT INTO checksum_results VALUES ('CHECKSUM_MATCH', 'Current checksum matched previous checksum');
INSERT INTO checksum_results VALUES ('CHECKSUM_NO_MATCH', 'Current checksum does not match previous checksum');
INSERT INTO checksum_results VALUES ('CHECKSUM_PREV_NOT_FOUND', 'Previous checksum was not found: no comparison possible');
INSERT INTO checksum_results VALUES ('BITSTREAM_INFO_NOT_FOUND', 'Bitstream info not found');
INSERT INTO checksum_results VALUES ('CHECKSUM_ALGORITHM_INVALID', 'Invalid checksum algorithm');
INSERT INTO checksum_results VALUES ('BITSTREAM_NOT_PROCESSED', 'Bitstream marked to_be_processed=false');
INSERT INTO checksum_results VALUES ('BITSTREAM_MARKED_DELETED', 'Bitstream marked deleted in bitstream table');


--
-- Data for Name: collection; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: collection2item; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: collection2item_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('collection2item_seq', 1, false);


--
-- Data for Name: collection_item_count; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: collection_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('collection_seq', 1, false);


--
-- Data for Name: communities2item; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: communities2item_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('communities2item_seq', 1, false);


--
-- Data for Name: community; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: community2collection; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: community2collection_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('community2collection_seq', 1, false);


--
-- Data for Name: community2community; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: community2community_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('community2community_seq', 1, false);


--
-- Data for Name: community_item_count; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: community_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('community_seq', 1, false);


--
-- Name: dcvalue_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('dcvalue_seq', 1, false);


--
-- Data for Name: doi; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: doi_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('doi_seq', 1, false);


--
-- Data for Name: eperson; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO eperson VALUES (1, 'testuser@iochem-bd.org', 'c28c12e6e94aea5fa441442b137bf8e8001ac5dc80a0b34b605a0dc7857a7c0f717ef943dcd8cceb776b2f4a6a863faddd4fb2f07019edc6d9b2f3e600f73f11', true, false, false, '2015-04-08 10:39:03.799', NULL, 'testuser@iochem-bd.org', '47f91038c478e419768ca4a7995765ae', 'SHA-512', 'testuser', '0');


--
-- Name: eperson_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('eperson_seq', 2, true);


--
-- Data for Name: epersongroup; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO epersongroup VALUES (0);
INSERT INTO epersongroup VALUES (1);


--
-- Data for Name: epersongroup2eperson; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO epersongroup2eperson VALUES (2, 1, 1);


--
-- Name: epersongroup2eperson_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('epersongroup2eperson_seq', 3, true);


--
-- Data for Name: epersongroup2workspaceitem; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: epersongroup2workspaceitem_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('epersongroup2workspaceitem_seq', 1, false);


--
-- Name: epersongroup_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('epersongroup_seq', 1, true);


--
-- Data for Name: fileextension; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO fileextension VALUES (1, 4, 'pdf');
INSERT INTO fileextension VALUES (2, 5, 'xml');
INSERT INTO fileextension VALUES (3, 6, 'txt');
INSERT INTO fileextension VALUES (4, 6, 'asc');
INSERT INTO fileextension VALUES (5, 7, 'htm');
INSERT INTO fileextension VALUES (6, 7, 'html');
INSERT INTO fileextension VALUES (7, 8, 'css');
INSERT INTO fileextension VALUES (8, 9, 'doc');
INSERT INTO fileextension VALUES (9, 10, 'docx');
INSERT INTO fileextension VALUES (10, 11, 'ppt');
INSERT INTO fileextension VALUES (11, 12, 'pptx');
INSERT INTO fileextension VALUES (12, 13, 'xls');
INSERT INTO fileextension VALUES (13, 14, 'xlsx');
INSERT INTO fileextension VALUES (14, 16, 'jpeg');
INSERT INTO fileextension VALUES (15, 16, 'jpg');
INSERT INTO fileextension VALUES (16, 17, 'gif');
INSERT INTO fileextension VALUES (17, 18, 'png');
INSERT INTO fileextension VALUES (18, 19, 'tiff');
INSERT INTO fileextension VALUES (19, 19, 'tif');
INSERT INTO fileextension VALUES (20, 20, 'aiff');
INSERT INTO fileextension VALUES (21, 20, 'aif');
INSERT INTO fileextension VALUES (22, 20, 'aifc');
INSERT INTO fileextension VALUES (23, 21, 'au');
INSERT INTO fileextension VALUES (24, 21, 'snd');
INSERT INTO fileextension VALUES (25, 22, 'wav');
INSERT INTO fileextension VALUES (26, 23, 'mpeg');
INSERT INTO fileextension VALUES (27, 23, 'mpg');
INSERT INTO fileextension VALUES (28, 23, 'mpe');
INSERT INTO fileextension VALUES (29, 24, 'rtf');
INSERT INTO fileextension VALUES (30, 25, 'vsd');
INSERT INTO fileextension VALUES (31, 26, 'fm');
INSERT INTO fileextension VALUES (32, 27, 'bmp');
INSERT INTO fileextension VALUES (33, 28, 'psd');
INSERT INTO fileextension VALUES (34, 28, 'pdd');
INSERT INTO fileextension VALUES (35, 29, 'ps');
INSERT INTO fileextension VALUES (36, 29, 'eps');
INSERT INTO fileextension VALUES (37, 29, 'ai');
INSERT INTO fileextension VALUES (38, 30, 'mov');
INSERT INTO fileextension VALUES (39, 30, 'qt');
INSERT INTO fileextension VALUES (40, 31, 'mpa');
INSERT INTO fileextension VALUES (41, 31, 'abs');
INSERT INTO fileextension VALUES (42, 31, 'mpega');
INSERT INTO fileextension VALUES (43, 32, 'mpp');
INSERT INTO fileextension VALUES (44, 32, 'mpx');
INSERT INTO fileextension VALUES (45, 32, 'mpd');
INSERT INTO fileextension VALUES (46, 33, 'ma');
INSERT INTO fileextension VALUES (47, 34, 'latex');
INSERT INTO fileextension VALUES (48, 35, 'tex');
INSERT INTO fileextension VALUES (49, 36, 'dvi');
INSERT INTO fileextension VALUES (50, 37, 'sgm');
INSERT INTO fileextension VALUES (51, 37, 'sgml');
INSERT INTO fileextension VALUES (52, 38, 'wpd');
INSERT INTO fileextension VALUES (53, 39, 'ra');
INSERT INTO fileextension VALUES (54, 39, 'ram');
INSERT INTO fileextension VALUES (55, 40, 'pcd');
INSERT INTO fileextension VALUES (56, 41, 'odt');
INSERT INTO fileextension VALUES (57, 42, 'ott');
INSERT INTO fileextension VALUES (58, 43, 'oth');
INSERT INTO fileextension VALUES (59, 44, 'odm');
INSERT INTO fileextension VALUES (60, 45, 'odg');
INSERT INTO fileextension VALUES (61, 46, 'otg');
INSERT INTO fileextension VALUES (62, 47, 'odp');
INSERT INTO fileextension VALUES (63, 48, 'otp');
INSERT INTO fileextension VALUES (64, 49, 'ods');
INSERT INTO fileextension VALUES (65, 50, 'ots');
INSERT INTO fileextension VALUES (66, 51, 'odc');
INSERT INTO fileextension VALUES (67, 52, 'odf');
INSERT INTO fileextension VALUES (68, 53, 'odb');
INSERT INTO fileextension VALUES (69, 54, 'odi');
INSERT INTO fileextension VALUES (70, 55, 'oxt');
INSERT INTO fileextension VALUES (71, 56, 'sxw');
INSERT INTO fileextension VALUES (72, 57, 'stw');
INSERT INTO fileextension VALUES (73, 58, 'sxc');
INSERT INTO fileextension VALUES (74, 59, 'stc');
INSERT INTO fileextension VALUES (75, 60, 'sxd');
INSERT INTO fileextension VALUES (76, 61, 'std');
INSERT INTO fileextension VALUES (77, 62, 'sxi');
INSERT INTO fileextension VALUES (78, 63, 'sti');
INSERT INTO fileextension VALUES (79, 64, 'sxg');
INSERT INTO fileextension VALUES (80, 65, 'sxm');
INSERT INTO fileextension VALUES (81, 66, 'sdw');
INSERT INTO fileextension VALUES (82, 67, 'sgl');
INSERT INTO fileextension VALUES (83, 68, 'sdc');
INSERT INTO fileextension VALUES (84, 69, 'sda');
INSERT INTO fileextension VALUES (85, 70, 'sdd');
INSERT INTO fileextension VALUES (86, 71, 'sdp');
INSERT INTO fileextension VALUES (87, 72, 'smf');
INSERT INTO fileextension VALUES (88, 73, 'sds');
INSERT INTO fileextension VALUES (89, 74, 'sdm');
INSERT INTO fileextension VALUES (90, 75, 'rdf');
INSERT INTO fileextension VALUES (91, 76, 'alc');
INSERT INTO fileextension VALUES (92, 77, 'csf');
INSERT INTO fileextension VALUES (93, 78, 'cbin');
INSERT INTO fileextension VALUES (94, 79, 'cascii');
INSERT INTO fileextension VALUES (95, 80, 'ctab');
INSERT INTO fileextension VALUES (96, 81, 'cdx');
INSERT INTO fileextension VALUES (97, 82, 'cer');
INSERT INTO fileextension VALUES (98, 83, 'c3d');
INSERT INTO fileextension VALUES (99, 84, 'chm');
INSERT INTO fileextension VALUES (100, 85, 'cif');
INSERT INTO fileextension VALUES (101, 86, 'mcif');
INSERT INTO fileextension VALUES (102, 87, 'cmdf');
INSERT INTO fileextension VALUES (103, 88, 'cpa');
INSERT INTO fileextension VALUES (104, 89, 'bsd');
INSERT INTO fileextension VALUES (105, 90, 'csml');
INSERT INTO fileextension VALUES (106, 90, 'csm');
INSERT INTO fileextension VALUES (107, 91, 'ctx');
INSERT INTO fileextension VALUES (108, 92, 'cxf');
INSERT INTO fileextension VALUES (109, 93, 'smi');
INSERT INTO fileextension VALUES (110, 94, 'emb');
INSERT INTO fileextension VALUES (111, 95, 'spc');
INSERT INTO fileextension VALUES (112, 96, 'inp');
INSERT INTO fileextension VALUES (113, 96, 'gam');
INSERT INTO fileextension VALUES (114, 97, 'fch');
INSERT INTO fileextension VALUES (115, 97, 'fchk');
INSERT INTO fileextension VALUES (116, 98, 'cub');
INSERT INTO fileextension VALUES (117, 99, 'gau');
INSERT INTO fileextension VALUES (118, 100, 'gcg');
INSERT INTO fileextension VALUES (119, 101, 'gen');
INSERT INTO fileextension VALUES (120, 102, 'istr');
INSERT INTO fileextension VALUES (121, 103, 'dx');
INSERT INTO fileextension VALUES (122, 104, 'rv3');
INSERT INTO fileextension VALUES (123, 105, 'rv2');
INSERT INTO fileextension VALUES (124, 105, 'vib');
INSERT INTO fileextension VALUES (125, 106, 'xyb');
INSERT INTO fileextension VALUES (126, 107, 'kin');
INSERT INTO fileextension VALUES (127, 108, 'mcm');
INSERT INTO fileextension VALUES (128, 109, 'mmd');
INSERT INTO fileextension VALUES (129, 109, 'mmod');
INSERT INTO fileextension VALUES (130, 110, 'mol2');
INSERT INTO fileextension VALUES (131, 111, 'mol');
INSERT INTO fileextension VALUES (132, 112, 'rd');
INSERT INTO fileextension VALUES (133, 113, 'rxn');
INSERT INTO fileextension VALUES (134, 114, 'sd');
INSERT INTO fileextension VALUES (135, 115, 'tgf');
INSERT INTO fileextension VALUES (136, 116, 'mif');
INSERT INTO fileextension VALUES (137, 117, 'cpa');
INSERT INTO fileextension VALUES (138, 118, 'gpt');
INSERT INTO fileextension VALUES (139, 119, 'mop');
INSERT INTO fileextension VALUES (140, 120, 'asn');
INSERT INTO fileextension VALUES (141, 121, 'val');
INSERT INTO fileextension VALUES (142, 122, 'pdb');
INSERT INTO fileextension VALUES (143, 123, 'sw');
INSERT INTO fileextension VALUES (144, 124, 'vms');
INSERT INTO fileextension VALUES (145, 125, 'vmd');
INSERT INTO fileextension VALUES (146, 126, 'xtel');
INSERT INTO fileextension VALUES (147, 127, 'xyz');
INSERT INTO fileextension VALUES (148, 128, 'vrml');
INSERT INTO fileextension VALUES (149, 129, 'cml');


--
-- Name: fileextension_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('fileextension_seq', 149, true);


--
-- Data for Name: group2group; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: group2group_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('group2group_seq', 1, false);


--
-- Data for Name: group2groupcache; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: group2groupcache_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('group2groupcache_seq', 1, false);


--
-- Data for Name: handle; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: handle_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('handle_seq', 1, false);


--
-- Data for Name: harvested_collection; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: harvested_collection_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('harvested_collection_seq', 1, false);


--
-- Data for Name: harvested_item; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: harvested_item_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('harvested_item_seq', 1, false);


--
-- Name: history_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('history_seq', 1, false);


--
-- Name: historystate_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('historystate_seq', 1, false);


--
-- Data for Name: item; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: item2bundle; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: item2bundle_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('item2bundle_seq', 1, false);


--
-- Name: item_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('item_seq', 1, false);


--
-- Data for Name: metadatafieldregistry; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO metadatafieldregistry VALUES (1, 1, 'contributor', NULL, 'A person, organization, or service responsible for the content of the resource.  Catch-all for unspecified contributors.');
INSERT INTO metadatafieldregistry VALUES (2, 1, 'contributor', 'advisor', 'Use primarily for thesis advisor.');
INSERT INTO metadatafieldregistry VALUES (3, 1, 'contributor', 'author', NULL);
INSERT INTO metadatafieldregistry VALUES (4, 1, 'contributor', 'editor', NULL);
INSERT INTO metadatafieldregistry VALUES (5, 1, 'contributor', 'illustrator', NULL);
INSERT INTO metadatafieldregistry VALUES (6, 1, 'contributor', 'other', NULL);
INSERT INTO metadatafieldregistry VALUES (7, 1, 'coverage', 'spatial', 'Spatial characteristics of content.');
INSERT INTO metadatafieldregistry VALUES (8, 1, 'coverage', 'temporal', 'Temporal characteristics of content.');
INSERT INTO metadatafieldregistry VALUES (9, 1, 'creator', NULL, 'Do not use; only for harvested metadata.');
INSERT INTO metadatafieldregistry VALUES (10, 1, 'date', NULL, 'Use qualified form if possible.');
INSERT INTO metadatafieldregistry VALUES (11, 1, 'date', 'accessioned', 'Date DSpace takes possession of item.');
INSERT INTO metadatafieldregistry VALUES (12, 1, 'date', 'available', 'Date or date range item became available to the public.');
INSERT INTO metadatafieldregistry VALUES (13, 1, 'date', 'copyright', 'Date of copyright.');
INSERT INTO metadatafieldregistry VALUES (14, 1, 'date', 'created', 'Date of creation or manufacture of intellectual content if different from date.issued.');
INSERT INTO metadatafieldregistry VALUES (15, 1, 'date', 'issued', 'Date of publication or distribution.');
INSERT INTO metadatafieldregistry VALUES (16, 1, 'date', 'submitted', 'Recommend for theses/dissertations.');
INSERT INTO metadatafieldregistry VALUES (17, 1, 'identifier', NULL, 'Catch-all for unambiguous identifiers not defined by qualified form; use identifier.other for a known identifier common to a local collection instead of unqualified form.');
INSERT INTO metadatafieldregistry VALUES (18, 1, 'identifier', 'citation', 'Human-readable, standard bibliographic citation of non-DSpace format of this item');
INSERT INTO metadatafieldregistry VALUES (19, 1, 'identifier', 'govdoc', 'A government document number');
INSERT INTO metadatafieldregistry VALUES (20, 1, 'identifier', 'isbn', 'International Standard Book Number');
INSERT INTO metadatafieldregistry VALUES (21, 1, 'identifier', 'issn', 'International Standard Serial Number');
INSERT INTO metadatafieldregistry VALUES (22, 1, 'identifier', 'sici', 'Serial Item and Contribution Identifier');
INSERT INTO metadatafieldregistry VALUES (23, 1, 'identifier', 'ismn', 'International Standard Music Number');
INSERT INTO metadatafieldregistry VALUES (24, 1, 'identifier', 'other', 'A known identifier type common to a local collection.');
INSERT INTO metadatafieldregistry VALUES (25, 1, 'identifier', 'uri', 'Uniform Resource Identifier');
INSERT INTO metadatafieldregistry VALUES (26, 1, 'description', NULL, 'Catch-all for any description not defined by qualifiers.');
INSERT INTO metadatafieldregistry VALUES (27, 1, 'description', 'abstract', 'Abstract or summary.');
INSERT INTO metadatafieldregistry VALUES (28, 1, 'description', 'provenance', 'The history of custody of the item since its creation, including any changes successive custodians made to it.');
INSERT INTO metadatafieldregistry VALUES (29, 1, 'description', 'sponsorship', 'Information about sponsoring agencies, individuals, or contractual arrangements for the item.');
INSERT INTO metadatafieldregistry VALUES (30, 1, 'description', 'statementofresponsibility', 'To preserve statement of responsibility from MARC records.');
INSERT INTO metadatafieldregistry VALUES (31, 1, 'description', 'tableofcontents', 'A table of contents for a given item.');
INSERT INTO metadatafieldregistry VALUES (32, 1, 'description', 'uri', 'Uniform Resource Identifier pointing to description of this item.');
INSERT INTO metadatafieldregistry VALUES (33, 1, 'format', NULL, 'Catch-all for any format information not defined by qualifiers.');
INSERT INTO metadatafieldregistry VALUES (34, 1, 'format', 'extent', 'Size or duration.');
INSERT INTO metadatafieldregistry VALUES (35, 1, 'format', 'medium', 'Physical medium.');
INSERT INTO metadatafieldregistry VALUES (36, 1, 'format', 'mimetype', 'Registered MIME type identifiers.');
INSERT INTO metadatafieldregistry VALUES (37, 1, 'language', NULL, 'Catch-all for non-ISO forms of the language of the item, accommodating harvested values.');
INSERT INTO metadatafieldregistry VALUES (38, 1, 'language', 'iso', 'Current ISO standard for language of intellectual content, including country codes (e.g. "en_US").');
INSERT INTO metadatafieldregistry VALUES (39, 1, 'publisher', NULL, 'Entity responsible for publication, distribution, or imprint.');
INSERT INTO metadatafieldregistry VALUES (40, 1, 'relation', NULL, 'Catch-all for references to other related items.');
INSERT INTO metadatafieldregistry VALUES (41, 1, 'relation', 'isformatof', 'References additional physical form.');
INSERT INTO metadatafieldregistry VALUES (42, 1, 'relation', 'ispartof', 'References physically or logically containing item.');
INSERT INTO metadatafieldregistry VALUES (43, 1, 'relation', 'ispartofseries', 'Series name and number within that series, if available.');
INSERT INTO metadatafieldregistry VALUES (44, 1, 'relation', 'haspart', 'References physically or logically contained item.');
INSERT INTO metadatafieldregistry VALUES (45, 1, 'relation', 'isversionof', 'References earlier version.');
INSERT INTO metadatafieldregistry VALUES (46, 1, 'relation', 'hasversion', 'References later version.');
INSERT INTO metadatafieldregistry VALUES (47, 1, 'relation', 'isbasedon', 'References source.');
INSERT INTO metadatafieldregistry VALUES (48, 1, 'relation', 'isreferencedby', 'Pointed to by referenced resource.');
INSERT INTO metadatafieldregistry VALUES (49, 1, 'relation', 'requires', 'Referenced resource is required to support function, delivery, or coherence of item.');
INSERT INTO metadatafieldregistry VALUES (50, 1, 'relation', 'replaces', 'References preceeding item.');
INSERT INTO metadatafieldregistry VALUES (51, 1, 'relation', 'isreplacedby', 'References succeeding item.');
INSERT INTO metadatafieldregistry VALUES (52, 1, 'relation', 'uri', 'References Uniform Resource Identifier for related item.');
INSERT INTO metadatafieldregistry VALUES (53, 1, 'rights', NULL, 'Terms governing use and reproduction.');
INSERT INTO metadatafieldregistry VALUES (54, 1, 'rights', 'uri', 'References terms governing use and reproduction.');
INSERT INTO metadatafieldregistry VALUES (55, 1, 'source', NULL, 'Do not use; only for harvested metadata.');
INSERT INTO metadatafieldregistry VALUES (56, 1, 'source', 'uri', 'Do not use; only for harvested metadata.');
INSERT INTO metadatafieldregistry VALUES (57, 1, 'subject', NULL, 'Uncontrolled index term.');
INSERT INTO metadatafieldregistry VALUES (58, 1, 'subject', 'classification', 'Catch-all for value from local classification system; global classification systems will receive specific qualifier');
INSERT INTO metadatafieldregistry VALUES (59, 1, 'subject', 'ddc', 'Dewey Decimal Classification Number');
INSERT INTO metadatafieldregistry VALUES (60, 1, 'subject', 'lcc', 'Library of Congress Classification Number');
INSERT INTO metadatafieldregistry VALUES (61, 1, 'subject', 'lcsh', 'Library of Congress Subject Headings');
INSERT INTO metadatafieldregistry VALUES (62, 1, 'subject', 'mesh', 'MEdical Subject Headings');
INSERT INTO metadatafieldregistry VALUES (63, 1, 'subject', 'other', 'Local controlled vocabulary; global vocabularies will receive specific qualifier.');
INSERT INTO metadatafieldregistry VALUES (64, 1, 'title', NULL, 'Title statement/title proper.');
INSERT INTO metadatafieldregistry VALUES (65, 1, 'title', 'alternative', 'Varying (or substitute) form of title proper appearing in item, e.g. abbreviation or translation');
INSERT INTO metadatafieldregistry VALUES (66, 1, 'type', NULL, 'Nature or genre of content.');
INSERT INTO metadatafieldregistry VALUES (67, 1, 'provenance', NULL, NULL);
INSERT INTO metadatafieldregistry VALUES (68, 1, 'rights', 'license', NULL);
INSERT INTO metadatafieldregistry VALUES (69, 2, 'abstract', NULL, 'A summary of the resource.');
INSERT INTO metadatafieldregistry VALUES (70, 2, 'accessRights', NULL, 'Information about who can access the resource or an indication of its security status. May include information regarding access or restrictions based on privacy, security, or other policies.');
INSERT INTO metadatafieldregistry VALUES (71, 2, 'accrualMethod', NULL, 'The method by which items are added to a collection.');
INSERT INTO metadatafieldregistry VALUES (72, 2, 'accrualPeriodicity', NULL, 'The frequency with which items are added to a collection.');
INSERT INTO metadatafieldregistry VALUES (73, 2, 'accrualPolicy', NULL, 'The policy governing the addition of items to a collection.');
INSERT INTO metadatafieldregistry VALUES (74, 2, 'alternative', NULL, 'An alternative name for the resource.');
INSERT INTO metadatafieldregistry VALUES (75, 2, 'audience', NULL, 'A class of entity for whom the resource is intended or useful.');
INSERT INTO metadatafieldregistry VALUES (76, 2, 'available', NULL, 'Date (often a range) that the resource became or will become available.');
INSERT INTO metadatafieldregistry VALUES (77, 2, 'bibliographicCitation', NULL, 'Recommended practice is to include sufficient bibliographic detail to identify the resource as unambiguously as possible.');
INSERT INTO metadatafieldregistry VALUES (78, 2, 'comformsTo', NULL, 'An established standard to which the described resource conforms.');
INSERT INTO metadatafieldregistry VALUES (79, 2, 'contributor', NULL, 'An entity responsible for making contributions to the resource. Examples of a Contributor include a person, an organization, or a service.');
INSERT INTO metadatafieldregistry VALUES (80, 2, 'coverage', NULL, 'The spatial or temporal topic of the resource, the spatial applicability of the resource, or the jurisdiction under which the resource is relevant.');
INSERT INTO metadatafieldregistry VALUES (81, 2, 'created', NULL, 'Date of creation of the resource.');
INSERT INTO metadatafieldregistry VALUES (82, 2, 'creator', NULL, 'An entity primarily responsible for making the resource.');
INSERT INTO metadatafieldregistry VALUES (83, 2, 'date', NULL, 'A point or period of time associated with an event in the lifecycle of the resource.');
INSERT INTO metadatafieldregistry VALUES (84, 2, 'dateAccepted', NULL, 'Date of acceptance of the resource.');
INSERT INTO metadatafieldregistry VALUES (85, 2, 'dateCopyrighted', NULL, 'Date of copyright.');
INSERT INTO metadatafieldregistry VALUES (86, 2, 'dateSubmitted', NULL, 'Date of submission of the resource.');
INSERT INTO metadatafieldregistry VALUES (87, 2, 'description', NULL, 'An account of the resource.');
INSERT INTO metadatafieldregistry VALUES (88, 2, 'educationLevel', NULL, 'A class of entity, defined in terms of progression through an educational or training context, for which the described resource is intended.');
INSERT INTO metadatafieldregistry VALUES (89, 2, 'extent', NULL, 'The size or duration of the resource.');
INSERT INTO metadatafieldregistry VALUES (90, 2, 'format', NULL, 'The file format, physical medium, or dimensions of the resource.');
INSERT INTO metadatafieldregistry VALUES (91, 2, 'hasFormat', NULL, 'A related resource that is substantially the same as the pre-existing described resource, but in another format.');
INSERT INTO metadatafieldregistry VALUES (92, 2, 'hasPart', NULL, 'A related resource that is included either physically or logically in the described resource.');
INSERT INTO metadatafieldregistry VALUES (93, 2, 'hasVersion', NULL, 'A related resource that is a version, edition, or adaptation of the described resource.');
INSERT INTO metadatafieldregistry VALUES (94, 2, 'identifier', NULL, 'An unambiguous reference to the resource within a given context.');
INSERT INTO metadatafieldregistry VALUES (95, 2, 'instructionalMethod', NULL, 'A process, used to engender knowledge, attitudes and skills, that the described resource is designed to support.');
INSERT INTO metadatafieldregistry VALUES (96, 2, 'isFormatOf', NULL, 'A related resource that is substantially the same as the described resource, but in another format.');
INSERT INTO metadatafieldregistry VALUES (97, 2, 'isPartOf', NULL, 'A related resource in which the described resource is physically or logically included.');
INSERT INTO metadatafieldregistry VALUES (98, 2, 'isReferencedBy', NULL, 'A related resource that references, cites, or otherwise points to the described resource.');
INSERT INTO metadatafieldregistry VALUES (99, 2, 'isReplacedBy', NULL, 'A related resource that supplants, displaces, or supersedes the described resource.');
INSERT INTO metadatafieldregistry VALUES (100, 2, 'isRequiredBy', NULL, 'A related resource that requires the described resource to support its function, delivery, or coherence.');
INSERT INTO metadatafieldregistry VALUES (101, 2, 'issued', NULL, 'Date of formal issuance (e.g., publication) of the resource.');
INSERT INTO metadatafieldregistry VALUES (102, 2, 'isVersionOf', NULL, 'A related resource of which the described resource is a version, edition, or adaptation.');
INSERT INTO metadatafieldregistry VALUES (103, 2, 'language', NULL, 'A language of the resource.');
INSERT INTO metadatafieldregistry VALUES (104, 2, 'license', NULL, 'A legal document giving official permission to do something with the resource.');
INSERT INTO metadatafieldregistry VALUES (105, 2, 'mediator', NULL, 'An entity that mediates access to the resource and for whom the resource is intended or useful.');
INSERT INTO metadatafieldregistry VALUES (106, 2, 'medium', NULL, 'The material or physical carrier of the resource.');
INSERT INTO metadatafieldregistry VALUES (107, 2, 'modified', NULL, 'Date on which the resource was changed.');
INSERT INTO metadatafieldregistry VALUES (108, 2, 'provenance', NULL, 'A statement of any changes in ownership and custody of the resource since its creation that are significant for its authenticity, integrity, and interpretation.');
INSERT INTO metadatafieldregistry VALUES (109, 2, 'publisher', NULL, 'An entity responsible for making the resource available.');
INSERT INTO metadatafieldregistry VALUES (110, 2, 'references', NULL, 'A related resource that is referenced, cited, or otherwise pointed to by the described resource.');
INSERT INTO metadatafieldregistry VALUES (111, 2, 'relation', NULL, 'A related resource.');
INSERT INTO metadatafieldregistry VALUES (112, 2, 'replaces', NULL, 'A related resource that is supplanted, displaced, or superseded by the described resource.');
INSERT INTO metadatafieldregistry VALUES (113, 2, 'requires', NULL, 'A related resource that is required by the described resource to support its function, delivery, or coherence.');
INSERT INTO metadatafieldregistry VALUES (114, 2, 'rights', NULL, 'Information about rights held in and over the resource.');
INSERT INTO metadatafieldregistry VALUES (115, 2, 'rightsHolder', NULL, 'A person or organization owning or managing rights over the resource.');
INSERT INTO metadatafieldregistry VALUES (116, 2, 'source', NULL, 'A related resource from which the described resource is derived.');
INSERT INTO metadatafieldregistry VALUES (117, 2, 'spatial', NULL, 'Spatial characteristics of the resource.');
INSERT INTO metadatafieldregistry VALUES (118, 2, 'subject', NULL, 'The topic of the resource.');
INSERT INTO metadatafieldregistry VALUES (119, 2, 'tableOfContents', NULL, 'A list of subunits of the resource.');
INSERT INTO metadatafieldregistry VALUES (120, 2, 'temporal', NULL, 'Temporal characteristics of the resource.');
INSERT INTO metadatafieldregistry VALUES (121, 2, 'title', NULL, 'A name given to the resource.');
INSERT INTO metadatafieldregistry VALUES (122, 2, 'type', NULL, 'The nature or genre of the resource.');
INSERT INTO metadatafieldregistry VALUES (123, 2, 'valid', NULL, 'Date (often a range) of validity of a resource.');
INSERT INTO metadatafieldregistry VALUES (124, 3, 'firstname', NULL, 'Metadata field used for the first name');
INSERT INTO metadatafieldregistry VALUES (125, 3, 'lastname', NULL, 'Metadata field used for the last name');
INSERT INTO metadatafieldregistry VALUES (126, 3, 'phone', NULL, 'Metadata field used for the phone number');
INSERT INTO metadatafieldregistry VALUES (127, 3, 'language', NULL, 'Metadata field used for the language');
INSERT INTO metadatafieldregistry VALUES (128, 1, 'date', 'updated', 'The last time the item was updated via the SWORD interface');
INSERT INTO metadatafieldregistry VALUES (129, 1, 'description', 'version', 'The Peer Reviewed status of an item');
INSERT INTO metadatafieldregistry VALUES (130, 1, 'identifier', 'slug', 'a uri supplied via the sword slug header, as a suggested uri for the item');
INSERT INTO metadatafieldregistry VALUES (131, 1, 'language', 'rfc3066', 'the rfc3066 form of the language for the item');
INSERT INTO metadatafieldregistry VALUES (132, 1, 'rights', 'holder', 'The owner of the copyright');
INSERT INTO metadatafieldregistry VALUES (133, 4, 'program', 'name', 'Program name used to generate this file');
INSERT INTO metadatafieldregistry VALUES (134, 4, 'program', 'version', 'Program version used to generate this file');
INSERT INTO metadatafieldregistry VALUES (135, 4, 'program', 'other', 'Significant program information');
INSERT INTO metadatafieldregistry VALUES (136, 4, 'method', NULL, 'Method used to generate this file, can exist more than one');
INSERT INTO metadatafieldregistry VALUES (137, 4, 'basisset', NULL, 'BasisSet used in the methods that generate this file, can exist more than one');
INSERT INTO metadatafieldregistry VALUES (138, 4, 'multiplicity', NULL, 'Multiplicity value');
INSERT INTO metadatafieldregistry VALUES (139, 4, 'spintype', NULL, 'Restricted, Unrestricted');
INSERT INTO metadatafieldregistry VALUES (140, 4, 'shelltype', NULL, 'Open shell, closed shell');
INSERT INTO metadatafieldregistry VALUES (141, 4, 'charge', NULL, 'Charge value');
INSERT INTO metadatafieldregistry VALUES (142, 4, 'energy', 'value', 'Energy value');
INSERT INTO metadatafieldregistry VALUES (143, 4, 'energy', 'units', 'Energy units, eV,  A.U.');
INSERT INTO metadatafieldregistry VALUES (144, 4, 'formula', 'generic', 'Generic formula of the molecule used in the calculation');
INSERT INTO metadatafieldregistry VALUES (145, 4, 'name', NULL, 'Compound name, common name (if exist)');
INSERT INTO metadatafieldregistry VALUES (146, 4, 'calculationtype', NULL, 'Can be geometry optimization or stationary point and also TS, minimal or structure.');
INSERT INTO metadatafieldregistry VALUES (147, 4, 'hassolvent', NULL, 'Is this calculation using solvents?');
INSERT INTO metadatafieldregistry VALUES (148, 4, 'hasvibrationalfrequencies', NULL, 'Does this calculations have representable vibrational frequencies?');
INSERT INTO metadatafieldregistry VALUES (149, 4, 'numberofjobs', NULL, 'This fields indicates the number of jobs contained in this output.');
INSERT INTO metadatafieldregistry VALUES (150, 4, 'hasmolecularorbitals', NULL, 'This calculation has representable molecular orbitals?.');
INSERT INTO metadatafieldregistry VALUES (151, 1, 'embargo', 'lift', '');
INSERT INTO metadatafieldregistry VALUES (152, 1, 'embargo', 'terms', '');

--
-- Name: metadatafieldregistry_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('metadatafieldregistry_seq', 152, true);


--
-- Data for Name: metadataschemaregistry; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO metadataschemaregistry VALUES (1, 'http://dublincore.org/documents/dcmi-terms/', 'dc');
INSERT INTO metadataschemaregistry VALUES (2, 'http://purl.org/dc/terms/', 'dcterms');
INSERT INTO metadataschemaregistry VALUES (3, 'http://dspace.org/eperson', 'eperson');
INSERT INTO metadataschemaregistry VALUES (4, 'http://www.xml-cml.org/schema', 'cml');


--
-- Name: metadataschemaregistry_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('metadataschemaregistry_seq', 4, true);


--
-- Data for Name: metadatavalue; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO metadatavalue VALUES (1, 0, 64, 'Anonymous', NULL, 1, NULL, -1, 6);
INSERT INTO metadatavalue VALUES (2, 1, 64, 'Administrator', NULL, 1, NULL, -1, 6);
INSERT INTO metadatavalue VALUES (3, 1, 125, 'Users', NULL, 1, NULL, -1, 7);
INSERT INTO metadatavalue VALUES (4, 1, 124, 'Test', NULL, 1, NULL, -1, 7);
INSERT INTO metadatavalue VALUES (5, 1, 127, 'en', NULL, 1, NULL, -1, 7);


--
-- Name: metadatavalue_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('metadatavalue_seq', 8, true);


--
-- Data for Name: most_recent_checksum; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: registrationdata; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: registrationdata_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('registrationdata_seq', 1, false);


--
-- Data for Name: requestitem; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: requestitem_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('requestitem_seq', 1, false);


--
-- Data for Name: resourcepolicy; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: resourcepolicy_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('resourcepolicy_seq', 1, false);


--
-- Data for Name: schema_version; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO schema_version VALUES (1, 1, '1', '<< Flyway Init >>', 'INIT', '<< Flyway Init >>', NULL, 'iochembd', '2015-04-08 10:22:13.079755', 0, true);
INSERT INTO schema_version VALUES (2, 2, '1.1', 'Initial DSpace 1.1 database schema', 'SQL', 'V1.1__Initial_DSpace_1.1_database_schema.sql', 1058944165, 'iochembd', '2015-04-08 10:22:13.297667', 660, true);
INSERT INTO schema_version VALUES (3, 3, '1.2', 'Upgrade to DSpace 1.2 schema', 'SQL', 'V1.2__Upgrade_to_DSpace_1.2_schema.sql', 1171188562, 'iochembd', '2015-04-08 10:22:13.995392', 47, true);
INSERT INTO schema_version VALUES (4, 4, '1.3', 'Upgrade to DSpace 1.3 schema', 'SQL', 'V1.3__Upgrade_to_DSpace_1.3_schema.sql', 432244245, 'iochembd', '2015-04-08 10:22:14.063799', 111, true);
INSERT INTO schema_version VALUES (5, 5, '1.3.9', 'Drop constraint for DSpace 1 4 schema', 'JDBC', 'org.dspace.storage.rdbms.migration.V1_3_9__Drop_constraint_for_DSpace_1_4_schema', -1, 'iochembd', '2015-04-08 10:22:14.195682', 4, true);
INSERT INTO schema_version VALUES (6, 6, '1.4', 'Upgrade to DSpace 1.4 schema', 'SQL', 'V1.4__Upgrade_to_DSpace_1.4_schema.sql', 1808866373, 'iochembd', '2015-04-08 10:22:14.224384', 215, true);
INSERT INTO schema_version VALUES (7, 7, '1.5', 'Upgrade to DSpace 1.5 schema', 'SQL', 'V1.5__Upgrade_to_DSpace_1.5_schema.sql', -1358482440, 'iochembd', '2015-04-08 10:22:14.461935', 338, true);
INSERT INTO schema_version VALUES (8, 8, '1.5.9', 'Drop constraint for DSpace 1 6 schema', 'JDBC', 'org.dspace.storage.rdbms.migration.V1_5_9__Drop_constraint_for_DSpace_1_6_schema', -1, 'iochembd', '2015-04-08 10:22:14.820757', 14, true);
INSERT INTO schema_version VALUES (9, 9, '1.6', 'Upgrade to DSpace 1.6 schema', 'SQL', 'V1.6__Upgrade_to_DSpace_1.6_schema.sql', -851753252, 'iochembd', '2015-04-08 10:22:14.860283', 98, true);
INSERT INTO schema_version VALUES (10, 10, '1.7', 'Upgrade to DSpace 1.7 schema', 'SQL', 'V1.7__Upgrade_to_DSpace_1.7_schema.sql', -1075634434, 'iochembd', '2015-04-08 10:22:14.986186', 4, true);
INSERT INTO schema_version VALUES (11, 11, '1.8', 'Upgrade to DSpace 1.8 schema', 'SQL', 'V1.8__Upgrade_to_DSpace_1.8_schema.sql', -93232544, 'iochembd', '2015-04-08 10:22:15.008867', 11, true);
INSERT INTO schema_version VALUES (12, 12, '3.0', 'Upgrade to DSpace 3.x schema', 'SQL', 'V3.0__Upgrade_to_DSpace_3.x_schema.sql', 1671414988, 'iochembd', '2015-04-08 10:22:15.044561', 52, true);
INSERT INTO schema_version VALUES (13, 13, '4.0', 'Upgrade to DSpace 4.x schema', 'SQL', 'V4.0__Upgrade_to_DSpace_4.x_schema.sql', 2051773448, 'iochembd', '2015-04-08 10:22:15.118719', 87, true);
INSERT INTO schema_version VALUES (14, 14, '5.0.2014.08.08', 'DS-1945 Helpdesk Request a Copy', 'SQL', 'V5.0_2014.08.08__DS-1945_Helpdesk_Request_a_Copy.sql', -872444192, 'iochembd', '2015-04-08 10:22:15.227205', 11, true);
INSERT INTO schema_version VALUES (15, 15, '5.0.2014.09.25', 'DS 1582 Metadata For All Objects drop constraint', 'JDBC', 'org.dspace.storage.rdbms.migration.V5_0_2014_09_25__DS_1582_Metadata_For_All_Objects_drop_constraint', -1, 'iochembd', '2015-04-08 10:22:15.255944', 3, true);
INSERT INTO schema_version VALUES (16, 16, '5.0.2014.09.26', 'DS-1582 Metadata For All Objects', 'SQL', 'V5.0_2014.09.26__DS-1582_Metadata_For_All_Objects.sql', -1043319466, 'iochembd', '2015-04-08 10:22:15.287937', 72, true);
INSERT INTO schema_version VALUES (17, 17, '5.0.2017.07.21', 'IO 0001', 'JDBC', 'org.dspace.storage.rdbms.migration.V5_0_2017_07_21__IO_0001', -1, 'iochembd', '2022-02-03 12:51:42.394206', 1, true);
INSERT INTO schema_version VALUES (18, 18, '5.0.2021.10.19', 'IO 0002', 'JDBC', 'org.dspace.storage.rdbms.update.V5_0_2021_10_19__IO_0002', -1, 'iochembd', '2022-02-03 12:51:42.907538', 126, true);
INSERT INTO schema_version VALUES (19, 19, '5.0.2021.10.20', 'IO 0003', 'JDBC', 'org.dspace.storage.rdbms.update.V5_0_2021_10_20__IO_0003', -1, 'iochembd', '2022-02-03 12:51:43.06247', 650, true);
INSERT INTO schema_version VALUES (20, 20, '5.0.2021.11.16', 'IO 0004', 'JDBC', 'org.dspace.storage.rdbms.update.V5_0_2021_11_16__IO_0004', -1, 'iochembd', '2022-02-03 12:51:43.746004', 283, true);
INSERT INTO schema_version VALUES (21, 21, '5.0.2022.02.16', 'IO 0005', 'JDBC', 'org.dspace.storage.rdbms.update.V5_0_2022_02_16__IO_0005', -1, 'iochembd', '2022-02-16 16:27:21.46138', 57665, true);



--
-- Data for Name: subscription; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: subscription_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('subscription_seq', 1, false);


--
-- Data for Name: tasklistitem; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: tasklistitem_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('tasklistitem_seq', 1, false);


--
-- Data for Name: versionhistory; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: versionhistory_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('versionhistory_seq', 1, false);


--
-- Data for Name: versionitem; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: versionitem_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('versionitem_seq', 1, false);


--
-- Name: webapp_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('webapp_seq', 1, true);


--
-- Data for Name: workflowitem; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: workflowitem_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('workflowitem_seq', 1, false);


--
-- Data for Name: workspaceitem; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: workspaceitem_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('workspaceitem_seq', 1, false);


--
-- Name: bitstream_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY bitstream
    ADD CONSTRAINT bitstream_pkey PRIMARY KEY (bitstream_id);


--
-- Name: bitstreamformatregistry_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY bitstreamformatregistry
    ADD CONSTRAINT bitstreamformatregistry_pkey PRIMARY KEY (bitstream_format_id);


--
-- Name: bitstreamformatregistry_short_description_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY bitstreamformatregistry
    ADD CONSTRAINT bitstreamformatregistry_short_description_key UNIQUE (short_description);


--
-- Name: bundle2bitstream_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY bundle2bitstream
    ADD CONSTRAINT bundle2bitstream_pkey PRIMARY KEY (id);


--
-- Name: bundle_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY bundle
    ADD CONSTRAINT bundle_pkey PRIMARY KEY (bundle_id);


--
-- Name: checksum_history_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY checksum_history
    ADD CONSTRAINT checksum_history_pkey PRIMARY KEY (check_id);


--
-- Name: checksum_results_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY checksum_results
    ADD CONSTRAINT checksum_results_pkey PRIMARY KEY (result_code);


--
-- Name: collection2item_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY collection2item
    ADD CONSTRAINT collection2item_pkey PRIMARY KEY (id);


--
-- Name: collection_item_count_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY collection_item_count
    ADD CONSTRAINT collection_item_count_pkey PRIMARY KEY (collection_id);


--
-- Name: collection_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_pkey PRIMARY KEY (collection_id);


--
-- Name: communities2item_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY communities2item
    ADD CONSTRAINT communities2item_pkey PRIMARY KEY (id);


--
-- Name: community2collection_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY community2collection
    ADD CONSTRAINT community2collection_pkey PRIMARY KEY (id);


--
-- Name: community2community_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY community2community
    ADD CONSTRAINT community2community_pkey PRIMARY KEY (id);


--
-- Name: community_item_count_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY community_item_count
    ADD CONSTRAINT community_item_count_pkey PRIMARY KEY (community_id);


--
-- Name: community_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY community
    ADD CONSTRAINT community_pkey PRIMARY KEY (community_id);


--
-- Name: doi_doi_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY doi
    ADD CONSTRAINT doi_doi_key UNIQUE (doi);


--
-- Name: doi_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY doi
    ADD CONSTRAINT doi_pkey PRIMARY KEY (doi_id);


--
-- Name: eperson_email_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY eperson
    ADD CONSTRAINT eperson_email_key UNIQUE (email);


--
-- Name: eperson_netid_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY eperson
    ADD CONSTRAINT eperson_netid_key UNIQUE (netid);


--
-- Name: eperson_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY eperson
    ADD CONSTRAINT eperson_pkey PRIMARY KEY (eperson_id);


--
-- Name: epersongroup2eperson_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY epersongroup2eperson
    ADD CONSTRAINT epersongroup2eperson_pkey PRIMARY KEY (id);


--
-- Name: epersongroup2item_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY epersongroup2workspaceitem
    ADD CONSTRAINT epersongroup2item_pkey PRIMARY KEY (id);


--
-- Name: epersongroup_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY epersongroup
    ADD CONSTRAINT epersongroup_pkey PRIMARY KEY (eperson_group_id);


--
-- Name: fileextension_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY fileextension
    ADD CONSTRAINT fileextension_pkey PRIMARY KEY (file_extension_id);


--
-- Name: group2group_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY group2group
    ADD CONSTRAINT group2group_pkey PRIMARY KEY (id);


--
-- Name: group2groupcache_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY group2groupcache
    ADD CONSTRAINT group2groupcache_pkey PRIMARY KEY (id);


--
-- Name: handle_handle_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY handle
    ADD CONSTRAINT handle_handle_key UNIQUE (handle);


--
-- Name: handle_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY handle
    ADD CONSTRAINT handle_pkey PRIMARY KEY (handle_id);


--
-- Name: harvested_collection_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY harvested_collection
    ADD CONSTRAINT harvested_collection_pkey PRIMARY KEY (id);


--
-- Name: harvested_item_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY harvested_item
    ADD CONSTRAINT harvested_item_pkey PRIMARY KEY (id);


--
-- Name: item2bundle_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY item2bundle
    ADD CONSTRAINT item2bundle_pkey PRIMARY KEY (id);


--
-- Name: item_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY item
    ADD CONSTRAINT item_pkey PRIMARY KEY (item_id);


--
-- Name: metadatafieldregistry_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY metadatafieldregistry
    ADD CONSTRAINT metadatafieldregistry_pkey PRIMARY KEY (metadata_field_id);


--
-- Name: metadataschemaregistry_namespace_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY metadataschemaregistry
    ADD CONSTRAINT metadataschemaregistry_namespace_key UNIQUE (namespace);


--
-- Name: metadataschemaregistry_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY metadataschemaregistry
    ADD CONSTRAINT metadataschemaregistry_pkey PRIMARY KEY (metadata_schema_id);


--
-- Name: metadatavalue_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY metadatavalue
    ADD CONSTRAINT metadatavalue_pkey PRIMARY KEY (metadata_value_id);


--
-- Name: most_recent_checksum_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY most_recent_checksum
    ADD CONSTRAINT most_recent_checksum_pkey PRIMARY KEY (bitstream_id);


--
-- Name: registrationdata_email_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY registrationdata
    ADD CONSTRAINT registrationdata_email_key UNIQUE (email);


--
-- Name: registrationdata_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY registrationdata
    ADD CONSTRAINT registrationdata_pkey PRIMARY KEY (registrationdata_id);


--
-- Name: requestitem_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY requestitem
    ADD CONSTRAINT requestitem_pkey PRIMARY KEY (requestitem_id);


--
-- Name: requestitem_token_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY requestitem
    ADD CONSTRAINT requestitem_token_key UNIQUE (token);


--
-- Name: resourcepolicy_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY resourcepolicy
    ADD CONSTRAINT resourcepolicy_pkey PRIMARY KEY (policy_id);


--
-- Name: schema_version_pk; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY schema_version
    ADD CONSTRAINT schema_version_pk PRIMARY KEY (version);


--
-- Name: subscription_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_pkey PRIMARY KEY (subscription_id);


--
-- Name: tasklistitem_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY tasklistitem
    ADD CONSTRAINT tasklistitem_pkey PRIMARY KEY (tasklist_id);


--
-- Name: versionhistory_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY versionhistory
    ADD CONSTRAINT versionhistory_pkey PRIMARY KEY (versionhistory_id);


--
-- Name: versionitem_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY versionitem
    ADD CONSTRAINT versionitem_pkey PRIMARY KEY (versionitem_id);


--
-- Name: webapp_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY webapp
    ADD CONSTRAINT webapp_pkey PRIMARY KEY (webapp_id);


--
-- Name: workflowitem_item_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY workflowitem
    ADD CONSTRAINT workflowitem_item_id_key UNIQUE (item_id);


--
-- Name: workflowitem_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY workflowitem
    ADD CONSTRAINT workflowitem_pkey PRIMARY KEY (workflow_id);


--
-- Name: workspaceitem_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY workspaceitem
    ADD CONSTRAINT workspaceitem_pkey PRIMARY KEY (workspace_item_id);


--
-- Name: bit_bitstream_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX bit_bitstream_fk_idx ON bitstream USING btree (bitstream_format_id);


--
-- Name: bundle2bitstream_bitstream_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX bundle2bitstream_bitstream_fk_idx ON bundle2bitstream USING btree (bitstream_id);


--
-- Name: bundle2bitstream_bundle_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX bundle2bitstream_bundle_idx ON bundle2bitstream USING btree (bundle_id);


--
-- Name: bundle_primary_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX bundle_primary_fk_idx ON bundle USING btree (primary_bitstream_id);


--
-- Name: ch_result_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX ch_result_fk_idx ON checksum_history USING btree (result);


--
-- Name: collection2item_collection_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection2item_collection_idx ON collection2item USING btree (collection_id);


--
-- Name: collection2item_item_id_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection2item_item_id_idx ON collection2item USING btree (item_id);


--
-- Name: collection_admin_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_admin_fk_idx ON collection USING btree (admin);


--
-- Name: collection_logo_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_logo_fk_idx ON collection USING btree (logo_bitstream_id);


--
-- Name: collection_submitter_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_submitter_fk_idx ON collection USING btree (submitter);


--
-- Name: collection_template_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_template_fk_idx ON collection USING btree (template_item_id);


--
-- Name: collection_workflow1_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_workflow1_fk_idx ON collection USING btree (workflow_step_1);


--
-- Name: collection_workflow2_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_workflow2_fk_idx ON collection USING btree (workflow_step_2);


--
-- Name: collection_workflow3_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX collection_workflow3_fk_idx ON collection USING btree (workflow_step_3);


--
-- Name: com2com_child_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX com2com_child_fk_idx ON community2community USING btree (child_comm_id);


--
-- Name: com2com_parent_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX com2com_parent_fk_idx ON community2community USING btree (parent_comm_id);


--
-- Name: comm2item_community_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX comm2item_community_fk_idx ON communities2item USING btree (community_id);


--
-- Name: communities2item_item_id_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX communities2item_item_id_idx ON communities2item USING btree (item_id);


--
-- Name: community2collection_collection_id_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX community2collection_collection_id_idx ON community2collection USING btree (collection_id);


--
-- Name: community2collection_community_id_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX community2collection_community_id_idx ON community2collection USING btree (community_id);


--
-- Name: community_admin_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX community_admin_fk_idx ON community USING btree (admin);


--
-- Name: community_logo_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX community_logo_fk_idx ON community USING btree (logo_bitstream_id);


--
-- Name: doi_doi_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX doi_doi_idx ON doi USING btree (doi);


--
-- Name: doi_resource_id_and_type_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX doi_resource_id_and_type_idx ON doi USING btree (resource_id, resource_type_id);


--
-- Name: eperson_email_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX eperson_email_idx ON eperson USING btree (email);


--
-- Name: epersongroup2eperson_group_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX epersongroup2eperson_group_idx ON epersongroup2eperson USING btree (eperson_group_id);


--
-- Name: epg2ep_eperson_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX epg2ep_eperson_fk_idx ON epersongroup2eperson USING btree (eperson_id);


--
-- Name: epg2wi_group_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX epg2wi_group_fk_idx ON epersongroup2workspaceitem USING btree (eperson_group_id);


--
-- Name: epg2wi_workspace_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX epg2wi_workspace_fk_idx ON epersongroup2workspaceitem USING btree (workspace_item_id);


--
-- Name: fe_bitstream_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX fe_bitstream_fk_idx ON fileextension USING btree (bitstream_format_id);


--
-- Name: g2g_child_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX g2g_child_fk_idx ON group2group USING btree (child_id);


--
-- Name: g2g_parent_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX g2g_parent_fk_idx ON group2group USING btree (parent_id);


--
-- Name: g2gc_child_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX g2gc_child_fk_idx ON group2group USING btree (child_id);


--
-- Name: g2gc_parent_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX g2gc_parent_fk_idx ON group2group USING btree (parent_id);


--
-- Name: handle_handle_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX handle_handle_idx ON handle USING btree (handle);


--
-- Name: handle_resource_id_and_type_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX handle_resource_id_and_type_idx ON handle USING btree (resource_id, resource_type_id);


--
-- Name: harvested_collection_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX harvested_collection_fk_idx ON harvested_collection USING btree (collection_id);


--
-- Name: harvested_item_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX harvested_item_fk_idx ON harvested_item USING btree (item_id);


--
-- Name: item2bundle_bundle_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX item2bundle_bundle_fk_idx ON item2bundle USING btree (bundle_id);


--
-- Name: item2bundle_item_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX item2bundle_item_idx ON item2bundle USING btree (item_id);


--
-- Name: item_submitter_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX item_submitter_fk_idx ON item USING btree (submitter_id);


--
-- Name: metadatafield_schema_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX metadatafield_schema_idx ON metadatafieldregistry USING btree (metadata_schema_id);


--
-- Name: metadatavalue_field_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX metadatavalue_field_fk_idx ON metadatavalue USING btree (metadata_field_id);


--
-- Name: metadatavalue_item_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX metadatavalue_item_idx ON metadatavalue USING btree (resource_id);


--
-- Name: metadatavalue_item_idx2; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX metadatavalue_item_idx2 ON metadatavalue USING btree (resource_id, metadata_field_id);


--
-- Name: mrc_result_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX mrc_result_fk_idx ON most_recent_checksum USING btree (result);


--
-- Name: resourcepolicy_type_id_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX resourcepolicy_type_id_idx ON resourcepolicy USING btree (resource_type_id, resource_id);


--
-- Name: rp_eperson_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX rp_eperson_fk_idx ON resourcepolicy USING btree (eperson_id);


--
-- Name: rp_epersongroup_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX rp_epersongroup_fk_idx ON resourcepolicy USING btree (epersongroup_id);


--
-- Name: schema_version_ir_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX schema_version_ir_idx ON schema_version USING btree (installed_rank);


--
-- Name: schema_version_s_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX schema_version_s_idx ON schema_version USING btree (success);


--
-- Name: schema_version_vr_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX schema_version_vr_idx ON schema_version USING btree (version_rank);


--
-- Name: subs_collection_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX subs_collection_fk_idx ON subscription USING btree (collection_id);


--
-- Name: subs_eperson_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX subs_eperson_fk_idx ON subscription USING btree (eperson_id);


--
-- Name: tasklist_eperson_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX tasklist_eperson_fk_idx ON tasklistitem USING btree (eperson_id);


--
-- Name: tasklist_workflow_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX tasklist_workflow_fk_idx ON tasklistitem USING btree (workflow_id);


--
-- Name: workflow_coll_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX workflow_coll_fk_idx ON workflowitem USING btree (collection_id);


--
-- Name: workflow_item_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX workflow_item_fk_idx ON workflowitem USING btree (item_id);


--
-- Name: workflow_owner_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX workflow_owner_fk_idx ON workflowitem USING btree (owner);


--
-- Name: workspace_coll_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX workspace_coll_fk_idx ON workspaceitem USING btree (collection_id);


--
-- Name: workspace_item_fk_idx; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX workspace_item_fk_idx ON workspaceitem USING btree (item_id);


--
-- Name: bitstream_bitstream_format_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY bitstream
    ADD CONSTRAINT bitstream_bitstream_format_id_fkey FOREIGN KEY (bitstream_format_id) REFERENCES bitstreamformatregistry(bitstream_format_id);


--
-- Name: bundle2bitstream_bitstream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY bundle2bitstream
    ADD CONSTRAINT bundle2bitstream_bitstream_id_fkey FOREIGN KEY (bitstream_id) REFERENCES bitstream(bitstream_id);


--
-- Name: bundle2bitstream_bundle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY bundle2bitstream
    ADD CONSTRAINT bundle2bitstream_bundle_id_fkey FOREIGN KEY (bundle_id) REFERENCES bundle(bundle_id);


--
-- Name: checksum_history_result_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY checksum_history
    ADD CONSTRAINT checksum_history_result_fkey FOREIGN KEY (result) REFERENCES checksum_results(result_code);


--
-- Name: coll2item_item_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection2item
    ADD CONSTRAINT coll2item_item_fk FOREIGN KEY (item_id) REFERENCES item(item_id) DEFERRABLE;


--
-- Name: collection2item_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection2item
    ADD CONSTRAINT collection2item_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(collection_id);


--
-- Name: collection_admin_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_admin_fkey FOREIGN KEY (admin) REFERENCES epersongroup(eperson_group_id);


--
-- Name: collection_item_count_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection_item_count
    ADD CONSTRAINT collection_item_count_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(collection_id);


--
-- Name: collection_logo_bitstream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_logo_bitstream_id_fkey FOREIGN KEY (logo_bitstream_id) REFERENCES bitstream(bitstream_id);


--
-- Name: collection_submitter_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_submitter_fkey FOREIGN KEY (submitter) REFERENCES epersongroup(eperson_group_id);


--
-- Name: collection_template_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_template_item_id_fkey FOREIGN KEY (template_item_id) REFERENCES item(item_id);


--
-- Name: collection_workflow_step_1_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_workflow_step_1_fkey FOREIGN KEY (workflow_step_1) REFERENCES epersongroup(eperson_group_id);


--
-- Name: collection_workflow_step_2_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_workflow_step_2_fkey FOREIGN KEY (workflow_step_2) REFERENCES epersongroup(eperson_group_id);


--
-- Name: collection_workflow_step_3_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY collection
    ADD CONSTRAINT collection_workflow_step_3_fkey FOREIGN KEY (workflow_step_3) REFERENCES epersongroup(eperson_group_id);


--
-- Name: com2com_child_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community2community
    ADD CONSTRAINT com2com_child_fk FOREIGN KEY (child_comm_id) REFERENCES community(community_id) DEFERRABLE;


--
-- Name: comm2coll_collection_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community2collection
    ADD CONSTRAINT comm2coll_collection_fk FOREIGN KEY (collection_id) REFERENCES collection(collection_id) DEFERRABLE;


--
-- Name: communities2item_community_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY communities2item
    ADD CONSTRAINT communities2item_community_id_fkey FOREIGN KEY (community_id) REFERENCES community(community_id);


--
-- Name: communities2item_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY communities2item
    ADD CONSTRAINT communities2item_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);


--
-- Name: community2collection_community_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community2collection
    ADD CONSTRAINT community2collection_community_id_fkey FOREIGN KEY (community_id) REFERENCES community(community_id);


--
-- Name: community2community_parent_comm_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community2community
    ADD CONSTRAINT community2community_parent_comm_id_fkey FOREIGN KEY (parent_comm_id) REFERENCES community(community_id);


--
-- Name: community_admin_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community
    ADD CONSTRAINT community_admin_fkey FOREIGN KEY (admin) REFERENCES epersongroup(eperson_group_id);


--
-- Name: community_item_count_community_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community_item_count
    ADD CONSTRAINT community_item_count_community_id_fkey FOREIGN KEY (community_id) REFERENCES community(community_id);


--
-- Name: community_logo_bitstream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY community
    ADD CONSTRAINT community_logo_bitstream_id_fkey FOREIGN KEY (logo_bitstream_id) REFERENCES bitstream(bitstream_id);


--
-- Name: epersongroup2eperson_eperson_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY epersongroup2eperson
    ADD CONSTRAINT epersongroup2eperson_eperson_group_id_fkey FOREIGN KEY (eperson_group_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: epersongroup2eperson_eperson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY epersongroup2eperson
    ADD CONSTRAINT epersongroup2eperson_eperson_id_fkey FOREIGN KEY (eperson_id) REFERENCES eperson(eperson_id);


--
-- Name: epersongroup2workspaceitem_eperson_group_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY epersongroup2workspaceitem
    ADD CONSTRAINT epersongroup2workspaceitem_eperson_group_id_fkey FOREIGN KEY (eperson_group_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: epersongroup2workspaceitem_workspace_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY epersongroup2workspaceitem
    ADD CONSTRAINT epersongroup2workspaceitem_workspace_item_id_fkey FOREIGN KEY (workspace_item_id) REFERENCES workspaceitem(workspace_item_id);


--
-- Name: fileextension_bitstream_format_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY fileextension
    ADD CONSTRAINT fileextension_bitstream_format_id_fkey FOREIGN KEY (bitstream_format_id) REFERENCES bitstreamformatregistry(bitstream_format_id);


--
-- Name: group2group_child_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY group2group
    ADD CONSTRAINT group2group_child_id_fkey FOREIGN KEY (child_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: group2group_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY group2group
    ADD CONSTRAINT group2group_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: group2groupcache_child_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY group2groupcache
    ADD CONSTRAINT group2groupcache_child_id_fkey FOREIGN KEY (child_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: group2groupcache_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY group2groupcache
    ADD CONSTRAINT group2groupcache_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: harvested_collection_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY harvested_collection
    ADD CONSTRAINT harvested_collection_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(collection_id) ON DELETE CASCADE;


--
-- Name: harvested_item_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY harvested_item
    ADD CONSTRAINT harvested_item_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id) ON DELETE CASCADE;


--
-- Name: item2bundle_bundle_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY item2bundle
    ADD CONSTRAINT item2bundle_bundle_id_fkey FOREIGN KEY (bundle_id) REFERENCES bundle(bundle_id);


--
-- Name: item2bundle_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY item2bundle
    ADD CONSTRAINT item2bundle_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);


--
-- Name: item_submitter_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY item
    ADD CONSTRAINT item_submitter_id_fkey FOREIGN KEY (submitter_id) REFERENCES eperson(eperson_id);


--
-- Name: metadatafieldregistry_metadata_schema_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY metadatafieldregistry
    ADD CONSTRAINT metadatafieldregistry_metadata_schema_id_fkey FOREIGN KEY (metadata_schema_id) REFERENCES metadataschemaregistry(metadata_schema_id);


--
-- Name: metadatavalue_metadata_field_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY metadatavalue
    ADD CONSTRAINT metadatavalue_metadata_field_id_fkey FOREIGN KEY (metadata_field_id) REFERENCES metadatafieldregistry(metadata_field_id);


--
-- Name: most_recent_checksum_bitstream_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY most_recent_checksum
    ADD CONSTRAINT most_recent_checksum_bitstream_id_fkey FOREIGN KEY (bitstream_id) REFERENCES bitstream(bitstream_id);


--
-- Name: most_recent_checksum_result_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY most_recent_checksum
    ADD CONSTRAINT most_recent_checksum_result_fkey FOREIGN KEY (result) REFERENCES checksum_results(result_code);


--
-- Name: primary_bitstream_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY bundle
    ADD CONSTRAINT primary_bitstream_id_fk FOREIGN KEY (primary_bitstream_id) REFERENCES bitstream(bitstream_id);


--
-- Name: resourcepolicy_eperson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY resourcepolicy
    ADD CONSTRAINT resourcepolicy_eperson_id_fkey FOREIGN KEY (eperson_id) REFERENCES eperson(eperson_id);


--
-- Name: resourcepolicy_epersongroup_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY resourcepolicy
    ADD CONSTRAINT resourcepolicy_epersongroup_id_fkey FOREIGN KEY (epersongroup_id) REFERENCES epersongroup(eperson_group_id);


--
-- Name: subscription_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(collection_id);


--
-- Name: subscription_eperson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY subscription
    ADD CONSTRAINT subscription_eperson_id_fkey FOREIGN KEY (eperson_id) REFERENCES eperson(eperson_id);


--
-- Name: tasklistitem_eperson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY tasklistitem
    ADD CONSTRAINT tasklistitem_eperson_id_fkey FOREIGN KEY (eperson_id) REFERENCES eperson(eperson_id);


--
-- Name: tasklistitem_workflow_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY tasklistitem
    ADD CONSTRAINT tasklistitem_workflow_id_fkey FOREIGN KEY (workflow_id) REFERENCES workflowitem(workflow_id);


--
-- Name: versionitem_eperson_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY versionitem
    ADD CONSTRAINT versionitem_eperson_id_fkey FOREIGN KEY (eperson_id) REFERENCES eperson(eperson_id);


--
-- Name: versionitem_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY versionitem
    ADD CONSTRAINT versionitem_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);


--
-- Name: versionitem_versionhistory_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY versionitem
    ADD CONSTRAINT versionitem_versionhistory_id_fkey FOREIGN KEY (versionhistory_id) REFERENCES versionhistory(versionhistory_id);


--
-- Name: workflowitem_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY workflowitem
    ADD CONSTRAINT workflowitem_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(collection_id);


--
-- Name: workflowitem_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY workflowitem
    ADD CONSTRAINT workflowitem_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);


--
-- Name: workflowitem_owner_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY workflowitem
    ADD CONSTRAINT workflowitem_owner_fkey FOREIGN KEY (owner) REFERENCES eperson(eperson_id);


--
-- Name: workspaceitem_collection_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY workspaceitem
    ADD CONSTRAINT workspaceitem_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collection(collection_id);


--
-- Name: workspaceitem_item_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY workspaceitem
    ADD CONSTRAINT workspaceitem_item_id_fkey FOREIGN KEY (item_id) REFERENCES item(item_id);


REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

