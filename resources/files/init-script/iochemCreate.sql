--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Name: energy_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE energy_type AS ENUM (
    'potential',
    'free',
    'zero point corrected',
    'enthalpy'
);


ALTER TYPE public.energy_type OWNER TO iochembd;

--
-- Name: param_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE param_type AS ENUM (
    'string',
    'integer',
    'float'
);


ALTER TYPE public.param_type OWNER TO iochembd;

--
-- Name: project_state_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE project_state_type AS ENUM (
    'created',
    'modified',
    'certified',
    'published'
);


ALTER TYPE public.project_state_type OWNER TO iochembd;

--
-- Name: rep_units_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE rep_units_type AS ENUM (
    'kJ*mol-1',
    'kcal*mol-1',
    'eV'
);


ALTER TYPE public.rep_units_type OWNER TO iochembd;

--
-- Name: res_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE res_type AS ENUM (
    'project',
    'calculation'
);


ALTER TYPE public.res_type OWNER TO iochembd;

--
-- Name: roles_type; Type: TYPE; Schema: public; Owner: iochembd
--

CREATE TYPE roles_type AS ENUM (
    'admin',
    'data-architect',
    'leader',
    'user',
    'guest'
);


ALTER TYPE public.roles_type OWNER TO iochembd;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: actions; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE actions (
    id bigint NOT NULL,
    mimetype character varying(50) NOT NULL,
    jumbo_format character varying(50) NOT NULL,
    action character varying(100) NOT NULL,
    parameters character varying(100) NOT NULL
);


ALTER TABLE public.actions OWNER TO iochembd;


CREATE SEQUENCE public.actions_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE public.actions_id_seq OWNER TO iochembd;
ALTER SEQUENCE actions_id_seq OWNED BY actions.id;


--
-- Name: areas_file_ref; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE areas_file_ref (
    id integer NOT NULL,
    calculation_id bigint NOT NULL,
    name character varying(128) NOT NULL,
    file character varying(256) NOT NULL
);


ALTER TABLE public.areas_file_ref OWNER TO iochembd;

--
-- Name: areas_file_ref_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE areas_file_ref_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.areas_file_ref_id_seq OWNER TO iochembd;

--
-- Name: areas_file_ref_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE areas_file_ref_id_seq OWNED BY areas_file_ref.id;


--
-- Name: calculation_types; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE calculation_types (
    id integer NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    abr character varying(4) NOT NULL,
    name character varying(64) NOT NULL,
    description character varying(512) NOT NULL,
    metadata_template character varying(512) DEFAULT ''::character varying NOT NULL
);


ALTER TABLE public.calculation_types OWNER TO iochembd;

--
-- Name: COLUMN calculation_types.metadata_template; Type: COMMENT; Schema: public; Owner: iochembd
--

COMMENT ON COLUMN calculation_types.metadata_template IS 'This field contains XSLT template name in charge of extracting metadata from CML files. Such files must come from current row format.';


--
-- Name: calculation_types_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE calculation_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.calculation_types_id_seq OWNER TO iochembd;

--
-- Name: calculation_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE calculation_types_id_seq OWNED BY calculation_types.id;


--
-- Name: calculations; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE calculations (
    id bigint NOT NULL,
    type_id integer NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    name character varying(64) NOT NULL,
    path character varying(512) NOT NULL,
    description character varying(304) NOT NULL,
    mets_xml text,
    published boolean DEFAULT false NOT NULL,
    handle character varying(256) DEFAULT ''::character varying NOT NULL,
    published_name character varying(256) DEFAULT ''::character varying NOT NULL,
    publication_time timestamp(6) without time zone,
    element_order int NOT NULL DEFAULT 0
);


ALTER TABLE public.calculations OWNER TO iochembd;


--
-- Name: dirs_id; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE dirs_id
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.dirs_id OWNER TO iochembd;

--
-- Name: cutting_area_definitions; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE cutting_area_definitions (
    id integer NOT NULL,
    calculation_type_id integer NOT NULL,
    default_type_sel boolean NOT NULL,
    abbreviation character varying(8) NOT NULL,
    file_name character varying(64) NOT NULL,
    url character varying(256) NOT NULL,
    description character varying(512) NOT NULL,
    jumbo_converter_class character varying(512) NOT NULL,
    jumbo_converter_in_type character varying(32) NOT NULL,
    jumbo_converter_out_type character varying(32) NOT NULL,
    mimetype character varying(32) NOT NULL,
    use character varying(32) NOT NULL,
    label character varying(32) NOT NULL,
    behaviour character varying(256)
);


ALTER TABLE public.cutting_area_definitions OWNER TO iochembd;


--
-- Name: cutting_area_definitions_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE public.cutting_area_definitions_id_seq
    INCREMENT 1
    START 1
    MINVALUE 1
    MAXVALUE 9223372036854775807
    CACHE 1;

ALTER TABLE public.cutting_area_definitions_id_seq OWNER TO iochembd;
ALTER SEQUENCE cutting_area_definitions_id_seq OWNED BY cutting_area_definitions.id;

--
-- Name: projects; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE projects (
    permissions bit(6) DEFAULT B'111000'::"bit" NOT NULL,
    owner_user_id integer NOT NULL,
    owner_group_id integer NOT NULL,
    id bigint NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    concept_group character varying(64) NOT NULL,
    name character varying(64) NOT NULL,
    path character varying(512) NOT NULL,
    description character varying(304) NOT NULL,
    modification_time timestamp without time zone,
    certification_time timestamp without time zone,
    published boolean DEFAULT false NOT NULL,
    handle character varying(256) DEFAULT ''::character varying NOT NULL,
    published_name character varying(256) DEFAULT ''::character varying NOT NULL,
    publication_time timestamp(6) without time zone,
    state project_state_type DEFAULT 'created'::project_state_type NOT NULL,
    element_order int NOT NULL DEFAULT 0
);


ALTER TABLE public.projects OWNER TO iochembd;

--
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.projects_id_seq OWNER TO iochembd;

--
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE projects_id_seq OWNED BY projects.id;


--
-- Name: report_calculations; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE report_calculations (
    id bigint NOT NULL,
    report_id integer NOT NULL,
    calc_order integer NOT NULL,
    title character varying(128) NOT NULL,
    calc_id bigint NOT NULL
);


ALTER TABLE public.report_calculations OWNER TO iochembd;

--
-- Name: report_calculations_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE report_calculations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.report_calculations_id_seq OWNER TO iochembd;

--
-- Name: report_calculations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE report_calculations_id_seq OWNED BY report_calculations.id;


--
-- Name: report_type; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE report_type (
    id integer NOT NULL,
    name character varying(64) DEFAULT ''::bpchar NOT NULL,
    output_type character varying(256) DEFAULT ''::bpchar NOT NULL,
    associated_xslt character varying(1024) DEFAULT ''::bpchar NOT NULL,
    associated_zul character varying(1024) DEFAULT ''::bpchar NOT NULL,
    enabled boolean NOT NULL DEFAULT true
);


ALTER TABLE public.report_type OWNER TO iochembd;

--
-- Name: report_type_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE report_type_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.report_type_id_seq OWNER TO iochembd;

--
-- Name: report_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE report_type_id_seq OWNED BY report_type.id;


--
-- Name: reports; Type: TABLE; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE TABLE reports (
    id integer NOT NULL,
    creation_time timestamp without time zone DEFAULT now(),
    owner_id integer NOT NULL,
    name character varying(64) NOT NULL,
    title character varying(256) NOT NULL,
    description character varying(512) NOT NULL,
    configuration text DEFAULT ''::text,
    type integer DEFAULT 0 NOT NULL,
    published boolean DEFAULT false NOT NULL
);


ALTER TABLE public.reports OWNER TO iochembd;

--
-- Name: reports_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

CREATE SEQUENCE reports_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 2147483647
    CACHE 1;


ALTER TABLE public.reports_id_seq OWNER TO iochembd;

--
-- Name: reports_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: iochembd
--

ALTER SEQUENCE reports_id_seq OWNED BY reports.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY areas_file_ref ALTER COLUMN id SET DEFAULT nextval('areas_file_ref_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY calculation_types ALTER COLUMN id SET DEFAULT nextval('calculation_types_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY projects ALTER COLUMN id SET DEFAULT nextval('projects_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY report_calculations ALTER COLUMN id SET DEFAULT nextval('report_calculations_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY report_type ALTER COLUMN id SET DEFAULT nextval('report_type_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY reports ALTER COLUMN id SET DEFAULT nextval('reports_id_seq'::regclass);



--
-- Name: actions_id_seq; Type: SEQUENCE; Schema: public; Owner: iochembd
--

ALTER TABLE actions ALTER COLUMN id set DEFAULT nextval('actions_id_seq'::regclass);

ALTER TABLE cutting_area_definitions ALTER COLUMN id set DEFAULT nextval('cutting_area_definitions_id_seq'::regclass);



--
-- Data for Name: actions; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO actions VALUES (1, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (2, 'chemical/x-gaussian-input', 'gaussian_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (3, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (4, 'chemical/x-gaussian-input', 'gaussian_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (5, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGAUSSIAN.xsl -extension .html');
INSERT INTO actions VALUES (6, 'chemical/x-cml', 'gaussian_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (7, 'chemical/x-cml', 'gaussian_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (8, 'chemical/x-adf-input', 'adf_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (9, 'chemical/x-adf-input', 'adf_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (10, 'chemical/x-cml', 'adf_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (11, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (12, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (13, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlADF.xsl -extension .html');
INSERT INTO actions VALUES (14, 'chemical/x-cml', 'adf_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (15, 'chemical/x-vasp-incar', 'vasp_incar', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (16, 'chemical/x-vasp-incar', 'vasp_incar', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (17, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (18, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (19, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (20, 'chemical/x-cml', 'vasp_outcar_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlVASP.xsl -extension .html');
INSERT INTO actions VALUES (21, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (22, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (23, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (24, 'chemical/x-cml', 'turbomole_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlTURBOMOLE.xsl -extension .html');
INSERT INTO actions VALUES (25, 'chemical/x-turbomole-control', 'turbomole_control', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (26, 'chemical/x-turbomole-control', 'turbomole_control', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (27, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (28, 'chemical/x-orca-input', 'orca_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (29, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (30, 'chemical/x-orca-input', 'orca_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (31, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlORCA.xsl -extension .html');
INSERT INTO actions VALUES (32, 'chemical/x-cml', 'orca_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (33, 'chemical/x-cml', 'orca_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (34, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (35, 'chemical/x-molcas-input', 'molcas_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (36, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (37, 'chemical/x-molcas-input', 'molcas_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (38, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlMOLCAS.xsl -extension .html');
INSERT INTO actions VALUES (39, 'chemical/x-cml', 'molcas_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (40, 'chemical/x-cml', 'molcas_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (41, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (42, 'chemical/x-qespresso-input', 'qespresso_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (43, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (44, 'chemical/x-qespresso-input', 'qespresso_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (45, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlQUANTUMESPRESSO.xsl -extension .html');
INSERT INTO actions VALUES (46, 'chemical/x-cml', 'qespresso_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (47, 'chemical/x-cml', 'qespresso_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (48, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (49, 'chemical/x-mopac-input', 'mopac_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (50, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (51, 'chemical/x-mopac-input', 'mopac_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (52, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlMOPAC.xsl -extension .html');
INSERT INTO actions VALUES (53, 'chemical/x-cml', 'mopac_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (54, 'chemical/x-cml', 'mopac_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (55, 'chemical/x-gronor-input', 'gronor_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (56, 'chemical/x-gronor-input', 'gronor_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (57, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (58, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (59, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (60, 'chemical/x-cml', 'gronor_xml_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGRONOR.xsl -extension .html');
INSERT INTO actions VALUES (61, 'chemical/x-gromacs-input', 'gromacs_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (62, 'chemical/x-gromacs-input', 'gromacs_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (63, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (64, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (65, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (66, 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGROMACS.xsl -extension .html');
INSERT INTO actions VALUES (67, 'chemical/x-gromos87', 'gromacs_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (68, 'chemical/x-gromos87', 'gromacs_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (69, 'chemical/x-gromacs-trajectory', 'gromacs_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (70, 'chemical/x-amber-input', 'amber_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (71, 'chemical/x-amber-input', 'amber_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (72, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (73, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (74, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (75, 'chemical/x-cml', 'amber_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlAMBER.xsl -extension .html');
INSERT INTO actions VALUES (76, 'chemical/x-amber-topology', 'amber_topology', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (77, 'chemical/x-amber-topology', 'amber_topology', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (78, 'chemical/x-amber-trajectory', 'amber_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');


SELECT setval('actions_id_seq', max(id)) FROM actions;

--
-- Data for Name: areas_file_ref; Type: TABLE DATA; Schema: public; Owner: iochembd
--


SELECT pg_catalog.setval('areas_file_ref_id_seq', 1, true);



--
-- Data for Name: calculation_types; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO calculation_types VALUES (1, '2010-10-11 20:46:10.24976', 'GAU', 'Gaussian', 'Gaussian Type', '/html/xsltmd/gaussian.xsl');
INSERT INTO calculation_types VALUES (2, '2010-10-11 20:46:10.24976', 'ADF', 'ADF', 'ADF Type', '/html/xsltmd/adf.xsl');
INSERT INTO calculation_types VALUES (3, '2010-10-11 20:46:10.24976', 'VSP', 'Vasp', 'VASP Type', '/html/xsltmd/vasp.xsl');
INSERT INTO calculation_types VALUES (4, '2014-12-15 18:09:24', 'TML', 'Turbomole', 'Turbomole Type', '/html/xsltmd/turbomole.xsl');
INSERT INTO calculation_types VALUES (5, '2015-11-02 16:36:01.147714', 'ORC', 'Orca', 'Orca Type', '/html/xsltmd/orca.xsl');
INSERT INTO calculation_types VALUES (6, '2016-07-05 08:15:00.000000', 'MOL', 'Molcas', 'Molcas Type', '/html/xsltmd/molcas.xsl');
INSERT INTO calculation_types VALUES (7, '2018-02-07 18:07:05.936654', 'QEX', 'QuantumEspresso', 'QuantumEspresso Type', '/html/xsltmd/qespresso.xsl');
INSERT INTO calculation_types VALUES (8, '2019-03-06 08:00:00.000000', 'MOP', 'Mopac', 'Mopac Type', '/html/xsltmd/mopac.xsl');
INSERT INTO calculation_types VALUES (9, '2021-04-23 12:00:00.000000', 'GRO', 'GronOR', 'GronOR Type', '/html/xsltmd/gronor.xsl');
INSERT INTO calculation_types VALUES (10, '2021-09-03 12:00:00.000000', 'GRM', 'GROMACS', 'GROMACS Type', '/html/xsltmd/gromacs.xsl');
INSERT INTO calculation_types VALUES (11, '2021-11-16 11:45:00.000000', 'AMB', 'Amber', 'Amber Type', '/html/xsltmd/amber.xsl');

--
-- Name: calculation_types_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('calculation_types_id_seq', 11, true);


--
-- Data for Name: calculations; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Data for Name: cutting_area_definitions; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO cutting_area_definitions VALUES (1, 1, true, '-o', '.*.out|.*.log', '-', 'Gaussian - output file', 'org.xmlcml.cml.converters.compchem.gaussian.log.GaussianLog2CompchemConverter', 'gaussian_log', 'gaussian_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (2, 1, true, '-i', '.*.in|.*.com', '-', 'Gaussian - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gaussian_input', 'gaussian_input', 'chemical/x-gaussian-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (3, 1, false, '-a', '*.*', '-', 'Gaussian - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (4, 2, true, '-o', '.*.out', '-', 'Adf - output file', 'org.xmlcml.cml.converters.compchem.adf.log.AdfLog2CompchemConverter', 'adf_log', 'adf_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (5, 2, true, '-i', '.*.in', '-', 'Adf - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'adf_input', 'adf_input', 'chemical/x-adf-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (6, 2, false, '-a', '*.*', '-', 'Adf - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (7, 3, false, '-a', '*.*', '-', 'VASP - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file/s(5)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (8, 3, false, '-dc', 'DOSCAR', '-', 'VASP - DOSCAR file ', 'org.xmlcml.cml.converters.compchem.vasp.doscar.Doscar2XMLConverter', 'vasp_doscar', 'vasp_doscar_xml', 'chemical/x-cml', 'append', 'DOSCAR file(3)', NULL);
INSERT INTO cutting_area_definitions VALUES (9, 3, true, '-o', 'OUTCAR', '-', 'VASP - OUTCAR file', 'org.xmlcml.cml.converters.compchem.vasp.outcar.VaspOutcar2CompchemConverter', 'vasp_outcar', 'vasp_outcar_compchem', 'chemical/x-cml', 'output', 'OUTCAR file (2*)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (10, 3, false, '-i', 'INCAR', '-', 'VASP - INCAR file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'vasp_incar', 'vasp_incar', 'chemical/x-vasp-incar', 'input', 'INCAR file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (11, 4, false, '-ob', 'basis', '-', 'Turbomole - basis file', 'org.xmlcml.cml.converters.compchem.turbomole.basis.TurbomoleBasis2XMLConverter', 'turbomole_basis', 'turbomole_basis_xml', 'chemical/x-cml', 'append', 'Basis file(3)', NULL);
INSERT INTO cutting_area_definitions VALUES (12, 4, false, '-oc', 'coords', '-', 'Turbomole - coord file', 'org.xmlcml.cml.converters.compchem.turbomole.coord.TurbomoleCoord2XMLConverter', 'turbomole_coord', 'turbomole_coord_xml', 'chemical/x-cml', 'append', 'Coord file(4)', NULL);
INSERT INTO cutting_area_definitions VALUES (13, 4, false, '-o', 'job.last', '-', 'Turbomole - job.last file', 'org.xmlcml.cml.converters.compchem.turbomole.log.TurbomoleLog2CompchemConverter', 'turbomole_log', 'turbomole_log_compchem', 'chemical/x-cml', 'output', 'job.last(5*)', NULL);
INSERT INTO cutting_area_definitions VALUES (14, 4, false, '-a', '*.*', '-', 'Turbomole - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file/s(6)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (15, 4, true, '-i', 'control', '-', 'Turbomole - control file', 'org.xmlcml.cml.converters.compchem.turbomole.control.TurbomoleControl2XMLConverter', 'turbomole_control', 'turbomole_control_xml', 'chemical/x-cml', 'append', 'Control file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (16, 4, true, '-i', 'control', '-', 'Turbomole - control file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'turbomole_control', 'turbomole_control', 'chemical/x-turbomole-control', 'input', 'Control file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (17, 4, false, '-oe', 'energy', '-', 'Turbomole - energy file', 'org.xmlcml.cml.converters.compchem.turbomole.energy.TurbomoleEnergy2XMLConverter', 'turbomole_energy', 'turbomole_energy_xml', 'chemical/x-cml', 'append', 'Energy file(2)', NULL);
INSERT INTO cutting_area_definitions VALUES (18, 3, false, '-kp', 'KPOINTS', '-', 'VASP - KPOINTS file', 'org.xmlcml.cml.converters.compchem.vasp.kpoints.VaspKpoint2XMLConverter', 'vasp_kpoints', 'vasp_kpoints_xml', 'chemical/x-cml', 'append', 'KPOINTS(4)', NULL);
INSERT INTO cutting_area_definitions VALUES (19, 3, false, '-i', 'INCAR', '-', 'VASP - INCAR file', 'org.xmlcml.cml.converters.compchem.vasp.incar.VaspIncar2XMLConverter', 'vasp_incar', 'vasp_incar_xml', 'chemical/x-cml', 'append', 'INCAR file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (20, 5, true, '-i', '*.inp', '-', 'Orca - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'orca_input', 'orca_input', 'chemical/x-orca-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (21, 5, true, '-o', '*.out', '-', 'Orca - output file', 'org.xmlcml.cml.converters.compchem.orca.log.OrcaLog2CompchemConverter', 'orca_log', 'orca_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (22, 5, false, '-a', '*.*', '-', 'Orca - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (23, 6, true, '-i', '*.input', '-', 'Molcas - input file', 'org.xmlcml.cml.converters.compchem.molcas.input.MolcasInput2XMLConverter', 'molcas_input', 'molcas_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (24, 6, true, '-i', '*.input', '-', 'Molcas - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'molcas_input', 'molcas_input', 'chemical/x-molcas-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (25, 6, true, '-o', '*.output', '-', 'Molcas - output file', 'org.xmlcml.cml.converters.compchem.molcas.log.MolcasLog2CompchemConverter', 'molcas_log', 'molcas_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (26, 6, false, '-a', '*.*', '-', 'Molcas - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (27, 7, true, '-i', '.*.in|.*.com', '-', 'QuantumEspresso - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'qespresso_input', 'qespresso_input', 'chemical/x-qespresso-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (28, 7, true, '-o', '.*.out|.*.log', '-', 'QuantumEspresso - output file', 'org.xmlcml.cml.converters.compchem.qespresso.log.QEspressoLog2CompchemConverter', 'qespresso_log', 'qespresso_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (29, 7, false, '-a', '*.*', '-', 'QuantumEspresso - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (30, 7, true, '-i', '.*.in|.*.com', '-', 'QuantumEspresso - input file', 'org.xmlcml.cml.converters.compchem.qespresso.input.QEspressoInput2XMLConverter', 'qespresso_input', 'qespresso_input_xml', 'chemical/x-cml', 'append', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (31, 8, true, '-i', '.*.in|.*.mop', '-', 'Mopac - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'mopac_input', 'mopac_input', 'chemical/x-mopac-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (32, 8, true, '-o', '.*.out|.*.log', '-', 'Mopac - output file', 'org.xmlcml.cml.converters.compchem.mopac.log.MopacLog2CompchemConverter', 'mopac_log', 'mopac_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (33, 8, false, '-a', '*.*', '-', 'Mopac - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (34, 9, true, '-i', '.*.in', '-', 'GronOR - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gronor_input', 'gronor_input', 'chemical/x-gronor-input', 'input', 'Input file(1)', '');
INSERT INTO cutting_area_definitions VALUES (35, 9, true, '-o', '.*.xml|.*.cml', '-', 'GronOR - output file', 'org.xmlcml.cml.converters.compchem.gronor.xml.GronorXML2CompchemConverter', 'gronor_xml', 'gronor_xml_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (36, 9, true, '-a', '.*.*', '-', 'GronOR - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(3)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (37, 10, true, '-i', '.*.in', '-', 'GROMACS - input file', 'org.xmlcml.cml.converters.compchem.gromacs.input.GromacsInput2XMLConverter', 'gromacs_input', 'gromacs_input_xml', 'chemical/x-cml', 'append', 'Input file .mdp(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (38, 10, true, '-i', '.*.in', '-', 'GROMACS - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_input', 'gromacs_input', 'chemical/x-gromacs-input', 'input', 'Input file .mdp(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (39, 10, true, '-o', '.*.log', '-', 'GROMACS - output file', 'org.xmlcml.cml.converters.compchem.gromacs.log.GromacsLog2CompchemConverter', 'gromacs_log', 'gromacs_log_compchem', 'chemical/x-cml', 'output', 'Output file .log(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (40, 10, true, '-oc', '.*.gro', '-', 'GROMACS - geometry file', 'org.xmlcml.cml.converters.compchem.gromacs.geometry.GromacsGeometry2XMLConverter', 'gromacs_geometry', 'gromacs_geometry_xml', 'chemical/x-cml', 'append', 'Geometry file .gro(3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (41, 10, true, '-oc', '.*.gro', '-', 'GROMACS - geometry file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_geometry', 'gromacs_geometry', 'chemical/x-gromos87', 'input', 'Geometry file .gro (3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (42, 10, true, '-t', '.*.xtc', '-', 'GROMACS - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_trajectory', 'gromacs_trajectory', 'chemical/x-gromacs-trajectory', 'input', 'Trajectory file .xtc (4*)', NULL);
INSERT INTO cutting_area_definitions VALUES (43, 10, true, '-a', '.*.*', '-', 'GROMACS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable');
INSERT INTO cutting_area_definitions VALUES (44, 11, true, '-i', '.*.in', '-', 'Amber - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_input', 'amber_input', 'chemical/x-amber-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (45, 11, true, '-o', '.*.log', '-', 'Amber - output file', 'org.xmlcml.cml.converters.compchem.amber.log.AmberLog2CompchemConverter', 'amber_log', 'amber_log_compchem', 'chemical/x-cml', 'output', 'Output file(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (46, 11, true, '-p', '.*.prmtop', '-', 'Amber - topology file', 'org.xmlcml.cml.converters.compchem.amber.topology.AmberTopology2XMLConverter', 'amber_topology', 'amber_topology_xml', 'chemical/x-cml', 'append', 'Topology file .prmtop(3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (47, 11, true, '-p', '.*.prmtop', '-', 'Amber - topology file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_topology', 'amber_topology', 'chemical/x-amber-topology', 'input', 'Topology file .prmtop(3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (48, 11, true, '-ir', '.*.inpcrd', '-', 'Amber - Input coordinates or initial .ncrst file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Input coords .inpcrd .ncrst(4*)', NULL);
INSERT INTO cutting_area_definitions VALUES (49, 11, true, '-r', '.*.ncrst', '-', 'Amber - NetCDF Restart', 'org.xmlcml.cml.converters.compchem.amber.trajectory.AmberTrajectory2XMLConverter', 'amber_trajectory', 'amber_trajectory_xml', 'chemical/x-cml', 'append', 'Final restart file .ncrst(5*)', NULL);
INSERT INTO cutting_area_definitions VALUES (50, 11, true, '-r', '.*.ncrst', '-', 'Amber - NetCDF Restart', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_trajectory', 'amber_trajectory', 'chemical/x-amber-coordinates', 'additional', 'Final restart file .ncrst(5*)', NULL);
INSERT INTO cutting_area_definitions VALUES (51, 11, true, '-t', '.*.nc', '-', 'Amber - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'amber_trajectory', 'amber_trajectory', 'chemical/x-amber-trajectory', 'input', 'Trajectory file .nc (6*)', NULL);
INSERT INTO cutting_area_definitions VALUES (52, 11, true, '-a', '.*.*', '-', 'Amber - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(7)', 'repeatable');



SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;

--
-- Name: dirs_id; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('dirs_id', 1, true);


--
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('projects_id_seq', 1, true);


--
-- Data for Name: report_calculations; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: report_calculations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('report_calculations_id_seq', 1, true);


--
-- Data for Name: report_type; Type: TABLE DATA; Schema: public; Owner: iochembd
--

INSERT INTO report_type VALUES (1, 'Supporting information', 'PDF|RTF', '/html/reports/ReportSupportingInformation/getAvailableEnergies.xsl|/html/reports/ReportSupportingInformation/dataExtraction.xsl|/html/reports/ReportSupportingInformation/supportingInformation.xsl', 'reportSupportingInformation.zul', true);
INSERT INTO report_type VALUES (2, 'Spin state energies for transition metal complexes', 'PDF|RTF', '/html/reports/ReportTurbomoleSpinStateEnergies2/dataExtraction.xsl|/html/reports/ReportTurbomoleSpinStateEnergies2/spinStateEnergies.xsl', 'reportTurbomoleSpinStateEnergies2.zul', false);
INSERT INTO report_type VALUES (3, 'Reaction energy profile', 'CHART', '/html/reports/ReportEnergyReactionProfile/getAvailableEnergies.xsl|/html/reports/ReportEnergyReactionProfile/getTempPressure.xsl|/html/reports/ReportEnergyReactionProfile/dataExtraction.xsl|/html/reports/ReportEnergyReactionProfile/chartGeneration.xsl', 'reportEnergyReactionProfile.zul', true);

--
-- Name: report_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('report_type_id_seq', 3, false);


--
-- Data for Name: reports; Type: TABLE DATA; Schema: public; Owner: iochembd
--



--
-- Name: reports_id_seq; Type: SEQUENCE SET; Schema: public; Owner: iochembd
--

SELECT pg_catalog.setval('reports_id_seq', 1, true);


--
-- Name: actions_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY actions
    ADD CONSTRAINT actions_id_key UNIQUE (id);


--
-- Name: actions_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY actions
    ADD CONSTRAINT actions_pkey PRIMARY KEY (id);


--
-- Name: areas_file_ref_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY areas_file_ref
    ADD CONSTRAINT areas_file_ref_pkey PRIMARY KEY (id);


--
-- Name: calculation_types_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY calculation_types
    ADD CONSTRAINT calculation_types_pkey PRIMARY KEY (id);


--
-- Name: calculations_for_reporting_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY report_calculations
    ADD CONSTRAINT calculations_for_reporting_pkey PRIMARY KEY (id);


--
-- Name: calculations_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY calculations
    ADD CONSTRAINT calculations_id_key UNIQUE (id);


--
-- Name: calculations_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY calculations
    ADD CONSTRAINT calculations_pkey PRIMARY KEY (name, path);


--
-- Name: cutting_area_definitions_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY cutting_area_definitions
    ADD CONSTRAINT cutting_area_definitions_pkey PRIMARY KEY (id);


--
-- Name: id_unique; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT id_unique UNIQUE (id);


--
-- Name: projects_id_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_id_key UNIQUE (id);


--
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (name, path);


--
-- Name: report_calculations_id; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY report_calculations
    ADD CONSTRAINT report_calculations_id UNIQUE (id);


--
-- Name: report_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT report_pkey PRIMARY KEY (id);


--
-- Name: report_type_key; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY report_type
    ADD CONSTRAINT report_type_key UNIQUE (id);


--
-- Name: report_type_pkey; Type: CONSTRAINT; Schema: public; Owner: iochembd; Tablespace: 
--

ALTER TABLE ONLY report_type
    ADD CONSTRAINT report_type_pkey PRIMARY KEY (id);


--
-- Name: calc_order_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX calc_order_calc_rep_index ON report_calculations USING btree (calc_order);


--
-- Name: certification_time_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX certification_time_pro_index ON projects USING btree (certification_time);


--
-- Name: concept_group_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX concept_group_pro_index ON projects USING btree (concept_group);


--
-- Name: cre_time_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX cre_time_rep_index ON reports USING btree (creation_time);


--
-- Name: creation_time_cal_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX creation_time_cal_index ON calculations USING btree (creation_time);


--
-- Name: creation_time_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX creation_time_pro_index ON projects USING btree (creation_time);


--
-- Name: desc_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX desc_rep_index ON reports USING btree (description);


--
-- Name: description_cal_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX description_cal_index ON calculations USING btree (description);


--
-- Name: description_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX description_pro_index ON projects USING btree (description);


--
-- Name: fki_report_calculations_calc_id_fk; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX fki_report_calculations_calc_id_fk ON report_calculations USING btree (calc_id);


--
-- Name: id_action_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX id_action_index ON actions USING btree (id);


--
-- Name: id_cal_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX id_cal_index ON calculations USING btree (id);


--
-- Name: id_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX id_calc_rep_index ON report_calculations USING btree (id);


--
-- Name: id_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX id_pro_index ON projects USING btree (id);


--
-- Name: id_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX id_rep_index ON reports USING btree (id);


--
-- Name: id_report_type_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX id_report_type_index ON report_type USING btree (id);


--
-- Name: modification_time_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX modification_time_pro_index ON projects USING btree (modification_time);


--
-- Name: name_cal_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX name_cal_index ON calculations USING btree (name);


--
-- Name: name_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX name_pro_index ON projects USING btree (name);


--
-- Name: name_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX name_rep_index ON reports USING btree (name);


--
-- Name: owner_group_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX owner_group_pro_index ON projects USING btree (owner_group_id);


--
-- Name: owner_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX owner_rep_index ON reports USING btree (owner_id);


--
-- Name: owner_user_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX owner_user_pro_index ON projects USING btree (owner_user_id);


--
-- Name: path_cal_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX path_cal_index ON calculations USING btree (path);


--
-- Name: path_name_cal_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX path_name_cal_index ON calculations USING btree (path, name);


--
-- Name: path_name_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX path_name_pro_index ON projects USING btree (path, name);


--
-- Name: path_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX path_pro_index ON projects USING btree (path);


--
-- Name: perm_pro_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX perm_pro_index ON projects USING btree (permissions);


--
-- Name: rep_id_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX rep_id_calc_rep_index ON report_calculations USING btree (report_id);


--
-- Name: title_calc_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX title_calc_rep_index ON report_calculations USING btree (title);


--
-- Name: title_rep_index; Type: INDEX; Schema: public; Owner: iochembd; Tablespace: 
--

CREATE INDEX title_rep_index ON reports USING btree (title);


--
-- Name: areas_file_ref_calculation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY areas_file_ref
    ADD CONSTRAINT areas_file_ref_calculation_id_fkey FOREIGN KEY (calculation_id) REFERENCES calculations(id);


--
-- Name: calculations_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY calculations
    ADD CONSTRAINT calculations_type_id_fkey FOREIGN KEY (type_id) REFERENCES calculation_types(id);


--
-- Name: cutting_area_definitions_calculation_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY cutting_area_definitions
    ADD CONSTRAINT cutting_area_definitions_calculation_type_id_fkey FOREIGN KEY (calculation_type_id) REFERENCES calculation_types(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report_calculations_calc_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY report_calculations
    ADD CONSTRAINT report_calculations_calc_id_fk FOREIGN KEY (calc_id) REFERENCES calculations(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report_calculations_report_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY report_calculations
    ADD CONSTRAINT report_calculations_report_id_fkey FOREIGN KEY (report_id) REFERENCES reports(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- Name: report_type_id_fk; Type: FK CONSTRAINT; Schema: public; Owner: iochembd
--

ALTER TABLE ONLY reports
    ADD CONSTRAINT report_type_id_fk FOREIGN KEY (type) REFERENCES report_type(id) ON UPDATE CASCADE ON DELETE CASCADE;




--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- Name: calculations; Type: ACL; Schema: public; Owner: iochembd
--

REVOKE ALL ON TABLE calculations FROM PUBLIC;
REVOKE ALL ON TABLE calculations FROM iochembd;
GRANT ALL ON TABLE calculations TO iochembd;


--
-- Name: projects; Type: ACL; Schema: public; Owner: iochembd
--

REVOKE ALL ON TABLE projects FROM PUBLIC;
REVOKE ALL ON TABLE projects FROM iochembd;
GRANT ALL ON TABLE projects TO iochembd;


--
-- PostgreSQL database dump complete
--

