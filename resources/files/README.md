# ioChem-BD chemical platform

[ioChem-BD](https://www.iochem-bd.org) relies in an unique software platform which is builded in a modular way to satisfy all the needs: data creation and curation, publishing, storage, indexing data and search engine services.

The main node of the network runs the **Find Module**, which acts as central server and is feed by any new data published in the repositories. The Find Module provides a fast chemical-aware search engine open public service and is hosted at the Barcelona Supercomputer Center (BSC). Also, the first public-access node is provided by BSC.

Other ioChem-BD modules automatize relevant data-extracting processes and transforms raw numerical data into labeled data in a database. It provides the researcher with tools for validating, enriching, publishing and sharing information, as well as tools to access, post-process and visualize data. The final goal is to contribute building a new reference tool in research. Users include computational chemistry research groups worldwide, university libraries and related services, and high performance supercomputer centers. 

Current package bundles a webserver abd both **Create** and **Browse** modules. 
You can read more about its installation on this [help pages](https://docs.iochem-bd.org/en/latest/guides/installation/system-requirements.html#system-requirements).

## Folder structure

The package is composed of the following folders:

* init-script: Contains installation required files
* apache-tomcat: The web server 
* browse: Browse module assetstore, additional libraries and configuration files 
* create: Create module assetstrore and configuration file  
* THIRD-PARTY-licenses: Third party licenses text files
* webapps: Directory with all the webapps are offered via HTTPS
* webapps2: Directory with all the webapps are offered via HTTP
* updates Update utility folder 

And the following files:

* README.md: This file
* init.sh: Installation script
* installer.jar: Installer tool used by init.sh utility

## Installation steps 

The software requires a *PostgreSQL* database server, *gcc* and optionally *libcap-progs* software packages, (review [platform requirements](https://www.iochem-bd.org/index-introduction.jsp)).

The installation procedure is quite straigtforward just by using the installer utility *init.sh* if you follow the steps described [in the documentation pages](https://docs.iochem-bd.org/en/latest/guides/installation/installation.html#installation-procedure).
