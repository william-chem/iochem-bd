# ioChem-BD third party (not-java) license summary

The following is a list of all third-party dependencies (not java related) used inside the ioChem-BD project, grouped by their license type. You can review each one on [SPDX webpage](https://spdx.org/licenses/).

The full text for each listed license can be found in *./license/third-party* folder.

## License list


###  Apache-2.0:

Name | version | url
---- | ------- | ----
hopscotch | 0.2.5 | http://fenixrepo.fao.org/cdn/faostat/js/hopscotch/0.2.6/demo/


###  BSD-3-Clause

Name | version | url
---- | ------- | ----
Ace code editor |  1.4.10 | https://github.com/ajaxorg/ace
D3.js | 5.15.0 | https://d3js.org/
d3-graphviz | 3.0.0 | https://github.com/magjac/d3-graphviz

###  CC-BY-4.0	

Name | version | url
---- | ------- | ----
Fontawesome free | 5 | https://fontawesome.com/license/free

###  LGPL-2.0-only
    			
Name | version | url
---- | ------- | ----
Jsmol | 14.29.25 | https://sourceforge.net/projects/jsmol/
JSpecView | 2 | https://sourceforge.net/projects/jspecview/
Jmol | 14.29.25 | https://sourceforge.net/projects/jmol/						
			
###  MIT
    
Name | version | url
---- | ------- | ----
Bootstrap | 4.1.3 | https://github.com/twbs/bootstrap/blob/master/LICENSE
Bootstrap-table | 1.11.0 | https://github.com/wenzhixin/bootstrap-table
Datatables | 1.11.5 | https://datatables.net/license/mit
Highslide JS | 5.0.0 | http://highslide.com/#licence
jQuery | 3.3.1 | https://jquery.org/license/
Jquery UI | 1.12.1 | https://github.com/jquery/jquery-ui/blob/master/LICENSE.txt
plotly.js | 1.49.0 | https://github.com/plotly/plotly.js/blob/master/LICENSE
popper.js | 2.5.0 | https://github.com/popperjs/popper-core
script.aculo.us | 1.8.2 | http://script.aculo.us/
Split.js | 1.5.9 | https://github.com/nathancahill/split/blob/master/LICENSE

###  MIT, GPL-2.0						

Name | version | url
---- | ------- | ----
jQuery blockUI plugin | 2.70.0 | https://github.com/malsup/blockui/
    
###  MPL-1.1

Name | version | url
---- | ------- | ----
FXSL - xslt Functional Programming Library | 2 | https://sourceforge.net/projects/fxsl/

###  SIL Open Font License			

Name | version | url
---- | ------- | ----
STIX Fonts | 2.0.2 | https://github.com/stipub/stixfonts/blob/master/docs/STIX_2.0.2_license.pdf



