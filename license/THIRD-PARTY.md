# ioChem-BD third party license summary

The following is a list of all Java third-party dependencies used inside the ioChem-BD project, grouped by their license type. You can review each one on [SPDX webpage](https://spdx.org/licenses/).

The full text for each listed license can be found in *./third-party* folder.

## License list


###  3-Clause BSD License, BSD-3-Clause:

Name | id | version | url
---- | --- | --------- | ----
Protocol Buffers [Core] | com.google.protobuf:protobuf-java | 3.12.4 | https://developers.google.com/protocol-buffers/protobuf-java/

###  AGPL-3.0-or-later:

Name | id | version | url
---- | --- | --------- | ----
ioChem-BD Create shell project | cat.iciq.iochembd.create:shell | 2.7.2 | https://gitlab.com/ioChem-BD/iochem-bd/-/tree/master/createshell
jumbo-saxon | jumbo:jumbo-saxon | 2.7.2 | https://gitlab.com/ioChem-BD/iochem-bd/-/tree/master/jumbo-saxon

###  Apache-1.0:

Name | id | version | url
---- | --- | --------- | ----
oscache | opensymphony:oscache | 2.1.1 | no url defined
qdox | qdox:qdox | 1.5 | no url defined

###  Apache-2.0:

Name | id | version | url
---- | --- | --------- | ----
Abdera Client | org.apache.abdera:abdera-client | 1.1.1 | http://abdera.apache.org/abdera-client
Abdera Core | org.apache.abdera:abdera-core | 1.1.1 | http://abdera.apache.org/abdera-core
Abdera Parser | org.apache.abdera:abdera-parser | 1.1.1 | http://abdera.apache.org/abdera-parser
Activation | org.apache.geronimo.specs:geronimo-activation_1.0.2_spec | 1.1 | http://geronimo.apache.org/geronimo-activation_1.0.2_spec
Activation 1.1 | org.apache.geronimo.specs:geronimo-activation_1.1_spec | 1.0.2 | http://geronimo.apache.org/specs/geronimo-activation_1.1_spec
aljava | xyz.avarel:aljava | 0.0.3 | no url defined
Annotations for Metrics | io.dropwizard.metrics:metrics-annotation | 3.2.5 | http://metrics.dropwizard.io/metrics-annotation/
Ant-Contrib Tasks | ant-contrib:ant-contrib | 1.0b3 | http://ant-contrib.sourceforge.net
ant-launcher | org.apache.ant:ant-launcher | 1.7.0 | http://ant.apache.org/ant-launcher/
Apache Commons BeanUtils | commons-beanutils:commons-beanutils | 1.9.3 | https://commons.apache.org/proper/commons-beanutils/
Apache Commons CLI | commons-cli:commons-cli | 1.3 | http://commons.apache.org/proper/commons-cli/
Apache Commons CLI | commons-cli:commons-cli | 1.4 | http://commons.apache.org/proper/commons-cli/
Apache Commons Codec | commons-codec:commons-codec | 1.11 | http://commons.apache.org/proper/commons-codec/
Apache Commons Codec | commons-codec:commons-codec | 1.9 | http://commons.apache.org/proper/commons-codec/
Apache Commons Collections | commons-collections:commons-collections | 3.2.2 | http://commons.apache.org/collections/
Apache Commons Collections | org.apache.commons:commons-collections4 | 4.1 | http://commons.apache.org/proper/commons-collections/
Apache Commons Compress | org.apache.commons:commons-compress | 1.21 | https://commons.apache.org/proper/commons-compress/
Apache Commons Compress | org.apache.commons:commons-compress | 1.7 | http://commons.apache.org/proper/commons-compress/
Apache Commons Configuration | org.apache.commons:commons-configuration2 | 2.2 | http://commons.apache.org/proper/commons-configuration/
Apache Commons FileUpload | commons-fileupload:commons-fileupload | 1.3.2 | http://commons.apache.org/proper/commons-fileupload/
Apache Commons IO | commons-io:commons-io | 2.6 | http://commons.apache.org/proper/commons-io/
Apache Commons Lang | org.apache.commons:commons-lang3 | 3.7 | http://commons.apache.org/proper/commons-lang/
Apache Commons Lang | org.apache.commons:commons-lang3 | 3.8.1 | http://commons.apache.org/proper/commons-lang/
Apache Commons Logging | commons-logging:commons-logging | 1.2 | http://commons.apache.org/proper/commons-logging/
Apache Commons Pool | org.apache.commons:commons-pool2 | 2.5.0 | http://commons.apache.org/proper/commons-pool/
Apache Commons Text | org.apache.commons:commons-text | 1.3 | http://commons.apache.org/proper/commons-text/
Apache Commons Text | org.apache.commons:commons-text | 1.6 | http://commons.apache.org/proper/commons-text
Apache Commons Validator | commons-validator:commons-validator | 1.6 | http://commons.apache.org/proper/commons-validator/
Apache FontBox | org.apache.pdfbox:fontbox | 1.8.7 | http://pdfbox.apache.org/
Apache FOP | org.apache.xmlgraphics:fop | 2.1 | http://xmlgraphics.apache.org/fop/
Apache Groovy | org.codehaus.groovy:groovy | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-console | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-groovysh | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-json | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-jsr223 | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-sql | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-swing | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-templates | 2.4.15 | http://groovy-lang.org
Apache Groovy | org.codehaus.groovy:groovy-xml | 2.4.15 | http://groovy-lang.org
Apache Hadoop Annotations | org.apache.hadoop:hadoop-annotations | 2.2.0 | no url defined
Apache Hadoop Auth | org.apache.hadoop:hadoop-auth | 2.2.0 | no url defined
Apache Hadoop Common | org.apache.hadoop:hadoop-common | 2.2.0 | no url defined
Apache Hadoop HDFS | org.apache.hadoop:hadoop-hdfs | 2.2.0 | no url defined
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.3.5 | http://hc.apache.org/httpcomponents-client
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.4 | http://hc.apache.org/httpcomponents-client
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.5.13 | http://hc.apache.org/httpcomponents-client
Apache HttpClient | org.apache.httpcomponents:httpclient | 4.5.6 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Cache | org.apache.httpcomponents:httpclient-cache | 4.2.6 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Mime | org.apache.httpcomponents:httpmime | 4.3.1 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Mime | org.apache.httpcomponents:httpmime | 4.4 | http://hc.apache.org/httpcomponents-client
Apache HttpClient Mime | org.apache.httpcomponents:httpmime | 4.5.13 | http://hc.apache.org/httpcomponents-client
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.3.2 | http://hc.apache.org/httpcomponents-core-ga
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.4.13 | http://hc.apache.org/httpcomponents-core-ga
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.4.9 | http://hc.apache.org/httpcomponents-core-ga
Apache HttpCore | org.apache.httpcomponents:httpcore | 4.4 | http://hc.apache.org/httpcomponents-core-ga
Apache JAMES Mime4j (Core) | org.apache.james:apache-mime4j-core | 0.7.2 | http://james.apache.org/mime4j/apache-mime4j-core
Apache JAMES Mime4j (DOM) | org.apache.james:apache-mime4j-dom | 0.7.2 | http://james.apache.org/mime4j/apache-mime4j-dom
Apache JAMES Mime4j | org.apache.james:apache-mime4j | 0.6 | http://james.apache.org/mime4j
Apache JempBox | org.apache.pdfbox:jempbox | 1.8.7 | http://www.apache.org/pdfbox-parent/jempbox/
Apache Jena - ARQ (SPARQL 1.1 Query Engine) | org.apache.jena:jena-arq | 2.12.0 | http://jena.apache.org/jena-arq/
Apache Jena - Core | org.apache.jena:jena-core | 2.12.0 | http://jena.apache.org/jena-core/
Apache Jena - IRI | org.apache.jena:jena-iri | 1.1.0 | http://jena.apache.org/jena-iri/
Apache Jena - Libraries POM | org.apache.jena:apache-jena-libs | 2.12.0 | http://jena.apache.org/apache-jena-libs/
Apache Jena - TDB (Native Triple Store) | org.apache.jena:jena-tdb | 1.1.0 | http://jena.apache.org/jena-tdb/
Apache Log4j | log4j:log4j | 1.2.16 | http://logging.apache.org/log4j/1.2/
Apache Log4j | log4j:log4j | 1.2.17 | http://logging.apache.org/log4j/1.2/
Apache Log4j 1.x Compatibility API | org.apache.logging.log4j:log4j-1.2-api | 2.16.0 | https://logging.apache.org/log4j/2.x/log4j-1.2-api/
Apache Log4j API | org.apache.logging.log4j:log4j-api | 2.16.0 | https://logging.apache.org/log4j/2.x/log4j-api/
Apache Log4j Commons Logging Bridge | org.apache.logging.log4j:log4j-jcl | 2.16.0 | https://logging.apache.org/log4j/2.x/log4j-jcl/
Apache Log4j Core | org.apache.logging.log4j:log4j-core | 2.16.0 | https://logging.apache.org/log4j/2.x/log4j-core/
Apache Log4j SLF4J Binding | org.apache.logging.log4j:log4j-slf4j-impl | 2.16.0 | https://logging.apache.org/log4j/2.x/log4j-slf4j-impl/
Apache Log4j Web | org.apache.logging.log4j:log4j-web | 2.16.0 | https://logging.apache.org/log4j/2.x/log4j-web/
Apache PDFBox | org.apache.pdfbox:pdfbox | 1.8.7 | http://www.apache.org/pdfbox-parent/pdfbox/
Apache POI | org.apache.poi:poi | 3.6 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-ooxml | 3.6 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-ooxml-schemas | 3.10.1 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-ooxml-schemas | 3.6 | http://poi.apache.org/
Apache POI | org.apache.poi:poi-scratchpad | 3.6 | http://poi.apache.org/
Apache Shiro :: Cache | org.apache.shiro:shiro-cache | 1.4.0 | http://shiro.apache.org/shiro-cache/
Apache Shiro :: Cache | org.apache.shiro:shiro-cache | 1.4.1 | http://shiro.apache.org/shiro-cache/
Apache Shiro :: Configuration :: Core | org.apache.shiro:shiro-config-core | 1.4.0 | http://shiro.apache.org/shiro-config-core/
Apache Shiro :: Configuration :: Core | org.apache.shiro:shiro-config-core | 1.4.1 | http://shiro.apache.org/shiro-config/shiro-config-core/
Apache Shiro :: Configuration :: OGDL | org.apache.shiro:shiro-config-ogdl | 1.4.0 | http://shiro.apache.org/shiro-config-ogdl/
Apache Shiro :: Configuration :: OGDL | org.apache.shiro:shiro-config-ogdl | 1.4.1 | http://shiro.apache.org/shiro-config/shiro-config-ogdl/
Apache Shiro :: Core | org.apache.shiro:shiro-core | 1.4.0 | http://shiro.apache.org/shiro-core/
Apache Shiro :: Core | org.apache.shiro:shiro-core | 1.4.1 | http://shiro.apache.org/shiro-core/
Apache Shiro :: Cryptography :: Ciphers | org.apache.shiro:shiro-crypto-cipher | 1.4.0 | http://shiro.apache.org/shiro-crypto-cipher/
Apache Shiro :: Cryptography :: Ciphers | org.apache.shiro:shiro-crypto-cipher | 1.4.1 | http://shiro.apache.org/shiro-crypto/shiro-crypto-cipher/
Apache Shiro :: Cryptography :: Core | org.apache.shiro:shiro-crypto-core | 1.4.0 | http://shiro.apache.org/shiro-crypto-core/
Apache Shiro :: Cryptography :: Core | org.apache.shiro:shiro-crypto-core | 1.4.1 | http://shiro.apache.org/shiro-crypto/shiro-crypto-core/
Apache Shiro :: Cryptography :: Hashing | org.apache.shiro:shiro-crypto-hash | 1.4.0 | http://shiro.apache.org/shiro-crypto-hash/
Apache Shiro :: Cryptography :: Hashing | org.apache.shiro:shiro-crypto-hash | 1.4.1 | http://shiro.apache.org/shiro-crypto/shiro-crypto-hash/
Apache Shiro :: Event | org.apache.shiro:shiro-event | 1.4.0 | http://shiro.apache.org/shiro-event/
Apache Shiro :: Event | org.apache.shiro:shiro-event | 1.4.1 | http://shiro.apache.org/shiro-event/
Apache Shiro :: Lang | org.apache.shiro:shiro-lang | 1.4.0 | http://shiro.apache.org/shiro-lang/
Apache Shiro :: Lang | org.apache.shiro:shiro-lang | 1.4.1 | http://shiro.apache.org/shiro-lang/
Apache Shiro :: Support :: CAS | org.apache.shiro:shiro-cas | 1.4.1 | http://shiro.apache.org/shiro-support/shiro-cas/
Apache Shiro :: Web | org.apache.shiro:shiro-web | 1.4.1 | http://shiro.apache.org/shiro-web/
Apache Solr Analysis Extras | org.apache.solr:solr-analysis-extras | 4.10.2 | http://lucene.apache.org/solr-parent/solr-analysis-extras
Apache Solr Content Extraction Library | org.apache.solr:solr-cell | 4.10.2 | http://lucene.apache.org/solr-parent/solr-cell
Apache Solr Core | org.apache.solr:solr-core | 4.10.2 | http://lucene.apache.org/solr-parent/solr-core
Apache Solr Search Server | org.apache.solr:solr | 4.10.2 | http://lucene.apache.org/solr-parent/solr
Apache Solr Solrj | org.apache.solr:solr-solrj | 4.10.2 | http://lucene.apache.org/solr-parent/solr-solrj
Apache Tika core | org.apache.tika:tika-core | 1.5 | http://tika.apache.org/
Apache Tika parsers | org.apache.tika:tika-parsers | 1.5 | http://tika.apache.org/
Apache Tika plugin for Ogg, Vorbis and FLAC | org.gagravarr:vorbis-java-tika | 0.1 | https://github.com/Gagravarr/VorbisJava
Apache Tika XMP | org.apache.tika:tika-xmp | 1.5 | http://tika.apache.org/
Apache Velocity | org.apache.velocity:velocity | 1.7 | http://velocity.apache.org/engine/devel/
Apache Velocity - Engine | org.apache.velocity:velocity-engine-core | 2.0 | http://velocity.apache.org/engine/devel/velocity-engine-core/
Apache XML Graphics Commons | org.apache.xmlgraphics:xmlgraphics-commons | 2.1 | http://xmlgraphics.apache.org/commons/
Apache XML Security for Java | org.apache.santuario:xmlsec | 2.0.10 | http://santuario.apache.org/
ASM based accessors helper used by json-smart | net.minidev:accessors-smart | 1.2 | http://www.minidev.net/
attoparser | org.attoparser:attoparser | 2.0.5.RELEASE | http://www.attoparser.org
Avalon Framework API | org.apache.avalon.framework:avalon-framework-api | 4.3.1 | http://www.apache.org/excalibur/avalon-framework/avalon-framework-api/
Avalon Framework Implementation | org.apache.avalon.framework:avalon-framework-impl | 4.3.1 | http://www.apache.org/excalibur/avalon-framework/avalon-framework-impl/
Axiom API | org.apache.ws.commons.axiom:axiom-api | 1.2.10 | http://ws.apache.org/axiom/axiom-api/
Axiom Impl | org.apache.ws.commons.axiom:axiom-impl | 1.2.10 | http://ws.apache.org/axiom/axiom-impl/
Batik animation engine | org.apache.xmlgraphics:batik-anim | 1.8 | http://xmlgraphics.apache.org/batik/
Batik AWT utilities | org.apache.xmlgraphics:batik-awt-util | 1.8 | http://xmlgraphics.apache.org/batik/
Batik bridge classes | org.apache.xmlgraphics:batik-bridge | 1.8 | http://xmlgraphics.apache.org/batik/
Batik CSS engine | org.apache.xmlgraphics:batik-css | 1.8 | http://xmlgraphics.apache.org/batik/
Batik DOM implementation | org.apache.xmlgraphics:batik-dom | 1.8 | http://xmlgraphics.apache.org/batik/
Batik extension classes | org.apache.xmlgraphics:batik-extension | 1.8 | http://xmlgraphics.apache.org/batik/
Batik external code | org.apache.xmlgraphics:batik-ext | 1.8 | http://xmlgraphics.apache.org/batik/
Batik GVT (Graphics Vector Tree) | org.apache.xmlgraphics:batik-gvt | 1.8 | http://xmlgraphics.apache.org/batik/
Batik Java2D SVG generator | org.apache.xmlgraphics:batik-svggen | 1.8 | http://xmlgraphics.apache.org/batik/
Batik scripting language classes | org.apache.xmlgraphics:batik-script | 1.8 | http://xmlgraphics.apache.org/batik/
Batik SVG DOM implementation | org.apache.xmlgraphics:batik-svg-dom | 1.8 | http://xmlgraphics.apache.org/batik/
Batik SVG microsyntax parser library | org.apache.xmlgraphics:batik-parser | 1.8 | http://xmlgraphics.apache.org/batik/
Batik SVG transcoder classes | org.apache.xmlgraphics:batik-transcoder | 1.8 | http://xmlgraphics.apache.org/batik/
Batik utility library | org.apache.xmlgraphics:batik-util | 1.8 | http://xmlgraphics.apache.org/batik/
Batik XML utility library | org.apache.xmlgraphics:batik-xml | 1.8 | http://xmlgraphics.apache.org/batik/
BeanShell | org.apache-extras.beanshell:bsh | 2.0b6 | https://github.com/beanshell/beanshell/
Bean Validation API | javax.validation:validation-api | 1.1.0.Final | http://beanvalidation.org
Boilerpipe -- Boilerplate Removal and Fulltext Extraction from HTML pages | de.l3s.boilerpipe:boilerpipe | 1.1.0 | http://code.google.com/p/boilerpipe/
builder-commons | com.lyncode:builder-commons | 1.0.2 | http://nexus.sonatype.org/oss-repository-hosting.html/builder-commons
Byte Buddy (without dependencies) | net.bytebuddy:byte-buddy | 1.6.14 | http://bytebuddy.net/byte-buddy
Byte Buddy Java agent | net.bytebuddy:byte-buddy-agent | 1.7.9 | http://bytebuddy.net/byte-buddy-agent
Caffeine cache | com.github.ben-manes.caffeine:caffeine | 2.6.2 | https://github.com/ben-manes/caffeine
Caffeine cache | com.github.ben-manes.caffeine:guava | 2.6.2 | https://github.com/ben-manes/caffeine
cas-server-core-api | org.apereo.cas:cas-server-core-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-audit | org.apereo.cas:cas-server-core-api-audit | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-authentication | org.apereo.cas:cas-server-core-api-authentication | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-configuration | org.apereo.cas:cas-server-core-api-configuration | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-configuration-model | org.apereo.cas:cas-server-core-api-configuration-model | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-events | org.apereo.cas:cas-server-core-api-events | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-logout | org.apereo.cas:cas-server-core-api-logout | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-protocol | org.apereo.cas:cas-server-core-api-protocol | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-services | org.apereo.cas:cas-server-core-api-services | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-throttle | org.apereo.cas:cas-server-core-api-throttle | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-ticket | org.apereo.cas:cas-server-core-api-ticket | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-util | org.apereo.cas:cas-server-core-api-util | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-validation | org.apereo.cas:cas-server-core-api-validation | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-web | org.apereo.cas:cas-server-core-api-web | 5.3.12 | http://www.apereo.org/cas
cas-server-core-api-webflow | org.apereo.cas:cas-server-core-api-webflow | 5.3.12 | http://www.apereo.org/cas
cas-server-core-authentication | org.apereo.cas:cas-server-core-authentication | 5.3.12 | http://www.apereo.org/cas
cas-server-core-authentication-api | org.apereo.cas:cas-server-core-authentication-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-authentication-attributes | org.apereo.cas:cas-server-core-authentication-attributes | 5.3.12 | http://www.apereo.org/cas
cas-server-core-authentication-mfa | org.apereo.cas:cas-server-core-authentication-mfa | 5.3.12 | http://www.apereo.org/cas
cas-server-core-configuration-api | org.apereo.cas:cas-server-core-configuration-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-rest | org.apereo.cas:cas-server-core-rest | 5.3.12 | http://www.apereo.org/cas
cas-server-core-services | org.apereo.cas:cas-server-core-services | 5.3.12 | http://www.apereo.org/cas
cas-server-core-services-api | org.apereo.cas:cas-server-core-services-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-services-authentication | org.apereo.cas:cas-server-core-services-authentication | 5.3.12 | http://www.apereo.org/cas
cas-server-core-services-registry | org.apereo.cas:cas-server-core-services-registry | 5.3.12 | http://www.apereo.org/cas
cas-server-core-tickets | org.apereo.cas:cas-server-core-tickets | 5.3.12 | http://www.apereo.org/cas
cas-server-core-tickets-api | org.apereo.cas:cas-server-core-tickets-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-util | org.apereo.cas:cas-server-core-util | 5.3.12 | http://www.apereo.org/cas
cas-server-core-util-api | org.apereo.cas:cas-server-core-util-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-validation | org.apereo.cas:cas-server-core-validation | 5.3.12 | http://www.apereo.org/cas
cas-server-core-validation-api | org.apereo.cas:cas-server-core-validation-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-web-api | org.apereo.cas:cas-server-core-web-api | 5.3.12 | http://www.apereo.org/cas
cas-server-core-webflow | org.apereo.cas:cas-server-core-webflow | 5.3.12 | http://www.apereo.org/cas
cas-server-core-webflow-api | org.apereo.cas:cas-server-core-webflow-api | 5.3.12 | http://www.apereo.org/cas
CAS server security filter | org.apereo.cas:cas-server-security-filter | 2.0.10.4 | http://www.apereo.org/cas
cas-server-support-jdbc | org.apereo.cas:cas-server-support-jdbc | 5.3.12 | http://www.apereo.org/cas
cas-server-support-jdbc-authentication | org.apereo.cas:cas-server-support-jdbc-authentication | 5.3.12 | http://www.apereo.org/cas
cas-server-support-jdbc-drivers | org.apereo.cas:cas-server-support-jdbc-drivers | 5.3.12 | http://www.apereo.org/cas
cas-server-support-json-service-registry | org.apereo.cas:cas-server-support-json-service-registry | 5.3.12 | http://www.apereo.org/cas
cas-server-support-rest | org.apereo.cas:cas-server-support-rest | 5.3.12 | http://www.apereo.org/cas
cas-server-support-saml | org.apereo.cas:cas-server-support-saml | 5.3.12 | http://www.apereo.org/cas
cas-server-support-saml-core | org.apereo.cas:cas-server-support-saml-core | 5.3.12 | http://www.apereo.org/cas
cas-server-support-saml-idp-core | org.apereo.cas:cas-server-support-saml-idp-core | 5.3.12 | http://www.apereo.org/cas
cas-server-support-shiro-authentication | org.apereo.cas:cas-server-support-shiro-authentication | 5.3.12 | http://www.apereo.org/cas
cas-server-support-throttle-core | org.apereo.cas:cas-server-support-throttle-core | 5.3.12 | http://www.apereo.org/cas
cas-server-support-validation | org.apereo.cas:cas-server-support-validation | 5.3.12 | http://www.apereo.org/cas
cas-server-webapp | org.apereo.cas:cas-server-webapp | 5.3.12 | http://www.apereo.org/cas
cglib | cglib:cglib-nodep | 2.1_3 | http://cglib.sourceforge.net/
Closure Compiler | com.google.javascript:closure-compiler | v20170626 | https://developers.google.com/closure/compiler/
Closure Compiler Externs | com.google.javascript:closure-compiler-externs | v20170626 | https://github.com/google/closure-compiler/closure-compiler-externs/
Cocoon Configuration API | org.apache.cocoon:cocoon-configuration-api | 1.0.2 | http://cocoon.apache.org/subprojects/configuration/1.0/configuration-api/1.0/
Cocoon Core | org.apache.cocoon:cocoon-core | 2.2.0 | http://cocoon.apache.org/2.2/core-modules/core/2.2/
Cocoon Expression Language API | org.apache.cocoon:cocoon-expression-language-api | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/expression-language-api/1.0/
Cocoon Expression Language Implementation. | org.apache.cocoon:cocoon-expression-language-impl | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/expression-language-impl/1.0/
Cocoon Flowscript Block Implementation | org.apache.cocoon:cocoon-flowscript-impl | 1.0.0 | http://cocoon.apache.org/2.2/blocks/flowscript/1.0/
Cocoon Linkrewriter Block Implementation | org.apache.cocoon:cocoon-linkrewriter-impl | 1.0.0 | http://cocoon.apache.org/2.2/blocks/linkrewriter/1.0/
Cocoon Pipeline API | org.apache.cocoon:cocoon-pipeline-api | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/pipeline-api/1.0/
Cocoon Pipeline Components | org.apache.cocoon:cocoon-pipeline-components | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/pipeline-components/1.0/
Cocoon Pipeline Implementation | org.apache.cocoon:cocoon-pipeline-impl | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/pipeline-impl/1.0/
Cocoon Servlet Service Components | org.apache.cocoon:cocoon-servlet-service-components | 1.0.0 | http://cocoon.apache.org/subprojects/servlet-service/1.0/servlet-service-components/1.0/
Cocoon Sitemap API | org.apache.cocoon:cocoon-sitemap-api | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/sitemap-api/1.0/
Cocoon Sitemap Components | org.apache.cocoon:cocoon-sitemap-components | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/sitemap-components/1.0/
Cocoon Sitemap Implementation | org.apache.cocoon:cocoon-sitemap-impl | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/sitemap-impl/1.0/
Cocoon Spring Configurator | org.apache.cocoon:cocoon-spring-configurator | 1.0.2 | http://cocoon.apache.org/cocoon-spring-configurator
Cocoon Store Implementation | org.apache.cocoon:cocoon-store-impl | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/store-impl/1.0/
Cocoon Template Framework Block Implementation | org.apache.cocoon:cocoon-template-impl | 1.1.0 | http://cocoon.apache.org/2.2/blocks/template/1.0/
Cocoon Thread API | org.apache.cocoon:cocoon-thread-api | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/thread-api/1.0/
Cocoon Thread Implementation | org.apache.cocoon:cocoon-thread-impl | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/thread-impl/1.0/
Cocoon Util | org.apache.cocoon:cocoon-util | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/util/1.0/
Cocoon XML API | org.apache.cocoon:cocoon-xml-api | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/xml-api/1.0/
Cocoon XML Implementation | org.apache.cocoon:cocoon-xml-impl | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/xml-impl/1.0/
Cocoon XML Resolver | org.apache.cocoon:cocoon-xml-resolver | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/xml-resolver/1.0/
Cocoon XML Utilities | org.apache.cocoon:cocoon-xml-util | 1.0.0 | http://cocoon.apache.org/2.2/core-modules/xml-util/1.0/
Code Generation Library | cglib:cglib | 3.1 | http://cglib.sourceforge.net/
Collections | commons-collections:commons-collections | 3.2 | http://jakarta.apache.org/commons/collections/
com.papertrail:profiler | com.papertrail:profiler | 1.0.2 | https://github.com/papertrail/profiler
commons-attributes-api | commons-attributes:commons-attributes-api | 2.1 | no url defined
commons-attributes-compiler | commons-attributes:commons-attributes-compiler | 2.1 | no url defined
Commons BeanUtils | commons-beanutils:commons-beanutils | 1.8.3 | http://commons.apache.org/beanutils/
Commons CLI | commons-cli:commons-cli | 1.2 | http://commons.apache.org/cli/
Commons Codec | commons-codec:commons-codec | 1.7 | http://commons.apache.org/codec/
Commons Collections | commons-collections:commons-collections | 3.2.1 | http://commons.apache.org/collections/
Commons Configuration | commons-configuration:commons-configuration | 1.6 | http://commons.apache.org/${pom.artifactId.substring(8)}/
Commons Configuration | commons-configuration:commons-configuration | 1.8 | http://commons.apache.org/configuration/
Commons DBCP | commons-dbcp:commons-dbcp | 1.4 | http://commons.apache.org/dbcp/
Commons FileUpload | commons-fileupload:commons-fileupload | 1.2.1 | http://commons.apache.org/fileupload/
Commons IO | commons-io:commons-io | 2.3 | http://commons.apache.org/io/
Commons IO | commons-io:commons-io | 2.4 | http://commons.apache.org/io/
commons-jexl | commons-jexl:commons-jexl | 1.0 | no url defined
Commons JEXL | commons-jexl:commons-jexl | 1.1 | http://jakarta.apache.org/commons/jexl/
Commons JXPath | commons-jxpath:commons-jxpath | 1.3 | http://commons.apache.org/jxpath/
Commons Lang | commons-lang:commons-lang | 2.6 | http://commons.apache.org/lang/
Commons Lang | org.apache.commons:commons-lang3 | 3.1 | http://commons.apache.org/lang/
Commons Logging | commons-logging:commons-logging | 1.1.1 | http://commons.apache.org/logging
Commons Pool | commons-pool:commons-pool | 1.4 | http://commons.apache.org/pool/
Commons Validator | commons-validator:commons-validator | 1.4.0 | http://commons.apache.org/validator/
concurrent-jena | com.hp.hpl.jena:concurrent-jena | 1.3.2 | no url defined
ConcurrentLinkedHashMap | com.googlecode.concurrentlinkedhashmap:concurrentlinkedhashmap-lru | 1.2 | http://code.google.com/p/concurrentlinkedhashmap
Data Mapper for Jackson | org.codehaus.jackson:jackson-mapper-asl | 1.9.13 | http://jackson.codehaus.org
Data Mapper for Jackson | org.codehaus.jackson:jackson-mapper-asl | 1.9.2 | http://jackson.codehaus.org
Digester | commons-digester:commons-digester | 1.8 | http://jakarta.apache.org/commons/digester/
Disruptor Framework | com.lmax:disruptor | 3.4.2 | http://lmax-exchange.github.com/disruptor
Ehcache Core | net.sf.ehcache:ehcache-core | 1.7.2 | http://ehcache.sf.net/ehcache-core
elasticsearch | org.elasticsearch:elasticsearch | 1.4.0 | http://nexus.sonatype.org/oss-repository-hosting.html/elasticsearch
error-prone annotations | com.google.errorprone:error_prone_annotations | 2.0.18 | http://nexus.sonatype.org/oss-repository-hosting.html/error_prone_parent/error_prone_annotations
error-prone annotations | com.google.errorprone:error_prone_annotations | 2.1.3 | http://nexus.sonatype.org/oss-repository-hosting.html/error_prone_parent/error_prone_annotations
error-prone annotations | com.google.errorprone:error_prone_annotations | 2.3.4 | http://nexus.sonatype.org/oss-repository-hosting.html/error_prone_parent/error_prone_annotations
Evo Inflector | org.atteo:evo-inflector | 1.0.1 | http://atteo.org/static/evo-inflector
Excalibur Instrument API | org.apache.excalibur.containerkit:excalibur-instrument-api | 2.2.1 | http://www.apache.org/excalibur/excalibur-containerkit/excalibur-instrument-modules/excalibur-instrument-api/
Excalibur Logger | org.apache.excalibur.containerkit:excalibur-logger | 2.2.1 | http://www.apache.org/excalibur/excalibur-containerkit/excalibur-logger/
Excalibur Pool API | org.apache.excalibur.components:excalibur-pool-api | 2.2.1 | http://www.apache.org/excalibur/excalibur-components-modules/excalibur-pool-modules/excalibur-pool-api/
Excalibur Sourceresolve | org.apache.excalibur.components:excalibur-sourceresolve | 2.2.3 | http://www.apache.org/excalibur/excalibur-sourceresolve/
Excalibur Store | org.apache.excalibur.components:excalibur-store | 2.2.1 | http://www.apache.org/excalibur/excalibur-components-modules/excalibur-store/
Excalibur XML Utilities | org.apache.excalibur.components:excalibur-xmlutil | 2.2.1 | http://www.apache.org/excalibur/excalibur-components-modules/excalibur-xmlutil/
FindBugs-jsr305 | com.google.code.findbugs:jsr305 | 1.3.9 | http://findbugs.sourceforge.net/
FindBugs-jsr305 | com.google.code.findbugs:jsr305 | 3.0.0 | http://findbugs.sourceforge.net/
FindBugs-jsr305 | com.google.code.findbugs:jsr305 | 3.0.2 | http://findbugs.sourceforge.net/
flyway-core | org.flywaydb:flyway-core | 3.0 | http://flywaydb.org/flyway-core
FORESITE :: Object Reuse and Exchange library | com.googlecode.foresite-toolkit:foresite | 0.9 | http://www.openarchives.org/ore
geronimo-spec-jta | geronimo-spec:geronimo-spec-jta | 1.0.1B-rc4 | no url defined
Google Analytics API v3-rev103-1.19.0 | com.google.apis:google-api-services-analytics | v3-rev103-1.19.0 | http://nexus.sonatype.org/oss-repository-hosting.html/google-api-services-analytics
Google APIs Client Library for Java | com.google.api-client:google-api-client | 1.19.0 | http://code.google.com/p/google-api-java-client/google-api-client/
Google HTTP Client Library for Java | com.google.http-client:google-http-client | 1.19.0 | http://code.google.com/p/google-http-java-client/google-http-client/
Google OAuth Client Library for Java | com.google.oauth-client:google-oauth-client | 1.19.0 | http://code.google.com/p/google-oauth-java-client/google-oauth-client/
Gson | com.google.code.gson:gson | 2.2.1 | http://code.google.com/p/google-gson/
Gson | com.google.code.gson:gson | 2.7 | https://github.com/google/gson/gson
Guava: Google Core Libraries for Java | com.google.guava:guava | 13.0 | http://code.google.com/p/guava-libraries/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 14.0.1 | http://code.google.com/p/guava-libraries/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 18.0 | http://code.google.com/p/guava-libraries/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 20.0 | https://github.com/google/guava/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 25.0-jre | https://github.com/google/guava/guava
Guava: Google Core Libraries for Java | com.google.guava:guava | 30.1-jre | https://github.com/google/guava/guava
Guava: Google Core Libraries for Java | com.google.guava:guava-jdk5 | 13.0 | http://code.google.com/p/guava-libraries/guava-jdk5
Guava InternalFutureFailureAccess and InternalFutures | com.google.guava:failureaccess | 1.0.1 | https://github.com/google/guava/failureaccess
Guava ListenableFuture only | com.google.guava:listenablefuture | 9999.0-empty-to-avoid-conflict-with-guava | https://github.com/google/guava/listenablefuture
Hibernate Validator Engine | org.hibernate:hibernate-validator | 5.4.1.Final | http://hibernate.org/validator/hibernate-validator
HikariCP | com.zaxxer:HikariCP | 3.2.0 | https://github.com/brettwooldridge/HikariCP
HPPC Collections | com.carrotsearch:hppc | 0.5.2 | http://labs.carrotsearch.com/hppc.html/hppc
HttpClient | commons-httpclient:commons-httpclient | 3.1 | http://jakarta.apache.org/httpcomponents/httpclient-3.x/
I18N Libraries | org.apache.abdera:abdera-i18n | 1.1.1 | http://abdera.apache.org
ID-WSF WSC | net.shibboleth.liberty:idwsfconsumer | 1.0.0 | http://shibboleth.net/idwsfconsumer/
Inspektr - Auditing API | org.apereo.inspektr:inspektr-audit | 1.8.4.GA | http://github.com/apereo/inspektr/inspektr-audit
Inspektr - Common API | org.apereo.inspektr:inspektr-common | 1.8.4.GA | http://github.com/apereo/inspektr/inspektr-common
Inspektr - Error Logging | org.apereo.inspektr:inspektr-error | 1.8.4.GA | http://github.com/apereo/inspektr/inspektr-error
Inspektr - Spring Framework Support | org.apereo.inspektr:inspektr-support-spring | 1.8.4.GA | http://github.com/apereo/inspektr/inspektr-support-spring
ISO Parser | com.googlecode.mp4parser:isoparser | 1.0-RC-1 | http://code.google.com/p/mp4parser/
J2ObjC Annotations | com.google.j2objc:j2objc-annotations | 1.1 | https://github.com/google/j2objc/
J2ObjC Annotations | com.google.j2objc:j2objc-annotations | 1.3 | https://github.com/google/j2objc/
Jackson | org.codehaus.jackson:jackson-core-asl | 1.9.13 | http://jackson.codehaus.org
Jackson | org.codehaus.jackson:jackson-core-asl | 1.9.2 | http://jackson.codehaus.org
Jackson 2 extensions to the Google HTTP Client Library for Java. | com.google.http-client:google-http-client-jackson2 | 1.19.0 | http://code.google.com/p/google-http-java-client/google-http-client-jackson2/
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.10.0 | http://github.com/FasterXML/jackson
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.9.9 | http://github.com/FasterXML/jackson
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.1.3 | http://wiki.fasterxml.com/JacksonHome
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.10.0 | https://github.com/FasterXML/jackson-core
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.9.3 | https://github.com/FasterXML/jackson-core
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.10.0 | http://github.com/FasterXML/jackson
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.9.9 | http://github.com/FasterXML/jackson
Jackson-dataformat-YAML | com.fasterxml.jackson.dataformat:jackson-dataformat-yaml | 2.9.9 | https://github.com/FasterXML/jackson-dataformats-text
Jackson datatype: Guava | com.fasterxml.jackson.datatype:jackson-datatype-guava | 2.9.9 | https://github.com/FasterXML/jackson-datatypes-collections
Jackson datatype: JSR310 | com.fasterxml.jackson.datatype:jackson-datatype-jsr310 | 2.9.9 | https://github.com/FasterXML/jackson-modules-java8/jackson-datatype-jsr310
Jackson Integration for Metrics | io.dropwizard.metrics:metrics-json | 3.2.5 | http://metrics.dropwizard.io/metrics-json/
Jackson-JAXRS-base | com.fasterxml.jackson.jaxrs:jackson-jaxrs-base | 2.9.9 | http://github.com/FasterXML/jackson-jaxrs-providers/jackson-jaxrs-base
Jackson-JAXRS-JSON | com.fasterxml.jackson.jaxrs:jackson-jaxrs-json-provider | 2.9.9 | http://github.com/FasterXML/jackson-jaxrs-providers/jackson-jaxrs-json-provider
Jackson module: JAXB Annotations | com.fasterxml.jackson.module:jackson-module-jaxb-annotations | 2.9.9 | https://github.com/FasterXML/jackson-modules-base
jakarta-regexp | jakarta-regexp:jakarta-regexp | 1.4 | no url defined
Jasig CAS Client for Java - Core | org.jasig.cas.client:cas-client-core | 3.5.1 | http://www.jasig.org/cas/cas-client-core
Jasig CAS Client for Java - Core | org.jasig.cas.client:cas-client-core | 3.6.1 | https://www.apereo.org/cas/cas-client-core
Jasig CAS Client for Java - SAML Protocol Support | org.jasig.cas.client:cas-client-support-saml | 3.5.0 | http://www.jasig.org/cas/cas-client-support-saml
Jasig CAS Client for Java - SAML Protocol Support | org.jasig.cas.client:cas-client-support-saml | 3.6.1 | https://www.apereo.org/cas/cas-client-support-saml
JASYPT: Java Simplified Encryption | org.jasypt:jasypt | 1.9.2 | http://www.jasypt.org
Java 6 (and higher) extensions to the Google OAuth Client Library for Java. | com.google.oauth-client:google-oauth-client-java6 | 1.19.0 | http://code.google.com/p/google-oauth-java-client/google-oauth-client-java6/
Java Annotation Indexer | org.jboss:jandex | 2.0.3.Final | http://www.jboss.org/jandex
JavaMail 1.4 | org.apache.geronimo.specs:geronimo-javamail_1.4_spec | 1.6 | http://geronimo.apache.org/maven/specs/geronimo-javamail_1.4_spec/1.6
java-support | net.shibboleth.utilities:java-support | 7.4.0 | http://shibboleth.net/java-support/
javax.inject | javax.inject:javax.inject | 1 | http://code.google.com/p/atinject/
jaxen | jaxen:jaxen | 1.1.1 | http://jaxen.codehaus.org/
JBoss Logging 3 | org.jboss.logging:jboss-logging | 3.3.1.Final | http://www.jboss.org
JCIP Annotations under Apache License | com.github.stephenc.jcip:jcip-annotations | 1.0-1 | http://stephenc.github.com/jcip-annotations
JCommander | com.beust:jcommander | 1.48 | http://beust.com/jcommander
jcommander | com.beust:jcommander | 1.78 | https://jcommander.org
jdbm | jdbm:jdbm | 1.0 | no url defined
jdom | jdom:jdom | 1.0 | no url defined
Jettison | org.codehaus.jettison:jettison | 1.1 | no url defined
Jetty extensions to the Google OAuth Client Library for Java. | com.google.oauth-client:google-oauth-client-jetty | 1.19.0 | http://code.google.com/p/google-oauth-java-client/google-oauth-client-jetty/
Jetty Server | org.mortbay.jetty:jetty | 6.1.14 | http://jetty.mortbay.org/project/modules/jetty
Jetty Servlet Tester | org.mortbay.jetty:jetty-servlet-tester | 6.1.14 | http://jetty.mortbay.org/project/jetty-servlet-tester
Jetty Utilities | org.mortbay.jetty:jetty-util | 6.1.14 | http://jetty.mortbay.org/project/jetty-util
JEval | net.sourceforge.jeval:jeval | 0.9.4 | http://jeval.sourceforge.net
JHeaps | org.jheaps:jheaps | 0.11 | http://www.jheaps.org
jMimeMagic | net.sf.jmimemagic:jmimemagic | 0.1.5 | http://github.com/arimus/jmimemagic/
Joda-Time | joda-time:joda-time | 2.10.4 | https://www.joda.org/joda-time/
Joda time | joda-time:joda-time | 2.2 | http://joda-time.sourceforge.net
Joda-Time | joda-time:joda-time | 2.3 | http://www.joda.org/joda-time/
Joda-Time | joda-time:joda-time | 2.5 | http://www.joda.org/joda-time/
Joda-Time | joda-time:joda-time | 2.9.9 | http://www.joda.org/joda-time/
jOOL | org.jooq:jool | 0.9.12 | http://nexus.sonatype.org/oss-repository-hosting.html/jool
jose4j | org.bitbucket.b_c:jose4j | 0.6.3 | https://bitbucket.org/b_c/jose4j/
jsinterop-annotations | com.google.jsinterop:jsinterop-annotations | 1.0.0 | http://www.gwtproject.org/jsinterop-annotations/
json-jena | com.hp.hpl.jena:json-jena | 1.0 | no url defined
JSON Small and Fast Parser | net.minidev:json-smart | 2.3 | http://www.minidev.net/
Jtwig Core | com.lyncode:jtwig-core | 2.0.1 | http://www.lyncode.com/jtwig-core
Jtwig Core Functions | com.lyncode:jtwig-functions | 2.0.1 | http://www.lyncode.com/jtwig-functions
Jtwig Spring | com.lyncode:jtwig-spring | 2.0.1 | http://www.lyncode.com/jtwig-spring
JUnitParams | pl.pragmatists:JUnitParams | 1.0.2 | http://junitparams.googlecode.com
JVM Integration for Metrics | io.dropwizard.metrics:metrics-jvm | 3.2.5 | http://metrics.dropwizard.io/metrics-jvm/
Lucene codecs | org.apache.lucene:lucene-codecs | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-codecs
Lucene Common Analyzers | org.apache.lucene:lucene-analyzers-common | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-common
Lucene Core | org.apache.lucene:lucene-core | 2.2.0 | http://lucene.apache.org/java/lucene-core
Lucene Core | org.apache.lucene:lucene-core | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-core
Lucene Expressions | org.apache.lucene:lucene-expressions | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-expressions
Lucene Grouping | org.apache.lucene:lucene-grouping | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-grouping
Lucene Highlighter | org.apache.lucene:lucene-highlighter | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-highlighter
Lucene ICU Analysis Components | org.apache.lucene:lucene-analyzers-icu | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-icu
Lucene Join | org.apache.lucene:lucene-join | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-join
Lucene Kuromoji Japanese Morphological Analyzer | org.apache.lucene:lucene-analyzers-kuromoji | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-kuromoji
Lucene Memory | org.apache.lucene:lucene-memory | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-memory
Lucene Miscellaneous | org.apache.lucene:lucene-misc | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-misc
Lucene Morfologik Polish Lemmatizer | org.apache.lucene:lucene-analyzers-morfologik | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-morfologik
Lucene Phonetic Filters | org.apache.lucene:lucene-analyzers-phonetic | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-phonetic
Lucene Queries | org.apache.lucene:lucene-queries | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-queries
Lucene QueryParsers | org.apache.lucene:lucene-queryparser | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-queryparser
Lucene Sandbox | org.apache.lucene:lucene-sandbox | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-sandbox
Lucene Smart Chinese Analyzer | org.apache.lucene:lucene-analyzers-smartcn | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-smartcn
Lucene Spatial | org.apache.lucene:lucene-spatial | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-spatial
Lucene Stempel Analyzer | org.apache.lucene:lucene-analyzers-stempel | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-analyzers-stempel
Lucene Suggest | org.apache.lucene:lucene-suggest | 4.10.2 | http://lucene.apache.org/lucene-parent/lucene-suggest
metadata-extractor | com.drewnoakes:metadata-extractor | 2.6.2 | http://code.google.com/p/metadata-extractor/
Metrics Core | io.dropwizard.metrics:metrics-core | 3.2.5 | http://metrics.dropwizard.io/metrics-core/
Metrics Health Checks | io.dropwizard.metrics:metrics-healthchecks | 3.2.5 | http://metrics.dropwizard.io/metrics-healthchecks/
Metrics Spring Integration | com.ryantenney.metrics:metrics-spring | 3.1.3 | http://github.com/ryantenney/metrics-spring/
Metrics Utility Servlets | io.dropwizard.metrics:metrics-servlets | 3.2.5 | http://metrics.dropwizard.io/metrics-servlets/
MongoDB Java Driver | org.mongodb:mongo-java-driver | 3.11.0 | http://www.mongodb.org
Nimbus JOSE+JWT | com.nimbusds:nimbus-jose-jwt | 6.0.2 | https://bitbucket.org/connect2id/nimbus-jose-jwt
Nimbus LangTag | com.nimbusds:lang-tag | 1.6 | https://bitbucket.org/connect2id/nimbus-language-tags
Noggit | org.noggit:noggit | 0.5 | http://noggit.org
Not Yet Commons SSL | ca.juliusdavies:not-yet-commons-ssl | 0.3.9 | http://juliusdavies.ca/commons-ssl
oai4j | se.kb:oai4j | 0.6b1 | http://oai4j-client.sourceforge.net/
OAuth 2.0 SDK with OpenID Connect extensions | com.nimbusds:oauth2-oidc-sdk | 6.5 | https://bitbucket.org/connect2id/oauth-2.0-sdk-with-openid-connect-extensions
Objenesis | org.objenesis:objenesis | 2.6 | http://objenesis.org
Ogg and Vorbis for Java, Core | org.gagravarr:vorbis-java-core | 0.1 | https://github.com/Gagravarr/VorbisJava
opencsv | net.sf.opencsv:opencsv | 2.0 | http://opencsv.sf.net
opencsv | net.sf.opencsv:opencsv | 2.3 | http://opencsv.sf.net
OpenSAML :: Core | org.opensaml:opensaml-core | 3.4.0 | http://shibboleth.net/opensaml-core/
OpenSAML :: Messaging API | org.opensaml:opensaml-messaging-api | 3.4.0 | http://shibboleth.net/opensaml-messaging-api/
OpenSAML :: Messaging Implementations | org.opensaml:opensaml-messaging-impl | 3.4.0 | http://shibboleth.net/opensaml-messaging-impl/
OpenSAML :: Profile API | org.opensaml:opensaml-profile-api | 3.4.0 | http://shibboleth.net/opensaml-profile-api/
OpenSAML :: Profile Implementations | org.opensaml:opensaml-profile-impl | 3.4.0 | http://shibboleth.net/opensaml-profile-impl/
OpenSAML :: SAML Provider API | org.opensaml:opensaml-saml-api | 3.4.0 | http://shibboleth.net/opensaml-saml-api/
OpenSAML :: SAML Provider Implementations | org.opensaml:opensaml-saml-impl | 3.4.0 | http://shibboleth.net/opensaml-saml-impl/
OpenSAML :: SAML XACML Profile API | org.opensaml:opensaml-xacml-saml-api | 3.4.0 | http://shibboleth.net/opensaml-xacml-saml-api/
OpenSAML :: SAML XACML Profile Implementation | org.opensaml:opensaml-xacml-saml-impl | 3.4.0 | http://shibboleth.net/opensaml-xacml-saml-impl/
OpenSAML :: Security API | org.opensaml:opensaml-security-api | 3.4.0 | http://shibboleth.net/opensaml-security-api/
OpenSAML :: Security Implementation | org.opensaml:opensaml-security-impl | 3.4.0 | http://shibboleth.net/opensaml-security-impl/
OpenSAML :: SOAP Provider API | org.opensaml:opensaml-soap-api | 3.4.0 | http://shibboleth.net/opensaml-soap-api/
OpenSAML :: SOAP Provider Implementations | org.opensaml:opensaml-soap-impl | 3.4.0 | http://shibboleth.net/opensaml-soap-impl/
OpenSAML :: Storage API | org.opensaml:opensaml-storage-api | 3.4.0 | http://shibboleth.net/opensaml-storage-api/
OpenSAML :: Storage Implementation | org.opensaml:opensaml-storage-impl | 3.4.0 | http://shibboleth.net/opensaml-storage-impl/
OpenSAML :: XACML Provider API | org.opensaml:opensaml-xacml-api | 3.4.0 | http://shibboleth.net/opensaml-xacml-api/
OpenSAML :: XML Security API | org.opensaml:opensaml-xmlsec-api | 3.4.0 | http://shibboleth.net/opensaml-xmlsec-api/
OpenSAML :: XML Security Implementation | org.opensaml:opensaml-xmlsec-impl | 3.4.0 | http://shibboleth.net/opensaml-xmlsec-impl/
OpenSAML-J | org.opensaml:opensaml | 2.6.1 | http://opensaml.org/
OpenWS | org.opensaml:openws | 1.5.1 | http://opensaml.org/
org.apache.tools.ant | org.apache.ant:ant | 1.7.0 | http://ant.apache.org/ant/
oro | oro:oro | 2.0.8 | no url defined
pac4j configuration | org.pac4j:pac4j-config | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-config
pac4j core | org.pac4j:pac4j-core | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-core
pac4j for CAS protocol | org.pac4j:pac4j-cas | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-cas
pac4j for HTTP protocol | org.pac4j:pac4j-http | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-http
pac4j for JWT | org.pac4j:pac4j-jwt | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-jwt
pac4j for MongoDB | org.pac4j:pac4j-mongo | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-mongo
pac4j for OAuth protocol | org.pac4j:pac4j-oauth | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-oauth
pac4j for OpenID Connect protocol | org.pac4j:pac4j-oidc | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-oidc
pac4j for SAML protocol | org.pac4j:pac4j-saml | 3.6.1 | https://github.com/pac4j/pac4j/pac4j-saml
pac4j implementation for Spring Web MVC | org.pac4j:spring-webmvc-pac4j | 3.0.0 | https://github.com/pac4j/spring-webmvc-pac4j
parboiled-core | org.parboiled:parboiled-core | 1.1.6 | http://parboiled.org
parboiled-java | org.parboiled:parboiled-java | 1.1.6 | http://parboiled.org
Person Directory API | org.apereo.service.persondir:person-directory-api | 1.8.9 | http://nexus.sonatype.org/oss-repository-hosting.html/person-directory-parent/person-directory-api
Person Directory Implementations | org.apereo.service.persondir:person-directory-impl | 1.8.9 | http://nexus.sonatype.org/oss-repository-hosting.html/person-directory-parent/person-directory-impl
quartz | org.quartz-scheduler:quartz | 2.3.0 | http://www.quartz-scheduler.org/quartz
ROME, RSS and atOM utilitiEs for Java | rome:rome | 0.9 | https://rome.dev.java.net/
ROME, RSS and atOM utilitiEs for Java | rome:rome | 1.0 | https://rome.dev.java.net/
Rome A9 OpenSearch | rome:opensearch | 0.1 | http://wiki.java.net/bin/view/Javawsxml/OpenSearch
rome-modules | org.rometools:rome-modules | 1.0 | http://www.rometools.org
Servlet Specification API | org.mortbay.jetty:servlet-api | 2.5-20081211 | http://jetty.mortbay.org/servlet-api
Shibboleth IdP :: Attribute API | net.shibboleth.idp:idp-attribute-api | 3.4.0 | http://shibboleth.net/idp-attribute-api/
Shibboleth IdP :: Attribute Filter API | net.shibboleth.idp:idp-attribute-filter-api | 3.4.0 | http://shibboleth.net/idp-attribute-filter-api/
Shibboleth IdP :: Attribute Resolver API | net.shibboleth.idp:idp-attribute-resolver-api | 3.4.0 | http://shibboleth.net/idp-attribute-resolver-api/
Shibboleth IdP :: Authentication API | net.shibboleth.idp:idp-authn-api | 3.4.0 | http://shibboleth.net/idp-authn-api/
Shibboleth IdP :: Configuration Schemas | net.shibboleth.idp:idp-schema | 3.4.0 | http://shibboleth.net/idp-schema/
Shibboleth IdP :: Core | net.shibboleth.idp:idp-core | 3.4.0 | http://shibboleth.net/idp-core/
Shibboleth IdP :: Profile API | net.shibboleth.idp:idp-profile-api | 3.4.0 | http://shibboleth.net/idp-profile-api/
Shibboleth IdP :: Profile Implementation | net.shibboleth.idp:idp-profile-impl | 3.4.0 | http://shibboleth.net/idp-profile-impl/
Shibboleth IdP :: Profile Spring Integration | net.shibboleth.idp:idp-profile-spring | 3.4.0 | http://shibboleth.net/idp-profile-spring/
Shibboleth IdP :: SAML Profile API | net.shibboleth.idp:idp-saml-api | 3.4.0 | http://shibboleth.net/idp-saml-api/
Shibboleth IdP :: SAML Profile Implementation | net.shibboleth.idp:idp-saml-impl | 3.4.0 | http://shibboleth.net/idp-saml-impl/
Shibboleth IdP :: Session API | net.shibboleth.idp:idp-session-api | 3.4.0 | http://shibboleth.net/idp-session-api/
SnakeYAML | org.yaml:snakeyaml | 1.17 | http://www.snakeyaml.org
Spatial4J | com.spatial4j:spatial4j | 0.4.1 | https://github.com/spatial4j/spatial4j
spring-aop | org.springframework:spring-aop | 3.1.1.RELEASE | no url defined
Spring AOP | org.springframework:spring-aop | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring AOP | org.springframework:spring-aop | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
spring-asm | org.springframework:spring-asm | 3.1.1.RELEASE | no url defined
spring-beans | org.springframework:spring-beans | 3.1.1.RELEASE | no url defined
Spring Beans | org.springframework:spring-beans | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Beans | org.springframework:spring-beans | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring Binding | org.springframework.webflow:spring-binding | 2.5.0.RELEASE | https://github.com/spring-projects/spring-webflow
Spring Boot | org.springframework.boot:spring-boot | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Actuator | org.springframework.boot:spring-boot-actuator | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Actuator Starter | org.springframework.boot:spring-boot-starter-actuator | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot AutoConfigure | org.springframework.boot:spring-boot-autoconfigure | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Configuration Metadata | org.springframework.boot:spring-boot-configuration-metadata | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Configuration Processor | org.springframework.boot:spring-boot-configuration-processor | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Developer Tools | org.springframework.boot:spring-boot-devtools | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Mail Starter | org.springframework.boot:spring-boot-starter-mail | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Starter | org.springframework.boot:spring-boot-starter | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Thymeleaf Starter | org.springframework.boot:spring-boot-starter-thymeleaf | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot WebSocket Starter | org.springframework.boot:spring-boot-starter-websocket | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Boot Web Starter | org.springframework.boot:spring-boot-starter-web | 1.5.18.RELEASE | http://projects.spring.io/spring-boot/
Spring Cloud Commons | org.springframework.cloud:spring-cloud-commons | 1.3.0.RELEASE | https://projects.spring.io/spring-cloud/spring-cloud-commons/
Spring Cloud Context | org.springframework.cloud:spring-cloud-context | 1.3.0.RELEASE | https://projects.spring.io/spring-cloud/spring-cloud-context/
spring-context | org.springframework:spring-context | 3.1.1.RELEASE | no url defined
Spring Context | org.springframework:spring-context | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Context | org.springframework:spring-context | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
spring-context-support | org.springframework:spring-context-support | 3.1.1.RELEASE | no url defined
Spring Context Support | org.springframework:spring-context-support | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
spring-core | org.springframework:spring-core | 3.1.1.RELEASE | no url defined
Spring Core | org.springframework:spring-core | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Core | org.springframework:spring-core | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring Data Core | org.springframework.data:spring-data-commons | 1.13.15.RELEASE | http://www.spring.io/spring-data/spring-data-commons
Spring Data MongoDB - Core | org.springframework.data:spring-data-mongodb | 1.10.15.RELEASE | http://projects.spring.io/spring-data-mongodb/spring-data-mongodb
spring-expression | org.springframework:spring-expression | 3.1.1.RELEASE | no url defined
Spring Expression Language (SpEL) | org.springframework:spring-expression | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Expression Language (SpEL) | org.springframework:spring-expression | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring Framework: Mock | org.springframework:spring-mock | 2.0.8 | http://www.springframework.org
Spring Framework Extension | net.shibboleth.ext:spring-extensions | 5.4.0 | http://shibboleth.net/spring-extensions/
spring-jdbc | org.springframework:spring-jdbc | 3.1.1.RELEASE | no url defined
Spring JDBC | org.springframework:spring-jdbc | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring JMS | org.springframework:spring-jms | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring Messaging | org.springframework:spring-messaging | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
spring-modules-cache | org.springmodules:spring-modules-cache | 0.8 | https://springmodules.dev.java.net/
Spring Object/Relational Mapping | org.springframework:spring-orm | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
spring-security-core | org.springframework.security:spring-security-core | 4.2.8.RELEASE | http://spring.io/spring-security
spring-security-crypto | org.springframework.security:spring-security-crypto | 4.2.3.RELEASE | http://spring.io/spring-security
Spring Shell | org.springframework.shell:spring-shell | 1.2.0.RELEASE | http://github.com/SpringSource/spring-shell
Spring TestContext Framework | org.springframework:spring-test | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Transaction | org.springframework:spring-tx | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
spring-tx | org.springframework:spring-tx | 3.1.1.RELEASE | no url defined
spring-web | org.springframework:spring-web | 3.1.1.RELEASE | no url defined
Spring Web | org.springframework:spring-web | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Web | org.springframework:spring-web | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring Web Flow | org.springframework.webflow:spring-webflow | 2.5.0.RELEASE | https://github.com/spring-projects/spring-webflow
Spring Webflow Client Repository | org.apereo:spring-webflow-client-repo | 1.0.3 | https://github.com/Apereo/spring-webflow-client-repo
spring-webmvc | org.springframework:spring-webmvc | 3.1.1.RELEASE | no url defined
Spring Web MVC | org.springframework:spring-webmvc | 3.2.5.RELEASE | https://github.com/SpringSource/spring-framework
Spring Web MVC | org.springframework:spring-webmvc | 4.3.25.RELEASE | https://github.com/spring-projects/spring-framework
Spring WebSocket | org.springframework:spring-websocket | 4.3.21.RELEASE | https://github.com/spring-projects/spring-framework
Spymemcached | net.spy:spymemcached | 2.12.3 | http://www.couchbase.org/code/couchbase/java
standard | taglibs:standard | 1.1.2 | no url defined
StAX API | stax:stax-api | 1.0.1 | http://stax.codehaus.org/
stax-api | stax:stax-api | 1.0 | no url defined
Streaming API for XML (STAX API 1.0) | org.apache.geronimo.specs:geronimo-stax-api_1.0_spec | 1.0.1 | http://geronimo.apache.org/specs/geronimo-stax-api_1.0_spec
Streaming API for XML (STAX API 1.0) | org.apache.geronimo.specs:geronimo-stax-api_1.0_spec | 1.0 | http://geronimo.apache.org/specs/geronimo-stax-api_1.0_spec
SWORD Java API, GUI and CLI | org.swordapp:sword-common | 1.1 | http://nexus.sonatype.org/oss-repository-hosting.html/sword-common
SWORD v2 :: Client Library | org.swordapp:sword2-client | 0.9.3 | http://www.swordapp.org/
SWORD v2 :: Common Server Library | org.swordapp:sword2-server | 1.0 | http://www.swordapp.org/
TagSoup | org.ccil.cowan.tagsoup:tagsoup | 1.2.1 | http://home.ccil.org/~cowan/XML/tagsoup/
Test Support | com.lyncode:test-support | 1.0.3 | http://nexus.sonatype.org/oss-repository-hosting.html/test-support
The ZK EL Library | org.zkoss.common:zel | 8.5.0 | http://www.zkoss.org/common
thymeleaf | org.thymeleaf:thymeleaf | 3.0.11.RELEASE | http://www.thymeleaf.org
thymeleaf-spring4 | org.thymeleaf:thymeleaf-spring4 | 3.0.11.RELEASE | http://www.thymeleaf.org
twitter4j-core | org.twitter4j:twitter4j-core | 4.0.6 | http://twitter4j.org/
unbescape | org.unbescape:unbescape | 1.1.6.RELEASE | http://www.unbescape.org
Woodstox | com.fasterxml.woodstox:woodstox-core | 5.0.3 | https://github.com/FasterXML/woodstox
Woodstox | org.codehaus.woodstox:wstx-asl | 3.2.0 | http://woodstox.codehaus.org
Woodstox | org.codehaus.woodstox:wstx-asl | 3.2.6 | http://woodstox.codehaus.org
Woodstox | org.codehaus.woodstox:wstx-asl | 3.2.7 | http://woodstox.codehaus.org
xalan | xalan:xalan | 2.7.0 | no url defined
Xalan Java | xalan:xalan | 2.7.1 | http://xml.apache.org/xalan-j/
Xalan Java | xalan:xalan | 2.7.2 | http://xml.apache.org/xalan-j/
Xalan Java Serializer | xalan:serializer | 2.7.1 | http://xml.apache.org/xalan-j/
Xalan Java Serializer | xalan:serializer | 2.7.2 | http://xml.apache.org/xalan-j/
Xerces2-j | xerces:xercesImpl | 2.10.0 | https://xerces.apache.org/xerces2-j/
Xerces2 Java Parser | xerces:xercesImpl | 2.8.1 | http://xerces.apache.org/xerces2-j/
xml-apis | xml-apis:xml-apis | 1.3.02 | http://xml.apache.org/commons/#external
XmlBeans | org.apache.xmlbeans:xmlbeans | 2.3.0 | http://xmlbeans.apache.org
XmlBeans | org.apache.xmlbeans:xmlbeans | 2.6.0 | http://xmlbeans.apache.org
XML Commons External Components XML APIs | xml-apis:xml-apis | 1.0.b2 | http://xml.apache.org/commons/#external
XML Commons External Components XML APIs Extensions | xml-apis:xml-apis-ext | 1.3.04 | http://xml.apache.org/commons/components/external/
XML Commons Resolver Component | xml-resolver:xml-resolver | 1.2 | http://xml.apache.org/commons/components/resolver/
xml-matchers | org.xmlmatchers:xml-matchers | 0.10 | http://code.google.com/p/xml-matchers/
xmlParserAPIs | xerces:xmlParserAPIs | 2.0.2 | no url defined
xmlParserAPIs | xerces:xmlParserAPIs | 2.6.2 | no url defined
xmlParserAPIs | xml-apis:xmlParserAPIs | 2.0.2 | no url defined
XMLSecTool | net.shibboleth.tool:xmlsectool | 2.0.0 | http://shibboleth.net/xmlsectool/
XML Security | org.apache.santuario:xmlsec | 1.4.3 | http://santuario.apache.org/
XMLTooling-J | org.opensaml:xmltooling | 1.4.1 | http://opensaml.org/
XOAI : OAI-PMH Java Toolkit | com.lyncode:xoai | 3.2.9 | http://www.lyncode.com
zookeeper | org.apache.zookeeper:zookeeper | 3.4.6 | no url defined
ZXing Core | com.google.zxing:core | 3.3.2 | https://github.com/zxing/zxing/core

###  Apache-2.0, BSD-4-Clause, EPL-1.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
databene ContiPerf | org.databene:contiperf | 2.2.0 | http://databene.org/contiperf

###  Apache-2.0, CDDL-1.0, EPL-1.0, LGPL-2.1-or-later, LGPL-3.0:

Name | id | version | url
---- | --- | --------- | ----
Restlet Core - API and Engine | org.restlet.jee:org.restlet | 2.1.1 | http://www.restlet.org/org.restlet
Restlet Extension - Servlet | org.restlet.jee:org.restlet.ext.servlet | 2.1.1 | http://www.restlet.org/org.restlet.ext.servlet

###  Apache-2.0, EPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
Jetty Server | org.mortbay.jetty:jetty | 6.1.26 | http://www.eclipse.org/jetty/jetty-parent/project/modules/jetty
Jetty Utilities | org.mortbay.jetty:jetty-util | 6.1.26 | http://www.eclipse.org/jetty/jetty-parent/project/jetty-util

###  Apache-2.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
Cryptacular Library | org.cryptacular:cryptacular | 1.2.2 | http://www.cryptacular.org
Jackson-annotations | com.fasterxml.jackson.core:jackson-annotations | 2.3.0 | http://wiki.fasterxml.com/JacksonHome
Jackson-core | com.fasterxml.jackson.core:jackson-core | 2.3.3 | http://wiki.fasterxml.com/JacksonHome
jackson-databind | com.fasterxml.jackson.core:jackson-databind | 2.3.3 | http://wiki.fasterxml.com/JacksonHome
javaparser-core | com.github.javaparser:javaparser-core | 3.6.5 | https://github.com/javaparser/javaparser-core
JAX-RS provider for JSON content type | org.codehaus.jackson:jackson-jaxrs | 1.9.2 | http://jackson.codehaus.org
Xml Compatibility extensions for Jackson | org.codehaus.jackson:jackson-xc | 1.9.2 | http://jackson.codehaus.org

###  Apache-2.0, LGPL-2.1-or-later, MPL-1.1:

Name | id | version | url
---- | --- | --------- | ----
Javassist | org.javassist:javassist | 3.16.1-GA | http://www.javassist.org/
Javassist | org.javassist:javassist | 3.18.2-GA | http://www.javassist.org/
Javassist | org.javassist:javassist | 3.22.0-GA | http://www.javassist.org/

###  Apache-2.0, SAX-PD, W3C:

Name | id | version | url
---- | --- | --------- | ----
XML Commons External Components XML APIs | xml-apis:xml-apis | 1.4.01 | http://xml.apache.org/commons/components/external/

###  Bouncy Castle:

Name | id | version | url
---- | --- | --------- | ----
Bouncy Castle CMS and S/MIME API | org.bouncycastle:bcmail-jdk15 | 1.45 | http://www.bouncycastle.org/java.html
Bouncy Castle PKIX, CMS, EAC, TSP, PKCS, OCSP, CMP, and CRMF APIs | org.bouncycastle:bcpkix-jdk15on | 1.60 | http://www.bouncycastle.org/java.html
Bouncy Castle PKIX, CMS, EAC, TSP, PKCS, OCSP, CMP, and CRMF APIs | org.bouncycastle:bcpkix-jdk15on | 1.63 | http://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15 | 1.45 | http://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15 | 1.46 | http://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15on | 1.60 | http://www.bouncycastle.org/java.html
Bouncy Castle Provider | org.bouncycastle:bcprov-jdk15on | 1.63 | http://www.bouncycastle.org/java.html
Bouncy Castle S/MIME API | org.bouncycastle:bcmail-jdk15on | 1.60 | http://www.bouncycastle.org/java.html

###  BSD-2-Clause:

Name | id | version | url
---- | --- | --------- | ----
PostgreSQL JDBC Driver - JDBC 4.2 | org.postgresql:postgresql | 42.2.12 | https://github.com/pgjdbc/pgjdbc

###  BSD-3-Clause:

Name | id | version | url
---- | --- | --------- | ----
Apache Solr Webapp | org.dspace:dspace-solr | 5.1 | https://github.com/dspace/DSpace/dspace-solr
Biblio Transformation Engine :: Core | gr.ekt.bte:bte-core | 0.9.3.5 | http://github.com/EKT/Biblio-Transformation-Engine/bte-core
Biblio Transformation Engine :: Input/Output | gr.ekt.bte:bte-io | 0.9.3.5 | http://github.com/EKT/Biblio-Transformation-Engine/bte-io
cdm-core | edu.ucar:cdm-core | 5.4.2 | no url defined
Commons Compiler | org.codehaus.janino:commons-compiler | 2.7.8 | http://docs.codehaus.org/display/JANINO/Home/commons-compiler
coverity-escapers | com.coverity.security:coverity-escapers | 1.1.1 | http://coverity.com/security
dom4j | org.dom4j:dom4j | 2.1.1 | http://dom4j.github.io/
DSpace JSP-UI | org.dspace:dspace-jspui | 5.1 | https://github.com/dspace/DSpace/dspace-jspui
DSpace Kernel :: Additions and Local Customizations | org.dspace.modules:additions | 5.1 | https://github.com/dspace/DSpace/modules/additions
DSpace Kernel :: API and Implementation | org.dspace:dspace-api | 5.1 | https://github.com/dspace/DSpace/dspace-api
DSpace OAI-PMH | org.dspace:dspace-oai | 5.1 | https://github.com/dspace/DSpace/dspace-oai
DSpace RDF | org.dspace:dspace-rdf | 5.1 | https://github.com/dspace/DSpace/dspace-rdf
DSpace REST :: API and Implementation | org.dspace:dspace-rest | 5.1 | http://demo.dspace.org
DSpace Services Framework :: API and Implementation | org.dspace:dspace-services | 5.1 | https://github.com/dspace/DSpace/dspace-services
DSpace SWORD | org.dspace:dspace-sword | 5.1 | https://github.com/dspace/DSpace/dspace-sword
DSpace SWORD v2 | org.dspace:dspace-swordv2 | 5.1 | https://github.com/dspace/DSpace/dspace-swordv2
DSpace XML-UI (Manakin) | org.dspace:dspace-xmlui | 5.1 | https://github.com/dspace/DSpace/dspace-xmlui
Hamcrest All | org.hamcrest:hamcrest-all | 1.3 | https://github.com/hamcrest/JavaHamcrest/hamcrest-all
Hamcrest Core | org.hamcrest:hamcrest-core | 1.3 | https://github.com/hamcrest/JavaHamcrest/hamcrest-core
httpservices | edu.ucar:httpservices | 5.4.2 | no url defined
Janino | org.codehaus.janino:janino | 2.7.8 | http://docs.codehaus.org/display/JANINO/Home/janino
JBibTeX | org.jbibtex:jbibtex | 1.0.10 | http://www.jbibtex.org
JSONLD Java :: Core | com.github.jsonld-java:jsonld-java | 0.5.0 | http://github.com/jsonld-java/jsonld-java/jsonld-java/
Protocol Buffer Java API | com.google.protobuf:protobuf-java | 2.5.0 | http://code.google.com/p/protobuf
Protocol Buffer Java API | com.google.protobuf:protobuf-java | 2.6.0 | http://code.google.com/p/protobuf
ThreeTen backport | org.threeten:threetenbp | 1.3.6 | https://www.threeten.org/threetenbp
udunits | edu.ucar:udunits | 5.4.2 | no url defined
User-Agent-Utils | eu.bitwalker:UserAgentUtils | 1.18 | http://www.bitwalker.eu/software/user-agent-utils

###  BSD-3-Clause, EPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
Java Persistence API, Version 2.1 | org.hibernate.javax.persistence:hibernate-jpa-2.1-api | 1.0.0.Final | http://hibernate.org

###  BSD-4-Clause:

Name | id | version | url
---- | --- | --------- | ----
ANTLR 3 Runtime | org.antlr:antlr-runtime | 3.5 | http://www.antlr.org
ANTLR 4 Runtime | org.antlr:antlr4-runtime | 4.7.2 | http://www.antlr.org/antlr4-runtime
AntLR Parser Generator | antlr:antlr | 2.7.7 | http://www.antlr.org/
ARQ | com.hp.hpl.jena:arq | 2.2 | http://jena.sourceforge.net/ARQ/
ARQ Extra | com.hp.hpl.jena:arq-extra | 2.2 | http://jena.sourceforge.net/ARQ/
ASM Analysis | org.ow2.asm:asm-analysis | 4.1 | http://asm.objectweb.org/asm-analysis/
ASM Commons | org.ow2.asm:asm-commons | 4.1 | http://asm.objectweb.org/asm-commons/
ASM Core | org.ow2.asm:asm | 4.1 | http://asm.objectweb.org/asm/
ASM Core | org.ow2.asm:asm | 4.2 | http://asm.objectweb.org/asm/
ASM Core | org.ow2.asm:asm | 5.0.4 | http://asm.objectweb.org/asm/
ASM Tree | org.ow2.asm:asm-tree | 4.1 | http://asm.objectweb.org/asm-tree/
ASM Util | org.ow2.asm:asm-util | 4.1 | http://asm.objectweb.org/asm-util/
Automaton | dk.brics.automaton:automaton | 1.11-8 | http://www.brics.dk/automaton/
dnsjava | dnsjava:dnsjava | 2.1.1 | http://www.dnsjava.org
dom4j | dom4j:dom4j | 1.6.1 | http://dom4j.org
DSpace I18N :: Language Packs | org.dspace:dspace-api-lang | 5.0.7 | http://nexus.sonatype.org/oss-repository-hosting.html/dspace-api-lang
DSpace XML-UI (Manakin) I18N :: Language Packs | org.dspace:dspace-xmlui-lang | 5.0.7 | http://nexus.sonatype.org/oss-repository-hosting.html/dspace-xmlui-lang
handle | org.dspace:handle | 6.2 | no url defined
IRI | com.hp.hpl.jena:iri | 0.5 | http://jena.sourceforge.net/
jargon | org.dspace:jargon | 1.4.25 | no url defined
jaxen | jaxen:jaxen | 1.1 | http://jaxen.codehaus.org/
Jena | com.hp.hpl.jena:jena | 2.5.5 | http://jena.sourceforge.net/
Jena | com.hp.hpl.jena:jena | 2.6.4 | http://www.openjena.org/
Jena IRI | com.hp.hpl.jena:iri | 0.8 | http://jena.sf.net/iri
Jena Tests | com.hp.hpl.jena:jenatest | 2.5.5 | http://jena.sourceforge.net/
JLine | jline:jline | 2.12 | http://nexus.sonatype.org/oss-repository-hosting.html/jline
mets | org.dspace:mets | 1.5.2 | no url defined
Morfologik FSA | org.carrot2:morfologik-fsa | 1.7.1 | http://morfologik.blogspot.com/morfologik-fsa/
Morfologik Stemming APIs | org.carrot2:morfologik-stemming | 1.7.1 | http://morfologik.blogspot.com/morfologik-stemming/
Morfologik Stemming Dictionary for Polish | org.carrot2:morfologik-polish | 1.7.1 | http://morfologik.blogspot.com/morfologik-polish/
oclc-harvester2 | org.dspace:oclc-harvester2 | 0.1.12 | no url defined
Repackaged Cocoon Servlet Service Implementation | org.dspace.dependencies.cocoon:dspace-cocoon-servlet-service-impl | 1.0.3 | http://projects.dspace.org/dspace-pom/dspace-cocoon-servlet-service-impl
Stax2 API | org.codehaus.woodstox:stax2-api | 3.1.4 | http://wiki.fasterxml.com/WoodstoxStax2
XMLUnit for Java | xmlunit:xmlunit | 1.1 | http://xmlunit.sourceforge.net/
XMLUnit for Java | xmlunit:xmlunit | 1.3 | http://xmlunit.sourceforge.net/
XMP Library for Java | com.adobe.xmp:xmpcore | 5.1.2 | http://www.adobe.com/devnet/xmp.html
yui compressor | com.yahoo.platform.yui:yuicompressor | 2.3.6 | http://developer.yahoo.com/yui/compressor/

###  BSD-4-Clause, CC-BY-SA-3.0:

Name | id | version | url
---- | --- | --------- | ----
ESAPI | org.owasp.esapi:esapi | 2.0.1 | http://www.esapi.org/

###  CDDL-1.0:

Name | id | version | url
---- | --- | --------- | ----
JavaBeans Activation Framework (JAF) | javax.activation:activation | 1.1 | http://java.sun.com/products/javabeans/jaf/index.jsp
jsp-api | javax.servlet:jsp-api | 2.0 | no url defined
jsr311-api | javax.ws.rs:jsr311-api | 1.1.1 | https://jsr311.dev.java.net
jstl | javax.servlet:jstl | 1.1.2 | no url defined
servlet-api | javax.servlet:servlet-api | 2.5 | no url defined
Servlet Specification 2.5 API | org.mortbay.jetty:servlet-api-2.5 | 6.1.14 | http://jetty.mortbay.org/project/modules/servlet-api-2.5

###  CDDL-1.0, GPL-1.0-only:

Name | id | version | url
---- | --- | --------- | ----
Streaming API for XML | javax.xml.stream:stax-api | 1.0-2 | no url defined

###  CDDL-1.0, GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
JavaMail API (compat) | javax.mail:mail | 1.4.7 | http://kenai.com/projects/javamail/mail
Java Transaction API | org.jboss.spec.javax.transaction:jboss-transaction-api_1.2_spec | 1.0.1.Final | http://www.jboss.org/jboss-transaction-api_1.2_spec

###  CDDL-1.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
JHighlight | com.uwyn:jhighlight | 1.0 | https://jhighlight.dev.java.net/

###  CDDL-1.0 + GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
Expression Language 3.0 API | javax.el:javax.el-api | 3.0.0 | http://uel-spec.java.net
Expression Language API (2.1 Maintenance Release) | javax.el:el-api | 2.2 | no url defined
Expression Language Implementation | org.glassfish.web:el-impl | 2.2 | no url defined
JavaMail API | com.sun.mail:javax.mail | 1.5.6 | http://javamail.java.net/javax.mail
Java Servlet API | javax.servlet:javax.servlet-api | 3.0.1 | http://servlet-spec.java.net
Java Servlet API | javax.servlet:javax.servlet-api | 3.1.0 | http://servlet-spec.java.net
javax.annotation API | javax.annotation:javax.annotation-api | 1.2 | http://jcp.org/en/jsr/detail?id=250

###  CDDL 1.1, GPL-2.0-with-classpath-exception:

Name | id | version | url
---- | --- | --------- | ----
JAXB API bundle for GlassFish V3 | javax.xml.bind:jaxb-api | 2.2.2 | https://jaxb.dev.java.net/
JAXB Reference Implementation | com.sun.xml.bind:jaxb-impl | 2.2.5 | http://jaxb.java.net/
JAXB RI | com.sun.xml.bind:jaxb-impl | 2.2.3-1 | http://jaxb.java.net/
jersey-core | com.sun.jersey:jersey-core | 1.19.4 | https://jersey.java.net/jersey-core/
jersey-json | com.sun.jersey:jersey-json | 1.19.4 | https://jersey.java.net/jersey-json/
jersey-server | com.sun.jersey:jersey-server | 1.19.4 | https://jersey.java.net/jersey-server/
jersey-servlet | com.sun.jersey:jersey-servlet | 1.19.4 | https://jersey.java.net/jersey-servlet/
jersey-spring | com.sun.jersey.contribs:jersey-spring | 1.8 | http://maven.apache.org

###  CDDL-1.1 + GPL-2.0:

Name | id | version | url
---- | --- | --------- | ----
JSR 353 (JSON Processing) API | javax.json:javax.json-api | 1.0 | http://json-processing-spec.java.net
JSR 353 (JSON Processing) Default Provider | org.glassfish:javax.json | 1.0.4 | http://jsonp.java.net

###  CPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
JUnit | junit:junit | 4.11 | http://junit.org

###  EPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
AspectJ runtime | org.aspectj:aspectjrt | 1.6.11 | http://www.aspectj.org
AspectJ runtime | org.aspectj:aspectjrt | 1.9.1 | http://www.aspectj.org
AspectJ weaver | org.aspectj:aspectjweaver | 1.9.1 | http://www.aspectj.org
org.apache.xml.resolver_1.2.0.v201005080400.jar | org.eclipse.birt.runtime.3_7_1:org.apache.xml.resolver | 1.2.0 | http://www.eclipse.org/projects/project.php?id=birt
org.apache.xml.serializer_2.7.1.v201005080400.jar | org.eclipse.birt.runtime.3_7_1:org.apache.xml.serializer | 2.7.1 | http://www.eclipse.org/projects/project.php?id=birt
oshi-core | com.github.oshi:oshi-core | 3.5.0 | https://github.com/oshi/oshi/oshi-core

###  EPL-1.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
mchange-commons-java | com.mchange:mchange-commons-java | 0.2.11 | https://github.com/swaldman/mchange-commons-java

###  EPL-2.0, LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
JGraphT - Core | org.jgrapht:jgrapht-core | 1.4.0 | http://www.jgrapht.org/jgrapht-core
JGraphT - I/O | org.jgrapht:jgrapht-io | 1.4.0 | http://www.jgrapht.org/jgrapht-io

###  Go License:

Name | id | version | url
---- | --- | --------- | ----
RE2/J | com.google.re2j:re2j | 1.3 | http://github.com/google/re2j

###  GPL-2.0:

Name | id | version | url
---- | --- | --------- | ----
jfiglet | com.github.lalyos:jfiglet | 0.0.8 | http://lalyos.github.io/jfiglet/

###  GPL-2.0-with-classpath-exception, MIT:

Name | id | version | url
---- | --- | --------- | ----
Checker Qual | org.checkerframework:checker-compat-qual | 2.0.0 | http://checkerframework.org

###  GPL-2.0-with-foss-exception:

Name | id | version | url
---- | --- | --------- | ----
MySQL Connector/J | mysql:mysql-connector-java | 8.0.11 | http://dev.mysql.com/doc/connector-j/en/

###  GPL-3.0-only, LGPL-3.0:

Name | id | version | url
---- | --- | --------- | ----
jnati - Artifact Deployer | net.sf.jnati:jnati-deploy | 0.4 | http://jnati.sourceforge.net/jnati-deploy/
jnati - Core | net.sf.jnati:jnati-core | 0.4 | http://jnati.sourceforge.net/jnati-core/
JNI-InChI | net.sf.jni-inchi:jni-inchi | 0.9-SNAPSHOT | http://jniinchi.sourceforge.net/

###  HSQLDB:

Name | id | version | url
---- | --- | --------- | ----
HyperSQL Database | org.hsqldb:hsqldb | 2.4.0 | http://hsqldb.org

###  ICU:

Name | id | version | url
---- | --- | --------- | ----
ICU4J | com.ibm.icu:icu4j | 51.1 | http://icu-project.org/
ICU4J | com.ibm.icu:icu4j | 53.1 | http://icu-project.org/

###  LGPL-2.1:

Name | id | version | url
---- | --- | --------- | ----
mariadb-java-client | org.mariadb.jdbc:mariadb-java-client | 2.2.4 | https://mariadb.com/kb/en/mariadb/about-mariadb-connector-j/

###  LGPL-2.1-or-later:

Name | id | version | url
---- | --- | --------- | ----
(deprecated - use hibernate-core instead) Hibernate JPA Support | org.hibernate:hibernate-entitymanager | 5.2.18.Final | http://hibernate.org
Core Hibernate O/RM functionality | org.hibernate:hibernate-core | 5.2.18.Final | http://hibernate.org
DSpace TM-Extractors Dependency | org.dspace.dependencies:dspace-tm-extractors | 1.0.1 | http://projects.dspace.org/dspace-pom/dspace-tm-extractors
FindBugs-Annotations | com.google.code.findbugs:annotations | 3.0.0 | http://findbugs.sourceforge.net/
Hibernate/HikariCP Integration | org.hibernate:hibernate-hikaricp | 5.2.18.Final | http://hibernate.org
Hibernate Commons Annotations | org.hibernate.common:hibernate-commons-annotations | 5.0.1.Final | http://hibernate.org
im4java | org.im4java:im4java | 1.4.0 | http://sourceforge.net/projects/im4java/
jTDS | net.sourceforge.jtds:jtds | 1.3.1 | http://jtds.sourceforge.net
MaxMind GeoIP API | com.maxmind.geoip:geoip-api | 1.2.11 | https://github.com/maxmind/geoip-api-java
mets | cat.iciq.mets:mets | 1.0 | no url defined
org.jdesktop - Swing Worker | org.jdesktop:swing-worker | 1.1 | no url defined
xom | xom:xom | 1.1 | http://www.xom.nu
XOM | xom:xom | 1.2.5 | http://xom.nu

###  LGPL-3.0:

Name | id | version | url
---- | --- | --------- | ----
The ZKoss Common Library | org.zkoss.common:zcommon | 8.5.0 | http://www.zkoss.org/common
The ZKoss Web Library | org.zkoss.common:zweb | 8.5.0 | http://www.zkoss.org/web
threads | com.github.axet:threads | 0.0.14 | https://github.com/axet/threads
wget | com.github.axet:wget | 1.4.9 | https://github.com/axet/wget
ZK Bind | org.zkoss.zk:zkbind | 8.5.0 | http://www.zkoss.org/zkbind
ZK Kernel | org.zkoss.zk:zk | 8.5.0 | http://www.zkoss.org/zk
ZK Plus Utilities | org.zkoss.zk:zkplus | 8.5.0 | http://www.zkoss.org/zkplus
ZK XHTML Components | org.zkoss.zk:zhtml | 8.5.0 | http://www.zkoss.org/zhtml
ZK XUL Components | org.zkoss.zk:zul | 8.5.0 | http://www.zkoss.org/zul

###  MIT:

Name | id | version | url
---- | --- | --------- | ----
Animal Sniffer Annotations | org.codehaus.mojo:animal-sniffer-annotations | 1.14 | http://mojo.codehaus.org/animal-sniffer/animal-sniffer-annotations
args4j | args4j:args4j | 2.33 | http://args4j.kohsuke.org/args4j/
Checker Qual | org.checkerframework:checker-qual | 3.5.0 | https://checkerframework.org
Hjson Library | org.hjson:hjson | 3.0.0 | https://github.com/hjson/hjson-java
java-statsd-client | com.timgroup:java-statsd-client | 3.1.0 | http://github.com/tim-group/java-statsd-client
JCL 1.1.1 implemented over SLF4J | org.slf4j:jcl-over-slf4j | 1.6.1 | http://www.slf4j.org
jsoup | org.jsoup:jsoup | 1.10.1 | https://jsoup.org/
jsoup | org.jsoup:jsoup | 1.6.1 | http://jsoup.org/
JUL to SLF4J bridge | org.slf4j:jul-to-slf4j | 1.6.1 | http://www.slf4j.org
JUL to SLF4J bridge | org.slf4j:jul-to-slf4j | 1.7.25 | http://www.slf4j.org
Main | org.jmockit:jmockit | 1.10 | http://www.jmockit.org
Microsoft JDBC Driver for SQL Server | com.microsoft.sqlserver:mssql-jdbc | 6.4.0.jre8 | https://github.com/Microsoft/mssql-jdbc
Mockito | org.mockito:mockito-all | 1.9.5 | http://www.mockito.org
Mockito | org.mockito:mockito-core | 1.9.5 | http://www.mockito.org
mockito-core | org.mockito:mockito-core | 2.13.0 | https://github.com/mockito/mockito
Objenesis | org.objenesis:objenesis | 1.0 | http://objenesis.googlecode.com/svn/docs/index.html
OpenCloud | org.mcavallo:opencloud | 0.3 | http://opencloud.mcavallo.org/
Project Lombok | org.projectlombok:lombok | 1.18.10 | https://projectlombok.org
ScribeJava APIs | com.github.scribejava:scribejava-apis | 5.6.0 | https://github.com/scribejava/scribejava/scribejava-apis
ScribeJava Core | com.github.scribejava:scribejava-core | 5.6.0 | https://github.com/scribejava/scribejava/scribejava-core
semver4j | com.vdurmont:semver4j | 2.2.0 | https://github.com/vdurmont/semver4j
SLF4J API Module | org.slf4j:slf4j-api | 1.6.1 | http://www.slf4j.org
SLF4J API Module | org.slf4j:slf4j-api | 1.7.25 | http://www.slf4j.org
SLF4J API Module | org.slf4j:slf4j-api | 1.7.28 | http://www.slf4j.org
SLF4J LOG4J-12 Binding | org.slf4j:slf4j-log4j12 | 1.6.1 | http://www.slf4j.org
zsoup | org.zkoss:zsoup | 1.8.2.5 | http://www.zkoss.org

###  MPL-1.1:

Name | id | version | url
---- | --- | --------- | ----
juniversalchardet | com.googlecode.juniversalchardet:juniversalchardet | 1.0.3 | http://juniversalchardet.googlecode.com/
Rhino | rhino:js | 1.6R7 | http://www.mozilla.org/rhino/

###  MPL-2.0:

Name | id | version | url
---- | --- | --------- | ----
Saxon-HE | net.sf.saxon:Saxon-HE | 9.9.1-8 | http://www.saxonica.com/

###  MPL-2.0 + EPL-1.0:

Name | id | version | url
---- | --- | --------- | ----
H2 Database Engine | com.h2database:h2 | 1.4.180 | http://www.h2database.com

###  No License:

Name | id | version | url
---- | --- | --------- | ----
kd | edu.wlu.cs.levy.CG:kd | 1.0.0 | no url defined

###  Public Domain:

Name | id | version | url
---- | --- | --------- | ----
antlr | antlr:antlr | 2.7.5 | no url defined
AOP alliance | aopalliance:aopalliance | 1.0 | http://aopalliance.sourceforge.net
Dough Lea's util.concurrent package | concurrent:concurrent | 1.3.4 | no url defined
Reflections | org.reflections:reflections | 0.9.9-RC1 | http://code.google.com/p/reflections/reflections/
XZ for Java | org.tukaani:xz | 1.4 | http://tukaani.org/xz/java.html

###  Public Domain, The New BSD License:

Name | id | version | url
---- | --- | --------- | ----
Reflections | org.reflections:reflections | 0.9.11 | http://github.com/ronmamo/reflections

###  Similar to Apache License but with the acknowledgment clause removed:

Name | id | version | url
---- | --- | --------- | ----
JDOM | org.jdom:jdom2 | 2.0.6 | http://www.jdom.org

###  The JSON License:

Name | id | version | url
---- | --- | --------- | ----
JSON in Java | org.json:json | 20160810 | https://github.com/douglascrockford/JSON-java

