
List of non-java related third-party dependencies grouped by their license type, review each type on https://spdx.org/licenses/.

The full text for each listed license can be found in ./license/third-party folder.

    Apache-2.0

        * hopscotch (0.2.5 : http://fenixrepo.fao.org/cdn/faostat/js/hopscotch/0.2.6/demo/)

    BSD-3-Clause

        * Ace code editor (1.4.10 - https://github.com/ajaxorg/ace)
        * D3.js (5.15.0 - https://d3js.org/)
        * d3-graphviz (3.0.0 - https://github.com/magjac/d3-graphviz)

    CC-BY-4.0	

        * Fontawesome free (5 - https://fontawesome.com/license/free)

    LGPL-2.0-only
    			
        * Jsmol (14.29.25 - https://sourceforge.net/projects/jsmol/)
        * JSpecView (2 - https://sourceforge.net/projects/jspecview/)
        * Jmol (14.29.25 - https://sourceforge.net/projects/jmol/)						
			
    MIT
    
        * Bootstrap (4.1.3 - https://github.com/twbs/bootstrap/blob/master/LICENSE)
        * Bootstrap-table (1.11.0 - https://github.com/wenzhixin/bootstrap-table)
        * Datatables (1.11.5 - https://datatables.net/license/mit)
        * Highslide JS (5.0.0 - http://highslide.com/#licence)
        * jQuery (3.3.1 - https://jquery.org/license/)
        * Jquery UI (1.12.1 - https://github.com/jquery/jquery-ui/blob/master/LICENSE.txt)
        * plotly.js (1.49.0 - https://github.com/plotly/plotly.js/blob/master/LICENSE)
        * popper.js (2.5.0 - https://github.com/popperjs/popper-core)
        * script.aculo.us (1.8.2 - http://script.aculo.us/)
        * Split.js (1.5.9 - https://github.com/nathancahill/split/blob/master/LICENSE)

    MIT, GPL-2.0						

        * jQuery blockUI plugin (2.70.0 - https://github.com/malsup/blockui/)
    
    MPL-1.1

        * FXSL - xslt Functional Programming Library (2 - https://sourceforge.net/projects/fxsl/)

    SIL Open Font License			

        * STIX Fonts (2.0.2 - https://github.com/stipub/stixfonts/blob/master/docs/STIX_2.0.2_license.pdf)
