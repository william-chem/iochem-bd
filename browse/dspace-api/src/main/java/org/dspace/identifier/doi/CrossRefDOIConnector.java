/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.identifier.doi;

import org.dspace.content.DSpaceObject;
import org.dspace.core.Context;

public class CrossRefDOIConnector implements DOIConnector {

	@Override
	public boolean isDOIReserved(Context context, String doi)
			throws DOIIdentifierException {	
		return false;
	}

	@Override
	public boolean isDOIReserved(Context context, DSpaceObject dso, String doi)
			throws DOIIdentifierException {
		return false;
	}

	@Override
	public boolean isDOIRegistered(Context context, String doi)
			throws DOIIdentifierException {
		return false;
	}

	@Override
	public boolean isDOIRegistered(Context context, DSpaceObject dso, String doi)
			throws DOIIdentifierException {
		return false;
	}

	@Override
	public void deleteDOI(Context context, String doi)
			throws DOIIdentifierException {


	}

	@Override
	public void reserveDOI(Context context, DSpaceObject dso, String doi)
			throws DOIIdentifierException {

	}

	@Override
	public void registerDOI(Context context, DSpaceObject dso, String doi)
			throws DOIIdentifierException {

	}

	@Override
	public void updateMetadata(Context context, DSpaceObject dso, String doi)
			throws DOIIdentifierException {

	}

}
