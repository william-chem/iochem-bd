/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.storage.rdbms.update;

import java.sql.Connection;
import org.dspace.core.Constants;
import org.dspace.storage.rdbms.DatabaseManager;
import org.dspace.storage.rdbms.DatabaseUtils;
import org.flywaydb.core.api.migration.MigrationChecksumProvider;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;

public class V5_0_2021_11_16__IO_0004 implements JdbcMigration, MigrationChecksumProvider {
	
    /* The checksum to report for this migration (when successful) */
    private int checksum = -1;

	@Override
	public Integer getChecksum() {		
		return checksum;
	}

	@Override
	public void migrate(Connection browseConn) throws Exception {				
		String packagePath = V5_0_2021_11_16__IO_0004.class.getPackage().getName().replace(".", "/");		
		
		//Update Create database
		Connection createConn  = CreateDatabaseHandler.getCreateConnection();
		String dbMigrateSQL = new ClassPathResource(packagePath + "/" +
														DatabaseManager.getDbKeyword() +
														"/IO_0004_create.sql", getClass().getClassLoader()).loadAsString(Constants.DEFAULT_ENCODING);
		DatabaseUtils.executeSql(createConn, dbMigrateSQL);
		CreateDatabaseHandler.closeConnection(createConn);		
		//Update Browse database
		dbMigrateSQL = new ClassPathResource(packagePath + "/" +
				DatabaseManager.getDbKeyword() +
				"/IO_0004_browse.sql", getClass().getClassLoader()).loadAsString(Constants.DEFAULT_ENCODING);
		DatabaseUtils.executeSql(browseConn, dbMigrateSQL);
		
				
	}
}
