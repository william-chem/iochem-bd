/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.storage.rdbms.update;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.NamingException;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.dspace.core.Constants;
import org.dspace.storage.rdbms.DatabaseManager;
import org.dspace.storage.rdbms.DatabaseUtils;
import org.flywaydb.core.api.migration.MigrationChecksumProvider;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import org.flywaydb.core.internal.util.scanner.classpath.ClassPathResource;

public class V5_0_2022_02_16__IO_0005 implements JdbcMigration, MigrationChecksumProvider {
	
    /* The checksum to report for this migration (when successful) */
    private int checksum = -1;

	@Override
	public Integer getChecksum() {		
		return checksum;
	}

	@Override
	public void migrate(Connection browseConn) throws Exception {	
	    updateCreateDatabase();
		updateCreateConfiguration();
	}
	
	private void updateCreateDatabase() throws SQLException, NamingException {
	    String packagePath = V5_0_2022_02_16__IO_0005.class.getPackage().getName().replace(".", "/");      
        
        //Update Create database
        Connection createConn  = CreateDatabaseHandler.getCreateConnection();
        String dbMigrateSQL = new ClassPathResource(packagePath + "/" +
                                                        DatabaseManager.getDbKeyword() +
                                                        "/IO_0005_create.sql", getClass().getClassLoader()).loadAsString(Constants.DEFAULT_ENCODING);
        DatabaseUtils.executeSql(createConn, dbMigrateSQL);
        CreateDatabaseHandler.closeConnection(createConn);      
	}

    private void updateCreateConfiguration() throws IOException, ConfigurationException {
        String basePath = System.getenv("IOCHEMBD_DIR");
        Path resourcesPath = Paths.get(basePath, "create", "resources.properties");
        PropertiesConfiguration conf = new PropertiesConfiguration(resourcesPath.toFile());
        conf.setProperty("upload.restriction.users.hard", "");
        conf.setProperty("upload.max.file.size.soft", "-1");
        conf.setProperty("upload.max.file.size.hard", "-1");
        conf.setProperty("upload.max.output.file.size.soft", "-1");
        conf.setProperty("upload.max.output.file.size.hard", "-1");
        conf.setProperty("upload.max.file.size.message", "");
        conf.save();
    }

}
