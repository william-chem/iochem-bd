/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.storage.rdbms.migration;

import java.sql.Connection;

import org.flywaydb.core.api.migration.MigrationChecksumProvider;
import org.flywaydb.core.api.migration.jdbc.JdbcMigration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



public class V5_0_2017_07_21__IO_0001 implements JdbcMigration, MigrationChecksumProvider {

    /** logging category */
    private static final Logger log = LoggerFactory.getLogger(V5_0_2017_07_21__IO_0001.class);

    
    /* The checksum to report for this migration (when successful) */
    private int checksum = -1;

	@Override
	public Integer getChecksum() {		
		return checksum;
	}

	@Override
	public void migrate(Connection connection) throws Exception {

	}
	
}
