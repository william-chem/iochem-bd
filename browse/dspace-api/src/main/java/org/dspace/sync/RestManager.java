/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.sync;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.security.SecureRandom;
import java.util.HashMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;

import org.apache.http.HeaderElement;
import org.apache.http.HeaderElementIterator;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.ConnectionKeepAliveStrategy;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLContexts;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.message.BasicHeaderElementIterator;
import org.apache.http.protocol.HTTP;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

public class RestManager {
	
    private static Logger log = Logger.getLogger(RestManager.class);
	private static CloseableHttpClient client; 
	
	// Code taken from http://prasans.info/2014/06/making-https-call-using-apache-httpclient/
    static {    	
    	try{
    		PoolingHttpClientConnectionManager connManager;
    		RequestConfig config;
    		ConnectionKeepAliveStrategy myStrategy;
    		
    		int readTimeout = 6;
    	    int connectionTimeout = 6;
    	    int connectionFetchTimeout =6;    	 
    	    int poolSize = 200;
    	    int routeSize = 50;

    	    //HTTP socket factory
    	    PlainConnectionSocketFactory plainsf = PlainConnectionSocketFactory.getSocketFactory();
    	    //HTTPS socket factory
	        SSLContext sslcontext = SSLContexts.custom().useSSL().build();
	        sslcontext.init(null, new X509TrustManager[]{new HttpsTrustManager()}, new SecureRandom());
	        SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslcontext, SSLConnectionSocketFactory.BROWSER_COMPATIBLE_HOSTNAME_VERIFIER);
	        
    	    Registry<ConnectionSocketFactory> r = RegistryBuilder.<ConnectionSocketFactory>create()
    	            .register("http", plainsf)
    	            .register("https", sslsf)
    	            .build();    	    
    	    connManager = new PoolingHttpClientConnectionManager(r);
    	    // Connection pool size and number of routes to cache
    	    connManager.setMaxTotal(poolSize);
    	    connManager.setDefaultMaxPerRoute(routeSize);    	      
    	    // ConnectTimeout : time to establish connection with GSA
    	    // ConnectionRequestTimeout : time to get connection from pool
    	    // SocketTimeout : waiting for packets form GSA
    	    config = RequestConfig.custom()
    	    		.setConnectTimeout(connectionTimeout * 1000)
    	    		.setConnectionRequestTimeout(connectionFetchTimeout * 1000)
    	    		.setSocketTimeout(readTimeout * 1000).build();

    		// Keep alive for 5 seconds if server does not have keep alive header
    		myStrategy = new ConnectionKeepAliveStrategy() {
    		@Override
    		public long getKeepAliveDuration(HttpResponse response, HttpContext context) {
    			HeaderElementIterator it = new BasicHeaderElementIterator(response.headerIterator(HTTP.CONN_KEEP_ALIVE));
    			while (it.hasNext()) {
    				HeaderElement he = it.nextElement();
    		        String param = he.getName();
    		        String value = he.getValue();
    		        if (value != null && param.equalsIgnoreCase("timeout")) {
    		        	return Long.parseLong(value) * 1000;
    		        }
    		    }
    		    return 5 * 1000;
    			}
    		  };
    		  
    		  client = HttpClients.custom().setDefaultRequestConfig(config).setKeepAliveStrategy(myStrategy).setConnectionManager(connManager).build();
    	}catch(Exception e){    		
    		log.error("Unable to setup ClosableHttpClient configuration. " + e.getMessage());
    	}    	
	}
	
	public static String getRequestAsText(String url, HashMap<String, String> headers) throws ParseException, Exception{
		HttpGet httpGet = new HttpGet(url);		
		httpGet.setHeader("Accept", "text/plain");
		if(headers != null)
			for(String key : headers.keySet())
				httpGet.setHeader(key, headers.get(key));		
		CloseableHttpResponse response = client.execute(httpGet);		
		try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :  if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);								
								return new String( baos.toByteArray(), StandardCharsets.UTF_8); 	
							}else
								return "";							
				default  : 	throw new Exception(EntityUtils.toString(entity));																				
			}
		} finally {
			EntityUtils.consume(response.getEntity());
		    response.close();
		}		
	} 
	
	public static String postRequest(String url, HttpEntity postEntity, HashMap<String, String> headers) throws ParseException, Exception{
		HttpPost httpPost = new HttpPost(url);
		if(headers != null)
			for(String key : headers.keySet())
				httpPost.setHeader(key, headers.get(key));
		httpPost.setEntity(postEntity);
		CloseableHttpResponse response = client.execute(httpPost);
	    try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :  if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);								
								return new String( baos.toByteArray(), StandardCharsets.UTF_8); 	
							}else
								return "";							
				default  : 	throw new Exception(EntityUtils.toString(entity));																				
			}
		} finally {
			EntityUtils.consume(response.getEntity());
		    response.close();
		}	  
	}
	
	public static String putRequest(String url, HttpEntity putEntity, HashMap<String, String> headers) throws ParseException, Exception{
		HttpPut httpPut = new HttpPut(url);
		if(headers != null)
			for(String key : headers.keySet())
				httpPut.setHeader(key, headers.get(key));		
		httpPut.setEntity(putEntity);
		CloseableHttpResponse response = client.execute(httpPut);
	    try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :  if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);								
								return new String( baos.toByteArray(), StandardCharsets.UTF_8); 	
							}else
								return "";							
				default  : 	throw new Exception(EntityUtils.toString(entity));																				
			}
		} finally {
			EntityUtils.consume(response.getEntity());
		    response.close();
		}
	}
	
	public static String deleteRequest(String url, HashMap<String, String> headers) throws ParseException, Exception{
		HttpDelete httpDelete = new HttpDelete(url);
		if(headers != null)
			for(String key : headers.keySet())
				httpDelete.setHeader(key, headers.get(key));	
		CloseableHttpResponse response = client.execute(httpDelete);		
	    try {
		    HttpEntity entity = response.getEntity();			
		    switch(response.getStatusLine().getStatusCode()){
				case 200 :  if (entity != null) {
								ByteArrayOutputStream baos = new ByteArrayOutputStream();
								entity.writeTo(baos);								
								return new String( baos.toByteArray(), StandardCharsets.UTF_8); 	
							}else
								return "";							
				default  : 	throw new Exception(EntityUtils.toString(entity));																				
			}
		} finally {
			EntityUtils.consume(response.getEntity());
		    response.close();
		}
	}	
	
	public static void close(){
		try {
			client.close();
		} catch (IOException e) {
			log.error("Can't close RestManager http client" + e.getMessage());
		}
	}
	
}
