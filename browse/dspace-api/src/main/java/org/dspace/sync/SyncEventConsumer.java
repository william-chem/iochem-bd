/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.sync;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.http.HttpEntity;
import org.apache.http.ParseException;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.log4j.Logger;
import org.dspace.app.util.TokenManager;
import org.dspace.content.Bitstream;
import org.dspace.content.Collection;
import org.dspace.content.DSpaceObject;
import org.dspace.content.Item;
import org.dspace.content.Metadatum;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.event.Consumer;
import org.dspace.event.Event;
import org.dspace.storage.bitstore.BitstreamStorageManager;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.gson.Gson;
import com.google.gson.JsonObject;


public class SyncEventConsumer  implements Consumer {
	
	private static final String FIND_URL = ConfigurationManager.getProperty("find.url");
	private static final String REST_OAI_ENDPOINT = "/rest/oai";	
	private static final String OAI_COLLECTION_ENDPOINT = "/collection";
	private static final String OAI_COLLECTION_EMBARGO_ENDPOINT = "/embargo";	
	private static final String OAI_COLLECTION_METADATA_ENDPOINT = "/metadata";	
	private static final String OAI_ITEM_ENDPOINT = "/item";
	private static final String OAI_COLLECTION_DOI = "/doi";
	private static final String OAI_BITSTREAM_ENDPOINT = "/bitstream";
	private static final String OAI_BITSTREAM_RETRIEVE_ENDPOINT = "/retrieve";
	
	private static final String DOI_PREFIX = "doi:";

    private static Logger log = Logger.getLogger(SyncEventConsumer.class);
  
	@Override
	public void initialize() throws Exception {		
		log.info("Synchronization event consumer started");
	}
	
	@Override
	public void consume(Context ctx, Event event) throws Exception {
		try{
			if(FIND_URL != null){				
				if(isAddCollection(event)){
					addCollection(event);
				}else if(isModifyCollection(event)){
					modifyCollection(event);
				}else if(isModifyCollectionMetadata(event)){
					modifyCollectionMetadata(event);
				}else if(isAddCollectionDoi(event)){
					addCollectionDoi(event);
				}else if(isUnembargoCollectionContent(event)){
					unembargoCollection(event);
				}else if(isDeleteCollection(event)){
					deleteCollection(event);		
				}else if(isModifyItem(event)){
					modifyItem(ctx, event);
				}else if(isDeleteItem(event)){
					deleteItem(event);
				}else if(isRetrieveBitstream(event)) {
				    retrieveBitstream(event);
				}
			}
		}catch(Exception e){
			log.error(e.getMessage());
		}
	}

    private boolean isAddCollection(Event event){
		return event.getEventType() == Event.ADD &&
		   event.getSubjectType() == Constants.SITE &&
		   event.getObjectType() == Constants.COLLECTION;			
	}

	private boolean isModifyCollection(Event event){
		return event.getEventType() == Event.MODIFY &&
		   event.getSubjectType() == Constants.SITE &&		   				
		   event.getObjectType() == Constants.COLLECTION;
	}

	private boolean isModifyCollectionMetadata(Event event){
		return event.getEventType() == Event.MODIFY_METADATA &&
		   event.getSubjectType() == Constants.SITE &&		   				
		   event.getObjectType() == Constants.COLLECTION &&
		   event.getDetail() != null && !event.getDetail().startsWith(DOI_PREFIX);
	}
	
	private boolean isAddCollectionDoi(Event event){
		return event.getEventType() == Event.MODIFY_METADATA &&
		   event.getSubjectType() == Constants.SITE &&		   				
		   event.getObjectType() == Constants.COLLECTION &&
		   event.getDetail() != null && event.getDetail().startsWith(DOI_PREFIX);
	}
	
	private boolean isUnembargoCollectionContent(Event event){
		return event.getEventType() == Event.ADD &&
		   event.getSubjectType() == Constants.SITE &&		   				
		   event.getObjectType() == Constants.ITEM;
	}
	
	private boolean isDeleteCollection(Event event){
		return event.getEventType() == Event.DELETE &&
		   event.getSubjectType() == Constants.COLLECTION;
	}

	private boolean isModifyItem(Event event){
		return (event.getEventType() == Event.MODIFY || event.getEventType() == Event.MODIFY_METADATA) &&
			event.getSubjectType() == Constants.SITE &&
		    event.getObjectType() == Constants.ITEM;
	}
	
	private boolean isDeleteItem(Event event){
		return (event.getEventType() == Event.REMOVE) &&
			event.getSubjectType() == Constants.SITE &&
		    event.getObjectType() == Constants.ITEM;
	}
	
	private boolean isRetrieveBitstream(Event event) {
	    return event.getEventType() == Event.RETRIEVE &&
            event.getSubjectType() == Constants.BITSTREAM;
    }

	private void addCollection(Event event) throws Exception{
		updateOAI();
		sendAddCollectionMessage(event);
	}
	
	private void modifyCollection(Event event) throws Exception{
		updateOAI();
		sendModifyCollectionMessage(event);
	}
	
	private void unembargoCollection(Event event) throws Exception{
		updateOAI();
		sendUnembargoCollectionMessage(event);
	}
	
	private void modifyCollectionMetadata(Event event) throws Exception{
		updateOAI();
		sendModifyCollectionMetadataMessage(event);
	}
	
	private void addCollectionDoi(Event event) throws Exception{
		sendAddCollectionDoiMessage(event);
	}
	
	private void deleteCollection(Event event) throws Exception{
		sendDeleteCollectionMessage(event);
	}
	
	private void modifyItem(Context ctx, Event event) throws ParseException, Exception{
		updateOAI();
		Item item = Item.find(ctx, event.getObjectID());
		for(Collection collection : item.getCollections()){			
				sendModifyItemMessage(collection);			
		}
	}
	
	private void deleteItem(Event event) throws ParseException, Exception {			
		sendDeleteItemMessage(event);
	}
	
	private void retrieveBitstream(Event event)  throws ParseException, Exception {            
	    sendRetrieveBitstreamMessage(event);
	}	
	
	private synchronized void updateOAI() throws FileNotFoundException, IOException{
		String folder = ConfigurationManager.getProperty("dspace.dir") + "/bin";
		String command = folder + "/dspace";
		
        ArrayList<String> commandList = new ArrayList<String>();
    	String commands[];
		commandList.add(command);
		commandList.add("oai");
		commandList.add("import");
		commandList.add("-o");  
		commands = new String[commandList.size()];
		commandList.toArray(commands);
		try
		{
		    Process child = Runtime.getRuntime().exec(commands, null, new File(folder));
		    InputStream is = child.getInputStream();
		    BufferedReader br = new BufferedReader(new InputStreamReader(is));
		    String aux = br.readLine();
		    log.info("OAI import collection request raised");              
			while (aux != null) {			                  
			    aux = br.readLine();
			    log.info(aux);
			}
		}
		catch (Exception e)
		{
			log.error("Error importing collection into OAI registries." + e.getMessage());			
		}
	}	
	
	private void sendAddCollectionMessage(Event event) throws ParseException, Exception{
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT;
		HttpEntity postEntity = buildCollectionAddEntity(event, "POST");					
		RestManager.postRequest(url, postEntity, null);		
	}
	
	private HttpEntity buildCollectionAddEntity(Event event, String command){	
		String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
		String salt = ConfigurationManager.getProperty("external.communication.secret");
		
		int collectionId = event.getObjectID();		
		String secret = command + "#" + baseUrl + "#COLLECTION##" + collectionId; 
		String hash = TokenManager.encode(salt, secret);
		
		StringBuilder jsonText = new StringBuilder();
		jsonText.append("{");
		jsonText.append(" \"institutionUrl\":\"" + baseUrl + "\",");
		jsonText.append(" \"objectType\":\"COLLECTION\", ");
		jsonText.append(" \"handle\":\"\","); 
		jsonText.append(" \"id\":" + String.valueOf(collectionId) + ",");
		jsonText.append(" \"hash\":\"" + hash  + "\"");
		jsonText.append("}");
		
	    return new StringEntity(jsonText.toString(), ContentType.create("application/json"));		
	}
	
	private void sendModifyCollectionMessage(Event event) throws ParseException, Exception{
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT;
		HttpEntity putEntity = buildCollectionModifyEntity(event, "PUT");					
		RestManager.putRequest(url, putEntity, null);
	}
	
	private void sendModifyCollectionMetadataMessage(Event event) throws ParseException, Exception{
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT + OAI_COLLECTION_METADATA_ENDPOINT;
		HttpEntity putEntity = buildCollectionModifyEntity(event, "PUT");					
		RestManager.putRequest(url, putEntity, null);
	}
	
	private HttpEntity buildCollectionModifyEntity(Event event, String command) throws SQLException{	
		String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
		String salt = ConfigurationManager.getProperty("external.communication.secret");
		
		int collectionId = event.getObjectID();
		Context context = new Context();
		Collection collection = Collection.find(context, collectionId);
		
		String secret = command + "#" + baseUrl + "#COLLECTION#" + collection.getHandle() +"#" + collectionId; 
		String hash = TokenManager.encode(salt, secret);
		String metadataFields = buildCollectionMetadataFields(collection, event.getDetail());
		StringBuilder jsonText = new StringBuilder();
		jsonText.append("{");
		jsonText.append(" \"institutionUrl\":\"" + baseUrl + "\",");
		jsonText.append(" \"objectType\":\"COLLECTION\", ");
		jsonText.append(" \"handle\":\"" + collection.getHandle() + "\","); 
		jsonText.append(" \"id\":" + collection.getID() + ",");
		jsonText.append(" \"hash\":\"" + hash  + "\",");
		jsonText.append(" \"metadata\":" + metadataFields);
		jsonText.append("}");		
	    return new StringEntity(jsonText.toString(), ContentType.create("application/json"));		
	}
	
	private void sendAddCollectionDoiMessage(Event event) throws ParseException, Exception{
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT + OAI_COLLECTION_DOI;
		HttpEntity putEntity = buildAddCollectionDoiEntity(event, event.getDetail(), "PUT");					
		RestManager.putRequest(url, putEntity, null);				
	}
	
	private HttpEntity buildAddCollectionDoiEntity(Event event, String doi, String command) throws SQLException{	
		String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
		String salt = ConfigurationManager.getProperty("external.communication.secret");
		
		int collectionId = event.getObjectID();
		Context context = new Context();
		Collection collection = Collection.find(context, collectionId);
		
		String secret = command + "#" + baseUrl + "#COLLECTION#" + collection.getHandle() +"#" + collectionId; 
		String hash = TokenManager.encode(salt, secret);		
		StringBuilder jsonText = new StringBuilder();
		jsonText.append("{");
		jsonText.append(" \"institutionUrl\":\"" + baseUrl + "\",");
		jsonText.append(" \"objectType\":\"COLLECTION\", ");
		jsonText.append(" \"handle\":\"" + collection.getHandle() + "\","); 
		jsonText.append(" \"id\":" + collection.getID() + ",");
		jsonText.append(" \"hash\":\"" + hash  + "\",");
		jsonText.append(" \"metadata\": [ {\"element\":\"dc.relation.ispartof\", \"value\":\"" + doi.replace(DOI_PREFIX, "") + "\" }]" );
		jsonText.append("}");		
	    return new StringEntity(jsonText.toString(), ContentType.create("application/json"));		
	}

	private String buildCollectionMetadataFields(Collection collection, String relationField) throws SQLException{
		List<JsonDataPair> elements = new ArrayList<>();
		//Add collection metadata
		List<Metadatum> fields = collection.getMetadata("dc", Item.ANY, Item.ANY, Item.ANY, Item.ANY);			
		for(Metadatum field : fields)
			elements.add(new JsonDataPair(field.getField(), field.value));					
		
		//Add item provenance metadata 
		Item firstChild = collection.getItems().next();
		if(firstChild != null){
			List<Metadatum> childMetadata = firstChild.getMetadata("dc", "relation", Item.ANY, Item.ANY, Item.ANY);
			for(Metadatum field: childMetadata)					
				elements.add(new JsonDataPair(field.getField(), field.value));
		}
		//Add paper related metadata
		if(relationField != null && !relationField.isEmpty())
			elements.add(new JsonDataPair("dc.relation", relationField));		
		return new Gson().toJson(elements);
		//	return new ObjectMapper().writeValueAsString(elements);
	
	}
	
	private void sendUnembargoCollectionMessage(Event event) throws ParseException, Exception{
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT + OAI_COLLECTION_EMBARGO_ENDPOINT;
		HttpEntity putEntity = buildCollectionModifyEntity(event, "PUT");					
		RestManager.putRequest(url, putEntity, null);
	}
	
	private void sendDeleteCollectionMessage(Event event) throws ParseException, Exception{
		String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
		String salt = ConfigurationManager.getProperty("external.communication.secret");
		String handle = event.getDetail();
		String secret = "DELETE#" + baseUrl + "#COLLECTION#" + handle + "#";		
		String hash = TokenManager.encode(salt, secret);		
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT + "/" + handle.replace("/", "_") + "?institutionurl=" + StringEscapeUtils.escapeHtml(baseUrl)  + "&hash=" + hash;						
		RestManager.deleteRequest(url, null);
	}
	
	private void sendModifyItemMessage(Collection collection) throws ParseException, Exception{
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT + OAI_COLLECTION_EMBARGO_ENDPOINT;	
		HttpEntity putEntity = buildItemModifyEntity(collection, "PUT");					
		RestManager.putRequest(url, putEntity, null);			
	}
	
	private HttpEntity buildItemModifyEntity(Collection collection, String command) throws SQLException{	
		String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
		String salt = ConfigurationManager.getProperty("external.communication.secret");
		String secret = command + "#" + baseUrl + "#COLLECTION#" + collection.getHandle() +"#" + collection.getID(); 
		String hash = TokenManager.encode(salt, secret);
		
		StringBuilder jsonText = new StringBuilder();
		jsonText.append("{");
		jsonText.append(" \"institutionUrl\":\"" + baseUrl + "\",");
		jsonText.append(" \"objectType\":\"COLLECTION\", ");
		jsonText.append(" \"handle\":\"" + collection.getHandle() + "\","); 
		jsonText.append(" \"id\":" + collection.getID() + ",");
		jsonText.append(" \"hash\":\"" + hash  + "\"");
		jsonText.append("}");		
	    return new StringEntity(jsonText.toString(), ContentType.create("application/json"));		
	}

	private void sendDeleteItemMessage(Event event) throws ParseException, Exception{
		String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
		String host = ConfigurationManager.getProperty("dspace.hostname");
		String salt = ConfigurationManager.getProperty("external.communication.secret");		
		String collectionHandle = event.getIdentifiers()[0];
		String itemHandle = event.getIdentifiers()[1];
		
		String secret = "DELETE#" + baseUrl + "#ITEM#" + itemHandle + "#";
		String hash = TokenManager.encode(salt, secret);
		String url = FIND_URL + REST_OAI_ENDPOINT + OAI_COLLECTION_ENDPOINT + "/" + collectionHandle.replace("/", "_") + OAI_ITEM_ENDPOINT + "/" + itemHandle.replace("/", "_");		
		String parameters = "?hash=" + hash + "&host=" + host + "&institutionurl=" + baseUrl;
		RestManager.deleteRequest(url + parameters, null);
	}
	
	private void sendRetrieveBitstreamMessage(Event event) throws ParseException, Exception{	    
	    String url = FIND_URL + REST_OAI_ENDPOINT + OAI_BITSTREAM_ENDPOINT + OAI_BITSTREAM_RETRIEVE_ENDPOINT;

	    int bitstreamId = event.getSubjectID();
        Context context = new Context();
        Bitstream bitstream = Bitstream.find(context, bitstreamId);
	    String email = event.getDetail();
	    String bitstreamUrl = event.getIdentifiers()[1];

	    HttpEntity putEntity = buildBitstreamRetrieveEntity(context, bitstream, email, bitstreamUrl, "PUT");
        RestManager.putRequest(url, putEntity, null);
	}
	
	private HttpEntity buildBitstreamRetrieveEntity(Context context, Bitstream bitstream, String email, String url, String command) throws SQLException, IOException{   
        String baseUrl = ConfigurationManager.getProperty("dspace.baseUrl");
        String salt = ConfigurationManager.getProperty("external.communication.secret");
      
        DSpaceObject parent = bitstream.getParentObject();
        Item item = Item.find(context, parent.getID());
        String path  = BitstreamStorageManager.getRealPath(context, bitstream.getID());                      

        String secret = command + "#" + baseUrl + "#BITSTREAM#" + item.getHandle() + "#" + bitstream.getID();        
        String hash = TokenManager.encode(salt, secret);            

        List<JsonDataPair> metadata = new ArrayList<>();
        metadata.add(new JsonDataPair("file", path));
        metadata.add(new JsonDataPair("name", bitstream.getName()));
        metadata.add(new JsonDataPair("url", url));
        metadata.add(new JsonDataPair("email", email));

        Map<String, Object> params = new HashMap<>();
        params.put("institutionUrl", baseUrl);
        params.put("objectType", "BITSTREAM");
        params.put("handle", item.getHandle());
        params.put("id", bitstream.getID());
        params.put("hash", hash);
        params.put("metadata", metadata);
               
        return new StringEntity(new Gson().toJson(params, HashMap.class), ContentType.create("application/json"));       
    }
	
	
	
	@Override
	public void end(Context ctx) throws Exception {
		log.info("Synchronization event consumer ended");
	}

	@Override
	public void finish(Context ctx) throws Exception {
		RestManager.close();		
	}
	
	class JsonDataPair {
		String element = null;
		String value = null;
		
		public JsonDataPair(String element, String value) {
			this.element = element; 
			this.value = value;
		}
		
		public String getElement() {
			return element;
		}

		public void setElement(String element) {
			this.element = element;
		}

		public String getValue() {
			return value;
		}

		public void setValue(String value) {
			this.value = value;
		}
	}
	
}
