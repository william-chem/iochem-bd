/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.embargo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.authorize.ResourcePolicy;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.DCDate;
import org.dspace.content.Item;
import org.dspace.core.Context;
import org.dspace.eperson.Group;

public class CustomEmbargoLifter implements EmbargoLifter {
	
    private static Logger log = Logger.getLogger(CustomEmbargoLifter.class);
	
	@Override
	public void liftEmbargo(Context context, Item item) throws SQLException, AuthorizeException, IOException {                
		// Policies List
        List<ResourcePolicy> rpolicies = new ArrayList<ResourcePolicy>();
        try
        {
        	//Remove item policies
            rpolicies = AuthorizeManager.findPoliciesByDSOAndType(context, item, ResourcePolicy.TYPE_CUSTOM);
            for(ResourcePolicy rpolicy : rpolicies){
            	rpolicy.delete();
            }
            item.inheritCollectionDefaultPolicies(item.getOwningCollection());   
            item.clearMetadata("dc", "embargo", "terms", Item.ANY);        	
            item.update();
            
    		liftEmbargoOnBitstreams(context, item);    	
            context.commit();            
        }
        catch (SQLException e)
        {
            log.error(e.getMessage(), e);
        }
	}
	
	/**
	 * Lift embargo by removing existing access restrictions 
	 * @param context
	 * @param item 
	 * @throws SQLException
	 * @throws AuthorizeException
	 */
	public void liftEmbargoOnBitstreams(Context context, Item item) throws SQLException, AuthorizeException{    	
		for(Bundle bundle: item.getBundles()){    		
    		for(Bitstream bitstream : bundle.getBitstreams()){
    			List<ResourcePolicy> rpolicies = AuthorizeManager.findPoliciesByDSOAndType(context, bitstream, ResourcePolicy.TYPE_CUSTOM);
                for(ResourcePolicy rpolicy : rpolicies){
                	rpolicy.delete();
                	ResourcePolicy readPolicy = AuthorizeManager.createOrModifyPolicy(null, context, null, Group.ANONYMOUS_ID, null, null, org.dspace.core.Constants.READ, null, bitstream);
                	readPolicy.update();
                }
                	 
                bitstream.update();
    		}
        }
	}
    
    

}
