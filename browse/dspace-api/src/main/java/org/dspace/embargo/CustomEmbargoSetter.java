/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.embargo;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.regex.Pattern;

import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.authorize.ResourcePolicy;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Collection;
import org.dspace.content.DCDate;
import org.dspace.content.DSpaceObject;
import org.dspace.content.Item;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.eperson.Group;
import org.dspace.license.CreativeCommons;

public class CustomEmbargoSetter implements EmbargoSetter {

    protected String termsOpen = null;
    
    public static final String EXCLUDED_BUNDLES_FROM_EMBARGO_REGEX = "(" + Constants.LICENSE_BUNDLE_NAME  + "|" + Constants.METADATA_BUNDLE_NAME + "|" + CreativeCommons.CC_BUNDLE_NAME + "|TEXT)";        
    
    public CustomEmbargoSetter()
    {
        super();
        termsOpen = ConfigurationManager.getProperty("embargo.terms.open");
    }
    
    /**
     * Parse the terms into a definite date. Terms are expected to consist of
     * either: a token (value configured in 'embargo.terms.open' property) to indicate
     * indefinite embargo, or a literal lift date formatted in ISO 8601 format (yyyy-mm-dd)
     * 
     * @param context the DSpace context
     * @param item the item to embargo
     * @param terms the embargo terms
     * @return parsed date in DCDate format
     */
    public DCDate parseTerms(Context context, Item item, String terms)
        throws SQLException, AuthorizeException, IOException
    {
    	if (terms != null && terms.length() > 0)
    	{
    		if (termsOpen.equals(terms))
            {
                return EmbargoManager.FOREVER;
            }
            else
            {
                return new DCDate(terms);
            }
    	}
        return null;
    }

    /**
     * Enforce embargo by turning off all read access to this Item.
     *
     * @param context the DSpace context
     * @param item the item to embargo
     */
    public void setEmbargo(Context context, Item item)
        throws SQLException, AuthorizeException, IOException
    {
        DCDate liftDate = EmbargoManager.getEmbargoTermsAsDate(context, item);
        ResourcePolicy rp = null;
		rp=AuthorizeManager.createOrModifyPolicy(null, context, "Embargoed element", Group.ANONYMOUS_ID, null, liftDate.toDate(), org.dspace.core.Constants.READ, "Embargoed element under review by external parties", item);		
		rp.update();		
		setEmbargoOnBitstreams(context, item, liftDate);
		context.commit();
    }


    /**
     * Setup embargo by turning off all read access to this item bitstreams 
     * @param context
     * @param item 
     * @param liftDate
     * @throws SQLException
     * @throws AuthorizeException
     */
    public void setEmbargoOnBitstreams(Context context, Item item, DCDate liftDate) throws SQLException, AuthorizeException{    	
    	for(Bundle bundle: item.getBundles()){    		
            if (!bundle.getName().matches(EXCLUDED_BUNDLES_FROM_EMBARGO_REGEX)){    	
	    		for(Bitstream bitstream : bundle.getBitstreams()){
	    			ResourcePolicy rp = null;
	    			rp=AuthorizeManager.createOrModifyPolicy(null, context, "Embargoed element", Group.ANONYMOUS_ID, null, liftDate.toDate(), org.dspace.core.Constants.READ, "Embargoed element under review by external parties", bitstream);
	    			rp.update();
	    		}
            }
    	}
    }    
    
    
    /**
     * Check that embargo is properly set on Item: no read access to bitstreams.
     *
     * @param context the DSpace context
     * @param item the item to embargo
     */
    public void checkEmbargo(Context context, Item item)
        throws SQLException, AuthorizeException, IOException
    {
        for (Bundle bn : item.getBundles())
        {
            // Skip the LICENSE and METADATA bundles, they stay world-readable
            String bnn = bn.getName();
            if (!(bnn.equals(Constants.LICENSE_BUNDLE_NAME) || bnn.equals(Constants.METADATA_BUNDLE_NAME) || bnn.equals(CreativeCommons.CC_BUNDLE_NAME)))
            {
                // don't report on "TEXT" or "THUMBNAIL" bundles; those
                // can have READ long as the bitstreams in them do not.
                if (!(bnn.equals("TEXT") || bnn.equals("THUMBNAIL")))
                {
                    // check for ANY read policies and report them:
                    for (ResourcePolicy rp : AuthorizeManager.getPoliciesActionFilter(context, bn, Constants.READ))
                    {
                        System.out.println("CHECK WARNING: Item "+item.getHandle()+", Bundle "+bn.getName()+" allows READ by "+
                          ((rp.getEPersonID() < 0) ? "Group "+rp.getGroup().getName() :
                                                      "EPerson "+rp.getEPerson().getFullName()));
                    }
                }

                for (Bitstream bs : bn.getBitstreams())
                {
                    for (ResourcePolicy rp : AuthorizeManager.getPoliciesActionFilter(context, bs, Constants.READ))
                    {
                        System.out.println("CHECK WARNING: Item "+item.getHandle()+", Bitstream "+bs.getName()+" (in Bundle "+bn.getName()+") allows READ by "+
                          ((rp.getEPersonID() < 0) ? "Group "+rp.getGroup().getName() :
                                                      "EPerson "+rp.getEPerson().getFullName()));
                    }
                }
            }
        }
    }

	
	
	
	
	
	
	
	
}
