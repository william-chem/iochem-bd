/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Hex;

public class TokenManager {
	private static final int HASH_ROUNDS = 1024; // XXX magic 1024 rounds
	private static final String encodingAlgorithm = "SHA-512";
	private static final String characterEncoding = "UTF-8";
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static final int RANDOM_STRING_LENGTH = 33;
	
	private static SecureRandom rnd = new SecureRandom();

	public static String encode(String salt, String secret) {
		try {
			MessageDigest digester = MessageDigest.getInstance(encodingAlgorithm);
			if (secret == null)
				secret = "";
			// Grind up the salt with the password, yielding a hash
			digester.update(salt.getBytes(characterEncoding));
			digester.update(secret.getBytes(characterEncoding)); // Round 0
			for (int round = 1; round < HASH_ROUNDS; round++) {
				byte[] lastRound = digester.digest();
				digester.reset();
				digester.update(lastRound);
			}
			return new String(Hex.encodeHex(digester.digest()));

		} catch (UnsupportedEncodingException e) {
			return null;
		} catch (NoSuchAlgorithmException e) {
			return null;
		}
	}

	public static String buildRandomString() {
		return buildRandomString(RANDOM_STRING_LENGTH);
	}

	public static String buildRandomString(int len) {
		StringBuilder sb = new StringBuilder(len);
		for (int i = 0; i < len; i++)
			sb.append(AB.charAt(rnd.nextInt(AB.length())));
		return sb.toString();
	}
}
