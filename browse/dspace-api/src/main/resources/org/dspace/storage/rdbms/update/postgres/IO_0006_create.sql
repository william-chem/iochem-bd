-- Modify cutting_area_definitions table
ALTER TABLE cutting_area_definitions ADD COLUMN requires VARCHAR;
ALTER TABLE cutting_area_definitions ADD COLUMN renameto VARCHAR;


-- Insert LAMMPS type 
SELECT setval('calculation_types_id_seq', max(id)) FROM calculation_types;
INSERT INTO calculation_types VALUES (nextval('calculation_types_id_seq'), '2022-05-06 13:34:00.000000', 'LAM', 'LAMMPS', 'LAMMPS Type', '/html/xsltmd/lammps.xsl');


SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '.in.file', '-', 'LAMMPS - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'lammps_input', 'lammps_input', 'chemical/x-lammps-input', 'input', 'Input file(1*)', NULL, NULL, NULL);

INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-p', '.*.dat', '-', 'LAMMPS - data_file', 'org.xmlcml.cml.converters.compchem.lammps.data.LammpsData2XMLConverter', 'lammps_data', 'lammps_data_xml', 'chemical/x-cml', 'append', 'Data file for read_data(2*)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-p', '.*.dat', '-', 'LAMMPS - data file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'lammps_data', 'lammps_data', 'chemical/x-lammps-datafile', 'input', 'Data file for read_data(2*)', NULL, NULL, NULL);

INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-o', 'log.lammps', '-', 'LAMMPS - output file', 'org.xmlcml.cml.converters.compchem.lammps.log.LammpsLog2CompchemConverter', 'lammps_log', 'lammps_log_compchem', 'chemical/x-cml', 'output', 'Output file log.lammps(3*)', NULL, NULL, NULL);

INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-t', '.*.tar.gz|.*.zip', '-', 'LAMMPS - Trajectory file', 'org.xmlcml.cml.converters.compchem.lammps.trajectory.LammpsTrajectory2CIFConverter', 'lammps_trajectory', 'lammps_trajectory_cif', 'chemical/x-cif', 'input', 'Trajectory file (4*)', NULL, 'lammps_data_xml', 'multistep.cif');
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-t', '.*.tar.gz|.*.zip', '-', 'LAMMPS - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'lammps_trajectory', 'lammps_trajectory', 'chemical/x-lammps-trajectory', 'additional', 'Trajectory file (4*)', NULL, NULL, NULL);

INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-a', '.*.*', '-', 'LAMMPS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable', NULL, NULL);

SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-lammps-input', 'lammps_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-lammps-input', 'lammps_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'lammps_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlLAMMPS.xsl -extension .html');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/chemical/x-lammps-trajectory', 'lammps_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
