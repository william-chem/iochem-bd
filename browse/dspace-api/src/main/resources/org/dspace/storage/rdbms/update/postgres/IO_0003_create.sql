-- Fix typo on cutting_area_definitions
UPDATE cutting_area_definitions SET use = 'input' where use = 'input ';

-- Insert GROMACS type 
SELECT setval('calculation_types_id_seq', max(id)) FROM calculation_types;
INSERT INTO calculation_types VALUES (nextval('calculation_types_id_seq'), '2021-09-03 12:00:00.000000', 'GRM', 'GROMACS', 'GROMACS Type', '/html/xsltmd/gromacs.xsl');

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '.*.in', '-', 'GROMACS - input file', 'org.xmlcml.cml.converters.compchem.gromacs.input.GromacsInput2XMLConverter', 'gromacs_input', 'gromacs_input_xml', 'chemical/x-cml', 'append', 'Input file .mdp(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '.*.in', '-', 'GROMACS - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_input', 'gromacs_input', 'chemical/x-gromacs-input', 'input', 'Input file .mdp(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-o', '.*.log', '-', 'GROMACS - output file', 'org.xmlcml.cml.converters.compchem.gromacs.log.GromacsLog2CompchemConverter', 'gromacs_log', 'gromacs_log_compchem', 'chemical/x-cml', 'output', 'Output file .log(2*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-oc', '.*.gro', '-', 'GROMACS - geometry file', 'org.xmlcml.cml.converters.compchem.gromacs.geometry.GromacsGeometry2XMLConverter', 'gromacs_geometry', 'gromacs_geometry_xml', 'chemical/x-cml', 'append', 'Geometry file .gro(3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-oc', '.*.gro', '-', 'GROMACS - geometry file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_geometry', 'gromacs_geometry', 'chemical/x-gromos87', 'input', 'Geometry file .gro (3*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-t', '.*.xtc', '-', 'GROMACS - Trajectory file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'gromacs_trajectory', 'gromacs_trajectory', 'chemical/x-gromacs-trajectory', 'input', 'Trajectory file .xtc (4*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-a', '.*.*', '-', 'GROMACS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable');

SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-gromacs-input', 'gromacs_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-gromacs-input', 'gromacs_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'gromacs_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlGROMACS.xsl -extension .html');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-gromos87', 'gromacs_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-gromos87', 'gromacs_geometry', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-gromacs-trajectory', 'gromacs_trajectory', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
