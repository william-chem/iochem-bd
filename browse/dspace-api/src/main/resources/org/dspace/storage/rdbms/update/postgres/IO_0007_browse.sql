-- Append new mimetypes

SELECT setval('bitstreamformatregistry_seq', max(bitstream_format_id)) FROM bitstreamformatregistry;
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-castep-input', 'CASTEP input', 'CASTEP input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-castep-cell', 'CASTEP cell file', 'CASTEP cell file with geometry information, including cell parameters, atomic coordinates, and k-point coordinates and weights.', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-castep-geometry', 'CASTEP geometry optimization file', 'Geometry optimization trajectory data.', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-castep-xcd', 'CASTEP graph file', 'Chart document.', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-castep-output', 'CASTEP report on the calculation', 'CASTEP report on the calculation.', 1, false);
