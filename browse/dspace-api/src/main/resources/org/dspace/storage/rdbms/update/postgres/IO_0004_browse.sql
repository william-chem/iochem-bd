-- Append new mimetypes

SELECT setval('bitstreamformatregistry_seq', max(bitstream_format_id)) FROM bitstreamformatregistry;
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-amber-input', 'Amber input', 'Amber input file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-amber-topology', 'Amber topology', 'Amber parameter/trajectory file', 1, false);
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-amber-trajectory', 'Amber trajectory', 'Amber trajectory in NetCDF format', 1, false);