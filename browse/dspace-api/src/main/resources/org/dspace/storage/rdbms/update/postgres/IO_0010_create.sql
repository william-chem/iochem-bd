-- Insert SIESTA type 
SELECT setval('calculation_types_id_seq', max(id)) FROM calculation_types;
INSERT INTO calculation_types VALUES (nextval('calculation_types_id_seq'), '2022-12-24 11:45:00.000000', 'SIE', 'SIESTA', 'SIESTA Type', '/html/xsltmd/siesta.xsl');

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '.*.in', '-', 'SIESTA - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'siesta_input', 'siesta_input', 'chemical/x-siesta-input', 'input', 'Input file(1*)', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-a', '.*.*', '-', 'SIESTA - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(6)', 'repeatable');

SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-siesta-input', 'siesta_input', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-siesta-input', 'siesta_input', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');

INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'siesta_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlSIESTA.xsl -extension .html');