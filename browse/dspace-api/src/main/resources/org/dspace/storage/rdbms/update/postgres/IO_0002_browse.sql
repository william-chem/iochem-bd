-- Append new mimetypes

SELECT setval('bitstreamformatregistry_seq', max(bitstream_format_id)) FROM bitstreamformatregistry;
INSERT INTO bitstreamformatregistry VALUES (nextval('bitstreamformatregistry_seq'), 'chemical/x-qespresso-spectra', 'QuantumEspresso absorption spectra', 'QuantumEspresso absorption spectra file', 1, false);

