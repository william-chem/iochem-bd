<?xml version="1.0" encoding="UTF-8"?>
<!--
  - Browse module - Browse module inside the ioChem-BD software.
  - Copyright © 2019 ioChem-BD (contact@iochem-bd.org)
  -
  - This program is free software: you can redistribute it and/or modify
  - it under the terms of the GNU Affero General Public License as published by
  - the Free Software Foundation, either version 3 of the License, or
  - (at your option) any later version.
  -
  - This program is distributed in the hope that it will be useful,
  - but WITHOUT ANY WARRANTY; without even the implied warranty of
  - MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  - GNU Affero General Public License for more details.
  - 
  - You should have received a copy of the GNU Affero General Public License
  - along with this program.  If not, see <http://www.gnu.org/licenses/>.
  -
  -->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output method="xml" encoding="utf-8" indent="yes" omit-xml-declaration="no" />
    <xsl:template match="/">
        <xsl:apply-templates />            
    </xsl:template>
        
    <xsl:template match="@*|node()">
        <xsl:choose>
            <xsl:when test=" current() instance of element() and .[compare(@cmlx:templateRef,'magnetization') = 0 and cmlx:isMagnetizationInvalid(current())]">
                <xsl:call-template name="fixMagnetization">
                    <xsl:with-param name="node" select="current()"/>
                </xsl:call-template>
            </xsl:when>
            <xsl:otherwise>
                <xsl:copy>
                    <xsl:apply-templates select="@*|node()"/>
                </xsl:copy>        
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>    
    
    <xsl:template name="fixMagnetization">
        <xsl:param name="node"/>
        
        <module xmlns="http://www.xml-cml.org/schema" cmlx:templateRef="magnetization">
            <xsl:copy-of select="$node/*:array[@dictRef='cc:serial']"/>
            <xsl:copy-of select="$node/*:array[@dictRef='v:coeffs']"/>
            <xsl:copy-of select="$node/*:array[@dictRef='v:coeffp']"/>                       
            <xsl:copy-of select="$node/*:array[@dictRef='v:coeffd']"/>            
            <xsl:variable name="coefff" select="tokenize($node/*:array[@dictRef='v:coefff'],'\s+')"/>
            <xsl:variable name="totals" select="tokenize($node/*:array[@dictRef='v:coefftotal'],'\s+')"/>
            <xsl:variable name="newTotals">                               
                <xsl:for-each select="1 to count($coefff)">
                    <xsl:variable name="outerIndex" select="."/>
                    <xsl:value-of select="concat($coefff[$outerIndex], $totals[$outerIndex], ' ')"/>
                </xsl:for-each>                
            </xsl:variable>
            <array dataType="xsd:double" dictRef="v:coefftotal" size="{count($coefff)}"><xsl:value-of select="replace($newTotals, '\s+$', '')"/></array>
            
            <list cmlx:templateRef="totals">
                <xsl:copy-of select="$node/*:list[@cmlx:templateRef='totals']/*:scalar[@dictRef='v:totalcoeffs']"/>
                <xsl:copy-of select="$node/*:list[@cmlx:templateRef='totals']/*:scalar[@dictRef='v:totalcoeffp']"/>
                <xsl:copy-of select="$node/*:list[@cmlx:templateRef='totals']/*:scalar[@dictRef='v:totalcoeffd']"/>                
                <xsl:variable name="total_part1" select="$node/*:list[@cmlx:templateRef='totals']/*:scalar[@dictRef='v:totalcoefff']/text()"/>
                <xsl:variable name="total_part2" select="$node/*:list[@cmlx:templateRef='totals']/*:scalar[@dictRef='v:coefftotalsum']/text()"/>                
                <scalar dataType="xsd:double" dictRef="v:coefftotalsum"><xsl:value-of select="concat($total_part1, $total_part2)"/></scalar>
            </list>
                        
        </module>
    </xsl:template>
    
    
    <xsl:function name="cmlx:isMagnetizationInvalid" as="xs:boolean">
        <xsl:param name="node"/>        
        <xsl:variable name="totals" select="$node//cml:array[@dictRef='v:coefftotal']"/>
        <xsl:value-of select="exists($totals) and not(contains($totals/text(),'.'))"/>        
    </xsl:function>
</xsl:stylesheet>
