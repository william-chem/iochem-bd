-- Insert AMS type (newer ADF versions) 
SELECT setval('calculation_types_id_seq', max(id)) FROM calculation_types;
INSERT INTO calculation_types VALUES (nextval('calculation_types_id_seq'), '2022-10-27 11:14:00.000000', 'AMS', 'AMS', 'AMS with ADF Type', '/html/xsltmd/adf.xsl');

SELECT setval('cutting_area_definitions_id_seq', max(id)) FROM cutting_area_definitions;
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-i', '.in', '-', 'ADF - input file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'adf_input', 'adf_input', 'chemical/x-adf-input', 'input', 'Input file (1*)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-rkf1', 'ams.rkf', '-', 'AMS - ams.rkf file', 'org.xmlcml.cml.converters.compchem.ams.log.AmsRkf2XMLPathConverter', 'ams_rkf', 'ams_rkf_xml', 'application/xml', 'append', 'ams.rkf file(2*)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-rkf2', 'adf.rkf', '-', 'AMS - ads.rkf file', 'org.xmlcml.cml.converters.compchem.ams.log.AmsRkf2XMLConverter',     'ads_rkf', 'ads_rkf_xml', 'chemical/x-cml', 'append', 'adf.rkf file(3*)', NULL, 'ams_rkf_xml', NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-o', '.out', '-', 'AMS - output file', 'org.xmlcml.cml.converters.compchem.ams.log.AmsLog2CompchemConverter', 'ams_log', 'ams_log_compchem', 'chemical/x-cml', 'output', 'Output .out file (4*)', NULL, NULL, NULL);
INSERT INTO cutting_area_definitions VALUES (nextval('cutting_area_definitions_id_seq'), currval('calculation_types_id_seq'), true, '-a', '.*.*', '-', 'AMS - additional file', 'org.xmlcml.cml.converters.compchem.additional.Additional2AdditionalConverter', 'noformat_additional', 'noformat_additional', '', 'additional', 'Additional file(5)', 'repeatable', NULL, NULL);

SELECT setval('actions_id_seq', max(id)) FROM actions;
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.JmolViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.DownloadAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.TextViewAction', '-');
INSERT INTO actions VALUES (nextval('actions_id_seq'), 'chemical/x-cml', 'ams_log_compchem', 'cat.iciq.tcg.labbook.zk.manager.actions.XsltTransformAction', '-template  html/xslt/cml2htmlAMS.xsl -extension .html');