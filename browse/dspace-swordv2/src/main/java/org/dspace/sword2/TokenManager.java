package org.dspace.sword2;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import org.apache.commons.codec.binary.Hex;

public class TokenManager {	
	private static final int HASH_ROUNDS = 1024; // XXX magic 1024 rounds	
	private static final String encodingAlgorithm = "SHA-512";	
	private static final String characterEncoding = "UTF-8";		
	private static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
	private static SecureRandom rnd = new SecureRandom();
		
	public static String encode(String salt, String secret){		
		try{			
			MessageDigest digester = MessageDigest.getInstance(encodingAlgorithm);		
	        if (secret == null)
	            secret = "";	        
	        // Grind up the salt with the password, yielding a hash
	        digester.update(salt.getBytes(characterEncoding));	
	        digester.update(secret.getBytes(characterEncoding)); // Round 0
	        for (int round = 1; round < HASH_ROUNDS; round++){
	            byte[] lastRound = digester.digest();
	            digester.reset();
	            digester.update(lastRound);
	        }
	        return new String(Hex.encodeHex(digester.digest()));
	        
		}catch(UnsupportedEncodingException e){
			return null;
		}catch(NoSuchAlgorithmException e){
			return null;
		}
	}

	public static String buildRandomString(int len) {	
		StringBuilder sb = new StringBuilder( len );
		for( int i = 0; i < len; i++ ) 
			sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
		return sb.toString();		
	}
}