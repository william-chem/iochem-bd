/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.servlet;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.StringWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Templates;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.log4j.Logger;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Item;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.LogManager;


public class CML2XYZServlet extends DSpaceServlet {

private static Logger log = Logger.getLogger(CML2XYZServlet.class);
    
	private static final long serialVersionUID = 1L;
	private static final String XSLT_PATH 		= "/xslt/cml2xyz.xsl";
	private static final String XYZ_MIMETYPE 	= "chemical/x-xyz";
	private static final String XYZ_FILENAME	= "cml2xyz.xyz";
	private static TransformerFactory tFactory 	= null; 
	private static Templates template			= null;
	
	static {
		tFactory = new net.sf.saxon.TransformerFactoryImpl();
	}
	
	public CML2XYZServlet()
	{
		super();
	}

	@Override
	protected void doDSGet(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException 
    {
		OutputStream    ostream 	= response.getOutputStream();
    	if(template == null){		//First call to servlet will load conversion template
			StreamSource xslSource =  new StreamSource(new File(this.getServletContext().getRealPath("/") + XSLT_PATH));
			try {
				template = tFactory.newTemplates(xslSource);
			} catch (TransformerConfigurationException e) {
				log.error(LogManager.getHeader(context, "load_template", "bitstream_id=" + this.getServletContext().getRealPath("/") + XSLT_PATH));
				log.error(e.getMessage());
			}
		}
    	    	
		try
		{
			int calculationId = Integer.parseInt(request.getParameter("id"));
			String mode = request.getParameter("mode");
			String xyzFileContent = getXYZFileContent(context, request, calculationId, mode);						
			String mimetype =  XYZ_MIMETYPE;
		    response.addHeader("Access-Control-Allow-Origin", "*");
		    response.addHeader("Access-Control-Allow-Methods","GET");		 
			response.setContentType(mimetype);
	        response.setContentLength((int)xyzFileContent.length());	    
	        response.setHeader("Content-Disposition", "attachment; filename=\"" + XYZ_FILENAME + "\"");
	        ostream.write(xyzFileContent.getBytes());
		}
		catch (Exception e)	
		{			
			
		}			
	}
	
	private String getXYZFileContent(Context context, HttpServletRequest request, int id, String mode) throws SQLException, IOException, AuthorizeException
	{		
		Item item = Item.find(context, id);
		// Ensure the user has authorisation
        AuthorizeManager.authorizeAction(context, item, Constants.READ);
		AuthorizeManager.checkItemIsNotEmbargoed(context, item);
		//Search for ORIGINAL bundle + cml bitstream
		Bundle[] metaBunds = item.getBundles("ORIGINAL");
		Bitstream cmlFileBitstream = null;
		if (metaBunds.length > 0)
		{
		    Bitstream[] bits = metaBunds[0].getBitstreams();             
		    for (int i = 0; (i < bits.length); i++)
		        if (bits[i].getFormat().getMIMEType().equals("chemical/x-cml"))
		            cmlFileBitstream = bits[i];
		}          
		if(cmlFileBitstream == null)
			 return "";         		
	
        try
        {	
    		context.turnOffAuthorisationSystem();
    		StreamSource xml = new StreamSource(cmlFileBitstream.retrieve());
    	    StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);        	              	
            Transformer transformer = template.newTransformer();
            if(mode != null ) 
                transformer.setParameter("mode", mode);
            transformer.transform(xml, result);            
    		context.restoreAuthSystemState();
    		log.info( LogManager.getHeader(context, "cml2xyz", "bitstream_id=" + cmlFileBitstream.getID()));
            return writer.toString();
        }
        catch (TransformerConfigurationException e)
        {
            return "";
        }
        catch (TransformerException e)
        {
            return "";        
        }  
	}	
}

