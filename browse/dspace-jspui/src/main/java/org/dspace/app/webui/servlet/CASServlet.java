/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.servlet;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.dspace.app.webui.util.Authenticate;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.authenticate.AuthenticationManager;
import org.dspace.authenticate.AuthenticationMethod;
import org.dspace.authorize.AuthorizeException;
import org.dspace.core.Context;
import org.dspace.core.LogManager;

public class CASServlet extends DSpaceServlet {

    /** log4j logger */
    private static Logger log = Logger.getLogger(LDAPServlet.class);

    protected void doDSGet(Context context,
        HttpServletRequest request,
        HttpServletResponse response)
        throws ServletException, IOException, SQLException, AuthorizeException
    {    
   
    	String jsp = null;
    	
        // Locate the eperson
        int status = AuthenticationManager.authenticate(context, null, null, null, request);
        
        if (status == AuthenticationMethod.SUCCESS){
            // Logged in OK.
            Authenticate.loggedIn(context, request, context.getCurrentUser());
            log.info(LogManager.getHeader(context, "login", "type=cas"));
           
            HttpSession session = request.getSession(false);
            if(session!=null){
            	session.setAttribute("loginType", "CAS");
            }          
            // resume previous request
            Authenticate.resumeInterruptedRequest(request, response);
            return;
        }else if (status == AuthenticationMethod.CERT_REQUIRED){
            jsp = "/error/require-certificate.jsp";
        }else if(status == AuthenticationMethod.NO_SUCH_USER){
            jsp = "/login/no-single-sign-out.jsp";
        }else if(status == AuthenticationMethod.BAD_ARGS){
            jsp = "/login/no-email.jsp";
        }
        
        // If we reach here, supplied email/password was duff.
        log.info(LogManager.getHeader(context, "failed_login","result="+String.valueOf(status)));
        JSPManager.showJSP(request, response, jsp);
        return;
    }


    protected void doDSPost(Context context,
        HttpServletRequest request,
        HttpServletResponse response)
        throws ServletException, IOException, SQLException, AuthorizeException
    {

	}
}
