/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package org.dspace.app.webui.servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Item;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.LogManager;
import org.dspace.handle.HandleManager;
import org.dspace.usage.UsageEvent;
import org.dspace.utils.DSpace;

import com.google.gson.Gson;

public class MoldenServlet extends DSpaceServlet
{
	private static final long serialVersionUID = 1L;
	
	private static final String TEXT_MIMETYPE = "text/plain";
	private static final String JSON_MIMETYPE = "application/json";
	private static final Pattern symmPattern = Pattern.compile("\\s*Sym=\\s*(\\S*)\\s*");
	
	private static final String FILE_PARAMETER = "file";
	private static final String LABEL_PARAMETER = "label";
	private static final String INDEX_PARAMETER = "index";
	private static final String BITSTREAM_PARAMETER = "bitstream";
	
    private static Logger log = Logger.getLogger(MoldenServlet.class);
      
    private static Bitstream getItemBitstreamById(Item item, int bitstreamId) throws SQLException{
        Bundle[] bundles = item.getBundles();
        for (int i = 0; i < bundles.length; i++){
            Bitstream[] bitstreams = bundles[i].getBitstreams();
            for (int k = 0; k < bitstreams.length; k++)
                if (bitstreams[k].getID() == bitstreamId)
                    return bitstreams[k];                           
        }
        return null;
    }

    // On the surface it doesn't make much sense for this servlet to handle POST requests, but in practice some HTML pages which are actually JSP get called on with a POST, so it's needed.
    protected void doDSPost(Context context, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, AuthorizeException {
        doDSGet(context, request, response);
    }

    protected void doDSGet(Context context, HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException, AuthorizeException {
        Item item = null;
        Bitstream bitstream = null;
        String idString = request.getPathInfo();
        String handle = null;
        String bitstreamId = null;
        String orbitalLabel = null;
        int orbitalIndex = -1;
        // Parse URL        
        try {
        	 if (idString != null){
                 // Remove leading slash
                 if (idString.startsWith("/"))
                     idString = idString.substring(1);

                 handle = idString;
                 if(request.getParameterMap().size() > 0) {
                	if(request.getParameterMap().containsKey(FILE_PARAMETER)) 
                		bitstreamId = request.getParameter(FILE_PARAMETER).trim();
                	if(request.getParameterMap().containsKey(BITSTREAM_PARAMETER)) 
                		bitstreamId = request.getParameter(BITSTREAM_PARAMETER).trim();		//Override bitstreamId if its provided
                	if(request.getParameterMap().containsKey(LABEL_PARAMETER))
                		orbitalLabel = request.getParameter(LABEL_PARAMETER).trim();
                 	if(request.getParameterMap().containsKey(INDEX_PARAMETER))
                 		orbitalIndex = Integer.valueOf(request.getParameter(INDEX_PARAMETER));                 	
                 }
             }        	        	
        }catch(Exception e) {
        	log.error(LogManager.getHeader(context, "molden", "invalid_parameters=" + idString));
            JSPManager.showInvalidIDError(request, response, idString,Constants.BITSTREAM);
        }
       
        if(handle != null) {
        	// Find the item
            try{
                // If the original item doesn't have a Handle yet (because it's in the workflow) what we actually have is a fake Handle in the form: db-id/1234 where 1234 is the database ID of the item.                 
                if (handle.startsWith("db-id")){                    
                	String dbIDString = handle.substring(handle.indexOf('/') + 1);
                    int dbID = Integer.parseInt(dbIDString);
                    item = Item.find(context, dbID);
                }
                else{
                    item = (Item) HandleManager.resolveToObject(context, handle);
                }
            }
            catch (NumberFormatException nfe){
                // Invalid ID - this will be dealt with below
            }
        	
            
            try {
            	if(item != null) {
            		OutputStream ostream = response.getOutputStream();
            		if(bitstreamId != null) {        			
            			// Try to find bitstream with exactly matching name + path
                        bitstream = getItemBitstreamById(item, Integer.valueOf(bitstreamId));
                        if (bitstream != null ){
                        	log.info(LogManager.getHeader(context, "molden", "handle=" + handle + ",bitstream_id=" + bitstream.getID()));        	
                            new DSpace().getEventService().fireEvent(new UsageEvent(UsageEvent.Action.VIEW, request, context, bitstream));                                                   
                            if(orbitalLabel != null || orbitalIndex != -1) {
                    			//Build molden file with the specific orbital label	                        	
                            	String content = getMoldenFile(bitstream, orbitalLabel, orbitalIndex);	                        				
    							content = content.replace("MOLDEN FORMAT", "Molden Format");	 //Action performed to avoid a bug of Jsmol that fails to load molden orbital files with its header in capital letters												
    							response.setContentType(TEXT_MIMETYPE + ";charset=UTF-8");
    							response.setContentLength(content.length());												    
    							ostream.write(content.getBytes(Charset.forName("UTF-8")));													
                    		}
                        }else {
                            // No bitstream - we got an invalid ID
                            log.info(LogManager.getHeader(context, "molden", "invalid_parameters=" + idString));
                            JSPManager.showInvalidIDError(request, response, idString,Constants.BITSTREAM);
                        }
                	}else {
                		//Retrieve calculation molden orbital files names and internal bitstream ids            		
                		String content = new Gson().toJson(getMoldenFilesInfo(item));
    					response.setContentType(JSON_MIMETYPE + ";charset=UTF-8");
    					response.setContentLength(content.length());
    					ostream.write(content.getBytes(Charset.forName("UTF-8")));
                	}
            		response.getOutputStream().flush();
            	}
            }catch(Exception e){
            	log.error(LogManager.getHeader(context, "molden", "invalid_parameters=" + idString));
                JSPManager.showInvalidIDError(request, response, idString,Constants.BITSTREAM);
            }
        }
    }

	private List<MoldenFile> getMoldenFilesInfo(Item item) throws Exception {
		ArrayList<MoldenFile> moldenFiles = new ArrayList<MoldenFile>();
		
		Bundle[] bunds = item.getBundles("ORIGINAL");		
		if (bunds[0] != null) {
			Bitstream[] bits = bunds[0].getBitstreams();
			for (int i = 0; (i < bits.length); i++) {			
				if (bits[i].getFormat().getMIMEType().equals("chemical/x-molden")) {
					Bitstream orbital = bits[i];
					MoldenFile molden = new MoldenFile();
					molden.setFilename(orbital.getName());
					molden.setBitstreamId(orbital.getID());
					molden.setOrbitals(buildOrbitalHeaders(orbital));                    
                    moldenFiles.add(molden);
                }
			}
		}				
		return moldenFiles;
	}

    private List<String> buildOrbitalHeaders(Bitstream bitstream) throws IOException, SQLException, AuthorizeException{
    	List<String> headers = new ArrayList<String>();
		BufferedReader bf = new BufferedReader(new InputStreamReader(bitstream.retrieve()));	
		String line = null;		
		int spinAlpha = 0;
		int spinBeta = 0;
		while( (line = bf.readLine()) != null){
			if(symmPattern.matcher(line).matches()){
				String symmetry = line.replaceAll("\\s*Sym=\\s*","");				
				String energy = bf.readLine().replaceAll("\\s*Ene=\\s*", "");				
				String spin = bf.readLine().replaceAll("\\s*Spin=\\s*", "");
				String occup = bf.readLine().replaceAll("\\s*Occup=\\s*", "");
				occup = formatDouble(occup);
				energy = formatDouble(energy);
				if(spin.equals("Alpha"))
					headers.add(String.format("%1$-4s %2$-6s %3$-4s %4$11s %5$11s",spinAlpha++, spin, symmetry, occup, energy));
				else
					headers.add(String.format("%1$-4s %2$-6s %3$-4s %4$11s %5$11s",spinBeta++, spin, symmetry, occup, energy));
			}			
		}			
		bf.close();		
		return headers;
	}

	private String formatDouble(String val){
		try{
			double d = Double.parseDouble(val);					
			DecimalFormat df = new DecimalFormat("#0.000");
			return df.format(d);				
		}catch(Exception e){
			log.error("Exception raised while formatting orbital values from :" + val);
			return val;
		}			
	}

	private String getMoldenFile(Bitstream bitstream, String label, int index) throws Exception {
		if(label != null && !label.equals("")) 
			index = getOrbitalIndex(bitstream, label);
		
		return 	buildMoldenOrbitalFile(bitstream, index);			
	}
	
	private int getOrbitalIndex(Bitstream bitstream, String label) throws Exception {		
		BufferedReader bf = new BufferedReader(new InputStreamReader(bitstream.retrieve()));
		String line = null;
		int spinAlpha = 0;
		int spinBeta = 0;
		int index = 0;		
		try {
			
			int labelIndex = Integer.parseInt(label.replaceAll("(?![0-9]+).*", ""));
			String labelSymmetry = label.replaceAll("^[0-9]+", "").toUpperCase();		
			while((line = bf.readLine()) != null){
				if(symmPattern.matcher(line).matches()){					
					String symmetry = line.replaceAll("\\s*Sym=\\s*","").trim().toUpperCase();				
					bf.readLine();				
					String spin = bf.readLine().replaceAll("\\s*Spin=\\s*", "").trim().toUpperCase();
					// Exact match by symmetry label
					if(spin.equals("ALPHA")) {
						if(symmetry.equals(label.toUpperCase()))
							return spinAlpha + spinBeta + 1;
						spinAlpha++;						
					} else {
						if(symmetry.equals(label.toUpperCase()))
							return spinAlpha + spinBeta + 1;
						spinBeta++;
					}	
					
					// Alternative match by current index and label, starts on 0
					try {
						symmetry = symmetry.replaceAll("^[0-9]+", "");
						if(!labelSymmetry.equals("")) {
							if(labelSymmetry.startsWith("A") && spin.equals("ALPHA") && (spinAlpha -1) == labelIndex)
								index = spinAlpha + spinBeta;
							else if(labelSymmetry.startsWith("B") && spin.equals("BETA") && (spinBeta -1) == labelIndex)
								index = spinAlpha + spinBeta;																					
						}
					}catch(Exception e) {
						index = 0;
					}
				}
			}				
		}catch(IOException e) {
			
		}finally {
			try {
				bf.close();
			} catch (IOException e) {
				
			}	
		}
		return index;
	}	

	private String buildMoldenOrbitalFile(Bitstream bitstream, int index) throws IOException, SQLException, AuthorizeException{			
		BufferedReader bf = new BufferedReader(new InputStreamReader(bitstream.retrieve()));
		StringBuilder sb = new StringBuilder();
		String line = null;		
		int inx = 0;	
		while((line = bf.readLine()) != null && !line.contains("[MO]")){ //Copy content until we get to molecular orbital section 
			sb.append(line + "\n");
		}
		sb.append(line + "\n");
		while( (line = bf.readLine()) != null && !line.startsWith("[")){  //Discard other sections
			if(line.toUpperCase().contains("SYM=") && (++inx == index)){			
				do{
					sb.append(line + "\n");					
					line = bf.readLine();
				}while(line != null && !line.startsWith("[") && !line.toUpperCase().contains("SYM="));
				break;
			}		
		}			
		bf.close();		
		return sb.toString();		
	}
}

class MoldenFile {
	String filename;
	int bitstreamId;
	Collection<String> orbitals;
	
	public MoldenFile() {
		filename = "";
		orbitals = new ArrayList<String>();		
	}
	
	public MoldenFile(String filename, int bitstreamId, Collection<String> orbitals) {
		this.filename = filename;
		this.bitstreamId = bitstreamId;
		this.orbitals = orbitals;		
	}
	
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}

	public int getBitstreamId() {
		return bitstreamId;
	}

	public void setBitstreamId(int bitstreamId) {
		this.bitstreamId = bitstreamId;
	}

	public Collection<String> getOrbitals() {
		return orbitals;
	}
	
	public void setOrbitals(Collection<String> orbitals) {
		this.orbitals = orbitals;
	}

	public void addOrbital(String orbital) {
		this.orbitals.add(orbital);
	}	
}

