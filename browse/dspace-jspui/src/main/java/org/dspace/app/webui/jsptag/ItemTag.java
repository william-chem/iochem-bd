/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 *
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */
package org.dspace.app.webui.jsptag;



import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocaleSupport;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.dspace.app.util.DCInputsReaderException;
import org.dspace.app.util.MetadataExposure;
import org.dspace.app.util.Util;
import org.dspace.app.webui.servlet.CollectionEditServlet;
import org.dspace.app.webui.util.StyleSelection;
import org.dspace.app.webui.util.UIUtil;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.browse.BrowseException;
import org.dspace.content.Bitstream;
import org.dspace.content.Bundle;
import org.dspace.content.Collection;
import org.dspace.content.DCDate;
import org.dspace.content.Metadatum;
import org.dspace.content.Item;
import org.dspace.content.authority.MetadataAuthorityManager;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.I18nUtil;
import org.dspace.core.PluginManager;
import org.dspace.core.Utils;
import org.dspace.identifier.DOIIdentifierProvider;
import org.dspace.identifier.doi.CrossRefDOIConnector;

/**
 * <P>
 * JSP tag for displaying an item.
 * </P>
 * <P>
 * The fields that are displayed can be configured in <code>dspace.cfg</code>
 * using the <code>webui.itemdisplay.(style)</code> property. The form is
 * </P>
 * 
 * <PRE>
 * 
 * &lt;schema prefix&gt;.&lt;element&gt;[.&lt;qualifier&gt;|.*][(date)|(link)], ...
 * 
 * </PRE>
 * 
 * <P>
 * For example:
 * </P>
 * 
 * <PRE>
 * 
 * dc.title = Dublin Core element 'title' (unqualified)
 * dc.title.alternative = DC element 'title', qualifier 'alternative'
 * dc.title.* = All fields with Dublin Core element 'title' (any or no qualifier)
 * dc.identifier.uri(link) = DC identifier.uri, render as a link
 * dc.date.issued(date) = DC date.issued, render as a date
 * dc.identifier.doi(doi) = DC identifier.doi, render as link to http://dx.doi.org
 * dc.identifier.hdl(handle) = DC identifier.hanlde, render as link to http://hdl.handle.net
 * dc.relation.isPartOf(resolver) = DC relation.isPartOf, render as link to the base url of the resolver 
 *                                  according to the specified urn in the metadata value (doi:xxxx, hdl:xxxxx, 
 *                                  urn:issn:xxxx, etc.)
 * 
 * </PRE>
 * 
 * <P>
 * When using "resolver" in webui.itemdisplay to render identifiers as resolvable
 * links, the base URL is taken from <code>webui.resolver.<n>.baseurl</code> 
 * where <code>webui.resolver.<n>.urn</code> matches the urn specified in the metadata value.
 * The value is appended to the "baseurl" as is, so the baseurl need to end with slash almost in any case.
 * If no urn is specified in the value it will be displayed as simple text.
 * 
 * <PRE>
 * 
 * webui.resolver.1.urn = doi
 * webui.resolver.1.baseurl = http://dx.doi.org/
 * webui.resolver.2.urn = hdl
 * webui.resolver.2.baseurl = http://hdl.handle.net/
 * 
 * </PRE>
 * 
 * For the doi and hdl urn defaults values are provided, respectively http://dx.doi.org/ and 
 * http://hdl.handle.net/ are used.<br> 
 * 
 * If a metadata value with style: "doi", "handle" or "resolver" matches a URL
 * already, it is simply rendered as a link with no other manipulation.
 * </P>
 * 
 * <PRE>
 * 
 * <P>
 * If an item has no value for a particular field, it won't be displayed. The
 * name of the field for display will be drawn from the current UI dictionary,
 * using the key:
 * </P>
 * 
 * <PRE>
 * 
 * &quot;metadata.&lt;style.&gt;.&lt;field&gt;&quot;
 * 
 * e.g. &quot;metadata.thesis.dc.title&quot; &quot;metadata.thesis.dc.contributor.*&quot;
 * &quot;metadata.thesis.dc.date.issued&quot;
 * 
 * 
 * if this key is not found will be used the more general one
 * 
 * &quot;metadata.&lt;field&gt;&quot;
 * 
 * e.g. &quot;metadata.dc.title&quot; &quot;metadata.dc.contributor.*&quot;
 * &quot;metadata.dc.date.issued&quot;
 * 
 * </PRE>
 * 
 * <P>
 * You need to specify which strategy use for select the style for an item.
 * </P>
 * 
 * <PRE>
 * 
 * plugin.single.org.dspace.app.webui.util.StyleSelection = \
 *                      org.dspace.app.webui.util.CollectionStyleSelection
 *                      #org.dspace.app.webui.util.MetadataStyleSelection
 * 
 * </PRE>
 * 
 * <P>
 * With the Collection strategy you can also specify which collections use which
 * views.
 * </P>
 * 
 * <PRE>
 * 
 * webui.itemdisplay.&lt;style&gt;.collections = &lt;collection handle&gt;, ...
 * 
 * </PRE>
 * 
 * <P>
 * FIXME: This should be more database-driven
 * </P>
 * 
 * <PRE>
 * 
 * webui.itemdisplay.thesis.collections = 123456789/24, 123456789/35
 * 
 * </PRE>
 * 
 * <P>
 * With the Metadata strategy you MUST specify which metadata use as name of the
 * style.
 * </P>
 * 
 * <PRE>
 * 
 * webui.itemdisplay.metadata-style = schema.element[.qualifier|.*]
 * 
 * e.g. &quot;dc.type&quot;
 * 
 * </PRE>
 * 
 * @author Robert Tansley
 * @version $Revision$
 */
public class ItemTag extends TagSupport
{
    private static final String HANDLE_DEFAULT_BASEURL = "http://hdl.handle.net/";

    private static final String DOI_DEFAULT_BASEURL = "http://dx.doi.org/";

    /** Item to display */
    private transient Item item;

    /** Collections this item appears in */
    private transient Collection[] collections;

    /** The style to use - "default" or "full" */
    private String style;

    /** Whether to show preview thumbs on the item page */
    private boolean showThumbs;

    /** Default DC fields to display, in absence of configuration */
    private static String defaultFields = "dc.title, dc.title.alternative, dc.contributor.*, dc.subject, dc.date.issued(date), dc.publisher, dc.identifier.citation, dc.relation.ispartofseries, dc.description.abstract, dc.description, dc.identifier.govdoc, dc.identifier.uri(link), dc.identifier.isbn, dc.identifier.issn, dc.identifier.ismn, dc.identifier";

    /** log4j logger */
    private static Logger log = Logger.getLogger(ItemTag.class);

    private StyleSelection styleSelection = (StyleSelection) PluginManager.getSinglePlugin(StyleSelection.class);
    
    /** Hashmap of linked metadata to browse, from dspace.cfg */
    private static Map<String,String> linkedMetadata;
    
    /** Hashmap of urn base url resolver, from dspace.cfg */
    private static Map<String,String> urn2baseurl;
    
    /** regex pattern to capture the style of a field, ie <code>schema.element.qualifier(style)</code> */
    private Pattern fieldStylePatter = Pattern.compile(".*\\((.*)\\)");

    /** regex pattern to render geometry files **/
    private String GEOMETRY_FORMATS = "chemical/x-gromos87";
    
    private static DOIIdentifierProvider doiProvider;
    
    private static final long serialVersionUID = -3841266490729417240L;

    static {
        int i;

        linkedMetadata = new HashMap<String, String>();
        String linkMetadata;

        i = 1;
        do {
            linkMetadata = ConfigurationManager.getProperty("webui.browse.link."+i);
            if (linkMetadata != null) {
                String[] linkedMetadataSplit = linkMetadata.split(":");
                String indexName = linkedMetadataSplit[0].trim();
                String metadataName = linkedMetadataSplit[1].trim();
                linkedMetadata.put(indexName, metadataName);
            }

            i++;
        } while (linkMetadata != null);

        urn2baseurl = new HashMap<String, String>();

        String urn;
        i = 1;
        do {
            urn = ConfigurationManager.getProperty("webui.resolver."+i+".urn");
            if (urn != null) {
                String baseurl = ConfigurationManager.getProperty("webui.resolver."+i+".baseurl");
                if (baseurl != null){
                    urn2baseurl.put(urn, baseurl);
                } else {
                    log.warn("Wrong webui.resolver configuration, you need to specify both webui.resolver.<n>.urn and webui.resolver.<n>.baseurl: missing baseurl for n = "+i);
                }
            }

            i++;
        } while (urn != null);

        // Set sensible default if no config is found for doi & handle
        if (!urn2baseurl.containsKey("doi")){
            urn2baseurl.put("doi",DOI_DEFAULT_BASEURL);
        }

        if (!urn2baseurl.containsKey("hdl")){
            urn2baseurl.put("hdl",HANDLE_DEFAULT_BASEURL);
        }
        
        doiProvider = new DOIIdentifierProvider();
        doiProvider.setDOIConnector(new CrossRefDOIConnector());        
    }
    
    public ItemTag()
    {
        super();
        getThumbSettings();
    }

    public int doStartTag() throws JspException
    {
        try
        {
            if (style == null || style.equals(""))
            {
                style = styleSelection.getStyleForItem(item);
            }

            if (style.equals("full"))
            {
                renderFull();
            }
            else
            {
                render();
            }
        }
        catch (SQLException sqle)
        {
            throw new JspException(sqle);
        }
        catch (IOException ie)
        {
            throw new JspException(ie);
        }
        catch (DCInputsReaderException ex)
        {
            throw new JspException(ex);
        }


        return SKIP_BODY;
    }

    /**
     * Get the item this tag should display
     * 
     * @return the item
     */
    public Item getItem()
    {
        return item;
    }

    /**
     * Set the item this tag should display
     * 
     * @param itemIn
     *            the item to display
     */
    public void setItem(Item itemIn)
    {
        item = itemIn;
    }

    /**
     * Get the collections this item is in
     * 
     * @return the collections
     */
    public Collection[] getCollections()
    {
        return (Collection[]) ArrayUtils.clone(collections);
    }

    /**
     * Set the collections this item is in
     * 
     * @param collectionsIn
     *            the collections
     */
    public void setCollections(Collection[] collectionsIn)
    {
        collections = (Collection[]) ArrayUtils.clone(collectionsIn);
    }

    /**
     * Get the style this tag should display
     * 
     * @return the style
     */
    public String getStyle()
    {
        return style;
    }

    /**
     * Set the style this tag should display
     * 
     * @param styleIn
     *            the Style to display
     */
    public void setStyle(String styleIn)
    {
        style = styleIn;
    }

    public void release()
    {
        style = "default";
        item = null;
        collections = null;
    }

    /**
     * Render an item in the given style
     */
    private void render() throws IOException, SQLException, DCInputsReaderException
    {
        JspWriter out = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        Context context = UIUtil.obtainContext(request);
        Locale sessionLocale = UIUtil.getSessionLocale(request);
        String configLine = styleSelection.getConfigurationForStyle(style);

        if (configLine == null)
        {
            configLine = defaultFields;
        }

        listBitstreams();
        
    	out.println("<p><strong>" + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.metadata") + ":</strong></p>\n");
        out.println("<table class=\"table itemDisplayTable\">");

        /*
         * Break down the configuration into fields and display them
         * 
         * FIXME?: it may be more efficient to do some processing once, perhaps
         * to a more efficient intermediate class, but then it would become more
         * difficult to reload the configuration "on the fly".
         */
        StringTokenizer st = new StringTokenizer(configLine, ",");

        while (st.hasMoreTokens())
        {
        	String field = st.nextToken().trim();
            boolean isDate = false;
            boolean isLink = false;
            boolean isResolver = false;
            boolean isNoBreakLine = false;
            boolean isDisplay = false;

            String style = null;
            Matcher fieldStyleMatcher = fieldStylePatter.matcher(field);
            if (fieldStyleMatcher.matches()){
                style = fieldStyleMatcher.group(1);
            }
            
            String browseIndex;
            try
            {
                browseIndex = getBrowseField(field);
            }
            catch (BrowseException e)
            {
                log.error(e);
                browseIndex = null;
            }

            // Find out if the field should rendered with a particular style

            if (style != null)
            {
                isDate = style.contains("date");
                isLink = style.contains("link");
				isNoBreakLine = style.contains("nobreakline");
				isDisplay = style.equals("inputform");
                isResolver = style.contains("resolver") || urn2baseurl.keySet().contains(style);
                field = field.replaceAll("\\("+style+"\\)", "");
            } 

            // Get the separate schema + element + qualifier

            String[] eq = field.split("\\.");
            String schema = eq[0];
            String element = eq[1];
            String qualifier = null;
            if (eq.length > 2 && eq[2].equals("*"))
            {
                qualifier = Item.ANY;
            }
            else if (eq.length > 2)
            {
                qualifier = eq[2];
            }

            // check for hidden field, even if it's configured..
            if (MetadataExposure.isHidden(context, schema, element, qualifier))
            {
                continue;
            }

            // FIXME: Still need to fix for metadata language?
            Metadatum[] values = item.getMetadata(schema, element, qualifier, Item.ANY);
            
            if (values.length > 0)
            {
                out.print("<tr><td class=\"metadataFieldLabel\">");

                String label = null;
                try
                {
                    label = I18nUtil.getMessage("metadata."
                            + ("default".equals(this.style) ? "" : this.style + ".") + field,
                            context);
                }
                catch (MissingResourceException e)
                {
                    // if there is not a specific translation for the style we
                    // use the default one
                    label = LocaleSupport.getLocalizedMessage(pageContext,
                            "metadata." + field);
                }
                
                out.print(label);
                out.print(":&nbsp;</td><td class=\"metadataFieldValue\">");
                
                //If the values are in controlled vocabulary and the display value should be shown
                if (isDisplay){
                    List<String> displayValues = new ArrayList<String>();
                   

                    displayValues = Util.getControlledVocabulariesDisplayValueLocalized(item, values, schema, element, qualifier, sessionLocale);
                                
                        if (displayValues != null && !displayValues.isEmpty())
                        {
                            for (int d = 0; d < displayValues.size(); d++)
                            {
                                out.print(displayValues.get(d));
                                if (d<displayValues.size()-1)  out.print(" <br/>");
                                
                            }
                        }
                    out.print("</td>");
                    continue;
                 }   
                for (int j = 0; j < values.length; j++)
                {
                    if (values[j] != null && values[j].value != null)
                    {
                        if (j > 0)
                        {
                            if (isNoBreakLine)
                            {
                                String separator = ConfigurationManager
                                        .getProperty("webui.itemdisplay.nobreakline.separator");
                                if (separator == null)
                                {
                                    separator = ";&nbsp;";
                                }
                                out.print(separator);
                            }
                            else
                            {
                                out.print("<br />");
                            }
                        }

                        if (isLink)
                        {
                            out.print("<a href=\"" + values[j].value + "\">"
                                    + Utils.addEntities(values[j].value) + "</a>");
                        }
                        else if (isDate)
                        {
                            DCDate dd = new DCDate(values[j].value);

                            // Parse the date
                            out.print(UIUtil.displayDate(dd, false, false, (HttpServletRequest)pageContext.getRequest()));
                        }
                        else if (isResolver)
                        {
                            String value = values[j].value;
                            if (value.startsWith("http://")
                                    || value.startsWith("https://")
                                    || value.startsWith("ftp://")
                                    || value.startsWith("ftps://"))
                            {
                                // Already a URL, print as if it was a regular link
                                out.print("<a href=\"" + value + "\">"
                                        + Utils.addEntities(value) + "</a>");
                            }
                            else
                            {
                                String foundUrn = null;
                                if (!style.equals("resolver"))
                                {
                                    foundUrn = style;
                                }
                                else
                                {
                                    for (String checkUrn : urn2baseurl.keySet())
                                    {
                                        if (value.startsWith(checkUrn))
                                        {
                                            foundUrn = checkUrn;
                                        }
                                    }
                                }

                                if (foundUrn != null)
                                {

                                    if (value.startsWith(foundUrn + ":"))
                                    {
                                        value = value.substring(foundUrn.length()+1);
                                    }

                                    String url = urn2baseurl.get(foundUrn);
                                    out.print("<a href=\"" + url
                                            + value + "\">"
                                            + Utils.addEntities(values[j].value)
                                            + "</a>");
                                }
                                else
                                {
                                    out.print(value);
                                }
                            }

                        }
                        else if (browseIndex != null)
                        {
	                        String argument, value;
	                        if ( values[j].authority != null &&
	                                            values[j].confidence >= MetadataAuthorityManager.getManager()
	                                                .getMinConfidence( values[j].schema,  values[j].element,  values[j].qualifier))
	                        {
	                            argument = "authority";
	                            value = values[j].authority;
	                        }
	                        else
	                        {
	                            argument = "value";
	                            value = values[j].value;
	                        }
	                    	out.print("<a class=\"" + ("authority".equals(argument)?"authority ":"") + browseIndex + "\""
	                                                + "href=\"" + request.getContextPath() + "/browse?type=" + browseIndex + "&amp;" + argument + "="
	                    				+ URLEncoder.encode(value, "UTF-8") + "\">" + Utils.addEntities(values[j].value)
	                    				+ "</a>");
	                    }
                        else
                        {
                            out.print(Utils.addEntities(values[j].value));
                        }
                    }
                }

                out.println("</td></tr>");
            }
        }

        listCollections();

        out.println("</table><br/>");

        if (ConfigurationManager.getBooleanProperty("webui.licence_bundle.show"))
        {
            out.println("<br/><br/>");
            showLicence();
        }
    }

    /**
     * Render full item record
     */
    private void renderFull() throws IOException, SQLException
    {
        JspWriter out = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest)pageContext.getRequest();
        Context context = UIUtil.obtainContext(request);

        listBitstreams();
        
        // Get all the metadata
        Metadatum[] values = item.getMetadata(Item.ANY, Item.ANY, Item.ANY, Item.ANY);
        
        out.println("<div class=\"panel panel-info\"><div class=\"panel-heading\">"
                + LocaleSupport.getLocalizedMessage(pageContext,
                        "org.dspace.app.webui.jsptag.ItemTag.full") + "</div>");

        // Three column table - DC field, value, language
        out.println("<table class=\"panel-body table itemDisplayTable\">");
        out.println("<tr>" 
        				+ "<th id=\"s1\" class='standard'>"  + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.dcfield") + "</th>" 
                		+ "<th id=\"s2\" class='standard'>" + LocaleSupport.getLocalizedMessage(pageContext, "org.dspace.app.webui.jsptag.ItemTag.value") + "</th>" 
                		+ "<th id=\"s3\" class='standard'>" + LocaleSupport.getLocalizedMessage(pageContext, "org.dspace.app.webui.jsptag.ItemTag.lang")  + "</th>"
                	+ "</tr>");
        for (int i = 0; i < values.length; i++)
        {
            if (!MetadataExposure.isHidden(context, values[i].schema, values[i].element, values[i].qualifier))
            {
                out.print("<tr><td headers=\"s1\" class=\"metadataFieldLabel\">");
                out.print(values[i].schema);
                out.print("." + values[i].element);

                if (values[i].qualifier != null)
                {
                    out.print("." + values[i].qualifier);
                }

                out.print("</td><td headers=\"s2\" class=\"metadataFieldValue\">");
                out.print(Utils.addEntities(values[i].value));
                out.print("</td><td headers=\"s3\" class=\"metadataFieldValue\">");

                if (values[i].language == null)
                {
                    out.print("-");
                }
                else
                {
                    out.print(values[i].language);
                }

                out.println("</td></tr>");
            }
        }

        listCollections();

        out.println("</table></div><br/>");



        if (ConfigurationManager
                .getBooleanProperty("webui.licence_bundle.show"))
        {
            out.println("<br/><br/>");
            showLicence();
        }
    }

    /**
     * List links to collections if information is available
     */
    private void listCollections() throws IOException
    {
        JspWriter out = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest) pageContext
                .getRequest();

        if (collections != null)
        {
            out.print("<tr><td class=\"metadataFieldLabel\">");
            if (item.getHandle()==null)  // assume workspace item
            {
                out.print(LocaleSupport.getLocalizedMessage(pageContext,
                        "org.dspace.app.webui.jsptag.ItemTag.submitted"));
            }
            else
            {
                out.print(LocaleSupport.getLocalizedMessage(pageContext,
                          "org.dspace.app.webui.jsptag.ItemTag.appears"));
            }
            out.print("</td><td class=\"metadataFieldValue\""+
            		(style.equals("full")?"colspan=\"2\"":"")
            		+">");

            for (int i = 0; i < collections.length; i++)
            {
            	String doi = getCollectionDoi(request, collections[i]);
            	
                out.print("<a href=\"");
                out.print(request.getContextPath());
                out.print("/handle/");
                out.print(collections[i].getHandle());
                out.print("\">");
                out.print(collections[i].getMetadata("name"));
                out.print("</a>");
                if(doi!= null)
                	out.print(" - DOI: " +  "<a href='https://doi.org/" + doi + "' target='_blank'>" + doi + "</a></span>");
                out.print("<br/>");
            }

            out.println("</td></tr>");
        }
    }
    
    private String getCollectionDoi(HttpServletRequest request, Collection collection) {
        String doi = null;
        try{
        	doi = doiProvider.lookup(UIUtil.obtainContext(request), collection);
            if(doi != null)
                    doi = doi.replaceFirst("doi:", "");
        }catch(Exception e){

        }
        return doi;    
    }

    /**
     * List bitstreams in the item
     */
    private void listBitstreams() throws IOException
    {
        JspWriter out = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();        
        out.print(getMainContentDiv(request));       
    }

    private void getThumbSettings()
    {
        showThumbs = ConfigurationManager.getBooleanProperty("webui.item.thumbnail.show");
    }
 
    private String getMainContentDiv(HttpServletRequest request) throws IOException{
    	StringBuilder out = new StringBuilder();    	
    	StringBuilder bitstreamArea = new StringBuilder(); 
    	StringBuilder itemActions = new StringBuilder();
    	StringBuilder bitstreamTable = new StringBuilder();
    	StringBuilder geometry = new StringBuilder();
    	StringBuilder paperInfoTable = new StringBuilder();
    	
    	
    	bitstreamArea.append("<div class='col-lg-6 col-md-12 col-sm-12'>\n");
    	
    	bitstreamTable.append("   <div class='panel panel-info'>\n");   	
    	bitstreamTable.append("     <div class='panel-heading'>" + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.files") + "</div>\n");

        String contextPath = request.getContextPath();
        
        try{
        	Bundle[] bundles = item.getBundles("ORIGINAL");
        	boolean filesExist = false;            
            for (Bundle bnd : bundles){
            	filesExist = bnd.getBitstreams().length > 0;
            	if (filesExist){
            		break;
            	}
            }            
            // if user already has uploaded at least one file
        	if (!filesExist)        
        		bitstreamTable.append("<div class=\"panel-body\">" + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.files.no") + "</div>");        	
        	else{
        		boolean html = false;
        		boolean hasCML = false;
        		boolean hasMoldenOrbitals = false;
        		
        		String handle = item.getHandle();
        		Bitstream primaryBitstream = null;
        		Bitstream cmlFileBitstream = null;
        		Bundle[] bunds = item.getBundles("ORIGINAL");
        		Bundle[] thumbs = item.getBundles("THUMBNAIL");
        		Bundle[] geoms = item.getBundles("GEOMETRY");
        		ArrayList<Bitstream> geometries = new ArrayList<>();

        		// if item contains multiple bitstreams, display bitstream
        		// description
        		boolean multiFile = false;
        		Bundle[] allBundles = item.getBundles();

        		for (int i = 0, filecount = 0; (i < allBundles.length)
                    	&& !multiFile; i++)
        		{
        			filecount += allBundles[i].getBitstreams().length;
        			multiFile = (filecount > 1);
        		}

        		// check item is published on a journal
        		paperInfoTable = buildPaperInfoDiv(item);
        		
        		// check if primary bitstream is html
        		if (bunds[0] != null)
        		{
        			Bitstream[] bits = bunds[0].getBitstreams();

        			for (int i = 0; (i < bits.length) && !html; i++)
        			{
        				if (bits[i].getID() == bunds[0].getPrimaryBitstreamID())
        				{
        					html = bits[i].getFormat().getMIMEType().equals("text/html");
        					primaryBitstream = bits[i];
        				}
        				if (bits[i].getFormat().getMIMEType().equals("chemical/x-cml"))
                        {
                            hasCML = true;
                            cmlFileBitstream = bits[i];
                        }
        				if (bits[i].getFormat().getMIMEType().equals("chemical/x-molden"))
                        {
                            hasMoldenOrbitals = true;
                        }
        				if (bits[i].getFormat().getMIMEType().matches(GEOMETRY_FORMATS)) {
        					geometries.add(bits[i]);
        				}
        			}
        		}
        		
        		bitstreamTable.append("<table class=\"table panel-body\">" 
        						+ "<tr>" 
        						+	"<th id=\"t1\" class='standard'>" + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.file") + "</th>\n");

        		if (multiFile)
        		{
        			bitstreamTable.append("<th id=\"t2\" class='standard hidden-xs hidden-sm hidden-md hidden-lg'>" 
        						+	LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.description")
                                + "</th>\n");
        		}

        		bitstreamTable.append("<th id=\"t3\" class='standard hidden-xs hidden-sm '>"
        				 +      LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.filesize")
        				 + "</th><th id=\"t4\" class='standard hidden-xs hidden-sm '>"
        				 +      LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.fileformat")
        				 + "</th><th>&nbsp;</th>" 
        				 + "</tr>\n");
            	// if primary bitstream is html, display a link for only that one to
            	// HTMLServlet
            	if (html)
            	{
            		// If no real Handle yet (e.g. because Item is in workflow)
            		// we use the 'fake' Handle db-id/1234 where 1234 is the
            		// database ID of the item.
            		if (handle == null)
            		{
            			handle = "db-id/" + item.getID();
            		}

            		bitstreamTable.append("<tr><td headers=\"t1\" class='standard' style=\"word-break: break-all;white-space: normal;\">");
                    bitstreamTable.append("<a target=\"_blank\" href=\"");
                    bitstreamTable.append(contextPath);
                    bitstreamTable.append("/html/");
                    bitstreamTable.append(handle + "/");
                    bitstreamTable.append(UIUtil.encodeBitstreamName(primaryBitstream
                                .getName(), Constants.DEFAULT_ENCODING));
                    bitstreamTable.append("\">");
                    bitstreamTable.append(primaryBitstream.getName());
                    bitstreamTable.append("</a>");
                    
                    
            		if (multiFile)
            		{
            			bitstreamTable.append("</td><td headers=\"t2\" class='standard hidden-xs hidden-sm hidden-md hidden-lg'>");

            			String desc = primaryBitstream.getDescription();
            			bitstreamTable.append((desc != null) ? desc : "");
            		}

            		bitstreamTable.append("</td><td headers=\"t3\" class='standard hidden-xs hidden-sm '>");
                    bitstreamTable.append(UIUtil.formatFileSize(primaryBitstream.getSize()));
                    bitstreamTable.append("</td><td headers=\"t4\" class='standard hidden-xs hidden-sm '>");
            		bitstreamTable.append(primaryBitstream.getFormatDescription());
            		bitstreamTable.append("</td><td class='standard'><a class=\"btn btn-primary\" target=\"_blank\" href=\"");
            		bitstreamTable.append(contextPath);
            		bitstreamTable.append("/html/");
            		bitstreamTable.append(handle + "/");
            		bitstreamTable.append(UIUtil.encodeBitstreamName(primaryBitstream.getName(), Constants.DEFAULT_ENCODING));
            		bitstreamTable.append("\">"
            				+ LocaleSupport.getLocalizedMessage(pageContext, "org.dspace.app.webui.jsptag.ItemTag.view")
            				+ "</a></td></tr>");
            	}else if(hasCML){                    
            	    for (int i = 0; i < bundles.length; i++)
                    {
                        Bitstream[] bitstreams = bundles[i].getBitstreams();

                        for (int k = 0; k < bitstreams.length; k++)
                        {
                            // Skip internal types
                            if (!bitstreams[k].getFormat().isInternal())
                            {
                                String bsLink = buildBitstreamLink(request, handle, bitstreams[k]);

                                bitstreamTable.append("<tr><td headers=\"t1\" class='standard' style=\"word-break: break-all;white-space: normal;\">");
                                bitstreamTable.append(bitstreams[k].getName());

                                if (multiFile)
                                {
                                    bitstreamTable.append("</td><td headers=\"t2\" class='standard hidden-xs hidden-sm hidden-md hidden-lg'>");
                                    String desc = bitstreams[k].getDescription();
                                    bitstreamTable.append((desc != null) ? desc : "");
                                }

                                bitstreamTable.append("</td><td headers=\"t3\" class='standard hidden-xs hidden-sm '>");
                                bitstreamTable.append(UIUtil.formatFileSize(bitstreams[k].getSize()));
                                bitstreamTable.append("</td><td headers=\"t4\" class='standard hidden-xs hidden-sm '>");
                                bitstreamTable.append(bitstreams[k].getFormatDescription());
                                bitstreamTable.append("</td><td class='standard' align=\"left\">");

                                // is there a thumbnail bundle?
                                if ((thumbs.length > 0) && showThumbs)
                                {
                                    String tName = bitstreams[k].getName() + ".jpg";
                                    String tAltText = LocaleSupport.getLocalizedMessage(pageContext, "org.dspace.app.webui.jsptag.ItemTag.thumbnail");
                                    Bitstream tb = thumbs[0]
                                        .   getBitstreamByName(tName);

                                    if (tb != null)
                                    {
                                        String myPath = contextPath
                                                + "/retrieve/"
                                                + tb.getID()
                                                + "/"
                                                + UIUtil.encodeBitstreamName(tb.getName(),Constants.DEFAULT_ENCODING);

                                        bitstreamTable.append(bsLink);
                                        bitstreamTable.append("<img src=\"" + myPath + "\" ");
                                        bitstreamTable.append("alt=\"" + tAltText + "\" /></a><br />");
                                    }
                                }

                                
                                
                                bitstreamTable.append("<a class=\"btn btn-primary\"");
            					bitstreamTable.append(bsLink + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.view") + "</a>");                                
                                bitstreamTable.append("</td></tr>");
                            }
                        }
                    }  
                    //Html report button
            	    itemActions.append("<p><strong>Actions:</strong></p>");
            	    itemActions.append(buildViewMainDataAction(contextPath, handle, cmlFileBitstream.getName()));            	                	     
            	    itemActions.append(buildDownloadXYZAction());
            	}
            	else
            	{
            		Context context = UIUtil.obtainContext(request);
            		boolean showRequestCopy = false;
            		if ("all".equalsIgnoreCase(ConfigurationManager.getProperty("request.item.type")) || 
            				("logged".equalsIgnoreCase(ConfigurationManager.getProperty("request.item.type")) &&
            						context.getCurrentUser() != null))
					{
            			showRequestCopy = true;
					}
            		for (int i = 0; i < bundles.length; i++)
            		{
            			Bitstream[] bitstreams = bundles[i].getBitstreams();

            			for (int k = 0; k < bitstreams.length; k++)
            			{
            				// Skip internal types
            				if (!bitstreams[k].getFormat().isInternal())
            				{

                                // Work out what the bitstream link should be
                                // (persistent
                                // ID if item has Handle)
                                String bsLink = "target=\"_blank\" href=\"" + contextPath;

                                if ((handle != null)
                                        && (bitstreams[k].getSequenceID() > 0))
                                {
                                    bsLink = bsLink + "/bitstream/"
                                            + item.getHandle() + "/"
                                            + bitstreams[k].getSequenceID() + "/";
                                }
                                else
                                {
                                    bsLink = bsLink + "/retrieve/" + bitstreams[k].getID() + "/";
                                }

                                bsLink = bsLink + UIUtil.encodeBitstreamName(bitstreams[k].getName(), Constants.DEFAULT_ENCODING) + "\">";

            					bitstreamTable.append("<tr><td headers=\"t1\" class='standard' style=\"word-break: break-all;white-space: normal;\">");
                                bitstreamTable.append("<a ");
            					bitstreamTable.append(bsLink);
            					bitstreamTable.append(bitstreams[k].getName());
                                bitstreamTable.append("</a>");
                                

            					if (multiFile)
            					{
            						bitstreamTable.append("</td><td headers=\"t2\" class='standard hidden-xs hidden-sm hidden-md hidden-lg'>");
            						String desc = bitstreams[k].getDescription();
            						bitstreamTable.append((desc != null) ? desc : "");
            					}

            					bitstreamTable.append("</td><td headers=\"t3\" class='standard hidden-xs hidden-sm '>");
                                bitstreamTable.append(UIUtil.formatFileSize(bitstreams[k].getSize()));
            					bitstreamTable.append("</td><td headers=\"t4\" class='standard hidden-xs hidden-sm '>");
            					bitstreamTable.append(bitstreams[k].getFormatDescription());
            					bitstreamTable.append("</td><td class='standard' align=\"center\">");

            					// is there a thumbnail bundle?
            					if ((thumbs.length > 0) && showThumbs)
            					{
            						String tName = bitstreams[k].getName() + ".jpg";
                                    String tAltText = LocaleSupport.getLocalizedMessage(pageContext, "org.dspace.app.webui.jsptag.ItemTag.thumbnail");
            						Bitstream tb = thumbs[0]
                                        .	getBitstreamByName(tName);

            						if (tb != null)
            						{
            							String myPath = contextPath
                                            	+ "/retrieve/"
                                            	+ tb.getID()
                                            	+ "/"
                                            	+ UIUtil.encodeBitstreamName(tb
                                            			.getName(),
                                            			Constants.DEFAULT_ENCODING);

            							bitstreamTable.append("<a ");
            							bitstreamTable.append(bsLink);
            							bitstreamTable.append("<img src=\"" + myPath + "\" ");
            							bitstreamTable.append("alt=\"" + tAltText
            									+ "\" /></a><br />");
            						}
            					}

            					bitstreamTable.append("<a class=\"btn btn-primary\" ");
            					bitstreamTable.append(bsLink
                                            + LocaleSupport
                                                    .getLocalizedMessage(
                                                            pageContext,
                                                            "org.dspace.app.webui.jsptag.ItemTag.view")
                                            + "</a>");
            					
								try {
									if (showRequestCopy && !AuthorizeManager
											.authorizeActionBoolean(context,
													bitstreams[k],
													Constants.READ))
										bitstreamTable.append("&nbsp;<a class=\"btn btn-success\" href=\""
												+ contextPath
												+ "/request-item?handle="
												+ handle
												+ "&bitstream-id="
												+ bitstreams[k].getID()
												+ "\">"
												+ LocaleSupport
														.getLocalizedMessage(
																pageContext,
																"org.dspace.app.webui.jsptag.ItemTag.restrict")
												+ "</a>");
								} catch (Exception e) {
								}
								bitstreamTable.append("</td></tr>");
            				}
            			}
            		}
            	}

                if (hasCML)                            	
                {	
                	geometries.add(0, geoms[0].getBitstreams()[0]);                	
                	geometry.append(buildJsmolTabpanel(request, handle, hasMoldenOrbitals, geometries));
                }
            	
            	bitstreamTable.append("   </table>\n");
            	bitstreamTable.append("  </div>\n");
            	bitstreamTable.append("</div>\n");
            	
            	
            	// Append paper info 
            	if(!paperInfoTable.toString().equals("")) 
            		bitstreamTable.append(paperInfoTable);
            	            	
        	}
        }
        catch(SQLException sqle)
        {
        	throw new IOException(sqle.getMessage(), sqle);
        }
        
        bitstreamArea.append(itemActions);
        bitstreamArea.append("<hr/>\n");
        bitstreamArea.append(bitstreamTable);
        
        out.append("<div class='row'>");    
        out.append(geometry.toString());        
        out.append(bitstreamArea.toString());        
        out.append("</div>\n");
        return out.toString();    	
    }

    private String buildJsmolTabpanel(HttpServletRequest request, String handle, boolean hasMoldenOrbitals, ArrayList<Bitstream> geometryBitstreams) throws MalformedURLException {
    	URL baseURL = new URL(request.getScheme(),request.getServerName(),request.getServerPort(),request.getContextPath());
    	
    	//Get molecular geometry and display it on JSmol              	    	                
    	StringBuilder out = new StringBuilder();
    	out.append("<div class='col-lg-6 col-md-12 col-sm-12'>\n");
    	out.append(" <ul class='nav nav-tabs'>\n");

    	for(Bitstream bis : geometryBitstreams) {
    		boolean isFirst = geometryBitstreams.indexOf(bis) == 0;
    		String label = StringEscapeUtils.escapeHtml(isFirst? LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.jmolView") : bis.getName());;
    		String url = getGeometryRetrieveUrl(request, handle, bis);
    		out.append("    <li class='" + (isFirst ? "active" : "") + "'>\n");
    		out.append("      <a data-toggle='tab' class='geometryTab' href='#jsmolView' title='" + label + "' url='" + url +"'>" + label + "</a>\n");
    		out.append("    </li>\n");
    	}
    	if(hasMoldenOrbitals){
    		out.append("    <li>\n");
    		out.append("      <a data-toggle='tab' href='#moldenView'>" + LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.moldenView") + "</a>\n");
        	out.append("    </li>\n");
    	}
    	out.append("  </ul>\n");
    	out.append("<div class='tab-content'>");
        out.append(" <div id='jsmolView' class='tab-pane fade in active'>");
        out.append("   <div id='jsmolPlaceholderDiv'>");        	
    	out.append("        <script type='text/javascript' src='" + baseURL.toString() + "/xslt/jsmol/JSmol.min.nojq.js'></script>\n");
    	out.append("        <script type='text/javascript' src='" + baseURL.toString() + "/static/js/display-item.js'></script>");
        out.append("   </div>");
        out.append("     <div><p><strong>Code snippets</strong></p></div> ");
        out.append("        <div class='codeSnippetsDiv'>");
        out.append("          <div id='htmlSnippetDiv' class='codeSnippetDiv' title='Get entire HTML page code snippet'><a href='#' onclick='showHtmlCodeSnippet()'><img class='code-snippet-logo' src='" + request.getContextPath() + "/image/html_logo.jpg' ></img></a></div>");
        out.append("          <div id='authoreaSnippetDiv' class='codeSnippetDiv' title='Get Authorea code snippet'><a href='#' onclick='showAuthoreaCodeSnippet()'><img class='code-snippet-logo' src='" + request.getContextPath() + "/image/authorea_logo.jpg' ></img></a></div>");
        out.append("        </div>");
        out.append("     </div>");        
        
        if(hasMoldenOrbitals)        	        
            out.append(showMoldenJmol(request, handle));            
             
        out.append("</div>");    	
        out.append("</div>");
    	return out.toString();		
	}

	private String buildViewMainDataAction(String contextPath, String handle, String filename) {
		StringBuilder out = new StringBuilder();
		try{	    	
	        out.append("<a class='btn btn-default btn-lg btn-block' target='_blank' href='");
	        out.append(contextPath);
	        out.append("/cml2html/");
	        out.append(handle + "/");
	        out.append(UIUtil.encodeBitstreamName(filename, Constants.DEFAULT_ENCODING));
	        out.append("'>");
	        out.append(LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.html5resume") + "</a>");			
		}catch(UnsupportedEncodingException ex){
			out.setLength(0);
		}		    	
        return out.toString();
	}
	
    private String buildDownloadXYZAction() {
    	StringBuilder out = new StringBuilder();
    	out.append("<a class='btn btn-default btn-lg btn-block' href='#' onclick='javascript:itemViewDisplayXYZ()'>");
        out.append(LocaleSupport.getLocalizedMessage(pageContext,"org.dspace.app.webui.jsptag.ItemTag.getxyz"));    	    	
    	out.append("</a>");    	
		return out.toString();
	}

    private StringBuilder buildPaperInfoDiv(Item item) {
    	StringBuilder out = new StringBuilder();    	
    	List<Metadatum> manuscriptInfo = item.getMetadata("dc","relation", null, Item.ANY, Item.ANY);
  		if(manuscriptInfo.size() > 0 && manuscriptInfo.get(0).value.startsWith("Original title")) {
		  	String paperMetadata = manuscriptInfo.get(0).value.replaceAll("\\n", " ").trim();
		    String title = paperMetadata.replaceAll(CollectionEditServlet.TITLE_REGEX,"").trim();
		    String paperDoi = paperMetadata.replaceAll(CollectionEditServlet.DOI_REGEX,"").trim();
		    String journal = paperMetadata.replaceAll(CollectionEditServlet.JOURNAL_REGEX, "").trim();
		    		    
		    out.append("<div class='col-lg-6 col-md-12'>");
		    out.append("    <div class='panel panel-info'>");
		    out.append("       <div class='panel-heading'>Referenced by:</div>");
		    out.append("       <div class=' panel-body'>");		    		      			
			if(!title.equals(""))  
					out.append("		<p class='collectionJournalLine'><span style='font-style:oblique'>Manuscript title</span>: " + title + "</p>");
			if(!journal.equals(""))  
					out.append("		<p class='collectionJournalLine'><span style='font-style:oblique'>Journal</span>: " + journal + "</p>");					
 			if(!paperDoi.equals(""))  						  						  	
					out.append("		<p class='collectionJournalLine'><span style='font-style:oblique'>DOI</span>: <a href='https://doi.org/" + paperDoi + "' target='_blank'>" + paperDoi + "</a></p>");
         	out.append("		</div>");
         	out.append("	</div>");
         	out.append("</div>");         	         	
  		}
    	return out;
    }
    

	/**
     * Link to the item licence
     */
    private void showLicence() throws IOException
    {
        JspWriter out = pageContext.getOut();
        HttpServletRequest request = (HttpServletRequest) pageContext.getRequest();

        Bundle[] bundles = null;
        try
        {
        	bundles = item.getBundles("LICENSE");
        }
        catch(SQLException sqle)
        {
        	throw new IOException(sqle.getMessage(), sqle);
        }

        out.println("<table align=\"center\" class=\"table attentionTable\"><tr>");

        out.println("<td class=\"attentionCell\"><p><strong>"
                + LocaleSupport.getLocalizedMessage(pageContext,
                        "org.dspace.app.webui.jsptag.ItemTag.itemprotected")
                + "</strong></p>");

        for (int i = 0; i < bundles.length; i++)
        {
            Bitstream[] bitstreams = bundles[i].getBitstreams();

            for (int k = 0; k < bitstreams.length; k++)
            {
                out.print("<div align=\"center\" class='standard'>");
                out.print("<strong><a class=\"btn btn-primary\" target=\"_blank\" href=\"");
                out.print(request.getContextPath());
                out.print("/retrieve/");
                out.print(bitstreams[k].getID() + "/");
                out.print(UIUtil.encodeBitstreamName(bitstreams[k].getName(),
                        Constants.DEFAULT_ENCODING));
                out
                        .print("\">"
                                + LocaleSupport
                                        .getLocalizedMessage(pageContext,
                                                "org.dspace.app.webui.jsptag.ItemTag.viewlicence")
                                + "</a></strong></div>");
            }
        }

        out.println("</td></tr></table>");
    }

    /**
     * Return the browse index related to the field. <code>null</code> if the field is not a browse field
     * (look for <cod>webui.browse.link.<n></code> in dspace.cfg) 
     * 
     * @param field
     * @return the browse index related to the field. Null otherwise 
     * @throws BrowseException 
     */
    private String getBrowseField(String field) throws BrowseException
    {
        for (String indexName : linkedMetadata.keySet())
        {            
            StringTokenizer bw_dcf = new StringTokenizer(linkedMetadata.get(indexName), ".");
            
            String[] bw_tokens = { "", "", "" };
            int i = 0;
            while(bw_dcf.hasMoreTokens())
            {
                bw_tokens[i] = bw_dcf.nextToken().toLowerCase().trim();
                i++;
            }
            String bw_schema = bw_tokens[0];
            String bw_element = bw_tokens[1];
            String bw_qualifier = bw_tokens[2];
            
            StringTokenizer dcf = new StringTokenizer(field, ".");
            
            String[] tokens = { "", "", "" };
            int j = 0;
            while(dcf.hasMoreTokens())
            {
                tokens[j] = dcf.nextToken().toLowerCase().trim();
                j++;
            }
            String schema = tokens[0];
            String element = tokens[1];
            String qualifier = tokens[2];
            if (schema.equals(bw_schema) // schema match
                    && element.equals(bw_element) // element match
                    && (
                              (bw_qualifier != null) && ((qualifier != null && qualifier.equals(bw_qualifier)) // both not null and equals 
                                      || bw_qualifier.equals("*")) // browse link with jolly
                           || (bw_qualifier == null && qualifier == null)) // both null
                        )
            {
                return indexName;
            }
        }
        return null;
    }
    

    
    private String buildBitstreamLink(HttpServletRequest request,String handle, Bitstream bitstream) throws UnsupportedEncodingException
    {
    	// Work out what the bitstream link should be (persistent ID if item has Handle)
    	StringBuilder bsLink = new StringBuilder("<a target=\"_blank\" href=\"" + request.getContextPath());    	
        if ((handle != null) && (bitstream.getSequenceID() > 0))
            bsLink.append("/bitstream/" + item.getHandle() + "/"  + bitstream.getSequenceID() + "/");
        else
            bsLink.append("/retrieve/" + bitstream.getID() + "/");
        bsLink.append(UIUtil.encodeBitstreamName(bitstream.getName(),Constants.DEFAULT_ENCODING) + "\">");
        return bsLink.toString();
    }
    
    
    private String getGeometryRetrieveUrl(HttpServletRequest request, String handle, Bitstream cmlFileBitstream) 
    {
    	try {
    		StringBuilder link = new StringBuilder();
        	link.append("load ");
        	link.append(request.getContextPath());
            if ((handle != null) && cmlFileBitstream.getSequenceID() > 0)
           	 link.append("/bitstream/" + item.getHandle() + "/" + cmlFileBitstream.getSequenceID() + "/");
            else
                link.append( "/retrieve/" + cmlFileBitstream.getID() + "/");
            link.append(UIUtil.encodeBitstreamName(cmlFileBitstream.getName(), Constants.DEFAULT_ENCODING));
            link.append(";");
            return link.toString();	
    	}catch(Exception e) {
    		return "";
    	}    	
    }
    
    

    private String showMoldenJmol(HttpServletRequest request, String handle) {   
    	StringBuilder out = new StringBuilder();
    	try {
    		URL reconstructedURL = new URL(request.getScheme(),request.getServerName(),request.getServerPort(),request.getContextPath());
    		out.append("<div id='moldenView' class='tab-pane fade in'>");            
    		out.append("	<iframe id='moldenIframe' src='" + reconstructedURL.toString() + "/xslt/jsmol/jsmol-molden.html?handle=" + handle + "'></iframe>\n");
    		out.append("</div>");
    	}catch(Exception e) {
    		e.printStackTrace();
    	}    	                
        return out.toString();
    }    
    
}
