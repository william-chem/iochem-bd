/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * This file incorporates work covered by the following copyright and
 * permission notice:
 *
 *
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */
package org.dspace.app.webui.servlet.admin;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Locale;

import javax.mail.MessagingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.dspace.app.webui.servlet.DSpaceServlet;
import org.dspace.app.webui.util.Authenticate;
import org.dspace.app.webui.util.EpersonCSV;
import org.dspace.app.webui.util.JSPManager;
import org.dspace.app.webui.util.UIUtil;
import org.dspace.authenticate.AuthenticationManager;
import org.dspace.authorize.AuthorizeException;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.content.Community;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Context;
import org.dspace.core.I18nUtil;
import org.dspace.core.LogManager;
import org.dspace.eperson.AccountManager;
import org.dspace.eperson.EPerson;
import org.dspace.eperson.EPersonDeletionException;
import org.dspace.eperson.Group;

/**
 * Servlet for editing and creating e-people
 * 
 * @author David Stuve
 * @version $Revision$
 */
public class EPersonAdminServlet extends DSpaceServlet
{
        
    /** Logger */
    private static Logger log = Logger.getLogger(EPersonAdminServlet.class);
    
    protected void doDSGet(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {
        showMain(context, request, response);
    }

    protected void doDSPost(Context context, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {
        String button = UIUtil.getSubmitButton(request, "submit");

        if (button.equals("submit_add"))
        {
            // add an EPerson, then jump user to edit page
            EPerson e = EPerson.create(context);

            // create clever name and do update before continuing
            e.setEmail("newuser" + e.getID());
            e.setCanLogIn(true);
            e.update();

            request.setAttribute("is.new.eperson", true);
            request.setAttribute("eperson", e);

            JSPManager.showJSP(request, response,
                    "/dspace-admin/eperson-edit.jsp");

            context.complete();
        }
        else if (button.equals("submit_csv_add"))
        {        
            JSPManager.showJSP(request, response, "/dspace-admin/eperson-csv-add.jsp");            
        }
        else if(button.equals("submit_csv_save"))
        {
        	String epersonCsv = request.getParameter("csvfields");
        	String generateCommunities = request.getParameter("generateCommunities");
        	String sendResetPasswordEmails = request.getParameter("sendResetPasswordEmails");
            request.setAttribute("eperson_csv", epersonCsv);
            request.setAttribute("generate_communities", generateCommunities);
            request.setAttribute("send_reset_emails", sendResetPasswordEmails);
            JSPManager.showJSP(request, response, "/dspace-admin/eperson-csv-save.jsp");        	
        }
        else if (button.equals("submit_edit"))
        {
            // edit an eperson
            EPerson e = EPerson.find(context, UIUtil.getIntParameter(request,
                    "eperson_id"));
            
            // Check the EPerson exists
            if (e == null)
            {
            	request.setAttribute("no_eperson_selected", Boolean.TRUE);
            	showMain(context, request, response);
            }
            else 
            {            
	            // what groups is this person a member of?
	            Group[] groupMemberships = Group.allMemberGroups(context, e);
	            Community[] publishAllowCommunities = getPublishAllowedCommunities(context, e);
	
	            request.setAttribute("eperson", e);
	            request.setAttribute("group.memberships", groupMemberships);
	            request.setAttribute("publish.communities", publishAllowCommunities); 
	
	            JSPManager.showJSP(request, response,
	                    "/dspace-admin/eperson-edit.jsp");
	
	            context.complete();
            }
        }
        else if (button.equals("submit_request_username")) {
        	String firstName = request.getParameter("firstname");
        	String lastName = request.getParameter("lastname");
        	try {
				String suggestedName = EpersonCSV.buildUsername(context, firstName, lastName);
				response.getWriter().print(suggestedName);
				response.flushBuffer();
			} catch (Exception e) {
				response.sendError(406);
			}        	
        }
        else if (button.equals("submit_save") || button.equals("submit_resetpassword"))
        {
            // Update the metadata for an e-person
            EPerson e = EPerson.find(context, UIUtil.getIntParameter(request, "eperson_id"));

            // see if the user changed the email - if so, make sure the new email is unique
            String oldEmail = e.getEmail();
            String newEmail = request.getParameter("email").trim();
            String netid = newEmail;
            String newUsername 	= request.getParameter("username").trim();
            String mainGroupId	= request.getParameter("maingroup");     
            String epersonGroups[] = request.getParameterValues("susergroups");
            String epersonCommunities[] = request.getParameterValues("susercommunities");
            String passwordAction = request.getParameter("passwordAction");
            
            if (!newEmail.equals(oldEmail))
            {
                // change to email, now see if it's unique
                if (EPerson.findByEmail(context, newEmail) == null)
                {
                    // it's unique - proceed!
                    e.setEmail(newEmail);

                    e
                            .setFirstName(request.getParameter("firstname")
                                    .equals("") ? null : request
                                    .getParameter("firstname"));

                    e
                            .setLastName(request.getParameter("lastname")
                                    .equals("") ? null : request
                                    .getParameter("lastname"));

                    if (netid != null)
                    {
                        e.setNetid(netid.equals("") ? null : netid.toLowerCase());
                    }
                    else
                    {
                        e.setNetid(null);
                    }

                                                                       
                    if (EPerson.findByUsername(context, newUsername) == null || (EPerson.findByUsername(context, newUsername)).getID() == e.getID()) //New user or replacing current                        														
                    {                               	
                    	attachUserToGroups(context, e, epersonGroups);
                    	setUserAsCommunityAdministrator(context, e, epersonCommunities);
                    	e.setCanLogIn((request.getParameter("can_log_in") != null) && request.getParameter("can_log_in").equals("true"));
                    	e.setMainGroupId(mainGroupId.equals("") || !e.canLogIn() ? null : mainGroupId);                    	
                    	e.setUsername(newUsername.equals("") ? null : newUsername);                    	 
	                    e.setMetadata("phone", request.getParameter("phone").equals("") ? null : request.getParameter("phone"));
	                    e.setMetadata("language", request.getParameter("language").equals("") ? null : request.getParameter("language"));                    	                    
	                    e.setRequireCertificate((request.getParameter("require_certificate") != null) && request.getParameter("require_certificate").equals("true"));	                    
	                    if(passwordAction.equals("saveAutogenerated"))
	                    	e.setPassword(request.getParameter("password"));
	                    e.update();
	                    
	                	try {
	                		if(passwordAction.equals("sendEmail"))
	                			resetPassword(context, request, response, e); 
                    	} catch (MessagingException e1){
                    		 JSPManager
                             		.showJSP(request, response,
                             				"/dspace-admin/eperson-resetpassword-error.jsp");
                    		 return;
                    	}
	                    
	                    showMain(context, request, response);
	                    context.complete();
                    }
                    else
                    {
                    	// not unique - send error message & let try again
                        request.setAttribute("eperson", e);
                        request.setAttribute("username_exists", Boolean.TRUE);
                        JSPManager.showJSP(request, response, "/dspace-admin/eperson-edit.jsp");
                        context.complete();	
                    }
                    
                    if (button.equals("submit_resetpassword"))
                    {                        
                        try
                        {
                            resetPassword(context, request, response, e);
                        }
                        catch (MessagingException e1)
                        {
                            JSPManager
                                    .showJSP(request, response,
                                            "/dspace-admin/eperson-resetpassword-error.jsp");
                            return;
                        }
                    }
                }
                else
                {
                    // not unique - send error message & let try again
                    request.setAttribute("eperson", e);
                    request.setAttribute("email_exists", Boolean.TRUE);

                    JSPManager.showJSP(request, response,
                            "/dspace-admin/eperson-edit.jsp");

                    context.complete();
                }
            }
            else
            {
            	if (EPerson.findByUsername(context, newUsername) == null || (EPerson.findByUsername(context, newUsername)).getID() == e.getID()) //New user or replacing current
                {
	                // no change to email
	                if (netid != null)
	                {
	                    e.setNetid(netid.equals("") ? null : netid.toLowerCase());
	                }
	                else
	                {
	                    e.setNetid(null);
	                }
	
	                e
	                        .setFirstName(request.getParameter("firstname").equals(
	                                "") ? null : request.getParameter("firstname"));
	
	                e
	                        .setLastName(request.getParameter("lastname")
	                                .equals("") ? null : request
	                                .getParameter("lastname"));
	
	                
	                attachUserToGroups(context, e, epersonGroups);
	                setUserAsCommunityAdministrator(context, e, epersonCommunities);
	                
	                e.setCanLogIn((request.getParameter("can_log_in") != null)
	                        && request.getParameter("can_log_in").equals("true"));
	                
	                e.setMainGroupId(mainGroupId.equals("") || !e.canLogIn() ? null : mainGroupId);
	                
                	e.setUsername(newUsername.equals("") ? null : newUsername);
	                e.setMetadata("phone",
	                        request.getParameter("phone").equals("") ? null
	                                : request.getParameter("phone"));
	                
	                e.setMetadata("language", request.getParameter("language")
	                        .equals("") ? null : request.getParameter("language"));
	                         
	                
	                e.setRequireCertificate((request
	                        .getParameter("require_certificate") != null)
	                        && request.getParameter("require_certificate").equals(
	                                "true"));
	                
	                if(passwordAction.equals("saveAutogenerated"))
                    	e.setPassword(request.getParameter("password"));	                                       
                    
	                e.update();
	
	                if (button.equals("submit_resetpassword") || passwordAction.equals("sendEmail"))
	                {
	                    try
	                    {
	                        resetPassword(context, request, response, e);
	                    }
	                    catch (MessagingException e1)
	                    {
	                        JSPManager
	                                .showJSP(request, response,
	                                        "/dspace-admin/eperson-resetpassword-error.jsp");
	                        return;
	                    }                   
	                }
	                
	                showMain(context, request, response);
	                context.complete();
                } 
                else
                {
                	// not unique - send error message & let try again
                    request.setAttribute("eperson", e);
                    request.setAttribute("username_exists", Boolean.TRUE);
                    JSPManager.showJSP(request, response, "/dspace-admin/eperson-edit.jsp");
                    context.complete();
                }                                
            }
        }
        else if (button.equals("submit_delete"))
        {
            // Start delete process - go through verification step
            EPerson e = EPerson.find(context, UIUtil.getIntParameter(request,
                    "eperson_id"));
            
            // Check the EPerson exists
            if (e == null)
            {
            	request.setAttribute("no_eperson_selected", Boolean.TRUE);
            	showMain(context, request, response);
            }
            else 
            {       
	            request.setAttribute("eperson", e);
	
	            JSPManager.showJSP(request, response,
	                    "/dspace-admin/eperson-confirm-delete.jsp");
            }
        }
        else if (button.equals("submit_confirm_delete"))
        {
            // User confirms deletion of type
            EPerson e = EPerson.find(context, UIUtil.getIntParameter(request,
                    "eperson_id"));

            try
            {
                e.delete();
            }
            catch (EPersonDeletionException ex)
            {
                request.setAttribute("eperson", e);
                request.setAttribute("tableList", ex.getTables());
                JSPManager.showJSP(request, response,
                        "/dspace-admin/eperson-deletion-error.jsp");
            }

            showMain(context, request, response);
            context.complete();
        }
        else if (button.equals("submit_login_as"))
        {
            if (!ConfigurationManager.getBooleanProperty("webui.user.assumelogin", false))
            {
                throw new AuthorizeException("Turn on webui.user.assumelogin to activate Login As feature");                
            }
            EPerson e = EPerson.find(context, UIUtil.getIntParameter(request,
                    "eperson_id"));
            // Check the EPerson exists
            if (e == null)
            {
                request.setAttribute("no_eperson_selected", new Boolean(true));
                showMain(context, request, response);
            }
            // Only super administrators can login as someone else.
            else if (!AuthorizeManager.isAdmin(context))
            {                
                throw new AuthorizeException("Only site administrators may assume login as another user.");
            }
            else
            {
                
                log.info(LogManager.getHeader(context, "login-as",
                        "current_eperson="
                                + context.getCurrentUser().getFullName()
                                + ", id=" + context.getCurrentUser().getID()
                                + ", as_eperson=" + e.getFullName() + ", id="
                                + e.getID()));
                
                // Just to be double be sure, make sure the administrator
                // is the one who actually authenticated himself.
                HttpSession session = request.getSession(false);
                Integer authenticatedID = (Integer) session.getAttribute("dspace.current.user.id"); 
                if (context.getCurrentUser().getID() != authenticatedID)
                {                                         
                    throw new AuthorizeException("Only authenticated users who are administrators may assume the login as another user.");                    
                }
                
                // You may not assume the login of another super administrator
                Group administrators = Group.find(context,1);
                if (administrators.isMember(e))
                {                    
                    JSPManager.showJSP(request, response,
                            "/dspace-admin/eperson-loginas-error.jsp");
                    return;
                }
                               
                // store a reference to the authenticated admin
                session.setAttribute("dspace.previous.user.id", authenticatedID);
                
                // Logged in OK.
                Authenticate.loggedIn(context, request, e);

                // Set the Locale according to user preferences
                Locale epersonLocale = I18nUtil.getEPersonLocale(context
                        .getCurrentUser());
                context.setCurrentLocale(epersonLocale);
                Config.set(request.getSession(), Config.FMT_LOCALE,
                        epersonLocale);

                // Set any special groups - invoke the authentication mgr.
                int[] groupIDs = AuthenticationManager.getSpecialGroups(
                        context, request);

                for (int i = 0; i < groupIDs.length; i++)
                {
                    context.setSpecialGroup(groupIDs[i]);
                    log.debug("Adding Special Group id="
                            + String.valueOf(groupIDs[i]));
                }
                response.sendRedirect(request.getContextPath() + "/mydspace");
            }
        }        
        else
        {
            // Cancel etc. pressed - show list again
            showMain(context, request, response);
        }
    }

    private Community[] getPublishAllowedCommunities(Context context, EPerson eperson) {
		ArrayList<Community> communities = new ArrayList<Community>();
    	try {
			for(Community community : Community.findAll(context)) {
				addCommunityAdministratorGroup(context, community);
				if(community.getAdministrators().isMember(eperson))
						communities.add(community);
			}
			return communities.toArray(new Community[communities.size()]);
		} catch (SQLException  e) {			
			e.printStackTrace();
			return new Community[0];
		}        			
	}

	private void attachUserToGroups(Context context, EPerson eperson, String groupsIdStr[]) {
    	try {
    		if(groupsIdStr.length != 0) {
    			removeUsersFromAllCommonGroups(context, eperson);
    			for(String groupIdStr: groupsIdStr) {
    				int groupId = Integer.parseInt(groupIdStr);
    				Group group = Group.find(context, groupId);
    				if(!group.isMember(eperson)) {
            			group.addMember(eperson);
                		group.update();	
            		}
    			}
        	}	
    	}catch(Exception e) {
    		log.error("Unable to attach eperson to main group: " + e.getMessage());
    	}    	
    }
    
    private void removeUsersFromAllCommonGroups(Context context, EPerson eperson) {    	
    	try {
			Group[] groups = Group.allMemberGroups(context, eperson);
			for(Group group : groups) {
				if(group.getName().startsWith("COLLECTION") || 
						group.getName().startsWith("COMMUNITY") || 
						group.getName().equals("Anonymous")  || 
						group.getName().equals("Administrator") ||
						group.getName().equals("Reviewers"))
	        		continue;
				if(group.isMember(eperson)) {
        			group.removeMember(eperson);
            		group.update();	
        		}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (AuthorizeException e) {
			e.printStackTrace();
		}    
    }
    
    private void setUserAsCommunityAdministrator(Context context, EPerson eperson, String epersonCommunities[]) {
    	try {
    		for(Community community : Community.findAll(context)) {
    			addCommunityAdministratorGroup(context, community);    			    		
				if(community.getAdministrators().isMember(eperson) && !ArrayUtils.contains(epersonCommunities, String.valueOf(community.getID()))) {
					community.getAdministrators().removeMember(eperson);
					community.getAdministrators().update();
				}else if(!community.getAdministrators().isMember(eperson) && ArrayUtils.contains(epersonCommunities, String.valueOf(community.getID()))) {
					community.getAdministrators().addMember(eperson);
					community.getAdministrators().update();
				}		
			}
		} catch (SQLException | AuthorizeException e) {			
			e.printStackTrace();
		}    	
    }

	private void addCommunityAdministratorGroup(Context context, Community community) {
		try {
			if(community.getAdministrators() == null) {
				community.createAdministrators();
				community.update();	
			}						
		}catch(Exception e) {
			
		}
	}

	private void resetPassword(Context context, HttpServletRequest request,
            HttpServletResponse response, EPerson e) throws SQLException,
            IOException, AuthorizeException, ServletException,
            MessagingException
    {
        // Note, this may throw an error is the email is bad.
        AccountManager.sendForgotPasswordInfo(context, e.getEmail());
        request.setAttribute("reset_password", Boolean.TRUE);
    }

    private void showMain(Context c, HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException,
            SQLException, AuthorizeException
    {
        JSPManager.showJSP(request, response, "/dspace-admin/eperson-main.jsp");
    }
}
