/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.app.webui.jsptag;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.StringTokenizer;
import java.util.TreeMap;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocaleSupport;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.commons.lang.StringEscapeUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dspace.app.webui.util.UIUtil;
import org.dspace.authorize.AuthorizeManager;
import org.dspace.browse.BrowseException;
import org.dspace.browse.BrowseIndex;
import org.dspace.browse.CrossLinks;
import org.dspace.content.Bitstream;
import org.dspace.content.Collection;
import org.dspace.content.DCDate;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.Metadatum;
import org.dspace.content.Thumbnail;
import org.dspace.content.authority.MetadataAuthorityManager;
import org.dspace.content.service.ItemService;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.core.Context;
import org.dspace.core.Utils;

/**
 * Tag for displaying a list of items in the form of a tree.
 * It will be a master-detail tree, navigate through the tree and choosing a leaf will display its contained items.
 * It will use jquery datatables library to render leaf content along with its scroll plugin. It will be useful on leafs 
 * with multiple children items so it will render only a small section of the results, only the viewpoint. 
 * @author malvarez
 *
 */
public class TreeListTag extends TagSupport{

	/** log4j category */
    private static Logger log = Logger.getLogger(BrowseListTag.class);

    /** Collection to display */
    private Collection collection;

	private int parentIndex = 1;
    private List<String> children;
    private HashMap<Integer, Integer> calculationCount;
    private int firstChildWithContent = 0;
    private boolean rootHasContent = false;
    
    /** Config value of thumbnail view toggle */
    private static boolean showThumbs;

    /** Config browse/search width and height */
    private static int thumbItemListMaxWidth;

    private static int thumbItemListMaxHeight;
    
    /** The default field which is bound to the browse by date */
    private static String dateField = "dc.date.issued";
    
    /** The default fields to be displayed when listing items */
    private static final String DEFAULT_LIST_FIELDS;

    /** The default widths for the columns */
    private static final String DEFAULT_LIST_WIDTHS;
    
    /** The default field which is bound to the browse by title */
    private static String titleField = "dc.title";

    private static String authorField = "dc.contributor.*";
        
    // Arrays used to hold the information we will require when outputting each row
    private static boolean areHeadersLoaded = false;
    private static String[] fieldArr;
    private static String[] widthArr;
    private static boolean isDate[];
    private static boolean isBoolean[];
    private static boolean isHidden[];   
    private static boolean isAuthor[];
    private static boolean viewFull[];
    private static String[] browseType;
    private static String[] cOddOrEven;

	private static List<String> columns;
	private static List<String> columnDefs;
	private static HashMap<String, String> columnTranslate;	
    private int authorLimit = -1;

	public Collection getCollection() {
		return collection;
	}

	public void setCollection(Collection collection) {
		this.collection = collection;
	}
	
    public int getAuthorLimit() {
        return authorLimit;
    }

    public void setAuthorLimit(int al) {
        authorLimit = al;
    }
    
    static {
        getThumbSettings();        
        if (showThumbs)
        {
            DEFAULT_LIST_FIELDS = "thumbnail, dc.date.issued(date), dc.title, dc.contributor.*";
            DEFAULT_LIST_WIDTHS = "*, 130, 60%, 40%";
        }
        else
        {
            DEFAULT_LIST_FIELDS = "dc.date.issued(date), dc.title, dc.contributor.*";
            DEFAULT_LIST_WIDTHS = "130, 60%, 40%";
        }

        // get the date and title fields
        String dateLine = ConfigurationManager.getProperty("webui.browse.index.date");
        if (dateLine != null)
        {
            dateField = dateLine;
        }

        String titleLine = ConfigurationManager.getProperty("webui.browse.index.title");
        if (titleLine != null)
        {
            titleField = titleLine;
        }

        // get the author truncation config
        String authorLine = ConfigurationManager.getProperty("webui.browse.author-field");
        if (authorLine != null)
        {
        	authorField = authorLine;
        }       
        loadConfiguration();
    }

    private static void getThumbSettings() {
        showThumbs = ConfigurationManager.getBooleanProperty("webui.treelist.thumbnail.show");
        if (showThumbs) {
            thumbItemListMaxHeight = ConfigurationManager.getIntProperty("webui.treelist.thumbnail.maxheight");
            if (thumbItemListMaxHeight == 0) 
                thumbItemListMaxHeight = ConfigurationManager.getIntProperty("thumbnail.maxheight");
            thumbItemListMaxWidth = ConfigurationManager.getIntProperty("webui.treelist.thumbnail.maxwidth");
            if (thumbItemListMaxWidth == 0)
            	thumbItemListMaxWidth = ConfigurationManager.getIntProperty("thumbnail.maxwidth");            
        }        
    }

    private static void loadConfiguration() {    	 
        // get the elements to display
        String treeListColumns  = ConfigurationManager.getProperty("webui.treelist.columns");
        String treeListWidths = ConfigurationManager.getProperty("webui.treelist.widths");        
        if (treeListColumns != null)
        {
            // If thumbnails are disabled, strip out any thumbnail column from the configuration
            if (!showThumbs && treeListColumns.contains("thumbnail"))
            {
                // Ensure we haven't got any nulls
                treeListColumns  = treeListColumns  == null ? "" : treeListColumns;
                treeListWidths = treeListWidths == null ? "" : treeListWidths;

                // Tokenize the field and width lines
                StringTokenizer tlct = new StringTokenizer(treeListColumns,  ",");
                StringTokenizer tlwt = new StringTokenizer(treeListWidths, ",");

                StringBuilder newBLLine = new StringBuilder();
                StringBuilder newBWLine = new StringBuilder();
                while (tlct.hasMoreTokens() || tlwt.hasMoreTokens())
                {
                    String treeListTok  = tlct.hasMoreTokens() ? tlct.nextToken() : null;
                    String treeWidthTok = tlwt.hasMoreTokens() ? tlwt.nextToken() : null;

                    // Only use the Field and Width tokens, if the field isn't 'thumbnail'
                    if (treeListTok == null || !treeListTok.trim().equals("thumbnail"))
                    {
                        if (treeListTok != null)
                        {
                            if (newBLLine.length() > 0)
                            {
                                newBLLine.append(",");
                            }

                            newBLLine.append(treeListTok);
                        }

                        if (treeWidthTok != null)
                        {
                            if (newBWLine.length() > 0)
                            {
                                newBWLine.append(",");
                            }

                            newBWLine.append(treeWidthTok);
                        }
                    }
                }

                // Use the newly built configuration file
                treeListColumns  = newBLLine.toString();
                treeListWidths = newBWLine.toString();
            }
        }
        else
        {
            treeListColumns  = DEFAULT_LIST_FIELDS;
            treeListWidths = DEFAULT_LIST_WIDTHS;
        }
        
        
        // Get the interlinking configuration too
        CrossLinks cl = null;
		try {
			cl = new CrossLinks();
		} catch (BrowseException e1) {}

        // Arrays used to hold the information we will require when outputting each row
        fieldArr = treeListColumns == null  ? new String[0] : treeListColumns.split("\\s*,\\s*");
        widthArr = treeListWidths == null ? new String[0] : treeListWidths.split("\\s*,\\s*");
        isDate = new boolean[fieldArr.length];
        isBoolean = new boolean[fieldArr.length];
        isHidden  = new boolean[fieldArr.length];
        isAuthor = new boolean[fieldArr.length];
        cOddOrEven = new String[fieldArr.length];
        viewFull = new boolean[fieldArr.length];
        browseType = new String[fieldArr.length];
        
        for (int colIdx = 0; colIdx < fieldArr.length; colIdx++)
        {
            String field = fieldArr[colIdx].toLowerCase().trim();
            cOddOrEven[colIdx] = (((colIdx + 1) % 2) == 0 ? "Odd" : "Even");

            // find out if the field is a date
            if (field.indexOf("(date)") > 0)
            {
                field = field.replaceAll("\\(date\\)", "");
                isDate[colIdx] = true;
            }
            // find out if the field is a boolean value
            if (field.indexOf("(boolean)") > 0)
            {
                field = field.replaceAll("\\(boolean\\)", "");
                isBoolean[colIdx] = true;
            }
            // find out if the field starts being hidden                                
            if (field.indexOf("(hidden)") > 0)
            {
                field = field.replaceAll("\\(hidden\\)", "");
                isHidden[colIdx] = true;
            }                
            // Cache any modifications to field
            fieldArr[colIdx] = field;

            // find out if this is the author column
            if (field.equals(authorField))
            {
            	isAuthor[colIdx] = true;
            }          
            // find out if this field needs to link out to other browse views
            if (cl != null && cl.hasLink(field))
            {
            	browseType[colIdx] = cl.getLinkType(field);
            	try {
					viewFull[colIdx] = BrowseIndex.getBrowseIndex(browseType[colIdx]).isItemIndex();
				} catch (BrowseException e) {
					viewFull[colIdx] = false;
				}
            }
        }
    }
    
    public TreeListTag() {
        super();
    }

	public int doStartTag() throws JspException {
		JspWriter out = pageContext.getOut();
        try {
        	initTableColumns();
        	out.print(buildTree(collection));        	
        }catch(Exception e) {
        	log.error("Error building master-detail tree");
        	log.error(e.getMessage());        	        	
        }
        return SKIP_BODY;
    }
	
	private String buildTree(Collection collection) throws Exception {
		TreeMap<String, List<String>> sortedCalculations = getCalculationsFieldsByName(collection);
		return buildMasterDetailTree(sortedCalculations);		
	}
	
    private void initTableColumns() {
    	if(!areHeadersLoaded) {    	
    		columns = new ArrayList<String>();
	    	columnDefs = new ArrayList<String>();
	    	columnTranslate = new HashMap<String, String>();
	    	
	    	columns.add("{'title': 'parent', 'type': 'string', 'visible': false, 'orderable': false}");
	    	columns.add("{'title': 'name', 'type': 'string', 'visible': false, 'orderable': false}");    	
	    	for (int colIdx = 0; colIdx < fieldArr.length; colIdx++){
	    		String field = fieldArr[colIdx].toLowerCase().trim();    	
	    		String localMessage = LocaleSupport.getLocalizedMessage(pageContext, "itemlist." + field);
	    		if(field.equals("thumbnail")) {
	    			columns.add("{'title': 'Preview', 'type': 'string', 'width': '" + widthArr[colIdx] +"', 'orderable': false, 'className': 'treecell'}");
	    			columnDefs.add("{  'render' :  function ( data, type, row, meta ) {\n" +    					
									"							return \"<a href=\" + data + \"  class='highslide' onclick='return hs.expand(this)'><img src=\" + data + \"  alt='Highslide JS' title='Click to enlarge'  height='" + thumbItemListMaxHeight + "' width='" + thumbItemListMaxWidth + "'></img></a>\";},\n" +
									"  'responsivePriority': 1," +								
		   							"  'targets' : " + (columns.size() - 1) + "\n" + 
		   				    		"}");
	    		}else if(fieldArr[colIdx].equals(titleField)) {
	    			String normalizedField = field.replaceAll("[\\s\\W)]+", "_"); 
	    			columnTranslate.put(normalizedField, localMessage);
	    			columns.add("{'title': '" + localMessage + "', 'type': 'string', 'width': '" + widthArr[colIdx] +"', 'orderable': false, 'className': 'treecell'}");
	    			columnDefs.add("{  'render' :  function ( data, type, row, meta ) {\n" + 
									"							return \"<div class='" + getCellClass(colIdx) + "'><b><a href=\" + row[row.length - 2] + \">\" + (mode == 'tree'? row[1] : row[row.length - 1]) + \"</a></b></div>\" ;},\n" +
									"  'responsivePriority': 1," +
									"  'targets': " + (columns.size() - 1) + "\n" + 
									"}");    			
	    		}else if(isBoolean[colIdx]) {
	    			columns.add("{'title': '" + localMessage + "', 'type': 'string', 'width': '" + widthArr[colIdx] +"', 'orderable': false, 'className': 'treecell'}");
	    			columnDefs.add("{  'render' :  function ( data, type, row, meta ) {\n" +    								
		   							"							return \"<span class='glyphicon glyphicon-\" + ((data)?'ok':'remove') + \"'></span>\";},\n" +
									"  'responsivePriority': 10001," +
		   							"  'targets' : " + (columns.size() - 1) + "\n" + 
		   				    		"}");
	    		}else {    			
	    			String normalizedField = field.replaceAll("[\\s\\W)]+", "_"); 
	    			columnTranslate.put(normalizedField, localMessage);
	    			columns.add("{'title': '" + localMessage + "', 'type': 'string', 'width': '" + widthArr[colIdx] +"', 'orderable': false, 'className': 'treecell'}");
	    			columnDefs.add("{  'render' :  function ( data, type, row, meta ) {\n" + 
							"							return \"<div class='" + getCellClass(colIdx) + "'>\" + data + \"</div>\" ;},\n" +
							"  'responsivePriority': 2," +
							"  'targets': " + (columns.size() - 1) + "\n" + 
							"}");    			
	    			
	    			
		       }
		   }
	       columns.add("{'title': 'handle', 'type': 'string', 'visible': false, 'orderable': false}");
	       areHeadersLoaded = true;
    	}
	}
    
    private String getCellClass(int colIdx) {    	
    	if(fieldArr[colIdx].equals(titleField)) {
    		return "titleCell";
    	}else if(fieldArr[colIdx].equals(authorField)) {
    		return "authorCell";
    	}else if(isDate[colIdx]) {
    		return "dateCell";    		
    	}else {
    		return "genericCell";    		
    	}
    }

	private TreeMap<String, List<String>> getCalculationsFieldsByName(Collection collection) throws SQLException, UnsupportedEncodingException {
		HttpServletRequest hrq = (HttpServletRequest) pageContext.getRequest();
		TreeMap<String, List<String>> calculations = new TreeMap<String, List<String>>();
    	ItemIterator iter = collection.getAllItems();
    	while(iter.hasNext()){
    		Item item = iter.next();
    		List<String> values = new ArrayList<String>();
    		String title = "";
    		String handle = "";
    		for (int colIdx = 0; colIdx < fieldArr.length; colIdx++){        			
				String field = fieldArr[colIdx];
				if(field.equals("thumbnail")) {
					values.add("'" + getThumbnailUrl(hrq, item) + "'");												
				}else {											
					StringTokenizer eq = new StringTokenizer(field, ".");     
					String[] tokens = { "", "", "" };
					int k = 0;
					while(eq.hasMoreTokens()) {
						tokens[k] = eq.nextToken().toLowerCase().trim();
						k++;
					}
					String schema = tokens[0];
					String element = tokens[1];
					String qualifier = tokens[2];
					
					Metadatum[] metadataArray = getItemMetadata(item, schema, element, qualifier);
					String metadata = "-";
					if(field.equals(titleField)) {
						if(!item.isWithdrawn()) { 
							metadata = "<a href='" + hrq.getContextPath() + "/handle/" + item.getHandle() + "'><b>" + Utils.addEntities(metadataArray[0].value) + "</b></a>";
							handle = hrq.getContextPath() + "/handle/" + item.getHandle();
						}else
							metadata = Utils.addEntities(metadataArray[0].value);	                    // format the title field correctly for withdrawn items (ie. don't link)																					                            													
						values.add("\"" + metadata + "\"");
						title = metadataArray[0].value.replaceAll("\"", "&quot;").replaceAll("'", "&apos;").replaceAll("<", "&lt;");
					}else if (isDate[colIdx]) {
                        DCDate dd = new DCDate(metadataArray[0].value);
                        metadata = UIUtil.displayDate(dd, false, false, hrq);
                        values.add("\"" + metadata + "\"");
                    } else if(isBoolean[colIdx]){
                    	metadata = (metadataArray.length == 0 || Boolean.valueOf(metadataArray[0].value) == false)? "false": "true";
                    	values.add(metadata);	                    
                    } else {	                            
                        boolean truncated = false;														// limit the number of records if this is the author field (if -1, then the limit is the full list)
                        int loopLimit = metadataArray.length;
                        if (isAuthor[colIdx]) {                            
                        	int fieldMax = (authorLimit > 0 ? authorLimit : metadataArray.length);
                            loopLimit = (fieldMax > metadataArray.length ? metadataArray.length : fieldMax);
                            truncated = (fieldMax < metadataArray.length);	
                        }                                                                                   
                        StringBuffer sb = new StringBuffer();
                       
                        for (int j = 0; j < loopLimit; j++) {
                        	 String startLink = "";
                        	 String endLink = "";
                        	 if (!StringUtils.isEmpty(browseType[colIdx])) {
                        		 String argument;
                        		 String value;
                        		 if (metadataArray[j].authority != null && metadataArray[j].confidence >= MetadataAuthorityManager.getManager().getMinConfidence(metadataArray[j].schema, metadataArray[j].element, metadataArray[j].qualifier)){
                        		     argument = "authority";
                        		     value = metadataArray[j].authority;
                        		 }else {
                        		     argument = "value";
                        		     value = metadataArray[j].value;
                        		 }
                        		 if (viewFull[colIdx]){
                        		     argument = "vfocus";
                        		 }
                        		 startLink = "<a href='" + hrq.getContextPath() + "/browse?type=" + browseType[colIdx] + "&amp;" +
                        		     argument + "=" + URLEncoder.encode(value,"UTF-8");

                        		 if (metadataArray[j].language != null){
                        		     startLink = startLink + "&amp;" +
                        		         argument + "_lang=" + URLEncoder.encode(metadataArray[j].language, "UTF-8");
                        		 }

                        		 if ("authority".equals(argument)){
                        		     startLink += "' class='authority " +browseType[colIdx] + "'>";
                        		 }
                        		 else{
                        		     startLink = startLink + "'>";
                        		 }
                        		 endLink = "</a>";
                        	 }
                        	 sb.append(startLink);
                             sb.append(Utils.addEntities(metadataArray[j].value));
                             sb.append(endLink);                                 
                             if (j < (loopLimit - 1))
                            	 sb.append("; ");                                
                        }
                        if (truncated) {
                            String etal = LocaleSupport.getLocalizedMessage(pageContext, "itemlist.et-al");
                            sb.append(", ").append(etal);
                        }
                        metadata = "<em>" + sb.toString() + "</em>";
                        values.add("\"" + metadata + "\"");
                    }
				}
    		}   
    		values.add("\"" + handle +  "\"");
    		values.add("\"" + title +  "\"");
    		if(!calculations.containsKey(title))	//There can be multiple calculations with the same name and path 
    			calculations.put(title, new ArrayList<String>());        	        		
    		calculations.get(title).add(StringUtils.join(values, ","));        		
         }
		return calculations;
	}

	private String buildMasterDetailTree(TreeMap<String, List<String>> calculations) throws Exception {
		children = new ArrayList<String>();
		parentIndex = 1;
	    firstChildWithContent = 0;
	    rootHasContent = false;
	    calculationCount = new HashMap<Integer, Integer>();
		StringBuilder response = new StringBuilder();
    	response.append("<div id='mainDiv' class='row'>\n");
    	response.append("		 <div id='viewModeSelectorDiv' class='viewModeSelector'>\n");
    	response.append("        	<div id='treeModeSelectorDiv' class='modeSelectorDiv'>\n");
    	response.append("        	 	<span>View as </span>\n");    	
    	response.append(" 				<label class='radio-inline'>\n");
    	response.append(" 					<input type='radio' name='viewModeOptions' id='viewMode1' value='tree' onclick=\"changeDisplay('calculationTree', 'detailTable', 'tree'); resetTree('calculationTree', 'detailTable')\">Tree");
    	response.append(" 				</label>\n");
    	response.append(" 				<label class='radio-inline'>\n");    	
    	response.append(" 					<input type='radio' name='viewModeOptions' id='viewMode2' value='list' onclick=\"changeDisplay('calculationTree', 'detailTable', 'full')\">List\n");    	
    	response.append(" 				</label>\n");
    	response.append(" 			</div>\n");
    	response.append("		</div>\n");    
    	// Build parent path tree, with nested <ul>s and <li>s	
    	response.append("        <div id='masterDiv' class='col-md-3'>\n");            	
    	response.append("            <ul id='calculationTree' class='tree'>\n");
    	response.append("				<ul id='rootNode'>\n");
		response.append("   				<li class='node' index='0'><span class='nodeText'>ROOT</span>\n");
		
		String lastPath = "";
    	parentIndex = 1;
    	String fields[];
		String path;		
		Iterator<String> calcIter = calculations.keySet().iterator();
		while(calcIter.hasNext()) {
			path = calcIter.next();
			if(!path.startsWith("/")) 					
				fields = (" " + path.trim().replaceAll("\\s+", "_")).split(" ", 2);
			else 
				fields = path.trim().split(" ", 2);			
			String parentPath = fields[0];
			String name = (fields.length == 1) ? "" : fields[1];
			for(String values : calculations.get(path))
				lastPath = processPath(response, lastPath, parentPath, name, values);
		}		
    	// Close current nesting
    	for(int i = 0; i < lastPath.split("/").length -1; i++)
			response.append("</li></ul>\n");      	
    	response.append("							</li>\n");
    	response.append("						</ul>\n");
    	response.append("					</div>\n");
    	response.append("					<div id='detailDiv' class='col-md-9'>\n");
    	response.append("						<div class='detailTableAffix'>");
    	response.append("							<div id='content'>\n"); 
    	response.append("								<table id='detailTable' class='table'></table>\n");
    	response.append("							</div>\n"); 
    	response.append("						</div>\n");
    	response.append("					</div>\n");
		response.append("				</div>\n");
    	// Set tree values and launch tree init command    
    	response.append("<script type='text/javascript'>\n");
		response.append("var calculationDetails = [\n\t");
		response.append(StringUtils.join(children, ",\n\t"));
		response.append("\t];\n");
		
		response.append("var nodeOccupation = {");
		for(int i : calculationCount.keySet()) 
			response.append("\"node" + i + "\": " + calculationCount.get(i) +  ",");
		response.append("};\n");
		response.append("columns = [" + StringUtils.join(columns, ',') + "];\n");
		response.append("columnDefs = [" + StringUtils.join(columnDefs, ',') + "];\n");
		response.append("$(document).ready(function(){\n");				
		response.append("		initTree('calculationTree', 'detailTable', columns ,columnDefs, " + (rootHasContent? "0" : firstChildWithContent) + ");\n");
		if(firstChildWithContent == 0)		// No hierarchy, set mode to full
			response.append("		setFullModeOnly('calculationTree', 'detailTable');\n");
		else
			response.append("       resetTree('calculationTree', 'detailTable');\n");
		response.append("});\n");
		response.append("</script>\n");			
		return response.toString();
	}

    public String processPath(StringBuilder response, String lastPath, String path, String name, String otherFields) throws IOException {    	
		if(lastPath.equals(path)) {
			if(path.equals("")) {			// Root calculations
				children.add("[0, '" + getNameOrPath(path, name) + "', " + otherFields + "]");
				rootHasContent = true;
				countCalculation(0);
			} else {						// Nested calculations				
				firstChildWithContent = firstChildWithContent!=0 ? firstChildWithContent : parentIndex -1;   
				children.add("[" + (parentIndex -1) + ", '" + getNameOrPath(path, name) + "', " + otherFields + "]");
				countCalculation(parentIndex -1);
			}
			return lastPath;
		}else if(path.startsWith(lastPath + "/")) {
			String partialPath[] = path.replaceAll("^" + Pattern.quote(lastPath), "").split("/");
			response.append("<ul>\n");
			response.append("   <li class='node' index='" + parentIndex + "'><span class='nodeText'>" + partialPath[1] + "</span>\n");
			lastPath = lastPath + "/" + partialPath[1];
			parentIndex++;
			return processPath(response, lastPath, path, name, otherFields);			
		}else {
			response.append("   </li>\n");			
			response.append("</ul>\n");
			lastPath = lastPath.substring(0, lastPath.lastIndexOf("/"));			
			return processPath(response, lastPath, path, name, otherFields);
		}
	}

    private String getNameOrPath(String path, String name) {
    	try {
    		if(name == null || name.equals("")) {
        		if(path != null && path.contains("/"))
        			return path.substring(path.lastIndexOf("/")+ 1);
        		else return "";
        	}else 
        		return name.replaceAll("\"", "&quot;").replaceAll("'", "&apos;").replaceAll("<", "&lt;");	
    	}catch(Exception e) {
    		return "";	
    	}
    	
    }
    
    private void countCalculation(int parentIndex) {    	
    	if(calculationCount.containsKey(parentIndex)) 
    		calculationCount.put(parentIndex, calculationCount.get(parentIndex) + 1);    		
    	else
    		calculationCount.put(parentIndex, 1);
    }
    
	private String getThumbnailUrl(HttpServletRequest hrq, Item item) {		
		try {
			Context c = UIUtil.obtainContext(hrq);                        
            if(AuthorizeManager.authorizeActionBoolean(c, item, Constants.READ, false)){            	
    			Thumbnail thumbnail = ItemService.getThumbnail(c, item.getID(), false);
    	        if (thumbnail == null)
    	        	return hrq.getContextPath() + "/image/notavailable.png";	    
    	        Bitstream thumb = thumbnail.getThumb();
    	        return hrq.getContextPath() + "/retrieve/" + thumb.getID() + "/" + UIUtil.encodeBitstreamName(thumb.getName(), Constants.DEFAULT_ENCODING);
            }            
            else{
            	return hrq.getContextPath() + "/image/loginrequired.png";
            }             		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return hrq.getContextPath() + "/image/notavailable.png";
	}

	private Metadatum[] getItemMetadata(Item item, String schema, String element, String qualifier) {
		Metadatum[] metadataArray;
		if (qualifier.equals("*"))
			metadataArray = item.getMetadata(schema, element, Item.ANY, Item.ANY);
		else if (qualifier.equals(""))
			metadataArray = item.getMetadata(schema, element, null, Item.ANY);
		else
			metadataArray = item.getMetadata(schema, element, qualifier, Item.ANY);
		// save on a null check which would make the code untidy
		if (metadataArray == null)
		  	metadataArray = new Metadatum[0];
		return metadataArray;
	}	
}
