<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Footer for home page
  --%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ page import="java.net.URLEncoder" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.core.Context" %>
<%@ page import="java.io.BufferedReader" %>
<%@ page import="java.io.FileReader" %>
<%@ page import="java.util.GregorianCalendar" %>
<%@ page import="java.util.Calendar" %>

<%
    String sidebar = (String) request.getAttribute("dspace.layout.sidebar");
%>

            <%-- Right-hand side bar if appropriate --%>
<%
    if (sidebar != null)
    {
%>
	</div>
	<div class="col-md-3">
                    <%= sidebar %>
    </div>
    </div>       
<%
    }
%>
</div>
</main>
<% 
	// Retrieve current version and store it on servlet context  
	String iochemVersion = (String)(pageContext.getServletContext().getAttribute("iochem.version"));	
	String currentYear = String.valueOf(new GregorianCalendar().get(Calendar.YEAR));
	if(iochemVersion == null){
		try{
			String versionFilePath = pageContext.getServletContext().getInitParameter("dspace.dir") + "/../updates/version.txt";
			BufferedReader br = new BufferedReader(new FileReader(versionFilePath));
			iochemVersion = br.readLine();
			br.close();
			if(iochemVersion == null)
				iochemVersion = "";
					
		}catch(Exception e){
			iochemVersion = "";			
		}
		pageContext.getServletContext().setAttribute("iochem.version",iochemVersion);
	}

    String footer = (String) request.getAttribute("dspace.layout.footer");
	if (footer != null)
	{
%>
    <%-- Page footer --%>
	<footer class="navbar navbar-custom hidden-xs hidden-sm" style="height:80px">
		<div class="container">
			<div class="row">					       
		       	<div class="col-md-4 col-sm-12">
		       		<div class="row" style="text-align:center; display: flex; flex-direction:column; justify-content: space-around; align-items: center; height: 73px">
		       			<span style="margin-right: 10px">Synchronized with: </span>
		       			<a href="https://www.iochem-bd.org" target="_blank"><img src="<%= request.getContextPath() %>/image/logo-find.png" alt="ioChem-BD homepage"></a>		       		
		       		</div>
		       		<div class="row">		       			
				        <a href="<%= request.getContextPath() %>/htmlmap"></a>
		       		</div>
		       	</div>
		       	<div class="col-md-4" style="text-align:center; display: flex; flex-direction:column; justify-content: space-around; align-items: center; height: 73px">
		       		<div>
		       		<fmt:message key="jsp.layout.footer-default.text">					    
       	                <fmt:param value="<%=iochemVersion %>"/>       	                       	
		       		</fmt:message>
		       		<span>&nbsp;-&nbsp;<a target="_blank" href="<%= request.getContextPath() %>/feedback"><fmt:message key="jsp.layout.footer-default.feedback"/></a></span>		       			
		       		</div>
		       		<fmt:message key="jsp.layout.footer-default.text.second">
		       			<fmt:param value="<%=currentYear %>"/>
		       		</fmt:message>		       				       					      
		       	</div>
		       	<div class="col-md-4 col-sm-12">
		       		<div class="row" style="display:flex;flex-direction: column; align-items:center">
		       			<span style="margin-right: 10px">Developed by:</span>
		       			<div>       		
			       			<a target="_blank" href="http://www.iciq.cat/"><img src="<%= request.getContextPath() %>/image/logo-iciq.png" alt="ICIQ homepage" height="55" /></a>
			        		<a target="_blank" href="http://www.quimica.urv.es/w3qf/" class="pull-right"><img src="<%= request.getContextPath() %>/image/logo-urv.png" alt="URV homepage" height="55" /></a>
		        		</div>
		        	</div>
		       		
		       		</div>     	
		       	</div>
			</div>
			<div class="row">
		     	
			</div>
		</div>
    </footer>
<%
	}
%>
	</body>
</html>
