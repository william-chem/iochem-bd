<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Home page JSP
  -
  - Attributes:
  -    communities - Community[] all communities in DSpace
  -    recent.submissions - RecetSubmissions
  --%>

<%@page import="org.dspace.core.Utils"%>
<%@page import="org.dspace.content.Bitstream"%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="java.io.File" %>
<%@ page import="java.util.Enumeration"%>
<%@ page import="java.util.Locale"%>
<%@ page import="javax.servlet.jsp.jstl.core.*" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.app.webui.components.RecentSubmissions" %>
<%@ page import="org.dspace.content.Community" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.NewsManager" %>
<%@ page import="org.dspace.browse.ItemCounter" %>
<%@ page import="org.dspace.content.Metadatum" %>
<%@ page import="org.dspace.content.Item" %>
<%@ page import="org.dspace.core.Constants" %>
<%@ page import="org.dspace.core.Context"%>
<%@ page import="org.dspace.content.service.ItemService" %>
<%@ page import="org.dspace.content.Thumbnail" %>


<%
    Community[] communities = (Community[]) request.getAttribute("communities");

    Locale sessionLocale = UIUtil.getSessionLocale(request);
    Config.set(request.getSession(), Config.FMT_LOCALE, sessionLocale);
    String topNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-top.html"));
    String sideNews = NewsManager.readNewsFile(LocaleSupport.getLocalizedMessage(pageContext, "news-side.html"));

    boolean feedEnabled = ConfigurationManager.getBooleanProperty("webui.feed.enable");
    String feedData = "NONE";
    if (feedEnabled)
    {
        feedData = "ALL:" + ConfigurationManager.getProperty("webui.feed.formats");
    }
    
    ItemCounter ic = new ItemCounter(UIUtil.obtainContext(request));

    RecentSubmissions submissions = (RecentSubmissions) request.getAttribute("recent.submissions");
%>

<dspace:layout locbar="nolink" titlekey="jsp.home.title" headerbanner="true" footer="true" feedData="<%= feedData %>">

	<div class="jumbotron">
        <%= topNews %>
	</div>


	<div class="container row">
<%
	if (communities != null && communities.length != 0)
	{
%>
		<div class="col-md-4">		
	               <h3><fmt:message key="jsp.home.com1"/></h3>
	                <p><fmt:message key="jsp.home.com2"/></p>
					<div class="list-group">
<%
		boolean showLogos = ConfigurationManager.getBooleanProperty("jspui.home-page.logos", true);
	    for (int i = 0; i < communities.length; i++)
	    {
%><div class="list-group-item row">
<%  
			Bitstream logo = communities[i].getLogo();
			if (showLogos && logo != null) { %>
		<div class="col-md-3">
	        <img alt="Logo" class="img-responsive" src="<%= request.getContextPath() %>/retrieve/<%= logo.getID() %>" /> 
		</div>
		<div class="col-md-9">
<% } else { %>
		<div class="col-md-12">
<% }  %>		
			<h4 class="list-group-item-heading"><a href="<%= request.getContextPath() %>/handle/<%= communities[i].getHandle() %>"><%= communities[i].getMetadata("name") %></a>
<%
	        if (ConfigurationManager.getBooleanProperty("webui.strengths.show"))
	        {
%>
			<span class="badge pull-right"><%= ic.getCount(communities[i]) %></span>
<%
	        }
	
%>
			</h4>
			<p><%= communities[i].getMetadata("short_description") %></p>
	    </div>
	</div>                            
<%
	    }
%>
		</div>
		</div>
<%
	}
%>
<%
	    	int discovery_panel_cols = 8;
	    	int discovery_facet_cols = 4;
%>
	<%@ include file="discovery/static-sidebar-facet.jsp" %>
	</div>
	
	<div class="row">
		<%@ include file="discovery/static-tagcloud-facet.jsp" %>
	</div>


	<div class="row">
<%
if (submissions != null && submissions.count() > 0)
{
%>
        <div class="col-md-12">
        <div class="panel panel-primary">        
        <div id="recent-submissions-carousel" class="panel-heading carousel slide">
          <h3><fmt:message key="jsp.collection-home.recentsub"/>
              <%
    if(feedEnabled)
    {
	    	String[] fmts = feedData.substring(feedData.indexOf(':')+1).split(",");
	    	String icon = null;
	    	int width = 0;
	    	for (int j = 0; j < fmts.length; j++)
	    	{
	    		if ("rss_1.0".equals(fmts[j]))
	    		{
	    		   icon = "rss1.gif";
	    		   width = 43;
	    		}
	    		else if ("rss_2.0".equals(fmts[j]))
	    		{
	    		   icon = "rss2.gif";
	    		   width = 43;
	    		}
	    		else
	    	    {
	    	       icon = "rss.gif";
	    	       width = 43;
	    	    }
	%>
	    <a href="<%= request.getContextPath() %>/feed/<%= fmts[j] %>/site"><img src="<%= request.getContextPath() %>/image/<%= icon %>" alt="RSS Feed" width="<%= width %>" height="32" style="margin: 3px 0 3px" /></a>
	<%
	    	}
	    }
	%>
          </h3>
          
		  <!-- Wrapper for slides -->
		  <div class="carousel-inner">		  		  
  	<%  boolean first = true;
	  	for (Item item : submissions.getRecentSubmissions())		  		
			{
			Metadatum[] dcv = item.getMetadata("dc", "title", null, Item.ANY);
			
			String displayTitle = "Untitled";
			String displayAuthor = "Unknown";
			String displayDate = "Unknown";
			String displaySubject  = "";		
			String thumbnailImage = null;
			
			if (dcv != null)
			{
				if (dcv.length > 0)
				{
					displayTitle = Utils.addEntities(dcv[0].value);
					
				}
			}
			
			dcv = item.getMetadata("dc", "contributor", "author", Item.ANY);
			if( dcv != null)
			{
				if(dcv.length > 0)
				{
					displayAuthor = Utils.addEntities(dcv[0].value);
				}
			}
			
			dcv = item.getMetadata("dc", "date", "issued", Item.ANY);
			if( dcv != null)
			{
				if(dcv.length > 0)
				{
					displayDate = Utils.addEntities(dcv[0].value);
				}
			}			
			
			dcv = item.getMetadata("dc", "subject", null, Item.ANY);
			if( dcv != null)
			{
				for(Metadatum meta : dcv)
				{
					displaySubject += Utils.addEntities(meta.value) + "<br/>";
				}
			}
			
			try{
				Context c = UIUtil.obtainContext(request);
				Thumbnail thumbnail = ItemService.getThumbnail(c, item.getID(), false);
	            if (thumbnail != null)
	    		{
		        	StringBuffer thumbFrag = new StringBuffer();
		        	String link = request.getContextPath() + "/handle/" + item.getHandle();
		        	thumbFrag.append("<a href=\"" + link + "\" />");

		        	Bitstream thumb = thumbnail.getThumb();
		        	String img = request.getContextPath() + "/retrieve/" + thumb.getID() + "/" + UIUtil.encodeBitstreamName(thumb.getName(), Constants.DEFAULT_ENCODING);
		        	String alt = thumb.getName();			            
		            thumbFrag.append("<img src=\"")
		                    .append(img)
		                    .append("\" alt=\"").append(alt).append("\" ")			                     
		                    .append("/ border=\"0\" class=\"img-responsive\"  onerror=\"this.onerror=null;this.src='image/loginrequired_big.png';\"  ></a>");
		        	thumbnailImage = thumbFrag.toString();
	    		}else {
	    			thumbnailImage = "<img src='image/nopreview.png' border='0' class='img-responsive'>";	    				
	    		}
			}catch(Exception e){}
			
			%>
		    <div style="padding-bottom: 50px; min-height: 200px;" class="item <%= first?"active":""%>">
		    	<div class="row">
		    		<div class="col-md-6 col-sm-12 text-center">
					    <% if(thumbnailImage != null) { %>
							<%=thumbnailImage %>		    
					    <% } %>		
		    		</div>
		    		<div class="col-md-6 col-sm-12">	
		    			<table class="table itemDisplayTable">
		    				<tbody>
		    					<tr>
		    						<td class="metadataFieldLabel">Date:</td>		    						
		    						<td class="metadataFieldValue"><%=displayDate%></td>
		    					</tr>
		    					<tr>
		    						<td class="metadataFieldLabel">Title:</td>		    						
		    						<td class="metadataFieldValue"><%=StringUtils.abbreviate(displayTitle, 100)%></td>
		    					</tr>		    				
		    					<tr>
		    						<td class="metadataFieldLabel">Author:</td>		    						
		    						<td class="metadataFieldValue"><%=displayAuthor%></td>
		    					</tr>
		    					
		    					<tr>
		    						<td class="metadataFieldLabel">Keywords:</td>		    						
		    						<td class="metadataFieldValue"><%=displaySubject%></td>
		    					</tr>
		    				</tbody>
		    			</table>				      	
		    		</div>
		    		<div class="col-sm-12 col-md-6">
		    			<a href="<%= request.getContextPath() %>/handle/<%=item.getHandle() %>" class="btn btn-success btn-block">View</a>
		    		</div>		    
		    	</div>
		      
		    </div>
<%
				first = false;
		     }
		%>
		  
		  </div>

		  <!-- Controls -->
		  <a class="left carousel-control" href="#recent-submissions-carousel" data-slide="prev">
		    <span class="icon-prev"></span>
		  </a>
		  <a class="right carousel-control" href="#recent-submissions-carousel" data-slide="next">
		    <span class="icon-next"></span>
		  </a>

          <ol class="carousel-indicators">
		    <li data-target="#recent-submissions-carousel" data-slide-to="0" class="active"></li>
		    <% for (int i = 1; i < submissions.count(); i++){ %>
		    <li data-target="#recent-submissions-carousel" data-slide-to="<%= i %>"></li>
		    <% } %>
	      </ol>
     </div></div></div>
<%
}
%>
		<div class="col-md-4">
		    <%= sideNews %>
		</div>
	</div>
</div>

</dspace:layout>
