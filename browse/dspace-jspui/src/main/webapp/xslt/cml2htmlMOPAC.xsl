<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:mp="http://www.iochem-bd.org/dictionary/mopac/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
  
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk mp helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Feb 25, 2019</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>       
    </xd:doc>
    <xsl:include href="helper/chemistry/mopac.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>    
    <xsl:param name="browseurl"/>
    <xsl:param name="jcampdxurl"/>

    <!-- Build calculation types variable, one per job  -->    
    <xsl:variable name="calcTypes">
        <xsl:for-each select="//cml:module[@cmlx:templateRef='job']">
            <xsl:variable name="inputLines" select="./cml:module [@dictRef='cc:initialization']/cml:module[@cmlx:templateRef='inputlines']/cml:list[@cmlx:templateRef='lines']" />
            <xsl:variable name="isOptimization" select="exists(.//cml:module[@cmlx:templateRef='optimization'])" />
            <xsl:variable name="vibrations" select=".//cml:module[@dictRef='cc:finalization']//cml:property[@dictRef='cc:frequencies']" />
            <xsl:variable name="hasVibrations" select="exists($vibrations)"/>
            <xsl:variable name="negativeFrequenciesCount" select="count(tokenize($vibrations//cml:module[@dictRef='cc:finalization']//cml:property[@dictRef='cc:frequencies']//cml:array[@dictRef='cc:frequency'],'-')) -1" />
            <xsl:variable name="isTs" select="mp:isTS($inputLines)" />            

            <xsl:element name="cml:calcType">
                <xsl:attribute name="methods" select="distinct-values(mp:getMethods($inputLines))" />
                <xsl:attribute name="isOptimization" select="$isOptimization"/>
                <xsl:attribute name="hasVibrations" select="$hasVibrations"/>
                <xsl:attribute name="negativeFrequenciesCount" select="$negativeFrequenciesCount"/>
                <xsl:value-of select="mp:getCalcType($isOptimization, $hasVibrations, $negativeFrequenciesCount, $isTs)"/>
            </xsl:element>
        </xsl:for-each>
    </xsl:variable>

    <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])" />
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    <xsl:variable name="subversionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programSubversion']"/>
    <xsl:variable name="charge">
        <xsl:variable name="value" select="helper:trim(mp:getCharge(//cml:module[@cmlx:templateRef='inputlines']))"/>
        <xsl:choose>
            <xsl:when test="$value = ''">0</xsl:when>
            <xsl:otherwise><xsl:value-of select="$value"/></xsl:otherwise>
        </xsl:choose>
    </xsl:variable>
    
    <xsl:variable name="multiplicity" select="mp:getMultiplicity(//cml:module[@cmlx:templateRef='inputlines'])"/>

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/exporting.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-ui.min.js"/>                
                <xsl:if test="$hasVibrations">
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>                          
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>
                </xsl:if>                      
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />                               
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/jquery-ui.min.css" type="text/css" />

                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <div id="container">
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">
                            <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                                <xsl:variable name="jobNumber" select="position()"/>
                                <div id="module-{generate-id(.)}">                                
                                    <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                                        <xsl:call-template name="atomicCoordinates">
                                            <xsl:with-param name="molecule" select="./cml:module[@dictRef='cc:finalization']/cml:molecule"/>
                                        </xsl:call-template>
                                        <xsl:call-template name="molecularInfo" />                                                                                                 
                                        <xsl:call-template name="vibrationalFrequencies" />
                                        <xsl:call-template name="finalresults"/>                                       
                                </div>
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>
                        </div>
                    </div>                    
    
                    <script type="text/javascript">

                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }                           
                        
                        $(document).ready(function() {
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                            
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                        $(this).find('.dataTable').each(function(index){
                            var oTable = $(this).dataTable();
                            $(oTable).css({ width: $(oTable).parent().width() });
                            oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                    </script>
                </div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="$subversionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>
            <xsl:for-each select="$calcTypes/cml:calcType">
                <tr>               
                    <td>Calculation type:</td>
                    <td>                                                                 
                        <xsl:value-of select="./text()"/>                        
                    </td>                    
                </tr>
                <tr>
                    <td>Method:</td>
                    <td>
                        <xsl:value-of select="./@methods"/>
                    </td>                    
                </tr>
            </xsl:for-each>            
        </table>
    </xsl:template>

    <!-- Molecular info -->
    <xsl:template name="molecularInfo">        
        <div class="row bottom-buffer">
            <div class="col-md-12 col-sm-12">
                <h4>MOLECULAR INFO</h4>
                <table class="display" id="molecularInfo-{generate-id(.)}">
                    <thead>
                        <tr>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>    
                    <tbody>
                        <tr>
                            <td>Multiplicity</td>
                            <td><xsl:value-of select="$multiplicity"/></td>
                        </tr>
                        <tr>
                            <td>Charge</td>
                            <td><xsl:value-of select="$charge"/></td>
                        </tr>                
                    </tbody>
                </table>  
            </div>
        </div>
        <xsl:call-template name="atomicDistances"/>
    </xsl:template>
    
    <xsl:template name="atomicCoordinates">
        <xsl:param name="molecule"/>            
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinates-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                           
                </h4>
            </div>
            <div id="atomicCoordinates-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>                              
                            <script type="text/javascript">
                                var atomicCoordinatesXYZ = [
                                    ['<xsl:value-of select="$title"/>','','',''],                                
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                        
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                        <xsl:variable name="id" select="@id"/>
                                        [ '<xsl:value-of select="$elementType"/>',<xsl:value-of select="format-number(@x3,'#0.000000')"/>,<xsl:value-of select="format-number(@y3,'#0.000000')"/>,<xsl:value-of select="format-number(@z3,'#0.000000')"/>]<xsl:if test="$outerIndex &lt; count($molecule/cml:atomArray/cml:atom)">,</xsl:if>
                                    </xsl:for-each>                                       
                                ];
                                
                                
                                $(document).ready(function() {
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "aaData" : atomicCoordinatesXYZ, 
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                });
                                
                                function getXYZ(){
                                var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                 new $.fn.dataTable.Buttons( table, {
                                    buttons: [ {
                                    extend: 'copyHtml5',
                                    text: 'XYZ'
                                    }
                                    ]
                                 });
                                 table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));                                    
                                 $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                }     
                            </script>
                        
                            <div id="atomicCoordinatesDiv" class="col-md-6">
                                <table class="display" id="atomicCoordinatesT-{generate-id($molecule)}">
                                    <thead>
                                        <tr>
                                            <th style="text-align:left">Atom</th>
                                            <th style="text-align:left"></th>                                            
                                            <th style="text-align:right">x</th>
                                            <th style="text-align:right">y</th>
                                            <th style="text-align:right">z</th>
                                        </tr>
                                    </thead>
                                </table>                                                
                            </div> 
                            
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                    $('table#atomicCoordinatesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable( {
                                        "aaData": [
                                        <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            <xsl:variable name="elementType" select="@elementType"/>                                                                   
                                            <xsl:variable name="id" select="@id"/>
                                            [<xsl:value-of select="position() - 1"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.000000')"/>","<xsl:value-of select="format-number(@y3,'#0.000000')"/>","<xsl:value-of select="format-number(@z3,'#0.000000')"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>    
                                        ],
                                        "aoColumns": [
                                            null,
                                            null,
                                            { "sClass": "right" },
                                            { "sClass": "right" },
                                            { "sClass": "right" }
                                        ],   
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    } );   
                                } );                                       
                            </script>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <!-- Interatomic distances (bond distances) -->       
    <xsl:template name="atomicDistances">
        <xsl:variable name="molecule" select="./cml:module[@dictRef='cc:finalization']/cml:molecule"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#bondDistances-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Bond distances                           
                </h4>
            </div>
            <div id="bondDistances-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-4 col-sm-12">
                            <table class="display" id="bondDistancesT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th>Atom1</th>
                                        <th>Atom2</th>
                                        <th class="right">Distance</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="$molecule/cml:bondArray/cml:bond">
                                        <xsl:variable name="atomRefs2" select="tokenize(./@atomRefs2,' ')"/>
                                        <!-- For each bond calculate interatomic distances -->
                                        <xsl:variable name="atomType1" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@elementType"/>
                                        <xsl:variable name="x1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@x3"/>
                                        <xsl:variable name="y1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@y3"/>
                                        <xsl:variable name="z1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@z3"/>
                                        <xsl:variable name="position1" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/preceding-sibling::*)+1"/>
                                        <!-- http://stackoverflow.com/questions/226405/find-position-of-a-node-using-xpath -->
                                        <xsl:variable name="atomType2" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@elementType"/>
                                        <xsl:variable name="x2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@x3"/>
                                        <xsl:variable name="y2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@y3"/>
                                        <xsl:variable name="z2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@z3"/>
                                        <xsl:variable name="position2" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/preceding-sibling::*)+1"/>
                                        <xsl:variable name="distance"  select="ckbk:sqrt((($x2 - $x1) * ($x2 - $x1)) + ($y2 - $y1) * ($y2 - $y1) + ($z2 - $z1) * ($z2 - $z1))"/>
                                        <tr>
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType1"/>
                                                <xsl:value-of select="$position1"/>
                                            </td>                                            
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType2"/>
                                                <xsl:value-of select="$position2"/>
                                            </td>
                                            <td class="right">                                                
                                                <xsl:value-of select="format-number($distance,'#0.000000')"/>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                 
                                    $('table#bondDistancesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable();                               
                                });  
                            </script>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <!-- Vibrational frequencies -->
    <xsl:template name="vibrationalFrequencies">
        <xsl:variable name="frequencies" select=".//cml:module[@id='finalization']/cml:propertyList/cml:property[@dictRef='cc:frequencies']/cml:module[@dictRef='cc:vibrations']"/>        
        <xsl:if test="exists($frequencies)">
            <div id="vibrationPanel" class="panel panel-default" >
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        IR spectrum / Vibrational frequencies   
                    </h4>
                </div>
                <div id="frequencies" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <xsl:call-template name="irSpectrum">
                                <xsl:with-param name="frequencies" select="$frequencies"/>
                            </xsl:call-template>                            
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if>                    
    </xsl:template>

    <xsl:template name="irSpectrum">
        <xsl:param name="frequencies"/>                       
        <xsl:if test="exists($frequencies)">
            <xsl:variable name="frequency" select="tokenize($frequencies/array[@dictRef='cc:frequency'],'\s+')"/>
                <div class="col-lg-12">                                                                                             
                    <script type="text/javascript">
                      	delete Jmol._tracker;
                        Jmol._local = true;
                    
                        jsvApplet0 = "jsvApplet0";
                        jmolApplet0 = "jmolApplet0";                                
                        Jmol.setAppletSync([jsvApplet0, jmolApplet0], ["load <xsl:value-of select="$jcampdxurl"/>", ""], true);                                       
                        var jmolInfo = {
                            width: 560,
                            height: 400,
                            debug: false,
                            color: "0xF0F0F0",
                            use: "HTML5",
                            j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                            disableJ2SLoadMonitor: true,
                            disableInitialConsole: true,
                            readyFunction: vibrationsLoaded,
                            animframecallback: "modelchanged",
                            allowjavascript: true,
                            script: "background white; vibration off; vectors off; sync on;"
                        }                                       
                        var jsvInfo = {
                            width: 560,
                            height: 400,
                            debug: false,
                            color: "0xC0C0C0",
                            use: "HTML5",
                            j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",                            
                            script: null,
                            initParams: null,
                            disableJ2SLoadMonitor: true,
                            disableInitialConsole: true,
                            readyFunction: null,
                            allowjavascript: true
                        }

                        $('div#frequencies').on('shown.bs.collapse', function () {
                            if($("div#jmolApplet").children().length == 0){       
                                use="HTML5";                                       
                                jsvApplet0 = Jmol.getJSVApplet("jsvApplet0", jsvInfo);
                                $("div#jsvApplet").html(Jmol.getAppletHtml(jsvApplet0));                                            
                                jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                                $("div#jmolApplet").html(Jmol.getAppletHtml(jmolApplet0));
                                vibrationsLoading();
                            }                                      
                        });

                        function vibrationsLoading(){                                       
                            $('div#frequencies').block({ 
                                message: '<h3>Loading spectrum</h3>', 
                                css: { border: '3px solid #a00' } 
                            });                                                                                    
                        }

                        function vibrationsLoaded(){
                            $('div#frequencies').unblock();                                                           
                        }

                    </script>
                    <div id="irSpectrumDiv">
                        <div id="jsvApplet" style="display:inline; float:left">
                        </div>
                        <div id="jmolApplet" style="display:inline; float:left">
                        </div>
                    </div>
                    <div id="frequencyComboboxDiv" style="display:block">
                        Selected frequency :
                        <script type="text/javascript">
                            function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                                vibrationcboIndex = $("select[name='jmolMenu0'] option:selected").index();
                                if(vibrationcboIndex != objwhat + 1)
                                    $("select[name='jmolMenu0']").val(objwhat + 2);
                            }
                            
                            Jmol.jmolMenu(jmolApplet0,[                    
                                ["vibration off", ".... select ....", true],
                                    <xsl:for-each select="1 to count($frequency)">
                                        <xsl:variable name="outerIndex" select="."/>
                                        ["model <xsl:value-of select="$outerIndex"/>;vibration on" ,"<xsl:value-of select="$frequency[$outerIndex]"/>"]<xsl:if test="$outerIndex != count($frequency)"><xsl:text>,</xsl:text></xsl:if>
                                    </xsl:for-each>                    
                            ]);                        
                        </script>                    
                    </div>
                    <script type="text/javascript">
                        $("#irSpectrumDiv" ).prepend( $("#frequencyComboboxDiv") );
                    </script>                                                              
                </div>                                                        
        </xsl:if> 
    </xsl:template> 
 
    <!-- Final results -->
    <xsl:template name="finalresults">        
        <xsl:variable name="energies" select=".//cml:module[@cmlx:templateRef='energies']" />        
        <xsl:if test="exists($energies)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#energy-{generate-id($energies)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Final results 
                    </h4>
                </div>
                <div id="energy-{generate-id($energies)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-5 col-md-5 col-sm-12 ">
                                <table class="display" id="energyT-{generate-id($energies)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>                                          
                                        <xsl:if test="exists($energies/cml:scalar[@dictRef='cc:totalener'])">
                                            <tr>
                                                <td>Total Energy</td>
                                                <td class="right"><xsl:value-of select="round-half-to-even(number($energies/cml:scalar[@dictRef='cc:totalener']),8)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($energies/cml:scalar[@dictRef='cc:totalener']/@units)"/></td>
                                            </tr>    
                                        </xsl:if>
                                        <xsl:if test="exists($energies/cml:scalar[@dictRef='cc:electener'])">
                                            <tr>
                                                <td>Electronic Energy</td>
                                                <td class="right"><xsl:value-of select="round-half-to-even(number($energies/cml:scalar[@dictRef='cc:electener']),8)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($energies/cml:scalar[@dictRef='cc:electener']/@units)"/></td>
                                            </tr>    
                                        </xsl:if>
                                        <xsl:if test="exists($energies/cml:scalar[@dictRef='cc:nucrepener'])">
                                            <tr>
                                                <td>Core-Core Repulsion</td>
                                                <td class="right"><xsl:value-of select="round-half-to-even(number($energies/cml:scalar[@dictRef='cc:nucrepener']),8)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($energies/cml:scalar[@dictRef='cc:nucrepener']/@units)"/></td>
                                            </tr>    
                                        </xsl:if>                                                                                       
                                        <xsl:if test="exists($energies/cml:scalar[@dictRef='cc:zeropoint'])">
                                            <tr>
                                                <td>Zero point vibrational energy</td>
                                                <td class="right"><xsl:value-of select="round-half-to-even(number($energies/cml:scalar[@dictRef='cc:zeropoint']),8)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($energies/cml:scalar[@dictRef='cc:zeropoint']/@units)"/></td>
                                            </tr>                                            
                                        </xsl:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>
        </div>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
    
</xsl:stylesheet>
