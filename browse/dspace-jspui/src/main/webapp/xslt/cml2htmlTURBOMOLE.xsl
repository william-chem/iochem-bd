<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet     
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl" 
    xmlns:cml="http://www.xml-cml.org/schema" 
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx" 
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77" 
    xmlns:turbo="http://www.turbomole.com" 
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions" 
    xpath-default-namespace="http://www.xml-cml.org/schema" 
    exclude-result-prefixes="xs xd cml ckbk turbo helper cmlx" 
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Dec 16, 2014</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>
        </xd:desc>
    </xd:doc>
    <xsl:include href="helper/chemistry/turbomole.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>
    <xsl:param name="jcampdxurl"/>
    <xsl:param name="moldenurl"/>

    <xsl:strip-space elements="*"/>

    <xsl:variable name="isRestrictedOptimization" select="exists(//cml:module[@cmlx:templateRef='restrictions'])"/>
    <xsl:variable name="isOptimization" select="exists(//cml:module[@cmlx:templateRef='convergence.info'])"/>
    <xsl:variable name="isIncomplete" select="count(//cml:module[@cmlx:templateRef='convergence.info']//cml:scalar[@dictRef='cc:converged' and normalize-space(text())='no']) > 0"/>
    <xsl:variable name="vibrations" select="//cml:module[@cmlx:templateRef='vibrations']"/>
    <xsl:variable name="hasOrbitals" select="exists($moldenurl) and ($moldenurl != '')" />
    <xsl:variable name="statpt" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:statpt']"/>
    <xsl:variable name="soes" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:soes']/cml:list"/>
    <xsl:variable name="calcType" select="helper:trim(turbo:getCalcType($isRestrictedOptimization, $isOptimization, $isIncomplete, $vibrations, $statpt, $soes))"/>
    <!-- Environment module -->
    <xsl:variable name="environmentModule" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:environment']"/>
    <xsl:variable name="program" select="$environmentModule/cml:parameterList/cml:parameter[@dictRef='cc:program']/cml:scalar"/>
    <xsl:variable name="programVersion" select="$environmentModule/cml:parameterList/cml:parameter[@dictRef='cc:programVersion']/cml:scalar"/>
    <!-- Initialization module -->
    <xsl:variable name="parameterList" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList"/>    
    <xsl:variable name="dispersion" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']//cml:module[@cmlx:templateRef='dispersion']/cml:scalar[@dictRef='t:correction']"/>
    <xsl:variable name="scfinstab" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:scfinstab']/cml:scalar"/>
    <xsl:variable name="exopt" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:exopt']/cml:scalar"/>

    <xsl:variable name="gridsize" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:dftgridsize']/cml:scalar"/>    
    <xsl:variable name="methods" select="$parameterList/cml:parameter[@dictRef='cc:method']/child::*"/>
    <xsl:variable name="parameters" select="$parameterList/cml:parameter[matches(@dictRef,'(t:ri|t:ricc2|t:rir12)')]/child::*"/>    
    <xsl:variable name="calcMethod" select="turbo:getMethod($soes, $methods)"/>
    
    <xsl:variable name="functional" select="//cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='cc:functional']/cml:scalar"/>
    <xsl:variable name="initialMolecule" select="(//cml:module[@dictRef='cc:initialization' and child::cml:molecule])[last()]//cml:molecule"/>
    <xsl:variable name="restrictions" select="(//cml:module[@dictRef='cc:initialization']//cml:module[@cmlx:templateRef='restrictions'])[1]/cml:module[@cmlx:templateRef='internals']"/>
    <xsl:variable name="atominfo" select="(//cml:module[@dictRef='cc:initialization']/cml:parameterList/cml:parameter[@dictRef='t:atoms'])[1]"/>
    <!-- Geometry -->
    <xsl:variable name="finalMolecule" select="(//cml:module[@dictRef='cc:finalization' and child::cml:molecule])[last()]//cml:molecule"/>
    <!-- Calculation module -->
    <xsl:variable name="basis" select="(//cml:module[@cmlx:templateRef='basisset'])[1]"/>
    <xsl:variable name="symmetry" select="(//cml:module[@dictRef='cc:calculation']//cml:module[@cmlx:templateRef='symmetry'])[1]"/>
    <!-- Finalization module -->
    <xsl:variable name="electrostaticmoments" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='electrostatic.moments'])[last()]"/>
    <xsl:variable name="cosmo" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='cosmo'])[last()]"/>
    <xsl:variable name="orbitalSpecs" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='orbitals'])[last()]"/>
       
    <xsl:variable name="orbitalSpecsRestricted" select="//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='restrictedorbitals']"/>    
    <xsl:variable name="orbitalSpecsAlpha" select="//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='unrestrictedorbitals' and compare(child::cml:scalar[@dictRef='t:spintype']/text(),'alpha') = 0]"/>
    <xsl:variable name="orbitalSpecsBeta" select="//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='unrestrictedorbitals' and compare(child::cml:scalar[@dictRef='t:spintype']/text(),'beta') = 0]"/>   
    <xsl:variable name="occupiedAlphaLabels" select="tokenize($orbitalSpecsAlpha/cml:array[@dictRef='t:label'],'\s+')"/>
    <xsl:variable name="occupiedAlpha" select="tokenize( replace($orbitalSpecsAlpha/cml:array[@dictRef='t:mosrange'],'\s*-\s*','-'),'\s+')"/>
    <xsl:variable name="occupiedBetaLabels" select="tokenize($orbitalSpecsBeta/cml:array[@dictRef='t:label'],'\s+')"/>
    <xsl:variable name="occupiedBeta" select="tokenize( replace($orbitalSpecsBeta/cml:array[@dictRef='t:mosrange'],'\s*-\s*','-'), '\s+')"/>

    <xsl:variable name="s2" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='s2']/cml:scalar)[last()]"/>
    <xsl:variable name="tddftRoots" select="//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']"/>  

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title>
                    <xsl:value-of select="$title"/>
                </title>
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>                        
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>                
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-ui.min.js"/>
                <xsl:if test="$vibrations or $hasOrbitals">
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>                    
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>                            
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>
                </xsl:if>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/jquery-ui.min.css" type="text/css" />

                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>
                    </License>
                </rdf:RDF>
            </head>
            <body>            
                <div id="container">
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>ATOM INFO</h3>
                            <xsl:variable name="molecule" select="
                                if(exists($finalMolecule)) then 
                                    $finalMolecule
                                else
                                    $initialMolecule
                                "/>
                            <div>
                                <xsl:call-template name="atomicCoordinates">
                                    <xsl:with-param name="molecule" select="$molecule"/>
                                    <xsl:with-param name="basis" select="$basis"/>
                                </xsl:call-template>
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>MOLECULAR INFO</h3>
                            <xsl:call-template name="symmetrySection"/>
                            <xsl:call-template name="chargeMultiplicity"/>
                            <xsl:call-template name="atomicDistances"/>
                            <xsl:call-template name="geometryRestrictions"/>
                            <xsl:call-template name="solventSection"/>
                        </div>
                    </div>
                    <!-- Results -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                                <xsl:variable name="scfConverged" select="exists(.//cml:module[@id='calculation']//cml:module[@cmlx:templateRef='scf']/cml:scalar[@dictRef='cc:scfConverged'])"/>
                                <div id="module-{generate-id(.)}">
                                    <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"/></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"/></a></small>
                                       <xsl:text> </xsl:text>
                                    </h3>
                                    <xsl:call-template name="populationAnalysis"/>
                                    <xsl:call-template name="electrostaticMoments"/>
                                    <xsl:call-template name="orbitalSpecification"/>
                                    <xsl:call-template name="orbitals"/>
                                    <xsl:call-template name="energy"/>                                   
                                    <xsl:call-template name="tddft"/>
                                    <xsl:call-template name="frequencies"/>
                                    <br/>
                                </div>
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>                            
                        </div>                        
                    </div>
                    
                    <!-- If there are molden orbital files, load its viewer -->
                    <xsl:if test="$hasOrbitals">                                                
                        <xsl:copy-of select="helper:appendMoldenViewerCode($moldenurl, $webrootpath)"/>                        
                    </xsl:if>   
                    
                    
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });                        
                        
                            //Add download action to every dataTable                           
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                       
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                        $(this).find('.dataTable').each(function(index){
                            var oTable = $(this).dataTable();
                            $(oTable).css({ width: $(oTable).parent().width() });
                            oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>
                </div>
            </body>
        </html>
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$program"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$programVersion"/>
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//cml:formula/@concise)[1]"/>
                </td>
            </tr>
            <tr>
                <td>Calculation type:</td>
                <td>
                    <xsl:value-of select="$calcType"/>
                </td>
            </tr>
            <tr>
                <td>Method(s):</td>
                <td><xsl:value-of select="$calcMethod"/>
                    <xsl:if test="exists($functional) 
                               or exists($dispersion) 
                               or exists($parameters)
                               or exists($gridsize)
                               or exists($scfinstab)">
                        <xsl:variable name="parameters" >
                          <xsl:value-of select="$functional"/><xsl:text>&#32;&#32;</xsl:text>
                            <xsl:value-of select="turbo:translateDispersion($dispersion)"/><xsltext>,&#32;&#32;</xsltext>
                          <xsl:value-of select="turbo:translateParameters($parameters)"/><xsl:text>,&#32;&#32;</xsl:text>
                          <xsl:value-of select="turbo:translateGridSize($gridsize)"/><xsl:text>,&#32;&#32;</xsl:text>
                          <xsl:value-of select="turbo:translateSCFinstab(upper-case($scfinstab))"/>                         
                        </xsl:variable>
                        (<xsl:value-of select="replace(replace($parameters, '[,\s]+', ', '),'(^([,\s*]+))|([,\s*]+$)','')"/>)
                    </xsl:if>
                </td>
            </tr>
        </table>
    </xsl:template>
    
    <xsl:template name="atomicCoordinates">
        <xsl:param name="molecule"/>
        <xsl:param name="basis"/>

        <xsl:variable name="collapseAccordion" select="if(count($molecule/cml:atomArray/cml:atom) > 10) then '' else 'in'"/>
        <xsl:variable name="basisAtoms" select="$basis/cml:list[@cmlx:templateRef='basis']/cml:array[@dictRef='cc:atomType']"/>
        <xsl:variable name="basisValues" select="$basis/cml:list[@cmlx:templateRef='basis']/cml:array[@dictRef='t:basis']"/>
        <xsl:variable name="contractionValues" select="$basis/cml:list[@cmlx:templateRef='basis']/cml:array[@dictRef='t:contraction']"/>
        <xsl:variable name="basisInfo" select="turbo:getBasisPerAtomType($basisAtoms, $basisValues, $contractionValues)"/>

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                        Atomic coordinates [&#8491;] <xsl:if test="contains(upper-case($calcType),upper-case($turbo:GeometryOptimization))">
                            <xsl:choose>
                                <xsl:when test="not($isIncomplete)">
                                    <small>(optimized)</small>
                                </xsl:when>
                                <xsl:otherwise>
                                    <small><strong>(calculation did not converge)</strong></small>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:if>
                </h4>
            </div>
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">
                    <div class="row bottom-buffer">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <tr>
                                        <td><xsl:value-of select="$title"/></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                        <xsl:variable name="id" select="@id"/>
                                        <tr>
                                            <td><xsl:value-of select="$elementType"/></td>
                                            <td><xsl:value-of select="format-number(@x3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@y3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@z3,'#0.000000')"/></td>
                                        </tr>
                                    </xsl:for-each>                                       
                                </tbody>                            
                            </table>                              
                            <script type="text/javascript">                                                                  
                                $(document).ready(function() {
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                });
                                function getXYZ(){
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons( table, {
                                    buttons: [ {
                                    extend: 'copyHtml5',
                                    text: 'XYZ'
                                    }
                                    ]
                                    });
                                    table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));                                    
                                    $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                }     
                            </script>                              
                            
                            <table class="display" id="atomicCoordinates">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Atom</th>
                                        <th rowspan="2"></th>
                                        <th rowspan="2" style="text-align: center">x</th>
                                        <th rowspan="2" style="text-align: center">y</th>
                                        <th rowspan="2" style="text-align: center">z</th>
                                        <th colspan="2" style="text-align: center">BASIS SET</th>                                        
                                    </tr>
                                    <tr>
                                        <th>TYPE</th>
                                        <th>(Primitive) / [Contracted]</th>
                                    </tr>
                                </thead>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#atomicCoordinates').dataTable( {
                                "aaData": [
                                /* Reduced data set */
                                <xsl:for-each select="$molecule/cml:atomArray/cml:atom">
                                    <xsl:variable name="elementType" select="@elementType"/>
                                    <xsl:variable name="id" select="@id"/>                      
                                    [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>",
                                    "<xsl:value-of select="format-number(@x3,'#0.0000')"/>",
                                    "<xsl:value-of select="format-number(@y3,'#0.0000')"/>",
                                    "<xsl:value-of select="format-number(@z3,'#0.0000')"/>",
                                    "<xsl:value-of select="$basisInfo/node()[@atomType=$elementType]/@basis"/><xsl:text> </xsl:text><xsl:value-of select="turbo:getEcpType(position(),@elementType)"/>",
                                    "<xsl:value-of select="$basisInfo/node()[@atomType=$elementType]/@contraction"/>"]
                                    <xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                </xsl:for-each>    
                                ],
                                "aoColumns": [
                                null,
                                null,
                                { "sClass": "right" },
                                { "sClass": "right" },
                                { "sClass": "right" },
                               null,
                               null
                                ],   
                                
                                
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                                } );   
                                } );                                       
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="symmetrySection">
        <xsl:if test="exists($symmetry)">
            <div class="row bottom-buffer">
                <div class="col-md-6 col-sm-6">
                    <h4>Symmetry</h4>
                    <table class="display" id="symmetryGroup">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>                            
                        </thead>
                        <tbody>
                            <tr>
                                <td>Symmetry group of the molecule</td>
                                <td class="left">
                                    <xsl:value-of select="$symmetry/cml:scalar[@dictRef='t:symmetryGroup']"/>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top;">Symmetry operators: </td>
                                <td>
                                    <xsl:for-each select="$symmetry/cml:list[@cmlx:templateRef='generators']/cml:scalar">
                                       <xsl:value-of select="."/>
                                       <br/>
                                    </xsl:for-each>   
                                </td>
                            </tr>                            
                        </tbody>
                    </table> 
                    <script>
                        $(document).ready(function() {                 
                            $('table#symmetryGroup').dataTable({ "bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});                               
                        });                            
                    </script>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="chargeMultiplicity">
        <xsl:variable name="charge" select="$electrostaticmoments/cml:scalar[@dictRef='t:charge']"/>
        <xsl:variable name="multiplicity">
            <xsl:choose>                
                <xsl:when test="exists($occupiedAlpha)"><xsl:value-of select="turbo:calculateMultiplicity($occupiedAlphaLabels, $occupiedAlpha, $occupiedBetaLabels, $occupiedBeta)"/></xsl:when>
                <xsl:otherwise><xsl:value-of select="1"/></xsl:otherwise>
            </xsl:choose>            
        </xsl:variable>
        <div class="row bottom-buffer">
            <div class="col-md-2 col-sm-6">
                <table class="display" id="chargemult">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                        </tr>                        
                    </thead>
                    <tbody>
                        <xsl:choose>
                            <xsl:when test="exists($charge)">   <!-- Readed from electrostatic moments -->
                                <tr>
                                    <td>Charge</td>
                                    <td class="right">
                                        <xsl:value-of select="format-number($charge,'#0')"/>
                                    </td>
                                </tr>                                
                            </xsl:when>
                            <xsl:otherwise>                     <!-- Calculated manually -->
                                <tr>
                                    <td>Charge</td>
                                    <xsl:variable name="atoms" select="(//cml:module[@dictRef='cc:calculation']//cml:module[@cmlx:templateRef='basisset'])[last()]"/>                                    
                                    <xsl:choose>
                                        <xsl:when test="exists($occupiedAlpha)">
                                            <xsl:variable name="electrons" select="(//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='orbitals']/cml:scalar[@dictRef='t:occupied'])[last()]"/>
                                            <td class="right"><xsl:value-of select="turbo:calculateCharge($atoms, $electrons, $occupiedAlphaLabels, $occupiedAlpha, $occupiedBetaLabels, $occupiedBeta )"/></td>                                                                                        
                                        </xsl:when>
                                        <xsl:otherwise>
                                            <xsl:variable name="orbitalstats" select="(//cml:module[@cmlx:templateRef='molecular.orbitals.statistics'])[last()]"/>
                                            <xsl:variable name="orbitaltotals" select="$orbitalSpecs/cml:scalar[@dictRef='t:occupied']"/>
                                            <xsl:variable name="electrons">
                                                <xsl:choose>
                                                    <xsl:when test="exists($orbitalstats)">
                                                        <xsl:value-of select="(number($orbitalstats/cml:scalar[@dictRef='t:frozenocc']) + number($orbitalstats/cml:scalar[@dictRef='t:activeocc']))"/>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="number($orbitaltotals)"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>                                                 
                                                
                                            </xsl:variable> 
                                            <td class="right"><xsl:value-of select="turbo:calculateCharge($atoms, $electrons, null, null, null,null)"/></td>                                                                                        
                                        </xsl:otherwise>                                       
                                    </xsl:choose>
                                </tr>                                
                            </xsl:otherwise>
                        </xsl:choose>                          
                        <tr>
                            <td>Multiplicity</td>
                            <td class="right">
                                <xsl:value-of select="$multiplicity"/>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <script>
                    $(document).ready(function() {                 
                        $('table#chargemult').dataTable({ "bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});                               
                    });                            
                </script>
            </div>
        </div>                    
    </xsl:template>

    <xsl:function name="turbo:calculateCharge">        
        <xsl:param name="atoms"/>
        <xsl:param name="electrons"/>
        <xsl:param name="occupiedAlphaLabels" as="item()*"/>
        <xsl:param name="occupiedAlpha" as="item()*"/>
        <xsl:param name="occupiedBetaLabels" as="item()*"/>
        <xsl:param name="occupiedBeta" as="item()*"/>
        
        <xsl:variable name="atomTypes" select="tokenize($atoms//array[@dictRef='cc:atomType'],'\s+')"/>
        <xsl:variable name="atomCount" select="tokenize($atoms//array[@dictRef='t:atoms'],'\s+')"/>
        <xsl:variable name="atomSum">
            <xsl:for-each select="1 to count($atomTypes)">
                <xsl:variable name="outerIndex" select="."/>
                <xsl:element name="atom" namespace="http://www.xml-cml.org/schema">
                    <xsl:variable name="atomCharge" select="helper:atomType2Number(helper:correctAtomTypeCase($atomTypes[$outerIndex]))"/>                        
                    <xsl:attribute name="charge" select="$atomCharge * number($atomCount[$outerIndex])"/>                               
                </xsl:element>
            </xsl:for-each>           
        </xsl:variable>        
        <xsl:choose>
            <xsl:when test="exists($occupiedAlpha)">
                <xsl:variable name="totalOccupiedAlpha" select="sum(turbo:sumRanges($occupiedAlphaLabels, $occupiedAlpha))"/>
                <xsl:variable name="totalOccupiedBeta" select="sum(turbo:sumRanges($occupiedBetaLabels, $occupiedBeta))"/>
                <xsl:variable name="electronCharge" select="$totalOccupiedAlpha + $totalOccupiedBeta"/>               
                <xsl:value-of select="sum($atomSum/atom/@charge) - $electronCharge"/>                        
            </xsl:when>
            <xsl:otherwise>
                <xsl:variable name="electronCharge" select="number($electrons) * 2"/>
                <xsl:value-of select="sum($atomSum/atom/@charge) - $electronCharge"/>                
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>

    <!-- Interatomic distances (bond distances) -->       
    <xsl:template name="atomicDistances">
        <xsl:variable name="molecule" select=".//cml:module[@dictRef='cc:finalization']/cml:molecule"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#bondDistances-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Bond distances                           
                </h4>
            </div>
            <div id="bondDistances-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-4 col-sm-12">
                            <table class="display" id="bondDistancesT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th>Atom1</th>
                                        <th>Atom2</th>
                                        <th class="right">Distance</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="$molecule/cml:bondArray/cml:bond">
                                        <xsl:variable name="atomRefs2" select="tokenize(./@atomRefs2,' ')"/>
                                        <!-- For each bond calculate interatomic distances -->
                                        <xsl:variable name="atomType1" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@elementType"/>
                                        <xsl:variable name="x1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@x3"/>
                                        <xsl:variable name="y1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@y3"/>
                                        <xsl:variable name="z1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@z3"/>
                                        <xsl:variable name="position1" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/preceding-sibling::*)+1"/>
                                        <!-- http://stackoverflow.com/questions/226405/find-position-of-a-node-using-xpath -->
                                        <xsl:variable name="atomType2" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@elementType"/>
                                        <xsl:variable name="x2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@x3"/>
                                        <xsl:variable name="y2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@y3"/>
                                        <xsl:variable name="z2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@z3"/>
                                        <xsl:variable name="position2" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/preceding-sibling::*)+1"/>
                                        <xsl:variable name="distance"  select="ckbk:sqrt((($x2 - $x1) * ($x2 - $x1)) + ($y2 - $y1) * ($y2 - $y1) + ($z2 - $z1) * ($z2 - $z1))"/>
                                        <tr>
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType1"/>
                                                <xsl:value-of select="$position1"/>
                                            </td>                                            
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType2"/>
                                                <xsl:value-of select="$position2"/>
                                            </td>
                                            <td class="right">                                                
                                                <xsl:value-of select="format-number($distance,'#0.000000')"/>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                 
                                    $('table#bondDistancesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable();                               
                                });  
                            </script>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="geometryRestrictions">
        <xsl:if test="exists($restrictions)">
            <xsl:variable name="bond" select="$restrictions/cml:list[@cmlx:templateRef='bond']"/>
            <xsl:variable name="angle" select="$restrictions/cml:list[@cmlx:templateRef='angle']"/>
            <xsl:variable name="dihedral" select="$restrictions/cml:list[@cmlx:templateRef='dihedral']"/>
            <xsl:variable name="outofplane" select="$restrictions/cml:list[@cmlx:templateRef='outofplane']"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#restrictionsCollapse" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Restrictions in the Geometry Optimization
                    </h4>
                </div>
                <div id="restrictionsCollapse" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="restrictions"/>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    var oTable = $('table#restrictions').dataTable({                                            
                                    "aaData": [                                    
                                    <xsl:for-each select="1 to count($bond)">
                                        <xsl:variable name="innerIndex" select="."/>
                                        ["Bond","<xsl:value-of select="$bond[$innerIndex]/cml:scalar[@dictRef='t:atomIndex1']"/>","<xsl:value-of select="$bond[$innerIndex]/cml:scalar[@dictRef='t:atomIndex2']"/>","","","",<xsl:value-of select="format-number(number($bond[$innerIndex]/cml:scalar[@dictRef='t:value']),'#0.000')"/>,"<xsl:value-of select="helper:printUnitSymbol($bond[$innerIndex]/cml:scalar[@dictRef='t:value']/@units)"/>"]
                                        <xsl:text>,</xsl:text>                         
                                    </xsl:for-each>
                                    <xsl:for-each select="1 to count($angle)">
                                        <xsl:variable name="innerIndex" select="."/>
                                        ["Angle","<xsl:value-of select="$angle[$innerIndex]/cml:scalar[@dictRef='t:atomIndex1']"/>","<xsl:value-of select="$angle[$innerIndex]/cml:scalar[@dictRef='t:atomIndex3']"/>","<xsl:value-of select="$angle[$innerIndex]/cml:scalar[@dictRef='t:atomIndex2']"/>","","",<xsl:value-of select="format-number(number($angle[$innerIndex]/cml:scalar[@dictRef='t:value']),'#0.000')"/>,"<xsl:value-of select="helper:printUnitSymbol($angle[$innerIndex]/cml:scalar[@dictRef='t:value']/@units)"/>"]
                                        <xsl:text>,</xsl:text>                         
                                    </xsl:for-each>
                                    <xsl:for-each select="1 to count($dihedral)">
                                        <xsl:variable name="innerIndex" select="."/>
                                        ["Dihedral","<xsl:value-of select="$dihedral[$innerIndex]/cml:scalar[@dictRef='t:atomIndex1']"/>","<xsl:value-of select="$dihedral[$innerIndex]/cml:scalar[@dictRef='t:atomIndex2']"/>","<xsl:value-of select="$dihedral[$innerIndex]/cml:scalar[@dictRef='t:atomIndex3']"/>","<xsl:value-of select="$dihedral[$innerIndex]/cml:scalar[@dictRef='t:atomIndex4']"/>","",<xsl:value-of select="format-number(number($dihedral[$innerIndex]/cml:scalar[@dictRef='t:value']),'#0.000')"/>,"<xsl:value-of select="helper:printUnitSymbol($dihedral[$innerIndex]/cml:scalar[@dictRef='t:value']/@units)"/>"]
                                       <xsl:text>,</xsl:text>                         
                                    </xsl:for-each>
                                    <xsl:for-each select="1 to count($outofplane)">
                                        <xsl:variable name="innerIndex" select="."/>
                                        ["OutOfPlane","<xsl:value-of select="$outofplane[$innerIndex]/cml:scalar[@dictRef='t:atomIndex1']"/>","<xsl:value-of select="$outofplane[$innerIndex]/cml:scalar[@dictRef='t:atomIndex2']"/>","<xsl:value-of select="$outofplane[$innerIndex]/cml:scalar[@dictRef='t:atomIndex3']"/>","<xsl:value-of select="$outofplane[$innerIndex]/cml:scalar[@dictRef='t:atomIndex4']"/>","<xsl:value-of select="$outofplane[$innerIndex]/cml:scalar[@dictRef='t:atomIndex5']"/>",<xsl:value-of select="format-number(number($outofplane[$innerIndex]/cml:scalar[@dictRef='t:value']),'#0.000')"/>,"<xsl:value-of select="helper:printUnitSymbol($outofplane[$innerIndex]/cml:scalar[@dictRef='t:value']/@units)"/>"]
                                        <xsl:text>,</xsl:text>                         
                                    </xsl:for-each>
                                        ["","","","","","","","",""]
                                    ],
                                    "aoColumns": [
                                   
                                    { "sTitle": "Type" },
                                    { "sTitle": "" },
                                    { "sTitle": "" },
                                    { "sTitle": "" },
                                    { "sTitle": "" },
                                    { "sTitle": "" },
                                    { "sTitle": "Value", "sClass": "right" },
                                    { "sTitle": "Units", "sClass": "right" }
                                    ],                                   
                                    "bPaginate": false,
                                    "bScrollCollapse": true,
                                    "bFilter": false,
                                    "bSort": false,
                                    "bInfo": false
                                    });                                       
                                    }); 
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="solventSection">
        <xsl:if test="exists($cosmo)">

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#solventSectionCollapse" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Solvation input 
                    </h4>
                </div>
                <div id="solventSectionCollapse" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <table>
                                    
<!--                                    <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='parameters']/cml:list)">
                                        <tr>
                                            <td>
                                                <b>PARAMETERS:</b>
                                            </td>
                                            <td/>
                                        </tr>
                                        <xsl:for-each select="$cosmo/cml:list[@cmlx:templateRef='parameters']/cml:list">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="./cml:scalar[@dictRef='t:parameter']"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="./cml:scalar[@dictRef='t:value']"/>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:if>
-->                                    
                                   
                                    <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='cavityVolumeArea']/cml:list)">
                                        <tr>
                                            <td>
                                                <b>CAVITY VOLUME/AREA [a.u.]:</b>
                                            </td>
                                            <td/>
                                        </tr>
                                        <tr>
                                            <td>Surface</td>
                                            <td class="right">
                                                <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='cavityVolumeArea']/cml:list/cml:scalar[@dictRef='t:surface']"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>A matrix</td>
                                            <td class="right">
                                                <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='cavityVolumeArea']/cml:list/cml:scalar[@dictRef='t:matrix']"/>
                                            </td>
                                        </tr>
                                        <xsl:for-each select="$cosmo/cml:list[@cmlx:templateRef='cavityVolumeArea']/cml:list">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="./cml:scalar[@dictRef='t:parameter']"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="./cml:scalar[@dictRef='t:value']"/>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:if>
                                    <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='screeningCharge']/cml:list)">
                                        <tr>
                                            <td>
                                                <b>SCREENING CHARGE:</b>
                                            </td>
                                            <td/>
                                        </tr>
                                        <xsl:for-each select="$cosmo/cml:list[@cmlx:templateRef='screeningCharge']/cml:list">
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="./cml:scalar[@dictRef='t:parameter']"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="./cml:scalar[@dictRef='t:value']"/>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </xsl:if>
                                </table>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <div class="row bottom-buffer">
                                    <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='energies'])">
                                        <div class="col-md-12">
                                            <table>
                                                <tr>
                                                    <td>
                                                        <b>ENERGIES [a.u.]:</b>
                                                    </td>
                                                    <td/>
                                                </tr>
                                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='cc:energy'])">
                                                    <tr>
                                                        <td>Total energy</td>
                                                        <td class="right">
                                                            <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='cc:energy']"/>
                                                        </td>
                                                    </tr>                                                                                                        
                                                </xsl:if>
                                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:energyOcCorr'])">
                                                    <tr>
                                                        <td>Total energy + OC corr.</td>
                                                        <td class="right">
                                                            <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:energyOcCorr']"/>
                                                        </td>
                                                    </tr>                                                    
                                                </xsl:if>
                                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:dielectricEnergy'])">
                                                    <tr>
                                                        <td>Dielectric energy</td>
                                                        <td class="right">
                                                            <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:dielectricEnergy']"/>
                                                        </td>
                                                    </tr>                                                    
                                                </xsl:if>
                                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:dielectricEnergyOcCorr'])">
                                                    <tr>
                                                        <td>Diel. energy + OC corr.</td>
                                                        <td class="right">
                                                            <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:dielectricEnergyOcCorr']"/>
                                                        </td>
                                                    </tr>                                                                                                       
                                                </xsl:if>
                                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:energyCorrected'])">
                                                    <tr>
                                                        <td>Total energy corrected</td>
                                                        <td class="right">
                                                            <xsl:value-of select="$cosmo/cml:list[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:energyCorrected']"/>
                                                        </td>
                                                    </tr>                                                      
                                                </xsl:if>
                                            </table>
                                        </div>
                                    </xsl:if>                                                                                                          
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='cc:serial'])">
                                    <!-- RADII format 1 -->                                    
                                    <b>RADII [Angstroem]:</b>
                                    <xsl:variable name="serial" select="tokenize($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='cc:serial'],'\s+')"/>
                                    <xsl:variable name="radius" select="tokenize($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='t:radius'],'\s+')"/>
                                    <table class="display" id="cosmoAtomRadii"/>
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                        $('table#cosmoAtomRadii').dataTable( {
                                        "aaData": [
                                        /* Reduced data set */
                                        <xsl:for-each select="1 to count($serial)">
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            [<xsl:value-of select="$serial[$outerIndex]"/>,<xsl:value-of select="$radius[$outerIndex]"/>]<xsl:if test="(position() &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>    
                                        ],
                                        "aoColumns": [
                                        { "sTitle": "Atom" },
                                        { "sTitle": "Radius" }                                            
                                        ],
                                        "bFilter": false,
                                        "bPaginate": true,
                                        "bSort": false,
                                        "bInfo": false
                                        } );   
                                        } );                                       
                                    </script>                                    
                                </xsl:if>
                                
                                <xsl:if test="exists($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='t:atomrange'])">
                                    <!-- RADII format 2 -->
                                    <b>RADII [Angstroem]:</b>
                                    <xsl:variable name="elementType" select="tokenize($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='cc:elementType'],'\s+')"/>
                                    <xsl:variable name="radii" select="tokenize($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='t:atomicradii'],'\s+')"/>
                                    <xsl:variable name="atomrange" select="tokenize($cosmo/cml:list[@cmlx:templateRef='radii']/cml:array[@dictRef='t:atomrange'],'\s+')"/>
                                    <table class="display" id="cosmoAtomRadii"/>
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                        $('table#cosmoAtomRadii').dataTable( {
                                        "aaData": [
                                        /* Reduced data set */
                                        <xsl:for-each select="1 to count($elementType)">
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            ['<xsl:value-of select="helper:correctAtomTypeCase($elementType[$outerIndex])"/>',<xsl:value-of select="$radii[$outerIndex]"/>, '<xsl:value-of select="$atomrange[$outerIndex]"/>']<xsl:if test="(position() &lt; count($elementType))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>    
                                        ],
                                        "aoColumns": [
                                        { "sTitle": "Atom" },
                                        { "sTitle": "Radius" },
                                        { "sTitle": "Range" },
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                        } );   
                                        } );                                       
                                    </script>                                    
                                </xsl:if>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Results Info -->
    <xsl:template name="populationAnalysis">
        <xsl:variable name="charges" select="(./cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='population.analysis'])[last()]"/>
        <xsl:variable name="fitpointcharges" select="(./cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='fit.pointcharges']/cml:module[@cmlx:templateRef='fitcharges'])[last()]"/>
                
        <xsl:if test="exists($charges) or exists($fitpointcharges)">            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#charges-{generate-id($charges)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Population analysis 
                    </h4>
                </div>
                <div id="charges-{generate-id($charges[1])}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <xsl:for-each select="$charges">
                                <xsl:variable name="chargeModule" select="."/>
                                <xsl:variable name="type" select="$chargeModule/cml:scalar[@dictRef='t:populationtype']"/>
                                <xsl:variable name="population" select="$chargeModule/cml:module[@cmlx:templateRef='populations']"/>
                                <xsl:variable name="spindensity" select="$chargeModule/cml:module[@cmlx:templateRef='spin.density']"/>

                                <div class="col-lg-5 col-md-6 ">
                                    <h4> 
                                        <xsl:choose>
                                            <xsl:when test="$type = 'NATURAL'">Charges from NBO analysis</xsl:when>
                                            <xsl:otherwise><xsl:value-of select="$type"/> charges from total density</xsl:otherwise>
                                        </xsl:choose>
                                     </h4>
                                    <table class="display" id="population-{generate-id($chargeModule)}"/>
                                    <xsl:variable name="serial" select="tokenize($population/cml:array[@dictRef='cc:serial'],'\s+')"/>
                                    <xsl:variable name="type" select="tokenize($population/cml:array[@dictRef='cc:atomType'],'\s+')"/>
                                    <xsl:variable name="charge" select="tokenize($population/cml:array[@dictRef='x:charge'],'\s+')"/>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                        $('table#population-<xsl:value-of select="generate-id($chargeModule)"/>').dataTable( {
                                        "aaData": [                                                                    
                                        <xsl:for-each select="1 to count($serial)">
                                            <xsl:variable name="innerIndex" select="."/>                                       
                                            [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="turbo:correctAtomTypeCase($type[$innerIndex])"/>",'<xsl:value-of select="format-number(number($charge[$innerIndex]),'#0.0000')"/>']<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>                                                   
                                        ],
                                        "aoColumns": [
                                        { "sTitle": "Atom" },
                                        { "sTitle": ""},
                                        { "sTitle": "Charge", "sClass": "right" }
                                        ],
                                        "bFilter": false,                                 
                                        "bPaginate": true,                                    
                                        "bSort": false,
                                        "bInfo": true
                                        } );   
                                        } );                                       
                                    </script>
                                </div>
                                <xsl:if test="exists($spindensity)">
                                    <div class="col-lg-5 col-md-6 ">
                                        <h4> Unpaired electrons from D(alpha)-D(beta)</h4>
                                        <table class="display" id="spinDensity-{generate-id($chargeModule)}"/>
                                        <xsl:variable name="serial" select="tokenize($spindensity/cml:array[@dictRef='cc:serial'],'\s+')"/>
                                        <xsl:variable name="type" select="tokenize($spindensity/cml:array[@dictRef='cc:atomType'],'\s+')"/>
                                        <xsl:variable name="unpairedTotal" select="tokenize($spindensity/cml:array[@dictRef='t:unpairedTotal'],'\s+')"/>
                                        <script type="text/javascript">
                                            $(document).ready(function() {                        
                                            $('table#spinDensity-<xsl:value-of select="generate-id($chargeModule)"/>').dataTable( {
                                            "aaData": [                                                                    
                                            <xsl:for-each select="1 to count($serial)">
                                                <xsl:variable name="innerIndex" select="."/>                                       
                                                [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="turbo:correctAtomTypeCase($type[$innerIndex])"/>",'<xsl:value-of select="format-number(number($unpairedTotal[$innerIndex]),'#0.0000')"/>']<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                            </xsl:for-each>                                                   
                                            ],
                                            "aoColumns": [
                                            { "sTitle": "Atom" },
                                            { "sTitle": ""},
                                            { "sTitle": "Total", "sClass": "right" }
                                            ],
                                            "bFilter": false,                                 
                                            "bPaginate": true,                                    
                                            "bSort": false,
                                            "bInfo": true
                                            } );   
                                            } );                                       
                                        </script>
                                    </div>
                                </xsl:if>
                                
                            </xsl:for-each>
                                               
                            <xsl:if test="exists($fitpointcharges)">
                                <xsl:variable name="serial" select="tokenize($fitpointcharges/cml:array[@dictRef='cc:serial'],'\s+')"/>
                                <xsl:variable name="type" select="tokenize($fitpointcharges/cml:array[@dictRef='cc:elementType'],'\s+')"/>
                                <xsl:variable name="charge" select="tokenize($fitpointcharges/cml:array[@dictRef='t:charge'],'\s+')"/>
                                <div class="col-lg-5 col-md-6">                                    
                                    <h4>Charges resulting from fit to electrostatic potential</h4>
                                    <table class="display" id="fitPointCharges-{generate-id($fitpointcharges)}"/>
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                        $('table#fitPointCharges-<xsl:value-of select="generate-id($fitpointcharges)"/>').dataTable( {
                                        "aaData": [                                                                    
                                        <xsl:for-each select="1 to count($serial)">
                                            <xsl:variable name="innerIndex" select="."/>                                       
                                            [ <xsl:value-of select="$serial[$innerIndex]"/>,"<xsl:value-of select="turbo:correctAtomTypeCase($type[$innerIndex])"/>",'<xsl:value-of select="$charge[$innerIndex]"/>']<xsl:if test="($innerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>                                                   
                                        ],
                                        "aoColumns": [
                                        { "sTitle": "Atom" },
                                        { "sTitle": ""},
                                        { "sTitle": "Charge", "sClass": "right" }
                                        ],
                                        "bFilter": false,                                 
                                        "bPaginate": true,                                    
                                        "bSort": false,
                                        "bInfo": true
                                        } );   
                                        } );                                       
                                    </script>                                    
                                </div>
                                
                            </xsl:if>                                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="electrostaticMoments">
        <xsl:variable name="electroMoments" select="(./cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='electrostatic.moments'])[last()]"/>
        <xsl:if test="exists($electroMoments)">            
            <xsl:variable name="dipole" select="tokenize($electroMoments/cml:array[@dictRef='cc:dipole'],'\s+')"/>
            <xsl:variable name="quadrupole" select="tokenize($electroMoments/cml:array[@dictRef='cc:quadrupole'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#dipole-{generate-id($electroMoments)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Electrostatic moments 
                    </h4>
                </div>
                <div id="dipole-{generate-id($electroMoments)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-md-6 mb-4">
                                <h4>Charge</h4>
                                <table class="display" id="charge">
                                    <thead>
                                        <tr>
                                            <th></th>                                          
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><xsl:value-of select="$electroMoments/cml:scalar[@dictRef='t:charge']"/></td>
                                        </tr>                     
                                    </tbody>
                                </table>                       
                                <h4 class="mt-4">Dipole moment</h4>
                                <xsl:choose>
                                    <xsl:when test="count($dipole) = 9">
                                        <table class="display" id="dipole">                                    
                                            <thead>
                                                <tr>
                                                    <th> </th>
                                                    <th style="text-align:right">NUC</th>
                                                    <th style="text-align:right">ELEC</th>
                                                    <th style="text-align:right">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>x</td>
                                                    <td class="right"><xsl:value-of select="$dipole[1]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[2]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[3]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>y</td>
                                                    <td class="right"><xsl:value-of select="$dipole[4]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[5]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[6]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>z</td>
                                                    <td class="right"><xsl:value-of select="$dipole[7]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[8]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[9]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>&#956; [Debye]</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="right"><xsl:value-of select="$electroMoments/cml:scalar[@dictRef='t:debye']"/></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <table class="display" id="dipole">                                    
                                            <thead>
                                                <tr>
                                                    <th rowspan="2" ></th>
                                                    <th rowspan="2" style="text-align:center">NUC</th>
                                                    <th colspan="2" style="text-align:center">GS</th>
                                                    <th colspan="2" style="text-align:center">Excited state</th>                                                    
                                                </tr>
                                                <tr>
                                                   <th style="text-align:right">ELECT</th>
                                                   <th style="text-align:right">TOTAL</th>
                                                   <th style="text-align:right">ELECT</th> 
                                                   <th style="text-align:right">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>x</td>
                                                    <td class="right"><xsl:value-of select="$dipole[1]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[2]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[3]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[4]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[5]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>y</td>
                                                    <td class="right"><xsl:value-of select="$dipole[6]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[7]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[8]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[9]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[10]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>z</td>
                                                    <td class="right"><xsl:value-of select="$dipole[11]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[12]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[13]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[14]"/></td>
                                                    <td class="right"><xsl:value-of select="$dipole[15]"/></td>
                                                </tr>
                                                <tr>
                                                    <xsl:variable name="tmpgsdebye" as="element()*">
                                                        <Item><xsl:value-of select="$dipole[3]"/></Item>
                                                        <Item><xsl:value-of select="$dipole[8]"/></Item>
                                                        <Item><xsl:value-of select="$dipole[13]"/></Item>
                                                    </xsl:variable>
                                                    <xsl:variable name="gsdebye" select="helper:calcAxisLength($tmpgsdebye)"></xsl:variable>
                                                    <td>&#956; [Debye]</td>
                                                    <td></td>
                                                    <td></td>
                                                    <td class="right"><xsl:value-of select="format-number(number($gsdebye) * $AU_TO_DEBYE,'#0.0000') "/></td>                                                    
                                                    <td></td>                                                    
                                                    <td class="right"><xsl:value-of select="$electroMoments/cml:scalar[@dictRef='t:debye']"/></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </xsl:otherwise>
                                </xsl:choose>                                                                                             
                            </div>
                            
                            <div class="col-lg-6 col-md-6 ">
                                <xsl:choose>
                                    <xsl:when test="count($quadrupole) = 18">
                                        <h4>Quadrupole moment</h4>
                                        <table class="display" id="quadrupole">
                                            <thead>
                                                <tr>
                                                    <th> </th>
                                                    <th style="text-align:right">NUC</th>
                                                    <th style="text-align:right">ELEC</th>
                                                    <th style="text-align:right">TOTAL</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>xx</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[1]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[2]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[3]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>yy</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[4]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[5]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[6]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>zz</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[7]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[8]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[9]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>xy</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[10]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[11]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[12]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>xz</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[13]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[14]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[15]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>yz</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[16]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[17]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[18]"/></td>
                                                </tr>
                                            </tbody>
                                        </table> 
                                        
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <h4>Quadrupole moment</h4>
                                        <table class="display" id="quadrupole">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2"></th>
                                                    <th rowspan="2" style="text-align:center">NUC</th>
                                                    <th colspan="2" style="text-align:center">GS</th>
                                                    <th colspan="2" style="text-align:center">Excited state</th>                                                    
                                                </tr>
                                                <tr>
                                                    <th style="text-align:right">ELECT</th>
                                                    <th style="text-align:right">TOTAL</th>
                                                    <th style="text-align:right">ELECT</th> 
                                                    <th style="text-align:right">TOTAL</th>
                                                </tr>                                                
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>xx</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[1]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[2]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[3]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[4]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[5]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>yy</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[6]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[7]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[8]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[9]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[10]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>zz</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[11]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[12]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[13]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[14]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[15]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>xy</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[16]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[17]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[18]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[19]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[20]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>xz</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[21]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[22]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[23]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[24]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[25]"/></td>
                                                </tr>
                                                <tr>
                                                    <td>yz</td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[26]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[27]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[28]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[29]"/></td>
                                                    <td class="right"><xsl:value-of select="$quadrupole[30]"/></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </xsl:otherwise>
                                </xsl:choose>                              
                            </div>
                            <div class="col-lg-6 col-md-6 mt-3">
                                <table class="display" id="anisotropy">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>1/3 trace</td>
                                            <td class="right">
                                                <xsl:value-of select="$electroMoments/cml:scalar[@dictRef='t:onethirdtrace']"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Anisotropy</td>
                                            <td class="right">
                                                <xsl:value-of select="$electroMoments/cml:scalar[@dictRef='t:anisotropy']"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>                            
                        </div>
                    </div>
                </div>

                <script type="text/javascript">
                    $(document).ready(function() {
                        $('table#charge').dataTable({"bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});
                        $('table#dipole').dataTable({"bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});
                        $('table#quadrupole').dataTable({"bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});
                        $('table#anisotropy').dataTable({"bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});
                    });  
                </script>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="orbitalSpecification">
        <xsl:if test="exists($orbitalSpecs)">
            <xsl:variable name="label" select="tokenize($orbitalSpecs/cml:list[@cmlx:templateRef='mooccupation']/cml:array[@dictRef='t:irrep'],'\s+')"/>
            <xsl:variable name="totalmos" select="tokenize($orbitalSpecs/cml:list[@cmlx:templateRef='mooccupation']/cml:array[@dictRef='t:numberofmos'],'\s+')"/>

            <xsl:variable name="mosperlabel">
                <xsl:for-each select="1 to count($label)">
                    <xsl:variable name="outerIndex" select="position()"/>
                    <xsl:element name="orbital" namespace="http://www.xml-cml.org/schema">
                        <xsl:attribute name="label" select="$label[$outerIndex]"/>
                        <xsl:attribute name="total" select="turbo:getNumberOfBasis($totalmos[$outerIndex],$label[$outerIndex])"/>
                    </xsl:element>
                </xsl:for-each>
            </xsl:variable>

            <xsl:variable name="occupied">
                <xsl:variable name="labels" select="tokenize($orbitalSpecsRestricted/cml:array[@dictRef='t:label'],'\s+')"/>
                <xsl:variable name="mosrange" select="tokenize(replace($orbitalSpecsRestricted/cml:array[@dictRef='t:mosrange'],'\s*-\s*','-'),'\s+')"/>
                <xsl:for-each select="1 to count($labels)">
                    <xsl:variable name="outerIndex" select="."/>
                    <xsl:element name="occupied" namespace="http://www.xml-cml.org/schema">
                        <xsl:attribute name="label" select="$labels[$outerIndex]"/>
                        <xsl:attribute name="mosrange" select="$mosrange[$outerIndex]"/>
                    </xsl:element>
                </xsl:for-each>                
            </xsl:variable>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbitals-{generate-id($orbitalSpecs)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Orbitals specifications 
                    </h4>
                </div>
                <div id="orbitals-{generate-id($orbitalSpecs)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-md-6 ">
                                <table class="display" id="orbitalsT-{generate-id($orbitalSpecs)}">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <xsl:for-each select="1 to count($label)">
                                                <th></th>
                                            </xsl:for-each>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Serial</td>
                                            <xsl:for-each select="1 to count($label)">
                                                <xsl:variable name="outerIndex" select="position()"/>
                                                <td>
                                                    <xsl:value-of select="$outerIndex"/>
                                                </td>
                                            </xsl:for-each>
                                        </tr>
    
                                        <tr>
                                            <td>Label</td>
                                            <xsl:for-each select="$label">
                                                <td>
                                                    <xsl:value-of select="."/>
                                                </td>
                                            </xsl:for-each>
                                        </tr>
                                        <xsl:if test="exists($orbitalSpecsRestricted)">
                                            <tr>
                                                <td>Occupied orbitals</td>
                                                <xsl:for-each select="1 to count($label)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <td>                                                      
                                                        <xsl:value-of select="turbo:getRangeValue('', $occupied/occupied[@label = $label[$outerIndex]]/@mosrange)"/>
                                                    </td>
                                                </xsl:for-each>
                                            </tr>
                                            <tr>
                                                <td>Secondary orbitals</td>
                                                <xsl:for-each select="1 to count($label)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <td>                                                                                                        
                                                        <xsl:value-of select="$mosperlabel/orbital[@label=$label[$outerIndex]]/@total - number(turbo:getRangeValue('', $occupied/occupied[@label = $label[$outerIndex]]/@mosrange))"/>
                                                    </td>
                                                </xsl:for-each>
                                            </tr>
                                        </xsl:if>
    
                                        <xsl:if test="exists($occupiedAlpha)">
                                            <tr>
                                                <td>Occupied orbitals alpha</td>
                                                <xsl:for-each select="$label">
                                                    <td>
                                                        <xsl:value-of select="turbo:getOrbitalRangeValue(., $occupiedAlphaLabels, $occupiedAlpha)"/>
                                                    </td>                                                
                                                </xsl:for-each>                                           
                                            </tr>
                                            <tr>
                                                <td>Occupied orbitals beta</td>
                                                <xsl:for-each select="$label">                                                  
                                                    <td>
                                                        <xsl:value-of select="turbo:getOrbitalRangeValue(., $occupiedBetaLabels, $occupiedBeta)"/>                                                    
                                                    </td>
                                                </xsl:for-each>
                                            </tr>
                                            <tr>
                                                <td>Secondary orbitals alpha</td>
                                                <xsl:for-each select="$label">
                                                    <xsl:variable name="currentLabel" select="."/>
                                                    <td>
                                                        <xsl:value-of select="$mosperlabel/orbital[@label = $currentLabel]/@total - number(turbo:getOrbitalRangeValue($currentLabel, $occupiedAlphaLabels, $occupiedAlpha))"/>
                                                    </td>
                                                </xsl:for-each>
                                            </tr>
    
                                            <tr>
                                                <td>Secondary orbitals beta</td>
                                                <xsl:for-each select="$label">
                                                    <xsl:variable name="currentLabel" select="."/>
                                                    <td>
                                                        <xsl:value-of select="$mosperlabel/orbital[@label = $currentLabel]/@total - number(turbo:getOrbitalRangeValue($currentLabel, $occupiedBetaLabels, $occupiedBeta))"/>
                                                    </td>
                                                </xsl:for-each>                                                                                              
                                            </tr>
                                        </xsl:if>
                                        <tr>
                                            <td>Number of basis functions</td>
                                            <xsl:for-each select="$label">
                                                <xsl:variable name="outerIndex" select="position()"/>
                                                <td>
                                                    <xsl:value-of select="$mosperlabel/orbital[@label=$label[$outerIndex]]/@total"/>
                                                </td>
                                            </xsl:for-each>
                                        </tr>
                                    </tbody>
                                </table>
                                
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('table#orbitalsT-<xsl:value-of select="generate-id($orbitalSpecs)"/>').dataTable({"bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});                                  
                                    });  
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>



    <!-- Frontier orbitals -->
    <xsl:template name="orbitals">
        <xsl:variable name="orbitals" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='orbitalenergies'])[last()]"/>
        <xsl:if test="exists($orbitals)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbitals-{generate-id($orbitals)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Frontier orbitals                            
                    </h4>
                </div>
                <div id="orbitals-{generate-id($orbitals)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <xsl:for-each select="$orbitals/cml:module">
                                <xsl:variable name="id" select="generate-id(.)"/>
                                <div class="col-md-6 col-sm-6 col-xs-12">                                    
                                    <xsl:variable name="irrep" select="tokenize(./cml:array[@dictRef='cc:irrep'],'\|')"/>
                                    <xsl:variable name="occup" select="tokenize(./cml:array[@dictRef='cc:occupation'],'\s+')"/>
                                    <xsl:variable name="energy" select="tokenize(./cml:array[@dictRef='t:orbitalenergy'],'\s+')"/>
                                    <xsl:variable name="spin" select="./cml:scalar[@dictRef='t:spin']" />
                                    <xsl:if test="exists($spin)">
                                        <h4>Spin <xsl:value-of select="$spin"/> orbitals</h4>
                                    </xsl:if>
                                    <input type="radio" value="all" name="orbitalrangetype-{$id}" onclick="javascript:filterTable(0,'{$id}')"/>All
                                    <input type="radio" value="homolumo" name="orbitalrangetype-{$id}" onclick="javascript:filterHomoLumo('{$id}')" checked="checked"/>Homo/Lumo range:
                                    <input type="text"  value="5"       id="orbitalrange-{$id}" oninput="javascript:filterHomoLumo('{$id}')" />
                                    <table class="display" id="orbitalsT-{$id}"></table>
                                    
                                    <script type="text/javascript">                                        
                                        var molecularOrbitalsData<xsl:value-of select="$id"/> = [
                                            <xsl:for-each select="$irrep">                                                
                                                <xsl:variable name="outerIndex" select="position()"/>
                                                ["<xsl:value-of select="turbo:escapeQuotesJavascript($irrep[$outerIndex])"/>","<xsl:value-of select="if(count($occup) &lt;$outerIndex) then '0.0000' else $occup[$outerIndex]"/>","<xsl:value-of select="$energy[$outerIndex]"/>"]<xsl:if test="($outerIndex &lt; count($irrep))"><xsl:text>,</xsl:text></xsl:if>
                                            </xsl:for-each>                                        
                                        ];
                                        
                                        var molecularOrbitalsDataShort<xsl:value-of select="$id"/> = null;                                        
                                       
                                        $(document).ready(function() {                        
                                            $('table#orbitalsT-<xsl:value-of select="$id"/>').dataTable({
                                                "aaData": molecularOrbitalsData<xsl:value-of select="$id"/>,
                                                "aoColumns": [
                                                    { "sTitle": "No"    },
                                                    { "sTitle": "Occ", "sClass": "right"  },
                                                    { "sTitle": "E(eV)", "sClass": "right" }                                                
                                                ],
                                                "bFilter": false,
                                                "bPaginate": true,
                                                "bSort": false,
                                                "bInfo": false
                                                <xsl:if test="$hasOrbitals">
                                                    ,"columnDefs" : [{  
                                                    'targets': 0,
                                                    'render' : function ( data, type, row, meta ) {                                                     
                                                    return '&lt;a href="javascript:displayMoldenOrbital(\'' + data + '\')">' + data + '&lt;/a>';                                                                                                        
                                                    }
                                                    }]
                                                </xsl:if>                                                
                                            });
                                        });
                                        
                                      
                                        var homoLumoFrontier = null;
                                        
                                        function filterHomoLumo(id){
                                            $("input[name='orbitalrangetype-" + id + "'][value='homolumo']").prop('checked',true);
                                            var range = $('input#orbitalrange-' + id).val();
                                            if(!$.isNumeric(range))               
                                                range = 10;                                    
                                            filterTable(range, id);               
                                        }
                                        
                                        function findHomoRange(startIndex, range, id){
                                            energyChangesCounter = 0;
                                            for(inx = startIndex - 1; inx > 0 ; inx-- ){
                                                if(window['molecularOrbitalsData' + id][inx][1].length != 0)                                          
                                                    energyChangesCounter++;                                                                                                                    
                                                if(energyChangesCounter == range)
                                                    return homoLumoFrontier - inx ;                                            
                                            }
                                            return 0;                                                                        
                                        }
                                        
                                        function findLumoRange(startIndex, range, id){
                                            energyChangesCounter = 0;    
                                            for(inx = startIndex + 1; inx &#60; window['molecularOrbitalsData' + id].length ; inx++ ){
                                                if(window['molecularOrbitalsData' + id][inx][1].length != 0)             
                                                    energyChangesCounter++;                                                                                            
                                                if(energyChangesCounter == range)
                                                    return (inx -1) - homoLumoFrontier;                                            
                                            }
                                            return window['molecularOrbitalsData' + id].length - 1;
                                        }                                    
                                        
                                        function filterTable(range, id) {                                                                                                            
                                            findHomoLumoFrontier(id);
                                            if(homoLumoFrontier == -1 || range == 0) {
                                                window['molecularOrbitalsDataShort' + id] = window['molecularOrbitalsData' + id];
                                            } else {                                        
                                                var lumoRange = findLumoRange(homoLumoFrontier, range, id);
                                                var homoRange = findHomoRange(homoLumoFrontier, range, id);
                                                
                                                var homoOrbitals = window['molecularOrbitalsData' + id].slice(homoLumoFrontier - Number(homoRange) , homoLumoFrontier );
                                                var lumoOrbitals = window['molecularOrbitalsData' + id].slice(homoLumoFrontier , homoLumoFrontier + Number(lumoRange) + 1);
                                                window['molecularOrbitalsDataShort' + id] = homoOrbitals.concat(lumoOrbitals);
                                            }                                    
                                            var table = $('#orbitalsT-' + id).DataTable();
                                            table.clear();
                                            table.rows.add(window['molecularOrbitalsDataShort' + id]);
                                            table.draw();                        
                                        }
                                        
                                       
                                        function findHomoLumoFrontier(id){                                    
                                            homoLumoFrontier = -1;
                                            for(inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> window['molecularOrbitalsData' + id].length; inx++){
                                                if((window['molecularOrbitalsData' + id][inx][1] == 0) <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> (window['molecularOrbitalsData' + id][inx][1].length != 0 )){
                                                    homoLumoFrontier = inx;
                                                    break;
                                                }
                                            }                                
                                            return homoLumoFrontier;
                                        }
                                    </script>                                
                                </div>                                
                            </xsl:for-each>                                         
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
        
    </xsl:template>


    <xsl:template name="energy">
        <xsl:variable name="energy" select="./cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='t:energy']/cml:scalar"/>
        <xsl:variable name="energyresume" select="./cml:module[@dictRef='cc:finalization']/cml:module[@dictRef='cc:userDefinedModule']/cml:module[@id='energy.resume']"/>        
        <xsl:variable name="dispersionCorrectionType" select="//cml:module[@cmlx:templateRef='job'][1]/cml:module[@dictRef='cc:initialization']/cml:module[@id='otherComponents']"/>
        <xsl:variable name="nuclearRepulsion" select="./cml:module[@dictRef='cc:finalization']/cml:module[@dictRef='cc:userDefinedModule']/cml:module[@cmlx:templateRef='nuclear.repulsion']"/>
        <xsl:variable name="zeroPoint" select="(./cml:module[@dictRef='cc:finalization']/cml:propertyList/cml:property[@dictRef='t:zeropoint']/cml:scalar)[last()]"/>        
        
        <xsl:if test="exists($energy)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#energy-{generate-id($energy)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Final results 
                    </h4>
                </div>
                <div id="energy-{generate-id($energy)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-8 col-md-8 ">
                                <table class="display" id="energyT-{generate-id($energy)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <xsl:if test="not(exists($energyresume/cml:scalar[@dictRef='t:rhfEnergy']))">
                                            <tr>
                                                <td>Total energy <xsl:if test="exists($functional)">
                                                    <xsl:value-of select="$functional"/>
                                                </xsl:if>
                                                </td>
                                                <td  class="right">
                                                    <xsl:value-of select="$energy"/>
                                                </td>
                                                <td>Eh <xsl:if test="contains($calcType, $turbo:ExcitedState) and contains($calcType, $turbo:GeometryOptimization)">(GS)</xsl:if></td>
                                            </tr>                                                                                        
                                        </xsl:if>
                                        <xsl:if test="contains($calcType, $turbo:ExcitedState) and contains($calcType, $turbo:GeometryOptimization)">
                                            <xsl:choose>
                                                <xsl:when test="exists($exopt)">
                                                    <xsl:variable name="excitationmodule" select="//cml:module[@cmlx:templateRef='excitation'][descendant::cml:scalar[@dictRef='t:serial'] = $exopt and descendant-or-self::cml:scalar[@dictRef='t:label'] = $soes/cml:array[@dictRef='t:irrep']]"/>                                                                       
                                                    <tr>
                                                        <td></td>
                                                        <td class="right"><xsl:value-of select="$excitationmodule/cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:totalEnergy']"/></td>
                                                        <td>Eh (<xsl:value-of select="$exopt"/><xsl:value-of select="$soes/cml:array[@dictRef='t:irrep']"/>)</td>
                                                    </tr>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:variable name="excitationmodule" select="//cml:module[@cmlx:templateRef='excitation'][descendant::cml:scalar[@dictRef='t:serial'] = $soes/cml:array[@dictRef='t:lowest'] and descendant-or-self::cml:scalar[@dictRef='t:label'] = $soes/cml:array[@dictRef='t:irrep']]"/>                                                                       
                                                    <tr>
                                                        <td></td>
                                                        <td class="right"><xsl:value-of select="$excitationmodule/cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:totalEnergy']"/></td>
                                                        <td>Eh (<xsl:value-of select="$soes/cml:array[@dictRef='t:lowest']"/><xsl:value-of select="$soes/cml:array[@dictRef='t:irrep']"/>)</td>
                                                    </tr>                                                    
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </xsl:if>
                                        <xsl:if test="exists($energyresume/cml:scalar[@dictRef='t:rhfEnergy'])">
                                            <tr>
                                                <td>
                                                    RHF energy
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="$energyresume/cml:scalar[@dictRef='t:rhfEnergy']"/>
                                                </td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($energyresume/cml:scalar[@dictRef='t:rhfEnergy']/@units)"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:for-each select="distinct-values($energyresume/module/@dictRef)">
                                            <xsl:variable name="methodName" select="."/>
                                            <xsl:variable name="method" select="($energyresume/module[@dictRef=$methodName])[last()]"/>
                                            <tr>
                                                <td>
                                                    <xsl:value-of select="$methodName" /> energy
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="$method/cml:scalar"/>
                                                </td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($method/cml:scalar/@units)"/></td>
                                            </tr>                                           
                                        </xsl:for-each>
                                        <xsl:if test="exists($nuclearRepulsion)">
                                            <tr>
                                                <td><xsl:choose>
                                                        <xsl:when test="lower-case($dispersionCorrectionType) = 'disp3'">D3 Dispersion correction</xsl:when>
                                                        <xsl:when test="lower-case($dispersionCorrectionType) = 'olddisp'">D1 Dispersion correction</xsl:when>
                                                        <xsl:when test="lower-case($dispersionCorrectionType) = 'disp'">D2 dispersion correction</xsl:when>
                                                        <xsl:otherwise>Empirical dispersive energy correction</xsl:otherwise>
                                                    </xsl:choose>                                                    
                                                </td>
                                                <td class="right"><xsl:value-of select="($nuclearRepulsion/cml:scalar[@dictRef='t:empdispcorrection'])[last()]"/></td>
                                                <td></td>
                                            </tr>                                                                                        
                                        </xsl:if>
                                        <xsl:if test="exists($energyresume/cml:scalar[@dictRef='t:d1diagnostic'])">
                                            <tr>
                                                <td>
                                                    D1 diagnostic
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="$energyresume/cml:scalar[@dictRef='t:d1diagnostic']"/>
                                                </td>
                                                <td></td>
                                            </tr>                                            
                                        </xsl:if>    
                                        <xsl:if test="exists($zeroPoint)">
                                            <tr>
                                                <td>Zero point vibrational energy</td>
                                                <td class="right"><xsl:value-of select="$zeroPoint"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeroPoint/@units)"/></td>                                                
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($occupiedAlpha)">
                                            <xsl:variable name="alphabetaMultiplicity" select="turbo:calculateMultiplicity($occupiedAlphaLabels, $occupiedAlpha, $occupiedBetaLabels,  $occupiedBeta)"/>
                                            <xsl:variable name="expectedS2" select="turbo:calculateS2($alphabetaMultiplicity)"/>
                                            <tr>
                                                <td>Multiplicity (from alpha-beta)</td>
                                                <td class="right">
                                                    <xsl:value-of select="$alphabetaMultiplicity"/>
                                                </td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>&#60;S^2&#62;</td>
                                                <td class="right"><xsl:value-of select="format-number($s2,'#0.000')"/></td>
                                                <td>(expected value: <xsl:value-of select="format-number($expectedS2,'#0.000')"/>)</td>
                                            </tr>
                                        </xsl:if>
                                    </tbody>
                                </table>                                
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('table#energyT-<xsl:value-of select="generate-id($energy)"/>').dataTable({"bFilter": false, "bPaginate": false, "bSort": false, "bInfo": false});                                  
                                    });  
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
  
    <xsl:template name="tddft">
        <xsl:if test="exists($tddftRoots)">
            <xsl:variable name="EV_TO_CM_1" select="8065.5446"/>
            <xsl:variable name="EV_TO_NM" select="1239.84187"/>
            <xsl:variable name="groundState" select="//cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='ground.state']/cml:scalar[@dictRef='t:groundenergy']"/>
            <!-- Units step -->
            <xsl:variable name="bandwidthev" select="0.15"/>
            <xsl:variable name="bandwidthcm1" select="1200"/>
            <xsl:variable name="bandwidthnm" select="20"/>
            <!-- Root state calculation -->
            <xsl:variable name="rootState">
                <xsl:if test="exists($orbitalSpecsAlpha)">                                                                                                                                       
                    <xsl:variable name="multiplicity" select="turbo:calculateMultiplicity($occupiedAlphaLabels, $occupiedAlpha, $occupiedBetaLabels, $occupiedBeta)"/>
                    <xsl:choose>
                        <xsl:when test="$multiplicity = 1">singlet</xsl:when>
                        <xsl:when test="$multiplicity = 2">doublet</xsl:when>
                        <xsl:when test="$multiplicity = 3">triplet</xsl:when>
                        <xsl:when test="$multiplicity = 4">quartet</xsl:when>
                        <xsl:when test="$multiplicity = 5">quintet</xsl:when>                        
                        <xsl:when test="$multiplicity = 6">sextet</xsl:when>
                        <xsl:when test="$multiplicity = 7">septet</xsl:when>
                        <xsl:when test="$multiplicity = 8">octet</xsl:when>
                        <xsl:when test="$multiplicity = 9">nonet</xsl:when>
                    </xsl:choose>
                </xsl:if>
                <xsl:if test="not(exists($orbitalSpecsAlpha))">singlet</xsl:if>
            </xsl:variable>
            <xsl:variable name="statesError" select="$rootState = 'singlet' and $tddftRoots[1]/cml:list[@cmlx:templateRef='label']/cml:scalar[@dictRef='t:type'] = 'triplet'"/>

            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#tddft" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        TDHF / TDDFT 
                    </h4>
                </div>
                <div id="tddft" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <table class="display" id="roots">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="text-align:left">Root</th>                                            
                                            <th rowspan="2" style="text-align:left">Spin</th>
                                            <th rowspan="2" style="text-align:right">Total energy (au)</th>
                                            <th rowspan="2" style="text-align:right">∆E (eV)</th>
                                            <th rowspan="2" style="text-align:right">∆E (cm-1)</th>
                                            <th rowspan="2" style="text-align:right">nm</th>
                                            <th rowspan="2" style="text-align:right">osc. strength</th>
                                            <th colspan="4" style="text-align:center">Transition dipole moment</th>
                                        </tr>                                        
                                        <tr>                                           
                                            <th style="text-align:right">x</th>
                                            <th style="text-align:right">y</th>
                                            <th style="text-align:right">z</th>
                                            <th style="text-align:right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GS</td>
                                            <td>                                                                                               
                                                <xsl:value-of select="$rootState"/>                                                
                                            </td>
                                            <td class="right">
                                                <xsl:value-of select="format-number($groundState,'#.000000')"/>
                                            </td>
                                            <td class="right">0.00</td>
                                            <td class="right">0</td>
                                            <td class="right">0.0000</td>
                                            <td class="right"/>
                                            <td class="right"/>
                                            <td class="right"/>
                                            <td class="right"/>
                                            <td class="right"/>
                                        </tr>
                                        <xsl:for-each select="1 to count($tddftRoots)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <tr>
                                                <td>                                                   
                                                    <xsl:value-of select="$tddftRoots[$outerIndex]/cml:list[@cmlx:templateRef='label']/cml:scalar[@dictRef='t:serial']"/>
                                                    <xsl:value-of select="$tddftRoots[$outerIndex]/cml:list[@cmlx:templateRef='label']/cml:scalar[@dictRef='t:label']"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="$tddftRoots[$outerIndex]/cml:list[@cmlx:templateRef='label']/cml:scalar[@dictRef='t:type']"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="format-number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:totalEnergy'], '#.000000')"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="format-number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:excitationEnergy'], '#.00')"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="format-number(($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:excitationEnergy']) * $EV_TO_CM_1, '#')"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="format-number( $EV_TO_NM div number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:excitationEnergy']), '#.000')"/>
                                                </td>
                                                <xsl:choose>
                                                    <xsl:when test="not($statesError)">
                                                        <td class="right">                                                   
                                                            <xsl:value-of select="helper:formatScientific(number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='oscillator']/cml:scalar[@dictRef='t:velocity']),2)"/>
                                                        </td>
                                                        <td class="right">
                                                            <xsl:value-of select="helper:formatScientific(number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='dipole']/cml:scalar[@dictRef='cc:x3']),2)"/>
                                                        </td>
                                                        <td class="right">
                                                            <xsl:value-of select="helper:formatScientific(number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='dipole']/cml:scalar[@dictRef='cc:y3']),2)"/>
                                                        </td>
                                                        <td class="right">
                                                            <xsl:value-of select="helper:formatScientific(number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='dipole']/cml:scalar[@dictRef='cc:z3']),2)"/>
                                                        </td>
                                                        <td class="right">
                                                            <xsl:value-of select="helper:formatScientific(number($tddftRoots[$outerIndex]/cml:module[@cmlx:templateRef='dipole']/cml:scalar[@dictRef='t:norm']),2)"/>
                                                        </td>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>                                                        
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Root plot -->                          
                            <div class="col-md-12">
                                <div id="dipoleTransitionSpinFreePlotlyContainer" style="min-height:450px; width: 100%" class=" d-flex"/>
                            </div>                            
                            
                            <div class="col-sm-12 col-md-12">
                                <div id="dipoleTransitionSpinFreeContainerControls">
                                    <p style="text-align:right;padding:0px;font-weight: 400;">
                                        <input type="radio" name="tddftGraphUnits" value="ev" checked="true" onclick="tddftParametersChanged()">eV</input>
                                        <input type="radio" name="tddftGraphUnits" value="cm1" onclick="tddftParametersChanged()">cm-1</input>
                                        <input type="radio" name="tddftGraphUnits" value="nm" onclick="tddftParametersChanged()">nm</input>
                                    </p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">Bandwidth: <input id="bandWidthSpinFree" type="text" size="5" value="0.15"/>
                                    </p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">min X: <input id="minXSpinFree" type="text" size="5" value="0"/>
                                    </p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">max X: <input id="maxXSpinFree" type="text" size="5" value="10"/>
                                    </p>
                                    <p style="text-align:right;padding:0px">
                                        <input type="button" value="Change" id="changeDipoleTransitionParametersButton" onclick="generateTddftChart()"/>
                                    </p>
                                    <p/>
                                </div>

                                <xsl:variable name="bandwidthev" select="0.15"/>
                                <xsl:variable name="bandwidthcm1" select="1200"/>
                                <xsl:variable name="bandwidthnm" select="20"/>

                                <xsl:variable name="oscStrengths">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']//cml:scalar[@dictRef='t:velocity']">,<xsl:value-of select="."/></xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="energyEv">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']//cml:scalar[@dictRef='t:excitationEnergy']">,<xsl:value-of select="."/></xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="energyCm1">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']//cml:scalar[@dictRef='t:excitationEnergy']">,<xsl:value-of select=". * $EV_TO_CM_1"/></xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="energyNm">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']//cml:scalar[@dictRef='t:excitationEnergy']">,<xsl:value-of select="format-number($EV_TO_NM div number(.), '#0.000')"/></xsl:for-each>
                                </xsl:variable>
                                <!--,<xsl:value-of select=". * $EV_TO_NM"/>  format-number($EV_TO_CM_1 div number($energyLowest[$position]),'#0.000')                             -->
                                <script type="text/javascript">                                    
                                    //On expand accordion we'll generate our graph, to be correctly resized 
                                    $('#tddft.panel-collapse').on('shown.bs.collapse', function () {
                                        tddftParametersChanged();                                   
                                    })   
                                    
                                    var evBandwidth = <xsl:value-of select="$bandwidthev"/>;
                                    var cm1Bandwidth   = <xsl:value-of select="$bandwidthcm1"/>;
                                    var nmBandwidth = <xsl:value-of select="$bandwidthnm"/>;
                                    
                                    var evStep = evBandwidth / 30;
                                    var cm1Step = cm1Bandwidth / 30;
                                    var nmStep = nmBandwidth / 30;
                                    
                                    var oscStrengths = [<xsl:value-of select="$oscStrengths"/>]                          
                                    var energyEv     = [<xsl:value-of select="$energyEv"/>];                     
                                    var energyCm1    = [<xsl:value-of select="$energyCm1"/>];
                                    var energyNm     = [<xsl:value-of select="$energyNm"/>];
                                    
                                    <xsl:variable name="energyMax" select="number(format-number( round(10* 
                                                                                                max(//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']//cml:scalar[@dictRef='t:excitationEnergy'])
                                                                                         ) div 10 ,'##0.0' ))
                                                                                        "/>
                                    <xsl:variable name="energyMin" select="number(format-number( round(10*
                                                                                                min(//cml:module[@dictRef='cc:finalization']//cml:module[@ cmlx:templateRef='excitation']//cml:scalar[@dictRef='t:excitationEnergy'])
                                                                                         ) div 10 ,'##0.0' ))
                                                                                         "/>
                                    
                                    var energyMaxEv = round(<xsl:value-of select="$energyMax"/> + evBandwidth);                 
                                    var energyMinEv = round(<xsl:value-of select="$energyMin"/> - evBandwidth);                
                                    var energyMaxCm1 = round(<xsl:value-of select="$energyMax * $EV_TO_CM_1"/> + cm1Bandwidth);          
                                    var energyMinCm1 = round(<xsl:value-of select="$energyMin * $EV_TO_CM_1"/> - cm1Bandwidth);
                                    var energyMaxNm = round(<xsl:value-of select="$EV_TO_NM div $energyMin"/> + nmBandwidth);   
                                    var energyMinNm = round(<xsl:value-of select="$EV_TO_NM div $energyMax"/> - nmBandwidth);     
                                 
                                    function round(num){
                                        return Math.round(num * 100) / 100                                    
                                    }

                                    function tddftParametersChanged (){
                                        setTddftGraphLimitValues();                                                                   
                                        generateTddftChart();                       
                                    }
                                    
                                    function setTddftGraphLimitValues(){
                                        value = $("input[name='tddftGraphUnits']:checked").val();
                                        if(value == "ev"){                        		     
                                            $("#maxXSpinFree").val(energyMaxEv);
                                            $("#minXSpinFree").val(energyMinEv);      
                                            $("#bandWidthSpinFree").val(evBandwidth);            
                                        }
                                        if(value == "cm1"){     
                                            $("#maxXSpinFree").val(energyMaxCm1);
                                            $("#minXSpinFree").val(energyMinCm1);
                                            $("#bandWidthSpinFree").val(cm1Bandwidth);
                                        }
                                        if(value == "nm"){
                                            $("#maxXSpinFree").val(energyMaxNm);
                                            $("#minXSpinFree").val(energyMinNm);
                                            $("#bandWidthSpinFree").val(nmBandwidth);
                                        }
                                    }
                                                                      
                                   function generateTddftChart(){
                                        var data = fillTddftChart();
                                        
                                        var xLabel = "";                                        
                                        unitType = $("input[name='tddftGraphUnits']:checked").val();
                                        if(unitType == "ev")                        		              
                                            xLabel = '∆E (eV)';
                                        else if(unitType == "cm1")
                                            xLabel = '∆E (cm-1)';
                                        else  if(unitType == "nm")
                                            xLabel = "nm";

                                        var layout = {
                                            height: 450,
                                            xaxis: {
                                                title: xLabel,
                                                showgrid: true,
                                                zeroline: true,
                                                tickmode: 'auto'
                                            },
                                            hovermode: 'closest',
                                            yaxis: {
                                                title: 'Intensity [arb. units]'
                                            }
                                            /*
                                            legend: {
                                                orientation: 'h',
                                                y: -0.5
                                                }
                                            */
                                        };                                                
                                        Plotly.react('dipoleTransitionSpinFreePlotlyContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                    }
                                                                        
                                    function fillTddftChart(){
                                        //Clear possible previous values                                        
                                        var data = new Array();
                                        var dataSum = new Array();
                                        
                                        // Now we'll generate root and all-roots-sum series and append them in data  and dataSum arrays
                                        var bandWidth = $("#bandWidthSpinFree").val();
                                        var minX = parseFloat($("#minXSpinFree").val());
                                        var maxX = parseFloat($("#maxXSpinFree").val());
                                                                              
                                        unitType = $("input[name='tddftGraphUnits']:checked").val();                                       
                                        for(var inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> oscStrengths.length; inx++){
                                            generateTddftValuesForRoot(data,dataSum,  inx+1, unitType,bandWidth, minX, maxX);
                                        }                                        
                                        
                                        var xVal = new Array();
                                        var yVal = new Array();
                                        var sum = 0;
                                        for(var inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> dataSum.length; inx++){
                                            xVal.push(dataSum[inx][0]);
                                            yVal.push(dataSum[inx][1]);
                                            sum += dataSum[inx][1];
                                        }
                                        
                                        //Check if there is representable data, if not (no osc.strength values) we'll not show graph                                        
                                        if(sum == 0){
                                            $('#dipoleTransitionSpinFreePlotlyContainer').hide();
                                            $('#dipoleTransitionSpinFreeContainerControls').hide();
                                            return;
                                        }
                                        
                                        //Add sum serie and plot graph
                                        var serie = {
                                            x: xVal,
                                            y: yVal,
                                            mode: 'lines',
                                            name: 'Total sum',
                                            marker: {         
                                                color: 'rgb(00, 00, 00)'
                                            }
                                        };                                                           
                                        data.push(serie);   
                                        return data;
                                    }

                                    function generateTddftValuesForRoot(data, dataSum, root, units, bandWidth, minX, maxX){
                                        var step;
                                        var energyValue;
                                        switch(units){
                                            case 'ev':  step = evStep;
                                                        energyValue = energyEv[root-1];
                                                        break;
                                            case 'cm1': step = cm1Step;
                                                        energyValue = energyCm1[root-1];
                                                        break;
                                            case 'nm':  step = nmStep;
                                                        energyValue = energyNm[root-1];
                                                        break;         
                                        }      
                                                                                
                                        var inx = 0;
                                        var xVal = new Array();
                                        var yVal = new Array();
                                        for(var x = minX; x <xsl:text disable-output-escaping="yes">&lt;</xsl:text> maxX ; x = x + step ) {  
                                            if(dataSum.length <xsl:text disable-output-escaping="yes">&lt;</xsl:text>= inx)     
                                                dataSum.push([x,oscStrengths[root-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - energyValue),2))]);
                                            else
                                                dataSum[inx][1] += oscStrengths[root-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - energyValue),2))
                                                
                                            xVal.push(x);
                                            yVal.push(oscStrengths[root-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x -energyValue),2)));
                                            inx++;
                                        }
                                                                                
                                        var serie = {
                                            x: xVal,
                                            y: yVal,
                                            mode: 'lines',
                                            name: 'Root ' + root,
                                        };                                                           
                                        data.push(serie);
                                    }                                                                       
                                </script>
                            </div>
                            <!-- Dominant contributions -->
                            <div class="col-sm-12 col-md-6">
                                <h4>Dominant contributions</h4>
                                <xsl:for-each select="$tddftRoots">
                                    <xsl:variable name="info" select="./cml:list[@cmlx:templateRef='label']"/>
                                    <xsl:variable name="occOrbitalNumber" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:occOrbitalNumber'],'\s+')"/>
                                    <xsl:variable name="occOrbitalLabel" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:occOrbitalLabel'],'\s+')"/>
                                    <xsl:variable name="occOrbitalSpin" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:occOrbitalSpin'],'\s+')"/>
                                    <xsl:variable name="occEnergy" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:occEnergy'],'\s+')"/>
                                    <xsl:variable name="virtOrbitalNumber" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:virtOrbitalNumber'],'\s+')"/>
                                    <xsl:variable name="virtOrbitalLabel" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:virtOrbitalLabel'],'\s+')"/>
                                    <xsl:variable name="virtOrbitalSpin" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:virtOrbitalSpin'],'\s+')"/>
                                    <xsl:variable name="virtEnergy" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:virtEnergy'],'\s+')"/>
                                    <xsl:variable name="coefficient" select="tokenize(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:coeff'],'\s+')"/>
                                    <xsl:variable name="isOpenShell" select="exists(./cml:module[@cmlx:templateRef='contributions']/cml:array[@dictRef='t:occOrbitalSpin'])"/>
                                    <p>
                                        <span style="margin-right: 10px">
                                            <strong>
                                                <xsl:value-of select="$info/cml:scalar[@dictRef='t:serial']"/>
                                                <xsl:text> </xsl:text>
                                                <xsl:value-of select="$info/cml:scalar[@dictRef='t:type']"/>
                                                <xsl:text> </xsl:text>
                                                <xsl:value-of select="$info/cml:scalar[@dictRef='t:label']"/>
                                            </strong>
                                        </span>
                                        <span style="margin-right: 10px">
                                            <strong>∆E (eV): </strong>
                                            <xsl:value-of select="format-number(./cml:module[@cmlx:templateRef='energies']/cml:scalar[@dictRef='t:excitationEnergy'],'#0.00')"/>
                                        </span>
                                        <span style="margin-right: 10px">
                                            <strong>Osc. strength : </strong>
                                            <xsl:value-of select="helper:formatScientific(number(./cml:module[@cmlx:templateRef='oscillator']/cml:scalar[@dictRef='t:velocity']),2)"/>
                                        </span>
                                    </p>
                                    <xsl:variable name="maxContributions">
                                        <xsl:choose>
                                            <xsl:when test="count($occOrbitalNumber) > 10">10</xsl:when>
                                            <xsl:otherwise><xsl:value-of select="count($occOrbitalNumber)"></xsl:value-of></xsl:otherwise>
                                        </xsl:choose>                                                                                                                     
                                    </xsl:variable>      
                                    <table class="display" id="symmetry">
                                        <thead>
                                            <tr>
                                                <th>occ. orbital</th>                                              
                                                <th>energy / eV</th>
                                                <th>virt. orbital</th>                                                
                                                <th>energy / eV</th>
                                                <th>|coeff.|^2*100</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <xsl:for-each select="1 to $maxContributions">
                                                <xsl:variable name="outerIndex" select="."/>
                                                <tr>
                                                    <td>
                                                        <xsl:variable name="orbital">
                                                            <xsl:value-of select="$occOrbitalNumber[$outerIndex]"/>
                                                            <xsl:value-of select="$occOrbitalLabel[$outerIndex]"/>
                                                        </xsl:variable>
                                                        
                                                        <xsl:choose>
                                                            <xsl:when test="$hasOrbitals">
                                                                <a id="tddft-{generate-id($orbital)}" href="javascript:displayMoldenOrbital('{$orbital}')">
                                                                    <xsl:value-of select="$orbital"/><xsl:if test="$isOpenShell"><xsl:text> </xsl:text><xsl:value-of select="$occOrbitalSpin[$outerIndex]"/></xsl:if>
                                                                </a>        
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:value-of select="$orbital"/><xsl:if test="$isOpenShell"><xsl:text> </xsl:text><xsl:value-of select="$occOrbitalSpin[$outerIndex]"/></xsl:if>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="$occEnergy[$outerIndex]"/>
                                                    </td>
                                                    <td>
                                                        <xsl:variable name="virtorbital">
                                                            <xsl:value-of select="$virtOrbitalNumber[$outerIndex]"/>
                                                            <xsl:value-of select="$virtOrbitalLabel[$outerIndex]"/>
                                                        </xsl:variable>
                                                        
                                                        <xsl:choose>
                                                            <xsl:when test="$hasOrbitals">
                                                                <a id="tddft-{generate-id($virtorbital)}" href="javascript:displayMoldenOrbital('{$virtorbital}')">
                                                                    <xsl:value-of select="$virtorbital"/><xsl:if test="$isOpenShell"><xsl:text> </xsl:text><xsl:value-of select="$virtOrbitalSpin[$outerIndex]"/></xsl:if>
                                                                </a>        
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:value-of select="$virtorbital"/><xsl:if test="$isOpenShell"><xsl:text> </xsl:text><xsl:value-of select="$virtOrbitalSpin[$outerIndex]"/></xsl:if>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="$virtEnergy[$outerIndex]"/>
                                                    </td>
                                                    <td>
                                                        <xsl:value-of select="$coefficient[$outerIndex]"/>
                                                    </td>
                                                </tr>
                                            </xsl:for-each>
                                        </tbody>
                                    </table>
                                    <!-- Warning message -->
                                    <xsl:variable name="mostRelevantArray" as="item()*">
                                        <xsl:for-each select="1 to $maxContributions">
                                                <xsl:variable name="outerIndex" select="."/>                                            
                                                <xsl:value-of select="number($coefficient[$outerIndex])"/>
                                        </xsl:for-each>
                                    </xsl:variable>                                  
                                    <xsl:if test="sum($mostRelevantArray) &lt; 50">
                                        <div class="alert alert-warning" role="alert">
                                            Warning: &#931;c<sup>2</sup><sub>i</sub> &lt; 50 %                                            
                                        </div>
                                    </xsl:if>
                                    <br/>
                                </xsl:for-each>                               
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="frequencies">
        <xsl:variable name="vibrations" select=".//cml:module[@cmlx:templateRef='vibrations']"/>
        <xsl:if test="exists($vibrations)">
            <xsl:variable name="frequency" select="tokenize(.//cml:module[@id='finalization']/propertyList/property[@dictRef='cc:frequencies']//cml:module[@cmlx:templateRef='spectrum']/array[@dictRef='cc:frequency'],'\s+')"/>
            <div id="vibrationPanel" class="panel panel-default" >
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        IR spectrum
                    </h4>
                </div>
                <div id="frequencies" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-12">                                                                                             
                                <script type="text/javascript">
                                  	delete Jmol._tracker;
                                    Jmol._local = true;                                
                                
                                    jsvApplet0 = "jsvApplet0";
                                    jmolApplet0 = "jmolApplet0";                                
                                    Jmol.setAppletSync([jsvApplet0, jmolApplet0], ["load <xsl:value-of select="$jcampdxurl"/>", ""], true);                                       
                                    var jmolInfo = {
                                        width: 560,
                                        height: 400,
                                        debug: false,
                                        color: "0xF0F0F0",
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        readyFunction: vibrationsLoaded,
                                        animframecallback: "modelchanged",
                                        allowjavascript: true,
                                        script: "background white; vibration off; vectors off; sync on;"                                       
                                    }                                       
                                    var jsvInfo = {
                                        width: 560,
                                        height: 400,
                                        debug: false,
                                        color: "0xC0C0C0",
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                        script: null,
                                        initParams: null,
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        readyFunction: null,
                                        allowjavascript: true
                                    }
                                    
                                    $('div#frequencies').on('shown.bs.collapse', function () {
                                     if($("div#jmolApplet").children().length == 0){       
                                        use="HTML5";                                       
                                        jsvApplet0 = Jmol.getJSVApplet("jsvApplet0", jsvInfo);
                                        $("div#jsvApplet").html(Jmol.getAppletHtml(jsvApplet0));                                            
                                        jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                                        $("div#jmolApplet").html(Jmol.getAppletHtml(jmolApplet0));
                                        vibrationsLoading();
                                     }                                      
                                    });
                                    
                                    
                                    function vibrationsLoading(){                                       
                                        $('div#frequencies').block({ 
                                            message: '<h3>Loading spectrum</h3>', 
                                            css: { border: '3px solid #a00' } 
                                        });                                                                                    
                                    }
                                    
                                    function vibrationsLoaded(){
                                        $('div#frequencies').unblock();                                                           
                                    }             
                                    
                                </script>                                
                                <div id="irSpectrumDiv">
                                    <div id="jsvApplet" style="display:inline; float:left">                                                                         
                                    </div>
                                    <div id="jmolApplet" style="display:inline; float:left">
                                    </div>                                                                                                                               
                                </div>                                
                                <div id="frequencyComboboxDiv" style="display:block">
                                    Selected frequency : 
                                    <script type="text/javascript">
                                        function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                                        vibrationcboIndex = $("select[name='jmolMenu0'] option:selected").index();
                                        if(vibrationcboIndex != objwhat + 1)
                                        $("select[name='jmolMenu0']").val(objwhat + 2);                                                                                      
                                        }                    
                                        
                                        Jmol.jmolMenu(jmolApplet0,[                    
                                        ["vibration off", ".... select ....", true],
                                        <xsl:variable name="frequencyNumber" as="xs:double*">      <!-- Discard zero vibrations from frequencies, code copied from cml2jcampdx -->
                                            <xsl:for-each select="$frequency">
                                                <xsl:if test="number(.) != 0">
                                                    <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="."/></xsl:element>
                                                </xsl:if>                
                                            </xsl:for-each>        
                                        </xsl:variable>                                        
                                        <xsl:variable name="discardedNumber" select="count($frequency) - count($frequencyNumber)"/>
                                        <xsl:for-each select="1 to count($frequency)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <xsl:if test="number($frequency[$outerIndex]) != 0">
                                                ["model <xsl:value-of select="turbo:normalizeVibrationIndex($outerIndex,$frequency[$outerIndex],$discardedNumber) "/>;vibration on" ,"<xsl:value-of select="$frequency[$outerIndex]"/>"]<xsl:if test="$outerIndex != count($frequency)"><xsl:text>,</xsl:text></xsl:if>                                                                                                
                                            </xsl:if>    
                                        </xsl:for-each>                                        
                                        ]); 
                                        
                                    </script>                    
                                </div>
                                <script type="text/javascript">
                                    $("#irSpectrumDiv" ).prepend( $("#frequencyComboboxDiv") );
                                </script>                                                              
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:function name="turbo:normalizeVibrationIndex">
        <xsl:param name="currentIndex"/>
        <xsl:param name="currentFrequency"/>
        <xsl:param name="discardedIndexes"/>
        <xsl:if test="number($currentFrequency) &lt; 0"><xsl:value-of select="$currentIndex"/></xsl:if>
        <xsl:if test="number($currentFrequency) &gt; 0"><xsl:value-of select="$currentIndex - $discardedIndexes"/></xsl:if>
    </xsl:function>

    <xsl:function name="turbo:getEcpType">        
        <xsl:param name="atomIndex"/>
        <xsl:param name="atomType"/>
                
        <xsl:choose>
            <xsl:when test="exists($atominfo)">
                <xsl:variable name="singleatom" select="$atominfo//cml:module[@cmlx:templateRef='atomdef' and descendant::cml:list[@dictRef='t:ecp']]"/> <!-- Get atom types with ecp section defined-->
                <xsl:for-each select="$singleatom">
                    <xsl:variable name="atomT" select="helper:correctAtomTypeCase(./descendant::cml:list[@dictRef='t:atom']/cml:scalar[@dictRef='cc:elementType']/text())"/>                    
                    <xsl:if test=" $atomType = $atomT ">
                        <xsl:variable name="ecp" select="./cml:list[@dictRef='t:ecp']/cml:scalar[@dictRef='t:ecp']"/>                        
                        <xsl:variable name="singleRangeValue" select="tokenize(.//cml:scalar[@dictRef='t:atomrange'],',')"/>
                        <xsl:for-each select="$singleRangeValue">                            
                            <xsl:variable name="atomRange" select="tokenize(.,'-')"/>
                            <xsl:if test="count($atomRange) = 2 and $atomIndex &gt;= number($atomRange[1]) and $atomIndex &lt;= number($atomRange[2])">
                                <xsl:value-of select="$ecp"/>                       
                            </xsl:if>
                            <xsl:if test="count($atomRange) = 1 and number($atomRange) = $atomIndex">
                                <xsl:value-of select="$ecp"/>
                            </xsl:if>                                                        
                        </xsl:for-each>                                            
                    </xsl:if>
                </xsl:for-each>
            </xsl:when>
        </xsl:choose>        
    </xsl:function>

    <xsl:function name="turbo:escapeQuotesJavascript">
        <xsl:param name="value"/>
        <xsl:value-of select="replace($value, '&quot;', '\\&quot;')" />
        
    </xsl:function>

    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>
        </div>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>

</xsl:stylesheet>
