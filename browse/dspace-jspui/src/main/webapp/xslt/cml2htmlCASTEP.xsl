<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:ca="http://www.iochem-bd.org/dictionary/castep/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk ca helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Jul 15, 2022</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>       
    </xd:doc> 
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:include href="helper/chemistry/castep.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>

    <!-- Environment module -->
    <xsl:variable name="environment" select="//cml:module[@id='job'][1]/cml:module[@id='environment']/cml:parameterList"/>
    <xsl:variable name="programParameter" select="$environment/cml:parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="$environment/cml:parameter[@dictRef='cc:programVersion']"/>

    <!-- Geometry -->
    <xsl:variable name="initialMolecule" select="(//cml:molecule[@id='initial'])[last()]"/>
    <xsl:variable name="finalMolecule" select="(//cml:molecule[@id='final'])[last()]"/>    

    <!-- Initialization -->
    <xsl:variable name="initialization" select="//cml:module[@id='job'][1]/cml:module[@dictRef='cc:initialization']"/>
    <xsl:variable name="setup" select="$initialization/cml:parameterList" />
    <xsl:variable name="calcType" select="ca:getCalcType($setup)" />
    <xsl:variable name="isOptimization" select="contains($calcType,$ca:minimize)"/>    
    <xsl:variable name="pointGroup" select="$initialization/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='symmetry']" />
    <xsl:variable name="kpoints" select="$initialization/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='kpoints']/cml:matrix" />
 
    <!-- Calculation -->
    <xsl:variable name="calculation" select="//cml:module[@id='job'][1]/cml:module[@dictRef='cc:calculation']"/>
    
    <!-- Finalization -->
    <xsl:variable name="finalization" select="//cml:module[@id='job'][1]/cml:module[@dictRef='cc:finalization']"/>  
    <xsl:variable name="graphs" select="$finalization/cml:module[@dictRef='cc:userDefinedModule']/cml:module[@id='chart']" />
    
    <xsl:template match="/">
        <html lang="en">
            <head>
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>
                
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />

                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Settings -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>SETTINGS</h3>                                                                                                                                               
                            <div>                                         
                                <xsl:call-template name="settings"/>                                
                            </div>
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                                                                                                               
                            <div>                                
                                <xsl:call-template name="atomicCoordinates" />
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->                                       
                    <xsl:if test="exists($kpoints)">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <h3>MOLECULAR INFO</h3>
                                <xsl:call-template name="pointGroup"/>
                                <xsl:call-template name="kpoints"/>
                            </div>
                        </div>
                    </xsl:if>
                    
                    
                    <!-- Results -->
                    <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                        <div id="module-{generate-id(.)}">                                
                            <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                            <div class="row bottom-buffer">
                                <div class="col-md-12">
                                    <xsl:call-template name="energies" />
                                    <xsl:call-template name="mulliken" />
                                    <xsl:call-template name="graphs" />
                                    <xsl:call-template name="timing" />
                                </div>
                            </div>
                        </div>
                    </xsl:for-each>
                    <xsl:call-template name="printLicense"/>
                    
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {    
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                        
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                            $.unblockUI();                             
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                               var oTable = $(this).dataTable();
                               $(oTable).css({ width: $(oTable).parent().width() });
                               oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
        </body>
    </html>
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">
        <xsl:variable name="isLargeMolecule" select="count($finalMolecule/atomArray/atom) > 1000"/>
        <xsl:variable name="collapseAccordion" select="if($isLargeMolecule) then '' else 'in'"/>
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]      
                    <xsl:if test="$isOptimization">
                        <xsl:choose>
                            <xsl:when test="exists($calculation//cml:module[@cmlx:templateRef='scf']//cml:scalar[@dictRef='cc:scfConverged']) and not(exists(//cml:molecule[@id='not.converged']))">
                                <small>(optimized)</small>
                            </xsl:when>                          
                            <xsl:otherwise>                                                           
                                <small><strong>(not converged geometry)</strong></small>        
                            </xsl:otherwise>
                        </xsl:choose>                                                           
                    </xsl:if>                                    
                </h4>
            </div>
            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">
                    <div class="row bottom-buffer">
                        <script>
                            var coordinates = new Array();
                                                     
                            function buildXYZTable(id) {
                                var coordinatesXYZ = coordinates[id].map((val) => val.slice(1,5));
                                coordinatesXYZ.unshift(['<xsl:value-of select="$title"/>','','','']);
                                $('table#atomicCoordinatesXYZT-' + id).DataTable({
                                    "aaData" : coordinatesXYZ,
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false,
                                    "bDeferRender": true
                                });
                                $('table#atomicCoordinatesXYZT-' + id + '-wrapper').hide();
                            }
                            
                            function buildMolecularInfoTable(id) {
                                $('table#atomicCoordinates' + id).dataTable( {
                                    "aaData": coordinates[id],
                                    "aoColumns": [
                                        { "sTitle": "ATOM" },
                                        { "sTitle": "" },
                                        { "sTitle": "x", "sClass": "right" },
                                        { "sTitle": "y", "sClass": "right" },
                                        { "sTitle": "z", "sClass": "right" },
                                        { "sTitle": "u", "sClass": "right" },
                                        { "sTitle": "v", "sClass": "right" },
                                        { "sTitle": "w", "sClass": "right" }],
                                    "bFilter": false,
                                    "bPaginate": <xsl:value-of select="if($isLargeMolecule) then 'true' else 'false'" />,
                                    "bSort": false,
                                    "bInfo": false
                                } );
                            }

                            function viewCartesianCoordinatesOnly(id){
                                $('input[name=coordinateType ][value=cartesian]').prop('checked', true);
                                changeMoleculeColumnVisibility(id);
                                $('#coordinateTypeDiv{$id}').hide();
                            }

                            function changeMoleculeColumnVisibility(id){
                                var mode = $('input[name=coordinateType'+ id + ']:checked').val();
                                var dt = $('#atomicCoordinates' + id).DataTable();
                                if( mode == 'both'){
                                    dt.columns([2,3,4,5,6,7]).visible(true);
                                }else if(mode == 'cartesian') {                                    
                                    dt.columns([5,6,7]).visible(false);
                                    dt.columns([2,3,4]).visible(true);
                                }else {
                                    dt.columns([2,3,4]).visible(false); 
                                    dt.columns([5,6,7]).visible(true);
                                }

                                dt.columns.adjust().draw();
                                // Adjust table width to its container
                                dt = $('#atomicCoordinates' + id).dataTable();
                                $(dt).css({ width: $(dt).parent().width() });
                                dt.fnAdjustColumnSizing();
                            }
                            
                            function getXYZ(id) {
                                var table = $('table#atomicCoordinatesXYZT-' + id).DataTable();
                                new $.fn.dataTable.Buttons(table, {
                                <xsl:choose>
                                    <xsl:when test="$isLargeMolecule">
                                        buttons: [ {
                                        extend: 'csv',
                                        text: 'CSV'
                                        }]
                                    </xsl:when>
                                    <xsl:otherwise>
                                        buttons: [ {
                                        extend: 'copyHtml5',
                                        text: 'XYZ'
                                        }]
                                    </xsl:otherwise>
                                </xsl:choose>
                                });
                                
                                table.buttons().container().appendTo($('div#coordinatesXYZ-' + id));
                                $('div#coordinatesXYZ-' + id + ' span.fa').hide();                                
                                
                                <xsl:if test="$isLargeMolecule">
                                    $("div#coordinatesXYZ-' + id + ' div.dt-buttons button.buttons-csv").on('mousedown', function(){
                                        $("div#coordinatesXYZ-' + id + ' div.dt-buttons button.buttons-csv").addClass('processing');
                                    });
                                </xsl:if>
                            }
                            
                            
                            //JSmol-related variables and functions
                            
                            delete Jmol._tracker;
                            Jmol._local = true;
                            
                            jmol_isReady = function(applet) {
                                var id = applet._id.replace('jmolApplet', '') 
                                resizeJsmolApplet(id);
                            }
                            
                            function resizeJsmolApplet(id){
                                var width = $("div#geometryDiv" + id).parent().width() - 50;
                                var height = width; 
                                Jmol.resizeApplet(window['jmolApplet'+id], [width,height]);
                            }         
                            var JmolConfigInfo = {
                                debug: false,
                                color: "0xFFFFFF",
                                addSelectionOptions: false,
                                use: "HTML5",
                                j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                readyFunction: jmol_isReady,
                                disableJ2SLoadMonitor: true,
                                disableInitialConsole: true,
                                allowJavaScript: true,
                                console: "none"
                            };
                            
                            <![CDATA[
                            function loadJSmol(id){                             
                                window['jmolApplet' + id] = Jmol.getApplet("jmolApplet" + id, JmolConfigInfo);
                                $("#geometryDiv" + id).html(Jmol.getAppletHtml(window['jmolApplet' + id]));
                                Jmol.script(window['jmolApplet' + id],'set platformSpeed 2;');
                                Jmol.script(window['jmolApplet' + id], 'data "model"| ' + window['coordinatesXml' + id] + '|end "model";');                                
                            }
                            ]]>
                        </script>
                        <xsl:choose>
                            <xsl:when test="$isOptimization">                                
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <h3 style="text-decoration:underline">Initial geometry</h3>
                                    <xsl:apply-templates  select="$initialMolecule"/>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <h3 style="text-decoration:underline">Final geometry</h3>
                                    <xsl:apply-templates  select="$finalMolecule"/>
                                </div>
                            </xsl:when>
                            <xsl:otherwise>
                                <div class="col-lg-6 col-md-12 col-sm-12">
                                    <xsl:apply-templates  select="$finalMolecule"/>
                                </div>                                
                            </xsl:otherwise>
                        </xsl:choose>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <xsl:template name="atomicCoordinatesFractional" match="cml:molecule[cml:crystal]">
        <xsl:variable name="id" select="generate-id(.)"/>
        <xsl:variable name="isLargeMolecule" select="(./atomArray/atom) > 1000"/>

        <xsl:variable name="boxCoords" select="./cml:crystal"/>

        <div class="row">
            <div class="col-lg-12 col-md-8 col-sm-12">
                <script type="text/javascript">
                    var jmolApplet<xsl:value-of select="$id"/> = null;
                    var coordinatesXml<xsl:value-of select="$id"/> = '<xsl:copy-of select="."/>';
                    
                    window.addEventListener("resize", function(){
                        resizeJsmolApplet('<xsl:value-of select="$id"/>');
                    });
                </script> 
                <div id="geometryDiv{$id}"></div>
            </div>                        
            <xsl:if test="exists($boxCoords)">
                <div class="col-md-12 mb-4">
                    <h5>Cell parameters:</h5>
                    <table id="cellParameters{$id}" class="display">
                        <thead>
                            <tr>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>a = <xsl:value-of select="./crystal/scalar[@title='a']"/></td>
                            </tr>
                            <tr>
                                <td>b = <xsl:value-of select="./crystal/scalar[@title='b']"/></td>
                            </tr>
                            <tr>
                                <td>c = <xsl:value-of select="./crystal/scalar[@title='c']"/></td>
                            </tr>
                            <tr>
                                <td>α = <xsl:value-of select="./crystal/scalar[@title='alpha']"/></td>
                            </tr>
                            <tr>
                                <td>β = <xsl:value-of select="./crystal/scalar[@title='beta']"/></td>
                            </tr>
                            <tr>
                                <td>γ = <xsl:value-of select="./crystal/scalar[@title='gamma']"/></td>
                            </tr>
                        </tbody>
                    </table>    
                </div>
            </xsl:if>
            
            <div class="col-md-12 mb-4">
                <div id="coordinateTypeDiv{$id}">
                    <span>Coordinate type :</span>
                    <input type="radio" name="coordinateType{$id}" value="both" checked="checked" onclick="changeMoleculeColumnVisibility('{$id}')" />Both
                    <input type="radio" name="coordinateType{$id}" value="cartesian" onclick="changeMoleculeColumnVisibility('{$id}')"/>Cartesian
                    <input type="radio" name="coordinateType{$id}" value="fractional" onclick="changeMoleculeColumnVisibility('{$id}')"/>Fractional
                </div>
                <div id="coordinatesXYZ-{$id}" class="right">
                    <xsl:if test="$isLargeMolecule">
                    </xsl:if>
                    <a class="text-right" href="javascript:getXYZ('{$id}')"><span class="text-right fa fa-download"/></a>
                </div>
                
                <!-- Build an XYZ-format-compatible table  -->
                <table class="display" style="display:none" id="atomicCoordinatesXYZT-{$id}">
                    <thead>
                        <tr>
                            <th><xsl:value-of select="count(./cml:atomArray/cml:atom)"/></th>
                            <th> </th>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>                               
                </table>
                
                
                <table id="atomicCoordinates{$id}">
                    <thead>
                        <tr>
                            <th rowspan="2"/>
                            <th rowspan="2"/>
                            <th colspan="3" style="text-align:center">Cartesian coordinates</th>
                            <th colspan="3" style="text-align:center">Fractional coordinates</th>
                        </tr>
                        <tr>
                            <th style="text-align:right">x</th>
                            <th style="text-align:right">y</th>
                            <th style="text-align:right">z</th>
                            <th style="text-align:right">u</th>
                            <th style="text-align:right">v</th>
                            <th style="text-align:right">w</th>
                        </tr>
                    </thead>
                </table>
                <script type="text/javascript">                    
                    coordinates['<xsl:value-of select="$id"/>'] = 
                    [<xsl:for-each select="./cml:atomArray/cml:atom"><xsl:variable name="outerIndex" select="position()"/>
                        [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="@elementType"/>","<xsl:value-of select="format-number(@x3, '#0.0000')"/>","<xsl:value-of select="format-number(@y3, '#0.0000')"/>","<xsl:value-of select="format-number(@z3, '#0.0000')"/>","<xsl:value-of select="format-number(@xFract, '#0.0000')"/>","<xsl:value-of select="format-number(@yFract, '#0.0000')"/>","<xsl:value-of select="format-number(@zFract, '#0.0000')"/>"]<xsl:if test="position() != last()"><xsl:text>,</xsl:text></xsl:if>
                    </xsl:for-each>];
                    
                    $(document).ready(function() {
                        buildXYZTable('<xsl:value-of select="$id"/>');
                        buildMolecularInfoTable('<xsl:value-of select="$id"/>');
                        <xsl:if test="not(exists(./cml:atomArray/cml:atom/@xFract))">   
                            viewCartesianCoordinatesOnly('<xsl:value-of select="$id"/>');
                        </xsl:if> 
                        
                        <xsl:if test="$isOptimization">
                            //On expand accordion we'll load geometry on JSmol
                            $('#atomicCoordinatesCollapse.panel-collapse').on('shown.bs.collapse', function () {                            
                                loadJSmol('<xsl:value-of select="$id"/>');
                            });
                        </xsl:if> 
                        
                    } );
                </script>
            </div>                         
        </div>
    
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>  
            <tr>
                <td style="vertical-align: top;">Calculation type:</td>
                <td><xsl:value-of select="$calcType"/></td>
            </tr>
        </table>        
    </xsl:template>
    
    <!-- Settings -->
    <xsl:template name="settings">
        <div class="row bottom-buffer">
            <xsl:for-each select="('Geometry Optimization', 'Electronic Spectroscopy', 'Exchange Correlation')">
                <xsl:variable name="header" select="." />
                <xsl:variable name="section" select="replace(lower-case(.),' ','.')"/>
                <xsl:variable name="tablename" select="replace(lower-case(.), ' ', '')" />
                
                <xsl:if test="exists($setup/cml:parameter[@title=$section])">
                    <div class="col-lg-6 col-md-8 col-sm-12">
                        <h4><xsl:value-of select="$header"/></h4>
                        <table class="display" id="settings-{$tablename}">
                            <thead>
                                <tr>
                                    <th>Parameter</th>
                                    <th>Value</th>
                                    <th>Units</th>
                                </tr>
                            </thead>
                            <tbody>
                                <xsl:copy-of select="helper:printSectionRows($setup, $section)"/>
                            </tbody>
                        </table>
                        <script type="text/javascript">
                            
                            $(document).ready(function() {
                                $('table#settings-<xsl:value-of select="$tablename"/>').dataTable( {
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false,
                                    "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 1 ]}]                                    
                                });   
                            } );                                       
                        </script>
                    </div>
                </xsl:if>
            </xsl:for-each>
        </div>   
    </xsl:template>

    <!-- Kpints -->
    <xsl:template name="kpoints">        
        <xsl:if test="exists($kpoints)">
            <xsl:variable name="kpmatrix" select="tokenize($kpoints/text(), '\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#kpoints-{generate-id($kpoints)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Kpoints</h4>
                </div>
                <div id="kpoints-{generate-id($kpoints)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">                           
                            <div class="col-md-6">                             
                                <table class="display" id="kpointList">
                                    <thead>
                                        <tr>
                                            <th colspan="3" style="text-align:center">Fractional coordinates</th>
                                            <th rowspan="2"  style="text-align:right"></th>
                                        </tr>
                                        <tr>
                                            <th style="text-align:right">u</th>
                                            <th style="text-align:right">v</th>
                                            <th style="text-align:right">w</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody> 
                                        <xsl:for-each select="1 to $kpoints/@rows">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <tr>
                                                <td><xsl:value-of select="number($kpmatrix[(($outerIndex - 1) * $kpoints/@rows) + 1])"/></td>
                                                <td><xsl:value-of select="number($kpmatrix[(($outerIndex - 1) * $kpoints/@rows) + 2])"/></td>
                                                <td><xsl:value-of select="number($kpmatrix[(($outerIndex - 1) * $kpoints/@rows) + 3])"/></td>
                                                <td><xsl:value-of select="number($kpmatrix[(($outerIndex - 1) * $kpoints/@rows) + 4])"/></td>
                                            </tr>    
                                        </xsl:for-each>
                                    </tbody>
                                </table>                                
                                <script type="text/javascript">                                    
                                    $(document).ready(function() {
                                        $('table#kpointList').dataTable( {
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 0,1,2,3 ]}],
                                            "aoColumns": [                                            
                                            { "sTitle": "u", "sClass": "right" },
                                            { "sTitle": "v", "sClass": "right" },
                                            { "sTitle": "w", "sClass": "right" },
                                            { "sTitle": "Weight", "sClass": "right" }]
                                        });   
                                    });                                       
                                </script>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Point group line -->
    <xsl:template name="pointGroup">
        <xsl:if test="exists($pointGroup)">
            <div class="row bottom-buffer">
                <div class="col-md-6">
                    <table class="display compact" id="pointgroup">
                        <thead>
                            <tr>
                                <th></th>
                                <th></th>
                            </tr>                         
                        </thead>
                        <tbody>
                            <tr>
                                <td>Point group</td><td><xsl:value-of select="$pointGroup//cml:scalar[@dictRef='cc:pointgroup']"/></td>                                
                            </tr>
                            <tr>
                                <td>Space group</td><td><xsl:value-of select="$pointGroup//cml:scalar[@dictRef='ca:spacegroup']"/></td>
                            </tr>
                        </tbody>
                    </table>
                    <script type="text/javascript">                                    
                        $(document).ready(function() {
                        $('table#pointgroup').dataTable( {
                            "bFilter": false,
                            "bPaginate": false,
                            "bSort": false,
                            "bInfo": false                           
                            });   
                        });                                       
                    </script>               
                </div>
            </div>
        </xsl:if>
    </xsl:template>    

    <!-- Modules -->
    <xsl:template name="energies">                
        <xsl:variable name="scf" select="($calculation/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='step']/cml:module[@cmlx:templateRef='scf'])[last()]" />
        <xsl:if test="exists($scf)">
            <xsl:variable name="finalEnergy" select="$scf/cml:scalar[@dictRef='cc:finalEnergy']" />
            <xsl:variable name="freeEnergy" select="$scf/cml:scalar[@dictRef='cc:freeEnergy']" />
            <xsl:variable name="enthalpy" select="$finalization/cml:propertyList/cml:property[@dictRef='ca:enthalpy']/cml:scalar" />
            <xsl:variable name="converged" select="$scf/cml:scalar[@dictRef='cc:scfConverged']" />
                        
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#scf-{generate-id($scf)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Final energies</h4>
                </div>            
                <div id="scf-{generate-id($scf)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-lg-6">                                
                                <table class="display nowrap" id="scfT-{generate-id($scf)}">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Final energy</td>
                                            <td><xsl:value-of select="$finalEnergy"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($finalEnergy/@units)" /></td>
                                        </tr>
                                        <tr>
                                            <td>Final free energy</td>
                                            <td><xsl:value-of select="$freeEnergy"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($freeEnergy/@units)" /></td>
                                        </tr>
                                        <xsl:if test="exists($enthalpy)">
                                            <tr>
                                                <td>Final enthalpy</td>
                                                <td><xsl:value-of select="number($enthalpy)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($enthalpy/@units)" /></td>
                                            </tr>
                                        </xsl:if>
                                    </tbody>
                                </table>
                                <br/>                                
                                <p class="text-right"><xsl:value-of select="$converged"/></p>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#scfT-<xsl:value-of select="generate-id($scf)"/>").dataTable({                                    
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    }); 
                                </script>
                            </div>
                        </div>
                    </div>
                </div>                
            </div>
        </xsl:if>
    </xsl:template>
   
    <xsl:template name="mulliken">
        <xsl:variable name="mulliken" select="$finalization/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='mulliken']" />
        <xsl:if test="exists($mulliken)">
            
            <xsl:variable name="serial" select="tokenize($mulliken/list/*[@dictRef='cc:serial'],'\s+')"/>
            <xsl:variable name="elementType" select="tokenize($mulliken/list/*[@dictRef='cc:elementType'],'\s+')"/>            
            <xsl:variable name="spinType" select="tokenize($mulliken/list/*[@dictRef='ca:spin'],'\s+')"/>
            <xsl:variable name="orbitalS" select="tokenize($mulliken/list/*[@dictRef='ca:orbitalS'],'\s+')"/>
            <xsl:variable name="orbitalP" select="tokenize($mulliken/list/*[@dictRef='ca:orbitalP'],'\s+')"/>
            <xsl:variable name="orbitalD" select="tokenize($mulliken/list/*[@dictRef='ca:orbitalD'],'\s+')"/>
            <xsl:variable name="orbitalF" select="tokenize($mulliken/list/*[@dictRef='ca:orbitalF'],'\s+')"/>    
            <xsl:variable name="total" select="tokenize($mulliken/list/*[@dictRef='x:total'],'\s+')"/>
            <xsl:variable name="charge" select="tokenize($mulliken/list/*[@dictRef='x:charge'],'\s+')"/>
            <xsl:variable name="spin" select="tokenize($mulliken/list/*[@dictRef='x:spin'],'\s+')"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#charges-{generate-id($mulliken)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Mulliken Atomic Charges   
                    </h4>
                </div>
                <div id="charges-{generate-id($mulliken)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-8 col-md-8 col-sm-12">
                                <table id="chargesT-{generate-id($mulliken)}"></table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#chargesT-<xsl:value-of select="generate-id($mulliken)"/>').dataTable({
                                            "aaData": [                                                                    
                                            <xsl:for-each select="1 to count($elementType)">
                                                <xsl:variable name="innerIndex" select="."/>                                        
                                                [ <xsl:value-of select="$serial[$innerIndex * 2 - 1]"/>,
                                                  "<xsl:value-of select="$elementType[$innerIndex]"/>",
                                                  "<xsl:value-of select="$spinType[$innerIndex * 2 - 1]"/>",
                                                  "<xsl:value-of select="$orbitalS[$innerIndex * 2 - 1]"/>",
                                                  "<xsl:value-of select="$orbitalP[$innerIndex * 2 - 1]"/>",
                                                  "<xsl:value-of select="$orbitalD[$innerIndex * 2 - 1]"/>",
                                                  "<xsl:value-of select="$orbitalF[$innerIndex * 2 - 1]"/>",
                                                  "<xsl:value-of select="$total[$innerIndex * 2 - 1]"/>",
                                                  "<xsl:value-of select="$charge[$innerIndex]"/>",
                                                  "<xsl:value-of select="$spin[$innerIndex]"/>"
                                                    ],                                                
                                                [ "<xsl:value-of select="$serial[$innerIndex * 2]"/>",
                                                  '',
                                                  "<xsl:value-of select="$spinType[$innerIndex * 2]"/>",                                                  
                                                  "<xsl:value-of select="$orbitalS[$innerIndex * 2]"/>",
                                                  "<xsl:value-of select="$orbitalP[$innerIndex * 2]"/>",
                                                  "<xsl:value-of select="$orbitalD[$innerIndex * 2]"/>",
                                                  "<xsl:value-of select="$orbitalF[$innerIndex * 2]"/>",
                                                  "<xsl:value-of select="$total[$innerIndex * 2 - 1]"/>",
                                                  '',
                                                  '']                                                  
                                                <xsl:if test="($innerIndex &lt; count($elementType))"><xsl:text>,</xsl:text></xsl:if>
                                               
                                            </xsl:for-each>                                                   
                                            ],
                                            "aoColumns": [
                                                { "sTitle": "Atom" },
                                                { "sTitle": ""},                                                
                                                { "sTitle": ""},
                                                { "sTitle": "S" },
                                                { "sTitle": "P" },
                                                { "sTitle": "D" },
                                                { "sTitle": "F"  },
                                                { "sTitle": "Total"  },
                                                { "sTitle": "Charge"},
                                                { "sTitle": "Spin"  },
                                            ],
                                            "aoColumnDefs" : [
                                                { "sClass": "text-right", "aTargets": [3,4,5,6,7,8,9] }                                    
                                            ],
                                            "bFilter": false,                                 
                                            "bPaginate": true,                                    
                                            "bSort": false,
                                            "bInfo": true
                                            } );   
                                    } );                                       
                                </script>                
                            </div>                
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
        
    </xsl:template>
   
    <!-- Graphs -->
    <xsl:template name="graphs">
        <xsl:if test="exists($graphs)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#graphs-{generate-id($graphs[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Graphs
                    </h4>
                </div>
                <div id="graphs-{generate-id($graphs[1])}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <xsl:for-each select="$graphs">
                                <xsl:variable name="graph" select="."/>
                                <xsl:variable name="outerIndex" select="position()" />
                                <div class="col-sm-12 col-md-10">
                                    <div id="graphPlotlyContainer-{$outerIndex}" style="min-height:450px" class="mt-4"/>
                                    
                                    <script type="text/javascript">                                      
                                        function getLineColor(colorUp,colorDown,data){
                                            if(data.reduce(((curr,prev)=> curr+prev))/data.length &gt; 0)
                                                return colorUp;
                                             else
                                                return colorDown;
                                        }
                                        
                                        function getMaxY(data){
                                            var maxs = data.map((element) => element.y.reduce((prev, curr) => (prev &gt; curr)? prev:curr));                                            
                                            return maxs.reduce((prev,curr)=>(prev &gt; curr)? prev:curr);
                                        }
                                        
                                        function getMinY(data){
                                            var mins = data.map((element) => element.y.reduce((prev, curr) => (prev &lt; curr)? prev:curr));                                            
                                            return mins.reduce((prev,curr)=>(prev &lt; curr)? prev:curr);
                                        }                                        
                                        
                                        $(document).ready(function(){
                                            //Build graphs                                              
                                            var data = new Array();                                            
                                            <xsl:for-each select="$graph/cml:list[@id='series']/cml:list">
                                                <xsl:variable name="name" select="./@name"/>
                                                <xsl:variable name="style" select="if(exists(./cml:scalar[@dictRef='x:linestyle'])) then ./cml:scalar[@dictRef='x:linestyle']/text() else 'solid'" />
                                                <xsl:variable name="color" select="./cml:scalar[@dictRef='x:linecolor']" />
                                                <xsl:variable name="innerIndex" select="position()"/>
                                                <xsl:variable name="x" select="tokenize(./cml:array[@dictRef='cc:x2'],'\s+')" />
                                                <xsl:variable name="y" select="tokenize(./cml:array[@dictRef='cc:y2'],'\s+')" />

                                                <xsl:variable name="lines">
                                                    <xsl:element name="cml:item" namespace="http://www.xml-cml.org/schema">
                                                        <xsl:element name="line" namespace="http://www.xml-cml.org/schema">
                                                            <xsl:attribute name="index" select="0"/>
                                                        </xsl:element>
                                                        <xsl:for-each select="1 to count($x)">
                                                            <xsl:variable name="index" select="."/>
                                                            <xsl:if test="number($x[$index]) &gt; number($x[$index +1])">
                                                                <xsl:element name="line" namespace="http://www.xml-cml.org/schema">
                                                                    <xsl:attribute name="index" select="$index"/>
                                                                </xsl:element>
                                                            </xsl:if>
                                                        </xsl:for-each>
                                                        <xsl:element name="cml:line" namespace="http://www.xml-cml.org/schema">
                                                            <xsl:attribute name="index" select="count($x)"/>
                                                        </xsl:element>
                                                    </xsl:element>
                                                </xsl:variable>                                             
                                              
                                              
                                              <xsl:choose>
                                                  <xsl:when test="count($lines/cml:item/cml:line)-2 &gt; 1" >  <!-- Electronic band gap graph, multiple lines defined per serie -->
                                                      <xsl:for-each select="1 to count($lines/cml:item/cml:line)-2">
                                                          <xsl:variable name="line" select="." />
                                                          <xsl:variable name="from" select="xs:integer($lines/cml:item/cml:line[$line]/@index) + 1" />
                                                          <xsl:variable name="to" select="xs:integer($lines/cml:item/cml:line[$line+1]/@index)" />
                                                          
                                                          var graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_x_<xsl:value-of select="$line"/> = 
                                                          [<xsl:for-each select="$from to $to">
                                                              <xsl:variable name="index" select="."/>
                                                              <xsl:if test="number($y[$index]) &lt; 10000000"><xsl:value-of select="$x[$index]"/><xsl:if test="position() != last()">,</xsl:if></xsl:if> <!-- Discard exteme, non-printable values -->
                                                          </xsl:for-each>];
                                                          
                                                          var graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_y_<xsl:value-of select="$line"/> = 
                                                          [<xsl:for-each select="$from to $to">
                                                              <xsl:variable name="index" select="."/>
                                                              <xsl:if test="number($y[$index]) &lt; 10000000"><xsl:value-of select="$y[$index]"/><xsl:if test="position() != last()">,</xsl:if></xsl:if> <!-- Discard exteme, non-printable values -->
                                                          </xsl:for-each>];
                                                          
                                                          serie = {
                                                            x: graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_x_<xsl:value-of select="$line"/>,
                                                            y: graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_y_<xsl:value-of select="$line"/>,
                                                            line: {       
                                                              <xsl:choose>
                                                                  <xsl:when test="exists($color)">
                                                                      color: '<xsl:value-of select="$color"/>'
                                                                  </xsl:when>
                                                                  <xsl:otherwise>
                                                                      color: getLineColor('#dc3545', '#1b47ae', graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_y_<xsl:value-of select="$line"/>)            
                                                                  </xsl:otherwise>
                                                              </xsl:choose>
                                                            },
                                                            type: 'scatter',                                                
                                                            name: '<xsl:value-of select="$name"/>_<xsl:value-of select="$line"/>',
                                                            legendgroup: '<xsl:value-of select="$name"/>',
                                                            legendgrouptitle: { 
                                                                text: '<xsl:value-of select="$name"/>  group'
                                                            },
                                                            showlegend: false 
                                                          };                                               
                                                          data.push(serie);                                                                                                
                                                      </xsl:for-each>
                                                            // This dummy serie is included just to enable/disable the entire group from the legend
                                                            serie = {
                                                              x: [0],
                                                              y: [0],
                                                              line: {
                                                                <xsl:choose>
                                                                    <xsl:when test="exists($color)">
                                                                  color: '<xsl:value-of select="$color"/>',
                                                                    </xsl:when>
                                                                    <xsl:otherwise>
                                                                  color: '#000',            
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                                  dash: '<xsl:value-of select="$style" />'
                                                              },
                                                              mode: 'lines',
                                                              type: 'scatter',
                                                              name: '<xsl:value-of select="$name"/>',
                                                              legendgroup: '<xsl:value-of select="$name"/>',
                                                              legendgrouptitle: { 
                                                                text: '<xsl:value-of select="$name"/>  group'
                                                              }
                                                            };
                                                            data.push(serie);
                                                  </xsl:when>
                                                  <xsl:otherwise>
                                                      
                                                      var graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_x = [ <xsl:value-of select="replace(./cml:array[@dictRef='cc:x2'],'\s+',',')"/>];
                                                      var graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_y = [ <xsl:value-of select="replace(./cml:array[@dictRef='cc:y2'],'\s+',',')"/>];
                                                      
                                                      serie = {
                                                        x: graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_x,
                                                        y: graph<xsl:value-of select="$outerIndex"/>_serie<xsl:value-of select="$innerIndex"/>_y,
                                                        type: 'scatter',
                                                        line: {
                                                      <xsl:choose>
                                                          <xsl:when test="matches(lower-case($name),'sum')">
                                                              color: '#666666',
                                                          </xsl:when>
                                                          <xsl:when test="exists($color)">
                                                              color: '<xsl:value-of select="$color"/>',
                                                          </xsl:when>
                                                      </xsl:choose>
                                                            dash: '<xsl:value-of select="$style" />'
                                                        },
                                                        name: '<xsl:value-of select="$name"/>'                                                        
                                                        };
                                                      data.push(serie);
                                                  </xsl:otherwise>
                                              </xsl:choose>                                               
                                            </xsl:for-each>
                                        
                                            var minY = getMinY(data);
                                            var maxY = getMaxY(data);
                                            
                                            var layout = {
                                               title: '<xsl:value-of select="$graph/cml:scalar[@dictRef='cc:title']"/>',
                                               height: 450,
                                               showlegend: true,
                                               xaxis: {
                                                   title: '[<xsl:value-of select="$graph/cml:list[@id='axis']/cml:list[@id='x']/cml:scalar[@dictRef='cc:title']" />]',
                                                   showgrid: true,
                                                   tickmode: 'auto'
                                               },
                                               hovermode:'closest',
                                                <xsl:if test="exists($graph/cml:list[@id='axis']/cml:list[@id='x']/cml:list[@id='ticks'])">
                                                   shapes: [
                                                   <xsl:for-each select="$graph/cml:list[@id='axis']/cml:list[@id='x']/cml:list[@id='ticks']/cml:list[@id='tick']">
                                                      <xsl:variable name="tick" select="."/>                                                       
                                                       {
                                                            name: 'X',
                                                            type: 'line',
                                                            x0: <xsl:value-of select="$tick/cml:scalar[@dictRef='cc:x2']"/>,
                                                            x1: <xsl:value-of select="$tick/cml:scalar[@dictRef='cc:x2']"/>,
                                                            y0: minY,
                                                            y1: maxY,
                                                            line: {
                                                                color: '#444',
                                                                dash: 'longdash',
                                                                width: 2
                                                            }
                                                       }<xsl:if test="position() != last()">,</xsl:if>
                                                   </xsl:for-each>
                                                    ],
                                                    annotations: [
                                                    <xsl:for-each select="$graph/cml:list[@id='axis']/cml:list[@id='x']/cml:list[@id='ticks']/cml:list[@id='tick']">
                                                        <xsl:variable name="tick" select="."/>                                                       
                                                        {
                                                            x: <xsl:value-of select="$tick/cml:scalar[@dictRef='cc:x2']"/>,
                                                            y: minY,
                                                            xref: 'x',
                                                            yref: 'y',
                                                            text: '<xsl:value-of select="$tick/cml:scalar[@dictRef='cc:title']"/>',
                                                            showarrow: true, 
                                                            arrowhead: 7,
                                                            ax: 10,
                                                            ay: 10
                                                        }<xsl:if test="position() != last()">,</xsl:if>
                                                    </xsl:for-each>
                                                    ],
                                               </xsl:if>
                                               yaxis: {
                                                   title: '[<xsl:value-of select="$graph/cml:list[@id='axis']/cml:list[@id='y']/cml:scalar[@dictRef='cc:title']" />]',
                                               }                                                                                    
                                            };
                                        
                                            Plotly.react('graphPlotlyContainer-<xsl:value-of select="$outerIndex"/>', data, layout,  {responsive: true, showSendToCloud: true});
                                        
                                            $('#graphs-<xsl:value-of select="generate-id($graphs[1])"/>').on('shown.bs.collapse', function () {                                            
                                                Plotly.relayout($('#graphPlotlyContainer-<xsl:value-of select="$outerIndex" />')[0],{autosize: true});                                            
                                            });
                                        });
                                    </script>
                                </div>
                            </xsl:for-each>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- Timing -->
    <xsl:template name="timing">
        <xsl:variable name="inittime" select="$finalization/cml:propertyList/cml:property[@dictRef='ca:inittime']" />
        <xsl:variable name="calctime" select="$finalization/cml:propertyList/cml:property[@dictRef='ca:calctime']" />
        <xsl:variable name="endtime" select="$finalization/cml:propertyList/cml:property[@dictRef='ca:endtime']" />                
        <xsl:variable name="wallTime" select="$finalization/cml:propertyList/cml:property[@dictRef='cc:wallTtime']" />
        <xsl:if test="exists($wallTime)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#timing-{generate-id($wallTime)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Timing
                    </h4>
                </div>
                <div id="timing-{generate-id($wallTime)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-lg-6">
                                <table id="timingT-{generate-id($wallTime)}" class="display nowrap">
                                    <thead>
                                        <tr>
                                            <th>Timing</th>
                                            <th> </th>
                                            <th></th>
                                        </tr> 
                                    </thead>
                                    <tbody>                                                         
                                        <tr>
                                            <td>Initialisation time</td>
                                            <td><xsl:value-of select="$inittime"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($inittime/cml:scalar/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Calculation time</td>
                                            <td><xsl:value-of select="$calctime"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($calctime/cml:scalar/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Finalisation time</td>
                                            <td><xsl:value-of select="$endtime"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($endtime/cml:scalar/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Total time</td>
                                            <td><xsl:value-of select="$wallTime" /></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($wallTime/cml:scalar/@units)"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#timingT-<xsl:value-of select="generate-id($wallTime)"/>').dataTable( {                                                
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [                                    
                                            { "sClass": "text-right", "aTargets": [ 1 ] }
                                            ]
                                        } );   
                                    } );                                       
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template> 

    <xsl:function name="helper:printSectionRows">
        <xsl:param name="params" />
        <xsl:param name="title" />
        
        <xsl:for-each select="$params/cml:parameter[@title=$title]">
            <xsl:variable name="param" select="."/>
            <tr>
                <td><xsl:value-of select="$param/cml:scalar[@dictRef='x:label']" /></td>
                <td><xsl:value-of select="$param/cml:scalar[@dictRef='x:value']" /></td>
                <td><xsl:if test="exists($param/cml:scalar[@dictRef='x:value']/@units)"><xsl:value-of select="helper:printUnitSymbol($param/cml:scalar[@dictRef='x:value']/@units)"/></xsl:if></td>
            </tr>
        </xsl:for-each>
    </xsl:function>
   
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>        
</xsl:stylesheet>
