<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet 
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:gaussian="http://www.gaussian.com/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk gaussian helper cmlx"
    version="2.0">    
    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b> Apr 5, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>            
        </xd:desc>     
    </xd:doc>
    <xsl:include href="helper/chemistry/gaussian.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>    
    <xsl:param name="browseurl"/>
    <xsl:param name="jcampdxurl"/>
    
    <xsl:variable name="isOptimization" select="exists(//module[@cmlx:templateRef='l103']) and exists(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='g:keyword'][matches(., '[Oo][Pp][Tt](\s*=.*)?')])"/>
    <xsl:variable name="isFragmented" select="exists(//molecule//property[@dictRef='g:fragment'])" />
    <xsl:variable name="hasStationaryPoint" select="count(//module[@cmlx:templateRef='l103.optimizedparam']//scalar[@dictRef='g:optimization' and contains(text(),'Stationary point found')]) > 0"/>
    <xsl:variable name="hasMinimum" select="count(//module[@cmlx:templateRef='l103.localminsaddle']/scalar[@dictRef='cc:minmaxts' and contains(text(),'local minimum')])> 0"/>
    <xsl:variable name="isEET" select="count(//module[@id='initialization']/parameterList/parameter[@dictRef='g:keyword']/scalar[matches(lower-case(text()),'eet..*')]) > 0" />
    <xsl:variable name="calcType" select="gaussian:getCalcType($isOptimization,$hasStationaryPoint,$hasMinimum,$isEET)"/>  
    <xsl:variable name="methods">
        <xsl:for-each select="distinct-values(tokenize(string-join(//module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:method']/scalar,' '), '[ /\(\):,]+'))">
            <xsl:variable name="candidate" select="."/>            
            <xsl:if test="gaussian:isMethod($candidate)">
                <xsl:value-of select="$candidate"/><xsl:text> </xsl:text>
            </xsl:if>            
        </xsl:for-each>            
    </xsl:variable>    
    
    <xsl:variable name="qmmModule" select="(//module[@cmlx:templateRef='l101.qmmm'])[1]"/>
    <xsl:variable name="zmatModule" select="//module[@cmlx:templateRef='l101.zmat']"/>
    <xsl:variable name="zmataModule" select="//module[@cmlx:templateRef='l101.zmata']"/>
    <xsl:variable name="charge" select="if((//module[@id='initialization']/molecule/@formalCharge)[1] != '') then
                                            (//module[@id='initialization']/molecule/@formalCharge)[1]
                                        else
                                            (//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@type='chargemult']//scalar[@dictRef='x:formalCharge'])[last()]                                            
                                        "/>
    <xsl:variable name="multiplicity" select="if((//module[@id='initialization']/molecule/@spinMultiplicity)[1] != '') then
                                                   (//module[@id='initialization']/molecule/@spinMultiplicity)[1]
                                              else
                                                   (//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@type='chargemult']//scalar[@dictRef='x:spinMultiplicity'])[last()]
                                              "/>  

    <xsl:variable name="modRedundant" select="//module[@cmlx:templateRef='l101.modredundant']"/>
    <xsl:variable name="pointGroup" select="(//module[@cmlx:templateRef='l202.stoich'])[last()]"/>
    <xsl:variable name="solvationModule" select="//module[@cmlx:templateRef='l301.pcm.standard']"/>
    
    <xsl:variable name="hasVibrations" select="exists(//module[@cmlx:templateRef='l716.freq.chunkx' and @dictRef='cc:vibrations'])"/>
    <!-- Environment module-->
    <xsl:variable name="programParameter" select="(//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']/scalar/text())[1]"/>
    <xsl:variable name="versionParameter" select="(//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:version']/scalar/text())[1]"/>
    <!-- Initialization module -->
    <xsl:variable name="initialMolecule" select="(//module[@dictRef='cc:initialization' and child::molecule])[1]/molecule"/>   
    <!-- Geometry -->
    <xsl:variable name="finalMolecule" select="(//module[@dictRef='cc:finalization' and child::molecule])[last()]//molecule[@id='mol9999']"/>
    <xsl:variable name="pseudopotentials" select="//module[@id='job']//module[@cmlx:templateRef='l301.basis2']/module[@cmlx:templateRef='pseudopot']/module[@cmlx:templateRef='atom']"/>
    <xsl:variable name="centers" select="//module[@id='job'][1]//module[@cmlx:templateRef='l301.basis2'][1]/module[@cmlx:templateRef='centers']"/>
    <xsl:variable name="centers2" select="distinct-values(//module[@id='initialization']/parameterList/parameter[@dictRef='cc:basis']/scalar)"/>
    <xsl:variable name="oniombasis" select="//module[@id='calculation']//module[@cmlx:templateRef='l120a']"/>    
    
    <!-- If exists SCAN calculations, check all YES on convergence table otherwise search for "Stationary point found" text-->
    <xsl:variable name="converged" select="
        if(exists(//module[@cmlx:templateRef='l101.modredundant'])) 
        then
            matches((//module[@cmlx:templateRef='l103.itemconverge'])[last()]/list/array[@dictRef='g:converged'],'^(YES\s*)+$')
        else
            exists(//scalar[@dictRef='g:optimization' and contains(text(),'Stationary point found')])
    "/>    
    <!-- Thermochemistry -->
    <xsl:variable name="thermochemistryModule" select="exists(//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry'])"/>                                                       
    <xsl:variable name="temperature" select="//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry'][1]//scalar[@dictRef='cc:temp']"/>
    <xsl:variable name="pressure" select="//module[@id='finalization']/propertyList/property[@dictRef='cc:thermochemistry'][1]//scalar[@dictRef='cc:press']"/>
    
    <xsl:variable name="quote">"</xsl:variable>
    <xsl:variable name="quote_escaped">\\"</xsl:variable>
    
    <xsl:variable name="singlequote">'</xsl:variable>
    <xsl:variable name="singlequote_escaped">\\'</xsl:variable>
    
    
    <xsl:template match="/">
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>                        
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>                                                
                <xsl:if test="$hasVibrations">
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>                    
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>                            
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>
                </xsl:if>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />                                                
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />                                                
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                       
                    </License>
                </rdf:RDF>
            </head>          
            <body> 
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Atom Info -->                    
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                                                       
                            <xsl:variable name="molecule" select="
                                if(exists($finalMolecule)) 
                                    then $finalMolecule
                                else
                                    $initialMolecule
                                "/>                                                             
                            <div>
                            <xsl:choose>
                                <xsl:when test="$isFragmented">
                                    <xsl:call-template name="atomicCoordinatesWithFragments">
                                        <xsl:with-param name="molecule" select="$molecule"/>
                                        <xsl:with-param name="centers"  select="$centers"/>                                                                                        
                                        <xsl:with-param name="centers2" select="$centers2"/>
                                        <xsl:with-param name="pseudos"  select="$pseudopotentials"/>
                                        <xsl:with-param name="oniombasis" select="$oniombasis"/>
                                    </xsl:call-template>                                    
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:call-template name="atomicCoordinates">
                                        <xsl:with-param name="molecule" select="$molecule"/>
                                        <xsl:with-param name="centers"  select="$centers"/>                                                                                        
                                        <xsl:with-param name="centers2" select="$centers2"/>
                                        <xsl:with-param name="pseudos"  select="$pseudopotentials"/>
                                        <xsl:with-param name="oniombasis" select="$oniombasis"/>
                                    </xsl:call-template>    
                                </xsl:otherwise>
                            </xsl:choose>                                
                                
                            </div>
                        </div>
                    </div>                    
                    <!-- Molecular Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>MOLECULAR INFO</h3>                            
                            <xsl:call-template name="chargemultiplicity"/>
                            <xsl:call-template name="mcscf"/>
                            <xsl:call-template name="qmmmmSection"/>
                            <xsl:call-template name="frozenSection"/>
                            <xsl:call-template name="pointgroupSection"/>
                            <xsl:call-template name="solvatationSection"/>                            
                        </div>
                    </div>                    
                    <!-- Results -->
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">
                            <xsl:for-each select="//module[@cmlx:templateRef='job']">
                                <div id="module-{generate-id(.)}">     
                                    <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                                        <xsl:choose>
                                            <xsl:when test="not($isFragmented)">
                                                <xsl:call-template name="energiesSection"/>        
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:call-template name="energiesFragmentedSection"/>
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        
                                        <xsl:call-template name="spinSection" />  
                                        <xsl:call-template name="frequencies"/>
                                        <xsl:call-template name="espcharges"/>
                                        <xsl:call-template name="mullikenCharges"/>
                                        <xsl:call-template name="dipoleMoment"/>
                                        <xsl:call-template name="excitationEnergies"/>
                                        <xsl:call-template name="eCoupling" />                                    
                                </div>                            
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>
                        </div>
                    </div> 
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {
                             //Add custom styles to tables
                             $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });

                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            }); 
                            $.unblockUI();                             
                        });

                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })      
                        
                        //Vibrational frequency variables and functions
                        var jmolInfo = {
                            width: 560,
                            height: 400,
                            debug: false,
                            color: "0xF0F0F0",
                            use: "HTML5",
                            j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                            disableJ2SLoadMonitor: true,
                            disableInitialConsole: true,
                            animframecallback: "modelchanged",
                            readyFunction: vibrationsLoaded,
                            allowjavascript: true,
                            script: "background white; vibration off; vectors off; sync on;"                                       
                        }
                        
                        var jsvInfo = {
                            width: 560,
                            height: 400,
                            debug: false,
                            color: "0xC0C0C0",
                            use: "HTML5",
                            j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                            script: null,
                            initParams: null,
                            disableJ2SLoadMonitor: true,
                            disableInitialConsole: true,
                            readyFunction: null,
                            allowjavascript: true
                        }

                        function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                            var vibrationcboIndex = $("div[id^=frequencyComboboxDiv] select:enabled option:selected").index();
                            $("div[id^=frequencyComboboxDiv] select:enabled option:eq(" + (objwhat + 1) +")").prop('selected', true);                                                                                                                      
                        }
                        
                        function vibrationsLoading() {                                       
                            $('div.frequencies').block({ 
                                message: '<h3>Loading spectrum</h3>', 
                                css: { border: '3px solid #a00' } 
                            });                                                                                    
                        }
                        
                        function vibrationsLoaded() {
                            $('div.frequencies').unblock();                                                           
                        }                    

                        function clearVibrationApplets() {
                            jsvApplet0 = null;
                            jmolApplet0 = null;
                            $('div.spectrum > div[id^=jsvApplet]').html('');
                            $('div.spectrum > div[id^=jmolApplet]').html('');                            
                        }

                        function collapseVibrationSections(excludedId) {
                            $('div.frequencies').each(function(index, value){
                                if(this.id != 'frequencies-' + excludedId) {
                                    $('div#' + this.id).collapse('hide');
                                }                            
                            });
                        }

                        function loadVibrationApplet(uid, index) {
                            $("div[id^=frequencyComboboxDiv] select :nth-child(1)").prop('selected', true);
                            
                            $("div[id^=frequencyComboboxDiv] select").attr('disabled', true);
                            $("div[id=frequencyComboboxDiv-" + uid +"] select").removeAttr('disabled');
                            
                            jsvApplet0 = "jsvApplet0";
                            jmolApplet0 = "jmolApplet0";                                
                            Jmol.setAppletSync([jsvApplet0, jmolApplet0], ["load <xsl:value-of select="$jcampdxurl" />&amp;index=" + index, ""], true);
                            
                            jsvApplet0 = Jmol.getJSVApplet("jsvApplet0", jsvInfo);
                            $("div#jsvApplet-" + uid).html(Jmol.getAppletHtml(jsvApplet0));                                            
                            jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                            $("div#jmolApplet-" + uid).html(Jmol.getAppletHtml(jmolApplet0));
                            vibrationsLoading();   
                        }
                        
                        function changeVibration(uid) {
                            var script = $('div#frequencyComboboxDiv-' + uid + " select").val();
                            Jmol.script(jmolApplet0, script);
                        }
                        
                    </script>                   
                </div>
            </body>
        </html>
    </xsl:template>

    <xsl:template name="generalInfo">       
            <div class="page-header">
                <h3>GENERAL INFO</h3>
                <table>
                    <xsl:if test="$title">
                        <tr>
                            <td>Title:</td>
                            <td>
                                <xsl:value-of select="$title"/>
                            </td>
                        </tr>                                   
                    </xsl:if>
                    <xsl:if test="$browseurl">
                        <tr>
                            <td>Browse item:</td>
                            <td>
                                <a href="{$browseurl}">
                                    <xsl:value-of select="$browseurl"/>
                                </a>
                            </td>
                        </tr>
                    </xsl:if>   
                    <tr>
                        <td>Program:</td>
                        <td>
                            <xsl:value-of select="$programParameter"/>                                        
                            <xsl:text> </xsl:text>
                            <xsl:value-of select="$versionParameter" />
                        </td>
                    </tr>
                    <xsl:if test="$author">
                        <tr>
                            <td>Author:</td>
                            <td>
                                <xsl:value-of select="$author"/>
                            </td>
                        </tr>
                    </xsl:if>
                    <tr>
                        <td>Formula:</td>
                        <td>
                            <xsl:value-of select="(//formula/@concise)[1]"/>
                        </td>
                    </tr>                               
                    <tr>
                        <td>Calculation type:</td>
                        <td>                                                                  
                            <xsl:value-of select="$calcType"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Method(s):</td>
                        <td>
                            <xsl:value-of select="$methods"/>                            
                            <xsl:variable name="dispersion" select="(.//module[@cmlx:templateRef='l301.dispersion'])[last()]"/>
                            <xsl:if test="exists($dispersion)"> - <xsl:value-of select="$dispersion/scalar[@dictRef='g:empdispersion']"/></xsl:if>                            
                        </td>
                    </tr>
                    <xsl:if test="$thermochemistryModule">
                        <tr>
                            <td>                                           
                                <xsl:text>Temperature</xsl:text>
                            </td>                                   
                            <td>
                                <xsl:value-of select="$temperature"/>   
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="helper:printUnitSymbol(($temperature/@units)[last()])"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <xsl:text>Pressure</xsl:text>
                            </td>
                            <td>
                                <xsl:value-of select="$pressure"/>
                                <xsl:text> </xsl:text>
                                <xsl:value-of select="helper:printUnitSymbol(($pressure/@units)[last()])"/>
                            </td>
                        </tr>    
                    </xsl:if>                                
                </table>         
            </div>        
    </xsl:template>

    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">
        <xsl:param name="molecule"/>
        <xsl:param name="centers"/>
        <xsl:param name="centers2"/>
        <xsl:param name="pseudos"/>
        <xsl:param name="oniombasis"/>
        <xsl:variable name="collapseAccordion" select="if(count($molecule/atomArray/atom) > 10) then '' else 'in'"/>
        <xsl:variable name="isLargeMolecule" select="count($molecule/atomArray/atom) > 500"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                        Atomic coordinates [&#8491;] 
                        <xsl:if test="contains($calcType,$gaussian:GeometryOptimization)">
                            <xsl:choose>
                                <xsl:when test="boolean($converged)">
                                    <small>(optimized)</small>                                    
                                </xsl:when>
                                <xsl:otherwise>                                                      
                                    <small><strong>(calculation did not converge)</strong></small>        
                                </xsl:otherwise>
                            </xsl:choose>                                                           
                        </xsl:if>                    
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-lg-6 col-md-8 col-sm-12">                            
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <xsl:if test="$isLargeMolecule">
                                    <div class="alert alert-warning text-left" role="alert">
                                        Molecule with +500 atoms, extra fields have been ommited for better performance                                        
                                    </div>                                    
                                </xsl:if>
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}"></table>
                            <script type="text/javascript">
                                var oTable;
                                var atomInfo = [ ['','<xsl:value-of select="$title"/>','','',''], <xsl:for-each select="$molecule/atomArray/atom">[<xsl:variable name="outerIndex" select="position()"/><xsl:variable name="elementType" select="@elementType"/><xsl:value-of select="$outerIndex"/>,'<xsl:value-of select="$elementType"/>',<xsl:value-of select="format-number(@x3,'#0.000000')"/>,<xsl:value-of select="format-number(@y3,'#0.000000')"/>,<xsl:value-of select="format-number(@z3,'#0.000000')"/>]<xsl:if test="$outerIndex&lt;count($molecule/atomArray/atom)">,</xsl:if></xsl:for-each>];

                                var atomInfoNoIndex = atomInfo.map(function(val) { return val.slice(1,5)});
                                atomInfo.shift();
                                
                                function getXYZ(){
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons( table, {
                                        buttons: [ {
                                            extend: 'copyHtml5',
                                            text: 'XYZ'
                                        } ]
                                    });
                                    table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));
                                    $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();
                                }

                                $(document).ready(function() {
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "aoColumns": [
                                            { "sTitle": "<xsl:value-of select="count($molecule/atomArray/atom)"/>" },
                                            { "sTitle": "" },
                                            { "sTitle": "" },
                                            { "sTitle": "" }
                                        ],
                                        "aaData" : atomInfoNoIndex
                                    });
                                });
                            </script>                            
                            <xsl:choose>
                                <xsl:when test="not($isLargeMolecule)">
                                    <script type="text/javascript">                                       
                                      $(document).ready(function() {                                        
                                        oTable = $('table#atomicCoordinates').dataTable( {
                                        "aaData": [ /* Reduced data set */
                                        
                                        <xsl:for-each select="$molecule/atomArray/atom">
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            <xsl:variable name="id" select="@id"/>
                                            <xsl:variable name="elementType" select="@elementType"/>
                                            <xsl:variable name="type">
                                                <xsl:choose>
                                                    <xsl:when test="exists($centers)">
                                                        <xsl:for-each select="$centers">
                                                            <xsl:variable name="basis" select="./scalar[@dictRef='cc:basis']"/>
                                                            <xsl:for-each select="tokenize(./array[@dictRef='cc:atomcount'],'(\s|\n)')">
                                                                <xsl:if test=".=string($outerIndex)">
                                                                    <xsl:value-of select="$basis"/><xsl:text>  </xsl:text>
                                                                </xsl:if>
                                                            </xsl:for-each>
                                                        </xsl:for-each>
                                                    </xsl:when>
                                                    <xsl:when test="exists($oniombasis)">
                                                        <xsl:variable name="level" select="(//module[@cmlx:templateRef='l101.qmmm'])[1]/list[@cmlx:templateRef='isotope'][$outerIndex]/scalar[@dictRef='x:layer']"/>
                                                        <xsl:choose>
                                                            <xsl:when test="$level = 'H'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'high']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                            <xsl:when test="$level = 'M'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'med']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                            <xsl:when test="$level = 'L'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'low']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                        </xsl:choose>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="$centers2"/>
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable> 
                                            <!-- Core information -->
                                            <xsl:variable name="core">
                                                <xsl:for-each select="$centers">                                           
                                                    <xsl:variable name="shell" select="./list[@cmlx:templateRef='shell']"/>                                                        
                                                    <xsl:for-each select="tokenize(./array[@dictRef='cc:atomcount'],'(\s|\n)')">                               
                                                        <xsl:if test=".=string($outerIndex) and count($shell) > 0">                                                
                                                            <xsl:for-each select="$shell">
                                                                <xsl:variable name="currentShell" select="."/>
                                                                <xsl:variable name="exponents" select="replace($currentShell/array[@dictRef='x:exponent'],'(\s+|\n+)',',')"/>
                                                                <xsl:variable name="coefficients" select="replace($currentShell/array[@dictRef='x:coeficient'],'(\s+|\n+)',',')"/>                                                        
                                                                ['<xsl:value-of select="$currentShell/scalar[@dictRef='x:itype']"/>',
                                                                '<xsl:value-of select="$currentShell/scalar[@dictRef='x:ngauss']"/><xsl:text> </xsl:text><xsl:value-of select="$currentShell/scalar[@dictRef='x:scale']"/>',                                                 
                                                                [<xsl:value-of select="$exponents"/>],[<xsl:value-of select="$coefficients"/>]                                                            
                                                                ]<xsl:if test="position() &lt; count($shell)"><xsl:text>,</xsl:text></xsl:if>                                                          
                                                            </xsl:for-each>                                            
                                                        </xsl:if>                                    
                                                    </xsl:for-each>                                            
                                                </xsl:for-each>                                                                                                                                                       
                                            </xsl:variable>                                            
                                            <!-- Pseudopotentials information -->                                        
                                            <xsl:variable name="pseudo" select="$pseudos[child::scalar[@dictRef='cc:serial'] = $outerIndex]"/>
                                            <xsl:variable name="pseudolines">
                                                <xsl:choose>
                                                    <xsl:when test="not(exists($pseudo/scalar[@dictRef='cc:nopseudo']))">                                              
                                                        [<xsl:for-each select="$pseudo//module[@cmlx:templateRef='params']">                                                   
                                                            <xsl:variable name="angmomentum" select=".//scalar[@dictRef='cc:angmomentum']"/>
                                                            <xsl:variable name="powerofr" select="tokenize(.//array[@dictRef='g:powerofr'],'\s+')"/>
                                                            <xsl:variable name="basisexponent" select="tokenize(.//array[@dictRef='cc:basisexponent'],'\s+')"/>
                                                            <xsl:variable name="expcoeff" select="tokenize(.//array[@dictRef='cc:expcoeff'],'\s+')"/>
                                                            <xsl:variable name="socoeff" select="tokenize(.//array[@dictRef='g:socoeff'],'\s+')"/>
                                                            <xsl:for-each select="1 to count($powerofr)">
                                                                <xsl:variable name="innerIndex" select="."/>["<xsl:value-of select="$angmomentum"/>","<xsl:value-of select="$powerofr[$innerIndex]"/>","<xsl:value-of select="$basisexponent[$innerIndex]"/>","<xsl:value-of select="$expcoeff[$innerIndex]"/>","<xsl:value-of select="$socoeff[$innerIndex]"/>"],                                                                                                              
                                                            </xsl:for-each>                                                                                                        
                                                        </xsl:for-each>]
                                                    </xsl:when>
                                                    <xsl:otherwise>'',''</xsl:otherwise>
                                                </xsl:choose>                                                
                                            </xsl:variable>
                                            [<xsl:value-of select="$outerIndex"/>,'<xsl:value-of select="$elementType"/>','<xsl:value-of select="format-number(@x3,'#0.0000')"/>','<xsl:value-of select="format-number(@y3,'#0.0000')"/>','<xsl:value-of select="format-number(@z3,'#0.0000')"/>','<xsl:value-of select="distinct-values($type)"/>',<xsl:choose><xsl:when test="matches($core,'^\s*$')">'','',</xsl:when><xsl:otherwise>'<span class="fa fa-plus"/>',[<xsl:value-of select="$core"/>],</xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="not(exists($pseudo)) or exists($pseudo/scalar[@dictRef='cc:nopseudo'])">'',''</xsl:when><xsl:otherwise>'<span class="fa fa-plus"/>',<xsl:value-of select="$pseudolines"/></xsl:otherwise></xsl:choose>]<xsl:if test="(position() &lt; count($molecule/atomArray/atom))"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>                                        
                                        ],
                                        
                                        "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" },
                                            { "sTitle": "TYPE", "sClass" : "nowrap" },
                                            { "sTitle": "Core" },                                
                                            { "sTitle": "Core" },
                                            { "sTitle": "ECP"  }, 
                                            { "sTitle": "ECP"  }
                                        ],
                                        "aoColumnDefs" : [
                                            { "bVisible": false, "aTargets": [ 7 ] },
                                            { "bVisible": false, "aTargets": [ 9 ]  }                                
                                        ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                        });
                                        
                                        function fnFormatCoreECP ( nTr )
                                        {                                
                                            var aData = oTable.fnGetData( nTr );                                
                                            var sOut = '';
                                            if(aData[7].length > 0){
                                            sOut +='<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'; 
                                                for(var i=0; i&lt; aData[7].length; i++) {                                    
                                                sOut += '<tr><td>' + aData[7][i][0] + '</td><td>' + aData[7][i][1] + '</td><td></td></tr>';
                                                for(var j=0; j &lt; aData[7][i][2].length; j++)
                                                sOut += '<tr><td></td><td class="nowrap">Exponent =' + aData[7][i][2][j] + '</td><td class="nowrap">Coefficients = ' + aData[7][i][3][j] + '</td></tr>';    
                                                }
                                                sOut += '</table>';
                                            }
                                            if(aData[9].length > 0){
                                            sOut += '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
                                                sOut += '<tr><td>Angular Momentum</td><td>Power of R</td><td>Exponent</td><td>Coefficient</td><td>SO-Coeffient</td></tr>';
                                                for(var i=0; i&lt; aData[9].length; i++) {
                                                sOut += '<tr><td>' + aData[9][i][0] + '</td><td class="right">' + aData[9][i][1] + '</td><td class="right">' + aData[9][i][2] + '</td><td class="right">' + aData[9][i][3] + '</td><td class="right">' + aData[9][i][4] + '</td></tr>';    
                                                }
                                                sOut += '</table>';                                         
                                            }
                                            return sOut;
                                        }
                                        
                                        $('#atomicCoordinates tbody tr td span.fa-plus , #atomicCoordinates tbody tr td span.fa-minus').on( 'click', function () {
                                            var nTr = $(this).parents('tr')[0];
                                            if ( oTable.fnIsOpen(nTr) ){
                                                /* This row is already open - close it */
                                                $(this).toggleClass("fa-plus fa-minus");
                                                oTable.fnClose( nTr );
                                            } else {
                                                /* Open this row */
                                                $(this).toggleClass("fa-plus fa-minus");
                                                oTable.fnOpen( nTr, fnFormatCoreECP(nTr), 'details' );
                                            }
                                        });                                        
                                        });
                                    </script>
                                </xsl:when>
                                <xsl:otherwise>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            oTable = $('table#atomicCoordinates').dataTable({
                                                "aaData": atomInfo,
                                                "aoColumns": [
                                                    { "sTitle": "ATOM" },
                                                    { "sTitle": "" },
                                                    { "sTitle": "x", "sClass": "right" },
                                                    { "sTitle": "y", "sClass": "right" },
                                                    { "sTitle": "z", "sClass": "right" }
                                                ],
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false
                                            });                       
                                        });
                                    </script>                                    
                                </xsl:otherwise>
                            </xsl:choose>

                            <table class="display" id="atomicCoordinates"></table>                                                                              
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>
    
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinatesWithFragments">
        <xsl:param name="molecule"/>
        <xsl:param name="centers"/>
        <xsl:param name="centers2"/>
        <xsl:param name="pseudos"/>
        <xsl:param name="oniombasis"/>
        <xsl:variable name="collapseAccordion" select="if(count($molecule/atomArray/atom) > 10) then '' else 'in'"/>
        <xsl:variable name="isLargeMolecule" select="count($molecule/atomArray/atom) > 500"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;] 
                    <xsl:if test="contains($calcType,$gaussian:GeometryOptimization)">
                        <xsl:choose>
                            <xsl:when test="boolean($converged)">
                                <small>(optimized)</small>                                    
                            </xsl:when>
                            <xsl:otherwise>                                                      
                                <small><strong>(calculation did not converge)</strong></small>        
                            </xsl:otherwise>
                        </xsl:choose>                                                           
                    </xsl:if>                    
                </h4>
            </div>            
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse {$collapseAccordion}">
                <div class="panel-body">                    
                    <div class="row bottom-buffer">
                        <div class="col-lg-6 col-md-8 col-sm-12">                            
                            <div id="xyz-{generate-id($molecule)}" class="right clearfix">
                                <xsl:choose>
                                    <xsl:when test="$isLargeMolecule">
                                        <div class="alert alert-warning text-left" role="alert">
                                            Molecule with +500 atoms, extra fields have been ommited for better performance                                        
                                        </div>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <div id="fragment-selector" class="float-left">                                                                                  
                                            <select id="fragment" class="mb-2" onchange="javascript:fragmentChanged()">
                                                <option value="" selected="selected" >Supermolecule</option>
                                                <xsl:for-each select="distinct-values($molecule//property[@dictRef='g:fragment']/scalar)">
                                                    <option value="{.}">Fragment <xsl:value-of select="."/></option>
                                                </xsl:for-each>                                                
                                            </select>                                        
                                        </div>
                                    </xsl:otherwise>
                                </xsl:choose>
                                <a class="text-right" href="javascript:getFragmentXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="coordsT-{generate-id($molecule)}"></table>
                            <script type="text/javascript">
                                var coordsTable = null;
                                var xyzTable = null;                                
                                var atomInfo = [<xsl:for-each select="$molecule/atomArray/atom">[<xsl:variable name="outerIndex" select="position()"/><xsl:variable name="elementType" select="@elementType"/><xsl:value-of select="./property[@dictRef='g:fragment']/scalar"/>,<xsl:value-of select="$outerIndex"/>,'<xsl:value-of select="$elementType"/>',<xsl:value-of select="format-number(@x3,'#0.000000')"/>,<xsl:value-of select="format-number(@y3,'#0.000000')"/>,<xsl:value-of select="format-number(@z3,'#0.000000')"/>]<xsl:if test="$outerIndex&lt;count($molecule/atomArray/atom)">,</xsl:if></xsl:for-each>];
                                
                                function getFragmentXYZ() {
                                    setupXYZTable($('#fragment option:selected').val());                                
                                    downloadXYZAction();
                                    $('div#xyz-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();
                                }
                              
                                function setupXYZTable(fragment) {                                
                                    var atomInfoNoIndex = atomInfo
                                                                .filter((item) => (fragment == undefined || fragment == '' || item[0] == fragment))
                                                                .map((val) => val.slice(2,6));
                                                                
                                    atomInfoNoIndex.unshift(['<xsl:value-of select="$title"/>','','','']);
                                
                                    if(xyzTable != null) {
                                        xyzTable.destroy();
                                    }                                    
                                    
                                    xyzTable = $('table#coordsT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false,
                                        "aoColumns": [
                                        { "sTitle": atomInfoNoIndex.length - 1 },
                                            { "sTitle": "" },
                                            { "sTitle": "" },
                                            { "sTitle": "" }
                                        ],
                                        "aaData" : atomInfoNoIndex
                                    });
                                };
                                
                                function downloadXYZAction() {
                                    var table = $('table#coordsT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons( table, {
                                        buttons: [ {
                                            extend: 'copyHtml5',
                                            text: 'XYZ'
                                        } ]
                                    });
                                    table.buttons().container().appendTo($('div#xyz-<xsl:value-of select="generate-id($molecule)"/>'));                                
                                }
                            </script>                            
                            <xsl:choose>
                                <xsl:when test="not($isLargeMolecule)">                                  
                                    <script type="text/javascript">
                                        function fragmentChanged() {                                   
                                            //Filter table 
                                            var fragmentSelected = $('#fragment option:selected').val()
                                            $('#atomicCoordinates').DataTable().columns(2).search(fragmentSelected, false, true).draw();
                                            // Show download button again
                                            if( $.fn.dataTable.isDataTable('#coordsT-<xsl:value-of select="generate-id($molecule)"/>'))
                                                $('#coordsT-<xsl:value-of select="generate-id($molecule)"/>').DataTable().button(0).remove();                                          
                                            $('div#xyz-<xsl:value-of select="generate-id($molecule)"/> span.fa').show();
                                        }
                                        
                                        $(document).ready(function() {
                                          $('select#fragment').val('');
                                          coordsTable = $('table#atomicCoordinates').dataTable( {
                                               "aaData": [ /* Reduced data set */
                                               <xsl:for-each select="$molecule/atomArray/atom">
                                                   <xsl:variable name="outerIndex" select="position()"/>
                                                   <xsl:variable name="id" select="@id"/>
                                                   <xsl:variable name="elementType" select="@elementType"/>
                                                   <xsl:variable name="fragment" select="./property[@dictRef='g:fragment']" />
                                                   <xsl:variable name="type">
                                                       <xsl:choose>
                                                           <xsl:when test="exists($centers)">
                                                               <xsl:for-each select="$centers">
                                                                   <xsl:variable name="basis" select="./scalar[@dictRef='cc:basis']"/>
                                                                   <xsl:for-each select="tokenize(./array[@dictRef='cc:atomcount'],'(\s|\n)')">
                                                                       <xsl:if test=".=string($outerIndex)">
                                                                           <xsl:value-of select="$basis"/><xsl:text>  </xsl:text>
                                                                       </xsl:if>
                                                                   </xsl:for-each>
                                                               </xsl:for-each>
                                                           </xsl:when>
                                                           <xsl:when test="exists($oniombasis)">
                                                               <xsl:variable name="level" select="(//module[@cmlx:templateRef='l101.qmmm'])[1]/list[@cmlx:templateRef='isotope'][$outerIndex]/scalar[@dictRef='x:layer']"/>
                                                               <xsl:choose>
                                                                   <xsl:when test="$level = 'H'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'high']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                                   <xsl:when test="$level = 'M'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'med']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                                   <xsl:when test="$level = 'L'"><xsl:value-of select="$oniombasis[child::scalar[@dictRef='x:level']/text() = 'low']/scalar[@dictRef='cc:basis']"/></xsl:when>
                                                               </xsl:choose>
                                                           </xsl:when>
                                                           <xsl:otherwise>
                                                               <xsl:value-of select="$centers2"/>
                                                           </xsl:otherwise>
                                                       </xsl:choose>
                                                   </xsl:variable> 
                                                   <!-- Core information -->
                                                   <xsl:variable name="core">
                                                       <xsl:for-each select="$centers">                                           
                                                           <xsl:variable name="shell" select="./list[@cmlx:templateRef='shell']"/>                                                        
                                                           <xsl:for-each select="tokenize(./array[@dictRef='cc:atomcount'],'(\s|\n)')">                               
                                                               <xsl:if test=".=string($outerIndex) and count($shell) > 0">                                                
                                                                   <xsl:for-each select="$shell">
                                                                       <xsl:variable name="currentShell" select="."/>
                                                                       <xsl:variable name="exponents" select="replace($currentShell/array[@dictRef='x:exponent'],'(\s+|\n+)',',')"/>
                                                                       <xsl:variable name="coefficients" select="replace($currentShell/array[@dictRef='x:coeficient'],'(\s+|\n+)',',')"/>                                                        
                                                                       ['<xsl:value-of select="$currentShell/scalar[@dictRef='x:itype']"/>',
                                                                       '<xsl:value-of select="$currentShell/scalar[@dictRef='x:ngauss']"/><xsl:text> </xsl:text><xsl:value-of select="$currentShell/scalar[@dictRef='x:scale']"/>',                                                 
                                                                       [<xsl:value-of select="$exponents"/>],[<xsl:value-of select="$coefficients"/>]                                                            
                                                                       ]<xsl:if test="position() &lt; count($shell)"><xsl:text>,</xsl:text></xsl:if>                                                          
                                                                   </xsl:for-each>                                            
                                                               </xsl:if>                                    
                                                           </xsl:for-each>                                            
                                                       </xsl:for-each>                                                                                                                                                       
                                                   </xsl:variable>                                            
                                                   <!-- Pseudopotentials information -->                                        
                                                   <xsl:variable name="pseudo" select="$pseudos[child::scalar[@dictRef='cc:serial'] = $outerIndex]"/>
                                                   <xsl:variable name="pseudolines">
                                                       <xsl:choose>
                                                           <xsl:when test="not(exists($pseudo/scalar[@dictRef='cc:nopseudo']))">                                              
                                                               [<xsl:for-each select="$pseudo//module[@cmlx:templateRef='params']">                                                   
                                                                   <xsl:variable name="angmomentum" select=".//scalar[@dictRef='cc:angmomentum']"/>
                                                                   <xsl:variable name="powerofr" select="tokenize(.//array[@dictRef='g:powerofr'],'\s+')"/>
                                                                   <xsl:variable name="basisexponent" select="tokenize(.//array[@dictRef='cc:basisexponent'],'\s+')"/>
                                                                   <xsl:variable name="expcoeff" select="tokenize(.//array[@dictRef='cc:expcoeff'],'\s+')"/>
                                                                   <xsl:variable name="socoeff" select="tokenize(.//array[@dictRef='g:socoeff'],'\s+')"/>
                                                                   <xsl:for-each select="1 to count($powerofr)">
                                                                       <xsl:variable name="innerIndex" select="."/>["<xsl:value-of select="$angmomentum"/>","<xsl:value-of select="$powerofr[$innerIndex]"/>","<xsl:value-of select="$basisexponent[$innerIndex]"/>","<xsl:value-of select="$expcoeff[$innerIndex]"/>","<xsl:value-of select="$socoeff[$innerIndex]"/>"],                                                                                                              
                                                                   </xsl:for-each>                                                                                                        
                                                               </xsl:for-each>]
                                                           </xsl:when>
                                                           <xsl:otherwise>'',''</xsl:otherwise>
                                                       </xsl:choose>                                                
                                                   </xsl:variable>
                                                   [<xsl:value-of select="$outerIndex" />,'<xsl:value-of select="$elementType"/>',<xsl:value-of select="$fragment"/>,'<xsl:value-of select="format-number(@x3,'#0.0000')"/>','<xsl:value-of select="format-number(@y3,'#0.0000')"/>','<xsl:value-of select="format-number(@z3,'#0.0000')"/>','<xsl:value-of select="distinct-values($type)"/>',<xsl:choose><xsl:when test="matches($core,'^\s*$')">'','',</xsl:when><xsl:otherwise>'<span class="fa fa-plus"/>',[<xsl:value-of select="$core"/>],</xsl:otherwise></xsl:choose><xsl:choose><xsl:when test="not(exists($pseudo)) or exists($pseudo/scalar[@dictRef='cc:nopseudo'])">'',''</xsl:when><xsl:otherwise>'<span class="fa fa-plus"/>',<xsl:value-of select="$pseudolines"/></xsl:otherwise></xsl:choose>]<xsl:if test="(position() &lt; count($molecule/atomArray/atom))"><xsl:text>,</xsl:text></xsl:if>
                                               </xsl:for-each>                                        
                                               ],
                                           
                                               "aoColumns": [
                                                   { "sTitle": "ATOM" },
                                                   { "sTitle": "" },
                                                   { "sTitle": "Fragment" },
                                                   { "sTitle": "x", "sClass": "right" },
                                                   { "sTitle": "y", "sClass": "right" },
                                                   { "sTitle": "z", "sClass": "right" },
                                                   { "sTitle": "TYPE", "sClass" : "nowrap" },
                                                   { "sTitle": "Core" },                                
                                                   { "sTitle": "Core" },
                                                   { "sTitle": "ECP"  }, 
                                                   { "sTitle": "ECP"  }
                                               ],
                                               "aoColumnDefs" : [
                                                   { "bVisible": false, "aTargets": [ 7 ] },
                                                   { "bVisible": false, "aTargets": [ 9 ]  }                                
                                               ],
                                               "bFilter": true,
                                               "bPaginate": false,
                                               "bSort": false,
                                               "bInfo": false
                                           });
                                           
                                           // Hide search box, use fragment selector instead
                                           $('#atomicCoordinates_filter').hide()
                                        
                                        function fnFormatCoreECP ( nTr ){                                
                                            var aData = coordsTable.fnGetData( nTr );                                
                                            var sOut = '';
                                            if(aData[7].length > 0) {
                                                sOut +='<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">'; 
                                                for(var i=0; i&lt; aData[7].length; i++) {                                    
                                                    sOut += '<tr><td>' + aData[7][i][0] + '</td><td>' + aData[7][i][1] + '</td><td></td></tr>';
                                                    for(var j=0; j &lt; aData[7][i][2].length; j++)
                                                        sOut += '<tr><td></td><td class="nowrap">Exponent =' + aData[7][i][2][j] + '</td><td class="nowrap">Coefficients = ' + aData[7][i][3][j] + '</td></tr>';    
                                                }
                                                sOut += '</table>';
                                            }
                                            if(aData[9].length > 0) {
                                                sOut += '<table cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
                                                sOut += '<tr><td>Angular Momentum</td><td>Power of R</td><td>Exponent</td><td>Coefficient</td><td>SO-Coeffient</td></tr>';
                                                for(var i=0; i&lt; aData[9].length; i++) {
                                                    sOut += '<tr><td>' + aData[9][i][0] + '</td><td class="right">' + aData[9][i][1] + '</td><td class="right">' + aData[9][i][2] + '</td><td class="right">' + aData[9][i][3] + '</td><td class="right">' + aData[9][i][4] + '</td></tr>';    
                                                }
                                                sOut += '</table>';                                         
                                            }
                                            return sOut;
                                        }
                                        
                                        $('#atomicCoordinates tbody tr td span.fa-plus , #atomicCoordinates tbody tr td span.fa-minus').on( 'click', function () {
                                            var nTr = $(this).parents('tr')[0];
                                            if ( coordsTable.fnIsOpen(nTr) ) {                                                
                                                $(this).toggleClass("fa-plus fa-minus");
                                                coordsTable.fnClose( nTr );
                                            } else {                                                
                                                $(this).toggleClass("fa-plus fa-minus");
                                                coordsTable.fnOpen( nTr, fnFormatCoreECP(nTr), 'details' );
                                            }
                                        });                                        
                                     });
                                    </script>
                                </xsl:when>
                                <xsl:otherwise>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            coordsTable = $('table#atomicCoordinates').dataTable({
                                                "aaData": atomInfo.map((val) => val.slice(1,6)),
                                                "aoColumns": [
                                                    { "sTitle": "ATOM" },
                                                    { "sTitle": "" },
                                                    { "sTitle": "x", "sClass": "right" },
                                                    { "sTitle": "y", "sClass": "right" },
                                                    { "sTitle": "z", "sClass": "right" }
                                                    ],
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false
                                            });                       
                                        });
                                    </script>                                    
                                </xsl:otherwise>
                            </xsl:choose>
                            
                            <table class="display" id="atomicCoordinates"></table>                                                                              
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>
    
    

    <!-- Charge / Multiplicity section -->
    <xsl:template name="chargemultiplicity">
        <div class="row bottom-buffer">
            <div class="col-md-2 col-sm-6">
                <table class="display" id="chargemultiplicity">
                    <tbody>
                        <tr>
                            <td class="nowrap">Charge / Multiplicity:</td>
                            <td class="nowrap">
                                <xsl:choose>                                            
                                    <xsl:when test="exists($qmmModule)">
                                        <span style="font-weight:normal">
                                            <xsl:variable name="charge" select="tokenize($qmmModule//array[@dictRef='g:charge'],'[\s]')"/>
                                            <xsl:variable name="multiplicity" select="tokenize($qmmModule//array[@dictRef='g:multiplicity'],'[\s]')"/>
                                            <xsl:for-each select="1 to count($charge)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                <xsl:value-of select="concat($charge[$outerIndex], ' ', $multiplicity[$outerIndex], '    ')"></xsl:value-of>
                                            </xsl:for-each>                                                        
                                        </span>
                                    </xsl:when>
                                    <xsl:otherwise>
                                        <span style="font-weight:normal">
                                            <xsl:choose>
                                                <xsl:when test="exists($zmatModule/molecule)">
                                                    <xsl:value-of select="$zmatModule/molecule/@formalCharge"/><xsl:text>   </xsl:text>
                                                    <xsl:value-of select="$zmatModule/molecule/@spinMultiplicity"/>
                                                </xsl:when>
                                                <xsl:when test="exists($zmataModule)">
                                                    <xsl:value-of select="$zmataModule//scalar[@dictRef='x:formalCharge']"/><xsl:text>   </xsl:text>
                                                    <xsl:value-of select="$zmataModule//scalar[@dictRef='x:multiplicity']"/>                                            
                                                </xsl:when>                                        
                                                <xsl:otherwise>                                                    
                                                    <xsl:value-of select="$charge"/><xsl:text>   </xsl:text>
                                                    <xsl:value-of select="$multiplicity"></xsl:value-of>                                                                                                                                   
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </span>
                                    </xsl:otherwise>
                                </xsl:choose>      
                            </td>                            
                        </tr>
                        <xsl:if test="$isFragmented and exists(//module[@cmlx:templateRef='l101.zmatfragment'])">
                            <xsl:variable name="fragCharge" select="tokenize(//module[@cmlx:templateRef='l101.zmatfragment']//array[@dictRef='g:charge'],'\s+')"/>
                            <xsl:variable name="fragMult" select="tokenize(//module[@cmlx:templateRef='l101.zmatfragment']//array[@dictRef='g:mult'],'\s+')"/>
                            <xsl:variable name="fragment" select="tokenize(//module[@cmlx:templateRef='l101.zmatfragment']//array[@dictRef='g:fragment'],'\s+')"/>
                            <xsl:for-each select="1 to count($fragment)">
                                <xsl:variable name="outerIndex" select="."/>
                                <tr>
                                    <td class="nowrap"></td>
                                    <td class="nowrap"><xsl:value-of select="concat($fragCharge[$outerIndex],
                                                                                    ' ', 
                                                                                    $fragMult[$outerIndex], 
                                                                                    ' ( ', 
                                                                                    $fragment[$outerIndex], 
                                                                                    ' )')"/>
                                    </td>                                                                        
                                </tr>
                            </xsl:for-each>                            
                        </xsl:if>
                    </tbody>
                </table>        
            </div>
        </div>      
    </xsl:template>
    
    <xsl:template name="mcscf">
        <xsl:variable name="mcscf" select="//module[@id='calculation']//module[@cmlx:templateRef='l405']"/>
        <xsl:if test="exists($mcscf)">
            <div class="row bottom-buffer">
                <div class="col-md-4 col-sm-12">
                    <table class="display" id="mcscf">
                        <tbody>
                            <tr>
                                <td>Number of orbitals:</td>
                                <td><xsl:value-of select="$mcscf/scalar[@dictRef='g:orbitalnum']"/></td>
                            </tr>
                            <tr>
                                <td>Number of electrons:</td>
                                <td><xsl:value-of select="$mcscf/scalar[@dictRef='g:electronnum']"/></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>            
        </xsl:if>
    </xsl:template>
    <!-- Interatomic distances (bond distances) DISABLED -->
    <xsl:template name="atomicDistances">
        <xsl:param name="atomArray"/>
        <table>
            <xsl:for-each select="(//module[@id='finalization']/molecule[@id='mol9999'])[1]/bondArray/bond">
                <xsl:variable name="atomRefs2" select="tokenize(./@atomRefs2,' ')"/>
                <!-- For each bond calculate interatomic distances -->
                <xsl:variable name="atomType1" select="$atomArray/atom[@id=$atomRefs2[1]]/@elementType"/>
                <xsl:variable name="x1" select="$atomArray/atom[@id=$atomRefs2[1]]/@x3"/>
                <xsl:variable name="y1" select="$atomArray/atom[@id=$atomRefs2[1]]/@y3"/>
                <xsl:variable name="z1" select="$atomArray/atom[@id=$atomRefs2[1]]/@z3"/>
                <xsl:variable name="position1" select="count($atomArray/atom[@id=$atomRefs2[1]]/preceding-sibling::*)+1"/>
                <!-- http://stackoverflow.com/questions/226405/find-position-of-a-node-using-xpath -->
                <xsl:variable name="atomType2" select="$atomArray/atom[@id=$atomRefs2[2]]/@elementType"/>
                <xsl:variable name="x2" select="$atomArray/atom[@id=$atomRefs2[2]]/@x3"/>
                <xsl:variable name="y2" select="$atomArray/atom[@id=$atomRefs2[2]]/@y3"/>
                <xsl:variable name="z2" select="$atomArray/atom[@id=$atomRefs2[2]]/@z3"/>
                <xsl:variable name="position2" select="count($atomArray/atom[@id=$atomRefs2[2]]/preceding-sibling::*)+1"/>
                <xsl:variable name="distance" select="ckbk:sqrt((($x2 - $x1) * ($x2 - $x1)) + ($y2 - $y1) * ($y2 - $y1) + ($z2 - $z1) * ($z2 - $z1))"/>
                <tr>
                    <td class="datacell">
                        <xsl:value-of select="$atomType1"/>
                        <xsl:value-of select="$position1"/>
                    </td>
                    <td>
                        <xsl:text>--</xsl:text>
                    </td>
                    <td class="datacellLeft">
                        <xsl:value-of select="$atomType2"/>
                        <xsl:value-of select="$position2"/>
                    </td>
                    <td class="datacell">
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="format-number($distance,'#0.000000')"/>
                    </td>
                </tr>
            </xsl:for-each>
        </table>
    </xsl:template>

    <!-- QM/MM section (module) -->
    <xsl:template name="qmmmmSection">
        <xsl:if test="exists($qmmModule)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#qmmSection" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        QM/MM section
                    </h4>
                </div>            
                <div id="qmmSection" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-lg-12 col-md-12 col-sm-12">                                
                                <table class="display" id="qmmSectionT"></table>
                                <script type="text/javascript">
                                   $(document).ready(function(){
                                    $("table#qmmSectionT").dataTable({
                                        "aaData" : [
                                    <xsl:for-each select="$qmmModule/list[@cmlx:templateRef='isotope']"><xsl:variable name="outerIndex" select="position()"/><xsl:variable name="coords" select="tokenize(./array[@dictRef='x:coords'],'[\s]')"/>['<xsl:value-of select="./scalar[@dictRef='x:atom']"/>','<xsl:value-of select="./scalar[@dictRef='g:atomicType']"/>','<xsl:value-of select="format-number(number(./scalar[@dictRef='x:charge']),'#0.000000')"/>','<xsl:value-of select="./scalar[@dictRef='x:frozen']"/>','<xsl:value-of select="format-number(number($coords[1]),'#0.000')"/>','<xsl:value-of select="format-number(number($coords[2]),'#0.000')"/>','<xsl:value-of select="format-number(number($coords[3]),'#0.000')"/>','<xsl:value-of select="./scalar[@dictRef='x:layer']"/>','<xsl:value-of select="./scalar[@dictRef='x:atomlink']"/>','<xsl:value-of select="./scalar[@dictRef='x:bondedto']"/>','<xsl:value-of select="./array[@dictRef='x:scalefactor']"/>']<xsl:if test="position() != last()"><xsl:text>,</xsl:text></xsl:if></xsl:for-each>],
                                        "aoColumns" : [
                                           {"sTitle" : ""},
                                           {"sTitle" : ""},
                                           {"sTitle" : ""},
                                           {"sTitle" : "Frozen"},
                                           {"sTitle" : ""},
                                           {"sTitle" : "Coords"},
                                           {"sTitle" : ""},
                                           {"sTitle" : "Layer"},
                                           {"sTitle" : "Atom Link"},
                                           {"sTitle" : "Bonded To"},
                                           {"sTitle" : "Scale Factor"}                                        
                                        ],
                                        "aoColumnDefs" : [
                                            { "sClass": "nowrap", "aTargets": [ 0,8 ] },
                                            { "sClass": "text-right", "aTargets": [ 2,3,4,5,6 ] }
                                        ],
                                        "bFilter": false,
                                        "bPaginate": true,
                                        "bSort": false,
                                        "bInfo": true                                        
                                    });
                                   }); 
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if> 
    </xsl:template>
    
    <!-- Frozen section (module) -->
    <xsl:template name="frozenSection">        
        <xsl:if test="exists($modRedundant)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#frozenSection" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Frozen section
                    </h4>
                </div>            
                <div id="frozenSection" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-6 col-sm-12">                                
                                <table class="display" id="frozenSectionT"></table>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    $("table#frozenSectionT").dataTable({
                                    "aaData" : [  
                                    <xsl:for-each select="$modRedundant/list[@cmlx:templateRef='modred']">
                                        <xsl:variable name="currentRestriction" select="."/>
                                        <xsl:variable name="parameter">
                                            <xsl:choose>
                                                <xsl:when test="exists($currentRestriction/scalar[@dictRef='x:parameter'])">
                                                    <xsl:text>'</xsl:text><xsl:value-of select="$currentRestriction/scalar[@dictRef='x:parameter']"/><xsl:text>'</xsl:text>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <xsl:text>'</xsl:text><xsl:value-of select="$currentRestriction/scalar[@dictRef='x:stepnumber']"/><xsl:text> </xsl:text><xsl:value-of select="$currentRestriction/scalar[@dictRef='x:stepincrement']"/><xsl:text>'</xsl:text>
                                                </xsl:otherwise>
                                            </xsl:choose>                                            
                                        </xsl:variable>                                        
                                            [ '<xsl:value-of select="$currentRestriction/scalar[@dictRef='x:restriction']"/>','<xsl:value-of select="$currentRestriction/array[@dictRef='x:serial']"/>','<xsl:value-of select="$currentRestriction/scalar[@dictRef='x:action']"/>',<xsl:value-of select="$parameter"></xsl:value-of>]<xsl:if test="position()!=last()">,</xsl:if>
                                    </xsl:for-each>
                                    ],
                                    "aoColumns" : [
                                    {"sTitle" : "Restriction"},
                                    {"sTitle" : "Atom index"},
                                    {"sTitle" : "Action"},
                                    {"sTitle" : "Parameters"}                                                                   
                                    ],                                  
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false                                        
                                    });
                                    }); 
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if> 
    </xsl:template>
    
    <!-- Point group section (module) -->
    <xsl:template name="pointgroupSection">
        <xsl:if test="exists($pointGroup)">
            <xsl:variable name="currentModule" select="($pointGroup)[1]"></xsl:variable>            
            <div class="row bottom-buffer">
                <div class="col-md-6 cold-sm-12">
                    <h4>Full point group</h4>
                    <div>
                        <table class="display" id="pointGroupT">
                            <thead>
                                <tr>
                                    <th> </th>
                                    <th> </th>
                                    <th> </th>
                                    <th> </th>
                                </tr>  
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Full point group</td>
                                    <td><xsl:value-of select="$currentModule/scalar[@dictRef='cc:pointgroup']"/></td>
                                    <td>NOp</td>
                                    <td><xsl:value-of select="($currentModule/scalar[@dictRef='cc:pointgroup']/following-sibling::scalar[@dictRef='cc:operatorcount'])[1]"/></td>
                                </tr>                           
                            </tbody>                    
                        </table>
                    </div>                                  
                </div>            
            </div>           
        </xsl:if>        
    </xsl:template>
    
    <!-- Solvatation section (module) -->
    <xsl:template name="solvatationSection">        
        <xsl:if test="exists($solvationModule)">
            <xsl:variable name="currentModule" select="($solvationModule)[1]"/>            
            <div class="row bottom-buffer">
                <div class="col-md-4 cold-sm-8">
                    <h4>Polarizable Continuum Model (PCM)</h4>
                    <div>
                        <table class="display" id="solvationModuleT">
                            <thead>
                                <tr>
                                    <th> </th>                                                   
                                    <th> </th>
                                </tr>  
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Model:</td>                                  
                                    <td><xsl:value-of select="$currentModule/scalar[@dictRef='g:model']"/></td>
                                </tr>
                                <tr>
                                    <td>Atomic radii</td>                                    
                                    <td><xsl:value-of select="$currentModule/scalar[@dictRef='g:atomicradii']"/></td>
                                </tr>
                                <tr>
                                    <td>Solvent</td>                                   
                                    <td><xsl:value-of select="$currentModule/scalar[@dictRef='g:solvent']"/></td>
                                </tr>
                                <tr>
                                    <td></td>                                   
                                    <td>Eps= <xsl:value-of select="$currentModule/scalar[@dictRef='g:eps']"/></td>
                                </tr>
                                <tr>
                                    <td></td>                                  
                                    <td>Eps(inf)= <xsl:value-of select="$currentModule/scalar[@dictRef='g:epsinfinity']"/></td>
                                </tr>                           
                            </tbody>                    
                        </table>
                    </div>                                  
                </div>            
            </div>          
            
            
            
            
            
        </xsl:if>        
    </xsl:template>
 
    <!-- Energies section (module) -->
    <xsl:template name="energiesSection">
        <xsl:variable name="finalEnergy" select="
            if(exists(.//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])) then
                (.//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])[last()]
             else if(exists(.//module[@cmlx:templateRef='l508']/scalar[@dictRef='g:rbhflyp'])) then 
                (.//module[@cmlx:templateRef='l508']/scalar[@dictRef='g:rbhflyp'])[last()]
             else
                (.//scalar[@dictRef='g:rbhflyp'])[last()]"/>
        <xsl:variable name="l502Module" select="(.//module[@cmlx:templateRef='l502.pcm'])[last()]"/>
        <xsl:variable name="l120Module" select="(.//module[@cmlx:templateRef='l120'])[last()]"/>
        <xsl:variable name="l804Module" select="(.//module[@cmlx:templateRef='l804_l906'])[last()]"/>     
        <xsl:variable name="l122Module" select="(.//module[@cmlx:templateRef='l122'])[last()]"/>
        <xsl:variable name="zeropointProperty" select=".//property[@dictRef='cc:zeropoint']"/>                        
        <xsl:variable name="uid" select="concat(generate-id($finalEnergy),generate-id($l502Module),generate-id($l120Module),generate-id($zeropointProperty),generate-id($l122Module))"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#energies-{$uid}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                        Energies                    
                </h4>
            </div>            
            <div id="energies-{$uid}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-8 col-md-8 col-sm-12">
                            <xsl:if test="exists($finalEnergy) or exists($l502Module) or exists($l120Module) or exists($zeropointProperty) or exists($l122Module)">
                                <table class="display" id="energiesT-{$uid}">
                                    <thead>
                                        <tr>
                                            <th>Energy</th>
                                            <th>Value</th>
                                            <th>Units</th>    
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <xsl:if test="exists($finalEnergy)">
                                            <tr> 
                                                <td>SCF Done:</td>
                                                <td><xsl:value-of select="$finalEnergy"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($finalEnergy/@units)"/></td>                                                                    
                                            </tr>                                                                                                              
                                        </xsl:if>
                                        <xsl:if test="exists($l502Module//scalar[@dictRef='cc:dispenergy'])">
                                            <tr>                                
                                                <td>Dispersion energy:</td>
                                                <td><xsl:value-of select="$l502Module//scalar[@dictRef='cc:dispenergy']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($l502Module//scalar[@dictRef='cc:dispenergy']/@units)"></xsl:value-of></td>
                                            </tr>                            
                                        </xsl:if>  
                                        <xsl:if test="exists($l120Module)">
                                            <xsl:variable name="method" select="tokenize($l120Module/array[@dictRef='cc:method'],'[\s]')"/>
                                            <xsl:variable name="system" select="tokenize($l120Module/array[@dictRef='cc:system'],'[\s]')"/>
                                            <xsl:variable name="energy" select="tokenize($l120Module/array[@dictRef='cc:energy'],'[\s]')"/>                            
                                            <xsl:for-each select="1 to count($method)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                <tr>
                                                    <td><xsl:value-of select="concat($method[$outerIndex], ' ', $system[$outerIndex])"/></td>                                    
                                                    <td><xsl:value-of select="$energy[$outerIndex]"/></td>
                                                    <td>Eh</td>
                                                </tr>
                                            </xsl:for-each>    
                                            <tr>
                                                <td>Oniom : Extrapolated energy</td>
                                                <td><xsl:value-of select="$l120Module/scalar[@dictRef='cc:extraenergy']"/></td>
                                                <td>Eh</td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($zeropointProperty)">
                                            <tr>
                                                <td>Zero-point correction</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/ scalar[@dictRef='cc:zpe.correction']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/ scalar[@dictRef='cc:zpe.correction']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Thermal correction to Energy</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrener']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrener']/@units)"></xsl:value-of></td>
                                            </tr> 
                                            <tr>
                                                <td>Thermal correction to Enthalpy</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrenthalpy']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrenthalpy']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Thermal correction to Gibbs Free Energy</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrgfe']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrgfe']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and zero-point Energies</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectzpe']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectzpe']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and thermal Energies</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermal']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermal']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and thermal Enthalpies</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalent']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalent']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and thermal Free Energies</td>
                                                <td><xsl:value-of select="$zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalfe']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalfe']/@units)"></xsl:value-of></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($l122Module)">
                                            <tr>
                                                <td>Counterpoise: corrected energy</td>
                                                <td><xsl:value-of select="$l122Module/scalar[@dictRef='g:counterpoiseEnergy']"/></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Counterpoise: BSSE energy</td>
                                                <td><xsl:value-of select="$l122Module/scalar[@dictRef='g:counterpoiseBSSE']"/></td>
                                                <td></td>
                                            </tr>                                                        
                                        </xsl:if>                                    
                                    </tbody>                                    
                                </table> 
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#energiesT-<xsl:value-of select="$uid"/>").dataTable({
                                            "aoColumnDefs" : [
                                                { "sClass": "text-right", "aTargets": [ 1 ] },
                                                { "sClass": "nowrap", "aTargets": [ 0 ] }
                                            ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                            
                                        });                                    
                                    });
                                    
                                </script>
                            </xsl:if>     
                            <br/>
                            <xsl:if test="exists($l804Module)">
                                <xsl:variable name="spinComponentT2" select="tokenize($l804Module/list[@cmlx:templateRef='spincomponents']/array[@dictRef='cc:T2'],'[\s]')"/>
                                <xsl:variable name="spinComponentE2" select="tokenize($l804Module/list[@cmlx:templateRef='spincomponents']/array[@dictRef='cc:E2'],'[\s]')"/>
                                <xsl:variable name="spinType" select="tokenize('alpha-alpha alpha-beta beta-beta','[\s]')"/>
                                <table class="display" id="energiesT-{generate-id($l804Module)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:for-each select="1 to count($spinComponentT2)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <tr>
                                                <td><xsl:value-of select="$spinType[$outerIndex]"/></td>
                                                <td>T2 = </td>
                                                <td><xsl:value-of select='$spinComponentT2[$outerIndex]'/></td>
                                                <td>E2 = </td>
                                                <td><xsl:value-of select="$spinComponentE2[$outerIndex]"/></td>
                                            </tr>                               
                                        </xsl:for-each>
                                        <tr>
                                            <td></td>
                                            <td>ANorm</td>
                                            <td><xsl:value-of select="$l804Module//scalar[@dictRef='cc:anorm']"/></td>
                                            <td></td>
                                            <td></td>
                                        </tr> 
                                        <tr>
                                            <td></td>
                                            <td>E2</td>
                                            <td><xsl:value-of select="$l804Module//scalar[@dictRef='cc:e2']"/></td>
                                            <td></td>                                            
                                            <td></td>
                                        </tr> 
                                        <tr>
                                            <td></td>
                                            <td>EUMP2</td>                                    
                                            <td><xsl:value-of select="$l804Module//scalar[@dictRef='cc:eump2']"/></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#energiesT-<xsl:value-of select="generate-id($l804Module)"/>").dataTable({                                    
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                            
                                        });                                    
                                    });
                                    
                                </script>
                            </xsl:if> 
                                                        
                            <xsl:variable name="archiveEnergies" select=".//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l9999.archive']//list[@dictRef='g:archive.namevalue']"/>                                                            
                            <xsl:if test="$archiveEnergies != ''">
                                <table class="display" id="energiesT-{generate-id($archiveEnergies)}">
                                    <thead>
                                        <tr>
                                            <th>Energy</th>
                                            <th>Value</th>
                                            <th>Units</th>                                   
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:for-each select="$archiveEnergies/scalar">
                                            <xsl:variable name="field" select="."/>
                                            <xsl:if test="matches($field,'.*=.*')">
                                                <xsl:variable name="splittedField" select="tokenize($field,'=')"/>
                                                <xsl:if test="gaussian:isMethod($splittedField[1])">
                                                    <tr>
                                                        <td><xsl:value-of select="$splittedField[1]"/></td>
                                                        <td class="right"><xsl:value-of select="$splittedField[2]"/></td>
                                                        <td><xsl:value-of select="helper:printUnitSymbol('nonsi:hartree')"/></td>
                                                    </tr>                                                   
                                                </xsl:if>
                                            </xsl:if>
                                        </xsl:for-each>    
                                    </tbody>
                                </table>    
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#energiesT-<xsl:value-of select="generate-id($archiveEnergies)"/>").dataTable({                                    
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [
                                                { "sClass": "text-right", "aTargets": [ 1 ] },
                                                { "sClass": "nowrap", "aTargets": [ 0 ] }
                                            ],
                                        });                                    
                                    });                                    
                                </script>
                            </xsl:if>
                        </div>
                    </div> 
                </div>
            </div>
        </div>         
    </xsl:template>
    
    <!-- Energies section (module) -->
    <xsl:template name="energiesFragmentedSection">
        <xsl:variable name="finalEnergy" select="
            if(exists(.//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])) then
                (.//module[@cmlx:templateRef='l502.footer']/list[@cmlx:templateRef='scfdone']//scalar[@dictRef='g:rbhflyp'])       
            else if(exists(.//module[@cmlx:templateRef='l508']/scalar[@dictRef='g:rbhflyp'])) then 
                (.//module[@cmlx:templateRef='l508']/scalar[@dictRef='g:rbhflyp'])[last()]
            else
                (.//scalar[@dictRef='g:rbhflyp'])[last()]"/>
        
        <xsl:variable name="l502Module" select="(.//module[@cmlx:templateRef='l502.pcm'])"/>
        <xsl:variable name="l120Module" select="(.//module[@cmlx:templateRef='l120'])"/>     
        <xsl:variable name="l122Module" select="(.//module[@cmlx:templateRef='l122'])"/>
        <xsl:variable name="zeropointProperty" select=".//property[@dictRef='cc:zeropoint']"/>                        
        <xsl:variable name="uid" select="concat(generate-id($finalEnergy[1]),generate-id($l502Module[1]),generate-id($l120Module[1]),generate-id($zeropointProperty[1]),generate-id($l122Module[1]))"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#energies-{$uid}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Energies                    
                </h4>
            </div>            
            <div id="energies-{$uid}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-8 col-md-8 col-sm-12">
                            <xsl:if test="exists($finalEnergy) or exists($l502Module) or exists($l120Module) or exists($zeropointProperty) or exists($l122Module)">
                                <table class="display" id="energiesT-{$uid}">
                                    <thead>
                                        <tr>
                                            <th>Energy</th>
                                            <th>Value</th>
                                            <th>Units</th>    
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <xsl:if test="exists($finalEnergy)">                                            
                                                <tr>                                             
                                                    <td>SCF Done:</td>
                                                    <td><xsl:for-each select="$finalEnergy"><xsl:value-of select="."/><br/></xsl:for-each></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($finalEnergy[1]/@units)"/></td>                                                                    
                                                </tr>                                                                                                                               
                                        </xsl:if>
                                        <xsl:if test="exists($l502Module//scalar[@dictRef='cc:dispenergy'])">                                            
                                                <tr>
                                                    <td>Dispersion energy:</td>
                                                    <td><xsl:for-each select="$l502Module"><xsl:value-of select=".//scalar[@dictRef='cc:dispenergy']"/><br/></xsl:for-each></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($l502Module[1]//scalar[@dictRef='cc:dispenergy']/@units)"></xsl:value-of></td>
                                                </tr>                                            
                                        </xsl:if>  
                                        <xsl:if test="exists($l120Module)">
                                            <xsl:for-each select="$l120Module">
                                                <xsl:variable name="method" select="tokenize(./array[@dictRef='cc:method'],'[\s]')"/>
                                                <xsl:variable name="system" select="tokenize(./array[@dictRef='cc:system'],'[\s]')"/>
                                                <xsl:variable name="energy" select="tokenize(./array[@dictRef='cc:energy'],'[\s]')"/>                            
                                                <xsl:for-each select="1 to count($method)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <tr>
                                                        <td><xsl:value-of select="concat($method[$outerIndex], ' ', $system[$outerIndex])"/></td>                                    
                                                        <td><xsl:value-of select="$energy[$outerIndex]"/></td>
                                                        <td>Eh</td>
                                                    </tr>
                                                </xsl:for-each>    
                                                <tr>
                                                    <td>Oniom : Extrapolated energy</td>
                                                    <td><xsl:value-of select="./scalar[@dictRef='cc:extraenergy']"/></td>
                                                    <td>Eh</td>
                                                </tr>                                                
                                            </xsl:for-each>
                                        </xsl:if>
                                        <xsl:if test="exists($zeropointProperty)">                                            
                                            <tr>
                                                <td>Zero-point correction</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/ scalar[@dictRef='cc:zpe.correction']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/ scalar[@dictRef='cc:zpe.correction']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Thermal correction to Energy</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrener']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrener']/@units)"></xsl:value-of></td>
                                            </tr> 
                                            <tr>
                                                <td>Thermal correction to Enthalpy</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrenthalpy']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrenthalpy']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Thermal correction to Gibbs Free Energy</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrgfe']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.thermalcorrgfe']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and zero-point Energies</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectzpe']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectzpe']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and thermal Energies</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermal']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermal']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and thermal Enthalpies</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalent']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalent']/@units)"></xsl:value-of></td>
                                            </tr>
                                            <tr>
                                                <td>Sum of electronic and thermal Free Energies</td>
                                                <td><xsl:for-each select="$zeropointProperty"><xsl:value-of select="./list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalfe']"/><br/></xsl:for-each></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropointProperty[1]/list[@id='l716.zeropoint']/scalar[@dictRef='cc:zpe.sumelectthermalfe']/@units)"></xsl:value-of></td>
                                            </tr>
                                            
                                        </xsl:if>
                                        <xsl:if test="exists($l122Module)">
                                            <tr>
                                                <td>Counterpoise: corrected energy</td>
                                                <td><xsl:for-each select="$l122Module"><xsl:value-of select="./scalar[@dictRef='g:counterpoiseEnergy']"/><br/></xsl:for-each></td>
                                                <td></td>
                                            </tr>
                                            <tr>
                                                <td>Counterpoise: BSSE energy</td>
                                                <td><xsl:for-each select="$l122Module"><xsl:value-of select="./scalar[@dictRef='g:counterpoiseBSSE']"/><br/></xsl:for-each></td>
                                                <td></td>
                                            </tr>                                                        
                                        </xsl:if>                                    
                                    </tbody>                                    
                                </table> 
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#energiesT-<xsl:value-of select="$uid"/>").dataTable({
                                        "aoColumnDefs" : [
                                            { "sClass": "text-right", "aTargets": [ 1 ] },
                                            { "sClass": "nowrap", "aTargets": [ 0 ] }],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                            
                                        });                                    
                                    });
                                    
                                </script>
                            </xsl:if>
                        </div>
                    </div> 
                </div>
            </div>
        </div>         
    </xsl:template>

    <!-- Spin section -->
    <xsl:template name ="spinSection">
        <xsl:variable name="mullikenSpin" select="(.//module[@cmlx:templateRef='l601.mullikenspin'])[last()]"/>
        <xsl:variable name="footer" select="(.//module[@cmlx:templateRef='l502.footer2'])[last()]"/>
        <xsl:if test="exists($mullikenSpin) or exists($footer)">
            <xsl:variable name="uid" select="concat(generate-id($mullikenSpin), generate-id($footer))"/> 
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#spinSection-{$uid}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Spin
                    </h4>
                </div>            
                <div id="spinSection-{$uid}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-md-8 col-sm-12">
                                <xsl:if test="exists($mullikenSpin)">
                                    <xsl:variable name="serial" select="tokenize($mullikenSpin/array[@dictRef='cc:serial'],'[\s]')"/>
                                    <xsl:variable name="elementtype" select="tokenize($mullikenSpin/array[@dictRef='cc:elementType'],'[\s]')"/>
                                    <xsl:variable name="charge" select="tokenize($mullikenSpin/array[@dictRef='x:charge'],'[\s]')"/>
                                    <h4>Mulliken atomic spin densities</h4>
                                    <table class="display" id="mullikenSpinT-{$uid}"></table>   
                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                        $("table#mullikenSpinT-<xsl:value-of select="$uid"/>").dataTable({
                                            "aaData" : [
                                                <xsl:for-each select="1 to count($serial)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                   [<xsl:value-of select="$serial[$outerIndex]"/>,'<xsl:value-of select="$elementtype[$outerIndex]"/>',<xsl:value-of select="format-number(number($charge[$outerIndex]),'#0.000000')"/>]<xsl:if test="$outerIndex &lt; count($serial)">,</xsl:if>                                            
                                                </xsl:for-each>                                            
                                            ],
                                            "aoColumns" : [
                                                {"sTitle" : "Atom"},
                                                {"sTitle" : ""},
                                                {"sTitle" : "Density"}
                                            ],
                                                "bFilter": false,
                                                "bPaginate": true,
                                                "bSort": false,
                                                "bInfo": true      
                                            });
                                        });
                                        
                                    </script>
                                </xsl:if>
                            </div>
                        </div>
                        <xsl:if test="exists($footer)">
                            <div class="row bottom-buffer">
                                <div class="col-md-6">
                                    <h4>S^2</h4>
                                    <span>S**2 before annihilation = </span><span><xsl:value-of select="$footer/scalar[@dictRef='cc:spincontamination']"/></span>
                                </div>                                            
                            </div>
                        </xsl:if>
                    </div>
                </div>
            </div>
        </xsl:if>    
    </xsl:template>
   
    <!-- Frequencies section -->
    <xsl:template name="frequencies">
        <xsl:variable name="vibrations" select="if(exists(.//module[@cmlx:templateRef='l716.freq.high.precision.chunkx' and @dictRef='cc:vibrations'])) then
                                                    .//module[@cmlx:templateRef='l716.freq.high.precision.chunkx' and @dictRef='cc:vibrations']
                                                else 
                                                    .//module[@cmlx:templateRef='l716.freq.chunkx' and @dictRef='cc:vibrations']
                                                 "/>
        <xsl:if test="exists($vibrations)">
            <xsl:variable name="vindex" select="count(preceding-sibling::*/descendant::module[@cmlx:templateRef='l716.freq.chunkx']/ancestor-or-self::module[@dictRef='cc:finalization']/molecule) + 1" />
            <xsl:variable name="uid" select="generate-id($vibrations)"/> 
            <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'[\s]')"/>
            
            <div id="vibrationPanel" class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies-{$uid}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        IR spectrum
                    </h4>
                </div>            
                <div id="frequencies-{$uid}" class="panel-collapse collapse frequencies">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">                            
                            <div class="col-lg-12" style="z-index:1">
                                <script type="text/javascript">
                                    $('div#frequencies-<xsl:value-of select="$uid" />').on('shown.bs.collapse', function () {
                                        clearVibrationApplets();
                                        collapseVibrationSections('<xsl:value-of select="$uid" />');                                        
                                        loadVibrationApplet('<xsl:value-of select="$uid"/>', <xsl:value-of select="$vindex"/>);
                                    });
                                </script>

                                <div id="frequencyComboboxDiv-{$uid}" class="col-lg-12 pl-0">
                                    Selected frequency: 
                                    <select onchange="changeVibration('{$uid}')">
                                        <option value="vibration off">.... select ....</option>
                                        <option value="model 1">Base</option>
                                        <xsl:for-each select="2 to count($frequency ) + 1">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <option value="model {$outerIndex}; vibration on;"><xsl:value-of select="$frequency[$outerIndex - 1]"/></option>
                                        </xsl:for-each>  
                                    </select>                                            
                                </div>
                                <div id="irSpectrumDiv-{$uid}" class="spectrum">
                                    <div id="jsvApplet-{$uid}" style="display:inline; float:left">                                                                         
                                    </div>
                                    <div id="jmolApplet-{$uid}" style="display:inline; float:left">
                                    </div>                                                                                                                               
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>  
    </xsl:template>

    <!-- ESP charges -->
    <xsl:template name="espcharges">
        <xsl:variable name="espcharges" select="(.//module[@cmlx:templateRef='l602.electrostatic']/module[@cmlx:templateRef='espcharges'])[last()]"/>
        <xsl:if test="exists($espcharges)">
            <xsl:variable name="serial" select="tokenize($espcharges/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="atomType" select="tokenize($espcharges/array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="charges" select="tokenize($espcharges/array[@dictRef='g:espcharge'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#charges-{generate-id($espcharges)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        ESP charges
                    </h4>
                </div>            
                <div id="charges-{generate-id($espcharges)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <table class="display" id="espchargesT-{generate-id($espcharges)}"></table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#espchargesT-<xsl:value-of select="generate-id($espcharges)"/>').dataTable( {
                                    "aaData": [                                                                                                                                                  
                                    <xsl:for-each select="1 to count($serial)">
                                        <xsl:variable name="outerIndex" select="."/>
                                        [<xsl:value-of select="$serial[$outerIndex]"/>,'<xsl:value-of select="$atomType[$outerIndex]"/>','<xsl:value-of select="$charges[$outerIndex]"/>']<xsl:if test="$outerIndex &lt; count($charges)">,</xsl:if>                                                        
                                    </xsl:for-each>      
                                    ],
                                    "aoColumns": [
                                    { "sTitle": "" },
                                    { "sTitle": "Atom"},
                                    { "sTitle": "Charge"}
                                    ],
                                    "aoColumnDefs" : [                                    
                                    { "sClass": "text-right", "aTargets": [ 2 ] }
                                    ],
                                    "bFilter": false,                                 
                                    "bPaginate": true,                                    
                                    "bSort": false,
                                    "bInfo": true
                                    } );   
                                    } );                                       
                                </script>                
                            </div>       
                        </div>
                    </div>
                </div>
            </div>        
        </xsl:if>
    </xsl:template>

    <!-- Mulliken charges  -->
    <xsl:template name="mullikenCharges">
        <xsl:variable name="mulliken" select="(.//module[@dictRef='cc:finalization']//module[@cmlx:templateRef='l601.mullik'][matches(descendant::scalar[@dictRef='g:title']/text(), 'Mulliken\s*(atomic)?\s*charges:')]/list[@cmlx:templateRef='row'])[last()]"/>        
        <xsl:if test="exists($mulliken)">
            <xsl:variable name="serial" select="tokenize($mulliken/array[@dictRef='cc:serial'], '\s+')"/>
            <xsl:variable name="atomType" select="tokenize($mulliken/array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="charges" select="tokenize($mulliken/array[@dictRef='x:charge'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#charges-{generate-id($mulliken)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Mulliken charges
                    </h4>
                </div>            
                <div id="charges-{generate-id($mulliken)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-lg-4 col-md-4 col-sm-6">
                                <table class="display" id="chargesT-{generate-id($mulliken)}"></table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                    $('table#chargesT-<xsl:value-of select="generate-id($mulliken)"/>').dataTable( {
                                    "aaData": [                                                                                                                                                  
                                    <xsl:for-each select="1 to count($serial)">
                                        <xsl:variable name="outerIndex" select="."/>
                                        [<xsl:value-of select="$serial[$outerIndex]"/>,'<xsl:value-of select="$atomType[$outerIndex]"/>','<xsl:value-of select="$charges[$outerIndex]"/>']<xsl:if test="$outerIndex &lt; count($charges)">,</xsl:if>                                                        
                                    </xsl:for-each>      
                                    ],
                                    "aoColumns": [
                                    { "sTitle": "" },
                                    { "sTitle": "Atom"},
                                    { "sTitle": "Charge"}
                                    ],
                                    "aoColumnDefs" : [                                    
                                    { "sClass": "text-right", "aTargets": [ 2 ] }
                                    ],
                                    "bFilter": false,                                 
                                    "bPaginate": true,                                    
                                    "bSort": false,
                                    "bInfo": true
                                    } );   
                                    } );                                       
                                </script>                
                            </div>       
                        </div>
                    </div>
                </div>
            </div>        
        </xsl:if>
    </xsl:template>
   
    <!-- Dipole moment -->
    <xsl:template name="dipoleMoment">
        <xsl:variable name="multipole" select="(.//list[@cmlx:templateRef='multipole'])[last()]"/>
        <xsl:if test="exists($multipole)">            
            <xsl:variable name="dipole"     select="tokenize($multipole/array[@dictRef='cc:dipole'],'\s+')"/>
            <xsl:variable name="quadrupole" select="tokenize(concat(($multipole/array[@dictRef='cc:quadrupole' and @size='3'])[1],' ',($multipole/array[@dictRef='cc:quadrupole' and @size='3'])[2]),'\s+')"/>
            <xsl:variable name="dipoleid" select="generate-id($multipole/array[@dictRef='cc:dipole'])"/>
            <xsl:variable name="quadrupoleid" select="generate-id(($multipole/array[@dictRef='cc:quadrupole'])[1])"/>
            <xsl:variable name="uid" select="concat($dipoleid,$quadrupoleid)"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#multipole-{$uid}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Dipole moment (Debye)
                    </h4>
                </div>            
                <div id="multipole-{$uid}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-md-8 col-sm-12">                               
                                <h4>Dipole moment</h4>
                                <table class="display" id="dipoleT-{$dipoleid}">
                                    <thead>
                                        <tr>
                                            <th>X</th>
                                            <th>Y</th>
                                            <th>Z</th>
                                            <th>Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><xsl:value-of select="$dipole[1]"/></td>
                                            <td><xsl:value-of select="$dipole[2]"/></td>
                                            <td><xsl:value-of select="$dipole[3]"/></td>
                                            <td><xsl:value-of select="$multipole//scalar[@dictRef='x:dipole']"/></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-6 col-md-6 col-sm-12">                        
                                <h4>Quadrupole moment</h4>
                                <table class="display" id="quadrupoleT-{$quadrupoleid}">
                                    <thead>
                                        <tr>
                                            <th>XX</th>
                                            <th>YY</th>
                                            <th>ZZ</th>
                                            <th>XY</th>
                                            <th>XZ</th>
                                            <th>YZ</th>                                            
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><xsl:value-of select="$quadrupole[1]"/></td>
                                            <td><xsl:value-of select="$quadrupole[2]"/></td>
                                            <td><xsl:value-of select="$quadrupole[3]"/></td>
                                            <td><xsl:value-of select="$quadrupole[4]"/></td>
                                            <td><xsl:value-of select="$quadrupole[5]"/></td>
                                            <td><xsl:value-of select="$quadrupole[6]"/></td>                                            
                                        </tr>
                                    </tbody>                                
                                </table>
                                
                                <script type="text/javascript">
                                     $(document).ready(function(){
                                        $("table#dipoleT-<xsl:value-of select="$dipoleid"/>").dataTable({
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false
                                    });
                                        $("table#quadrupoleT-<xsl:value-of select="$quadrupoleid"/>").dataTable({
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false
                                    });                                        
                                     });
                                    
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>            
            
    <!-- TDDFT excitation energies -->
    <xsl:template name="excitationEnergies">
        <xsl:variable name="excitations" select=".//module[@cmlx:templateRef='l914']/module[@cmlx:templateRef='l914_excitations']"/>        
        <xsl:if test="exists($excitations)">
            <xsl:for-each select="$excitations">
                <xsl:variable name="excitation" select="."/>
                <xsl:variable name="states" select="$excitation/module[@cmlx:templateRef='l914_excitations1']" />
                
                <div class="panel panel-default">
                    <div class="panel-heading" data-toggle="collapse" data-target="div#tddft-excitations-{generate-id($excitation)}" style="cursor: pointer; cursor: hand;">
                        <h4 class="panel-title">
                            Final Excitation Energies 
                        </h4>
                    </div>            
                    <div id="tddft-excitations-{generate-id($excitation)}" class="panel-collapse collapse">
                        <div class="panel-body">    
                            <div class="row bottom-buffer">
                                <div class="col-md-6 col-md-8 col-sm-12">
                                    <table class="display excitations" id="excitationsT-{generate-id($excitation)}">
                                        <thead>
                                            <tr>   
                                                <th>no.</th>
                                                <th>Type</th>
                                                <th>Energy (eV)</th>
                                                <th>Wavelength (nm)</th>
                                                <th>Osc.</th>
                                                <th>s^2</th>
                                                <th>Orbitals</th>
                                            </tr>
                                        </thead>                                                                        
                                    </table>
                                    <script type="text/javascript">
                                        var excitationsTable<xsl:value-of select="generate-id($excitation)"/>;
                                        
                                        $(document).ready(function() {                        
                                            excitationsTable<xsl:value-of select="generate-id($excitation)"/> = $('table#excitationsT-<xsl:value-of select="generate-id($excitation)"/>').dataTable( {
                                                "aaData": [                                                                                 
                                                    <xsl:for-each select="$states">                                                
                                                        <xsl:variable name="excitation" select="./list[@cmlx:templateRef='l914_excit1']/list" />
                                                        <xsl:variable name="orbitals" select="./list[@cmlx:templateRef='l914_excit2']/list" />
                                                        <xsl:variable name="orbitallines">[<xsl:for-each select="$orbitals"><xsl:sort select="child::scalar[@dictRef='g:tddft_unknown']" data-type="number" order="descending"/>["<xsl:value-of select="./scalar[@dictRef='g:tddft_orbitalstart']"/>","<xsl:value-of select="./scalar[@dictRef='g:tddft_orbitalend']"/>","<xsl:value-of select="./scalar[@dictRef='g:tddft_unknown']"/>"],</xsl:for-each>]</xsl:variable>
                                                        [<xsl:value-of select="$excitation/scalar[@dictRef='g:tddft_enumber']"/>,'<xsl:value-of select="gaussian:escapeQuotes($excitation/scalar[@dictRef='g:tddft_ttype'])"/>','<xsl:value-of select="$excitation/scalar[@dictRef='g:tddft_eenergy']"/>', '<xsl:value-of select="$excitation/scalar[@dictRef='g:tddft_wavelength']"/>', '<xsl:value-of select="$excitation/scalar[@dictRef='g:tddft_oscillator_strength']"/>', '<xsl:value-of select="$excitation/scalar[@dictRef='g:tddft_s2']"/>',<xsl:choose><xsl:when test="not(exists($orbitals))">'',''</xsl:when><xsl:otherwise>'<span class="fa fa-plus"/>',<xsl:value-of select="$orbitallines"/></xsl:otherwise></xsl:choose>],
                                                    </xsl:for-each>      
                                                ],
                                                "aoColumns": [
                                                    { "sTitle": "no."},
                                                    { "sTitle": "Type", "sClass": "excitation-type-col"},
                                                    { "sTitle": "Energy (eV)"},
                                                    { "sTitle": "Wavelength (nm)"},
                                                    { "sTitle": "Osc."},
                                                    { "sTitle": "s^2"},
                                                    { "sTitle": "Configurations"},
                                                    { "sTitle": "Configurations"},
                                                ],
                                                "aoColumnDefs" : [                                            
                                                    { "bVisible": false, "aTargets": [ 7 ]  }
                                                ],
                                                "bFilter": false,                                 
                                                "bPaginate": true,                                    
                                                "bSort": false,
                                                "bInfo": true
                                            } );  
                                        
                                            $('#excitationsT-<xsl:value-of select="generate-id($excitation)"/> tbody tr td span.fa-plus , #excitationsT-<xsl:value-of select="generate-id($excitation)"/> tbody tr td span.fa-minus').on( 'click', function () {
                                                var nTr = $(this).parents('tr')[0];
                                                if ( excitationsTable<xsl:value-of select="generate-id($excitation)"/>.fnIsOpen(nTr) ) {
                                                    /* This row is already open - close it */
                                                    $(this).toggleClass("fa-plus fa-minus");
                                                    excitationsTable<xsl:value-of select="generate-id($excitation)"/>.fnClose( nTr );
                                                }
                                                else {
                                                    /* Open this row */
                                                    $(this).toggleClass("fa-plus fa-minus");
                                                    excitationsTable<xsl:value-of select="generate-id($excitation)"/>.fnOpen( nTr, excitationOrbital(nTr), 'details' );
                                                }
                                            }); 
                                            
                                            $(excitationsTable<xsl:value-of select="generate-id($excitation)"/>).removeClass("compact");
                                        }); 
                                        
                                        function excitationOrbital( nTr ) {
                                            var aData = excitationsTable<xsl:value-of select="generate-id($excitation)"/>.fnGetData( nTr );                                
                                            var sOut = '';
                                            
                                            if(aData[7].length > 0){
                                            sOut += '<table class="excitation-orbitals" cellpadding="5" cellspacing="0" border="0" style="padding-left:50px;">';
                                                sOut += '<thead><tr><th>Occupied</th><th>Virtual</th><th></th></tr></thead>';
                                                for(var i=0; i&lt; aData[7].length; i++) {
                                                sOut += '<tr><td>' + aData[7][i][0] + '</td><td>' + aData[7][i][1] + '</td><td class="right">' + aData[7][i][2] + '</td></tr>';    
                                                }
                                                sOut += '</table>';                                         
                                            }
                                            return sOut;
                                        }
                                    </script>
                                    <style>
                                        table.excitations tr.even {
                                        background-color: #f9f9f9;
                                        }    
                                        
                                        table.excitation-orbitals {
                                        margin: 5px;
                                        }
                                        
                                        table.excitation-orbitals tr th {
                                        min-width: 100px;
                                        }
                                        
                                        .excitation-type-col {
                                        min-width: 75px;
                                        }
                                    </style>                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </xsl:for-each>
        </xsl:if>
    </xsl:template>
      
    <!-- Electronic Coupling for Excitation Energy Transfer -->
    <xsl:template name="eCoupling">
        <xsl:variable name="ecoupling" select=".//module[@cmlx:templateRef='l925']/module[@cmlx:templateRef='ecoupling']"/>
        <xsl:if test="exists($ecoupling)">
            <xsl:variable name="fragA"  select="tokenize($ecoupling/array[@dictRef='g:fragmentA'],'\s+')"/>
            <xsl:variable name="stateA"  select="tokenize($ecoupling/array[@dictRef='g:stateA'],'\s+')"/>
            <xsl:variable name="wA"  select="tokenize($ecoupling/array[@dictRef='g:wA'],'\s+')"/>
            <xsl:variable name="fragB"  select="tokenize($ecoupling/array[@dictRef='g:fragmentB'],'\s+')"/>
            <xsl:variable name="stateB"  select="tokenize($ecoupling/array[@dictRef='g:stateA'],'\s+')"/>
            <xsl:variable name="wB"  select="tokenize($ecoupling/array[@dictRef='g:wB'],'\s+')"/>
            <xsl:variable name="coupling"  select="tokenize($ecoupling/array[@dictRef='g:coupling'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#electronic-coupling-{generate-id($ecoupling)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Electronic coupling for Excitation Energy Transfer
                    </h4>
                </div>
                <div id="electronic-coupling-{generate-id($ecoupling)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-md-8 col-sm-12">
                                <table class="display" id="electronic-couplingT-{generate-id($ecoupling)}">
                                    <thead>
                                        <tr>
                                            <th>Fragment</th>
                                            <th>State</th>
                                            <th>w (eV)</th>
                                            <th>Fragment</th>
                                            <th>State</th>
                                            <th>w (eV)</th>
                                            <th>Total coupling (eV)</th>
                                        </tr>
                                    </thead>                                                                        
                                </table>
                                <script type="text/javascript">
                                    var coupling<xsl:value-of select="generate-id($ecoupling)"/>;
                                    
                                    $(document).ready(function() {                        
                                        couplingTable<xsl:value-of select="generate-id($ecoupling)"/> = $('table#electronic-couplingT-<xsl:value-of select="generate-id($ecoupling)"/>').dataTable({
                                            "aaData": [                                                                                 
                                            <xsl:for-each select="1 to count($fragA)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                [<xsl:value-of select="$fragA[$outerIndex]"/>, <xsl:value-of select="$stateA[$outerIndex]"/>, '<xsl:value-of select="$wA[$outerIndex]"/>', <xsl:value-of select="$fragB[$outerIndex]"/>, <xsl:value-of select="$stateB[$outerIndex]"/>, '<xsl:value-of select="$wB[$outerIndex]"/>', <xsl:value-of select="$coupling[$outerIndex]"/>]<xsl:if test="$outerIndex != count($coupling)">,</xsl:if>
                                            </xsl:for-each>      
                                            ],
                                            "bFilter": false,                                 
                                            "bPaginate": true,                                    
                                            "bSort": false,
                                            "bInfo": true,
                                            "aoColumnDefs" : [                                    
                                                { "sClass": "text-right", "aTargets": [ 6 ] }
                                            ],
                                        });
                                        $('#electronic-coupling-<xsl:value-of select="generate-id($ecoupling)"/>.panel-collapse').on('shown.bs.collapse', function () {                            
                                            $("#electronic-couplingT-<xsl:value-of select="generate-id($ecoupling)"/>").DataTable().page('last').draw('page');                               
                                        });                                       
                                    });                                    
                                </script>
                                
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
        </xsl:if>
    </xsl:template>  
      
    <xsl:function name="gaussian:escapeQuotes">
        <xsl:param name="value"/>
        <xsl:value-of select="replace(replace($value,$quote,$quote_escaped),$singlequote,$singlequote_escaped)"/>
    </xsl:function>
      
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>
    
   
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
            
</xsl:stylesheet>
