<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:orca="https://orcaforum.cec.mpg.de/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
  
    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk orca helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Nov 1, 2012</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institut Català d'Investigació Química</xd:p>
        </xd:desc>       
    </xd:doc>
    <xsl:include href="helper/chemistry/orca.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>
    
    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>    
    <xsl:param name="browseurl"/>
    <xsl:param name="jcampdxurl"/>
    <xsl:param name="moldenurl"/>

    <!-- Build calculation types variable, one per job  -->    
    <xsl:variable name="calcTypes">
        <xsl:for-each select="(//cml:module[@id='initialization'])[1]//cml:module[@cmlx:templateRef='job']">
            <xsl:element name="cml:calcType">
                <xsl:variable name="commands" select="."/>
                <xsl:variable name="position" select="position()" />

                <xsl:variable name="scfsettings" select="(//cml:module[@id='initialization'])[$position]//cml:module[@cmlx:templateRef='scfsettings'][1]" />
                <xsl:variable name="basissets" select="orca:getBasis($commands)"/>
                <xsl:variable name="functionals" select="orca:getFunctionals($commands, $scfsettings)"/>
                <xsl:variable name="isTddft" select="orca:isTddft($commands)" as="xs:boolean"/>
                <xsl:variable name="isTS" select="orca:isTS($commands)" as="xs:boolean"/>
                <xsl:variable name="isRestricted" select="exists((//cml:module[@id='initialization'])[$position]//cml:module[@cmlx:templateRef='optsetup'])"/>                
                <xsl:variable name="methods">
                    <xsl:variable name="currentMethods" select="orca:getMethods($commands, $isTddft)"/>
                    <xsl:choose>
                        <xsl:when test="exists($functionals)"><!-- If there is functional we are dealing with DFT -->                            
                            <xsl:if test="$isTddft">TD</xsl:if><xsl:text>DFT</xsl:text>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="$currentMethods"/>
                        </xsl:otherwise>
                    </xsl:choose>
                </xsl:variable>   
                
                <!-- Get calculation type -->                
                <xsl:variable name="isOptimization" select="orca:isOptimization($commands)"/>
                <xsl:variable name="isBrokenSymm" select="orca:isBrokenSymm($commands)"/>

                <xsl:variable name="hasVibrations" select="exists(//cml:module[@dictRef='cc:job'][$position]//cml:property[@dictRef='cc:frequencies'])"/>                            
                <xsl:variable name="negativeFrequenciesCount" select="orca:countNegativeFrequencies((//cml:module[@dictRef='cc:job'][$position]//cml:property[@dictRef='cc:frequencies'])[last()]//cml:array[@dictRef='cc:frequency']/text())"/>

                <xsl:variable name="calcTmp" select="orca:getCalcType($isOptimization, $isBrokenSymm, $hasVibrations, $negativeFrequenciesCount, $isTS)"/>
                <xsl:variable name="calcExtras">
                    <xsl:if test="$isRestricted">
                        <xsl:text> Restricted</xsl:text>
                        
                    </xsl:if>                    
                    <xsl:if test="contains($calcTmp, $orca:GeometryOptimization) and $isTddft and (orca:getTddftIRoot($commands) != '')">
                        <xsl:text>Excited state</xsl:text>(<xsl:value-of select="orca:getTddftIRoot($commands)"/>)
                    </xsl:if>
                     
                </xsl:variable>                    

                <xsl:variable name="methodExtras">
                    <xsl:variable name="params" select="orca:readExtraParameters($commands)"/>
                    <xsl:if test="exists($params)">
                        <xsl:value-of select="$params"/>
                    </xsl:if>                    
                </xsl:variable>

                <xsl:variable name="calcType" select="concat($calcTmp, ' ', $calcExtras)"/>

                <!-- Build element -->
                <xsl:attribute name="isTDDFT" select="$isTddft"/> 
                <xsl:attribute name="extras" select="orca:getMethodExtras($commands)"/>                                           
                <xsl:attribute name="isOptimization" select="$isOptimization"/>
                <xsl:attribute name="isBrokenSymm" select="$isBrokenSymm"/>
                <xsl:attribute name="hasVibrations" select="$hasVibrations"/>
                <xsl:attribute name="negativeFrequenciesCount" select="$negativeFrequenciesCount"/>
                <xsl:attribute name="methods" select="concat($methods, $methodExtras) " />
                <xsl:attribute name="isCalculationLevel" select="boolean($basissets/@isCalculationLevel)"/>
                <xsl:attribute name="basis" select="
                    if(boolean($basissets/@isCalculationLevel) and compare($basissets/@definedBasis, ' ') = 0) then
                        $basissets/@calculationLevelBasis
                    else
                        $basissets/@definedBasis
                   "/>
                <xsl:attribute name="functionals" select="$functionals"/>
                <xsl:attribute name="dlpno" select="orca:isDlpnoMethod($commands)" />
                <xsl:value-of select="$calcType"/>
            </xsl:element>
        </xsl:for-each>   
    </xsl:variable>

    <xsl:variable name="hasVibrations" select="exists(//property[@dictRef='cc:frequencies'])" />
    <xsl:variable name="hasOrbitals" select="exists($moldenurl) and ($moldenurl != '')" />
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    <xsl:variable name="subversionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programSubversion']"/>

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-ui.min.js"/>                
                <xsl:if test="$hasVibrations or $hasOrbitals">
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolMenu.js"/>                          
                    <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/js/JSmolJSV.js"/>
                </xsl:if>                      
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />                               
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/jquery-ui.min.css" type="text/css" />

                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <div id="container">
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">
                            <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                                <xsl:variable name="jobNumber" select="position()"/>
                                <div id="module-{generate-id(.)}">                                
                                    <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                                        <xsl:call-template name="atomicCoordinates">
                                            <xsl:with-param name="molecule" select="./cml:module[@dictRef='cc:finalization']/cml:molecule"/>
                                            <xsl:with-param name="inputMolecule" select="(.//cml:module[@cmlx:templateRef='input']/cml:module[@cmlx:templateRef='job'])[$jobNumber]/cml:molecule[@id='initial']"/>
                                            <xsl:with-param name="contraction" select="(./cml:module[@id='initialization']/cml:parameterList/cml:module[@cmlx:templateRef='basis'])[1]"/>
                                            <xsl:with-param name="calcType" select="$calcTypes/cml:calcType[$jobNumber]"/>
                                            <xsl:with-param name="basisecp" select="(./cml:module[@id='initialization']/cml:parameterList/cml:module[@cmlx:templateRef='basisecp'])[1]"/>
                                            <xsl:with-param name="userdefbasis" select="(.//cml:module[@cmlx:templateRef='input']/cml:module[@cmlx:templateRef='job'])[$jobNumber]/cml:module[@cmlx:templateRef='basis']"/>
                                        </xsl:call-template>
                                        <xsl:call-template name="molecularInfo" />                                                                                                 
                                        <xsl:call-template name="scfenergy">
                                            <xsl:with-param name="calcType" select="$calcTypes/cml:calcType[$jobNumber]" />
                                        </xsl:call-template>                                        
                                        <xsl:call-template name="vibrationalFrequencies" />                                                                           
                                        <xsl:call-template name="populationAnalysis" />
                                        <xsl:call-template name="electrostaticMoments" />                                    
                                        
                                        <xsl:call-template name="brokensym" />
                                        <xsl:call-template name="orbitals" />
                                        <xsl:call-template name="natural" />
                                        <xsl:call-template name="nmr" />
                                        <xsl:call-template name="tddft">
                                            <xsl:with-param name="multiplicity" select="(.//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Mult']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
                                        </xsl:call-template>
                                        <xsl:call-template name="epr" />
                                        <xsl:call-template name="finalresults">
                                            <xsl:with-param name="isBrokenSymm" select="$calcTypes/cml:calcType[$jobNumber]/@isBrokenSymm"/>
                                            <xsl:with-param name="calcType" select="$calcTypes/cml:calcType[$jobNumber]"/>
                                        </xsl:call-template>
                                        <xsl:call-template name="wavefunctionspecs" />
                                        <xsl:call-template name="ciExpansion" />
                                        <xsl:call-template name="waveFunctionTopWeightsValues"/>
                                        <xsl:call-template name="orbitalSpecs"/>
                                        <xsl:call-template name="finalCASPT2Energies"/>
                                </div>
                            </xsl:for-each>
                            <xsl:call-template name="printLicense"/>
                        </div>
                    </div>                    


                    <!-- If there are molden orbital files, load its viewer -->
                    <xsl:if test="$hasOrbitals">                                                
                        <xsl:copy-of select="helper:appendMoldenViewerCode($moldenurl, $webrootpath)"/>                        
                    </xsl:if>                                        
                    
                    <script type="text/javascript">

                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }                           
                        
                        $(document).ready(function() {
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                            
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }
                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                        $(this).find('.dataTable').each(function(index){
                            var oTable = $(this).dataTable();
                            $(oTable).css({ width: $(oTable).parent().width() });
                            oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                    </script>
                </div>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                    <xsl:text> - </xsl:text>
                    <xsl:value-of select="$subversionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>
            <xsl:for-each select="$calcTypes/cml:calcType">
                <tr>               
                    <td>Calculation type:</td>
                    <td>                                                                 
                        <xsl:value-of select="./text()"/>                        
                    </td>                    
                </tr>
                <tr>
                    <td>Method:</td>
                    <td>
                        <xsl:value-of select="./@methods"/>
                        <xsl:variable name="extraFields" select="orca:getExtraMethodFields(./@methods , .)"/>
                        <xsl:if test="exists($extraFields) and not(matches($extraFields,'^\s+$'))">                            
                            <xsl:text> ( </xsl:text>                            
                            <xsl:value-of select="$extraFields"/>                            
                            <xsl:text> )</xsl:text>
                        </xsl:if>                         
                    </td>                    
                </tr>
            </xsl:for-each>            
        </table>
    </xsl:template>

    <xsl:function name="orca:getExtraMethodFields">
        <xsl:param name="methods"></xsl:param>
        <xsl:param name="functional"/>
        <xsl:variable name="result">
            <xsl:for-each select="$functional/@functionals">
                <xsl:value-of select="string(.)"/><xsl:text> </xsl:text>
            </xsl:for-each>
            <xsl:if test="xs:boolean($functional/@isTDDFT) and not(matches(upper-case($methods),'(TDDFT|TDHF)'))"><xsl:text> TDDFT </xsl:text></xsl:if>
            <xsl:for-each select="$functional/@extras">
                <xsl:value-of select="string(.)"/><xsl:text> </xsl:text>
            </xsl:for-each>        
        </xsl:variable>
        <xsl:value-of select="$result"/>
    </xsl:function>

    <!-- Molecular info -->
    <xsl:template name="molecularInfo">
        <xsl:variable name="multiplicity" select="(.//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Mult']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
        <xsl:variable name="charge" select="(.//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Charge']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
        
        <div class="row bottom-buffer">
            <div class="col-md-12 col-sm-12">
                <h4>MOLECULAR INFO</h4>
                <table class="display" id="molecularInfo-{generate-id(.)}">
                    <thead>
                        <tr>
                            <th> </th>
                            <th> </th>
                        </tr>
                    </thead>    
                    <tbody>
                        <tr>
                            <td>Multiplicity</td>
                            <td><xsl:value-of select="$multiplicity"/></td>
                        </tr>
                        <tr>
                            <td>Charge</td>
                            <td><xsl:value-of select="$charge"/></td>
                        </tr>                
                    </tbody>
                </table>  
            </div>
        </div>
        <xsl:call-template name="atomicDistances"/>
        <xsl:call-template name="solventSection"/>
        <xsl:call-template name="restrictions"/>
    </xsl:template>
    
    <xsl:template name="atomicCoordinates">
        <xsl:param name="molecule"/>
        <xsl:param name="inputMolecule"/>
        <xsl:param name="contraction"/>
        <xsl:param name="calcType"/>     
        <xsl:param name="basisecp" />
        <xsl:param name="userdefbasis"/>
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinates-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]                           
                </h4>
            </div>
            <div id="atomicCoordinates-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-lg-8 col-md-8 col-sm-12">
                            <div id="atomicCoordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>                                
                                    <tr>
                                        <td><xsl:value-of select="$title"/></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                        <xsl:variable name="outerIndex" select="position()"/>
                                        <xsl:variable name="elementType" select="@elementType"/>                                                                                                       
                                        <xsl:variable name="id" select="@id"/>
                                        <tr>
                                            <td><xsl:value-of select="$elementType"/></td>
                                            <td><xsl:value-of select="format-number(@x3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@y3,'#0.000000')"/></td>
                                            <td><xsl:value-of select="format-number(@z3,'#0.000000')"/></td>
                                        </tr>
                                    </xsl:for-each>                                       
                                </tbody>                            
                            </table>                              
                            <script type="text/javascript">                                                                  
                                $(document).ready(function() {
                                 $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false
                                 });
                                 $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                });
                                function getXYZ(){
                                 var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                 new $.fn.dataTable.Buttons( table, {
                                    buttons: [ {
                                    extend: 'copyHtml5',
                                    text: 'XYZ'
                                    }
                                    ]
                                 });
                                 table.buttons().container().appendTo($('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));                                    
                                 $('div#atomicCoordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();                                
                                }     
                            </script>  
                            
                            <table class="display" id="atomicCoordinatesT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th rowspan="2">Atom</th>
                                        <th rowspan="2"></th>
                                        <th rowspan="2" style="text-align: center">x</th>
                                        <th rowspan="2" style="text-align: center">y</th>
                                        <th rowspan="2" style="text-align: center">z</th>
                                        <th colspan="2" style="text-align: center">BASIS SET</th>                                        
                                    </tr>
                                    <tr>
                                        <th>TYPE</th>
                                        <th>(Primitive) / [Contracted]</th>
                                    </tr>
                                </thead>
                            </table>
                            
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#atomicCoordinatesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable( {
                                "aaData": [
                                <xsl:for-each select="$molecule/cml:atomArray/cml:atom">                                                        
                                    <xsl:variable name="outerIndex" select="position()"/>
                                    <xsl:variable name="elementType" select="@elementType"/>
                                    <xsl:variable name="elementBasis">
                                        <xsl:choose>
                                            <xsl:when test="exists(($inputMolecule/cml:atomArray/cml:atom)[$outerIndex]/cml:scalar[@dictRef='cc:basis']/text()) and compare(($inputMolecule/cml:atomArray/cml:atom)[$outerIndex]/cml:scalar[@dictRef='cc:basis']/text(),'N/A') !=0 ">
                                                <xsl:value-of select="($inputMolecule/cml:atomArray/cml:atom)[$outerIndex]/cml:scalar[@dictRef='cc:basis']/text()"/>        
                                            </xsl:when>
                                            <xsl:otherwise>
                                                <xsl:choose>
                                                    <xsl:when test="boolean($calcType/@isCalculationLevel) and starts-with($calcType/@basis,'DefBas')">
                                                        <xsl:value-of select="orca:getDefaultBasisForAtomType($elementType, $calcType/@basis)"/>                                                        
                                                    </xsl:when>
                                                    <xsl:when test="exists($userdefbasis[descendant::cml:array[matches(upper-case(text()), concat('.*NEWGTO\s*', upper-case($elementType),'\s*.*')) ]])">
                                                        <xsl:variable name="basis" select="$userdefbasis//descendant::cml:array[matches(upper-case(text()), concat('.*NEWGTO\s*', upper-case($elementType),'\s.*'))]"/>                                                        
                                                        <xsl:value-of select="(tokenize($basis/text(),'&quot;'))[2]"/>
                                                    </xsl:when>                                                       
                                                    <xsl:otherwise>
                                                        <xsl:value-of select="(tokenize($calcType/@basis, '[\s+]'))[1]"/> <!-- Display main basis, not secondary -->                                                        
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:otherwise>                                            
                                        </xsl:choose>
                                        <xsl:if test="exists($basisecp)">
                                            <xsl:text> </xsl:text><xsl:value-of select="orca:getBasisECP($basisecp, $outerIndex -1)"/>
                                        </xsl:if>
                                        
                                        
                                    </xsl:variable>                            
                                    <xsl:variable name="id" select="@id"/>
                                    [<xsl:value-of select="position() - 1"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.000000')"/>","<xsl:value-of select="format-number(@y3,'#0.000000')"/>","<xsl:value-of select="format-number(@z3,'#0.000000')"/>","<xsl:value-of select="$elementBasis"/>","(<xsl:value-of select="orca:getPrimitive($contraction,$outerIndex)"/>) / [<xsl:value-of select="orca:getContraction($contraction,$outerIndex)"/>]"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                </xsl:for-each>    
                                ],
                                "aoColumns": [
                                null,
                                null,
                                { "sClass": "right" },
                                { "sClass": "right" },
                                { "sClass": "right" },
                                null,
                                null                           
                                ],   
                                "bFilter": false,
                                "bPaginate": false,
                                "bSort": false,
                                "bInfo": false
                                } );   
                                } );                                       
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>

    <xsl:template name="solventSection">
        <xsl:variable name="solvation" select=".//cml:module[@cmlx:templateRef='cosmo' or @cmlx:templateRef='cpcm']" />
        <xsl:if test="exists($solvation)">
            <xsl:variable name="technical" select="$solvation/cml:list[@cmlx:templateRef='technical']"/>
            <xsl:variable name="radii" select="$solvation/cml:list[@cmlx:templateRef='radii']"/>            
            <xsl:variable name="parameters" select="$solvation/cml:list[@cmlx:templateRef='parameters']"/>            
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#solventSectionCollapse{generate-id($solvation)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Solvation input 
                    </h4>
                </div>
                <div id="solventSectionCollapse{generate-id($solvation)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-6 col-md-6 col-sm-12">
                                <xsl:variable name="cosmoener" select="((.//cml:module[@cmlx:templateRef='totalenergy'])[last()])/cml:scalar[@dictRef='cc:cosmoener']"/>                                
                                <xsl:variable name="cpcmener" select="((.//cml:module[@cmlx:templateRef='totalenergy'])[last()])/cml:scalar[@dictRef='o:cpcmener']"/>
                                <table id="solvent-{generate-id($solvation)}" class="display">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:if test="exists($cosmoener)">
                                            <tr>
                                                <td>COSMO(ediel)</td>
                                                <td><xsl:value-of select="$cosmoener"/> <xsl:value-of select="helper:printUnitSymbol($cosmoener/@units)"/> </td>
                                            </tr>                                            
                                        </xsl:if>
                                        <xsl:if test="exists($cpcmener)">
                                            <tr>
                                                <td>CPCM Dielectric</td>
                                                <td><xsl:value-of select="$cpcmener"/> <xsl:value-of select="helper:printUnitSymbol($cpcmener/@units)"/> </td>
                                            </tr>                                            
                                        </xsl:if>
                                        <xsl:if test="exists($parameters) or exists($technical)">
                                            <tr>
                                                <td><h4>Parameters:</h4></td>
                                                <td></td>
                                            </tr>                                       
                                            <!-- Used for COSMO -->
                                            <xsl:for-each select="$parameters/cml:list">
                                                <xsl:if test=".[child::cml:scalar[@dictRef='cc:parameter' and matches(text(),'(Epsilon|Refractive Index)')]]">
                                                    <tr>
                                                        <td><xsl:value-of select="./cml:scalar[@dictRef='cc:parameter']"/></td>
                                                        <td><xsl:value-of select="./cml:scalar[@dictRef='cc:value']"/></td>
                                                    </tr>    
                                                </xsl:if>                                                
                                            </xsl:for-each>
                                            <!-- Used for CPCM -->
                                            <xsl:for-each select="$technical/cml:list">
                                                <xsl:if test=".[child::cml:scalar[@dictRef='cc:parameter' and matches(text(),'(Epsilon|Refrac)')]]">
                                                    <tr>
                                                        <td><xsl:value-of select="./cml:scalar[@dictRef='cc:parameter']"/></td>
                                                        <td><xsl:value-of select="./cml:scalar[@dictRef='cc:value']"/></td>
                                                    </tr>    
                                                </xsl:if>                                                
                                            </xsl:for-each>
                                        </xsl:if>
                                        <xsl:if test="exists($radii)">
                                            <tr>
                                                <td><h4>Radii (<xsl:value-of select="helper:printUnitSymbol($radii//cml:array[@dictRef='o:radius']/@units)"/>):</h4></td>
                                                <td></td>
                                            </tr>
                                            <xsl:variable name="elementType" select="tokenize($radii/cml:array[@dictRef='cc:elementType'],'\s+')"/>
                                            <xsl:variable name="radius" select="tokenize($radii/cml:array[@dictRef='o:radius'],'\s+')"/>                                            
                                            <xsl:for-each select="1 to count($elementType)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                <tr>
                                                    <td><xsl:value-of select="$elementType[$outerIndex]"/></td>
                                                    <td><xsl:value-of select="$radius[$outerIndex]"/></td>
                                                </tr>
                                            </xsl:for-each>
                                        </xsl:if>                                                                                
                                    </tbody>    
                                </table>
                                
                                <script type="text/javascript">
                                    $(document).ready(function() {                    
                                    $('table#solvent-<xsl:value-of select="generate-id($cpcmener)"/>').dataTable({
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false
                                    });
                                    });
                                </script>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="restrictions">
        <xsl:variable name="optsetup" select=".//cml:module[@dictRef='cc:initialization']//cml:module[@cmlx:templateRef='optsetup']" />
        <xsl:if test="exists($optsetup)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#restrictions-{generate-id($optsetup)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Restrictions in the Geometry Optimization
                    </h4>
                </div>            
                <div id="restrictions-{generate-id($optsetup)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">                                
                                <table class="display" id="restrictionsT-{generate-id($optsetup)}"></table>
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#restrictionsT-<xsl:value-of select="generate-id($optsetup)"/>").dataTable({
                                            "aaData" : [  
                                            <xsl:for-each select="$optsetup/cml:list">
                                                <xsl:variable name="outerIndex" select="position()"/>
                                                <xsl:variable name="currentRestriction" select="."/>
                                                <xsl:variable name="serial" select="tokenize($currentRestriction/cml:array[@dictRef='x:serial'],'\s+')" />
                                                
                                                <xsl:choose>
                                                    <xsl:when test="starts-with($currentRestriction/cml:scalar[@dictRef='x:restriction']/text(),'C')">
                                                        [ 'Cartesian <xsl:value-of select="$currentRestriction/cml:scalar[@dictRef='x:restriction']/text()"/>', '<xsl:value-of select="$serial[1]"/>', '', '','','','<xsl:value-of select="$currentRestriction/cml:scalar[@dictRef='x:parameter']"/>','']<xsl:if test="$outerIndex &lt; count($optsetup/cml:list)">,</xsl:if>        
                                                    </xsl:when>
                                                    <xsl:when test="$currentRestriction/cml:scalar[@dictRef='x:restriction']/text() = 'B'">
                                                        [ 'Bond', '<xsl:value-of select="$serial[1]"/>', '<xsl:value-of select="$serial[2]"/>', '','','','<xsl:value-of select="$currentRestriction/cml:scalar[@dictRef='x:parameter']"/>','Å']<xsl:if test="$outerIndex &lt; count($optsetup/cml:list)">,</xsl:if>        
                                                    </xsl:when>                                                                                                        
                                                    <xsl:when test="$currentRestriction/cml:scalar[@dictRef='x:restriction']/text() = 'A'">
                                                        [ 'Angle', '<xsl:value-of select="$serial[1]"/>', '<xsl:value-of select="$serial[2]"/>', '<xsl:value-of select="$serial[3]"/>','','','<xsl:value-of select="$currentRestriction/cml:scalar[@dictRef='x:parameter']"/>','deg']<xsl:if test="$outerIndex &lt; count($optsetup/cml:list)">,</xsl:if>        
                                                    </xsl:when>
                                                    <xsl:when test="$currentRestriction/cml:scalar[@dictRef='x:restriction']/text() = 'D'">
                                                        [ 'Dihedral', '<xsl:value-of select="$serial[1]"/>', '<xsl:value-of select="$serial[2]"/>', '<xsl:value-of select="$serial[3]"/>','<xsl:value-of select="$serial[4]"/>','','<xsl:value-of select="$currentRestriction/cml:scalar[@dictRef='x:parameter']"/>','deg']<xsl:if test="$outerIndex &lt; count($optsetup/cml:list)">,</xsl:if>        
                                                    </xsl:when>
                                                </xsl:choose>
                                            </xsl:for-each>
                                            ],
                                            "aoColumns" : [
                                                { "sTitle": "Type" },
                                                { "sTitle": "" },
                                                { "sTitle": "" },
                                                { "sTitle": "" },
                                                { "sTitle": "" },
                                                { "sTitle": "" },
                                                { "sTitle": "Value", "sClass": "right" },
                                                { "sTitle": "Units", "sClass": "right" }
                                                ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    }); 
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if> 
    </xsl:template>
    
    <!-- Interatomic distances (bond distances) -->       
    <xsl:template name="atomicDistances">
        <xsl:variable name="molecule" select="./cml:module[@dictRef='cc:finalization']/cml:molecule"/>
        
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#bondDistances-{generate-id($molecule)}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Bond distances                           
                </h4>
            </div>
            <div id="bondDistances-{generate-id($molecule)}" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <div class="col-md-4 col-sm-12">
                            <table class="display" id="bondDistancesT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th>Atom1</th>
                                        <th>Atom2</th>
                                        <th class="right">Distance</th>                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <xsl:for-each select="$molecule/cml:bondArray/cml:bond">
                                        <xsl:variable name="atomRefs2" select="tokenize(./@atomRefs2,' ')"/>
                                        <!-- For each bond calculate interatomic distances -->
                                        <xsl:variable name="atomType1" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@elementType"/>
                                        <xsl:variable name="x1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@x3"/>
                                        <xsl:variable name="y1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@y3"/>
                                        <xsl:variable name="z1"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/@z3"/>
                                        <xsl:variable name="position1" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[1]]/preceding-sibling::*)+1"/>
                                        <!-- http://stackoverflow.com/questions/226405/find-position-of-a-node-using-xpath -->
                                        <xsl:variable name="atomType2" select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@elementType"/>
                                        <xsl:variable name="x2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@x3"/>
                                        <xsl:variable name="y2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@y3"/>
                                        <xsl:variable name="z2"        select="$molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/@z3"/>
                                        <xsl:variable name="position2" select="count($molecule/cml:atomArray/cml:atom[@id=$atomRefs2[2]]/preceding-sibling::*)+1"/>
                                        <xsl:variable name="distance"  select="ckbk:sqrt((($x2 - $x1) * ($x2 - $x1)) + ($y2 - $y1) * ($y2 - $y1) + ($z2 - $z1) * ($z2 - $z1))"/>
                                        <tr>
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType1"/>
                                                <xsl:value-of select="$position1"/>
                                            </td>                                            
                                            <td class="datacell">
                                                <xsl:value-of select="$atomType2"/>
                                                <xsl:value-of select="$position2"/>
                                            </td>
                                            <td class="right">                                                
                                                <xsl:value-of select="format-number($distance,'#0.000000')"/>
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                 
                                    $('table#bondDistancesT-<xsl:value-of select="generate-id($molecule)"/>').dataTable();                               
                                });  
                            </script>                               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </xsl:template>
    
    <!-- Vibrational frequencies -->
    <xsl:template name="vibrationalFrequencies">
        <xsl:variable name="frequencies" select=".//cml:module[@id='finalization']/cml:propertyList/cml:property[@dictRef='cc:frequencies']/cml:module[@dictRef='cc:vibrations']"/>
        <xsl:variable name="irspectrum" select=".//cml:module[@id='finalization']/propertyList/property[@dictRef='cc:irspectrum']"/>
        <xsl:if test="exists($frequencies) or exists($irspectrum)">
            <div id="vibrationPanel" class="panel panel-default" >
                <div class="panel-heading" data-toggle="collapse" data-target="div#frequencies" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        IR spectrum / Vibrational frequencies   
                    </h4>
                </div>
                <div id="frequencies" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <xsl:call-template name="irSpectrum">
                                <xsl:with-param name="frequencies" select="$frequencies"/>
                            </xsl:call-template>
                            <xsl:call-template name="irSpectrum2">
                                <xsl:with-param name="irspectrum" select="$irspectrum"/> 
                            </xsl:call-template>
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if>                    
    </xsl:template>

    <xsl:template name="irSpectrum">
        <xsl:param name="frequencies"/>                       
        <xsl:if test="exists($frequencies)">
            <xsl:variable name="frequency" select="tokenize($frequencies/array[@dictRef='cc:frequency'],'\s+')"/>
                <div class="col-lg-12">                                                                                             
                    <script type="text/javascript">
                      	delete Jmol._tracker;
                        Jmol._local = true;
                    
                        jsvApplet0 = "jsvApplet0";
                        jmolApplet0 = "jmolApplet0";                                
                        Jmol.setAppletSync([jsvApplet0, jmolApplet0], ["load <xsl:value-of select="$jcampdxurl"/>", ""], true);                                       
                        var jmolInfo = {
                            width: 560,
                            height: 400,
                            debug: false,
                            color: "0xF0F0F0",
                            use: "HTML5",
                            j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                            disableJ2SLoadMonitor: true,
                            disableInitialConsole: true,
                            readyFunction: vibrationsLoaded,
                            animframecallback: "modelchanged",
                            allowjavascript: true,
                            script: "background white; vibration off; vectors off; sync on;"
                        }                                       
                        var jsvInfo = {
                            width: 560,
                            height: 400,
                            debug: false,
                            color: "0xC0C0C0",
                            use: "HTML5",
                            j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",                            
                            script: null,
                            initParams: null,
                            disableJ2SLoadMonitor: true,
                            disableInitialConsole: true,
                            readyFunction: null,
                            allowjavascript: true
                        }

                        $('div#frequencies').on('shown.bs.collapse', function () {
                            if($("div#jmolApplet").children().length == 0){       
                                use="HTML5";                                       
                                jsvApplet0 = Jmol.getJSVApplet("jsvApplet0", jsvInfo);
                                $("div#jsvApplet").html(Jmol.getAppletHtml(jsvApplet0));                                            
                                jmolApplet0 = Jmol.getApplet("jmolApplet0", jmolInfo);
                                $("div#jmolApplet").html(Jmol.getAppletHtml(jmolApplet0));
                                vibrationsLoading();
                            }                                      
                        });

                        function vibrationsLoading(){                                       
                            $('div#frequencies').block({ 
                                message: '<h3>Loading spectrum</h3>', 
                                css: { border: '3px solid #a00' } 
                            });                                                                                    
                        }

                        function vibrationsLoaded(){
                            $('div#frequencies').unblock();                                                           
                        }

                    </script>
                    <div id="irSpectrumDiv">
                        <div id="jsvApplet" style="display:inline; float:left">
                        </div>
                        <div id="jmolApplet" style="display:inline; float:left">
                        </div>
                    </div>
                    <div id="frequencyComboboxDiv" style="display:block">
                        Selected frequency :
                        <script type="text/javascript">
                            function modelchanged(n, objwhat, moreinfo, moreinfo2) {
                                vibrationcboIndex = $("select[name='jmolMenu0'] option:selected").index();
                                if(vibrationcboIndex != objwhat + 1)
                                    $("select[name='jmolMenu0']").val(objwhat + 2);
                            }
                            
                            Jmol.jmolMenu(jmolApplet0,[                    
                                ["vibration off", ".... select ....", true],
                                    <xsl:for-each select="1 to count($frequency)">
                                        <xsl:variable name="outerIndex" select="."/>
                                        ["model <xsl:value-of select="$outerIndex"/>;vibration on" ,"<xsl:value-of select="$frequency[$outerIndex]"/>"]<xsl:if test="$outerIndex != count($frequency)"><xsl:text>,</xsl:text></xsl:if>
                                    </xsl:for-each>                    
                            ]);                        
                        </script>                    
                    </div>
                    <script type="text/javascript">
                        $("#irSpectrumDiv" ).prepend( $("#frequencyComboboxDiv") );
                    </script>                                                              
                </div>                                                        
        </xsl:if> 
    </xsl:template> 
  
    <xsl:function name="orca:getDefaultBasisForAtomType">
        <xsl:param name="elementType"/>
        <xsl:param name="basis"/>        
        <!-- Standard Basis Sets  and Computational Levels -->
        <xsl:variable name="basisNumber" select="number(substring-after($basis,'DefBas-'))"/>        
        <xsl:choose>
            <xsl:when test="$basisNumber >2">TZV</xsl:when>
            <xsl:otherwise>
                <xsl:variable name="elementNumber" select="helper:atomType2Number($elementType)"/>
                <xsl:choose>
                    <xsl:when test="$elementNumber = 1 or not(helper:isTransitionMetal($elementNumber))">SV</xsl:when>
                    <xsl:otherwise>TZ</xsl:otherwise>
                </xsl:choose>
            </xsl:otherwise>            
        </xsl:choose>
    </xsl:function>

    <xsl:template name="irSpectrum2">
        <xsl:param name="irspectrum"/>               
        <xsl:if test="exists($irspectrum)">
            <xsl:variable name="serial" select="tokenize($irspectrum//cml:array[@dictRef='cc:serial'],'\s+')"/>
            <xsl:variable name="frequency" select="tokenize($irspectrum//cml:array[@dictRef='cc:frequency'],'\s+')"/>
            <xsl:variable name="t2" select="tokenize($irspectrum//cml:array[@dictRef='o:t2'],'\s+')"/>
            <xsl:variable name="displacement" select="tokenize($irspectrum//cml:matrix[@dictRef='cc:displacement'],'\s+')"/>                
            <div class="col-md-6 col-sm-12">
                <table class="display" id="irSpectrumT-{generate-id($irspectrum)}"></table>
                
                <script type="text/javascript">
                    $(document).ready(function() {                        
                        $('table#irSpectrumT-<xsl:value-of select="generate-id($irspectrum)"/>').dataTable( {
                            "aaData": [
                            <xsl:for-each select="$serial">
                                <xsl:variable name="outerIndex" select="position()"/>
                                [<xsl:value-of select="$serial[$outerIndex]"/>,"<xsl:value-of select="format-number(number($frequency[$outerIndex]),'#0.00')"/>","<xsl:value-of select="format-number(number($t2[$outerIndex]),'#0.000000')"/>",
                                "<xsl:value-of select="format-number(number($displacement[ (($outerIndex - 1) * 3) + 1 ]),'#0.000000')"/>","<xsl:value-of select="format-number(number($displacement[ (($outerIndex - 1) * 3) + 2 ]),'#0.000000')"/>","<xsl:value-of select="format-number(number($displacement[ (($outerIndex - 1) * 3) + 3 ]),'#0.000000')"/>"
                                ]<xsl:if test="($outerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                            </xsl:for-each>    
                            ],
                            "aoColumns": [
                                { "sTitle": "Mode"    },
                                { "sTitle": "freq (cm**-1)", "sClass": "right"  },
                                { "sTitle": "T**2", "sClass": "right" },
                                { "sTitle": "TX",  "sClass": "right" },
                                { "sTitle": "TY",  "sClass": "right" },
                                { "sTitle": "TZ",  "sClass": "right" }                                            
                            ],
                            "bFilter": false,
                            "bPaginate": true,
                            "bSort": false,
                            "bInfo": false
                        } );   
                    } );                                       
                </script>                                
            </div>
        </xsl:if>        
    </xsl:template>
    <!-- Loedwin charges and spin population -->
    <xsl:template name="populationAnalysis">
        <xsl:variable name="loewdin" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='loewdin'])[last()]"/>
        <xsl:variable name="mulliken" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='mullikenpopulation'])[last()]"/>
        <xsl:if test="exists($loewdin) or exists($mulliken)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#popanal-{generate-id($loewdin)}{generate-id($mulliken)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Population analysis
                    </h4>
                </div>
                <div id="popanal-{generate-id($loewdin)}{generate-id($mulliken)}" class="panel-collapse collapse">                                        
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <xsl:call-template name="loewdin">
                                <xsl:with-param name="loewdin" select="$loewdin"/>
                            </xsl:call-template>
                            <xsl:call-template name="mulliken">
                                <xsl:with-param name="mulliken" select="$mulliken"/>                                                                    
                            </xsl:call-template>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <xsl:template name="loewdin">
        <xsl:param name="loewdin"/>        
        <xsl:if test="exists($loewdin)">
            <xsl:variable name="elementType" select="tokenize($loewdin//cml:array[@dictRef='cc:elementType'],'\s+')"/>
            <xsl:variable name="charge" select="tokenize($loewdin//cml:array[@dictRef='x:charge'],'\s+')"/>
            <xsl:variable name="spin" select="tokenize($loewdin//cml:array[@dictRef='x:spin'],'\s+')"/>
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h4>Loewdin</h4>
                <table class="display" id="loewdinT-{generate-id($loewdin)}"></table>
                <script type="text/javascript">
                    $(document).ready(function() {
                    $('table#loewdinT-<xsl:value-of select="generate-id($loewdin)"/>').dataTable( {
                    "aaData": [
                    <xsl:for-each select="$elementType">
                        <xsl:variable name="outerIndex" select="position()"/>
                        [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="$elementType[$outerIndex]"/>","<xsl:value-of select="$charge[$outerIndex]"/>"<xsl:if test="exists($spin)">, "<xsl:value-of select="$spin[$outerIndex]"/>"</xsl:if>]<xsl:if test="($outerIndex &lt; count($elementType))"><xsl:text>,</xsl:text></xsl:if>
                    </xsl:for-each>    
                    ],
                    "aoColumns": [
                    { "sTitle": "Atom"    },
                    { "sTitle": "", "sClass": "right"  },
                    { "sTitle": "Charge", "sClass": "right" }
                    <xsl:if test="exists($spin)">,{ "sTitle": "Spin",  "sClass": "right" }</xsl:if>
                    ],
                    "bFilter": false,
                    "bPaginate": true,
                    "bSort": false,
                    "bInfo": false
                    } );   
                    } );                                       
                </script>                                       
            </div>          
        </xsl:if>        
    </xsl:template>
    <xsl:template name="mulliken">
        <xsl:param name="mulliken"/>
        <xsl:if test="exists($mulliken)">              
            <xsl:variable name="elementType" select="tokenize($mulliken/cml:module[starts-with(@cmlx:templateRef,'atomiccharges')]/cml:array[@dictRef='cc:elementType'],'\s+')"/>            
            <xsl:variable name="charge" select="tokenize($mulliken/cml:module[starts-with(@cmlx:templateRef,'atomiccharges')]/cml:array[@dictRef='x:charge'],'\s+')"/>
            <xsl:variable name="spin" select="tokenize($mulliken/cml:module[starts-with(@cmlx:templateRef, 'atomiccharges')]/cml:array[@dictRef='x:spin'],'\s+')"/>            
            <div class="col-md-4 col-sm-6 col-xs-12">
                <h4>Mulliken atomic charges</h4>
                <table class="display" id="mullikenT-{generate-id($mulliken)}"></table>                
                <script type="text/javascript">
                    $(document).ready(function() {                        
                    $('table#mullikenT-<xsl:value-of select="generate-id($mulliken)"/>').dataTable( {
                    "aaData": [
                    <xsl:for-each select="$elementType">
                        <xsl:variable name="outerIndex" select="position()"/>
                        [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="$elementType[$outerIndex]"/>","<xsl:value-of select="$charge[$outerIndex]"/>"<xsl:if test="exists($spin)">, "<xsl:value-of select="$spin[$outerIndex]"/>"</xsl:if>]<xsl:if test="($outerIndex &lt; count($elementType))"><xsl:text>,</xsl:text></xsl:if>
                    </xsl:for-each>    
                    ],
                    "aoColumns": [
                     { "sTitle": "Atom"    },
                     { "sTitle": "", },
                     { "sTitle": "Charge", "sClass": "right" }
                     <xsl:if test="exists($spin)">,{ "sTitle": "Spin",  "sClass": "right" }</xsl:if>
                    ],
                    "bFilter": false,
                    "bPaginate": true,
                    "bSort": false,
                    "bInfo": false
                    } );   
                    } );                                       
                </script>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Electrostatic moments -->
    <xsl:template name="electrostaticMoments">
        <xsl:variable name="charge" select="(.//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Charge']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
        <xsl:variable name="electricProps" select="(./cml:module[@dictRef='cc:finalization']//cml:module[@cmlx:templateRef='electricproperties'])[last()]"/>
        <xsl:if test="exists($electricProps)">            
            <xsl:variable name="dipole" select="tokenize($electricProps/cml:array[@dictRef='cc:dipole'],'\s+')"/>
            <xsl:variable name="quadrupole" select="tokenize($electricProps/cml:array[@dictRef='cc:quadrupole'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#dipole-{generate-id($electricProps)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Electrostatic moments 
                    </h4>
                </div>
                <div id="dipole-{generate-id($electricProps)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <xsl:if test="exists($dipole)">
                                <div class="col-lg-6 col-md-6 ">
                                    <h4>Charge</h4>
                                    <table class="display" id="chargeT-{generate-id($electricProps/cml:array[@dictRef='cc:dipole'])}">
                                        <thead>
                                            <tr>
                                                <th> </th>                                          
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><xsl:value-of select="$charge"/></td>
                                            </tr>                     
                                        </tbody>
                                    </table>                       
                                    <h4 style="padding-top:2em">Dipole moment</h4>
                                    <table class="display" id="dipoleT-{generate-id($electricProps/cml:array[@dictRef='cc:dipole'])}">                                    
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th style="text-align:right">NUC</th>
                                                <th style="text-align:right">ELEC</th>
                                                <th style="text-align:right">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>x</td>
                                                <td class="right"><xsl:value-of select="$dipole[1]"/></td>
                                                <td class="right"><xsl:value-of select="$dipole[2]"/></td>
                                                <td class="right"><xsl:value-of select="$dipole[3]"/></td>
                                            </tr>
                                            <tr>
                                                <td>y</td>
                                                <td class="right"><xsl:value-of select="$dipole[4]"/></td>
                                                <td class="right"><xsl:value-of select="$dipole[5]"/></td>
                                                <td class="right"><xsl:value-of select="$dipole[6]"/></td>
                                            </tr>
                                            <tr>
                                                <td>z</td>
                                                <td class="right"><xsl:value-of select="$dipole[7]"/></td>
                                                <td class="right"><xsl:value-of select="$dipole[8]"/></td>
                                                <td class="right"><xsl:value-of select="$dipole[9]"/></td>
                                            </tr>
                                            <tr>
                                                <td>&#956; [Debye]</td>
                                                <td></td>
                                                <td></td>
                                                <td class="right"><xsl:value-of select="$electricProps/cml:scalar[@dictRef='o:magnitude' and @units='nonsi2:debye']"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <script type="text/javascript">
                                        $(document).ready(function() {
                                            $('table#chargeT-<xsl:value-of select="generate-id($electricProps/cml:array[@dictRef='cc:dipole'])"/>').dataTable({
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false
                                            });
                                            
                                            $('table#dipoleT-<xsl:value-of select="generate-id($electricProps/cml:array[@dictRef='cc:dipole'])"/>').dataTable({
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false
                                            });                                            
                                        });
                                    </script>
                                </div>
                            </xsl:if>
                            <xsl:if test="exists($quadrupole)">
                                <div class="col-lg-6 col-md-6 ">                                        
                                    <h4 style="padding-top:2em">Quadrupole moment</h4>
                                    <table class="display" id="quadupoleT-{generate-id($electricProps/cml:array[@dictRef='cc:quadrupole'])}">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th style="text-align:right">NUC</th>
                                                <th style="text-align:right">ELEC</th>
                                                <th style="text-align:right">TOTAL</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>xx</td>
                                                <td class="right"><xsl:value-of select="$quadrupole[1]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[2]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[3]"/></td>
                                            </tr>
                                            <tr>
                                                <td>yy</td>
                                                <td class="right"><xsl:value-of select="$quadrupole[4]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[5]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[6]"/></td>
                                            </tr>
                                            <tr>
                                                <td>zz</td>
                                                <td class="right"><xsl:value-of select="$quadrupole[7]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[8]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[9]"/></td>
                                            </tr>
                                            <tr>
                                                <td>xy</td>
                                                <td class="right"><xsl:value-of select="$quadrupole[10]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[11]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[12]"/></td>
                                            </tr>
                                            <tr>
                                                <td>xz</td>
                                                <td class="right"><xsl:value-of select="$quadrupole[13]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[14]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[15]"/></td>
                                            </tr>
                                            <tr>
                                                <td>yz</td>
                                                <td class="right"><xsl:value-of select="$quadrupole[16]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[17]"/></td>
                                                <td class="right"><xsl:value-of select="$quadrupole[18]"/></td>
                                            </tr>
                                        </tbody>
                                    </table>                                        
                                </div>
                                <div class="col-lg-6 col-md-6">
                                    <table class="display">
                                        <thead>
                                            <tr>
                                                <th> </th>
                                                <th> </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>1/3 trace</td>
                                                <td class="right" id="on">                                                
                                                    <xsl:value-of select="round-half-to-even( (number($quadrupole[3]) + number($quadrupole[6]) +  number($quadrupole[9])) div 3 , 6)"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Anisotropy</td>
                                                <td class="right">                                                
                                                    <xsl:value-of select="orca:getAnisotropy(number($quadrupole[3]),number($quadrupole[6]),number($quadrupole[9]),number($quadrupole[12]),number($quadrupole[15]),number($quadrupole[18]))"/>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('table#quadrupoleT-<xsl:value-of select="generate-id($electricProps/cml:array[@dictRef='cc:quadrupole'])"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });                                            
                                    });
                                </script>
                            </xsl:if>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:function name="orca:getAnisotropy">
        <xsl:param name="xx"/>
        <xsl:param name="yy"/>
        <xsl:param name="zz"/>
        <xsl:param name="xy"/>
        <xsl:param name="xz"/>
        <xsl:param name="yz"/>
        
        <xsl:variable name="xx_yy_2" select="($xx - $yy) * ($xx - $yy)"/>
        <xsl:variable name="yy_zz_2" select="($yy - $zz) * ($yy - $zz)"/>
        <xsl:variable name="zz_xx_2" select="($zz - $xx) * ($zz - $xx)"/>
        <xsl:variable name="xy_2" select="$xy * $xy"/>
        <xsl:variable name="xz_2" select="$xz * $xz"/>
        <xsl:variable name="yz_2" select="$yz * $yz"/>
        
        <xsl:value-of select="round-half-to-even(ckbk:sqrt((0.5 * ($xx_yy_2 + $yy_zz_2 + $zz_xx_2)) + 3 * ($xy_2 + $xz_2 + $yz_2)),6)"/>
        
    </xsl:function>
    
    <!-- Broken Symmetry -->
    <xsl:template name="brokensym">
        <xsl:variable name="brokensym" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='brokensym'])[last()]"/>
        <xsl:if test="exists($brokensym)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#brokensym-{generate-id($brokensym)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Broken symmetry magnetic coupling analysis
                    </h4>
                </div>
                <div id="brokensym-{generate-id($brokensym)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="brokensymT-{generate-id($brokensym)}">
                                    <thead>
                                        <th>Field</th>                                        
                                        <th>Value</th>
                                        <th>Units</th>                                        
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Broken symmetry S(High-Spin)</td>
                                            <td class="right"><xsl:value-of select="$brokensym/cml:scalar[@dictRef='o:sHighSpin']"/></td>
                                            <td></td>
                                        </tr>
                                        <xsl:if test="exists($brokensym/cml:scalar[@dictRef='o:s2HighSpin'])">
                                            <tr>
                                                <td>S**2(High-Spin)</td>
                                                <td class="right"><xsl:value-of select="$brokensym/cml:scalar[@dictRef='o:s2HighSpin']"/></td>
                                                <td></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($brokensym/cml:scalar[@dictRef='o:s2BrokenSym'])">
                                            <tr>
                                                <td>S**2(BrokenSym)</td>
                                                <td class="right"><xsl:value-of select="$brokensym/cml:scalar[@dictRef='o:s2BrokenSym']" /></td>
                                                <td></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($brokensym/cml:scalar[@dictRef='o:eHighSpin'])">
                                            <tr>
                                                <td>E(High-spin)</td>
                                                <td class="right"><xsl:value-of select="$brokensym/cml:scalar[@dictRef='o:eHighSpin']" /></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($brokensym/cml:scalar[@dictRef='o:eHighSpin']/@units)"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($brokensym/cml:scalar[@dictRef='o:eBrokenSym'])">
                                            <tr>
                                                <td>E(BrokenSym)</td>
                                                <td class="right"><xsl:value-of select="$brokensym/cml:scalar[@dictRef='o:eBrokenSym']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($brokensym/cml:scalar[@dictRef='o:eBrokenSym']/@units)"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($brokensym/cml:scalar[@dictRef='o:ediff'])">
                                            <tr>
                                                <td>E(High-Spin)-E(BrokenSym)</td>
                                                <td class="right"><xsl:value-of select="$brokensym/cml:scalar[@dictRef='o:ediff']"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($brokensym/cml:scalar[@dictRef='o:ediff']/@units)"/></td>
                                            </tr>                                            
                                        </xsl:if>
                                    </tbody>
                                </table>  
                                
                                <script type="text/javascript">
                                    $(document).ready(function() {                    
                                        $('table#brokensymT-<xsl:value-of select="generate-id($brokensym)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>            
        </xsl:if>   
    </xsl:template>

    <!-- SCF energy -->    
    <xsl:template name="scfenergy">        
        <xsl:param name="calcType"/>
        <xsl:variable name="scfenergy" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='totalenergy'])[last()]"/>
        <xsl:variable name="mp2" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='mp2'])[last()]" />
        <xsl:variable name="mp2f12" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='mp2'])[last()]/cml:module[@cmlx:templateRef='mp2-f12']"/>
        <xsl:variable name="ci" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='ci'])[last()]"/>
        <xsl:variable name="d3" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='dftd3'])[last()]/cml:scalar[@dictRef='o:dispcorr']"/>
        <xsl:variable name="isDlpno" select="$calcType/@dlpno"/>
        <xsl:if test="exists($scfenergy)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#scfenergy-{generate-id($scfenergy)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Total SCF energy
                    </h4>
                </div>
                <div id="scfenergy-{generate-id($scfenergy)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <table class="display" id="scfenergyT-{generate-id($scfenergy)}">
                                    <thead>
                                         <tr>
                                             <th> </th>                                        
                                             <th class="right">Value</th>
                                             <th>Units</th>     
                                         </tr>                                                                               
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Total Energy</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:totalener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:totalener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Nuclear Repulsion</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:nucrepener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:nucrepener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Electronic Energy</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:electener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:electener']/@units)"/></td>
                                        </tr>                                                                     
                                        <tr>
                                            <td>One Electron Energy</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:oneelecener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:oneelecener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Two Electron Energy</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:twoeener']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:twoeener']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Potential Energy</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:potentialEnergy']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:potentialEnergy']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Kinetic Energy</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='cc:kineticenergy']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:kineticenergy']/@units)"/></td>
                                        </tr>
                                        <tr>
                                            <td>Virial Ratio</td>
                                            <td class="right"><xsl:value-of select="$scfenergy/cml:scalar[@dictRef='o:vircoeff']"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='o:vircoeff']/@units)"/></td>
                                        </tr>
                                        <xsl:if test="exists($mp2)">
                                            <tr>
                                                <td><xsl:value-of select="helper:trim(concat($isDlpno, ' ', 'MP2 Energy'))"/></td>
                                                <xsl:choose>
                                                    <xsl:when test="exists($mp2f12)">
                                                        <td class="right"><xsl:value-of select="round-half-to-even(number($mp2f12/cml:scalar[@dictRef='o:mp2beforef12']),8)"/></td>
                                                        <td><xsl:value-of select="helper:printUnitSymbol($mp2f12/cml:scalar[@dictRef='o:mp2beforef12']/@units)"/></td>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td class="right"><xsl:value-of select="round-half-to-even(number($mp2/cml:scalar[@dictRef='o:total']),8)"/></td>
                                                        <td><xsl:value-of select="helper:printUnitSymbol($mp2/cml:scalar[@dictRef='o:total']/@units)"/></td>        
                                                    </xsl:otherwise>
                                                </xsl:choose>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($ci)">
                                            <xsl:variable name="ccsd" select="($ci//cml:scalar[@dictRef='o:ccsdEner' or @dictRef = 'o:etot'])[1]"/>
                                            <xsl:variable name="ccsdt" select="$ci//cml:scalar[@dictRef='o:ccsdtEner']"/>
                                            <xsl:variable name="t1diag" select="$ci//cml:scalar[@dictRef='o:t1diag']"/>
                                            <xsl:if test="exists($ccsd)">
                                                <tr>
                                                    <td><xsl:value-of select="helper:trim(concat($isDlpno, ' ', 'CCSD Energy'))"/></td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even(number($ccsd),8)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($ccsd/@units)"/></td>
                                                </tr>
                                            </xsl:if>
                                            <xsl:if test="exists($ccsdt)">
                                                <tr>
                                                    <td><xsl:value-of select="helper:trim(concat($isDlpno, ' ', 'CCSD(T) Energy'))"/></td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even(number($ccsdt),8)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($ccsdt/@units)"/></td>
                                                </tr>                                               
                                            </xsl:if>
                                            <xsl:if test="exists($t1diag)">
                                                <tr>
                                                    <td>T1 diagnostic</td>
                                                    <td class="right"><xsl:value-of select="$t1diag"/></td>
                                                    <td></td>
                                                </tr>    
                                            </xsl:if>                                            
                                        </xsl:if>
                                        <xsl:if test="exists($d3)">
                                            <tr>
                                                <td>Dispersion correction</td>
                                                <td class="right"><xsl:value-of select="$d3"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($d3/@units)"/></td>
                                            </tr>                                            
                                        </xsl:if>                                           
                                    </tbody>
                                </table>                                    
                            </div>
                                                                            
                            <script type="text/javascript">
                                $(document).ready(function() {                    
                                    $('table#scfenergyT-<xsl:value-of select="generate-id($scfenergy)"/>').dataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });                                  
                                });                            
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>       
    
    <!-- Frontier orbitals -->
    <xsl:template name="orbitals">
        <xsl:variable name="orbitals" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='orbitalenergies'])[last()]"/>
        <xsl:if test="exists($orbitals)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbitals-{generate-id($orbitals)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Frontier orbitals                            
                    </h4>
                </div>
                <div id="orbitals-{generate-id($orbitals)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <xsl:for-each select="$orbitals/cml:list[@cmlx:templateRef='orbital']">
                                <xsl:variable name="id" select="generate-id(.)"/>
                                <div class="col-md-6 col-sm-6 col-xs-12">                                    
                                    <xsl:variable name="serial" select="tokenize(./cml:array[@dictRef='cc:serial'],'\s+')"/>
                                    <xsl:variable name="occup" select="tokenize(./cml:array[@dictRef='cc:occup'],'\s+')"/>
                                    <xsl:variable name="energy" select="tokenize(./cml:array[@dictRef='cc:energy'],'\s+')"/>
                                    <xsl:variable name="spin" select="./cml:scalar[@dictRef='o:spin']" />
                                    <xsl:if test="exists($spin)">
                                        <h4>Spin <xsl:value-of select="$spin"/> orbitals</h4>
                                    </xsl:if>                                    
                                    <input type="radio" value="all" name="orbitalrangetype-{$id}" onclick="javascript:filterTable(0,'{$id}')"/>All
                                    <input type="radio" value="homolumo" name="orbitalrangetype-{$id}" onclick="javascript:filterHomoLumo('{$id}')" checked="checked"/>Homo/Lumo range:
                                    <input type="text"  value="5"       id="orbitalrange-{$id}" oninput="javascript:filterHomoLumo('{$id}')" />
                                    <table class="display" id="orbitalsT-{$id}"></table>
                                    <script type="text/javascript">
                                                                                
                                        var molecularOrbitalsData<xsl:value-of select="$id"/> = [  
                                            <xsl:for-each select="$serial">
                                                <xsl:variable name="outerIndex" select="position()"/>
                                                [<xsl:value-of select="$serial[$outerIndex]"/>,"<xsl:value-of select="$occup[$outerIndex]"/>","<xsl:value-of select="$energy[$outerIndex]"/>"]<xsl:if test="($outerIndex &lt; count($serial))"><xsl:text>,</xsl:text></xsl:if>
                                            </xsl:for-each>
                                        ];
                                        var molecularOrbitalsDataShort<xsl:value-of select="$id"/> = null;
                                        
                                        $(document).ready(function() {                        
                                            $('table#orbitalsT-<xsl:value-of select="$id"/>').dataTable( {
                                                "aaData": molecularOrbitalsData<xsl:value-of select="$id"/>,
                                                "aoColumns": [
                                                    { "sTitle": "No"    },
                                                    { "sTitle": "Occ", "sClass": "right"  },
                                                    { "sTitle": "E(eV)", "sClass": "right" }                                                
                                                ],
                                                "bFilter": false,
                                                "bPaginate": true,
                                                "bSort": false,
                                                "bInfo": false
                                                 <xsl:if test="$hasOrbitals">
                                                    ,"columnDefs" : [{  
                                                        'targets': 0,
                                                        'render' : function ( data, type, row, meta ) { 
                                                              var label = data + '<xsl:choose><xsl:when test="not(exists($spin)) or lower-case($spin) = 'up' or lower-case($spin)= 'alpha'">a</xsl:when><xsl:otherwise>b</xsl:otherwise></xsl:choose>'  
                                                              return '&lt;a href="javascript:displayMoldenOrbital(\'' + label + '\')">' + data + '&lt;/a>';                                                                                                        
                                                         }
                                                    }]
                                                </xsl:if> 


                                            } );
                                            
                                            filterHomoLumo('<xsl:value-of select="$id"/>');
                                        } );                                       
                                    </script> 
                                </div>                                
                            </xsl:for-each>
                            <script type="text/javascript">                                                               
                                var homoLumoFrontier = null;
                                
                                function filterHomoLumo(id){
                                    $("input[name='orbitalrangetype-" + id + "'][value='homolumo']").prop('checked',true);
                                    var range = $('input#orbitalrange-' + id).val();
                                        if(!$.isNumeric(range))               
                                            range = 10;                                    
                                    filterTable(range, id);               
                                }
                                
                                function findHomoRange(startIndex, range, id){
                                    energyChangesCounter = 0;
                                    for(inx = startIndex - 1; inx > 0 ; inx-- ){
                                        if(window['molecularOrbitalsData' + id][inx][1].length != 0)                                          
                                            energyChangesCounter++;                                                                                                                    
                                        if(energyChangesCounter == range)
                                            return homoLumoFrontier - inx ;                                            
                                    }
                                    return 0;                                                                        
                                }
                                
                                function findLumoRange(startIndex, range, id){
                                    energyChangesCounter = 0;    
                                    for(inx = startIndex + 1; inx &#60; window['molecularOrbitalsData' + id].length ; inx++ ){
                                    if(window['molecularOrbitalsData' + id][inx][1].length != 0)             
                                            energyChangesCounter++;                                                                                            
                                        if(energyChangesCounter == range)
                                            return (inx -1) - homoLumoFrontier;                                            
                                    }
                                    return window['molecularOrbitalsData' + id].length - 1;
                                }                                    
                                
                                function filterTable(range, id){                                                                                                            
                                    findHomoLumoFrontier(id);
                                    if(homoLumoFrontier == -1 || range == 0){
                                        window['molecularOrbitalsDataShort' + id] = window['molecularOrbitalsData' + id];
                                    }else{                                        
                                        var lumoRange = findLumoRange(homoLumoFrontier, range, id);
                                        var homoRange = findHomoRange(homoLumoFrontier, range, id);
                                    
                                        var homoOrbitals = window['molecularOrbitalsData' + id].slice(homoLumoFrontier - Number(homoRange) , homoLumoFrontier );
                                        var lumoOrbitals = window['molecularOrbitalsData' + id].slice(homoLumoFrontier , homoLumoFrontier + Number(lumoRange) + 1);
                                        window['molecularOrbitalsDataShort' + id] = homoOrbitals.concat(lumoOrbitals);
                                    }                                    
                                    var table = $('#orbitalsT-' + id).DataTable();
                                    table.clear();
                                    table.rows.add(window['molecularOrbitalsDataShort' + id]);
                                    table.draw();                        
                                }
                                
                                function findHomoLumoFrontier(id){                                    
                                    homoLumoFrontier = -1;
                                    for(inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> window['molecularOrbitalsData' + id].length; inx++){
                                    if((window['molecularOrbitalsData' + id][inx][1] == 0) <xsl:text disable-output-escaping="yes">&amp;&amp;</xsl:text> (window['molecularOrbitalsData' + id][inx][1].length != 0 )){
                                            homoLumoFrontier = inx;
                                            break;
                                        }
                                    }                                
                                    return homoLumoFrontier;
                                }
                            </script>                                    
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
        
    </xsl:template>
    
    <!-- Natural orbitals -->
    <xsl:template name="natural">
        <xsl:variable name="natural" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='natural'])[last()]"/>
        <xsl:if test="exists($natural)">
            <xsl:variable name="naturalOrbitals" select="tokenize($natural/cml:array[@dictRef='cc:occup'],'\s+')"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#natural-{generate-id($natural)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Natural orbitals                                                    
                    </h4>
                </div>
                <div id="natural-{generate-id($natural)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">                           
                            <div class="col-md-3 col-sm-4 col-xs-12">                                    
                                <table class="display" id="natural-{generate-id($natural)}">
                                    <thead>
                                        <th> </th>
                                        <th> </th>
                                    </thead>
                                    <tbody>
                                        <xsl:for-each select="$naturalOrbitals">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <tr>
                                                <td>N[<xsl:value-of select="position()"/>]= </td>						
                                                <td class="right"><xsl:value-of select="."/></td>
                                            </tr>
                                        </xsl:for-each>								                                            			
                                    </tbody>                                        
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {                        
                                        $('table#natural-<xsl:value-of select="generate-id($natural)"/>').dataTable( {
                                        "bFilter": false,
                                        "bPaginate": true,
                                        "bSort": false,
                                        "bInfo": false                                                                       
                                        });
                                    });                                    
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    <!-- NMR -->
    <xsl:template name="nmr">
        <xsl:variable name="nmr" select=".//module[@id='finalization']/module[@id='otherComponents']/module[@cmlx:templateRef='nmr']"/>
        <xsl:if test="exists($nmr)">
            <xsl:variable name="elementType" select="tokenize($nmr//array[@dictRef='cc:elementType'], '\s+')"/>
            <xsl:variable name="nucleus" select="tokenize($nmr//array[@dictRef='o:nucleus'], '\s+')"/>
            <xsl:variable name="paramagnetic" select="tokenize($nmr//array[@dictRef='o:paramagneticShielding'], '\s+')"/>
            <xsl:variable name="diamagnetic" select="tokenize($nmr//array[@dictRef='o:diamagneticShielding'], '\s+')"/>
            <!--<xsl:variable name="spinOrbit" select="tokenize($nmr//array[@dictRef='o:spinorbitShielding'], '\s+')"/>-->
            <xsl:variable name="total" select="tokenize($nmr//array[@dictRef='o:total'], '\s+')"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#nmr-{generate-id($nmr)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        NMR Shielding Tensors                           
                    </h4>
                </div>
                <div id="nmr-{generate-id($nmr)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-12 col-sm-12">                           
                                <table id="nmrT-{generate-id($nmr)}">
                                    <thead>
                                        <tr>
                                            <th class="right">Atom</th>   
                                            <th class="right">Paramagnetic <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='o:paramagneticShielding']/@units),')')"/></th>
                                            <th class="right">Diamagnetic <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='o:diamagneticShielding']/@units),')')"/></th>
                                            <th class="right">Spin-orbit <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='o:spinorbitShielding']/@units),')')"/></th>
                                            <th class="right">Total <xsl:value-of select="concat('(', helper:printUnitSymbol($nmr//array[@dictRef='o:total']/@units),')')"/></th>
                                        </tr>                                        
                                    </thead>   
                                    <tbody>
                                        <xsl:for-each select="1 to count($elementType)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <tr>
                                                <td class="right"><xsl:value-of select="concat($elementType[$outerIndex],'(',$nucleus[$outerIndex],')')"/></td>   
                                                <td class="right"><xsl:value-of select="format-number(number($paramagnetic[$outerIndex]),'#0.000')"/></td>
                                                <td class="right"><xsl:value-of select="format-number(number($diamagnetic[$outerIndex]),'#0.000')"/></td>
                                                <td class="right"><!---<xsl:value-of select="$spinOrbit[$outerIndex]"/>--></td>
                                                <td class="right"><xsl:value-of select="format-number(number($total[$outerIndex]),'#0.000')"/></td>
                                            </tr>
                                        </xsl:for-each>                                        
                                    </tbody>
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('table#nmrT-<xsl:value-of select="generate-id($nmr)"/>').dataTable({
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false
                                    });
                                    } );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div> 
        </xsl:if>
    </xsl:template>

    <!-- TDDFT -->
    <xsl:template name="tddft">
        <xsl:param name="multiplicity"/>
        <xsl:variable name="tddft" select="(.//cml:module[@ cmlx:templateRef='tddft'])[last()]"/>               
        <xsl:if test="exists($tddft)">            
            <xsl:variable name="groundStateEV" select="number((.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='totalenergy'])[last()]/cml:scalar[@dictRef='cc:totalener'])"/>
            <xsl:variable name="groundStateAU" select="$groundStateEV * $EV_TO_AU"/>
            <xsl:variable name="absorptionspec" select="$tddft/cml:module[@cmlx:templateRef='absorptionspec']"/>
            <xsl:variable name="excitedstates" select="$tddft/cml:module[@cmlx:templateRef='excitedstates']"/>
            <!-- Units step -->
            <xsl:variable name="bandwidthev" select="0.15"/>
            <xsl:variable name="bandwidthcm1" select="1200"/>
            <xsl:variable name="bandwidthnm" select="20"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#tddft" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        TDHF / TDDFT 
                    </h4>
                </div>
                <div id="tddft" class="panel-collapse collapse">                    
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <xsl:variable name="serial" select="tokenize($absorptionspec/cml:array[@dictRef='cc:serial'],'\s+')"/>
                            <xsl:variable name="energy" select="tokenize($absorptionspec/cml:array[@dictRef='o:energy'],'\s+')"/>                                
                            <xsl:variable name="wavelength" select="tokenize($absorptionspec/cml:array[@dictRef='o:wavelength'],'\s+')"/>
                            <xsl:variable name="fosc" select="tokenize($absorptionspec/cml:array[@dictRef='o:fosc'],'\s+')"/>
                            <xsl:variable name="t2" select="tokenize($absorptionspec/cml:array[@dictRef='o:t2'],'\s+')"/>
                            <xsl:variable name="tx" select="tokenize($absorptionspec/cml:array[@dictRef='o:tx'],'\s+')"/>
                            <xsl:variable name="ty" select="tokenize($absorptionspec/cml:array[@dictRef='o:ty'],'\s+')"/>
                            <xsl:variable name="tz" select="tokenize($absorptionspec/cml:array[@dictRef='o:tz'],'\s+')"/>                            
                            
                            <xsl:variable name="orbital" select="(.//cml:module[@cmlx:templateRef='orbitalenergies'])[last()]/cml:list[@cmlx:templateRef='orbital']"/>                                                                                   
                            <div class="col-md-12">
                                <table class="display" id="roots">
                                    <thead>
                                        <tr>
                                            <th rowspan="2" style="text-align:left">Root</th>                                            
                                            <th rowspan="2" style="text-align:left">Spin</th>
                                            <th rowspan="2" style="text-align:right">Total energy (au)</th>
                                            <th rowspan="2" style="text-align:right">∆E (eV)</th>
                                            <th rowspan="2" style="text-align:right">∆E (cm-1)</th>
                                            <th rowspan="2" style="text-align:right">nm</th>
                                            <th rowspan="2" style="text-align:right">osc. strength</th>
                                            <th colspan="4" style="text-align:center">Transition dipole moment</th>
                                        </tr>                                        
                                        <tr>                                           
                                            <th style="text-align:right">x</th>
                                            <th style="text-align:right">y</th>
                                            <th style="text-align:right">z</th>
                                            <th style="text-align:right">Total</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>GS</td>
                                            <td><xsl:value-of select="orca:getRootState($multiplicity)"/></td>                                            
                                            <td class="right">
                                                <xsl:value-of select="format-number($groundStateAU,'#.000000')"/>
                                            </td>
                                            <td class="right">0.00</td>
                                            <td class="right">0</td>
                                            <td class="right">0.0000</td>
                                            <td class="right"/>
                                            <td class="right"/>
                                            <td class="right"/>
                                            <td class="right"/>
                                            <td class="right"/>
                                        </tr>
                                        <xsl:for-each select="1 to count($serial)">
                                            <xsl:variable name="outerIndex" select="."/>
                                            <xsl:variable name="deltaEnergy" select="number($energy[$outerIndex]) * $CM_1_TO_AU" />
                                            <xsl:variable name="rootEnergy" select=" $groundStateAU  + $deltaEnergy" />
                                            <xsl:variable name="tx" select="number($tx[$outerIndex])"/>
                                            <xsl:variable name="ty" select="number($ty[$outerIndex])"/>
                                            <xsl:variable name="tz" select="number($tz[$outerIndex])"/>                                            
                                            <xsl:variable name="t" select="ckbk:sqrt(number($t2[$outerIndex]))"/>
                                            <tr>
                                                <td>                                                   
                                                    <xsl:value-of select="$serial[$outerIndex]"/>
                                                </td>
                                                <td>
                                                    <xsl:value-of select="lower-case(($excitedstates)[1]//cml:scalar[@dictRef='o:serial' and text()= $serial[$outerIndex]]/parent::cml:module/parent::cml:module/cml:scalar[@dictRef='o:type'])"/>
                                                </td>
                                                <td class="right">                                                    
                                                    <xsl:value-of select="format-number($rootEnergy,'#0.000000')"/>
                                                </td>                                                                                                   
                                                <td class="right">
                                                    <xsl:value-of select="format-number($deltaEnergy * $AU_TO_EV, '#.00')"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="format-number($deltaEnergy * $AU_TO_CM_1, '#')"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="format-number($EV_TO_NM div ($deltaEnergy* $AU_TO_EV), '#.000')"/>
                                                </td>                                                
                                                <td class="right">                                                   
                                                    <xsl:value-of select="helper:formatScientific(number($fosc[$outerIndex]),2)"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="helper:formatScientific($tx,2)"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="helper:formatScientific($ty,2)"/>
                                                </td>
                                                <td class="right">
                                                    <xsl:value-of select="helper:formatScientific($tz,2)"/>
                                                </td>
                                                <td class="right">                                                    
                                                    <xsl:value-of select="helper:formatScientific($t,2)"/>
                                                </td>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                </table>
                            </div>
                            <!-- Root plot -->                          
                            <div class="col-md-12">
                                <div id="dipoleTransitionSpinFreePlotlyContainer" style="min-height:450px" class=" d-flex"/>
                            </div>
                            
                            <div class="col-sm-12 col-md-12">
                                <div id="dipoleTransitionSpinFreeContainerControls">
                                    <p style="text-align:right;padding:0px;font-weight: 400;">
                                        <input type="radio" name="tddftGraphUnits" value="ev" checked="checked" onclick="tddftParametersChanged()"/>eV
                                        <input type="radio" name="tddftGraphUnits" value="cm1" onclick="tddftParametersChanged()"/>cm-1
                                        <input type="radio" name="tddftGraphUnits" value="nm" onclick="tddftParametersChanged()"/>nm
                                    </p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">Bandwidth: <input id="bandWidthSpinFree" type="text" size="5" value="0.15"/>
                                    </p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">min X: <input id="minXSpinFree" type="text" size="5" value="0"/>
                                    </p>
                                    <p style="text-align:right;padding:0px;font-weight: 400;">max X: <input id="maxXSpinFree" type="text" size="5" value="10"/>
                                    </p>
                                    <p style="text-align:right;padding:0px">
                                        <input type="button" value="Change" id="changeDipoleTransitionParametersButton" onclick="generateTddftChart()"/>
                                    </p>
                                    <p/>
                                </div>
    
                                <xsl:variable name="bandwidthev" select="0.15"/>
                                <xsl:variable name="bandwidthcm1" select="1200"/>
                                <xsl:variable name="bandwidthnm" select="20"/>
    
    
                                <xsl:variable name="oscStrengths">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="$fosc">,<xsl:value-of select="."/></xsl:for-each>                                    
                                </xsl:variable>
                                <xsl:variable name="energyEv">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="$energy">,<xsl:value-of select="number(.) * $CM_1_TO_EV"/></xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="energyCm1">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="$energy">,<xsl:value-of select="number(.)"/></xsl:for-each>
                                </xsl:variable>
                                <xsl:variable name="energyNm">
                                    <xsl:text>0</xsl:text>
                                    <xsl:for-each select="$energy">,<xsl:value-of select="format-number( $EV_TO_NM div (number(.) * $CM_1_TO_EV), '#0.000')"/></xsl:for-each>
                                </xsl:variable>
                              
                                <script type="text/javascript">                           
                                    //On expand accordion we'll generate our graph, to be correctly resized 
                                    $('#tddft.panel-collapse').on('shown.bs.collapse', function () {
                                        tddftParametersChanged();                                   
                                    })           
                                                                                                        
                                    var evBandwidth = <xsl:value-of select="$bandwidthev"/>;
                                    var cm1Bandwidth   = <xsl:value-of select="$bandwidthcm1"/>;
                                    var nmBandwidth = <xsl:value-of select="$bandwidthnm"/>;
                                    
                                    var evStep = evBandwidth / 30;
                                    var cm1Step = cm1Bandwidth / 30;
                                    var nmStep = nmBandwidth / 30;
                                    
                                    var oscStrengths = [<xsl:value-of select="$oscStrengths"/>];                          
                                    var energyEv     = [<xsl:value-of select="$energyEv"/>];                     
                                    var energyCm1    = [<xsl:value-of select="$energyCm1"/>];
                                    var energyNm     = [<xsl:value-of select="$energyNm"/>];
                                    
                                    <xsl:variable name="energyNumber" as="xs:double*">      
                                        <xsl:for-each select="$energy">
                                            <xsl:element name="item" namespace="http://www.xml-cml.org/schema"><xsl:value-of select="number(.) * $CM_1_TO_EV"/></xsl:element>                                
                                        </xsl:for-each>        
                                    </xsl:variable>
                                    
                                    <xsl:variable name="energyMax" select="number(format-number( round-half-to-even(max($energyNumber),1)  ,'##0.0' )) "/>
                                    <xsl:variable name="energyMin" select="number(format-number( round-half-to-even(min($energyNumber),1) ,'##0.0' )) "/>                                    
                                    var energyMaxEv = round(<xsl:value-of select="$energyMax"/> + evBandwidth);                 
                                    var energyMinEv = round(<xsl:value-of select="$energyMin"/> - evBandwidth);                
                                    var energyMaxCm1 = round(<xsl:value-of select="$energyMax * $EV_TO_CM_1"/> + cm1Bandwidth);          
                                    var energyMinCm1 = round(<xsl:value-of select="$energyMin * $EV_TO_CM_1"/> - cm1Bandwidth);
                                    var energyMaxNm = round(<xsl:value-of select="$EV_TO_NM div $energyMin"/> + nmBandwidth);   
                                    var energyMinNm = round(<xsl:value-of select="$EV_TO_NM div $energyMax"/> - nmBandwidth);     
                                 
                                    function round(num){
                                        return Math.round(num * 100) / 100                                    
                                    }
                                                                     
                                    function tddftParametersChanged (){
                                        setTddftGraphLimitValues();                              
                                        generateTddftChart();                                        
                                    }
                                    
                                    function setTddftGraphLimitValues(){
                                        value = $("input[name='tddftGraphUnits']:checked").val();
                                        if(value == "ev"){                        		     
                                            $("#maxXSpinFree").val(energyMaxEv);
                                            $("#minXSpinFree").val(energyMinEv);      
                                            $("#bandWidthSpinFree").val(evBandwidth);            
                                        }
                                        if(value == "cm1"){     
                                            $("#maxXSpinFree").val(energyMaxCm1);
                                            $("#minXSpinFree").val(energyMinCm1);
                                            $("#bandWidthSpinFree").val(cm1Bandwidth);
                                        }
                                        if(value == "nm"){
                                            $("#maxXSpinFree").val(energyMaxNm);
                                            $("#minXSpinFree").val(energyMinNm);
                                            $("#bandWidthSpinFree").val(nmBandwidth);
                                        }
                                    }
                                    
                                    function generateTddftChart(){
                                        var data = fillTddftChart();
                                        
                                        var xLabel = "";                                        
                                        unitType = $("input[name='tddftGraphUnits']:checked").val();
                                        if(unitType == "ev")                        		              
                                            xLabel = '∆E (eV)';
                                        else if(unitType == "cm1")
                                            xLabel = '∆E (cm-1)';
                                        else  if(unitType == "nm")
                                            xLabel = "nm";

                                        var layout = {
                                            height: 450,
                                            xaxis: {
                                                title: xLabel,
                                                showgrid: true,
                                                zeroline: true,
                                                tickmode: 'auto'
                                            },
                                            hovermode: 'closest',
                                            yaxis: {
                                                title: 'Intensity [arb. units]'
                                            }
                                            /*
                                            legend: {
                                                orientation: 'h',
                                                y: -0.5
                                                }
                                            */
                                        };                                                
                                        Plotly.react('dipoleTransitionSpinFreePlotlyContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                    }
                                                                        
                                    function fillTddftChart(){
                                        //Clear possible previous values                                        
                                        var data = new Array();
                                        var dataSum = new Array();
                                        
                                        // Now we'll generate root and all-roots-sum series and append them in data  and dataSum arrays
                                        var bandWidth = $("#bandWidthSpinFree").val();
                                        var minX = parseFloat($("#minXSpinFree").val());
                                        var maxX = parseFloat($("#maxXSpinFree").val());
                                                                              
                                        unitType = $("input[name='tddftGraphUnits']:checked").val();                                       
                                        for(var inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> oscStrengths.length; inx++){
                                            generateTddftValuesForRoot(data,dataSum,  inx+1, unitType,bandWidth, minX, maxX);
                                        }                                        
                                        
                                        var xVal = new Array();
                                        var yVal = new Array();
                                        var sum = 0;
                                        for(var inx = 0; inx <xsl:text disable-output-escaping="yes">&lt;</xsl:text> dataSum.length; inx++){
                                            xVal.push(dataSum[inx][0]);
                                            yVal.push(dataSum[inx][1]);
                                            sum += dataSum[inx][1];
                                        }
                                        
                                        //Check if there is representable data, if not (no osc.strength values) we'll not show graph                                        
                                        if(sum == 0){
                                            $('#dipoleTransitionSpinFreePlotlyContainer').hide();
                                            $('#dipoleTransitionSpinFreeContainerControls').hide();
                                            return;
                                        }
                                        
                                        //Add sum serie and plot graph
                                        var serie = {
                                            x: xVal,
                                            y: yVal,
                                            mode: 'lines',
                                            name: 'Total sum',
                                            marker: {         
                                                color: 'rgb(00, 00, 00)'
                                            }
                                        };                                                           
                                        data.push(serie);   
                                        return data;
                                    }

                                    function generateTddftValuesForRoot(data, dataSum, root, units, bandWidth, minX, maxX){
                                        var step;
                                        var energyValue;
                                        switch(units){
                                            case 'ev':  step = evStep;
                                                        energyValue = energyEv[root-1];
                                                        break;
                                            case 'cm1': step = cm1Step;
                                                        energyValue = energyCm1[root-1];
                                                        break;
                                            case 'nm':  step = nmStep;
                                                        energyValue = energyNm[root-1];
                                                        break;         
                                        }      
                                                                                
                                        var inx = 0;
                                        var xVal = new Array();
                                        var yVal = new Array();
                                        for(var x = minX; x <xsl:text disable-output-escaping="yes">&lt;</xsl:text> maxX ; x = x + step ) {  
                                            if(dataSum.length <xsl:text disable-output-escaping="yes">&lt;</xsl:text>= inx)     
                                                dataSum.push([x,oscStrengths[root-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - energyValue),2))]);
                                            else
                                                dataSum[inx][1] += oscStrengths[root-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x - energyValue),2))
                                                
                                            xVal.push(x);
                                            yVal.push(oscStrengths[root-1] * Math.exp( -Math.pow(((2*Math.sqrt(2*Math.log(2)))/bandWidth),2) * Math.pow((x -energyValue),2)));
                                            inx++;
                                        }
                                                                                
                                        var serie = {
                                            x: xVal,
                                            y: yVal,
                                            mode: 'lines',
                                            name: 'Root ' + root,
                                        };                                                           
                                        data.push(serie);
                                    }                                    
                                </script>
                            </div>
                            <!-- Dominant contributions -->
                            <xsl:variable name="isUnrestricted" select="exists($orbital//cml:scalar[@dictRef='o:spin'])"/>                            
                            <xsl:variable name="orbitalEnergy" select="tokenize(($orbital/cml:array[@dictRef='cc:energy'])[1],'\s+')"/>
                            <xsl:variable name="orbitalEnergyBeta" select="tokenize(($orbital/cml:array[@dictRef='cc:energy'])[2],'\s+')"/>

                            <div class="col-sm-12 col-md-6">                                
                                <h4>Dominant contributions</h4>
                                <xsl:for-each select="$excitedstates">                                    
                                    <xsl:variable name="excitedstate" select="."/>
                                    <h5><xsl:value-of select="$excitedstate/cml:scalar[@dictRef='o:type']"/></h5>
                                    <xsl:for-each select="$excitedstate/cml:module[@cmlx:templateRef='root']">
                                        <xsl:sort select="number(./cml:scalar[@dictRef='o:serial'])"/>                                                               
                                        <xsl:variable name="serialNumber" select="./cml:scalar[@dictRef='o:serial']"/>
                                        <xsl:variable name="type" select="./parent::cml:module/cml:scalar[@dictRef='o:type']"/>
                                        <xsl:variable name="oscEnergy" select="helper:formatScientific(number($fosc[index-of($serial,$serialNumber)]),2)"/> 
                                        
                                        <xsl:variable name="occOrbitalNumber" select="tokenize(./cml:array[@dictRef='o:occOrbitalNumber'],'\s+')"/>
                                        <xsl:variable name="occOrbitalLabel" select="tokenize(./cml:array[@dictRef='o:occOrbitalLabel'],'\s+')"/>
                                        
                                        <xsl:variable name="virtOrbitalNumber" select="tokenize(./cml:array[@dictRef='o:virtOrbitalNumber'],'\s+')"/>
                                        <xsl:variable name="virtOrbitalLabel" select="tokenize(./cml:array[@dictRef='o:virtOrbitalLabel'],'\s+')"/>                                    
                                        
                                        <xsl:variable name="occEnergy" select="tokenize(./cml:array[@dictRef='o:occEnergy'],'\s+')"/>
                                        <p>
                                            
                                            <span style="margin-right: 10px">
                                                <strong>
                                                    <xsl:value-of select="$serialNumber"/>
                                                    <xsl:text> </xsl:text>
                                                    <xsl:value-of select="lower-case($type)"/>                                            
                                                </strong>
                                            </span>
                                            <span style="margin-right: 10px">
                                                <strong>∆E (eV): </strong>
                                                <xsl:value-of select="format-number(number(./cml:scalar[@dictRef='o:totalEnergy']*$AU_TO_EV),'#0.00')"/>
                                            </span>
                                            <span style="margin-right: 10px">
                                                <strong>Osc. strength : </strong>
                                                <xsl:value-of select="$oscEnergy"/>                                            
                                            </span>
                                        </p>
                                        <xsl:variable name="maxContributions">
                                            <xsl:choose>
                                                <xsl:when test="count($occOrbitalNumber) > 10">10</xsl:when>
                                                <xsl:otherwise><xsl:value-of select="count($occOrbitalNumber)"></xsl:value-of></xsl:otherwise>
                                            </xsl:choose>                                                                                                                     
                                        </xsl:variable>      
                                        <table class="display" id="symmetry">
                                            <thead>
                                                <tr>
                                                    <th>occ. orbital</th>                                              
                                                    <th>energy / eV</th>
                                                    <th>virt. orbital</th>                                                
                                                    <th>energy / eV</th>
                                                    <th>|coeff.|^2*100</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <xsl:for-each select="1 to $maxContributions">                                                
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <tr>
                                                        <td>
                                                            <xsl:variable name="orbital">
                                                                <xsl:value-of select="$occOrbitalNumber[$outerIndex]"/>
                                                                <xsl:value-of select="$occOrbitalLabel[$outerIndex]"/>
                                                            </xsl:variable>
                                                            
                                                            <xsl:choose>
                                                                <xsl:when test="$hasOrbitals">
                                                                    <a id="tddft-{generate-id($orbital)}" href="javascript:displayMoldenOrbital('{$orbital}')">
                                                                        <xsl:value-of select="$orbital"/>
                                                                    </a>        
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:value-of select="$orbital"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </td>
                                                        <td>
                                                            <xsl:choose>
                                                                <xsl:when test="$occOrbitalLabel[$outerIndex] = 'a'">
                                                                    <xsl:value-of select="$orbitalEnergy[number($occOrbitalNumber[$outerIndex]) + 1]"/>        
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:value-of select="$orbitalEnergyBeta[number($occOrbitalNumber[$outerIndex]) + 1]"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>                                                            
                                                        </td>
                                                        <td>
                                                            <xsl:variable name="virtorbital">
                                                                <xsl:value-of select="$virtOrbitalNumber[$outerIndex]"/>
                                                                <xsl:value-of select="$virtOrbitalLabel[$outerIndex]"/>
                                                            </xsl:variable>
                                                            
                                                            <xsl:choose>
                                                                <xsl:when test="$hasOrbitals">
                                                                    <a id="tddft-{generate-id($virtorbital)}" href="javascript:displayMoldenOrbital('{$virtorbital}')">
                                                                        <xsl:value-of select="$virtorbital"/>
                                                                    </a>        
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:value-of select="$virtorbital"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </td>
                                                        <td>
                                                            <xsl:choose>
                                                                <xsl:when test="$virtOrbitalLabel[$outerIndex] = 'a'">
                                                                    <xsl:value-of select="$orbitalEnergy[number($virtOrbitalNumber[$outerIndex]) + 1]"/>
                                                                </xsl:when>
                                                                <xsl:otherwise>
                                                                    <xsl:value-of select="$orbitalEnergyBeta[number($virtOrbitalNumber[$outerIndex]) + 1]"/>
                                                                </xsl:otherwise>
                                                            </xsl:choose>
                                                        </td>
                                                        <td>
                                                            <xsl:value-of select="round-half-to-even(number($occEnergy[$outerIndex]) * 100,2)"/>
                                                        </td>
                                                    </tr>
                                                </xsl:for-each>
                                            </tbody>
                                        </table>                              
                                        <!-- Warning message -->  
                                        <xsl:variable name="mostRelevantArray" as="item()*">
                                            <xsl:for-each select="1 to $maxContributions">
                                                <xsl:variable name="outerIndex" select="."/>                                            
                                                <xsl:value-of select="number(number($occEnergy[$outerIndex]) * 100)"/>
                                            </xsl:for-each>
                                        </xsl:variable>                                  
                                        <xsl:if test="sum($mostRelevantArray) &lt; 50">
                                            <div class="alert alert-warning" role="alert">
                                                Warning: &#931;c<sup>2</sup><sub>i</sub> &lt; 50 %                                            
                                            </div>
                                        </xsl:if>
                                        <br/>                                        
                                    </xsl:for-each>                                    
                                </xsl:for-each>                                                                
                            </div>                           
                        </div>
                    </div>
                </div>             
            </div>                       
        </xsl:if>
    </xsl:template>
    
    <xsl:function name="orca:getRootState" as="xs:string">
        <xsl:param name="multiplicity"/>        
        <xsl:choose>
            <xsl:when test="$multiplicity = '1'">Singlet</xsl:when>
            <xsl:when test="$multiplicity = '2'">Doblet</xsl:when>
            <xsl:when test="$multiplicity = '3'">Triplet</xsl:when>
            <xsl:when test="$multiplicity = '4'">Quartet</xsl:when>
            <xsl:when test="$multiplicity = '5'">Quintet</xsl:when>
            <xsl:otherwise><xsl:value-of select="$multiplicity"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <!-- EPR -->
    <xsl:template name="epr">
        <xsl:variable name="eprnmr" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='eprnmr'])[last()]"/>               
        <xsl:if test="exists($eprnmr)">
            <xsl:variable name="gmatrix" select="tokenize($eprnmr//cml:module[@cmlx:templateRef='gmatrix']/cml:matrix[@dictRef='gmatrix'],'\s+')"/>
            <xsl:variable name="gtot" select="tokenize(($eprnmr//cml:module[@cmlx:templateRef='gmatrix']/cml:array[@dictRef='o:g'])[last()],'\s+')" />
            <xsl:variable name="zerofield" select="$eprnmr/cml:module[@cmlx:templateRef='zerofield']"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#eprnmr-{generate-id($eprnmr)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        g-matrix and ZFS 
                    </h4>
                </div>
                <div id="eprnmr-{generate-id($eprnmr)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <h4>g-matrix</h4>
                                <table class="display compact" id="gmatrix-{generate-id($eprnmr)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="right">
                                            <td>
                                                <xsl:value-of select="$gmatrix[1]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$gmatrix[2]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$gmatrix[3]"/>
                                            </td>
                                            <td/>
                                        </tr>
                                        <tr class="right">
                                            <td>
                                                <xsl:value-of select="$gmatrix[4]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$gmatrix[5]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$gmatrix[6]"/>
                                            </td>
                                            <td/>
                                        </tr>
                                        <tr class="right">
                                            <td>
                                                <xsl:value-of select="$gmatrix[7]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$gmatrix[8]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$gmatrix[9]"/>
                                            </td>
                                            <td/>
                                        </tr>
                                        <tr class="right">
                                            <td>g(tot) X= <xsl:value-of select="$gtot[1]"/></td>
                                            <td>Y= <xsl:value-of select="$gtot[2]"/></td>
                                            <td>Z= <xsl:value-of select="$gtot[3]"/></td>
                                            <td>iso= <xsl:value-of
                                                  select="$eprnmr//cml:module[@cmlx:templateRef = 'gmatrix']/cml:scalar[@dictRef = 'o:giso']"
                                                /></td>
                                        </tr>
                                    </tbody>
                                </table>

                                <xsl:variable name="rawmatrix"
                                    select="tokenize($zerofield/cml:matrix[@dictRef = 'o:raw'], '\s+')"/>
                                <h4>ZFS</h4>
                                <table class="display compact" id="zerofieldmatrix-{generate-id($eprnmr)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[1]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[2]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[3]"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[4]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[5]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[6]"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[7]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[8]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$rawmatrix[9]"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <xsl:variable name="directiond"
                                    select="$zerofield/cml:list[@dictRef = 'direction']/cml:scalar[@dictRef = 'o:d']"/>
                                <xsl:variable name="directioned"
                                    select="$zerofield/cml:list[@dictRef = 'direction']/cml:scalar[@dictRef = 'o:ed']"/>
                                <br/>

                                <table class="display compact"
                                    id="directions-{generate-id($eprnmr)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody class="left">
                                        <tr>
                                            <td>D=</td>
                                            <td>
                                                <xsl:value-of select="$directiond"/>
                                            </td>
                                            <td>
                                                <xsl:value-of
                                                  select="helper:printUnitSymbol($directiond/@units)"
                                                />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>E/D=</td>
                                            <td>
                                                <xsl:value-of select="$directioned"/>
                                            </td>
                                            <td/>
                                        </tr>
                                    </tbody>
                                </table>

                                <xsl:variable name="contributions"
                                    select="$zerofield/cml:list[@dictRef = 'contributions']"/>
                                <xsl:variable name="contheaders"
                                    select="tokenize(upper-case($contributions/cml:array[@dictRef = 'o:part']), '[\|]+')"/>
                                <xsl:variable name="contd"
                                    select="tokenize($contributions/cml:array[@dictRef = 'o:d'], '\s+')"/>
                                <xsl:variable name="conte"
                                    select="tokenize($contributions/cml:array[@dictRef = 'o:e'], '\s+')"/>
                                <xsl:variable name="spinspin" select="'SPIN-SPIN'" as="xs:string"/>
                                <xsl:variable name="indexspinspin"
                                    select="index-of($contheaders, $spinspin)"/>

                                <table class="display" id="contributions-{generate-id($eprnmr)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th>D</th>
                                            <th>E</th>
                                        </tr>
                                    </thead>
                                    <tbody class="left">
                                        <tr>
                                            <td>SPIN-SPIN</td>
                                            <td>
                                                <xsl:value-of select="$contd[$indexspinspin]"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="$conte[$indexspinspin]"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>

                                <script type="text/javascript">
                                    $(document).ready(function() {
                                      $('table#gmatrix-<xsl:value-of select="generate-id($eprnmr)"/>').dataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                        });
                                      $('table#zerofieldmatrix-<xsl:value-of select="generate-id($eprnmr)"/>').dataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                        });
                                     $('table#directions-<xsl:value-of select="generate-id($eprnmr)"/>').dataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                        });                                      
                                      $('table#contributions-<xsl:value-of select="generate-id($eprnmr)"/>').dataTable({
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                        });
                                    });
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <!-- Final results -->
    <xsl:template name="finalresults">
        <xsl:param name="isBrokenSymm"/>
        <xsl:param name="calcType"/>
        <xsl:variable name="cosmoener" select="((.//cml:module[@cmlx:templateRef='totalenergy'])[last()])/cml:scalar[@dictRef='cc:cosmoener']"/>                                        
        <xsl:variable name="cpcmener" select="((.//cml:module[@cmlx:templateRef='totalenergy'])[last()])/cml:scalar[@dictRef='o:cpcmener']"/>
        <xsl:variable name="scfenergy" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='totalenergy'])[last()]"/>
        <xsl:variable name="spincontaminations" select=".//cml:module[@id='finalization']//cml:module[@cmlx:templateRef='spincontamination']"/>                         
        <xsl:variable name="zeropoint" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='innerenergy']/cml:module[@cmlx:templateRef='contributions']/cml:scalar[@dictRef='o:zeropoint'])" />
        <xsl:variable name="mp2" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='mp2'])[last()]" />
        <xsl:variable name="mp2f12" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='mp2'])[last()]/cml:module[@cmlx:templateRef='mp2-f12']"/>        
        <xsl:variable name="ci" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='ci'])[last()]"/>
        <xsl:variable name="d3" select="(.//cml:module[@id='finalization']/cml:module[@id='otherComponents']/cml:module[@cmlx:templateRef='dftd3'])[last()]/cml:scalar[@dictRef='o:dispcorr']"/>
        <xsl:variable name="isDlpno" select="$calcType/@dlpno"/>
        
        <xsl:if test="exists($scfenergy)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#energy-{generate-id($scfenergy)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Final results 
                    </h4>
                </div>
                <div id="energy-{generate-id($scfenergy)}" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">
                            <div class="col-lg-5 col-md-5 col-sm-12 ">
                                <table class="display" id="energyT-{generate-id($scfenergy)}">
                                    <thead>
                                        <tr>
                                            <th> </th>
                                            <th> </th>
                                            <th> </th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>                                        
                                        <tr>
                                            <td>Total Energy</td>
                                            <td class="right"><xsl:value-of select="round-half-to-even(number($scfenergy/cml:scalar[@dictRef='cc:totalener']),8)"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:totalener']/@units)"/></td>
                                        </tr>
                                            <xsl:if test="exists($cosmoener)">
                                         <tr>
                                             <td>COSMO(ediel)</td>
                                             <td class="right"><xsl:value-of select="round-half-to-even(number($cosmoener),8)"/></td>
                                             <td><xsl:value-of select="helper:printUnitSymbol($cosmoener/@units)"/></td>
                                         </tr>
                                            </xsl:if>
                                            <xsl:if test="exists($cpcmener)">
                                        <tr>
                                            <td>CPCM Dielectric</td>
                                            <td class="right"><xsl:value-of select="round-half-to-even(number($cpcmener),8)"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($cpcmener/@units)"/></td>
                                        </tr>
                                            </xsl:if>
                                        <tr>
                                            <td>Nuclear Repulsion</td>
                                            <td class="right"><xsl:value-of select="round-half-to-even(number($scfenergy/cml:scalar[@dictRef='cc:nucrepener']),8)"/></td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($scfenergy/cml:scalar[@dictRef='cc:nucrepener']/@units)"/></td>
                                        </tr>
                                        <xsl:if test="exists($zeropoint)">
                                            <tr>
                                                <td>Zero point vibrational energy</td>
                                                <td class="right"><xsl:value-of select="round-half-to-even(number($zeropoint),8)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($zeropoint/@units)"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($spincontaminations)">
                                            <xsl:choose>
                                                <xsl:when test="$isBrokenSymm = 'true'">
                                                    <xsl:for-each select="$spincontaminations">     <!-- On broken symm print all s**2 values -->
                                                        <xsl:variable name="spincontamination" select="."/>                                                
                                                        <tr>
                                                            <td>&#60;S^2&#62;</td>
                                                            <td class="right"><xsl:value-of select="round-half-to-even(number($spincontamination/cml:scalar[@dictRef='cc:s2']),3)"/></td>
                                                            <td>(expected value: <xsl:value-of select="round-half-to-even(number($spincontamination/cml:scalar[@dictRef='cc:s2ideal']),3)"/>)</td>
                                                        </tr>    
                                                    </xsl:for-each>        
                                                </xsl:when>
                                                <xsl:otherwise> <!-- By default print last s**2 -->
                                                    <xsl:variable name="spincontamination" select="($spincontaminations)[last()]"/>                                                
                                                    <tr>
                                                        <td>&#60;S^2&#62;</td>
                                                        <td class="right"><xsl:value-of select="round-half-to-even(number($spincontamination/cml:scalar[@dictRef='cc:s2']),3)"/></td>
                                                        <td>(expected value: <xsl:value-of select="round-half-to-even(number($spincontamination/cml:scalar[@dictRef='cc:s2ideal']),3)"/>)</td>
                                                    </tr>
                                                </xsl:otherwise>
                                            </xsl:choose>                                            
                                        </xsl:if>                                        
                                        <xsl:if test="exists($mp2)">
                                            <tr>
                                                <td><xsl:value-of select="helper:trim(concat($isDlpno, ' ', 'MP2 Energy'))"/></td>
                                                <xsl:choose>
                                                    <xsl:when test="exists($mp2f12)">
                                                        <td class="right"><xsl:value-of select="round-half-to-even(number($mp2f12/cml:scalar[@dictRef='o:mp2beforef12']),8)"/></td>
                                                        <td><xsl:value-of select="helper:printUnitSymbol($mp2f12/cml:scalar[@dictRef='o:mp2beforef12']/@units)"/></td>
                                                    </xsl:when>
                                                    <xsl:otherwise>
                                                        <td class="right"><xsl:value-of select="round-half-to-even(number($mp2/cml:scalar[@dictRef='o:total']),8)"/></td>
                                                        <td><xsl:value-of select="helper:printUnitSymbol($mp2/cml:scalar[@dictRef='o:total']/@units)"/></td>        
                                                    </xsl:otherwise>
                                                </xsl:choose>    
                                            </tr>
                                        </xsl:if>                                       
                                        <xsl:if test="exists($mp2f12)">
                                            <tr>
                                                <td>MP2-F12 Energy</td>
                                                <td class="right"><xsl:value-of select="round-half-to-even(number($mp2f12/cml:scalar[@dictRef='o:mp2f12energy']),8)"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($mp2f12/cml:scalar[@dictRef='o:mp2f12energy']/@units)"/></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($ci)">
                                            <xsl:variable name="ccsd" select="($ci//cml:scalar[@dictRef='o:ccsdEner' or @dictRef = 'o:etot'])[1]"/>
                                            <xsl:variable name="ccsdt" select="$ci//cml:scalar[@dictRef='o:ccsdtEner']"/>                                            
                                            <xsl:if test="exists($ccsd)">
                                                <tr>
                                                    <td><xsl:value-of select="helper:trim(concat($isDlpno, ' ', 'CCSD Energy'))"/></td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even(number($ccsd),8)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($ccsd/@units)"/></td>
                                                </tr>
                                            </xsl:if>
                                            <xsl:if test="exists($ccsdt)">
                                                <tr>
                                                    <td><xsl:value-of select="helper:trim(concat($isDlpno, ' ', 'CCSD(T) Energy'))"/></td>
                                                    <td class="right"><xsl:value-of select="round-half-to-even(number($ccsdt),8)"/></td>
                                                    <td><xsl:value-of select="helper:printUnitSymbol($ccsdt/@units)"/></td>
                                                </tr>                                               
                                            </xsl:if>                                                                                       
                                        </xsl:if>
                                        <xsl:if test="exists($d3)">
                                            <tr>
                                                <td>Dispersion correction</td>
                                                <td class="right"><xsl:value-of select="$d3"/></td>
                                                <td><xsl:value-of select="helper:printUnitSymbol($d3/@units)"/></td>
                                            </tr>                                            
                                        </xsl:if>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
   
    <xsl:template name="wavefunctionspecs">
        <xsl:variable name="casscf" select="(.//cml:module[@cmlx:templateRef='casscf'])[last()]"/>
        <xsl:if test="exists($casscf)">
            <xsl:variable name="totalelect" select="$casscf//cml:module[@cmlx:templateRef='systemsettings']/cml:scalar[@dictRef='o:elecno']"/>
            <xsl:variable name="activeelect" select="$casscf//cml:module[@cmlx:templateRef='systemsettings']/cml:scalar[@dictRef='o:activeelec']"/>
            <xsl:variable name="inactiveorb" select="$casscf//cml:module[@cmlx:templateRef='orbitalranges']/cml:scalar[@dictRef='o:inactiveorb']" />            
            <xsl:variable name="activeorb" select="$casscf//cml:module[@cmlx:templateRef='orbitalranges']/cml:scalar[@dictRef='o:activeorb']" />
            <xsl:variable name="externalorb" select="$casscf//cml:module[@cmlx:templateRef='orbitalranges']/cml:scalar[@dictRef='o:externalorb']" />    
            <xsl:variable name="multiplicity" select="$casscf/cml:module[@cmlx:templateRef='casscfstates']/cml:scalar[@dictRef='cc:multiplicity']"/>            
            <xsl:variable name="charge" select="(.//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Charge']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#waveSpecs-{generate-id($casscf)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Wave function specification</h4>
                </div>            
                <div id="waveSpecs-{generate-id($casscf)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12" >                            
                                <table class="display" id="waveSpecsT-{generate-id($casscf)}">
                                    <thead>
                                        <tr><td></td><td></td></tr>
                                    </thead>
                                    <tbody>
                                        <xsl:if test="exists($totalelect) and exists($activeelect)">
                                            <tr>
                                                <td>Number of closed shell electrons</td>
                                                <td><xsl:value-of select="$totalelect - $activeelect"/></td>
                                            </tr>                                        
                                        </xsl:if>
                                        <xsl:if test="exists($activeelect)">
                                            <tr>
                                                <td>Number of electrons in active shells</td>
                                                <td><xsl:value-of select="$activeelect" /> </td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($inactiveorb)">
                                            <tr>
                                                <td>Number of internal orbitals</td>
                                                <td><xsl:value-of select="$inactiveorb" />
                                                </td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($activeorb)">
                                            <tr>
                                                <td>Number of active orbitals</td>
                                                <td>
                                                    <xsl:choose>
                                                        <xsl:when test="$hasOrbitals">
                                                            <div class="btn-group btn-link">
                                                                <span class="dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <xsl:value-of select="$activeorb"/>
                                                                </span>                                                                    
                                                                <div class="dropdown-menu" >
                                                                    <xsl:variable name="activeOrbitalStart" select="number($inactiveorb) "/>
                                                                    <xsl:variable name="activeOrbitalEnd" select="$activeOrbitalStart + number($activeorb) - 1"/>
                                                                    <xsl:for-each select="xs:integer($activeOrbitalStart) to xs:integer($activeOrbitalEnd)">
                                                                        <xsl:variable name="outerIndex" select="."/>
                                                                        <a class="dropdown-item" href="javascript:displayMoldenOrbital('{$outerIndex}a')"><xsl:value-of select="$outerIndex"/></a>    
                                                                    </xsl:for-each>
                                                                </div>
                                                            </div>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="$activeorb"/>    
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                                

                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($externalorb)">
                                            <tr>
                                                <td>Number of external orbitals</td>
                                                <td><xsl:value-of select="$externalorb" /></td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($multiplicity)">
                                            <tr>
                                                <td>Spin quantum number</td>                                                
                                                <td>
                                                    <xsl:for-each select="$multiplicity">
                                                        <span class="mr-2"><xsl:value-of select="round(((. - 1) div 2) * 10) div 10" /><xsl:text> </xsl:text></span>
                                                    </xsl:for-each>
                                                </td>
                                            </tr>
                                        </xsl:if>                                        
                                        
                                        <xsl:if test="exists($charge)">
                                            <tr>
                                                <td>Total molecular charge</td>
                                                <td><xsl:value-of select="$charge"/></td>
                                            </tr>
                                        </xsl:if>                                    
                                    </tbody>                                
                                </table>
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                    $('table#waveSpecsT-<xsl:value-of select="generate-id($casscf)"/>').dataTable({
                                    "bFilter": false,
                                    "bPaginate": false,
                                    "bSort": false,
                                    "bInfo": false
                                    });
                                    } );
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </xsl:if>        
    </xsl:template>
    
  
    <xsl:template name="ciExpansion">
        <xsl:variable name="ciStep" select="(.//cml:module[@cmlx:templateRef='cistep'])[last()]"/>        
        <xsl:if test="exists($ciStep)">        
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#ciExpansion-{generate-id($ciStep)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">CI expansion specifications</h4>
                </div>            
                <div id="ciExpansion-{generate-id($ciStep)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-8 col-sm-12">
                                <script type="text/javascript">
                                    var ciBlocks = [<xsl:value-of select="replace($ciStep//cml:array[@dictRef='o:blocknum'], '\s', ',')"/>];
                                    var ciCSFs = [<xsl:value-of select="replace($ciStep//cml:array[@dictRef='o:csfnum'], '\s', ',')"/>];
                                    var ciRoots = [<xsl:value-of select="replace($ciStep//cml:array[@dictRef='o:ciroots'], '\s', ',')"/>];
                                    var ciMultiplicity = [<xsl:value-of select="replace($ciStep//cml:array[@dictRef='o:blockmult'], '\s', ',')"/>];
                                    <xsl:for-each select="$ciStep//cml:module[@cmlx:templateRef='block']">
                                        <xsl:variable name="block" select="."/>
                                        <xsl:variable name="index" select="position()"/>
                                        var ciRootIndexes_<xsl:value-of select="$index"/> = [ <xsl:value-of select="replace($block/cml:array[@dictRef='o:rootnumber'], '\s', ', ')"/>]; 
                                        var ciRootWeights_<xsl:value-of select="$index"/> = [ <xsl:value-of select="replace($block/cml:array[@dictRef='o:rootweight'], '\s', ', ')"/>];
                                    </xsl:for-each>                                    
                                </script>
                                <div class="button-group">
                                    Block : 
                                    <xsl:for-each select="1 to $ciStep//cml:array[@dictRef='o:blocknum']/@size">                                        
                                        <button class="btn btn-default btn-ci-block" type="button" onclick="javascript:buildCiBlockTable(this, {.})">
                                            <xsl:value-of select="."/>                                        
                                        </button>
                                    </xsl:for-each>
                                </div>  

                                <table id="ciExpansionT-{generate-id($ciStep)}" class="display">
                                    <thead>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </thead>
                                    <tbody>                                        
                                        <tr>
                                            <td>Multiplicity</td>
                                            <td id="ciExpansionMultiplicity-{generate-id($ciStep)}"></td>
                                        </tr>
                                        <tr>
                                            <td>Number of determinants</td>
                                            <td id="ciExpansionDeterminants-{generate-id($ciStep)}"></td>
                                        </tr>
                                        <tr>
                                            <td>Number of root(s) required	</td>
                                            <td id="ciExpansionNumRequiredRoots-{generate-id($ciStep)}"></td>
                                        </tr>
                                        <tr>
                                            <td>CI roots used/Weights</td>
                                            <td id="ciExpansionRootsWeights-{generate-id($ciStep)}"></td>
                                        </tr>
                                    </tbody>                                   
                                </table>
                                
                                <table id="ciExpansionWeightsT-{generate-id($ciStep)}" style="display:none">                                       
                                </table>
                                
                                <script type="text/javascript">  
                                    $(document).ready(function(){
                                        var ciButton = $(".btn-ci-block")[0];
                                        buildCiBlockTable(ciButton, 1);                                        
                                    });
                                    
                                    function buildCiBlockTable(button, index) {                                                                            
                                        $(".btn-ci-block").removeClass("btn-primary");
                                        $(button).addClass("btn-primary");
                                    
                                        $('#ciExpansionMultiplicity-<xsl:value-of select="generate-id($ciStep)"/>').html(ciMultiplicity[index-1]);
                                        $('#ciExpansionDeterminants-<xsl:value-of select="generate-id($ciStep)"/>').html(ciCSFs[index-1]);
                                        $('#ciExpansionNumRequiredRoots-<xsl:value-of select="generate-id($ciStep)"/>').html(ciRoots[index-1]);                                    
                                        fillWeightsTable(index);                                        
                                        $('#ciExpansionRootsWeights-<xsl:value-of select="generate-id($ciStep)"/>').html($('#ciExpansionWeightsT-<xsl:value-of select="generate-id($ciStep)"/>').html());
                                        $('#ciExpansionT-<xsl:value-of select="generate-id($ciStep)"/>').show();
                                        $('table#ciExpansionT-<xsl:value-of select="generate-id($ciStep)"/>').dataTable( {
                                          "bFilter": false,
                                          "bPaginate": false,
                                          "bSort": false,
                                          "bInfo": false, 
                                          "bDestroy": true
                                        } );
                                        
                                        //Render download option
                                        var tableName = 'ciExpansionT-<xsl:value-of select="generate-id($ciStep)"/>';
                                        $('div#downloadTable' + tableName).remove();
                                        var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                        $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                                                                    
                                     }     
                                     
                                     function fillWeightsTable(index) {
                                        var tableHtml = '<tr>';
                                        var rootIndexes = window['ciRootIndexes_' + index];
                                        var rootWeigths = window['ciRootWeights_' + index];
                                        for(let inx = 0; inx &lt;  rootIndexes.length; inx++) {
                                            tableHtml += '<td>' + rootIndexes[inx] +'</td>'
                                        }
                                        tableHtml += '</tr><tr>';
                                        for(let inx = 0; inx &lt;  rootWeigths.length; inx++) {
                                            tableHtml += '<td>' + rootWeigths[inx] +'</td>'
                                        }
                                        tableHtml += '</tr>';
                                        $('#ciExpansionWeightsT-<xsl:value-of select="generate-id($ciStep)"/>').html(tableHtml);
                                     }                                      
                                 
                                </script>         
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="waveFunctionTopWeightsValues">
        <xsl:variable name="waveFuncWeightsArray" select="(.//cml:module[@cmlx:templateRef='casscf'])[last()]//cml:module[@cmlx:templateRef='casscfstates']"/>
        <xsl:if test="exists($waveFuncWeightsArray)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#waveFuncWeights-{generate-id($waveFuncWeightsArray[0])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Wave functions / Weights of the most important CSFs</h4>
                </div>            
                <div id="waveFuncWeights-{generate-id($waveFuncWeightsArray[0])}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-12 col-sm-12">
                                <div class="button-group">
                                    Multiplicity : 
                                    <xsl:for-each select="$waveFuncWeightsArray">
                                        <xsl:variable name="waveFuncWeights" select="."/>
                                        <xsl:variable name="blockNum" select="$waveFuncWeights/cml:scalar[@dictRef='o:blocknum']" />                                        
                                        <xsl:variable name="multiplicity" select="$waveFuncWeights/cml:scalar[@dictRef='cc:multiplicity']" />
                                        <button class="btn btn-default btn-wavefunction-block" type="button" onclick="javascript:displayWaveFunctionWeightsTable(this,{$blockNum})">
                                            <xsl:value-of select="$multiplicity"/>                                        
                                        </button>
                                    </xsl:for-each>
                                </div>
                                                                
                                 <xsl:for-each select="$waveFuncWeightsArray">
                                     <xsl:variable name="waveFuncWeights" select="."/>                
                                     <xsl:variable name="colWidth" select="if(count($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients']/cml:scalar[@dictRef='o:rootnumber']) &gt; 4) then '12' else '6'"/>
                                     <xsl:variable name="weightsMatrix">                                         
                                        <xsl:for-each select="$waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients']">
                                            <xsl:variable name="coeff" select="." />                    
                                            <xsl:variable name="root" select="$coeff/cml:scalar[@dictRef='o:rootnumber']" />
                                            <xsl:variable name="confs" select="tokenize($coeff/cml:array[@dictRef='x:value'],'\s+')"/>
                                            <xsl:variable name="confIndex" select="tokenize($coeff/cml:array[@dictRef='o:configuration'],'\s+')"/>
                                            <xsl:variable name="weight" select="tokenize($coeff/cml:array[@dictRef='o:weight'],'\s+')" />
                                            <xsl:element name="cml:root">
                                                <xsl:attribute name="number" select="$root"/>
                                                
                                                <xsl:for-each select="1 to count($confs)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <xsl:element name="cml:value">
                                                        <xsl:attribute name="conf" select="$confs[$outerIndex]"/>
                                                        <xsl:attribute name="index" select="$confIndex[$outerIndex]"/>
                                                        <xsl:attribute name="weight" select="$weight[$outerIndex]"/>
                                                    </xsl:element>
                                                </xsl:for-each>
                                            </xsl:element>                    
                                        </xsl:for-each>
                                     </xsl:variable>                                     
                                     <xsl:variable name="numbers">
                                         <xsl:for-each select="distinct-values($weightsMatrix/cml:root/cml:value/@index)">
                                             <xsl:element name="cml:number">
                                                 <xsl:attribute name="value" select="."/>
                                             </xsl:element>                                        
                                         </xsl:for-each>                
                                     </xsl:variable>                                     
                                     <xsl:variable name="confNum">               
                                         <xsl:for-each select="$numbers/cml:number">
                                             <xsl:sort select="number(./@value)"/>
                                             <xsl:element name="cml:number">
                                                 <xsl:attribute name="value" select="./@value"/>
                                             </xsl:element>                                        
                                         </xsl:for-each>
                                     </xsl:variable>
                                     
                                     <!-- Render results per block -->
                                     <div id="waveFunctionWeights_{$waveFuncWeights/cml:scalar[@dictRef='o:blocknum']}" class="row waveFunctionWeightsDiv">
                                         <div class="col-md-{$colWidth} col-sm-12">
                                             <h4 style="margin-top:2em">Block: <xsl:value-of select="$waveFuncWeights/cml:scalar[@dictRef='o:blocknum']"/> , multiplicity: <xsl:value-of select="$waveFuncWeights/cml:scalar[@dictRef='cc:multiplicity']"/></h4>                                             
                                             <table class="display" id="waveFuncWeightsT-{generate-id($waveFuncWeights)}">
                                                 <thead>
                                                     <xsl:choose>
                                                         <xsl:when test="count($weightsMatrix/cml:root) = 1">
                                                             <tr>
                                                                 <th>Conf</th>
                                                                 <th></th>
                                                                 <th>1</th>
                                                             </tr>
                                                         </xsl:when>
                                                         <xsl:otherwise>
                                                             <tr>
                                                                 <th rowspan="2">Conf</th>
                                                                 <th rowspan="2"></th>
                                                                 <th colspan="{count($weightsMatrix/cml:root)}" style="text-align:center">Roots</th>                                                  
                                                             </tr>
                                                             <tr>
                                                                 <xsl:for-each select="0 to count($weightsMatrix/cml:root)-1">                                                        
                                                                     <th><xsl:value-of select="."/></th>    
                                                                 </xsl:for-each>
                                                             </tr>
                                                         </xsl:otherwise>
                                                     </xsl:choose>
                                                 </thead>
                                             </table>       
                                             <script type="text/javascript">
                                                 $(document).ready(function(){
                                                 $("table#waveFuncWeightsT-<xsl:value-of select="generate-id($waveFuncWeights)"/>").dataTable({
                                                    "aaData" : [
                                                    <xsl:for-each select="1 to count($confNum/cml:number)">
                                                        [
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <xsl:variable name="conf" select="$confNum/cml:number[$outerIndex]/@value"/>
                                                        '<xsl:value-of select="$conf"/>','<xsl:value-of select="($weightsMatrix//cml:value[@index=$conf])[1]/@conf"/>',
                                                        <xsl:for-each select="0 to count($weightsMatrix/cml:root)-1">
                                                            <xsl:variable name="innerIndex" select="."/>
                                                            
                                                            <xsl:variable name="root" select="$weightsMatrix/cml:root[@number=$innerIndex]"/>
                                                            <xsl:variable name="weight" select="$root/cml:value[@index=$conf]"/>
                                                            '<xsl:value-of select="if(exists($weight)) then $weight/@weight else '-'"/>'<xsl:if test="$innerIndex &lt; count($weightsMatrix/cml:root)-1">,</xsl:if>
                                                        </xsl:for-each>
                                                        ],
                                                    </xsl:for-each>
                                                    ['Total','',
                                                    <xsl:for-each select="1 to count($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'])">
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <xsl:variable name="coef" select="$waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'][$outerIndex]"/>                                            
                                                        <xsl:variable name="partials">
                                                            <xsl:for-each select="tokenize($coef/cml:array[@dictRef='o:weight'],'\s+')">
                                                                <xsl:element name="cml:value">
                                                                    <xsl:attribute name="value" select="number(.)"/>
                                                                </xsl:element>
                                                            </xsl:for-each>
                                                        </xsl:variable>
                                                        <xsl:value-of select="round-half-to-even(sum($partials/cml:value/@value),3)"/>
                                                        <xsl:if test="$outerIndex &lt; count($waveFuncWeights/cml:module[@cmlx:templateRef='ci.coefficients'])">,</xsl:if>
                                                    </xsl:for-each>
                                                    ]
                                                  ],
                                                 "aoColumns" : [
                                                 { "sClass": "left" }, { "sClass" : "left"}, <xsl:for-each select="0 to count($weightsMatrix/cml:root)-1"><xsl:variable name="outerIndex" select="."/>{ "sClass": "right" }<xsl:if test="$outerIndex &lt; count($weightsMatrix/cml:root)">,</xsl:if></xsl:for-each>                                    
                                                 ],
                                                 "bFilter": false,
                                                 "bPaginate": false,
                                                 "bSort": false,
                                                 "bInfo": false                                        
                                                 });
                                                 });                  
                                             </script> 
                                         </div>
                                     </div>
                                 </xsl:for-each> 
                                
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        var waveFuncButton = $(".btn-wavefunction-block")[0];
                                        <xsl:variable name="block" select="($waveFuncWeightsArray)[1]/cml:scalar[@dictRef='o:blocknum']"/>
                                        displayWaveFunctionWeightsTable(waveFuncButton, <xsl:value-of select="$block"/>);
                                    });

                                    function displayWaveFunctionWeightsTable(button, index) {
                                        $(".btn-wavefunction-block").removeClass("btn-primary");
                                        $(button).addClass("btn-primary");
                                    
                                    
                                        $(".waveFunctionWeightsDiv").hide();                                       
                                        $("#waveFunctionWeights_" + index).show();
                                        oTable = $('#waveFunctionWeights_' + index +' table').dataTable();;
                                        $(oTable).css({ width: $(oTable).parent().width() });
                                        oTable.fnAdjustColumnSizing();                                        
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
         
    <xsl:template name="finalCASPT2Energies">
        <xsl:variable name="casscfstates" select="(.//cml:module[@cmlx:templateRef='casscf'])[last()]//cml:module[@cmlx:templateRef='casscfstates']"/>
        <xsl:variable name="nevpt2" select=".//cml:module[@cmlx:templateRef='nevpt2']/cml:module[@cmlx:templateRef='nevpt2results']"/>
        
        <xsl:if test="exists($casscfstates)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#finalCASPT2Energies-{generate-id($casscfstates[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Final results CASSCF
                        <xsl:if test="exists($nevpt2)">
                            <xsl:text> / </xsl:text>
                            <xsl:variable name="nevpt2type" select=".//cml:module[@cmlx:templateRef='cisetup']/cml:module[@cmlx:templateRef='nevpt2settings']/cml:scalar[@dictRef='o:nevpt2type']"></xsl:variable>
                            <xsl:choose>
                                <xsl:when test="upper-case($nevpt2type) = 'SC-NEVPT2'">SC-NEVPT2</xsl:when>
                                <xsl:when test="upper-case($nevpt2type) = 'FIC-NEVPT2'">PC-NEVPT2</xsl:when>
                                <xsl:otherwise>NEVPT2</xsl:otherwise>
                            </xsl:choose>                            
                        </xsl:if>                        
                    </h4>
                </div>            
                <div id="finalCASPT2Energies-{generate-id($casscfstates[1])}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">                            
                            <div class="col-md-{if(exists($nevpt2)) then '12' else '6'} col-sm-12">
                                    <div id="finalCASPT2Energies_{generate-id($casscfstates[1])}" class="row finalCASPT2EnergiesDiv">
                                        <div class="col-md-12">                                           
                                            <table class="display" id="finalCASPT2EnergiesT-{generate-id($casscfstates[1])}">
                                                <thead>
                                                    <tr>
                                                        <th style="text-align:left">Root</th>
                                                        <th style="text-align:left">Multiplicity</th>
                                                        <th class="text-right">CASSCF (au)</th>
                                                        <th class="text-right">∆E (eV)</th>
                                                        <th class="text-right">∆E (cm-1)</th>
                                                        <xsl:if test="exists($nevpt2)">
                                                            <th class="text-right">NEVPT2 (au)</th>
                                                            <th class="text-right">∆E (eV)</th>
                                                            <th class="text-right">∆E (cm-1)</th>
                                                        </xsl:if>
                                                    </tr>
                                                </thead>
                                                <tbody>                                                
                                                </tbody>
                                            </table>     
                                        </div>
                                    </div>
                                    
                                    <xsl:variable name="ciStep" select=".//cml:module[@cmlx:templateRef='casscf']/cml:module[@cmlx:templateRef='cisetup']/cml:module[@cmlx:templateRef='cistep']"/>
                                    <xsl:variable name="minEnergy" select="min($casscfstates//cml:scalar[@dictRef='o:totalener'])"/>
                                    <!-- Convert string array to number nodes to allow min() function to work properly-->
                                    <xsl:variable name="nevPt2Energies" as="element()*">
                                        <xsl:for-each select="$nevpt2//cml:array[@dictRef='o:totalener']">
                                            <xsl:for-each select="tokenize(.,'\s+')">
                                                <Item><xsl:value-of select="number(.)"/></Item>
                                            </xsl:for-each>
                                        </xsl:for-each>                                   
                                    </xsl:variable>
                                    <xsl:variable name="minNevpt2" select="min($nevPt2Energies)" />
                                    
                                                           
                                    
                                    <script type="text/javascript">
                                        var caspt2Energies = [ 
                                        <xsl:for-each select="$casscfstates">
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            <xsl:variable name="casscfstate" select="." />
                                                                                                                                    
                                            <xsl:variable name="multiplicity" select="$casscfstate/cml:scalar[@dictRef='cc:multiplicity']" />
                                            <xsl:variable name="blockNum" select="$casscfstate/cml:scalar[@dictRef='o:blocknum']" />
                                            <xsl:variable name="energies" select="$casscfstate/cml:module[@cmlx:templateRef='ci.coefficients']/cml:scalar[@dictRef='o:totalener']/text()"/>
                                            <xsl:variable name="rootNumber" select="$casscfstate/cml:module[@cmlx:templateRef='ci.coefficients']/cml:scalar[@dictRef='o:rootnumber']/text()"/>
                                            
                                            
                                            
                                                                               
                                            <xsl:variable name="nevpt2block" select="$nevpt2/module[@cmlx:templateRef='rootenergies']/module[@cmlx:templateRef='block' and child::scalar[@dictRef='o:blocknum' and text() = $blockNum]]" />
                                            <xsl:variable name="nevpt2Energies">
                                                <xsl:variable name="multiplicity" select="tokenize($nevpt2block/cml:array[@dictRef='o:blockmult'],'\s+')" />
                                                <xsl:variable name="root" select="tokenize($nevpt2block/cml:array[@dictRef='o:rootnumber'],'\s+')" />
                                                <xsl:variable name="energies" select="tokenize($nevpt2block/cml:array[@dictRef='o:totalener'],'\s+')" />
                                                <xsl:for-each select="$energies">
                                                    <xsl:variable name="energy" select="."/>
                                                    <xsl:variable name="outerIndex" select="position()"/>
                                                    <xsl:element name="cml:item" namespace="http://www.xml-cml.org/schema">
                                                        <xsl:attribute name="multiplicity" select="$multiplicity[$outerIndex]"></xsl:attribute>
                                                        <xsl:attribute name="root" select="$root[$outerIndex]"></xsl:attribute>
                                                        <xsl:value-of select="$energy"/>
                                                    </xsl:element>
                                                </xsl:for-each>        
                                            </xsl:variable>        
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                            <xsl:for-each select="1 to count($energies)">
                                                <xsl:variable name="innerIndex" select="."/>
                                                <xsl:variable name="difference" select="number($energies[$innerIndex]) - number($minEnergy)"/>
                                                <xsl:variable name="difference2"  select="number($nevpt2Energies/cml:item[@multiplicity=$multiplicity and @root=$rootNumber[$innerIndex]]) - number($minNevpt2)" />                                                                                
                                                [   <xsl:value-of select="$rootNumber[$innerIndex]"/>, 
                                                <xsl:value-of select="$multiplicity"/>,
                                                '<xsl:value-of select="$energies[$innerIndex]"/>', 
                                                <xsl:choose>
                                                    <xsl:when test="string($difference) = 'NaN'">'0.00'</xsl:when>
                                                    <xsl:otherwise>
                                                        '<xsl:value-of select="format-number(number($difference) * $AU_TO_EV,'0.00')" />'
                                                    </xsl:otherwise>
                                                </xsl:choose>,
                                                <xsl:choose>
                                                    <xsl:when test="string($difference) = 'NaN'">'0'</xsl:when>
                                                    <xsl:otherwise>
                                                        '<xsl:value-of select="format-number(number($difference) * $AU_TO_CM_1,'####0')"/>'
                                                    </xsl:otherwise>
                                                </xsl:choose>,                                        
                                                <xsl:if test="exists($nevpt2)">
                                                    '<xsl:value-of select="$nevpt2Energies/cml:item[@multiplicity=$multiplicity and @root=$rootNumber[$innerIndex]]"/>',
                                                    <xsl:choose>
                                                        <xsl:when test="string($difference2) = 'NaN'">0.00</xsl:when>
                                                        <xsl:otherwise>
                                                            '<xsl:value-of select="format-number(number($difference2) * $AU_TO_EV,'0.00')"/>'
                                                        </xsl:otherwise>
                                                    </xsl:choose>,
                                                    <xsl:choose>
                                                        <xsl:when test="string($difference2) = 'NaN'">0</xsl:when>
                                                        <xsl:otherwise>
                                                            '<xsl:value-of select="format-number(number($difference2) * $AU_TO_CM_1,'####0')"/>'
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </xsl:if>
                                                ]<xsl:if test="$innerIndex &lt; count($energies)">,</xsl:if>
                                            </xsl:for-each>
                                            <xsl:if test="$outerIndex &lt; count($casscfstates)">,</xsl:if>                                   
                                        </xsl:for-each>
                                        ];
                                        
                                        $(document).ready(function(){
                                        caspt2Energies.sort(sortByCaspt2Energies);
                                        $("table#finalCASPT2EnergiesT-<xsl:value-of select="generate-id($casscfstates[1])"/>").dataTable({
                                        "aaData": caspt2Energies,
                                        "aoColumns" : [ { "sClass": "left" }, { "sClass" : "left"},{ "sClass": "right" },{ "sClass": "right" },{ "sClass": "right" }<xsl:if test="exists($nevpt2)">,{ "sClass": "right" },{ "sClass": "right" },{ "sClass": "right" }</xsl:if>],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                        
                                        });
                                        
                                        });                                     
                                        
                                        function sortByCaspt2Energies(a, b){
                                        if (Number(a[2]) === Number(b[2])) {
                                        return 0;
                                        } else {
                                        return (Number(a[2]) &lt; Number(b[2])) ? -1 : 1;
                                        }                                     
                                        }
                                    </script> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>
    
    
    <!-- Superseeded by finalCASPT2Energies, kept as reference -->
    <xsl:template name="finalCASPT2EnergiesOld">
        <xsl:variable name="casscfstates" select="(.//cml:module[@cmlx:templateRef='casscf'])[last()]//cml:module[@cmlx:templateRef='casscfstates']"/>
        <xsl:variable name="nevpt2" select=".//cml:module[@cmlx:templateRef='nevpt2']/cml:module[@cmlx:templateRef='nevpt2results']"/>
        
        <xsl:if test="exists($casscfstates)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#finalCASPT2Energies-{generate-id($casscfstates[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Final results CASSCF
                        <xsl:if test="exists($nevpt2)">
                            <xsl:text> / </xsl:text>
                            <xsl:variable name="nevpt2type" select=".//cml:module[@cmlx:templateRef='cisetup']/cml:module[@cmlx:templateRef='nevpt2settings']/cml:scalar[@dictRef='o:nevpt2type']"></xsl:variable>
                            <xsl:choose>
                                <xsl:when test="upper-case($nevpt2type) = 'SC-NEVPT2'">SC-NEVPT2</xsl:when>
                                <xsl:when test="upper-case($nevpt2type) = 'FIC-NEVPT2'">PC-NEVPT2</xsl:when>
                                <xsl:otherwise>NEVPT2</xsl:otherwise>
                            </xsl:choose>                            
                        </xsl:if>                        
                    </h4>
                </div>            
                <div id="finalCASPT2Energies-{generate-id($casscfstates[1])}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-{if(exists($nevpt2)) then '12' else '6'} col-sm-12">
                                <div class="button-group">
                                    Multiplicity : 
                                    <xsl:for-each select="$casscfstates">
                                        <xsl:variable name="casscfstate" select="." />
                                        <xsl:variable name="multiplicity" select="$casscfstate/cml:scalar[@dictRef='cc:multiplicity']" />                                        
                                        <xsl:variable name="blockNum" select="$casscfstate/cml:scalar[@dictRef='o:blocknum']" />
                                        <button class="btn btn-default btn-caspt2-button" type="button" onclick="javascript:displayFinalCASPT2EnergiesTable(this, {$blockNum})">
                                            <xsl:value-of select="$multiplicity"/>                                        
                                        </button>
                                    </xsl:for-each>
                                </div>
                                
                                <xsl:variable name="ciStep" select=".//cml:module[@cmlx:templateRef='casscf']/cml:module[@cmlx:templateRef='cisetup']/cml:module[@cmlx:templateRef='cistep']"/>
                                <xsl:for-each select="$casscfstates">
                                    <xsl:variable name="casscfstate" select="." />
                                    <xsl:variable name="blockNum" select="$casscfstate/cml:scalar[@dictRef='o:blocknum']" />
                                    <xsl:variable name="energies" select="$casscfstate/cml:module[@cmlx:templateRef='ci.coefficients']/cml:scalar[@dictRef='o:totalener']/text()"/>
                                    <xsl:variable name="energiesnevpt2" select="tokenize($nevpt2
                                        /cml:module[@cmlx:templateRef='rootenergies']
                                        /cml:module[@cmlx:templateRef='block' and child::cml:scalar[@dictRef='o:blocknum' and text() = $blockNum]]
                                        /cml:array[@dictRef='o:totalener'],'\s+')" /> 
                                    
                                    <div id="finalCASPT2Energies_{$blockNum}" class="row finalCASPT2EnergiesDiv">
                                        <div class="col-md-12">
                                            <table class="display" id="finalCASPT2EnergiesT-{$blockNum}">
                                                <thead>
                                                    <tr>
                                                        <th>Root</th>
                                                        <th class="text-right">CASSCF (au)</th>
                                                        <th class="text-right">∆E (eV)</th>
                                                        <th class="text-right">∆E (cm-1)</th>
                                                        <xsl:if test="exists($nevpt2)">
                                                            <th class="text-right">NEVPT2 (au)</th>
                                                            <th class="text-right">∆E (eV)</th>
                                                            <th class="text-right">∆E (cm-1)</th>
                                                        </xsl:if>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <xsl:for-each select="1 to count($energies)">
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <xsl:variable name="difference"
                                                            select="number($energies[$outerIndex]) - number($energies[1])"/>
                                                        <xsl:variable name="difference2" 
                                                            select="number($energiesnevpt2[$outerIndex]) - number($energiesnevpt2[1])" />                                                        
                                                        <tr>
                                                            <td>
                                                                <xsl:value-of select="$outerIndex"/>
                                                            </td>
                                                            <td class="datacell text-right">
                                                                <xsl:value-of select="$energies[$outerIndex]"/>
                                                            </td>
                                                            <td class="datacell text-right">
                                                                <xsl:choose>
                                                                    <xsl:when test="string($difference) = 'NaN'">0.00</xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of
                                                                            select="format-number(number($difference) * $AU_TO_EV,'0.00')"
                                                                        />
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                            <td class="datacell text-right">
                                                                <xsl:choose>
                                                                    <xsl:when test="string($difference) = 'NaN'">0</xsl:when>
                                                                    <xsl:otherwise>
                                                                        <xsl:value-of select="format-number(number($difference) * $AU_TO_CM_1,'####0')"/>
                                                                    </xsl:otherwise>
                                                                </xsl:choose>
                                                            </td>
                                                            <xsl:if test="exists($nevpt2)">
                                                                <td class="datacell text-right">
                                                                    <xsl:value-of select="$energiesnevpt2[$outerIndex]"/>
                                                                </td>
                                                                <td class="datacell text-right">
                                                                    <xsl:choose>
                                                                        <xsl:when test="string($difference2) = 'NaN'">0.00</xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="format-number(number($difference2) * $AU_TO_EV,'0.00')"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>
                                                                </td>
                                                                <td class="datacell text-right">
                                                                    <xsl:choose>
                                                                        <xsl:when test="string($difference2) = 'NaN'">0</xsl:when>
                                                                        <xsl:otherwise>
                                                                            <xsl:value-of select="format-number(number($difference2) * $AU_TO_CM_1,'####0')"/>
                                                                        </xsl:otherwise>
                                                                    </xsl:choose>
                                                                </td>                                                                                                                            
                                                            </xsl:if>
                                                        </tr>
                                                    </xsl:for-each>                                       
                                                </tbody>
                                            </table>     
                                            <script type="text/javascript">
                                                $(document).ready(function(){
                                                $("table#finalCASPT2EnergiesT-<xsl:value-of select="$blockNum"/>").dataTable({
                                                "bFilter": false,
                                                "bPaginate": false,
                                                "bSort": false,
                                                "bInfo": false                                        
                                                });
                                                });                                       
                                            </script>
                                        </div>
                                    </div>
                                    
                                    
                                </xsl:for-each>
                                
                                <script type="text/javascript">
                                    $(document).ready(function(){
                                    <xsl:variable name="block" select="$casscfstates/cml:scalar[@dictRef='o:blocknum']"/>
                                    var caspt2Button = $(".btn-caspt2-block")[0];
                                    displayFinalCASPT2EnergiesTable(caspt2Button, <xsl:value-of select="$block"/>);
                                    });                                    
                                    
                                    function displayFinalCASPT2EnergiesTable(this, index) {                                                                                                            
                                    $(".finalCASPT2EnergiesDiv").hide();                                       
                                    $("#finalCASPT2Energies_" + index).show();
                                    oTable = $('#finalCASPT2Energies_' + index +' table').dataTable();;
                                    $(oTable).css({ width: $(oTable).parent().width() });
                                    oTable.fnAdjustColumnSizing();                                        
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
    </xsl:template>

    <xsl:template name="orbitalSpecs">
        <xsl:variable name="orbSpecs" select="(.//cml:module[@cmlx:templateRef='nevpt2']/cml:module[@cmlx:templateRef='orbitalranges'])[last()]"/>
        <xsl:variable name="nevpt2" select=".//cml:module[@cmlx:templateRef='nevpt2']/cml:module[@cmlx:templateRef='nevpt2results']"/>
        <xsl:if test="exists($orbSpecs)">
            <xsl:variable name="inactiveorb" select="$orbSpecs/cml:scalar[@dictRef='o:inactiveorb']"/>
            <xsl:variable name="activeorb" select="$orbSpecs/cml:scalar[@dictRef='o:activeorb']"/>            
            <xsl:variable name="frozenorb" select="$orbSpecs/cml:scalar[@dictRef='o:frozenorb']"/>
            <xsl:variable name="externalorb" select="$orbSpecs/cml:scalar[@dictRef='o:externalorb']"/>
            <xsl:variable name="deletedorb" select="$orbSpecs/cml:scalar[@dictRef='o:deletedorb']"/>            
            <xsl:variable name="totalorb" select=".//cml:module[@cmlx:templateRef='nevpt2']/cml:module[@cmlx:templateRef='systemsettings']/cml:scalar[@dictRef='o:orbno']"/>            
            
            <xsl:variable name="basisno">
                <xsl:choose>
                    <xsl:when test="($totalorb - $deletedorb) &gt;= 0 ">
                        <xsl:value-of select="(.//cml:module[@name='General Settings']//cml:scalar[@dictRef='cc:parameter' and text() = 'Dim']/following-sibling::cml:scalar[@dictRef='cc:value'])[1]"/>         
                    </xsl:when>                    
                </xsl:choose>
            </xsl:variable>
         
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#orbSpecs-{generate-id($orbSpecs)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Orbital specifications                        
                        <xsl:if test="exists($nevpt2)">
                            <xsl:text> / </xsl:text>
                            <xsl:variable name="nevpt2type" select=".//cml:module[@cmlx:templateRef='cisetup']/cml:module[@cmlx:templateRef='nevpt2settings']/cml:scalar[@dictRef='o:nevpt2type']"></xsl:variable>
                            <xsl:choose>
                                <xsl:when test="upper-case($nevpt2type) = 'SC-NEVPT2'">SC-NEVPT2</xsl:when>
                                <xsl:when test="upper-case($nevpt2type) = 'FIC-NEVPT2'">PC-NEVPT2</xsl:when>
                                <xsl:otherwise>NEVPT2</xsl:otherwise>
                            </xsl:choose>                            
                        </xsl:if>
                    
                    </h4>
                </div>            
                <div id="orbSpecs-{generate-id($orbSpecs)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-6 col-sm-12">
                                <table class="display" id="orbSpecsT-{generate-id($orbSpecs)}">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                        </tr>                                        
                                    </thead>
                                    <tbody>
                                        <xsl:if test="exists($frozenorb)">
                                            <tr>
                                                <td>Frozen orbitals</td>
                                                <td><xsl:value-of select="$frozenorb"/></td>
                                            </tr>                                              
                                        </xsl:if>
                                        <xsl:if test="exists($inactiveorb)">
                                            <tr>
                                                <td>Inactive orbitals</td>
                                                <td><xsl:value-of select="$inactiveorb"/></td>
                                            </tr>                                                                                          
                                        </xsl:if>                                        
                                        <xsl:if test="exists($activeorb)">
                                            <tr>
                                                <td>Active orbitals</td>
                                                <td>
                                                    <xsl:choose>
                                                        <xsl:when test="$hasOrbitals">
                                                            <div class="btn-group btn-link">
                                                                <span class="dropdown-toggle " type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                    <xsl:value-of select="$activeorb"/>
                                                                </span>                                                                    
                                                                <div class="dropdown-menu" >
                                                                    <xsl:variable name="activeOrbitalStart" select="number($frozenorb) + number($inactiveorb) "/>
                                                                    <xsl:variable name="activeOrbitalEnd" select="$activeOrbitalStart + number($activeorb) - 1"/>
                                                                    <xsl:for-each select="xs:integer($activeOrbitalStart) to xs:integer($activeOrbitalEnd)">
                                                                        <xsl:variable name="outerIndex" select="."/>
                                                                        <a class="dropdown-item" href="javascript:displayMoldenOrbital('{$outerIndex}a')"><xsl:value-of select="$outerIndex"/></a>    
                                                                    </xsl:for-each>
                                                                </div>
                                                            </div>
                                                        </xsl:when>
                                                        <xsl:otherwise>
                                                            <xsl:value-of select="$activeorb"/>    
                                                        </xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                            </tr>
                                        </xsl:if>
                                        <xsl:if test="exists($externalorb)">
                                            <tr>
                                                <td>External orbitals</td>
                                                <td><xsl:value-of select="$externalorb"/></td>
                                            </tr>                                              
                                        </xsl:if>                                        
                                        <xsl:if test="exists($deletedorb)">
                                            <tr>
                                                <td>Deleted orbitals</td>
                                                <td>
                                                    <xsl:choose>
                                                        <xsl:when test="($totalorb - $deletedorb) &gt;= 0"><xsl:value-of select="$totalorb - $deletedorb + 1"/></xsl:when>
                                                        <xsl:otherwise>0</xsl:otherwise>
                                                    </xsl:choose>
                                                </td>
                                            </tr>                                                                                          
                                        </xsl:if>
                                        <xsl:if test="exists($totalorb)">
                                            <tr>
                                                <td>Total number of orbitals</td>
                                                <td><xsl:value-of select="$totalorb"/></td>
                                            </tr>                                             
                                        </xsl:if>
                                        <xsl:if test="exists($basisno) and $basisno!= '' and not(exists($totalorb))">
                                            <tr>
                                                <td>Number of basis functions</td>
                                                <td><xsl:value-of select="$basisno"/></td>
                                            </tr>                                                                                         
                                        </xsl:if>
                                    </tbody>
                                </table>
                                
                                <script type="text/javascript">
                                    $(document).ready(function() {
                                        $('table#orbSpecsT-<xsl:value-of select="generate-id($orbSpecs)"/>').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                               
                                        });
                                    });                                                                        
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </xsl:if>   
    </xsl:template>
   
   
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>
        </div>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
    
</xsl:stylesheet>
