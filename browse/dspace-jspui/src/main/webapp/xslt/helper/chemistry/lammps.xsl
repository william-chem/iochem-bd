<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:l="http://www.iochem-bd.org/dictionary/lammps/"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:variable name="l:npt.regex">.*\s+npt.*</xsl:variable>
    <xsl:variable name="l:nvt.regex">.*\s+(nvt.*|langevin.*|temp/.*)\s+.*</xsl:variable>
    <xsl:variable name="l:nvt.pairstyle.regex">.*\s+(dpd/tstat.*)\s+.*</xsl:variable>
    <xsl:variable name="l:nph.regex">.*\s+nph\s+.*</xsl:variable>
    <xsl:variable name="l:nve.regex">.*\s+nve\s*.*</xsl:variable>
    
    <xsl:variable name="l:minimize">Geometry optimization</xsl:variable>
    <xsl:variable name="l:md">Molecular Dynamics</xsl:variable>
    <xsl:variable name="l:npt">NPT</xsl:variable>
    <xsl:variable name="l:nvt">NVT</xsl:variable>
    <xsl:variable name="l:nph">NPH</xsl:variable>
    <xsl:variable name="l:nve">NVE</xsl:variable>
    
    
    <!-- thermostat / barostat types -->
    <xsl:variable name="l:thermostat.nosehoover">Nose-Hoover</xsl:variable>
    <xsl:variable name="l:thermostat.langevin">Langevin</xsl:variable>
    <xsl:variable name="l:thermostat.berendsen">Berendsen</xsl:variable>
    <xsl:variable name="l:thermostat.hd">Hamiltonian dynamics</xsl:variable>
    <xsl:variable name="l:thermostat.ld">Langevin dynamics</xsl:variable>
    <xsl:variable name="l:thermostat.rescaling">Rescaling</xsl:variable>
    <xsl:variable name="l:thermostat.dpd">Dissipative particle dynamics</xsl:variable>
    
    <xsl:variable name="l:barostat.nosehoover">Nose-Hoover</xsl:variable>
    <xsl:variable name="l:barostat.berendsen">Berendsen</xsl:variable>
    
    <!-- nvt parameters -->
    <xsl:variable name="l:nvt.nosehoover.regex">.*\s+nvt.*</xsl:variable>
    <xsl:variable name="l:nvt.langevin.regex">.*\s+langevin.*</xsl:variable>
    <xsl:variable name="l:nvt.berendsen.regex">.*\s+temp/berendsen.*</xsl:variable>
    <xsl:variable name="l:nvt.hd.regex">.*\s+temp/csvr.*</xsl:variable>
    <xsl:variable name="l:nvt.ld.regex">.*\s+temp/csld.*</xsl:variable>
    <xsl:variable name="l:nvt.rescaling.regex">.*\s+temp/rescale.*</xsl:variable>
    <xsl:variable name="l:nvt.dpd.regex">.*\s+dpd/tstat.*</xsl:variable>
   
    <!-- npt parameters -->    
    <xsl:variable name="l:npt.nosehoover.regex">.*\s+npt.*</xsl:variable>
    
    <!-- nph parameters -->
    <xsl:variable name="l:nph.nosehoover.regex">.*\s+nph.*</xsl:variable>
    <xsl:variable name="l:nph.berendsen.regex">.*\s+press/berendsen.*</xsl:variable>
       
    <xsl:function name="l:getMethod">
        <xsl:param name="setup"  />        
        <xsl:variable name="methods">            
                <xsl:if test="exists(l:getParameter($setup, 'minimize'))">
                    <xsl:value-of select="$l:minimize"/><xsl:value-of select="'|'"/>
                </xsl:if>            
            
                <xsl:for-each select="l:getParameter($setup, 'pair_style')">
                    <xsl:if test="matches(.,$l:nvt.pairstyle.regex)">
                        <xsl:value-of select="$l:nvt"/><xsl:value-of select="'|'"/>
                    </xsl:if>                                        
               </xsl:for-each>
                
                <xsl:for-each select="l:getParameter($setup, 'fix')">
                    <xsl:choose>
                        <xsl:when test="matches(., $l:npt.regex)"><xsl:value-of select="$l:npt"/><xsl:value-of select="'|'"/></xsl:when>
                        <xsl:when test="matches(., $l:nvt.regex)"><xsl:value-of select="$l:nvt"/><xsl:value-of select="'|'"/></xsl:when>
                        <xsl:when test="matches(., $l:nph.regex)"><xsl:value-of select="$l:nph"/><xsl:value-of select="'|'"/></xsl:when>
                        <xsl:when test="matches(., $l:nve.regex)"><xsl:value-of select="$l:nve"/><xsl:value-of select="'|'"/></xsl:when>
                    </xsl:choose>
                </xsl:for-each>
                               
        </xsl:variable>
        
        <xsl:value-of select="distinct-values(tokenize(substring($methods, 1, string-length($methods)-1), '\|'))"/>        
    </xsl:function>
    
    <xsl:function name="l:getCouplingMethods">
        <xsl:param name="setup"  />
        <xsl:variable name="cmethods">
            <xsl:for-each select="l:getParameter($setup, 'pair_style')">
                <xsl:if test="matches(.,$l:nvt.pairstyle.regex)">
                    <xsl:value-of select="concat('(T=', $l:thermostat.dpd,')|')"/>
                </xsl:if>                                        
            </xsl:for-each>
                        
            <xsl:for-each select="l:getParameter($setup, 'fix')">
                <!-- nvt -->
                <xsl:if test="matches(., $l:nvt.nosehoover.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.nosehoover,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nvt.langevin.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.langevin,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nvt.berendsen.regex)"><xsl:value-of select="concat('T=', $l:thermostat.berendsen,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nvt.hd.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.hd,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nvt.ld.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.ld,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nvt.rescaling.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.rescaling,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nvt.dpd.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.dpd,')|')"/></xsl:if>                                                
                <!-- npt -->
                <xsl:if test="matches(., $l:npt.nosehoover.regex)"><xsl:value-of select="concat('(T=', $l:thermostat.nosehoover,', P=', $l:barostat.nosehoover,')|')"/></xsl:if>
                <!-- nph -->                                
                <xsl:if test="matches(., $l:nph.nosehoover.regex)"><xsl:value-of select="concat('(P=', $l:barostat.nosehoover,')|')"/></xsl:if>
                <xsl:if test="matches(., $l:nph.berendsen.regex)"><xsl:value-of select="concat('(P=', $l:barostat.berendsen,')|')"/></xsl:if>                
            </xsl:for-each>
        </xsl:variable>                
        
        <xsl:value-of select="distinct-values(tokenize(substring($cmethods, 1, string-length($cmethods)-1), '\|'))"/>        
    </xsl:function>    
    
    <xsl:function name="l:getParameter">
        <xsl:param name="setup" />
        <xsl:param name="name" />                      
        <xsl:copy-of select="$setup/cml:parameter/cml:scalar[@dictRef='x:label'][text()=$name]/following-sibling::cml:scalar[@dictRef='x:value']"/>                
    </xsl:function>
    

    
    <xsl:function name="l:getPotential">
        <xsl:param name="pairstyle"/>        

        <xsl:variable name="potentials">
            <cml:potential id="none">none</cml:potential>
            <cml:potential id="hybrid">multiple styles of pairwise interactions</cml:potential>
            <cml:potential id="hybrid/overlay">multiple styles of superposed pairwise interactions</cml:potential>
            <cml:potential id="hybrid/scaled">multiple styles of scaled superposed pairwise interactions</cml:potential>
            <cml:potential id="zero">neighbor list but no interactions</cml:potential>
            <cml:potential id="adp">angular dependent potential (ADP) of Mishin</cml:potential>
            <cml:potential id="agni">AGNI machine-learning potential</cml:potential>
            <cml:potential id="airebo">AIREBO potential of Stuart</cml:potential>
            <cml:potential id="airebo/morse">AIREBO with Morse instead of LJ</cml:potential>
            <cml:potential id="atm">Axilrod-Teller-Muto potential</cml:potential>
            <cml:potential id="awpmd/cut">Antisymmetrized Wave Packet MD potential for atoms and electrons</cml:potential>
            <cml:potential id="beck">Beck potential</cml:potential>
            <cml:potential id="body/nparticle">interactions between body particles</cml:potential>
            <cml:potential id="body/rounded/polygon">granular-style 2d polygon potential</cml:potential>
            <cml:potential id="body/rounded/polyhedron">granular-style 3d polyhedron potential</cml:potential>
            <cml:potential id="bop">BOP potential of Pettifor</cml:potential>
            <cml:potential id="born">Born-Mayer-Huggins potential</cml:potential>
            <cml:potential id="born/coul/dsf">Born with damped-shifted-force model</cml:potential>
            <cml:potential id="born/coul/dsf/cs">Born with damped-shifted-force and core/shell model</cml:potential>
            <cml:potential id="born/coul/long">Born with long-range Coulomb</cml:potential>
            <cml:potential id="born/coul/long/cs">Born with long-range Coulomb and core/shell</cml:potential>
            <cml:potential id="born/coul/msm">Born with long-range MSM Coulomb</cml:potential>
            <cml:potential id="born/coul/wolf">Born with Wolf potential for Coulomb</cml:potential>
            <cml:potential id="born/coul/wolf/cs">Born with Wolf potential for Coulomb and core/shell model</cml:potential>
            <cml:potential id="brownian">Brownian potential for Fast Lubrication Dynamics</cml:potential>
            <cml:potential id="brownian/poly">Brownian potential for Fast Lubrication Dynamics with polydispersity</cml:potential>
            <cml:potential id="buck">Buckingham potential</cml:potential>
            <cml:potential id="buck/coul/cut">Buckingham with cutoff Coulomb</cml:potential>
            <cml:potential id="buck/coul/long">Buckingham with long-range Coulomb</cml:potential>
            <cml:potential id="buck/coul/long/cs">Buckingham with long-range Coulomb and core/shell</cml:potential>
            <cml:potential id="buck/coul/msm">Buckingham with long-range MSM Coulomb</cml:potential>
            <cml:potential id="buck/long/coul/long">long-range Buckingham with long-range Coulomb</cml:potential>
            <cml:potential id="buck/mdf">Buckingham with a taper function</cml:potential>
            <cml:potential id="buck6d/coul/gauss/dsf">dispersion-damped Buckingham with damped-shift-force model</cml:potential>
            <cml:potential id="buck6d/coul/gauss/long">dispersion-damped Buckingham with long-range Coulomb</cml:potential>
            <cml:potential id="colloid">integrated colloidal potential</cml:potential>
            <cml:potential id="comb">charge-optimized many-body (COMB) potential</cml:potential>
            <cml:potential id="comb3">charge-optimized many-body (COMB3) potential</cml:potential>
            <cml:potential id="cosine/squared">Cooke-Kremer-Deserno membrane model potential</cml:potential>
            <cml:potential id="coul/cut">cutoff Coulomb potential</cml:potential>
            <cml:potential id="coul/cut/dielectric">coul/cut/dielectric</cml:potential>
            <cml:potential id="coul/cut/global">cutoff Coulomb potential</cml:potential>
            <cml:potential id="coul/cut/soft">Coulomb potential with a soft core</cml:potential>
            <cml:potential id="coul/debye">cutoff Coulomb potential with Debye screening</cml:potential>
            <cml:potential id="coul/diel">Coulomb potential with dielectric permittivity</cml:potential>
            <cml:potential id="coul/dsf">Coulomb with damped-shifted-force model</cml:potential>
            <cml:potential id="coul/exclude">subtract Coulomb potential for excluded pairs</cml:potential>
            <cml:potential id="coul/long">long-range Coulomb potential</cml:potential>
            <cml:potential id="coul/long/cs">long-range Coulomb potential and core/shell</cml:potential>
            <cml:potential id="coul/long/dielectric">coul/long/dielectric</cml:potential>
            <cml:potential id="coul/long/soft">long-range Coulomb potential with a soft core</cml:potential>
            <cml:potential id="coul/msm">long-range MSM Coulomb</cml:potential>
            <cml:potential id="coul/slater/cut">smeared out Coulomb</cml:potential>
            <cml:potential id="coul/slater/long">long-range smeared out Coulomb</cml:potential>
            <cml:potential id="coul/shield">Coulomb for boron nitride for use with ilp/graphene/hbn potential</cml:potential>
            <cml:potential id="coul/streitz">Coulomb via Streitz/Mintmire Slater orbitals</cml:potential>
            <cml:potential id="coul/tt">damped charge-dipole Coulomb for Drude dipoles</cml:potential>
            <cml:potential id="coul/wolf">Coulomb via Wolf potential</cml:potential>
            <cml:potential id="coul/wolf/cs">Coulomb via Wolf potential with core/shell adjustments</cml:potential>
            <cml:potential id="deepmd">Deep Potential</cml:potential>
            <cml:potential id="dpd">dissipative particle dynamics (DPD)</cml:potential>
            <cml:potential id="dpd/ext">generalized force field for DPD</cml:potential>
            <cml:potential id="dpd/ext/tstat">pairwise DPD thermostatting with generalized force field</cml:potential>
            <cml:potential id="dpd/fdt">DPD for constant temperature and pressure</cml:potential>
            <cml:potential id="dpd/fdt/energy">DPD for constant energy and enthalpy</cml:potential>
            <cml:potential id="dpd/tstat">pairwise DPD thermostatting</cml:potential>
            <cml:potential id="dsmc">Direct Simulation Monte Carlo (DSMC)</cml:potential>
            <cml:potential id="e3b">Explicit-three body (E3B) water model</cml:potential>
            <cml:potential id="drip">Dihedral-angle-corrected registry-dependent interlayer potential (DRIP)</cml:potential>
            <cml:potential id="eam">embedded atom method (EAM)</cml:potential>
            <cml:potential id="eam/alloy">alloy EAM</cml:potential>
            <cml:potential id="eam/cd">concentration-dependent EAM</cml:potential>
            <cml:potential id="eam/cd/old">older two-site model for concentration-dependent EAM</cml:potential>
            <cml:potential id="eam/fs">Finnis-Sinclair EAM</cml:potential>
            <cml:potential id="eam/he">Finnis-Sinclair EAM modified for Helium in metals</cml:potential>
            <cml:potential id="edip">three-body EDIP potential</cml:potential>
            <cml:potential id="edip/multi">multi-element EDIP potential</cml:potential>
            <cml:potential id="edpd">eDPD particle interactions</cml:potential>
            <cml:potential id="eff/cut">electron force field with a cutoff</cml:potential>
            <cml:potential id="eim">embedded ion method (EIM)</cml:potential>
            <cml:potential id="exp6/rx">reactive DPD potential</cml:potential>
            <cml:potential id="extep">extended Tersoff potential</cml:potential>
            <cml:potential id="gauss">Gaussian potential</cml:potential>
            <cml:potential id="gauss/cut">generalized Gaussian potential</cml:potential>
            <cml:potential id="gayberne">Gay-Berne ellipsoidal potential</cml:potential>
            <cml:potential id="granular">Generalized granular potential</cml:potential>
            <cml:potential id="gran/hertz/history">granular potential with Hertzian interactions</cml:potential>
            <cml:potential id="gran/hooke">granular potential with history effects</cml:potential>
            <cml:potential id="gran/hooke/history">granular potential without history effects</cml:potential>
            <cml:potential id="gw">Gao-Weber potential</cml:potential>
            <cml:potential id="gw/zbl">Gao-Weber potential with a repulsive ZBL core</cml:potential>
            <cml:potential id="harmonic/cut">repulsive-only harmonic potential</cml:potential>
            <cml:potential id="hbond/dreiding/lj">DREIDING hydrogen bonding LJ potential</cml:potential>
            <cml:potential id="hbond/dreiding/morse">DREIDING hydrogen bonding Morse potential</cml:potential>
            <cml:potential id="hdnnp">High-dimensional neural network potential</cml:potential>
            <cml:potential id="ilp/graphene/hbn">registry-dependent interlayer potential (ILP)</cml:potential>
            <cml:potential id="ilp/tmd">interlayer potential (ILP) potential for transition metal dichalcogenides (TMD)</cml:potential>
            <cml:potential id="kim">interface to potentials provided by KIM project</cml:potential>
            <cml:potential id="kolmogorov/crespi/full">Kolmogorov-Crespi (KC) potential with no simplifications</cml:potential>
            <cml:potential id="kolmogorov/crespi/z">Kolmogorov-Crespi (KC) potential with normals along z-axis</cml:potential>
            <cml:potential id="lcbop">long-range bond-order potential (LCBOP)</cml:potential>
            <cml:potential id="lebedeva/z">Lebedeva interlayer potential for graphene with normals along z-axis</cml:potential>
            <cml:potential id="lennard/mdf">LJ potential in A/B form with a taper function</cml:potential>
            <cml:potential id="line/lj">LJ potential between line segments</cml:potential>
            <cml:potential id="list">potential between pairs of atoms explicitly listed in an input file</cml:potential>
            <cml:potential id="lj/charmm/coul/charmm">CHARMM potential with cutoff Coulomb</cml:potential>
            <cml:potential id="lj/charmm/coul/charmm/implicit">CHARMM for implicit solvent</cml:potential>
            <cml:potential id="lj/charmm/coul/long">CHARMM with long-range Coulomb</cml:potential>
            <cml:potential id="lj/charmm/coul/long/soft">CHARMM with long-range Coulomb and a soft core</cml:potential>
            <cml:potential id="lj/charmm/coul/msm">CHARMM with long-range MSM Coulomb</cml:potential>
            <cml:potential id="lj/charmmfsw/coul/charmmfsh">CHARMM with force switching and shifting</cml:potential>
            <cml:potential id="lj/charmmfsw/coul/long">CHARMM with force switching and long-rnage Coulomb</cml:potential>
            <cml:potential id="lj/class2">COMPASS (class 2) force field without Coulomb</cml:potential>
            <cml:potential id="lj/class2/coul/cut">COMPASS with cutoff Coulomb</cml:potential>
            <cml:potential id="lj/class2/coul/cut/soft">COMPASS with cutoff Coulomb with a soft core</cml:potential>
            <cml:potential id="lj/class2/coul/long">COMPASS with long-range Coulomb</cml:potential>
            <cml:potential id="lj/class2/coul/long/cs">COMPASS with long-range Coulomb with core/shell adjustments</cml:potential>
            <cml:potential id="lj/class2/coul/long/soft">COMPASS with long-range Coulomb with a soft core</cml:potential>
            <cml:potential id="lj/class2/soft">COMPASS (class 2) force field with no Coulomb with a soft core</cml:potential>
            <cml:potential id="lj/cubic">LJ with cubic after inflection point</cml:potential>
            <cml:potential id="lj/cut">cutoff Lennard-Jones potential without Coulomb</cml:potential>
            <cml:potential id="lj/cut/coul/cut">LJ with cutoff Coulomb</cml:potential>
            <cml:potential id="lj/cut/coul/cut/dielectric">lj/cut/coul/cut/dielectric</cml:potential>
            <cml:potential id="lj/cut/coul/cut/soft">LJ with cutoff Coulomb with a soft core</cml:potential>
            <cml:potential id="lj/cut/coul/debye">LJ with Debye screening added to Coulomb</cml:potential>
            <cml:potential id="lj/cut/coul/debye/dielectric">lj/cut/coul/debye/dielectric</cml:potential>
            <cml:potential id="lj/cut/coul/dsf">LJ with Coulomb via damped shifted forces</cml:potential>
            <cml:potential id="lj/cut/coul/long">LJ with long-range Coulomb</cml:potential>
            <cml:potential id="lj/cut/coul/long/cs">LJ with long-range Coulomb with core/shell adjustments</cml:potential>
            <cml:potential id="lj/cut/coul/long/dielectric">lj/cut/coul/long/dielectric</cml:potential>
            <cml:potential id="lj/cut/coul/long/soft">LJ with long-range Coulomb with a soft core</cml:potential>
            <cml:potential id="lj/cut/coul/msm">LJ with long-range MSM Coulomb</cml:potential>
            <cml:potential id="lj/cut/coul/msm/dielectric">lj/cut/coul/msm/dielectric</cml:potential>
            <cml:potential id="lj/cut/coul/wolf">LJ with Coulomb via Wolf potential</cml:potential>
            <cml:potential id="lj/cut/dipole/cut">point dipoles with cutoff</cml:potential>
            <cml:potential id="lj/cut/dipole/long">point dipoles with long-range Ewald</cml:potential>
            <cml:potential id="lj/cut/soft">LJ with a soft core</cml:potential>
            <cml:potential id="lj/cut/thole/long">LJ with Coulomb with thole damping</cml:potential>
            <cml:potential id="lj/cut/tip4p/cut">LJ with cutoff Coulomb for TIP4P water</cml:potential>
            <cml:potential id="lj/cut/tip4p/long">LJ with long-range Coulomb for TIP4P water</cml:potential>
            <cml:potential id="lj/cut/tip4p/long/soft">LJ with cutoff Coulomb for TIP4P water with a soft core</cml:potential>
            <cml:potential id="lj/expand">Lennard-Jones for variable size particles</cml:potential>
            <cml:potential id="lj/expand/coul/long">Lennard-Jones for variable size particles with long-range Coulomb</cml:potential>
            <cml:potential id="lj/gromacs">GROMACS-style Lennard-Jones potential</cml:potential>
            <cml:potential id="lj/gromacs/coul/gromacs">GROMACS-style LJ and Coulomb potential</cml:potential>
            <cml:potential id="lj/long/coul/long">long-range LJ and long-range Coulomb</cml:potential>
            <cml:potential id="lj/long/coul/long/dielectric">lj/long/coul/long/dielectric</cml:potential>
            <cml:potential id="lj/long/dipole/long">long-range LJ and long-range point dipoles</cml:potential>
            <cml:potential id="lj/long/tip4p/long">long-range LJ and long-range Coulomb for TIP4P water</cml:potential>
            <cml:potential id="lj/mdf">LJ potential with a taper function</cml:potential>
            <cml:potential id="lj/relres">LJ using multiscale Relative Resolution (RelRes) methodology (Chaimovich).</cml:potential>
            <cml:potential id="lj/sdk">LJ for SDK coarse-graining</cml:potential>
            <cml:potential id="lj/sdk/coul/long">LJ for SDK coarse-graining with long-range Coulomb</cml:potential>
            <cml:potential id="lj/sdk/coul/msm">LJ for SDK coarse-graining with long-range Coulomb via MSM</cml:potential>
            <cml:potential id="lj/sf/dipole/sf">LJ with dipole interaction with shifted forces</cml:potential>
            <cml:potential id="lj/smooth">smoothed Lennard-Jones potential</cml:potential>
            <cml:potential id="lj/smooth/linear">linear smoothed LJ potential</cml:potential>
            <cml:potential id="lj/switch3/coulgauss/long">smoothed LJ vdW potential with Gaussian electrostatics</cml:potential>
            <cml:potential id="lj96/cut">Lennard-Jones 9/6 potential</cml:potential>
            <cml:potential id="local/density">generalized basic local density potential</cml:potential>
            <cml:potential id="lubricate">hydrodynamic lubrication forces</cml:potential>
            <cml:potential id="lubricate/poly">hydrodynamic lubrication forces with polydispersity</cml:potential>
            <cml:potential id="lubricateU">hydrodynamic lubrication forces for Fast Lubrication Dynamics</cml:potential>
            <cml:potential id="lubricateU/poly">hydrodynamic lubrication forces for Fast Lubrication with polydispersity</cml:potential>
            <cml:potential id="mdpd">mDPD particle interactions</cml:potential>
            <cml:potential id="mdpd/rhosum">mDPD particle interactions for mass density</cml:potential>
            <cml:potential id="meam">modified embedded atom method (MEAM) in C</cml:potential>
            <cml:potential id="meam/spline">splined version of MEAM</cml:potential>
            <cml:potential id="meam/sw/spline">splined version of MEAM with a Stillinger-Weber term</cml:potential>
            <cml:potential id="mesocnt">mesoscale model for (carbon) nanotubes</cml:potential>
            <cml:potential id="mgpt">simplified model generalized pseudopotential theory (MGPT) potential</cml:potential>
            <cml:potential id="mesont/tpm">nanotubes mesoscopic force field</cml:potential>
            <cml:potential id="mie/cut">Mie potential</cml:potential>
            <cml:potential id="mm3/switch3/coulgauss/long">smoothed MM3 vdW potential with Gaussian electrostatics</cml:potential>
            <cml:potential id="momb">Many-Body Metal-Organic (MOMB) force field</cml:potential>
            <cml:potential id="morse">Morse potential</cml:potential>
            <cml:potential id="morse/smooth/linear">linear smoothed Morse potential</cml:potential>
            <cml:potential id="morse/soft">Morse potential with a soft core</cml:potential>
            <cml:potential id="multi/lucy">DPD potential with density-dependent force</cml:potential>
            <cml:potential id="multi/lucy/rx">reactive DPD potential with density-dependent force</cml:potential>
            <cml:potential id="nb3b/harmonic">non-bonded 3-body harmonic potential</cml:potential>
            <cml:potential id="nm/cut">N-M potential</cml:potential>
            <cml:potential id="nm/cut/coul/cut">N-M potential with cutoff Coulomb</cml:potential>
            <cml:potential id="nm/cut/coul/long">N-M potential with long-range Coulomb</cml:potential>
            <cml:potential id="nm/cut/split">Split 12-6 Lennard-Jones and N-M potential</cml:potential>
            <cml:potential id="oxdna/coaxstk">oxdna/coaxstk</cml:potential>
            <cml:potential id="oxdna/excv"></cml:potential>
            <cml:potential id="oxdna/hbond"></cml:potential>
            <cml:potential id="oxdna/stk"></cml:potential>
            <cml:potential id="oxdna/xstk"></cml:potential>
            <cml:potential id="oxdna2/coaxstk"></cml:potential>
            <cml:potential id="oxdna2/dh"></cml:potential>
            <cml:potential id="oxdna2/excv"></cml:potential>
            <cml:potential id="oxdna2/hbond"></cml:potential>
            <cml:potential id="oxdna2/stk"></cml:potential>
            <cml:potential id="oxdna2/xstk"></cml:potential>
            <cml:potential id="oxrna2/coaxstk"></cml:potential>
            <cml:potential id="oxrna2/dh"></cml:potential>
            <cml:potential id="oxrna2/excv"></cml:potential>
            <cml:potential id="oxrna2/hbond"></cml:potential>
            <cml:potential id="oxrna2/stk"></cml:potential>
            <cml:potential id="oxrna2/xstk"></cml:potential>
            <cml:potential id="pace">Atomic Cluster Expansion (ACE) machine-learning potential</cml:potential>
            <cml:potential id="peri/eps">peridynamic EPS potential</cml:potential>
            <cml:potential id="peri/lps">peridynamic LPS potential</cml:potential>
            <cml:potential id="peri/pmb">peridynamic PMB potential</cml:potential>
            <cml:potential id="peri/ves">peridynamic VES potential</cml:potential>
            <cml:potential id="polymorphic">polymorphic 3-body potential</cml:potential>
            <cml:potential id="python"></cml:potential>
            <cml:potential id="quip"></cml:potential>
            <cml:potential id="rann"></cml:potential>
            <cml:potential id="reaxff">ReaxFF potential</cml:potential>
            <cml:potential id="rebo">second generation REBO potential of Brenner</cml:potential>
            <cml:potential id="resquared">Everaers RE-Squared ellipsoidal potential</cml:potential>
            <cml:potential id="saip/metal">interlayer potential for hetero-junctions formed with hexagonal 2D materials and metal surfaces</cml:potential>
            <cml:potential id="sdpd/taitwater/isothermal">smoothed dissipative particle dynamics for water at isothermal conditions</cml:potential>
            <cml:potential id="smd/hertz"></cml:potential>
            <cml:potential id="smd/tlsph"></cml:potential>
            <cml:potential id="smd/tri_surface"></cml:potential>
            <cml:potential id="smd/ulsph"></cml:potential>
            <cml:potential id="smtbq"></cml:potential>
            <cml:potential id="mliap">Multiple styles of machine-learning potential</cml:potential>
            <cml:potential id="snap">SNAP machine-learning potential</cml:potential>
            <cml:potential id="soft">Soft (cosine) potential</cml:potential>
            <cml:potential id="sph/heatconduction"></cml:potential>
            <cml:potential id="sph/idealgas"></cml:potential>
            <cml:potential id="sph/lj"></cml:potential>
            <cml:potential id="sph/rhosum"></cml:potential>
            <cml:potential id="sph/taitwater"></cml:potential>
            <cml:potential id="sph/taitwater/morris"></cml:potential>
            <cml:potential id="spin/dipole/cut"></cml:potential>
            <cml:potential id="spin/dipole/long"></cml:potential>
            <cml:potential id="spin/dmi"></cml:potential>
            <cml:potential id="spin/exchange"></cml:potential>
            <cml:potential id="spin/exchange/biquadratic"></cml:potential>
            <cml:potential id="spin/magelec"></cml:potential>
            <cml:potential id="spin/neel"></cml:potential>
            <cml:potential id="srp"></cml:potential>
            <cml:potential id="sw">Stillinger-Weber 3-body potential</cml:potential>
            <cml:potential id="sw/mod">modified Stillinger-Weber 3-body potential</cml:potential>
            <cml:potential id="table">tabulated pair potential</cml:potential>
            <cml:potential id="table/rx"></cml:potential>
            <cml:potential id="tdpd">tDPD particle interactions</cml:potential>
            <cml:potential id="tersoff">Tersoff 3-body potential</cml:potential>
            <cml:potential id="tersoff/mod">modified Tersoff 3-body potential</cml:potential>
            <cml:potential id="tersoff/mod/c"></cml:potential>
            <cml:potential id="tersoff/table"></cml:potential>
            <cml:potential id="tersoff/zbl">Tersoff/ZBL 3-body potential</cml:potential>
            <cml:potential id="thole">Coulomb interactions with thole damping</cml:potential>
            <cml:potential id="tip4p/cut">Coulomb for TIP4P water w/out LJ</cml:potential>
            <cml:potential id="tip4p/long">long-range Coulomb for TIP4P water w/out LJ</cml:potential>
            <cml:potential id="tip4p/long/soft"></cml:potential>
            <cml:potential id="tracker">monitor information about pairwise interactions</cml:potential>
            <cml:potential id="tri/lj">LJ potential between triangles</cml:potential>
            <cml:potential id="ufm"></cml:potential>
            <cml:potential id="vashishta">Vashishta 2-body and 3-body potential</cml:potential>
            <cml:potential id="vashishta/table">vashishta/table</cml:potential>
            <cml:potential id="wf/cut">Wang-Frenkel Potential for short-ranged interactions</cml:potential>
            <cml:potential id="yukawa">Yukawa potential</cml:potential>
            <cml:potential id="yukawa/colloid">screened Yukawa potential for finite-size particles</cml:potential>
            <cml:potential id="zbl">Ziegler-Biersack-Littmark potential</cml:potential>
        </xsl:variable>        

        <xsl:variable name="potential" select="tokenize($pairstyle,'\s+')[1]"/>
        <xsl:if test="exists($potentials/cml:potential[@id=$potential])">
            <xsl:value-of select="$potentials/cml:potential[@id=$potential]/text()"/>    
        </xsl:if>
    </xsl:function>
    
    
    <!-- Trim whitespaces from string -->
    <xsl:function name="l:trim" as="xs:string">
        <xsl:param name="arg" as="xs:string?"/>        
        <xsl:sequence select="replace(replace($arg,'\s+$',''),'^\s+','')"/>        
    </xsl:function>
   
</xsl:stylesheet>