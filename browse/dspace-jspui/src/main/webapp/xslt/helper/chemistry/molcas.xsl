<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:molcas="http://www.iochem-bd.org/dictionary/molcas/"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:variable name="molcas:RestrictedGeomOpt" select="'Restricted geometry optimization'"/>
    <xsl:variable name="molcas:GeometryOpt" select="'Geometry optimization'"/>
    <xsl:variable name="molcas:SinglePoint" select="'Single point'"/>
    <xsl:variable name="molcas:TS" select="'TS'" />
    <xsl:variable name="molcas:Incomplete" select="'Incomplete'"/>
    
    <xsl:variable name="molcas:unrestrictedRegex" select="'UHF'"/>
    <xsl:variable name="molcas:methodsRegex" select="'(CASPT2|RASSCF|RASSI|SCF)'"/>
    <xsl:variable name="molcas:SCFmodule" select="'SCF'" />
    <xsl:variable name="molcas:RASSCFmodule" select="'RASSCF'"/>
    <xsl:variable name="molcas:CASSCFmodule" select="'CASSCF'"/>
    <xsl:variable name="molcas:RASPT2module" select="'RASPT2'"/>
    <xsl:variable name="molcas:CASPT2module" select="'CASPT2'"/>
    
    <xsl:variable name="optmizationRegex">
        <xsl:element name="regex"><xsl:attribute name="line" select="'1'"/>.*DO\s+WHILE.*</xsl:element>
        <xsl:element name="regex"><xsl:attribute name="line" select="'2'"/>.*SEWARD.*</xsl:element>
        <xsl:element name="regex"><xsl:attribute name="line" select="'3'"/>.*(RASSCF|CASPT2|SCF).*</xsl:element>
        <xsl:element name="regex"><xsl:attribute name="line" select="'4'"/>.*SLAPAF.*</xsl:element>
        <xsl:element name="regex"><xsl:attribute name="line" select="'5'"/>.*END\s+DO.*</xsl:element>
    </xsl:variable>
    
    <xsl:variable name="tsRegex">^\s*TS\s*$</xsl:variable>
    
    <xsl:function name="molcas:isOptimization" as="xs:boolean">
        <xsl:param name="inputLines"/>        
        <xsl:variable name="result" select="molcas:checkLineExists($inputLines, 1)"/>        
        <xsl:value-of select="contains($result,'true')"/>        
    </xsl:function>
    
    <xsl:function name="molcas:checkLineExists">        
        <xsl:param name="inputLines" as="xs:string*"/>
        <xsl:param name="lineToCheck"/>
        <xsl:choose>
            <xsl:when test="$lineToCheck > count($optmizationRegex/regex)">
                <xsl:value-of select="'true'"/>    
            </xsl:when>
            <xsl:otherwise>
                <xsl:for-each select="$inputLines">
                    <xsl:variable name="line" select="."/>
                    <xsl:if test="matches(upper-case($line), $optmizationRegex/regex[@line=$lineToCheck]/text())">
                        <xsl:value-of select="molcas:checkLineExists($inputLines, $lineToCheck+1)"/>
                    </xsl:if>            
                </xsl:for-each>        
            </xsl:otherwise>
        </xsl:choose>        
    </xsl:function>
    
    <xsl:function name="molcas:isTS" as="xs:boolean">
        <xsl:param name="inputLines"/>
        
        <xsl:variable name="result">
            <xsl:for-each select="$inputLines">
                <xsl:variable name="line" select="."/>
                <xsl:if test="matches(upper-case($line), $tsRegex)"><xsl:text>true</xsl:text></xsl:if>    
            </xsl:for-each>
        </xsl:variable>        
        <xsl:value-of select="contains($result,'true')"/>        
    </xsl:function>
    
    <xsl:function name="molcas:isIncomplete" as="xs:boolean">
        <xsl:param name="sections"/>        
        <xsl:param name="isOptimization"/>
        <xsl:param name="hasLastEnergySection" />
        
        <xsl:variable name="convergedResult">
            <xsl:for-each select="$sections">
                <xsl:variable name="section" select="."/>
                <xsl:if test="count($section//cml:scalar[@dictRef='m:converged' and matches(text(),'Yes')]) = 4">
                    <xsl:text>true</xsl:text>                    
                </xsl:if>
            </xsl:for-each>
            <xsl:if test="$isOptimization and $hasLastEnergySection">
                <xsl:text>true</xsl:text>
            </xsl:if>
        </xsl:variable>
        <xsl:variable name="hasConverged" select="contains($convergedResult,'true')"/>
        <xsl:choose>
            <xsl:when test="not(exists($sections))">
                <xsl:value-of select="false()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="not($hasConverged)"/>        
            </xsl:otherwise>
        </xsl:choose>

    </xsl:function>

    <xsl:function name="molcas:getCalcType">
        <xsl:param name="isRestrictedOpt"/>
        <xsl:param name="isOptimization"/>
        <xsl:param name="isTS"/>
        <xsl:param name="isIncomplete"/>

        <xsl:choose>
            <xsl:when test="$isRestrictedOpt">
                <xsl:value-of select="$molcas:RestrictedGeomOpt"/>
            </xsl:when>
            <xsl:when test="$isOptimization">
                <xsl:value-of select="$molcas:GeometryOpt"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$molcas:SinglePoint"/>    
            </xsl:otherwise>            
        </xsl:choose>
        <xsl:if test="$isTS">
            <xsl:text> </xsl:text><xsl:value-of select="$molcas:TS"/>
        </xsl:if>               
        <xsl:if test="$isIncomplete">
            <xsl:text> </xsl:text><xsl:value-of select="$molcas:Incomplete"/>
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="molcas:getMethods" >
        <xsl:param name="modules"/>
        <xsl:param name="ksdft" />
        <xsl:param name="wavespecs" />

                
        <xsl:variable name="output">  
            
            <xsl:variable name="moduleArray">
                <xsl:for-each select="$modules">                
                    <xsl:value-of select="concat(upper-case(string(.)),'|')"/>
                </xsl:for-each>   
            </xsl:variable>
            
            <xsl:variable name="isCASSCF" select="
                if(exists($wavespecs) and contains($moduleArray,$molcas:RASSCFmodule) and ($wavespecs/cml:scalar[@dictRef='m:ras1holes'] = '0') and ($wavespecs/cml:scalar[@dictRef='m:ras3holes'] = 0)) then
                true()                
                else
                false()"/>
            <xsl:variable name="isRASSCF" select="
                if(contains($moduleArray,$molcas:RASSCFmodule)) then
                true()
                else
                false()"/>            
          
            <xsl:for-each select="distinct-values(tokenize($moduleArray,'[|]+'))">
                    <xsl:if test="matches(.,$molcas:methodsRegex)">                                       
                    <xsl:choose>
                        <xsl:when test="exists($ksdft) and matches(.,$molcas:SCFmodule)">
                            <xsl:choose>
                                <xsl:when test="matches($ksdft,$molcas:SCFmodule)">
                                    <xsl:value-of select="'HF '"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="'DFT '"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="matches(.,$molcas:RASSCFmodule)">                        
                            <xsl:choose>
                                <xsl:when test="$isCASSCF">
                                    <xsl:value-of select="concat($molcas:CASSCFmodule, ' ')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat(., ' ')"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:when test="matches(.,$molcas:CASPT2module)">
                            <xsl:choose>
                                <xsl:when test="not($isCASSCF) and $isRASSCF">
                                    <xsl:value-of select="concat($molcas:RASPT2module, ' ')"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:value-of select="concat(., ' ')"/>
                                </xsl:otherwise>
                            </xsl:choose>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="concat(., ' ')"/>
                        </xsl:otherwise>
                    </xsl:choose>
                    </xsl:if>           
                </xsl:for-each>                 
        </xsl:variable>
        
        <xsl:value-of select="molcas:trim($output)"/>
    </xsl:function>

    <xsl:function name="molcas:filterModuleName">        
        <xsl:param name="moduleName"/>
        <xsl:param name="methods"/>
        <xsl:choose>
            <xsl:when test="upper-case($moduleName) = $molcas:RASSCFmodule and exists(index-of($methods,$molcas:CASSCFmodule))">
                <xsl:value-of select="$molcas:CASSCFmodule"/>
            </xsl:when>
            <xsl:when test="upper-case($moduleName) = $molcas:CASPT2module">
                <xsl:choose>
                    <xsl:when test="exists(index-of($methods, $molcas:RASSCFmodule))">
                        <xsl:value-of select="$molcas:RASPT2module"/>
                    </xsl:when>
                    <xsl:when test="exists(index-of($methods, $molcas:CASSCFmodule))">
                        <xsl:value-of select="$molcas:CASPT2module"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="$moduleName"/>
                    </xsl:otherwise>
                </xsl:choose>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="$moduleName"/>
            </xsl:otherwise>                                                
        </xsl:choose>
    </xsl:function>

    <xsl:function name="molcas:getBasis">
        <xsl:param name="basislines" as="node()*" />
        <xsl:variable name="basis">
            <xsl:for-each select="$basislines">
                <xsl:variable name="line" select="."/>
                <xsl:variable name="basis" select="tokenize($line, '\|')[2]"/>
                <xsl:if test="$basis != ''"><xsl:value-of select="$basis"/><xsl:text> </xsl:text></xsl:if>
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="distinct-values(tokenize($basis, '\s+'))"/>
    </xsl:function>

    <!-- Trim whitespaces from string -->
    <xsl:function name="molcas:trim" as="xs:string">
        <xsl:param name="arg" as="xs:string?"/>        
        <xsl:sequence select="replace(replace($arg,'\s+$',''),'^\s+','')"/>        
    </xsl:function>

</xsl:stylesheet>
