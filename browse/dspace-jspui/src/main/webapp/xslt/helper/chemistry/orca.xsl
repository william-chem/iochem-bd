<?xml version="1.0" encoding="UTF-8"?>
<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:orca="https://orcaforum.cec.mpg.de/"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <!-- Calculation type related variables -->
    <xsl:variable name="orca:GeometryOptimization" select="'Geometry optimization'" />
    <xsl:variable name="orca:SinglePoint" select="'Single point'" />
    <xsl:variable name="orca:BrokenSymmetry" select="'Broken symmetry'" />    
    <xsl:variable name="orca:TransitionState" select="'TS'" />
    <xsl:variable name="orca:Minimum" select="'Minimum'"/>
    
    <xsl:variable name="orca:OptCommand" select="'OPT'"/>
    <xsl:variable name="orca:BrokenSymmCommand" select="'BROKENSYM'"/>
    <xsl:variable name="orca:Tddft" select="'^(TDDFT|CIS)$'" />
    <xsl:variable name="orca:TS" select="'^OPTTS$'" />
    <xsl:variable name="orca:d3" select="'^D3$'" />
    <xsl:variable name="orca:grid" select="'^GRID$'" />    
    <xsl:variable name="orca:finalgrid" select="'^FINALGRID$'" />
    
    <!-- Method, functional and basis regexes -->
    <xsl:variable name="orca:calculationLevelRegex" select="'(QUICK-DFT|QUICK-OPT|MEDIUM-OPT|GOOD-OPT|ACC-OPT|DFT-ENERGY|DFT-ENERGY+)'"/>
    <xsl:variable name="orca:basisRegex" select="'^(3-21G|3-21GSP|4-22GSP|6-31G|6-311G|CC-PVDZ|CC-\(P\)VDZ|AUG-CC-PVDZ|CC-PVTZ|CC-\(P\)VTZ|AUG-CC-PVTZ|CC-PVQZ|AUG-CC-PVQZ|CC-PV5Z|AUG-CC-PV5Z|CC-PV6Z|AUG-CC-PV6Z|CC-PCVDZ|CC-PCVTZ|CC-PCVQZ|CC-PCV5Z|AUG-PCVDZ|AUG-PCVTZ|AUG-PCVQZ|AUG-PCV5Z|DUNNING-DZP|DEF2-SV\(P\)|DEF2-SVP|DEF2-TZVP|DEF2-TZVPP|DEF2-AUG-TZVPP|DEF2-QZVPP|BNANO-DZP|BNANO-TZ2P|BNANO-TZ3P|NASA-AMES-ANO|BAUSCHLICHER ANO|ROOS-ANO-DZP|ROOS-ANO-TZP|DGAUSS|DZVP-DFT|SADLEJ-PVTZ|EPR-II|EPR-III|IGLO-II|IGLO-III|PARTRIDGE-1|PARTRIDGE-2|PARTRIDGE-3|WACHTERS|AUTOAUX|DEMON/J|DGAUSS/J|SV/J|VDZ/J|TZV/J|VTZ/J|SV/J\(-F\)|TZV/J\(-F\)|QZVP/C|QZVPP/C|CC-PVDZ/C|CC-PVTZ/C|AUG-CC-PVDZ/C|AUG-CC-PVTZ/C|AUG-SV/C|AUG-TZV/C|AUG-TZVPP/C|SV|VDZ|VTZ|TZV|QZVP|DZ|PC-|AUG-PC-|ANO-PV|SAUG-ANO-PV|AUG-ANO-PV|TZVPP|SVP/JK|TZVPP/JK|QZVPP/JK|DEF2--SVP/JK|DEF2--TZVPP/JK|DEF2--QZVPP/JK|CC--PVDZ/JK|CC--PVTZ/JK|CC--PVQZ/JK|CC--PV5Z/JK|CC--PV6Z/JK).*'"/>
    <xsl:variable name="orca:methodsRegex" select="'^(HF|DFT|MP2|MP2RI|RI-MP2|SCS-MP2|RI-SCS-MP2|CCSD|CISD|CASSCF|CCSD\(T\)|QCISD|QCISD\(T\)|CPF/|NCPF/|CEPA/|NCEPA/|ACPF|NACPF|AQCC|MRCI|MRCI\+Q|MRACPF|MRAQCC|MRDDCI1|MRDDCI2|MRDDCI3|MRDDCIn\+Q|SORCI|ZINDO/S|ZINDO/1|ZINDO/2|NDDO/1|NDDO/2|MNDO|AM1|PM3|RHF|RKS|UHF|UKS|ROHF|ROKS|ALLOWRHF|F12|DLPNO-CCSD|DLPNO-CCSD\(T\)|DLPNO-MP2|DLPNO-MP2-F12/D).*'"/>
    <xsl:variable name="orca:functionalsRegex" select="'^(HFS|LSD|LDA|VWN|VWN5|VWN3|PWLDA|BP86|BP|BLYP|OLYP|GLYP|XLYP|PW91|MPWPW|MPWLYP|OPBE|PBE|RPBE|REVPBE|PWP|B1LYP|B3LYP|B3LYP/G|O3LYP|X3LYP|B1P|B3P|B3PW|PW1PW|MPW1PW|MPW1LYP|PBE0|TPSS|TPSSH|TPSS0|B2PLYP|RI-B2PLYP|RI-B2PLYP RIJONX|MPW2PLYP|PW6B95|B2GP-PLYP|B2K-PLYP|B2T-PLYP|PWPB95|RI-PWPB95).*'"/>
    <xsl:variable name="orca:dlpnoMethodRegex" select="'^DLPNO-.*'"/>
    <xsl:variable name="orca:extraParameters" select="'^(TIGHTPNO|LOOSEPNO)'"/>
        
    <!-- Regex to decide if a method is Restricted or Unrestricted -->
    <xsl:variable name="orca:restrictedMethodRegex" select="'^(RHF|RKS|ROHF|CASSCF|ROKS).*'"/>
    <xsl:variable name="orca:unrestrictedMethodRegex" select="'^(UHF|UKS).*'"/>
        
    <xsl:variable name="orca:calculationLevels">
        <level id="QUICK-DFT"   method="DFT" isOptimization="false" functional="BP" basisSet="DefBas-1"/>
        <level id="QUICK-OPT"   method="DFT" isOptimization="true"  functional="BP" basisSet="DefBas-1"/>
        <level id="MEDIUM-OPT"  method="DFT" isOptimization="true"  functional="BP" basisSet="DefBas-2"/>
        <level id="GOOD-OPT"    method="DFT" isOptimization="true"  functional="BP" basisSet="DefBas-3"/>
        <level id="ACC-OPT"     method="DFT" isOptimization="true"  functional="BP" basisSet="DefBas-4"/>
        <level id="DFT-ENERGY"  method="DFT" isOptimization="false" functional="B3LYP" basisSet="DefBas-4"/>
        <level id="DFT-ENERGY+" method="DFT" isOptimization="false" functional="B3LYP" basisSet="DefBas-5"/>
    </xsl:variable>
        
    <xsl:function name="orca:isOptimization">
        <xsl:param name="section"/>        
        <xsl:variable name="return">
            <xsl:variable name="lines" select="$section//cml:array[@dictRef='cc:keywords']"/>
            <xsl:for-each select="$lines">
                <xsl:variable name="line" select="./text()"/>                    
                    <!-- Check each word in line -->
                    <xsl:for-each select="tokenize($line,'\s+')">
                        <xsl:variable name="command" select="."/>
                        <xsl:choose>
                            <xsl:when test="matches(upper-case($command), $orca:OptCommand)">
                                <xsl:value-of select="'true'"/>
                            </xsl:when>
                            <xsl:when test="matches(upper-case($command),$orca:calculationLevelRegex)">
                                <xsl:value-of select="$orca:calculationLevels/level[@id=upper-case($command)]/@isOptimization"/>
                            </xsl:when>
                        </xsl:choose>                        
                    </xsl:for-each>                         
            </xsl:for-each>
            
            <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
            <xsl:for-each select="$blocklines">
                <xsl:variable name="line" select="./text()"/>                    
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:choose>
                        <xsl:when test="matches(upper-case($command), $orca:OptCommand)">
                            <xsl:value-of select="'true'"/>        
                        </xsl:when>
                        <xsl:when test="matches(upper-case($command),$orca:calculationLevelRegex)">
                            <xsl:value-of select="$orca:calculationLevels/level[@id=upper-case($command)]/@isOptimization"/>
                        </xsl:when>
                    </xsl:choose>                    
                </xsl:for-each>                
            </xsl:for-each>            
        </xsl:variable>                
        <xsl:value-of select="boolean(contains($return,'true'))"/>
    </xsl:function>
    
    <xsl:function name="orca:isBrokenSymm">
        <xsl:param name="section"/>
        <xsl:variable name="return">
            <xsl:for-each select="$section//cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>                               
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:BrokenSymmCommand)">
                        <xsl:value-of select="'true'"/>
                    </xsl:if>
                </xsl:for-each>                                   
            </xsl:for-each>

            <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
            <xsl:for-each select="$blocklines">
                <xsl:variable name="line" select="./text()"/>                    
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:BrokenSymmCommand)">
                        <xsl:value-of select="'true'"/>        
                    </xsl:if>
                </xsl:for-each>                
            </xsl:for-each>
        </xsl:variable>        
        <xsl:value-of select="boolean(contains($return,'true'))"/>  
    </xsl:function>

    <xsl:function name="orca:readExtraParameters">
        <xsl:param name="section"/>
        <xsl:variable name="return">
            <xsl:for-each select="$section//cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>                               
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:extraParameters)">
                        <xsl:value-of select="concat(' ', $command)"/>
                    </xsl:if>
                </xsl:for-each>                                   
            </xsl:for-each>
        </xsl:variable>
        <xsl:value-of select="$return"/>
    </xsl:function>

    <xsl:function name="orca:isTddft">
        <xsl:param name="section"/>
        <xsl:variable name="return">
            <xsl:for-each select="$section//cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>                               
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:Tddft)">
                        <xsl:value-of select="'true'"/>
                    </xsl:if>
                </xsl:for-each>                                   
            </xsl:for-each>
            
            <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
            <xsl:for-each select="$blocklines">
                <xsl:variable name="line" select="./text()"/>                    
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:Tddft)">
                        <xsl:value-of select="'true'"/>        
                    </xsl:if>
                </xsl:for-each>                
            </xsl:for-each>
            
        </xsl:variable>        
        <xsl:value-of select="contains($return,'true')"/>
    </xsl:function>
    
    <xsl:function name="orca:isTS">
        <xsl:param name="section"/>
        <xsl:variable name="return">
            <xsl:for-each select="$section//cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>                               
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:TS)">
                        <xsl:value-of select="'true'"/>
                    </xsl:if>
                </xsl:for-each>                                   
            </xsl:for-each>
            
            <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
            <xsl:for-each select="$blocklines">
                <xsl:variable name="line" select="./text()"/>                    
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:TS)">
                        <xsl:value-of select="'true'"/>        
                    </xsl:if>
                </xsl:for-each>                
            </xsl:for-each>
            
        </xsl:variable>        
        <xsl:value-of select="contains($return,'true')"/>
    </xsl:function>
    
    <xsl:function name="orca:isDlpnoMethod">
        <xsl:param name="section"/>
        <xsl:variable name="return">
            <xsl:for-each select="$section//cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>                               
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command), $orca:dlpnoMethodRegex)">
                        <xsl:value-of select="$command"/>
                    </xsl:if>
                </xsl:for-each>                                   
            </xsl:for-each>            
        </xsl:variable>
        <xsl:value-of select="$return"/>               
    </xsl:function>
    
    
    
    <xsl:function name="orca:countNegativeFrequencies">
        <xsl:param name="freqHeaders"/>    
        <xsl:value-of select="string-length(replace($freqHeaders,'[^-]',''))"/>        
    </xsl:function>
    
	<xsl:function name="orca:getCalcType">
		<xsl:param name="isOptimization" as="xs:boolean"/>
		<xsl:param name="isBrokenSymm" as="xs:boolean"/>
		<xsl:param name="hasVibrations" as="xs:boolean"/>
	    <xsl:param name="negativeFrequenciesCount" as="xs:integer"/>
	    <xsl:param name="isTS" as="xs:boolean"/>
        
        <xsl:variable name="type">
            <xsl:choose>
                <xsl:when test="$isOptimization">
                    <xsl:value-of select="$orca:GeometryOptimization"/>
                </xsl:when>
                <xsl:when test="$isBrokenSymm">
                    <xsl:value-of select="$orca:BrokenSymmetry"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="$orca:SinglePoint"/>
                </xsl:otherwise>
            </xsl:choose>     
        </xsl:variable>
	    
	    <xsl:variable name="type2">
	        <xsl:if test="$hasVibrations">
	            <xsl:choose>
	                <xsl:when test="$negativeFrequenciesCount = 1 and $isTS" >
	                    <xsl:value-of select="$orca:TransitionState"/>
	                </xsl:when>
	                <xsl:when test="$negativeFrequenciesCount = 0">
	                    <xsl:value-of select="$orca:Minimum"/>
	                </xsl:when >
	            </xsl:choose>
	        </xsl:if>	       
	    </xsl:variable>
	    
	    <xsl:value-of select="concat($type, ' ', $type2)"/>
	</xsl:function>
 
    <xsl:function name="orca:getBasis">
        <xsl:param name="section"/>

        <!-- Basis readed from single input lines -->
        <xsl:variable name="basisOnLine">
            <xsl:for-each select="$section/cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>
                <xsl:variable name="commands" select="tokenize($line,'\s+')"/>
                <xsl:for-each select="1 to count($commands)">
                    <xsl:variable name="outerIndex" select="position()"/>
                    <xsl:variable name="command" select="$commands[$outerIndex]"/>                    
                    <xsl:if test="matches(upper-case($command), $orca:basisRegex)">
                        <xsl:if test="compare(upper-case($commands[$outerIndex - 1]),'RI') != 0">   <!--Discard basis defined after RI command -->
                            <xsl:value-of select="$command"/><xsl:text> </xsl:text>
                        </xsl:if>
                    </xsl:if>                        
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="calculationLevelBasisOnLine">
            <xsl:for-each select="$section/cml:array[@dictRef='cc:keywords']">
                <xsl:variable name="line" select="./text()"/>
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                    <xsl:if test="matches(upper-case($command),$orca:calculationLevelRegex)">
                        <xsl:value-of select="$orca:calculationLevels/level[@id=upper-case($command)]/@basisSet"/><xsl:text> </xsl:text>
                    </xsl:if>
                </xsl:for-each>
            </xsl:for-each>
        </xsl:variable>
        
        <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
        
        <!-- Basis readed from an input block (terminated with 'end')-->
        <xsl:variable name="basisOnBlock">
            <xsl:for-each select="$blocklines">
                <xsl:variable name="line" select="./text()"/>                    
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>                
                    <xsl:if test="matches(upper-case($command), $orca:basisRegex)">
                        <xsl:value-of select="$command"/><xsl:text> </xsl:text>    
                    </xsl:if>
                </xsl:for-each>                
            </xsl:for-each>                
        </xsl:variable>
        
        <xsl:variable name="calculationLevelBasisOnBlock">
            <xsl:for-each select="$blocklines">
                <xsl:variable name="line" select="./text()"/>                    
                <xsl:for-each select="tokenize($line,'\s+')">
                    <xsl:variable name="command" select="."/>
                        <xsl:if test="matches(upper-case($command),$orca:calculationLevelRegex)">
                            <xsl:value-of select="$orca:calculationLevels/level[@id=upper-case($command)]/@basisSet"/><xsl:text> </xsl:text>
                        </xsl:if>               
                </xsl:for-each>                
            </xsl:for-each>        
        </xsl:variable>
        
        <xsl:element name="basis">
            <xsl:attribute name="isCalculationLevel" select="exists($calculationLevelBasisOnLine) or exists($calculationLevelBasisOnBlock)"/>
            <xsl:attribute name="definedBasis" select="concat($basisOnLine, ' ', $basisOnBlock)"/>
            <xsl:attribute name="calculationLevelBasis" select="concat($calculationLevelBasisOnLine, ' ', $calculationLevelBasisOnBlock)"/>
        </xsl:element>
    </xsl:function>
 
    <xsl:function name="orca:getMethods">
        <xsl:param name="section"/>  
        <xsl:param name="isTddft" as="xs:boolean"/>
        <xsl:for-each select="$section/cml:array[@dictRef='cc:keywords']">
            <xsl:variable name="line" select="./text()"/>
            <xsl:for-each select="tokenize($line,'\s+')">
                <xsl:variable name="command" select="."/>
                <xsl:choose>
                    <xsl:when test="matches(upper-case($command), $orca:methodsRegex)">
                        <xsl:if test="$isTddft and matches(upper-case($command), '(DFT|HF)')">TD</xsl:if><xsl:value-of select="$command"/><xsl:text> </xsl:text>    
                    </xsl:when>                   
                    <xsl:when test="matches(upper-case($command),$orca:calculationLevelRegex)">
                        <xsl:if test="$isTddft">TD</xsl:if><xsl:value-of select="$orca:calculationLevels/level[@id=upper-case($command)]/@method"/><xsl:text> </xsl:text>
                    </xsl:when>
                </xsl:choose>                             
            </xsl:for-each>
        </xsl:for-each>
        
        <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
        <xsl:for-each select="$blocklines">
            <xsl:variable name="line" select="./text()"/>                    
            <xsl:for-each select="tokenize($line,'\s+')">
                <xsl:variable name="command" select="."/>                
                <xsl:choose>
                    <xsl:when test="matches(upper-case($command), $orca:methodsRegex)">
                        <xsl:if test="$isTddft and matches(upper-case($command), '(DFT|HF)')">TD</xsl:if><xsl:value-of select="$command"/><xsl:text> </xsl:text>    
                    </xsl:when>
                    <xsl:when test="matches(upper-case($command),$orca:calculationLevelRegex)">
                        <xsl:if test="$isTddft">TD</xsl:if><xsl:value-of select="$orca:calculationLevels/level[@id=upper-case($command)]/@method"/><xsl:text> </xsl:text>
                    </xsl:when>
                </xsl:choose>             
            </xsl:for-each>                
        </xsl:for-each>       
    </xsl:function>    
   
    <xsl:function name="orca:getFunctionals">
       <xsl:param name="section"/>
       <xsl:param name="scfsettings"/>
       
       <xsl:for-each select="$section/cml:array[@dictRef='cc:keywords']">
           <xsl:variable name="line" select="./text()"/>
           <xsl:for-each select="tokenize($line,'\s+')">
               <xsl:variable name="command" select="."/>
               <xsl:choose>
                   <xsl:when test="matches(upper-case($command), $orca:functionalsRegex)">
                       <xsl:choose>
                           <xsl:when test="compare($command, 'B3LYP') = 0 and orca:isModifiedB3LYP($scfsettings)">
                               <xsl:value-of select="concat($command,'*')"/><xsl:text> </xsl:text>
                           </xsl:when>                           
                           <xsl:when test="compare($command, 'B3LYP') = 0 and orca:isNotDefaultB3LYP($scfsettings)">
                               <xsl:value-of select="concat($command,' modified')"/><xsl:text> </xsl:text><xsl:value-of select="orca:getNotDefaultB3LYPParams($scfsettings)"/>                               
                           </xsl:when>
                           
                           <xsl:otherwise>                               
                               <xsl:value-of select="$command"/><xsl:text> </xsl:text>        
                           </xsl:otherwise>
                       </xsl:choose>                  
                   </xsl:when>
                   <xsl:when test="matches(upper-case($command),$orca:calculationLevelRegex)">
                       <xsl:variable name="method" select="$orca:calculationLevels/level[@id=upper-case($command)]/@functional" />
                       <xsl:choose>
                           <xsl:when test="compare($method, 'B3LYP') = 0 and orca:isModifiedB3LYP($scfsettings)">
                               <xsl:value-of select="concat($method,'*')"/><xsl:text> </xsl:text>
                           </xsl:when>
                           <xsl:when test="compare($method, 'B3LYP') = 0 and orca:isNotDefaultB3LYP($scfsettings)">
                               <xsl:value-of select="concat($method,' modified')"/><xsl:text> </xsl:text><xsl:value-of select="orca:getNotDefaultB3LYPParams($scfsettings)"/>                               
                           </xsl:when>
                           <xsl:otherwise>
                               <xsl:value-of select="$method"/><xsl:text> </xsl:text>        
                           </xsl:otherwise>
                       </xsl:choose>                       
                   </xsl:when>
               </xsl:choose>               
           </xsl:for-each>
       </xsl:for-each>
       
       <xsl:variable name="blocklines" select="$section//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
       <xsl:for-each select="$blocklines">
           <xsl:variable name="line" select="./text()"/>                    
           <xsl:for-each select="tokenize($line,'\s+')">
               <xsl:variable name="command" select="."/>
               <xsl:choose>
                   <xsl:when test="matches(upper-case($command), $orca:functionalsRegex)">
                       <xsl:choose>
                           <xsl:when test="compare($command, 'B3LYP') = 0 and orca:isModifiedB3LYP($scfsettings)">
                               <xsl:value-of select="concat($command,'*')"/><xsl:text> </xsl:text>
                           </xsl:when>
                           <xsl:when test="compare($command, 'B3LYP') = 0 and orca:isNotDefaultB3LYP($scfsettings)">
                               <xsl:value-of select="concat($command,' modified')"/><xsl:text> </xsl:text><xsl:value-of select="orca:getNotDefaultB3LYPParams($scfsettings)"/>                               
                           </xsl:when>
                           <xsl:otherwise>
                               <xsl:value-of select="$command"/><xsl:text> </xsl:text>        
                           </xsl:otherwise>
                       </xsl:choose>         
                   </xsl:when>
                   <xsl:when test="matches(upper-case($command),$orca:calculationLevelRegex)">
                       <xsl:variable name="method" select="$orca:calculationLevels/level[@id=upper-case($command)]/@functional" />
                       <xsl:choose>
                           <xsl:when test="compare($method, 'B3LYP') = 0 and orca:isModifiedB3LYP($scfsettings)">
                               <xsl:value-of select="concat($method,'*')"/><xsl:text> </xsl:text>    
                           </xsl:when>
                           <xsl:when test="compare($method, 'B3LYP') = 0 and orca:isNotDefaultB3LYP($scfsettings)">
                               <xsl:value-of select="concat($method,' modified')"/><xsl:text> </xsl:text><xsl:value-of select="orca:getNotDefaultB3LYPParams($scfsettings)"/>                               
                           </xsl:when>
                           <xsl:otherwise>
                               <xsl:value-of select="$method"/><xsl:text> </xsl:text>
                           </xsl:otherwise>
                       </xsl:choose>
                   </xsl:when>
               </xsl:choose>               
           </xsl:for-each>                
       </xsl:for-each>       
   </xsl:function>
    
    <xsl:function name="orca:getPrimitive">
        <xsl:param name="basisSection"/>
        <xsl:param name="index"/>
        <xsl:variable name="group" select="number(tokenize($basisSection/cml:list[@dictRef='atombasis']/cml:array[@dictRef='o:group'],'\s+')[$index])" />
        <xsl:value-of select="tokenize($basisSection//cml:array[@dictRef='o:primitive'],'\s+')[$group]"/>        
    </xsl:function>
    
    <xsl:function name="orca:getContraction">
        <xsl:param name="basisSection"/>
        <xsl:param name="index"/>
        <xsl:variable name="group" select="number(tokenize($basisSection/cml:list[@dictRef='atombasis']/cml:array[@dictRef='o:group'],'\s+')[$index])" />
        <xsl:value-of select="tokenize($basisSection//cml:array[@dictRef='o:contraction'],'\s+')[$group]"/>        
    </xsl:function>    

    <xsl:function name="orca:isMethodUnrestricted">
        <xsl:param name="method"/>
        <xsl:choose>
            <xsl:when test="matches(upper-case($method),$orca:unrestrictedMethodRegex)">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:when test="matches(upper-case($method), $orca:restrictedMethodRegex)">
                <xsl:value-of select="false()"/>
            </xsl:when>
        </xsl:choose>
    </xsl:function>

    <xsl:function name="orca:getMethodExtras">
        <xsl:param name="commands"/>
        
        <xsl:for-each select="$commands/cml:array[@dictRef='cc:keywords']">
            <xsl:variable name="keywords" select="tokenize(upper-case(.),'\s+')"/>
            <xsl:for-each select="$keywords">
                <xsl:variable name="keyword" select="."/>
                <xsl:variable name="outerIndex" select="position()"/>
                <xsl:if test="matches($keyword,$orca:d3)">
                    <xsl:value-of select="' D3 '"/>
                </xsl:if> 
                <xsl:if test="matches($keyword,$orca:grid)">
                    <xsl:value-of select="concat(' Grid ',$keywords[$outerIndex + 1]),' '"/>
                </xsl:if>
                <xsl:if test="matches($keyword,$orca:finalgrid)">
                    <xsl:value-of select="concat(' FinalGrid ',$keywords[$outerIndex + 1]),' '"/>
                </xsl:if>
            </xsl:for-each>            
        </xsl:for-each>
        
        <xsl:variable name="blocklines" select="$commands//cml:module[@cmlx:templateRef='block']/cml:scalar"/>
        <xsl:for-each select="$blocklines">
            <xsl:variable name="block" select="." />
            <xsl:variable name="line" select="upper-case($block/text())"/>
            <xsl:variable name="commands" select="tokenize($line,'\s+')" />
            <xsl:for-each select="$commands">
                <xsl:variable name="command" select="."/>
                <xsl:variable name="outerIndex" select="position()"/>
                <!--Processing tddft section -->
                <xsl:if test="matches($command, $orca:Tddft)">
                    <xsl:if test="exists($block/following::cml:scalar[matches(upper-case(text()),'\s*TDA\s*FALSE\s*')])">
                        <xsl:value-of select="' no TDA '"/>
                    </xsl:if>                            
                </xsl:if>
                <xsl:if test="matches($command,$orca:d3)">
                    <xsl:value-of select="' D3 '"/>
                </xsl:if>
                <xsl:if test="matches($command,$orca:grid)">
                    <xsl:value-of select="concat(' Grid ',$commands[$outerIndex + 1]),' '"/>
                </xsl:if>
                <xsl:if test="matches($command,$orca:finalgrid)">
                    <xsl:value-of select="concat(' FinalGrid ',$commands[$outerIndex + 1]),' '"/>
                </xsl:if>
            </xsl:for-each>
        </xsl:for-each>
    </xsl:function>


    <xsl:function name="orca:isB3LYP">
        <xsl:param name="scfsettings"/>        
        <xsl:choose>
            <xsl:when test="exists($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'Exchange']/following::cml:scalar[1][text()='B88'])
                            and
                            exists($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'Correlation']/following::cml:scalar[1][text()='LYP'])">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="orca:isModifiedB3LYP" as="xs:boolean">
        <xsl:param name="scfsettings"/>        
        <xsl:choose>
            <xsl:when test="number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalHFX'])[1]/following::cml:scalar[1]/text()) = 0.15
                            and
                            number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFX'])[1]/following::cml:scalar[1]/text()) = 0.72
                            and
                            number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFC'])[1]/following::cml:scalar[1]/text()) = 0.81
                            and
                            number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalLDAC'])[1]/following::cml:scalar[1]/text()) = 1.00                
                            ">
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise><xsl:value-of select="false()"/></xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="orca:isNotDefaultB3LYP" as="xs:boolean">
        <xsl:param name="scfsettings"/>
        <xsl:choose>
            <xsl:when test="((exists($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalHFX']) and number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalHFX'])[1]/following::cml:scalar[1]/text()) != 0.20))
                            or
                            ((exists($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFX']) and number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFX'])[1]/following::cml:scalar[1]/text()) != 0.72))
                            or
                            ((exists($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFC']) and number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFC'])[1]/following::cml:scalar[1]/text()) != 0.81))
                            or
                            ((exists($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalLDAC']) and number(($scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalLDAC'])[1]/following::cml:scalar[1]/text()) != 1.00))                
                            ">                
                <xsl:value-of select="true()"/>
            </xsl:when>
            <xsl:otherwise>
                <xsl:value-of select="false()"/>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:function>
    
    <xsl:function name="orca:getNotDefaultB3LYPParams">
        <xsl:param name="scfsettings"/>
        <xsl:variable name="ScalHFX" select="$scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalHFX']"/>
        <xsl:variable name="ScalDFX" select="$scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFX']"/>
        <xsl:variable name="ScalDFC" select="$scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalDFC']"/>
        <xsl:variable name="ScalLDAC" select="$scfsettings//cml:scalar[@dictRef='cc:parameter'][text() = 'ScalLDAC']"/>
        
        <xsl:if test="exists($ScalHFX)">
            ScalHFX=<xsl:value-of select="$ScalHFX/following::cml:scalar[1]"/>  
        </xsl:if>
        <xsl:if test="exists($ScalDFX)">
            ScalDFX=<xsl:value-of select="$ScalDFX/following::cml:scalar[1]"/>  
        </xsl:if>       
        <xsl:if test="exists($ScalDFC)">
            ScalDFC=<xsl:value-of select="$ScalDFC/following::cml:scalar[1]"/>  
        </xsl:if>       
        <xsl:if test="exists($ScalLDAC)">
            ScalLDAC=<xsl:value-of select="$ScalLDAC/following::cml:scalar[1]"/>  
        </xsl:if>
    </xsl:function>
    
    <xsl:function name="orca:getBasisECP">
        <xsl:param name="basisecp"/>
        <xsl:param name="serial"/>
        
        <xsl:variable name="atombasisgroup" select="$basisecp/cml:module[@cmlx:templateRef='atombasis' and ./descendant::cml:scalar[@dictRef='cc:serial' and text() = $serial]]//cml:scalar[@dictRef='o:group']/text()"/>        
        <xsl:if test="exists($atombasisgroup)">
            <xsl:variable name="group" select="$basisecp/cml:module[@cmlx:templateRef='basisgroups']/cml:list[./descendant::cml:scalar[@dictRef='o:group' and text() = $atombasisgroup]]"/>
            <xsl:value-of select="$group//cml:scalar[@dictRef='o:ecptype']/text()"/><xsl:text> </xsl:text>            
        </xsl:if>        
    </xsl:function>
    
    <xsl:function name="orca:getTddftIRoot">
        <xsl:param name="commands"/>
        <xsl:variable name="tddftblock" select="$commands/cml:module[@cmlx:templateRef='block' and child::cml:scalar[@dictRef = 'o:type' and matches(upper-case(text()),'.*(TDDFT|CIS).*')]]"/>
        <xsl:variable name="irrotline" select="$tddftblock/cml:scalar[matches(upper-case(text()), 'IROOT')]"/>
        <xsl:variable name="irrot" select="upper-case($irrotline/text())"/>
        <xsl:value-of select="replace(tokenize($irrot, 'IROOT')[2], ' ','')"/>                
    </xsl:function>
    
</xsl:stylesheet>