<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:am="http://www.iochem-bd.org/dictionary/amber/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    xpath-default-namespace="http://www.xml-cml.org/schema" exclude-result-prefixes="xs xd cml ckbk am helper cmlx"
    version="2.0">
    <xd:doc scope="stylesheet">
        <xd:desc>            
            <xd:p><xd:b>Created on:</xd:b> Nov 10, 2021</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Institute of Chemical Research of Catalonia</xd:p>
        </xd:desc>       
    </xd:doc> 
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:include href="helper/chemistry/amber.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>

    <!-- Environment module -->
    <xsl:variable name="environment" select="//cml:module[@id='job'][1]/cml:module[@id='environment']/cml:parameterList"/>
    <xsl:variable name="programParameter" select="$environment/cml:parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="$environment/cml:parameter[@dictRef='cc:programVersion']"/>
        
    <!-- Geometry -->
    <xsl:variable name="molecule" select="(//cml:molecule)[last()]"/>
    <xsl:variable name="boxCoords" select="//cml:module[@dictRef='cc:finalization']/cml:molecule/cml:crystal"/>
    
    <!-- Initialization -->
    <xsl:variable name="initialization" select="//cml:module[@dictRef='cc:initialization']"/>
    <xsl:variable name="inputLines" select="$initialization//cml:module[@cmlx:templateRef='input.file']/cml:list[@cmlx:templateRef='lines']"/>
    <xsl:variable name="imin" select="$initialization/cml:parameterList/cml:parameter[@dictRef='am:imin']" />    
    <xsl:variable name="ntt" select="helper:readInputFileParameter($initialization, $inputLines,'ntt')" />
    <xsl:variable name="ntp" select="helper:readInputFileParameter($initialization, $inputLines,'ntp')" />    
    <xsl:variable name="barostatParam" select="helper:readInputFileParameter($initialization, $inputLines, 'barostat')" />    
    <xsl:variable name="method" select="helper:getMethod($imin, $ntp, $ntt)" />
    
    <!-- Finalization -->
    <xsl:variable name="finalization" select="//cml:module[@dictRef='cc:finalization']/cml:propertyList"/>               
    
    <xsl:template match="/">        
        <html lang="en">
            <head>  
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/> 
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css" />
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">
                    $.blockUI();
                </script>
                <div id="container">                                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Settings -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>SETTINGS</h3>                                                                                                                                               
                            <div>
                                <xsl:call-template name="settings"/>
                            </div>
                        </div>
                    </div>
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <h3>ATOM INFO</h3>
                            <div>
                                <xsl:apply-templates select="$molecule"/>
                            </div>
                        </div>
                    </div>
                    <!-- Results -->
                    <xsl:for-each select="//cml:module[@dictRef='cc:job']">
                        <div id="module-{generate-id(.)}">                                
                            <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(.)}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(.)}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                            <div class="row bottom-buffer">                     
                                <div class="col-md-12">
                                    <xsl:call-template name="averages"/>
                                    <xsl:call-template name="timing"/>
                                </div>
                            </div>
                        </div>
                    </xsl:for-each>
                    <xsl:call-template name="printLicense"/>
                    
                    <script type="text/javascript">
                        function expandModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {    
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });
                            
                            $("div:not([id^='atomicCoordinates']).dataTables_wrapper").each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });                            
                            $.unblockUI();                             
                        });
                        
                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }

                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                            $('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });                                                     
                        }

                        //On expand accordion we'll resize inner tables to fit current page width
                        $('.panel-collapse').on('shown.bs.collapse', function () {
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();
                            });
                        });
                    </script>
                </div>
        </body>
    </html>
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates" match="cml:molecule">
        <div class="panel panel-default">
            <div class="panel-heading"  data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Atomic coordinates [&#8491;]
                </h4>
            </div>
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row bottom-buffer">
                        <xsl:if test="exists($boxCoords)">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <h5>Cell parameters:</h5>
                                        <table id="cellParameters" class="display">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>a = <xsl:value-of select="./crystal/scalar[@title='a']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>b = <xsl:value-of select="./crystal/scalar[@title='b']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>c = <xsl:value-of select="./crystal/scalar[@title='c']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>α = <xsl:value-of select="./crystal/scalar[@title='alpha']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>β = <xsl:value-of select="./crystal/scalar[@title='beta']"/></td>
                                                </tr>
                                                <tr>
                                                    <td>γ = <xsl:value-of select="./crystal/scalar[@title='gamma']"/></td>
                                                </tr>
                                            </tbody>
                                        </table>    
                                    </div>
                                    <div class="col-md-4 col-sm-6 col-xs-12">
                                        <h5>Lattice vectors</h5>
                                        <table id="latticeVectors" class="display">
                                            <thead>
                                                <tr>
                                                    <th></th>
                                                    <th></th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <xsl:for-each select="(//module[@dictRef='cc:initialization']//module[@cmlx:templateRef='lattice'])[last()]/array">
                                                    <tr>
                                                        <xsl:for-each select="tokenize(.,'\s+')">
                                                            <td class="text-right pr-2"><xsl:value-of select="format-number(number(.),'#0.00000000')"/></td>                                                
                                                        </xsl:for-each>
                                                    </tr>
                                                </xsl:for-each>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </xsl:if>
                        <div class="col-lg-8 col-md-12">
                            <br/>
                            <div id="coordinateTypeDiv">
                                <span>Coordinate type :</span>
                                <input type="radio" name="coordinateType" value="both" checked="checked" onclick="changeMoleculeColumnVisibility()" />Both
                                <input type="radio" name="coordinateType" value="cartesian" onclick="changeMoleculeColumnVisibility()"/>Cartesian
                                <input type="radio" name="coordinateType" value="fractional" onclick="changeMoleculeColumnVisibility()"/>Fractional
                            </div>
                            <div id="coordinatesXYZ-{generate-id($molecule)}" class="right">
                                <a class="text-right" href="javascript:getXYZ()"><span class="text-right fa fa-download"/></a>
                            </div>
                            <!-- Build an XYZ-format-compatible table  -->
                            <table class="display" style="display:none" id="atomicCoordinatesXYZT-{generate-id($molecule)}">
                                <thead>
                                    <tr>
                                        <th><xsl:value-of select="count($molecule/cml:atomArray/cml:atom)"/></th>
                                        <th> </th>
                                        <th> </th>
                                        <th> </th>
                                    </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>

                            <table id="atomicCoordinates">
                                <thead>
                                    <tr>
                                        <th rowspan="2"/>
                                        <th rowspan="2"/>
                                        <th colspan="3" style="text-align:center">Cartesian coordinates</th>
                                        <th colspan="3" style="text-align:center">Fractional coordinates</th>
                                    </tr>
                                    <tr>
                                        <th style="text-align:right">x</th>
                                        <th style="text-align:right">y</th>
                                        <th style="text-align:right">z</th>
                                        <th style="text-align:right">u</th>
                                        <th style="text-align:right">v</th>
                                        <th style="text-align:right">w</th>
                                    </tr>
                                </thead>
                            </table>
                            <script type="text/javascript">
                                var coordinates = [<xsl:for-each select="$molecule/cml:atomArray/cml:atom"><xsl:variable name="outerIndex" select="position()"/>
                                    [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="@elementType"/>","<xsl:value-of select="format-number(@x3, '#0.0000')"/>","<xsl:value-of select="format-number(@y3, '#0.0000')"/>","<xsl:value-of select="format-number(@z3, '#0.0000')"/>","<xsl:value-of select="format-number(@xFract, '#0.0000')"/>","<xsl:value-of select="format-number(@yFract, '#0.0000')"/>","<xsl:value-of select="format-number(@zFract, '#0.0000')"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>
                                    </xsl:for-each>];

                                $(document).ready(function() {
                                    buildXYZTable();
                                    buildMolecularInfoTable();
                                    <xsl:if test="not(exists($molecule/cml:atomArray/cml:atom/@xFract))">   
                                    viewCartesianCoordinatesOnly();
                                    </xsl:if>                                    
                                });

                                function changeMoleculeColumnVisibility(){
                                    var mode = $('input[name=coordinateType]:checked').val();
                                    var dt = $('#atomicCoordinates').DataTable();
                                    if( mode == 'both'){
                                        dt.columns([2,3,4,5,6,7]).visible(true);
                                    }else if(mode == 'cartesian') {
                                        dt.columns([2,3,4]).visible(true); 
                                        dt.columns([5,6,7]).visible(false);
                                    }else {
                                        dt.columns([2,3,4]).visible(false); 
                                        dt.columns([5,6,7]).visible(true);
                                    }
                                    dt.columns.adjust().draw();
                                    // Adjust table width to its container
                                    dt = $('#atomicCoordinates').dataTable();
                                    $(dt).css({ width: $(dt).parent().width() });
                                    dt.fnAdjustColumnSizing();
                                }
                              

                                function buildMolecularInfoTable() {
                                    $('table#atomicCoordinates').dataTable({
                                        "aaData": coordinates,
                                        "aoColumns": [
                                            { "sTitle": "ATOM" },
                                            { "sTitle": "" },
                                            { "sTitle": "x", "sClass": "right" },
                                            { "sTitle": "y", "sClass": "right" },
                                            { "sTitle": "z", "sClass": "right" },
                                            { "sTitle": "u", "sClass": "right" },
                                            { "sTitle": "v", "sClass": "right" },
                                            { "sTitle": "w", "sClass": "right" }],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });                                
                                }
                                
                                function buildXYZTable() {
                                    var coordinatesXYZ = coordinates.map((val) => val.slice(1,5));
                                    coordinatesXYZ.unshift(['<xsl:value-of select="$title"/>','','','']);
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable({
                                        "aaData" : coordinatesXYZ,
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false
                                    });
                                    $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>-wrapper').hide();
                                }

                                function getXYZ() {
                                    var table = $('table#atomicCoordinatesXYZT-<xsl:value-of select="generate-id($molecule)"/>').DataTable();
                                    new $.fn.dataTable.Buttons( table, {
                                        buttons: [ {
                                            extend: 'copyHtml5',
                                            text: 'XYZ'
                                        }]
                                        });
                                    table.buttons().container().appendTo($('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/>'));
                                    $('div#coordinatesXYZ-<xsl:value-of select="generate-id($molecule)"/> span.fa').hide();
                                }
                                
                                function viewCartesianCoordinatesOnly(){
                                    $('input[name=coordinateType ][value=cartesian]').prop('checked', true); 
                                    changeMoleculeColumnVisibility();
                                    $('#coordinateTypeDiv').hide()
                                }
                                
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>

    <xsl:template name="printAtoms" match="cml:atom">        
            <xsl:variable name="elementType" select="./@elementType"/>
            <xsl:variable name="elementLabel" select="./@id" />
            <xsl:variable name="id" select="./@id"/>
            [<xsl:value-of select="position()"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3,'#0.0000')"/>","<xsl:value-of select="format-number(@y3,'#0.0000')"/>","<xsl:value-of select="format-number(@z3,'#0.0000')"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>                            
    </xsl:template>

    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="(//formula/@concise)[1]"/>
                </td>
            </tr>
           
            <tr>
                <td style="vertical-align: top;">Calculation type:</td>
                <td>Molecular Dynamics (<xsl:value-of select="$method"/>)             
                   <br/>                    
                    <xsl:copy-of select="helper:getCouplingMethods($ntt, $ntp, $barostatParam)"/>
                </td>                                
            </tr>          
        </table>        
    </xsl:template>
    
    <!-- Settings -->
    <xsl:template name="settings">
        <div class="row bottom-buffer">
            <div class="col-md-3 col-sm-6">
                <table class="display" id="timeSettings">
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th style="text-align:right">Value</th>
                        </tr>                        
                    </thead>
                    <tbody>                        
                        <xsl:copy-of select="helper:printParameterRow('dt','0.001')"/>
                        <xsl:copy-of select="helper:printParameterRow('nstlim', '1')"/>
                        <tr>
                            <td>ellapsed time (ps)</td>
                            <td class="text-right">
                                <xsl:variable name="dt" select="number(helper:readInputFileParameter($initialization, $inputLines, 'dt'))"/>
                                <xsl:variable name="nstlim" select="number(helper:readInputFileParameter($initialization, $inputLines, 'nstlim'))"/>
                                <xsl:value-of select="$dt*$nstlim"/>
                            </td>
                        </tr>                        
                    </tbody>
                </table>
                <script type="text/javascript">
                    $(document).ready(function() {                        
                        $('table#timeSettings').dataTable( {                                                      
                            "bFilter": false,
                            "bPaginate": false,
                            "bSort": false,
                            "bInfo": false
                        } );   
                    } );                                       
                </script>
            </div>
            <div class="col-md-3 col-sm-8">
                <table class="display" id="otherSettings">
                    <thead>
                        <tr>
                            <th>Parameter</th>
                            <th style="text-align:right">Value</th>
                        </tr>                        
                    </thead>
                    <tbody>                      
                        <xsl:copy-of select="helper:printParameterRow('taup','')"/>
                        <xsl:copy-of select="helper:printParameterRow('temp0','')"/>
                        <xsl:copy-of select="helper:printParameterRow('pres0','')"/>                        
                        <xsl:copy-of select="helper:printParameterRow('ntf','')"/>
                        <xsl:copy-of select="helper:printParameterRow('ntc','')"/>
                        <xsl:copy-of select="helper:printParameterRow('igb','')"/>
                        <xsl:copy-of select="helper:printParameterRow('cut','')"/>
                        <xsl:copy-of select="helper:printParameterRow('ibelly','')"/>
                        <xsl:copy-of select="helper:printParameterRow('ntr','')"/>
                        <xsl:copy-of select="helper:printParameterRow('gamma_ln','')"/>
                        <xsl:copy-of select="helper:printParameterRow('irest','')"/>
                        <xsl:copy-of select="helper:printParameterRow('ntb','')"/>
                    </tbody>
                </table>
                <script type="text/javascript">
                    $(document).ready(function() {                        
                    $('table#otherSettings').dataTable( {                                                      
                    "bFilter": false,
                    "bPaginate": false,
                    "bSort": false,
                    "bInfo": false
                    } );   
                    } );                                       
                </script>
            </div>
        </div>   
    </xsl:template>

    <xsl:function name="helper:printParameterRow">
        <xsl:param name="parameterName" as="xs:string"/>
        <xsl:param name="default" as="xs:string"/>     
        <!--<xsl:param name="mask" as="xs:string" />-->
        <xsl:variable name="param" select="helper:readInputFileParameter($initialization, $inputLines, $parameterName)" />
        <xsl:choose>
            <xsl:when test="exists($param) or $default != ''">
                <tr>          
                    <td><xsl:value-of select="$parameterName"/></td>
                    <td class="text-right"><xsl:value-of select="if(exists($param)) then string($param) else $default"/></td>
                </tr>
            </xsl:when>            
        </xsl:choose>
    </xsl:function>

    <!-- Averages -->
    <xsl:template name="averages">
        <xsl:variable name="properties" select="//cml:module[@dictRef='cc:finalization']//propertyList"/>
        <xsl:variable name="temperature" select="$properties/cml:property[@dictRef='am:temp']/cml:scalar"/>
        <xsl:variable name="pressure" select="$properties/cml:property[@dictRef='am:press']/cml:scalar"/>
        <xsl:if test="exists($properties)">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#averages-{generate-id($properties)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">Averages</h4>
                </div>
                <div id="averages-{generate-id($properties)}" class="panel-collapse collapse">
                    <div class="panel-body">                    
                        <div class="row bottom-buffer">
                            <div class="col-md-3 col-sm-12">
                                <table class="display" id="averagesAT-{generate-id($properties)}">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                </table>
                                <xsl:variable name="selectedProperties" select="'etot', 'ektot', 'eptot', 'bond', 'angle', 'dihed', 'nb14', 'eel14', 'vdwaals', 'eelec', 'ehbond', 'restraint', 'ekcmt', 'virial', 'volume', 'density'"/>
                                <xsl:variable name="labels" select="'Etot','EKtot','EPtot', 'Bond', 'Angle', 'Dihedral', '1-4 NB', '1-4 EEL', 'VDWaals', 'EELEC', 'EHBOND', 'Restraint', 'EKCMT', 'Virial', 'Volume', 'Density'"/>
                                <script>
                                    var averages = [<xsl:for-each select="1 to count($selectedProperties)">
                                                        <xsl:variable name="outerIndex" select="."/>
                                                        <xsl:variable name="property" select="helper:readPropertyAsArray($properties, $selectedProperties[$outerIndex], $labels[$outerIndex])"/>
                                                        <xsl:if test="$property != ''">                                                            
                                                            <xsl:if test="$outerIndex != 1">,</xsl:if>
                                                            <xsl:value-of select="$property"/>
                                                        </xsl:if>
                                                    </xsl:for-each>];
                                </script>

                                <script type="text/javascript">
                                    $(document).ready(function(){
                                        $("table#averagesAT-<xsl:value-of select="generate-id($properties)"/>").dataTable({
                                        "aaData" : averages,
                                        "aoColumns" : [
                                            {"sTitle" : "Type"},
                                            {"sTitle" : "Value"},
                                            ],
                                         "aoColumnDefs" : [
                                            { "sClass": "nowrap", "aTargets": [ 0 ] },
                                            { "sClass": "text-right", "aTargets": [ 1 ] }
                                            
                                            ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                        });
                                    }); 
                                </script>
                            </div>
                            <xsl:if test="exists($temperature) or exists($pressure)">
                                <div class="col-md-3 col-sm-12">
                                    <table class="display" id="propertiesT-{generate-id($properties)}">
                                        <thead>
                                            <tr>
                                                <th>Thermodynamic conditions</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Temperature (K)</td>
                                                <td><xsl:value-of select="$temperature"/></td>
                                            </tr>        
                                            <tr>
                                                <td>Pressure (bar)</td>
                                                <td><xsl:value-of select="$pressure"/></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    
                                    <script type="text/javascript">
                                        $(document).ready(function(){
                                        $("table#propertiesT-<xsl:value-of select="generate-id($properties)"/>").dataTable({                                    
                                            "aoColumns" : [
                                                {"sTitle" : "Thermodynamic conditions"},
                                                {"sTitle" : ""},
                                            ],
                                            "aoColumnDefs" : [
                                                { "sClass": "nowrap", "aTargets": [ 0 ] },
                                                { "sClass": "text-right", "aTargets": [ 1 ] }
                                            ],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false                                        
                                            });
                                        });
                                    </script>
                                </div>
                            </xsl:if>
                        </div>
                    </div>
                </div>
            </div>
            
            
        </xsl:if>
    </xsl:template>

    <!-- Timing -->
    <xsl:template name="timing">
        <xsl:variable name="gpu" select="$environment/cml:parameter[@dictRef='am:hardware']/cml:list[@cmlx:templateRef='gpu']" />
        <xsl:variable name="runDate" select="$environment/cml:parameter[@dictRef='cc:runDate']" />
        <xsl:variable name="endDate" select="$finalization/cml:property[@dictRef='cc:dateEnd']/cml:scalar" />
        <xsl:variable name="cpuTime" select="$finalization/cml:property[@dictRef='cc:cpuTime']" />
        <xsl:variable name="wallTime" select="$finalization/cml:property[@dictRef='cc:wallTime']" />
        <xsl:variable name="nsperday" select="$finalization/cml:property[@dictRef='am:nsperday']" />
        <xsl:variable name="secondsperns" select="$finalization/cml:property[@dictRef='am:secondsperns']" />
        <xsl:variable name="times" select="$finalization/cml:property[@dictRef='gm:time.accounting']/cml:list" />

        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#timing" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Timing
                </h4>
            </div>
            <div id="timing" class="panel-collapse collapse">
                <div class="panel-body">    
                    <div class="row bottom-buffer">
                        <xsl:if test="exists($gpu)">
                            <div class="col-lg-6 col-sm-12">
                                <div class="row">                                
                                    <div class="col-md-12">
                                        <table id="environment" class="display">
                                            <thead>
                                                <tr>
                                                    <th>Environment</th>
                                                    <th></th>
                                                </tr> 
                                            </thead>
                                            <tbody>
                                                <xsl:for-each select="1 to count($gpu/cml:list)">
                                                    <xsl:variable name="outerIndex" select="."/>
                                                    <tr>
                                                        <td><xsl:value-of select="$gpu/cml:list[$outerIndex]/cml:scalar[@dictRef='cc:parameter']"/></td>
                                                        <td><xsl:value-of select="$gpu/cml:list[$outerIndex]/cml:scalar[@dictRef='cc:value']"/></td>
                                                    </tr>                                                
                                                </xsl:for-each>
                                            </tbody>
                                        </table>
                                        <script type="text/javascript">
                                            $(document).ready(function() {                        
                                            $('table#environment').dataTable({
                                            "bFilter": false,                                 
                                            "bPaginate": false,                                    
                                            "bSort": false,
                                            "bInfo": false,
                                            "aoColumnDefs" : [{ "sClass": "text-right", "aTargets": [ 1 ] }]
                                            });
                                            });
                                        </script>
                                    </div>                               
                                </div>
                            </div>                                                        
                        </xsl:if>
                        <div class="col-lg-6 col-sm-12"> 
                            <table id="timingT">
                                <thead>
                                    <tr>
                                        <th>Timing</th>
                                        <th> </th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Start</td>
                                        <td><xsl:value-of select="replace(replace($runDate,'T',' '),'\.\d\d\d.*','')"/></td>
                                    </tr>
                                    <xsl:if test="exists($endDate)">
                                        <tr>
                                            <td>End</td>
                                            <td><xsl:value-of select="replace(replace($endDate,'T',' '),'\.\d\d\d.*','')"/></td>
                                        </tr>                                         
                                    </xsl:if>                                    
                                    <xsl:if test="exists($cpuTime)">
                                        <tr>
                                            <td>Cpu time</td>
                                            <td><xsl:value-of select="concat($cpuTime, ' ', helper:printUnitSymbol($cpuTime/cml:scalar/@units))"/></td>
                                        </tr>                                                
                                    </xsl:if>
                                    <xsl:if test="exists($wallTime)">
                                        <tr>
                                            <td>Wall time</td>
                                            <td><xsl:value-of select="concat($wallTime, ' ', helper:printUnitSymbol($wallTime/cml:scalar/@units))"/></td>                        
                                        </tr>    
                                    </xsl:if>
                                    <xsl:if test="exists($nsperday)">
                                        <tr>
                                          <td>ns per day</td>
                                          <td><xsl:value-of select="$nsperday"/></td>                        
                                        </tr>  
                                    </xsl:if>
                                    <xsl:if test="exists($secondsperns)">
                                        <tr>
                                            <td>Seconds per ns</td>
                                            <td><xsl:value-of select="$secondsperns"/></td>                        
                                        </tr>
                                    </xsl:if>
                                </tbody>
                            </table>
                            <script type="text/javascript">
                                $(document).ready(function() {                        
                                $('table#timingT').dataTable( {
                                "bFilter": false,                                 
                                "bPaginate": false,                                    
                                "bSort": false,
                                "bInfo": false,
                                "aoColumnDefs" : [                                    
                                { "sClass": "text-right", "aTargets": [ 1 ] }
                                ]
                                } );
                                } );
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </xsl:template>

    <xsl:function name="helper:readInputFileParameter">
        <xsl:param name="initialization" />
        <xsl:param name="inputLines"/>
        <xsl:param name="parameterName" />
        
        <!-- Will try reading from CONTROL section, otherwise from input lines section -->
        <xsl:choose>
            <xsl:when test="exists($initialization/cml:parameterList/cml:parameter[@dictRef= concat('am:', $parameterName)])">
                <xsl:value-of select="$initialization/cml:parameterList/cml:parameter[@dictRef= concat('am:', $parameterName)]"/>
            </xsl:when>
            <xsl:when test="exists($inputLines/cml:scalar[@dictRef='cc:inputLine'][matches(lower-case(text()),concat('^\s*', $parameterName, '\s*[=:].*'))])">
                <xsl:variable name="parameter" select="$inputLines/cml:scalar[@dictRef='cc:inputLine'][matches(lower-case(text()),concat('^\s*', $parameterName, '\s*[=:].*'))]"/>                                
                <xsl:value-of select="helper:trim(tokenize($parameter, '[=:,]')[2])"/>
            </xsl:when>                       
        </xsl:choose>
    </xsl:function>   
    
    <xsl:function name="helper:readPropertyAsArray">
        <xsl:param name="properties" />
        <xsl:param name="property" />
        <xsl:param name="label" />        
        <xsl:choose>
            <xsl:when test="exists($properties/cml:property[@dictRef= concat('am:', $property )])">
                ['<xsl:value-of select="$label"/>', <xsl:value-of select="$properties/cml:property[@dictRef= concat('am:', $property)]"/>]
            </xsl:when>
        </xsl:choose>
    </xsl:function>
   
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>            
        </div>
    </xsl:template>    
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>    
    
</xsl:stylesheet>
