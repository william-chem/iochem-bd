<!--

    Create module - Create module inside the ioChem-BD software.
    Copyright © 2014 ioChem-BD (contact@iochem-bd.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

-->
<xsl:stylesheet
    xmlns:xsl="http://www.w3.org/1999/XSL/Transform" 
    xmlns:xd="http://www.oxygenxml.com/ns/doc/xsl"
    xmlns:cml="http://www.xml-cml.org/schema"
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    xmlns:cmlx="http://www.xml-cml.org/schema/cmlx"
    xmlns:ckbk="http://my.safaribooksonline.com/book/xml/0596009747/numbers-and-math/77"
    xmlns:vasp="https://www.vasp.at/"
    xmlns:helper="http://www.w3.org/1999/XSL/Helper-Functions"

    xpath-default-namespace="http://www.xml-cml.org/schema"
    exclude-result-prefixes="xs xd cml ckbk vasp helper cmlx"
    version="2.0">

    <xd:doc scope="stylesheet">
        <xd:desc>
            <xd:p><xd:b>Created on:</xd:b>March 26 , 2014</xd:p>
            <xd:p><xd:b>Author:</xd:b>Moisés Álvarez Moreno</xd:p>
            <xd:p><xd:b>Center:</xd:b>Universitat Rovira i Virgili</xd:p>            
        </xd:desc>
    </xd:doc>
    <xsl:include href="helper/chemistry/vasp.xsl"/>
    <xsl:include href="helper/chemistry/helper.xsl"/>
    <xsl:output method="html" version="5.0" encoding="utf-8" indent="yes" omit-xml-declaration="yes" />
    <xsl:strip-space elements="*"/>

    <xsl:param name="webrootpath"/>
    <xsl:param name="title"/>
    <xsl:param name="author"/>
    <xsl:param name="browseurl"/>    
    <xsl:param name="jcampdxurl"/>    
    <xsl:param name="xyzurl"/>

    <xsl:variable name="ibrion" select="//module[@id='job']/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ibrion']/scalar"/>    
    <xsl:variable name="calcType" select="vasp:getCalcType($ibrion)"/>    
    <xsl:variable name="ispin" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ispin']/scalar"/>
    <xsl:variable name="shellType" select="vasp:getShellType($ispin)"/>
    <xsl:variable name="ediff" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ediff']/scalar"/>
    <xsl:variable name="ediffg" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ediffg']/scalar"/>
    <xsl:variable name="potim" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:potim']/scalar"/>
    <xsl:variable name="ldipol" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ldipol']/scalar"/>
    <xsl:variable name="idipol" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:idipol']/scalar"/>
    <xsl:variable name="grimmes" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@id='otherComponents']/module[@cmlx:templateRef='grimmes']"/>

    <xsl:variable name="energyCutoff" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:energyCutoff']/scalar"/>
    
    <xsl:variable name="isNeb" select="count($ibrion) > 1 and not(exists($ibrion[text() = '44']))"/>

    <!-- Functional calculation -->   
    <xsl:variable name="gga"      select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:gga']/scalar"/>
    <xsl:variable name="lhfcalc"  select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:lhfcalc']/scalar"/>
    <xsl:variable name="ldau"     select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ldau']/scalar"/>
    <xsl:variable name="aggac"    select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aggac']/scalar"/>
    <xsl:variable name="hfscreen" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:hfscreen']/scalar"/>    
    <xsl:variable name="luseVdw"  select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:luseVdw']/scalar"/>
    <xsl:variable name="zabVdw"   select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:zabVdw']/scalar"/>
    <xsl:variable name="param1"   select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:param1']/scalar"/>    
    <xsl:variable name="param2"   select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:param2']/scalar"/>    
    <xsl:variable name="aexx" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aexx']/scalar"/>
    <xsl:variable name="aggax" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aggax']/scalar"/>
    <xsl:variable name="aldac" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:aldac']/scalar"/>
    <xsl:variable name="functional" select="vasp:getFunctional($gga,$lhfcalc,$aggac,$hfscreen,$luseVdw,$zabVdw, $param1, $param2, $ldau, $aexx, $aggax, $aldac)"/>
    
    <!-- Environment module -->
    <xsl:variable name="programParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:program']"/>
    <xsl:variable name="versionParameter" select="//module[@id='job'][1]/module[@id='environment']/parameterList/parameter[@dictRef='cc:programVersion']"/>
    <!-- Initialization module -->    
    <xsl:variable name="multiplicity" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:nupdown']/scalar"/>
    <xsl:variable name="sigma"        select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:sigma']/scalar"/>
    <xsl:variable name="ismear"       select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ismear']/scalar"/>
    <xsl:variable name="nelect"       select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:nelect']/scalar"/>
    <xsl:variable name="kpoints"      select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='kpoints']"/>
    <xsl:variable name="potcar"       select="(//module[@id='job'][1]/module[@dictRef='cc:initialization']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='potcar'])[1]"/>
    <xsl:variable name="temperature" select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='cc:temp']/scalar"/>
    <!-- Calculation module -->
    <xsl:variable name="forces" select="//module[@id='job'][1]/module[@dictRef='cc:calculation']/module[@dictRef='cc:userDefinedModule']/module[@id='forces']"/>
    <xsl:variable name="dos" select="//module[@id='job'][1]/module[@dictRef='cc:calculation']/module[@dictRef='cc:userDefinedModule']/module[@id='vasp.doscar']"/>    
    <!-- Finalization module -->
    <xsl:variable name="magnetizations" select="//module[@dictRef='cc:finalization']/propertyList/property/module[@cmlx:templateRef='magnetization']"/>
    <xsl:variable name="vibrations" select="(//module[@dictRef='cc:finalization']/propertyList/property/module[@cmlx:templateRef='vibrations'])[last()]"/>        
    <!-- Geometry -->
    <xsl:variable name="energies" select="//module[@id='job']/module[@dictRef='cc:finalization']//property[@dictRef='cc:energies']/module[@cmlx:templateRef='energies']"/>
    <xsl:variable name="finalMolecules" select="//module[@dictRef='cc:finalization' and child::molecule]//molecule"/>    
    <xsl:variable name="isLargeMolecule" select="count($finalMolecules[1]/atomArray/atom) &gt; 1000" />

    <xsl:template match="/">
        <html lang="en">
            <head>
                <title><xsl:value-of select="$title"/></title>
                <meta charset="utf-8"></meta>
                <meta name="viewport" content="width=device-width, initial-scale=1.0"></meta>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/popper.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery-3.3.1.min.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/js/jquery.blockUI.js" />
                <script type="text/javascript" src="{$webrootpath}/xslt/datatables/datatables.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/bootstrap.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/js/plotly-latest.min.js"/>
                <script type="text/javascript" src="{$webrootpath}/xslt/jsmol/JSmol.min.nojq.js"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/font-awesome.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/datatables/datatables.min.css" type="text/css" />
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap.min.css" type="text/css"/>
                <link rel="stylesheet" href="{$webrootpath}/xslt/css/bootstrap-theme.css" type="text/css"/>
                
                <rdf:RDF xmlns="http://creativecommons.org/ns#" xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#">
                    <Work xmlns:dc="http://purl.org/dc/elements/1.1/" rdf:about="">
                        <license rdf:resource="http://creativecommons.org/licenses/by-nc-nd/4.0/"/>
                    </Work>
                    <License rdf:about="http://creativecommons.org/licenses/by-nc-nd/4.0/">
                        <permits rdf:resource="http://creativecommons.org/ns#Distribution"/>
                        <permits rdf:resource="http://creativecommons.org/ns#Reproduction"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Attribution"/>
                        <requires rdf:resource="http://creativecommons.org/ns#Notice"/>                        
                    </License>
                </rdf:RDF>
            </head>
            <body>
                <script type="text/javascript">                   
                    $.blockUI();                    
                </script>
                <div id="container">                 
                    <!-- General Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">
                            <xsl:call-template name="generalInfo"/>                              
                        </div>
                    </div>
                    <!-- Setttings section -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>SETTINGS</h3>
                            <div class="row bottom-buffer">
                                <div class="col-md-3 col-sm-6">
                                    <table class="display" id="electronicSmearing">
                                        <tbody>
                                            <tr>
                                                <td>SIGMA:</td>
                                                <td class="right">
                                                    <xsl:value-of select="$sigma"/>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>ISMEAR:</td>
                                                <td class="right">
                                                    <xsl:value-of select="$ismear"/>
                                                </td>
                                            </tr>
                                            <xsl:if test="exists($ldipol)">
                                                <tr>
                                                    <td>LDIPOL:</td>
                                                    <td class="right"><xsl:value-of select="$ldipol"/></td>
                                                </tr>                                               
                                            </xsl:if>
                                            <xsl:if test="exists($idipol)">
                                                <tr>
                                                    <td>IDIPOL:</td>
                                                    <td class="right"><xsl:value-of select="$idipol"/></td>
                                                </tr>    
                                            </xsl:if>
                                                                                       
                                            <xsl:if test="exists($multiplicity) and $multiplicity &gt; -1">
                                                <tr>
                                                    <td>Multiplicity :</td>
                                                    <td class="right"><xsl:value-of select="$multiplicity"/></td>                                
                                                </tr>
                                            </xsl:if>
                                            <tr>
                                                <td>NELECT:</td>
                                                <td class="right"><xsl:value-of select="$nelect"/></td>                            
                                            </tr>
                                            <xsl:if test="exists($energyCutoff)">
                                                <tr>
                                                    <td>ENCUT:</td>
                                                    <td class="right"><xsl:value-of select="format-number($energyCutoff,'#0.00')"/></td>
                                                </tr>                                               
                                            </xsl:if>
                                            <tr>
                                                <td>EDIFF:</td>
                                                <td class="right"><xsl:value-of select="$ediff"/></td>
                                            </tr>
                                            <xsl:if test="not(($calcType = $vasp:FrequencyCalculus ) or ( $calcType = $vasp:SinglePoint)) ">
                                                <tr>
                                                    <td>EDIFFG:</td>
                                                    <td class="right"><xsl:value-of select="$ediffg"/></td>
                                                </tr>
                                            </xsl:if>
                                            <tr>
                                                <td>POTIM:</td>
                                                <td class="right"><xsl:value-of select="$potim"/></td>
                                            </tr>
                                            <xsl:if test="exists(//parameter[contains(@dictRef,'v:ldau')])">
                                                <tr>
                                                    <td>LDAUL:</td>
                                                    <td class="right"><xsl:value-of select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ldaul']/array"/></td>
                                                </tr>                                               
                                                <tr>
                                                    <td>LDAUU:</td>
                                                    <td class="right"><xsl:value-of select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ldauu']/array"/></td>
                                                </tr>
                                                <tr>
                                                    <td>LDAUJ:</td>
                                                    <td class="right"><xsl:value-of select="//module[@id='job'][1]/module[@dictRef='cc:initialization']/parameterList/parameter[@dictRef='v:ldauj']/array"/></td>
                                                </tr>
                                            </xsl:if>
                                            <xsl:if test="exists(//parameter[contains(@dictRef,'v:lvdw')]) and compare(//parameter[contains(@dictRef,'v:lvdw')]/scalar,'true') = 0">
                                                <tr>
                                                    <td>LVDW:</td>
                                                    <td class="right">T</td>                                                                                                        
                                                </tr>
                                                <tr>
                                                    <td>VDW VERSION:</td>
                                                    <td class="right">D
                                                        <xsl:choose>
                                                            <xsl:when test="not(exists(//parameter[contains(@dictRef,'v:vdwversion')]))">
                                                                2
                                                            </xsl:when>
                                                            <xsl:otherwise>
                                                                <xsl:value-of select="//parameter[contains(@dictRef,'v:vdwversion')]"/>
                                                            </xsl:otherwise>
                                                        </xsl:choose>
                                                    </td>
                                                </tr>
                                            </xsl:if>
                                        </tbody>
                                    </table>
                                </div>                                 
                            </div>  
                            <xsl:if test="exists($grimmes)">
                            <div class="row bottom-buffer">
                                <div class="col-md-4 col-sm-12">                             
                                    <xsl:variable name="atomType" select="tokenize($grimmes/array[@dictRef='cc:elementType'],'\s+')"/>
                                    <xsl:variable name="grimmeC6" select="tokenize($grimmes/array[@dictRef='v:grimmeC6'],'\s+')"/>
                                    <xsl:variable name="grimmeR0" select="tokenize($grimmes/array[@dictRef='v:grimmeR0'],'\s+')"/>
                                    <br/>
                                    <p>Parameters for Grimme's potential</p>
                                    <table class="display dataTable compact">
                                        <thead>
                                            <tr>
                                                <th>Atom</th>
                                                <th class="right">C6(Jnm^6/mol)</th>
                                                <th class="right">R0(A)</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <xsl:for-each select="1 to count($atomType)">
                                                <xsl:variable name="outerIndex" select="."/>
                                                <tr>
                                                    <td><xsl:value-of select="$atomType[$outerIndex]"/></td>
                                                    <td class="right"><xsl:value-of select="$grimmeC6[$outerIndex]"/></td>
                                                    <td class="right"><xsl:value-of select="$grimmeR0[$outerIndex]"/></td>
                                                </tr>    
                                            </xsl:for-each>                                                                                                
                                        </tbody>                                            
                                    </table>
                                </div>
                            </div>                          
                            </xsl:if>
                        </div>
                    </div>                    
                    
                    <!-- Atom Info -->
                    <div class="row bottom-buffer">
                        <div class="col-md-12">                            
                            <h3>ATOM INFO</h3>                         
                            <div>
                               <xsl:call-template name="atomicCoordinates">
                                   <xsl:with-param name="molecules" select="$finalMolecules"/>                                                                                                                          
                               </xsl:call-template>
                            </div>
                        </div>
                    </div>
                    <!-- Molecular Info -->                    
                    <xsl:if test="exists($kpoints)">
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <h3>MOLECULAR INFO</h3>                                                               
                                <xsl:call-template name="kpoints"/>
                            </div>
                        </div>    
                    </xsl:if>                                        
                    <!-- Results -->
                    <div class="row bottom-buffer">                     
                        <div class="col-md-12">                                                                                          
                            <div id="module-{generate-id(//module[@id='job'][1])}">                                
                                <h3>JOB <small><a href="javascript:collapseModule('module-{generate-id(//module[@id='job'][1])}')"><span class="fa fa-chevron-up"></span></a> | <a href="javascript:expandModule('module-{generate-id(//module[@id='job'][1])}')"><span class="fa fa-chevron-down"></span></a></small></h3>
                                <xsl:call-template name="energy"/>
                                <xsl:call-template name="eigenvalues"/>
                                <xsl:call-template name="dos"/>        
                                <xsl:call-template name="magnetization"/>
                                <xsl:call-template name="vibrations"/>
                                <xsl:call-template name="structure"/>
                                <xsl:call-template name="molecularDynamics"/>
                                <br/>
                            </div>
                            <xsl:call-template name="printLicense"/>
                        </div>
                    </div> 
                    <script type="text/javascript">
                        function expandModule(moduleID){                        
                            $('div#' + moduleID + ' div.panel-collapse:not(.show)').collapse('show');
                        }
                        
                        function collapseModule(moduleID){
                            $('div#' + moduleID + ' div.panel-collapse.show').collapse('hide');
                        }   
                        
                        $(document).ready(function() {
                            //Add custom styles to tables
                            $('div.dataTables_wrapper').each(function(){ 
                                $(this).children("table").addClass("compact");
                                $(this).children("table").addClass("display");
                            });      
                            //Add download action to every dataTable                             
                            $('div.dataTables_wrapper').each(function(){ 
                                var tableName = $(this).children("table").attr("id");
                                if(tableName != null){
                                    var jsTable = "javascript:showDownloadOptions('" + tableName + "');"
                                    $('<div id="downloadTable'+ tableName + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableName +'_wrapper');
                                }
                            });
                            $.unblockUI();                             
                        });

                        function showDownloadOptions(tableName){                            
                            var table = $('#' + tableName).DataTable();                                                    
                            new $.fn.dataTable.Buttons( table, {
                                buttons: [ 'copy', 'csv', 'excel', 'pdf',  'print' ]
                            } );                            
                            table.buttons().container().appendTo($('div#downloadTable' + tableName) );
                            $('div#downloadTable' + tableName + ' span.fa').hide();                        
                        }

                        
                        $(window).resize(function() {
                            clearTimeout(window.refresh_size);
                            window.refresh_size = setTimeout(function() { update_size(); }, 250);
                        });
                        
                        //Resize all tables on window resize to fit new width
                        var update_size = function() {                        
                           $('.dataTable').each(function(index){
                             var oTable = $(this).dataTable();
                             $(oTable).css({ width: $(oTable).parent().width() });
                             oTable.fnAdjustColumnSizing();                           
                           });                                                     
                        }
                        
                        //On expand accordion we'll resize inner tables to fit current page width 
                        $('.panel-collapse').on('shown.bs.collapse', function () {                            
                            $(this).find('.dataTable').each(function(index){
                                var oTable = $(this).dataTable();
                                $(oTable).css({ width: $(oTable).parent().width() });
                                oTable.fnAdjustColumnSizing();                           
                            });  
                        })                        
                        
                    </script>                    
                </div>
            </body>
        </html>
    </xsl:template>
    
    <!-- General Info -->
    <xsl:template name="generalInfo">
        <div class="page-header">
            <h3>GENERAL INFO</h3>
        </div>        
        <table>
            <xsl:if test="$title">
                <tr>
                    <td>Title:</td>
                    <td>
                        <xsl:value-of select="$title"/>
                    </td>
                </tr>                                   
            </xsl:if>
            <xsl:if test="$browseurl">
                <tr>
                    <td>Browse item:</td>
                    <td>
                        <a href="{$browseurl}">
                            <xsl:value-of select="$browseurl"/>
                        </a>
                    </td>
                </tr>
            </xsl:if>   
            <tr>
                <td>Program:</td>
                <td>
                    <xsl:value-of select="$programParameter/scalar/text()"/>                                        
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="$versionParameter/scalar/text()" />
                </td>
            </tr>
            <xsl:if test="$author">
                <tr>
                    <td>Author:</td>
                    <td>
                        <xsl:value-of select="$author"/>
                    </td>
                </tr>
            </xsl:if>
            <tr>
                <td>Formula:</td>
                <td>
                    <xsl:value-of select="helper:calculateHillNotationFormula($finalMolecules[1]/atomArray/atom)"/>
                </td>
            </tr>                               
            <tr>
                <td>Calculation type:</td>
                <td>
                    <xsl:value-of select="$calcType"/>                                        
                </td>
            </tr>
            <tr>
                <td>Functional:</td>
                <td><xsl:value-of select="$functional"/></td>
            </tr>
            <tr>
                <td>Shell type:</td>
                <td>
                    <xsl:value-of select="$shellType"/> (ISPIN <xsl:value-of select="$ispin"/>)                    
                </td>
            </tr>
            
            <xsl:if test="exists($temperature)">                                  
                <tr>
                    <td>                                           
                        <xsl:text>Temperature:</xsl:text>
                    </td>                                   
                    <td>                                            
                        <xsl:value-of select="$temperature/text()"></xsl:value-of>   
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="helper:printUnitSymbol($temperature/@units)"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <xsl:text>Pressure:</xsl:text>
                    </td>
                    <td>
                        <xsl:value-of select="'N/A'"/>
                        <xsl:text> </xsl:text>
                        <xsl:value-of select="'N/A'"></xsl:value-of>
                    </td>
                </tr>
            </xsl:if>            
        </table>        
    </xsl:template>
    
    <!-- Atomic coordinates -->
    <xsl:template name="atomicCoordinates">        
        <xsl:param name="molecules"/>      
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#atomicCoordinatesCollapse" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">                        
                    Atomic coordinates [&#8491;]                                                 
                </h4>
            </div>
            <div id="atomicCoordinatesCollapse" class="panel-collapse collapse">
                <div class="panel-body">
                    <div class="row bottom-buffer">
                        <div class="col-lg-12 col-md-12 col-sm-12">                            
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h5>Cell parameters:</h5>
                                <table id="cellParameters" class="display">
                                    <thead>
                                        <tr>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>a = <xsl:value-of select="$molecules[1]/crystal/scalar[@title='a']"/></td>
                                        </tr>
                                        <tr>
                                            <td>b = <xsl:value-of select="$molecules[1]/crystal/scalar[@title='b']"/></td>
                                        </tr>
                                        <tr>
                                            <td>c = <xsl:value-of select="$molecules[1]/crystal/scalar[@title='c']"/></td>
                                        </tr>
                                        <tr>
                                            <td>α = <xsl:value-of select="$molecules[1]/crystal/scalar[@title='alpha']"/></td>
                                        </tr>
                                        <tr>
                                            <td>β = <xsl:value-of select="$molecules[1]/crystal/scalar[@title='beta']"/></td>
                                        </tr>
                                        <tr>
                                            <td>γ = <xsl:value-of select="$molecules[1]/crystal/scalar[@title='gamma']"/></td>                                  
                                        </tr>
                                    </tbody>
                                </table>    
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h5>Lattice vectors</h5>
                                <table id="latticeVectors" class="display">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <xsl:for-each select="(//module[@dictRef='cc:initialization']//module[@cmlx:templateRef='lattice'])[last()]/array">
                                            <tr>
                                                <xsl:for-each select="tokenize(.,'\s+')">
                                                    <td><xsl:value-of select="."/></td>                                                
                                                </xsl:for-each>
                                            </tr>
                                        </xsl:for-each>
                                    </tbody>
                                </table>                                
                            </div>
                            <xsl:if test="exists($potcar/array[@dictRef='cc:valence'])">
                                <div class="col-md-4 col-sm-6 col-xs-12">
                                    <h5>Nuclei charge</h5>                                 
                                    <xsl:variable name="element" select="tokenize($potcar/array[@dictRef='cc:atomType'],'\s+')"/>
                                    <xsl:variable name="valence" select="tokenize($potcar/array[@dictRef='cc:valence'],'\s+')"/>
                                    <table id="nucleiCharge" class="display"></table>                            
                                    <script type="text/javascript">
                                        $(document).ready(function() {                        
                                        $('table#nucleiCharge').dataTable( {
                                        "aaData": [
                                        <xsl:for-each select="1 to count($element)">  
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            ["<xsl:value-of select="$element[$outerIndex]"/>","<xsl:value-of select="$valence[$outerIndex]"/>"]<xsl:if test="$outerIndex &lt; count($element)"><xsl:text>,</xsl:text></xsl:if>
                                        </xsl:for-each>    
                                        ],
                                        "aoColumns" : [ 
                                            {"sTitle" : "Element"},
                                            {"sTitle" : "Valence"} ],
                                        "bFilter": false,
                                        "bPaginate": false,
                                        "bSort": false,
                                        "bInfo": false                                    
                                        } );   
                                        } );                     
                                    </script>
                                </div>
                            </xsl:if>

                            <div class="col-md-12 col-sm-12 mt-3">                                
                                <xsl:if test="count($molecules) > 1"> <!-- Multiple molecules -->
                                    <div class="mb-2">                                    
                                        Images :                                    
                                        <xsl:for-each select="1 to count($molecules)">
                                            <xsl:variable name="outerIndex" select="position()"/>                                        
                                            <button class="btn btn-default" type="button" onclick="javascript:buildMolecularInfoTable({$outerIndex});loadMolecularInfoTableDownloadButtons();">
                                                <xsl:value-of select="format-number($outerIndex,'00')"/>                                           
                                            </button>
                                        </xsl:for-each>
                                    </div>
                                </xsl:if>                               
                                
                                Coordinate type : 
                                <input type="radio" name="coordinateType" value="both" checked="checked" onclick="javascript:buildMolecularInfoTable();loadMolecularInfoTableDownloadButtons()"/>Both 
                                <input type="radio" name="coordinateType" value="cartesian" onclick="javascript:buildMolecularInfoTable();loadMolecularInfoTableDownloadButtons()"/>Cartesian 
                                <input type="radio" name="coordinateType" value="fractional" onclick="javascript:buildMolecularInfoTable();loadMolecularInfoTableDownloadButtons()"/>Fractional
                                <div class="row">
                                    <div id="atomicCoordinatesDiv" class="col-md-12">
                                        <table id="atomicCoordinates" class="display">
                                            <thead>
                                                <tr>
                                                    <th rowspan="2"/>
                                                    <th rowspan="2"/>
                                                    <th colspan="3" style="text-align:center">Cartesian coordinates</th>
                                                    <th colspan="3" style="text-align:center">Fractional coordinates</th>
                                                    <th rowspan="2" style="text-align:center">Pseudopotential</th>
                                                </tr>
                                                <tr>
                                                    <th style="text-align:right">x</th>
                                                    <th style="text-align:right">y</th>
                                                    <th style="text-align:right">z</th>
                                                    <th style="text-align:right">u</th>
                                                    <th style="text-align:right">v</th>
                                                    <th style="text-align:right">w</th>
                                                </tr>
                                            </thead>
                                        </table>                                                
                                    </div>
                                    <div id="atomicCoordinatesTypeFilteredDiv" class="col-md-8 col-sm-12">
                                        <table id="atomicCoordinatesTypeFiltered" class="display compact">
                                            <thead>                                         
                                                <tr>
                                                    <th/>
                                                    <th/>
                                                    <th style="text-align:right">x</th>
                                                    <th style="text-align:right">y</th>
                                                    <th style="text-align:right">z</th>
                                                    <th style="text-align:center">Basis</th>
                                                </tr>
                                            </thead>
                                        </table>      
                                    </div>
                                </div> 
                                <style type="text/css">
                                    .cellpaddingleft {
                                        padding-left: 20px !important;                                   
                                    }                                
                                </style>    
                               
                                <script type="text/javascript">
                                    $(document).ready(function() {                                        
                                        $('#latticeVectors').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                        $('#cellParameters').dataTable({
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false
                                        });
                                        buildMolecularInfoTable(1);
                                    });   
                                <xsl:variable name="basisPerElement">
                                    <node>
                                        <xsl:variable name="elements" select="tokenize($potcar/array[@dictRef = 'cc:atomType'], '\s+')"/>
                                        <xsl:variable name="atomsPerType" select="tokenize($potcar/array[@dictRef = 'cc:atomcount'], '\s+')"/>
                                        <xsl:variable name="basis" select="tokenize(replace($potcar/array[@dictRef = 'v:pseudopotential'], '\n', ''), '\|')"/>                                                                               
                                        <xsl:for-each select="1 to count($elements)">
                                            <xsl:variable name="outerIndex" select="position()"/>
                                            <xsl:for-each select="1 to xs:integer($atomsPerType[$outerIndex])">                                                                                        
                                                <element type="{$elements[$outerIndex]}"><xsl:value-of select="$basis[$outerIndex]"/></element>
                                            </xsl:for-each>
                                        </xsl:for-each>      
                                    </node>    
                                </xsl:variable>                                                                
                                <xsl:for-each select="$molecules">
                                    <xsl:variable name="molecule" select="."/>
                                    <xsl:variable name="index" select="position()"/>
                                    var molecular_data<xsl:value-of select="$index"/> = [ <xsl:for-each select="$molecule/cml:atomArray/cml:atom">
                                                                                            <xsl:variable name="elementType" select="./@elementType"/>                                                      
                                                                                            <xsl:variable name="id" select="@id"/>
                                                                                            <xsl:variable name="outerIndex" select="position()"/>
                                                                                            <xsl:variable name="basis" select="$basisPerElement/node()/node()[$outerIndex]/text()"/>
                                                                                           [<xsl:value-of select="$outerIndex"/>,"<xsl:value-of select="$elementType"/>","<xsl:value-of select="format-number(@x3, '#0.0000')"/>","<xsl:value-of select="format-number(@y3, '#0.0000')"/>","<xsl:value-of select="format-number(@z3, '#0.0000')"/>","<xsl:value-of select="format-number(@xFract, '#0.0000')"/>","<xsl:value-of select="format-number(@yFract, '#0.0000')"/>","<xsl:value-of select="format-number(@zFract, '#0.0000')"/>","<xsl:value-of select="$basis"/>"]<xsl:if test="(position() &lt; count($molecule/cml:atomArray/cml:atom))"><xsl:text>,</xsl:text></xsl:if>                                                                            
                                                                                          </xsl:for-each>]; 
                                </xsl:for-each>                                    
                                    var currentMolecularModel = 1;
                                    
                                    function buildMolecularInfoTable(index){                                                                             
                                        if(index != null)
                                            currentMolecularModel = index;                                             
                                        if( $("input:radio[name ='coordinateType']:checked").val() == 'both') {
                                            buildAtomicCoordinatesTable(currentMolecularModel);
                                            $("div#atomicCoordinatesDiv").show();
                                            $("div#atomicCoordinatesTypeFilteredDiv").hide();
                                            
                                            var dt = $('#atomicCoordinates').DataTable();
                                            dt.columns.adjust().draw();
                                            // Adjust table width to its container
                                            dt = $('#atomicCoordinates').dataTable();
                                            $(dt).css({ width: $(dt).parent().width() });
                                            dt.fnAdjustColumnSizing();                                            
                                            
                                        } else {
                                            buildAtomicCoordinatesTypeFilteredTable(currentMolecularModel);
                                            $("div#atomicCoordinatesDiv").hide();
                                            $("div#atomicCoordinatesTypeFilteredDiv").show();
                                            
                                            var dt = $('#atomicCoordinatesTypeFiltered').DataTable();
                                            dt.columns.adjust().draw();
                                            // Adjust table width to its container
                                            dt = $('#atomicCoordinatesTypeFiltered').dataTable();
                                            $(dt).css({ width: $(dt).parent().width() });
                                            dt.fnAdjustColumnSizing(); 
                                        }
                                    }

                                    function loadMolecularInfoTableDownloadButtons(){
                                        var tableId = 'atomicCoordinates';
                                        var jsTable = "javascript:showDownloadOptions('" + tableId + "');"
                                        $('div#downloadTable' + tableId).remove();
                                        $('<div id="downloadTable'+ tableId + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableId +'_wrapper'); 
                                        
                                        var tableId = 'atomicCoordinatesTypeFiltered';
                                        var jsTable = "javascript:showDownloadOptions('" + tableId + "');"
                                        $('div#downloadTable' + tableId).remove();
                                        $('<div id="downloadTable'+ tableId + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"></span></a></div>').insertBefore('div#' + tableId +'_wrapper');
                                    }

                                    function buildAtomicCoordinatesTable(index){
                                        $('table#atomicCoordinates').dataTable( {
                                            "aaData": window['molecular_data' + index],
                                            "aoColumns": [                                          
                                                {"sTitle":""},
                                                {"sTitle":"Atom", "sClass": "left"},
                                                {"sTitle":"x", "sClass": "right"},
                                                {"sTitle":"y", "sClass": "right"},
                                                {"sTitle":"z", "sClass": "right"},                                         
                                                {"sTitle":"u", "sClass": "right"},
                                                {"sTitle":"v", "sClass": "right"},
                                                {"sTitle":"w", "sClass": "right"},                                          
                                                {"sTitle":"Basis", sClass: "left cellpaddingleft"}],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "bDestroy": true
                                        });
                                    }

                                    function buildAtomicCoordinatesTypeFilteredTable(index, type){
                                        var type = $("input:radio[name ='coordinateType']:checked").val();
                                        var molecular_data = (window['molecular_data' + index]).map(function(obj) {
                                            if(type == 'cartesian'){
                                                var obj2 = obj.slice();
                                                obj2.splice(5,3);
                                                return obj2;
                                            }
                                            if(type == 'fractional'){
                                                var obj2 = obj.slice();
                                                obj2.splice(2,3);
                                                return obj2;
                                            }
                                        });
                                    
                                        $('table#atomicCoordinatesTypeFiltered').dataTable({
                                            "aaData": molecular_data,
                                            "aoColumns": [                                          
                                                {"sTitle":""},
                                                {"sTitle":"Atom", "sClass": "left"},                                                                                      
                                                {"sTitle": type == 'cartesian'? "x" : "u", "sClass": "right"},
                                                {"sTitle": type == 'cartesian'? "y" : "v", "sClass": "right"},
                                                {"sTitle": type == 'cartesian'? "z" : "w", "sClass": "right"},                                          
                                                {"sTitle":"Basis", sClass: "left cellpaddingleft"}],
                                            "bFilter": false,
                                            "bPaginate": false,
                                            "bSort": false,
                                            "bInfo": false,
                                            "bDestroy": true
                                        });
                                    }                                    
                            </script>  
                            </div>   
                        </div>
                    </div>
                </div>
            </div>
        </div> 
    </xsl:template>  
    
    <!-- kpoint list -->
    <xsl:template name="kpoints">                   
        <xsl:variable name="subdivisionN" select="tokenize($kpoints/array[@dictRef='v:subdivisionN'],'\s+')"/>
        <xsl:variable name="shiftS" select="tokenize($kpoints/array[@dictRef='v:shiftS'],'\s+')"/>
        <div class="row bottom-buffer">
            <div class="col-md-4 col-sm-12">                                        
                <h4>Kpoint list</h4>
                <p>Scheme - <xsl:value-of select="$kpoints/scalar[@dictRef='v:meshScheme']"/></p>
                <table  class="display" id="kpointList">
                    <tbody>
                        <tr>
                            <xsl:for-each select="1 to count($subdivisionN)"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$subdivisionN[$outerIndex]"/></td></xsl:for-each>                                                
                        </tr>
                        <tr>
                            <xsl:for-each select="1 to count($shiftS)"><xsl:variable name="outerIndex" select="."/><td><xsl:value-of select="$shiftS[$outerIndex]"/></td></xsl:for-each>
                        </tr>
                    </tbody>                                        
                </table>
            </div>                                
        </div>                    
    </xsl:template>    
    
    <!-- Final energy -->
    <xsl:template name="energy">        
        <xsl:if test="count($energies) = 1">    <!-- Single energy field, display all energies -->
            <xsl:choose>
                <xsl:when test="$calcType != $vasp:MolecularDynamics">
                    <xsl:variable name="energy" select="$energies[1]"/>
                    <div class="row bottom-buffer">
                        <div class="col-md-4 col-sm-12">
                            <table class="display" id="energies">                        
                                <thead>
                                    <tr></tr>
                                    <tr></tr>
                                    <tr></tr>                            
                                </thead>                       
                                <tbody>                            
                                    <tr>
                                        <td>Gibbs energy:</td>
                                        <td class="right">
                                            <xsl:value-of select="$energy/*[@dictRef='cc:freeEnergy']"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="helper:printUnitSymbol($energy/*[@dictRef='cc:freeEnergy']/@units)"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>E0:</td>
                                        <td class="right">
                                            <xsl:value-of select="$energy/*[@dictRef='cc:e0']"/>
                                        </td>
                                        <td>
                                            <xsl:value-of select="helper:printUnitSymbol($energy/*[@dictRef='cc:e0']/@units)"/>
                                        </td>                                
                                    </tr>                        
                                    <xsl:if test="exists($energy/*[@dictRef='cc:deltaEnergy'])">
                                        <tr>                            
                                            <td>dE:</td>
                                            <td class="right">
                                                <xsl:value-of select="number($energy/*[@dictRef='cc:deltaEnergy'])"/><xsl:text> </xsl:text>
                                            </td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($energy/*[@dictRef='cc:deltaEnergy']/@units)"/></td>
                                        </tr>
                                    </xsl:if>                        
                                    <xsl:if test="exists($energy/*[@dictRef='v:efermi'])">
                                        <tr>
                                            <td>E-fermi:</td>
                                            <td class="right">
                                                <xsl:value-of select="number($energy/*[@dictRef='v:efermi'])"/><xsl:text> </xsl:text>                                        
                                            </td>
                                            <td><xsl:value-of select="helper:printUnitSymbol($energy/*[@dictRef='v:efermi']/@units)"/></td>
                                        </tr>    
                                    </xsl:if>
                                </tbody>
                            </table>                
                        </div>
                    </div>
                </xsl:when>
                <xsl:otherwise>                                                
                    <xsl:variable name="free" select="tokenize($energies/cml:array[@dictRef='cc:freeEnergy'], '\s+')" />
                    <xsl:variable name="noentropy" select="tokenize($energies/cml:array[@dictRef='v:noEntropyEnergy'], '\s+')" />
                    <xsl:variable name="e0" select="tokenize($energies/cml:array[@dictRef='cc:e0'], '\s+')" />
                    <xsl:variable name="fermi" select="tokenize($energies/cml:array[@dictRef='v:efermi'], '\s+')" />
                    <xsl:variable name="delta" select="tokenize(concat('0.0 ', $energies/cml:array[@dictRef='cc:deltaEnergy']), '\s+')" />
                    <xsl:variable name="f" select="tokenize($forces/cml:matrix[@dictRef='cc:force'], '\s+')" />
                    <xsl:variable name="atomCount" select="xs:integer(number($forces/cml:matrix[@dictRef='cc:force']/@cols) div 3)" />
                    <xsl:variable name="maxforces">
                        <xsl:for-each select="1 to count($free)">
                            <xsl:variable name="outerIndex" select="."/>
                            <xsl:element name="step">
                                    <xsl:for-each select="1 to $atomCount">
                                        <xsl:element name="force">
                                            <xsl:variable name="innerIndex" select="."/>
                                            <xsl:variable name="coords" as="element()*">
                                                <Item><xsl:value-of select="$f[((($outerIndex - 1)* $atomCount * 3) + ($innerIndex -1)* 3) +1]"/></Item>
                                                <Item><xsl:value-of select="$f[((($outerIndex - 1)* $atomCount * 3) + ($innerIndex -1)* 3) +2]"/></Item>
                                                <Item><xsl:value-of select="$f[((($outerIndex - 1)* $atomCount * 3) + ($innerIndex -1)* 3) +3]"/></Item>
                                            </xsl:variable>
                                            <xsl:value-of select="helper:calcAxisLength($coords)" />
                                        </xsl:element>
                                    </xsl:for-each>
                            </xsl:element>
                        </xsl:for-each>
                    </xsl:variable>

                    <div class="panel panel-default">
                        <div class="panel-heading" data-toggle="collapse" data-target="div#steps-{generate-id($energies)}" style="cursor: pointer; cursor: hand;">
                            <h4 class="panel-title">                        
                                Energies                        
                            </h4>
                        </div>                                        
                        <div id="steps-{generate-id($energies)}" class="panel-collapse collapse">
                          <div class="panel-body">                    
                              <div class="row bottom-buffer">
                                  <div class="col-md-12">
                                      <div id="downloadTablestepsT-{generate-id($energies)}" class="text-right"><a class="text-right" href="javascript:showDownloadOptions('stepsT-{generate-id($energies)}');"><span class="text-right fa fa-download"></span></a></div>
                                      <table class="display nowrap" id="stepsT-{generate-id($energies)}">
                                          <thead>
                                              <tr>                                         
                                                  <th>Step</th>
                                                  <th>Free</th>
                                                  <th>ΔE</th>
                                                  <th>Max. force</th>                                                  
                                                  <th>No entropy</th>
                                                  <th>e0</th>
                                                  <th>Fermi</th>                                                                                                    
                                              </tr>                                        
                                          </thead>
                                          <tbody></tbody>                                    
                                      </table>                                                            
                                      <div id="stepsPlotlyContainer" style="min-height:650px" class="mt-4 d-flex"/>
                                      
                                      <script type="text/javascript">
                                          //Build table
                                          var stepTable = null;
                                          var stepnumber = [<xsl:for-each select="1 to count($free)"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          var free = [<xsl:for-each select="$free"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          var delta = [<xsl:for-each select="$delta"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          var forces = [<xsl:for-each select="1 to count($free)"><xsl:variable name="outerIndex" select="."/><xsl:value-of select="max($maxforces/*:step[$outerIndex]/*:force)"/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          var noentropy = [<xsl:for-each select="$noentropy"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          var e0 = [<xsl:for-each select="$e0"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          var fermi = [<xsl:for-each select="$fermi"><xsl:value-of select="."/><xsl:if test="position() != last()">,</xsl:if></xsl:for-each>];
                                          
                                          
                                          //Build graphs                                              
                                          var data = new Array();                                            
                                          
                                          var serie = {
                                              x: stepnumber,
                                              y: free,
                                              yaxis: 'y3',
                                              mode: 'lines',
                                              name: 'Free',
                                              hovertemplate:'%{y:.8f}'
                                          };                                      
                                          data.push(serie);
                                          
                                          serie = {
                                              x: stepnumber,
                                              y: noentropy,
                                              yaxis: 'y3',
                                              mode: 'lines',
                                              name: 'No entropy',
                                              hovertemplate:'%{y:.8f}'                                     
                                          };                                      
                                          data.push(serie);
                                          
                                          serie = {
                                              x: stepnumber,
                                              y: e0,
                                              yaxis: 'y3',
                                              mode: 'lines',
                                              name: 'e0',
                                              hovertemplate:'%{y:.8f}'
                                          };                                      
                                          data.push(serie);                                            
                                          
                                          serie = {
                                              x: stepnumber,
                                              y: fermi,
                                              yaxis: 'y2',
                                              mode: 'lines',
                                              name: 'Fermi',
                                              hovertemplate:'%{y:.8f}'
                                          };                                      
                                          data.push(serie);
                                          
                                          serie = {
                                              x: stepnumber,
                                              y: delta,
                                              yaxis: 'y2',
                                              mode: 'lines',
                                              name: 'ΔE',
                                              hovertemplate:'%{y:.8f}'
                                          };                                      
                                          data.push(serie);       
                                          
                                          serie = {
                                              x: stepnumber,
                                              y: forces,                                              
                                              mode: 'lines',
                                              name: 'Max. force',
                                              hovertemplate:'%{y:.8f}'
                                          };                                      
                                          data.push(serie);       
                                          
                                          
                                          $(document).ready(function(){
                                              var layout = {
                                                  title: 'Energies',
                                                  height: 650,
                                                  legend: {
                                                    tracegroupgap: 20
                                                  },                                                    
                                                  xaxis: {
                                                    title: 'Step',
                                                    showgrid: true,
                                                    tickmode: 'auto',
                                                    tickvals: stepnumber                                                
                                                  },
                                                  yaxis: {  
                                                    domain: [0, 0.30],
                                                    title: 'Forces [<xsl:value-of select="helper:printUnitSymbol($forces/cml:matrix[@dictRef='cc:force']/@units)"/>]'
                                                  },
                                                  yaxis2: {
                                                    domain:  [0.35, 0.65],
                                                    title: 'Energies [<xsl:value-of select="helper:printUnitSymbol($energies/cml:array[@dictRef='cc:freeEnergy']/@units)"/>]'
                                                  },
                                                  yaxis3: {
                                                    domain: [0.70, 1],
                                                    title: 'Energies [<xsl:value-of select="helper:printUnitSymbol($energies/cml:array[@dictRef='cc:freeEnergy']/@units)"/>]'
                                                  },
                                                  
                                              };
                                          
                                              Plotly.react('stepsPlotlyContainer', data, layout,  {responsive: true, showSendToCloud: true});
                                          
                                              $('#steps-<xsl:value-of select="generate-id($energies)"/>').on('shown.bs.collapse', function () {                                            
                                                  Plotly.relayout($('#stepsPlotlyContainer')[0],{autosize: true});
                                                  if(stepTable == null)
                                                      buildStepsTable();
                                              });
                                          });
                                          
                                          function buildStepsTable() {
                                              stepTable = $('table#stepsT-<xsl:value-of select="generate-id($energies)"/>').dataTable({
                                                  "aaData": buildSteps(),                                                                   
                                                  "bFilter": false,
                                                  "bPaginate": true,                                           
                                                  "bSort": false,
                                                  "aoColumnDefs" : [                                    
                                                      { "sClass": "text-right", "aTargets": [1,2,3,4,5,6] }
                                                  ]                                            
                                              });
                                          }
                                          
                                          function buildSteps() {
                                              var steps = new Array();
                                              for(i = 0; i &lt; stepnumber.length; i++){
                                                  var step = new Array();
                                                  step.push(stepnumber[i]);
                                                  step.push(free[i]);
                                                  step.push(delta[i]);
                                                  step.push(forces[i]);                                                  
                                                  step.push(noentropy[i]);
                                                  step.push(e0[i]);
                                                  step.push(fermi[i]);
                                                  steps.push(step);
                                              }
                                              return steps;
                                          }
                                      </script>
                                  </div>
                              </div>
                          </div>
                        </div>  
                    </div>
                </xsl:otherwise>
            </xsl:choose>         
        </xsl:if>
        <xsl:if test="count($energies) > 1 ">
            <!-- Multiple jobs, we're dealing with NEB calculations -->
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#deltaEnergiesCollapse" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        Energies                        
                    </h4>
                </div>
                <div id="deltaEnergiesCollapse" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="row bottom-buffer">                    
                            <xsl:variable name="baseEnergy" select="number(tokenize($energies[1]/*[@dictRef='cc:e0'],'\s+')[last()])"/>                
                            <div class="col-lg-7 col-md-5 col-sm-12">
                                <div id="deltaEnergiesChart"></div>                            
                                <script>
                                    $('div#deltaEnergiesCollapse').on('shown.bs.collapse', function () {                                    
                                        generateEnergiesChart();
                                    })
                                    
                                    
                                    function generateEnergiesChart(){
                                        var data = fillEnergiesChart()
                                        var layout = {
                                            yaxis: {
                                                title: 'ΔE (eV)',
                                                showgrid: false
                                            },
                                            xaxis: {
                                                title: '# Image',
                                                type: 'category'                                            
                                            },                                          
                                        };                                        
                                    
                                        Plotly.react('deltaEnergiesChart', data, layout, {responsive: true, showSendToCloud: true});                                           
                                    }
                                    
                                    function fillEnergiesChart() {
                                        var data = new Array();
                                        var serie = {
                                            mode: 'lines+markers',
                                            line: { shape: 'spline',
                                                    smoothing: 0.4,
                                                    color: '#7cb5ec',
                                                    dash: 'solid',
                                                    width: 4 },
                                            marker: { size: 12 },
                                            type: 'scatter',
                                            x: [<xsl:for-each select="$energies">'<xsl:value-of select="format-number(position()-1, '00')"/>',</xsl:for-each>],
                                            y: [<xsl:for-each select="$energies"><xsl:value-of select="format-number(round-half-to-even(number(tokenize(./*[@dictRef='cc:e0'], '\s+')[last()]) - $baseEnergy,10),'0.0000')"/>,</xsl:for-each>]
                                        };                                        
                                        data.push(serie);
                                        return data;
                                    }
                                </script>
                            </div>
                            <div class="col-lg-4 col-md-6 col-sm-12">
                                <br/>
                                <table id="energies">
                                    <thead>
                                        <tr>
                                            <th class="text-center"># Image</th>
                                            <th class="text.center">E0 (<xsl:value-of
                                                select="helper:printUnitSymbol($energies[1]/*[@dictRef='cc:e0']/@units)"
                                            />)</th>
                                            <th class="text-center">ΔE</th>
                                        </tr>
                                    </thead>
                                    <xsl:for-each select="$energies">
                                        <xsl:variable name="index" select="position() - 1"/>
                                        <tr>
                                            <td>
                                                <xsl:value-of select="format-number($index,'00')"/>
                                            </td>
                                            <td>
                                                <xsl:value-of select="number(tokenize(./*[@dictRef='cc:e0'], '\s+')[last()])"/>
                                            </td>
                                            <td class="right">
                                                <xsl:value-of select="round-half-to-even(number(tokenize(./*[@dictRef='cc:e0'],'\s+')[last()]) - $baseEnergy,10)"
                                                />
                                            </td>
                                        </tr>
                                    </xsl:for-each>
                                </table>           
                                <script>
                                    $('table#energies').dataTable({
                                        "paging":   false,
                                        "ordering": false,
                                        "info":     false,
                                        "searching":   false
                                    });                        
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>
        
    </xsl:template>
   
    <!-- Eigenvalues -->
    <xsl:template name="eigenvalues">
        <xsl:variable name="eigenvalues" select="//module[@dictRef='cc:calculation']/module[@dictRef='cc:userDefinedModule']/module[@cmlx:templateRef='eigenvalues']"/>        
        <xsl:if test="count($eigenvalues) = 1">
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#eigenvalues-{generate-id($eigenvalues)}" style="cursor: pointer; cursor: hand;" >
                    <h4 class="panel-title">                        
                        Eigenvalues                           
                    </h4>
                </div>
                <div id="eigenvalues-{generate-id($eigenvalues)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div class="row">                                
                                    <xsl:for-each select="$eigenvalues/list">
                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                            <xsl:variable name="nodeId" select="generate-id(.)"/>
                                            <xsl:variable name="spin" select="./scalar[@dictRef='cc:spin']"></xsl:variable>
                                            <h4>Spin <xsl:value-of select="if(compare($spin,'1') = 0) then
                                                'alpha'
                                                else
                                                'beta'"/></h4>
                                            
                                            <div class="button-group mb-2">
                                                Kpoint
                                                <xsl:for-each select="tokenize(./array[@dictRef='cc:serial'],'\s+')">
                                                    <button class="btn btn-default" type="button" onclick="pushButton(this);buildEigenOccupTable('eigenvaluesspin-{$nodeId}','{$spin}','{.}')"><xsl:value-of select="."/></button>
                                                </xsl:for-each>
                                            </div>                                       
                                            <script type="text/javascript">
                                                <xsl:variable name="numberOfRows" select=" number(./array[@dictRef='cc:eigen']/@size) idiv number(./array[@dictRef='cc:serial']/@size)"/>
                                                <xsl:variable name="eigen" select="tokenize(./array[@dictRef='cc:eigen'],'\s+')"/>
                                                <xsl:variable name="occup" select="tokenize(./array[@dictRef='cc:occup'],'\s+')"/>
                                                <xsl:for-each select="tokenize(./array[@dictRef='cc:serial'],'\s+')">
                                                    <xsl:variable name="outerIndex" select="position()"/>                                       
                                                    var eigenValuesSpin<xsl:value-of select="$spin"/>Serial<xsl:value-of select="."/> = [<xsl:for-each select="1 to $numberOfRows"><xsl:variable name="innerIndex" select="."/>['<xsl:value-of select="$eigen[ ($outerIndex - 1) * $numberOfRows + $innerIndex ]"/>','<xsl:value-of select="$occup[ ($outerIndex - 1) * $numberOfRows + $innerIndex ]"/>']<xsl:if test="$innerIndex &lt; $numberOfRows">,</xsl:if></xsl:for-each>];
                                                </xsl:for-each>
                                            </script>
                                            <table class="display" id="eigenvaluesspin-{$nodeId}"></table>
                                        </div>                                    
                                    </xsl:for-each>
                                </div>
                                <script type="text/javascript">
                                    function pushButton(button){                                        
                                        $(button.parentElement).children('.btn-default').removeClass('active');
                                        $(button).addClass('active');
                                    }
                                    function buildEigenOccupTable(tableId,spin,kpoint){                                                       
                                        $('table#' + tableId).dataTable({
                                            "aaData":  window['eigenValuesSpin' + spin + 'Serial' + kpoint],
                                            "aoColumns": [ { "sTitle": "Eigenvalues"},{"sTitle": "Occupation"}], 
                                            "bFilter": false,
                                            "bPaginate": true,
                                            "bSort": false,
                                            "bInfo": true,
                                            "bDestroy": true
                                        });
                                                                              
                                        var jsTable = "javascript:showDownloadOptions('" + tableId + "');"
                                        $('div#downloadTable' + tableId).remove();
                                        $('<div id="downloadTable'+ tableId + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableId +'_wrapper'); 
                                     
                                    }
                                </script>
                            </div>
                        </div>
                    </div>
                </div>
            </div>    
        </xsl:if>              
    </xsl:template>
   
    <!-- Density of states -->
    <xsl:template name="dos">
        <xsl:if test="exists($dos)">           
            <xsl:variable name="orbitalNumber" select="(//matrix[@dictRef='v:partialspin1']/@cols)[1]"/>
            <xsl:variable name="numberOfSpins" select="count($dos/module[@cmlx:templateRef='totals']/array[contains(@dictRef,'v:totalspin')])"/>
            <xsl:variable name="isClosedShell" select="$numberOfSpins = 1"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#dos-{generate-id($dos)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        DOS                         
                    </h4>
                </div>
                <div id="dos-{generate-id($dos)}" class="panel-collapse collapse">
                    <div class="panel-body">    
                        <div class="row bottom-buffer">
                            <div class="col-md-12">
                                <div class="row"> 
                                    <div class="col-md-12">                                   
                                        <form class="form-horizontal" role="form">
                                        <div class="form-group">
                                            <label for="inputDosAtomSelection" class="col-sm-2 control-label">Atom selection</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputDosAtomSelection" placeholder="Atom selection" title="Method 1: Blank space separated atom indexes or range values. &#013;&#09;Ex: 1 30 35-46&#013;Method 2: Atom type.&#013;&#09;Ex: Ce O.&#013;&#013;Formats can't be mixed."></input>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="inputDosDescription" class="col-sm-2 control-label">Description</label>
                                            <div class="col-sm-10">
                                                <input type="text" class="form-control" id="inputDosDescription" placeholder="Description" title="Line description. Optional"></input>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label for="selectDosLine" class="col-sm-2 control-label">Line</label>
                                            <div class="col-sm-10">
                                                <select id="selectDosLine" class="form-control">                                 
                                                </select>                                                              
                                            </div>
                                        </div>                                         
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Spin</label>
                                            <div class="col-sm-10">                           
                                                <label id="radioDosSpinUp" class="radio-inline">
                                                    <input type="radio" name="radioDosSpin" id="radioDosSpinUp" value="up" checked="checked">Up</input>
                                                </label>                            
                                                <label id="radioDosSpinDownLbl" class="radio-inline">
                                                    <input type="radio" name="radioDosSpin" id="radioDosSpinDown" value="down">Down</input>
                                                </label>                              
                                                <label id="radioDosSpinBothLbl" class="radio-inline">
                                                    <input type="radio" name="radioDosSpin" id="radioDosSpinBoth" value="both" >Both</input>
                                                </label>                               
                                            </div>
                                        </div>                       
                                        <div class="form-group">
                                            <label class="col-sm-12 col-md-2 control-label">Orbital selection</label>
                                            <div class="col-sm-12 col-md-10">
                                                <div class="row">
                                                    <div class="col-sm-12 col-md-8">
                                                        <div class="table-responsive">
                                                            <table class="table table-condensed">                                       
                                                                <tbody>
                                                                    <tr>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('[id^=checkOrb_]:enabled').prop('checked', true);">Select all</button></td>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('[id^=checkOrb_]:enabled').prop('checked', false);">Deselect all</button></td>
                                                                        <td colspan="7"><input id="checkOrb_s" name="orbitalcheck1" type="checkbox">s</input></td>                                               
                                                                    </tr>
                                                                    <tr>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('#checkOrb_px:enabled').prop('checked', true);$('#checkOrb_py:enabled').prop('checked', true);$('#checkOrb_pz:enabled').prop('checked', true);">Select p</button></td>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('#checkOrb_px:enabled').prop('checked', false);$('#checkOrb_py:enabled').prop('checked', false);$('#checkOrb_pz:enabled').prop('checked', false);">Deselect p</button>     </td>                                                                        
                                                                        <td><input id="checkOrb_py" name="orbitalcheck2" type="checkbox">py</input></td>
                                                                        <td><input id="checkOrb_pz" name="orbitalcheck3" type="checkbox">pz</input></td>
                                                                        <td colspan="5"><input id="checkOrb_px" name="orbitalcheck4" type="checkbox">px</input></td>                                               
                                                                    </tr>
                                                                    <tr>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('#checkOrb_dxy:enabled').prop('checked', true);$('#checkOrb_dyz:enabled').prop('checked', true);$('#checkOrb_dxz:enabled').prop('checked', true);$('#checkOrb_dz2:enabled').prop('checked', true);$('#checkOrb_dx2:enabled').prop('checked', true);">Select d</button></td>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('#checkOrb_dxy:enabled').prop('checked', false);$('#checkOrb_dyz:enabled').prop('checked', false);$('#checkOrb_dxz:enabled').prop('checked', false);$('#checkOrb_dz2:enabled').prop('checked', false);$('#checkOrb_dx2:enabled').prop('checked', false);">Deselect d</button></td>
                                                                        <td><input id="checkOrb_dxy" name="orbitalcheck5"  type="checkbox">dxy</input></td>
                                                                        <td><input id="checkOrb_dyz" name="orbitalcheck6" type="checkbox">dyz</input></td>
                                                                        <td><input id="checkOrb_dz2" name="orbitalcheck7" type="checkbox">dz2</input></td>
                                                                        <td><input id="checkOrb_dxz" name="orbitalcheck8" type="checkbox">dxz</input></td>                                                                        
                                                                        <td colspan="3"><input id="checkOrb_dx2"  name="orbitalcheck9"  type="checkbox">dx2</input></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('#checkOrb_f1:enabled').prop('checked', true);$('#checkOrb_f2:enabled').prop('checked', true);$('#checkOrb_f3:enabled').prop('checked', true);$('#checkOrb_f4:enabled').prop('checked', true);$('#checkOrb_f5:enabled').prop('checked', true);$('#checkOrb_f6:enabled').prop('checked', true);$('#checkOrb_f7:enabled').prop('checked', true);">Select f</button></td>
                                                                        <td><button class="btn btn-default btn-sm" type="button" onclick="$('#checkOrb_f1:enabled').prop('checked', false);$('#checkOrb_f2:enabled').prop('checked', false);$('#checkOrb_f3:enabled').prop('checked', false);$('#checkOrb_f4:enabled').prop('checked', false);$('#checkOrb_f5:enabled').prop('checked', false);$('#checkOrb_f6:enabled').prop('checked', false);$('#checkOrb_f7:enabled').prop('checked', false);">Deselect f</button></td>
                                                                        <td><input id="checkOrb_f1" name="orbitalcheck10" type="checkbox">f1</input></td>
                                                                        <td><input id="checkOrb_f2" name="orbitalcheck11" type="checkbox">f2</input></td>
                                                                        <td><input id="checkOrb_f3" name="orbitalcheck12" type="checkbox">f3</input></td>
                                                                        <td><input id="checkOrb_f4" name="orbitalcheck13" type="checkbox">f4</input></td>
                                                                        <td><input id="checkOrb_f5" name="orbitalcheck14" type="checkbox">f5</input></td>
                                                                        <td><input id="checkOrb_f6" name="orbitalcheck15" type="checkbox">f6</input></td>
                                                                        <td><input id="checkOrb_f7" name="orbitalcheck16" type="checkbox">f7</input></td>                                               
                                                                    </tr>
                                                                </tbody>   
                                                            </table>
                                                        </div>
                                                    </div>                                                   
                                                </div>
                                            </div>                       
                                        </div>
                                        <script type="text/javascript">
                                            var orbitalNumber = <xsl:value-of select="$orbitalNumber"/>;
                                            $(document).ready(function() {                                            
                                                $('[id^=checkOrb_]').each(function () {
                                                    if(parseInt($(this).prop("name").replace('orbitalcheck','')) &gt; orbitalNumber ){
                                                        $(this).prop( "disabled", true) 
                                                    }
                                                });
                                                <!-- On closed shell calculations we will disable 'Down' and 'Both' radiobuttons and set 'Up' selected by default -->
                                                <xsl:if test="$isClosedShell">                                              
                                                    $("#radioDosSpinDownLbl").hide();
                                                    $("#radioDosSpinBothLbl").hide();
                                                </xsl:if>                                                                                                
                                            });
                                        </script>                                                                                
                                        <div class="form-group"> 
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <button class="btn btn-default btn-primary" type="button" onclick="javascript:addDosToList()">Add new line</button>
                                                <button class="btn btn-default btn-primary" type="button" onclick="javascript:removeDosFromList()">Remove line</button>
                                                
                                                
                                                <div id="dosModal" class="modal fade">
                                                    <div class="modal-dialog">
                                                        <div class="modal-content">
                                                            <div class="modal-header">                                                                
                                                                <h4 class="modal-title">Wrong parameters</h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <p id="dosModalErrorMessage"></p>
                                                            </div>
                                                            <div class="modal-footer">
                                                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>                                                                
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>                            
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label"></label>
                                            <div class="col-sm-10">
                                                <button class="btn btn-default btn-lg" type="button" onclick="javascript:generateDosGraphic()">Plot graphic</button>
                                            </div>
                                        </div>
                                    </form>                                                                    
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div id="dosChart" style="min-height:450px"></div>
                                    </div>                                                           
                                </div>
                            </div>
                        </div>                                                      
                                <script type="text/javascript">  
                                    var dosAtomIndexes = { <xsl:for-each-group select="(//molecule)[position()=last()]/atomArray/atom" group-by="@elementType">
                                        "<xsl:value-of select="@elementType"/>":[<xsl:for-each select="current-group()"><xsl:value-of select="count(preceding-sibling::*[name() = name(current())]) + 1 "/><xsl:if test="position()!=last()">,</xsl:if></xsl:for-each>]<xsl:if test="position()!=last()">,</xsl:if>
                                    </xsl:for-each-group>};                                    
                                    <![CDATA[
                                    var dosLines = [];     
                                    var orbitalLabels = {'checkOrb_s':0,                                                      
                                                        'checkOrb_px':1,
                                                        'checkOrb_py':2,
                                                        'checkOrb_pz':3,
                                                        'checkOrb_dxy':4,
                                                        'checkOrb_dyz':5,
                                                        'checkOrb_dxz':6,
                                                        'checkOrb_dz2':7,
                                                        'checkOrb_dx2':8,
                                                        'checkOrb_f1':9,
                                                        'checkOrb_f2':10,
                                                        'checkOrb_f3':11,
                                                        'checkOrb_f4':12,
                                                        'checkOrb_f5':13,
                                                        'checkOrb_f6':14,
                                                        'checkOrb_f7':15};
                                                       
                                    function addDosToList(){
                                        var atoms = null;
                                        var atomSelection = $("#inputDosAtomSelection").val(); 
                                        if(atomSelection.replace(/[ ]+/g,'').match("[A-Za-z]+")){
                                            atoms = getAtomNumberByAtomType(atomSelection);
                                        }else{
                                            atoms = getAtomNumberByRange(atomSelection)
                                        }                                        
                                        var desc = $("#inputDosDescription").val();
                                        var spin = $("input[name='radioDosSpin']:checked").val();
                                        var orbitals = $("[id^=checkOrb]:checked");
                                        
                                        if(atoms.length == 0){
                                            $("#dosModalErrorMessage").html("Must select at least one atom");
                                            $('#dosModal').modal();
                                            return;
                                        }else if(orbitals.length == 0){
                                            $("#dosModalErrorMessage").html("Must select at least one orbital");
                                            $('#dosModal').modal();
                                            return;
                                        }
                                        
                                        if(desc == ''){ 
                                            desc = $("#inputDosAtomSelection").val();
                                            desc += "("
                                            for(var i = 0; i <  orbitals.length; i++){
                                                var orbital = $(orbitals[i]).attr('id').replace("checkOrb_","");
                                                desc += orbital + " ";
                                            }
                                            desc += ")";
                                        }
                                        var line = [];
                                        line.push(atoms);
                                        line.push(desc);
                                        line.push(spin);
                                        line.push(orbitals);                                                                            
                                        dosLines.push(line);
                                        $("#selectDosLine").append($('<option>', {
                                            value: desc,
                                            text: desc
                                            }));                                                                            
                                    }
                                    
                                                    
                                    function getAtomNumberByAtomType(range){
                                        var array = [];                                        
                                        range = range.trim().replace(/[ ]+/g,' ').split(' ');                                        
                                        for(var i = 0; i < range.length; i++){                                            
                                            var atomSelector = range[i];                                            
                                            try{
                                                jvar = eval('dosAtomIndexes.'+atomSelector);
                                                array = array.concat(jvar);
                                            }catch(ex){
                                                continue;
                                            }                                                                                                                              
                                        }  
                                        return array;
                                    }
                                                    
                                    function getAtomNumberByRange(range) {
                                        var array = [];                                        
                                        range = range.trim().replace(/ *- */g,'-').replace(/[ ]+/g,' ').split(' ');                                         
                                        for(var i = 0; i < range.length; i++){
                                            atomSelector = range[i];
                                            if(atomSelector.indexOf('-') != -1){
                                                var start = Number(atomSelector.split("-")[0]);
                                                var end   = Number(atomSelector.split("-")[1]);
                                                for(var j = start; j <= end; j++)
                                                    array.push(j); 
                                            }else if(Number(atomSelector) != 0)
                                                array.push(Number(atomSelector));                                            
                                        }  
                                        return array;
                                    }
                                    
                                    function removeDosFromList() {
                                        var selectedIndex = $("#selectDosLine").prop("selectedIndex");
                                        dosLines.splice(selectedIndex,1);                                        
                                        $("#selectDosLine option:selected").remove();
                                    }                                    

                                    function generateDosGraphic(){                                                                                   
                                        var data = fillDosChart()
                                        var layout = {
                                            title:'Density of states',                                           
                                            hovermode: 'closest', 
                                            xaxis: {
                                                title: 'Ɛ-Ɛ<sub>f</sub><b>(eV)</b>',
                                                showgrid: true,
                                                zeroline: true,
                                                tickmode: 'auto'
                                            },
                                            yaxis: {
                                                title: '<b>PDOS(states/eV)</b>'
                                            },
                                            legend: {
                                                orientation: 'h',
                                                y: -0.5
                                            }
                                        };                                        
                                        Plotly.react('dosChart', data, layout,  {responsive: true, showSendToCloud: true});       
                                    }
                                                                                                             
                                    function fillDosChart() {
                                       var data = new Array();
                                       var numberOfPoints = totalDosEnergy[0][0].length;
                                    
                                       //Alpha totals
                                       var totalDosAlpha = {
                                            x: totalDosEnergy[0][0],
                                            y: totalDos[0][0],
                                            mode: 'lines',
                                            name: 'Total Alpha',
                                            marker: {         
                                                color: 'rgb(00, 00, 00)'
                                            }
                                       };                           
                                       data.push(totalDosAlpha);  
                                                                              
                                       //Beta totals
                                       if(totalDos[0].length > 1){
                                        var yVal = new Array();
                                        totalDos[0][1].forEach(function(element){
                                            yVal.push(-element);
                                        });
                                       
                                       
                                        var totalDosBeta =  {
                                            x: totalDosEnergy[0][1],
                                            y: yVal,
                                            mode: 'lines',
                                            name: 'Total Beta',
                                            marker: {         
                                                color: 'rgb(00, 00, 00)'
                                            }
                                        };
                                        data.push(totalDosBeta);
                                       }                                       
                                       
                                       //Iterate all user defined lines
                                       for (var h = 0; h < dosLines.length; h++) {                                       
                                            var line = dosLines[h];                                                         
                                            var atoms = line[0];
                                            var desc  = line[1];
                                            var spin  = line[2];
                                            var orbitals = line[3];                                            
                                            //Sum values from all atoms and orbitals
                                            var ySerie = [];
                                            for (var i = 0; i < atoms.length; i++) {                                           
                                                var atom = atoms[i];  
                                                var serie = null;
                                                if(spin=='up' || spin == 'both'){
                                                    try{
                                                        jvar = eval('partial_'+atom+'_alpha');
                                                        for(var j = 0; j < orbitals.length; j++){
                                                            var orbital = $(orbitals[j]).attr('id');                    
                                                            serie = orbitalLabels[orbital];                             
                                                            y = jvar[serie];                                            
                                                            for (var k = 0; k < numberOfPoints; k++) {
                                                                if(ySerie.length -1 < k)
                                                                    ySerie.push(y[k]);
                                                                else
                                                                    ySerie[k] += y[k];
                                                            }
                                                        }
                                                    }catch(ex){
                                                        var text = "There was an error reading values from variable partial_"+atom+"_alpha_"+serie+".\n\n";
                                                        text += "Error description: " + ex.message + "\n\n";
                                                        $("#dosModalErrorMessage").html(text);
                                                        $('#dosModal').modal();                                                                                                            
                                                        continue;
                                                    }                                                     
                                                }
                                                if(spin=='down' || spin == 'both'){
                                                    try{
                                                        jvar = eval('partial_'+atom+'_beta');
                                                        for(var j = 0; j < orbitals.length; j++){
                                                            var orbital = $(orbitals[j]).attr('id');                    
                                                            var serie = orbitalLabels[orbital];                          
                                                            y = jvar[serie];                                            
                                                            for (var k = 0; k < numberOfPoints; k++) {
                                                                 if(ySerie.length -1 < k)
                                                                    ySerie.push(-y[k]);
                                                                else
                                                                    ySerie[k] -= y[k];                                                                   
                                                            }
                                                        }                                                    
                                                    }catch(ex){
                                                        var text = "There was an error reading values from variable partial_"+atom+"_beta.\n\n";
                                                        text += "Error description: " + ex.message + "\n\n";
                                                        $("#dosModalErrorMessage").html(text);
                                                        $('#dosModal').modal();                                                         
                                                        continue;
                                                    }           
                                                
                                                }
                                            }
                                            
                                            var newSerie = {
                                                x: totalDosEnergy[0][0],
                                                y: ySerie, 
                                                mode: 'lines',
                                                name: desc,                                                
                                            };
                                            data.push(newSerie);
                                         };                                                                                
                                         return data;                                         
                                    }
                                    
                                    ]]>
                                    
                                    //Total energies
                                    var totalDosEnergy = [                                    
                                    [<xsl:for-each select="1 to $numberOfSpins">
                                        <xsl:variable name="outerIndex" select="."/>
                                        [<xsl:value-of select="replace($dos/module[@cmlx:templateRef='totals']/array[@dictRef='v:dosenergy'],'\s+',',')"></xsl:value-of>]<xsl:if test="$outerIndex &lt; $numberOfSpins">,</xsl:if>
                                    </xsl:for-each>]                                    
                                    ];
                                    var totalDos = [                                  
                                    [<xsl:for-each select="1 to $numberOfSpins">
                                        <xsl:variable name="outerIndex" select="."/>
                                        [<xsl:value-of select="replace($dos/module[@cmlx:templateRef='totals']/array[@dictRef=concat('v:totalspin',$outerIndex)],'\s+',',')"></xsl:value-of>]<xsl:if test="$outerIndex &lt;$numberOfSpins">,</xsl:if>
                                    </xsl:for-each>]                                    
                                    ];
                                  
                                    //Partial energies
                                    <xsl:variable name="dosFields" select="tokenize('s py pz px dxy dyz dz2 dxz dx2 f-3 f-2 f-1 f0 f1 f2 f3','\s+')"/>
                                    var dosFields = [<xsl:for-each select="$dosFields"><xsl:variable name="outerIndex" select="position()"/>"<xsl:value-of select="$dosFields[$outerIndex]"/>"<xsl:if test="$outerIndex&lt;count($dosFields)">,</xsl:if></xsl:for-each>];
                                    
                                    
                                    <xsl:for-each select="1 to count($dos/module[@cmlx:templateRef='partials'])">
                                        <xsl:variable name="outerIndex" select="."/>
                                        <xsl:variable name="atomCount" select="$outerIndex"/>
                                        
                                        <xsl:for-each select="$dos/module[@cmlx:templateRef='partials'][$outerIndex]/matrix">                                            
                                            <xsl:variable name="spin">
                                                <xsl:choose>
                                                    <xsl:when test="replace(./@dictRef, 'v:partialspin','') = '1'">alpha</xsl:when>
                                                    <xsl:otherwise>beta</xsl:otherwise>
                                                </xsl:choose>
                                            </xsl:variable>
                                            <xsl:variable name="matrix" select="."/>
                                            var partial_<xsl:value-of select="concat($atomCount,'_',$spin)"/>= <xsl:value-of select="helper:matrixToJSON2($matrix)"/>
                                        </xsl:for-each>                                        
                                    </xsl:for-each>  
                                </script>                          
                        </div>
                    </div>
            </div>
        </xsl:if>     
    </xsl:template>
 
    <!-- Magnetization -->
    <xsl:template name="magnetization">
        <xsl:if test="exists($magnetizations)">   
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#magnetization-{generate-id($magnetizations[1])}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        Magnetization                         
                    </h4>
                </div>
                <div id="magnetization-{generate-id($magnetizations[1])}" class="panel-collapse collapse">
                    <div class="panel-body">                   
                        <div class="row bottom-buffer">
                            <div class="col-md-6">
                                
                                <div class="button-group mb-2">
                                    Images : 
                                    <xsl:for-each select="1 to count(//module[@cmlx:templateRef='job'])">
                                        <button class="btn btn-default" type="button" onclick="javascript:buildMagnetizationTable('magnetization_image{.}')">
                                            <xsl:choose>
                                                <xsl:when test="position()-1 &lt; 10"><xsl:value-of select="concat(0,position()-1)"/></xsl:when>
                                                <xsl:otherwise><xsl:value-of select="position()-1"/></xsl:otherwise>
                                            </xsl:choose>                                        
                                        </button>
                                    </xsl:for-each>
                                </div>  
                                
                                                                
                                
                                <script type="text/javascript">
                                    <xsl:for-each select="//module[@cmlx:templateRef='job']">  <!-- There can be uploaded calculations without -->
                                        <xsl:variable name="index" select="position()"/>
                                        <xsl:variable name="magnetization" select=".//module[@dictRef='cc:finalization']/propertyList/property/module[@cmlx:templateRef='magnetization']"/>                                        
                                        <xsl:choose>                                            
                                            <xsl:when test="exists($magnetization)">  
                                                <xsl:variable name="hasF" select="exists($magnetization/array[@dictRef='v:coefff'])"/>
                                                
                                                
                                                <xsl:variable name="serial" select="tokenize($magnetization/array[@dictRef='cc:serial'],'\s+')"/>
                                                <xsl:variable name="coeffs" select="tokenize($magnetization/array[@dictRef='v:coeffs'],'\s+')"/>
                                                <xsl:variable name="coeffp" select="tokenize($magnetization/array[@dictRef='v:coeffp'],'\s+')"/>
                                                <xsl:variable name="coeffd" select="tokenize($magnetization/array[@dictRef='v:coeffd'],'\s+')"/>
                                                <xsl:variable name="coefff" select="tokenize($magnetization/array[@dictRef='v:coefff'],'\s+')"/>
                                                <xsl:variable name="coefftotal" select="tokenize($magnetization/array[@dictRef='v:coefftotal'],'\s+')"/>           
                                                <xsl:variable name="totals" select="$magnetization/list[@cmlx:templateRef='totals']"/>
                                                var magnetization_image<xsl:value-of select="$index" /> = [
                                                <xsl:for-each select="1 to count($serial)">
                                                    <xsl:variable name="outerIndex" select="."/>[ <xsl:value-of select="$serial[$outerIndex]"/>, <xsl:value-of select="$coeffs[$outerIndex]"/>,<xsl:value-of select="$coeffp[$outerIndex]"/>, <xsl:value-of select="$coeffd[$outerIndex]"/><xsl:if test="$hasF">, <xsl:value-of select="$coefff[$outerIndex]"/></xsl:if>, <xsl:value-of select="$coefftotal[$outerIndex]"/>],
                                                </xsl:for-each>
                                                ["Total", <xsl:value-of select="$totals/scalar[@dictRef='v:totalcoeffs']"/>, <xsl:value-of select="$totals/scalar[@dictRef='v:totalcoeffp']"/>, <xsl:value-of select="$totals/scalar[@dictRef='v:totalcoeffd']"/><xsl:if test="$hasF">,<xsl:value-of select="$totals/scalar[@dictRef='v:totalcoefff']"/></xsl:if>, <xsl:value-of select="$totals/scalar[@dictRef='v:coefftotalsum']"/>]
                                                ];
                                                var magnetization_image<xsl:value-of select="$index" />_hasF = <xsl:value-of select="$hasF"/>;
                                            </xsl:when>
                                            <xsl:otherwise>
                                                var magnetization_image<xsl:value-of select="$index" /> = [[ '','','', '','','']];
                                                var magnetization_image<xsl:value-of select="$index" />_hasF = false; 
                                            </xsl:otherwise>
                                        </xsl:choose>
                                        
                                        
                                        
                                        
                                    </xsl:for-each>                       
                                </script>
                                <table class="display" id="magnetizationT-{generate-id($magnetizations[1])}"></table>
                                <script type="text/javascript">                        
                                    function buildMagnetizationTable(dataVariableName){
                                        var hasF = eval(dataVariableName + "_hasF");
                                        if(hasF){
                                            $('table#magnetizationT-<xsl:value-of select="generate-id($magnetizations[1])"/>').dataTable( {
                                            "aaData":  window[dataVariableName] ,
                                            "aoColumns": [
                                            { "sTitle": "serial"},
                                            { "sTitle": "coeff s" , "sClass" : "right"},
                                            { "sTitle": "coeff p" , "sClass" : "right"},
                                            { "sTitle": "coeff d" , "sClass" : "right" },
                                            { "sTitle": "coeff f" , "sClass" : "right"},
                                            { "sTitle": "total" , "sClass" : "right"}
                                            ],
                                            "bFilter": false,
                                            "bPaginate": true,
                                            "bSort": false,
                                            "bInfo": true, 
                                            "bDestroy": true
                                            } );                                          
                                        }else{
                                          $('table#magnetizationT-<xsl:value-of select="generate-id($magnetizations[1])"/>').dataTable( {
                                              "aaData":  window[dataVariableName] ,
                                              "aoColumns": [
                                              { "sTitle": "serial"},
                                              { "sTitle": "coeff s" , "sClass" : "right"},
                                              { "sTitle": "coeff p" , "sClass" : "right"},
                                              { "sTitle": "coeff d" , "sClass" : "right" },                                              
                                              { "sTitle": "total" , "sClass" : "right"}
                                              ],
                                              "bFilter": false,
                                              "bPaginate": true,
                                              "bSort": false,
                                              "bInfo": true, 
                                              "bDestroy": true
                                              } );                                                      
                                          }
                                                                    
                                         
                                        var tableId = "magnetizationT-<xsl:value-of select="generate-id($magnetizations[1])"/>";
                                        var jsTable = "javascript:showDownloadOptions('" + tableId + "');"  
                                        $('div#downloadTable' + tableId).remove();
                                        $('<div id="downloadTable'+ tableId + '" class="text-right"><a class="text-right" href="' + jsTable +'"><span class="text-right fa fa-download"/></a></div>').insertBefore('div#' + tableId +'_wrapper');                                         
                                                                         
                                        };                         
                                </script>                
                                
                            </div>                                                        
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>       
    </xsl:template>    
 
    <!-- Vibrational frequencies -->
    <xsl:template name="vibrations">        
        <xsl:if test="exists($vibrations)">
            <xsl:variable name="frequency" select="tokenize($vibrations/array[@dictRef='cc:frequency'],'\s+')"/>
            <xsl:variable name="type" select="tokenize($vibrations/array[@dictRef='v:freqtype'],'\s+')"/>
            <xsl:variable name="cell">
                <xsl:text>{</xsl:text>
                <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@id='sc1']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@id='sc2']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@id='sc3']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@id='sc4']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@id='sc5']"/>
                <xsl:text> </xsl:text>
                <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@id='sc6']"/>
                <xsl:text>}</xsl:text>
            </xsl:variable>            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#vibrations-{generate-id($vibrations)}" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">                        
                        Vibrational frequencies                        
                    </h4>
                </div>
                <div id="vibrations-{generate-id($vibrations)}" class="panel-collapse collapse in">
                    <div class="panel-body">                   
                        <div class="row bottom-buffer">
                            <div class="col-md-12">                                
                                <input id="loadVibrationsBtn" type="button" onclick="loadJSmol()" value="Load vibrations"/><br/>
                                
                                Choose frequency: <select id="vibrations" style="visible:false">
                                    <option value="1">-</option>
                                    <xsl:for-each select="1 to count($frequency)">
                                        <xsl:variable name="outerIndex" select="."/>
                                        <option value="{$outerIndex + 1}">
                                            <xsl:if test="compare($type[$outerIndex],'f/i') = 0"><xsl:text>-</xsl:text></xsl:if><xsl:value-of select="$frequency[$outerIndex]"/><xsl:text> cm-1</xsl:text>
                                        </option>
                                    </xsl:for-each>
                                </select>

                                <script type="text/javascript"> 
                                  	delete Jmol._tracker;
                                    Jmol._local = true;
                                
                                
                                                                   
                                    var jmolApplet0 = null;
                                    
                                    jmol_isReady = function(applet) {		                                        	
                                        resizeVibrationApplet();
                                    }

                                    var JmolConfigInfo = {
                                        debug: false,
                                        color: "0xFFFFFF",
                                        addSelectionOptions: false,
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                        readyFunction: jmol_isReady,
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        allowJavaScript: true,
                                        console: "none"
                                    };
                                                                       
                                    function loadJSmol(){
                                        jmolApplet0 = Jmol.getApplet("jmolApplet0", JmolConfigInfo);
                                        $("#vibrationJSmolDiv").html(Jmol.getAppletHtml(jmolApplet0));                                        
                                        Jmol.script(jmolApplet0,"load <xsl:value-of select="$xyzurl"/> { 1 1 1 } unitcell <xsl:value-of select="$cell"></xsl:value-of>; set platformSpeed 2;");
                                        $("#loadVibrationsBtn").hide();
                                        loadCellParameters();                                        
                                    }
                                    
                                    function resizeVibrationApplet(){
                                        var width = $("div#vibrations-<xsl:value-of select="generate-id($vibrations)"/>").parent().width() - 50;
                                        var height = $( window ).height() - 50; 
                                        Jmol.resizeApplet(jmolApplet0, [width,height]);
                                        $("select#vibrations").change(function() {
                                            vibrationChanged($("select#vibrations").val());
                                        });
                                    }
                                    
                                    window.onresize = function () {
                                        resizeVibrationApplet();
                                    };
                                   
                                    function vibrationChanged(model){
                                        Jmol.script(jmolApplet0, "vibration on; model " + model);
                                    }
                                    
                                </script> 
                                <div id="vibrationJSmolDiv"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </xsl:if>       
    </xsl:template>
    
    <!-- Structure step geometries -->
    <xsl:template name="structure">
        <xsl:variable name="unitCell">
            <xsl:text>{&#32;</xsl:text>
            <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@title='a']"/><xsl:text>&#32;</xsl:text>
            <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@title='b']"/><xsl:text>&#32;</xsl:text>
            <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@title='c']"/><xsl:text>&#32;</xsl:text>
            <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@title='alpha']"/><xsl:text>&#32;</xsl:text>
            <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@title='beta']"/><xsl:text>&#32;</xsl:text>
            <xsl:value-of select="$finalMolecules[1]/crystal/scalar[@title='gamma']"/><xsl:text>&#32;</xsl:text>
            <xsl:text>}</xsl:text>
        </xsl:variable>
        <div class="panel panel-default">
            <div class="panel-heading" data-toggle="collapse" data-target="div#structure-{generate-id($finalMolecules[1])}" style="cursor: pointer; cursor: hand;">
                <h4 class="panel-title">
                    Structure
                </h4>
            </div>          
            <div id="structure-{generate-id($finalMolecules[1])}" class="panel-collapse collapse">
                <div class="panel-body">                   
                    <div class="row bottom-buffer">                                
                        <div class="col-sm-12 col-md-4">                                    
                            <table>
                                <tr>
                                    <td><label>Symmetry:</label></td>
                                    <td>
                                        {<select id="nebsymmetry1" style="width:40px;">
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>                                            
                                        <select id="nebsymmetry2" style="width:40px;">
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                        <select id="nebsymmetry3" style="width:40px;">
                                            <option value="1" selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                        }
                                    </td>
                                    <td>
                                        <button id="loadNebGeometriesBtn" type="button" class="btn btn-default" onclick="loadJSmolNeb()">Load</button><br/>
                                    </td>                                            
                                </tr>                                        
                            </table>                                    
                            <select id="structure" class="form-control">
                                <xsl:for-each select="1 to count(//module[@cmlx:templateRef='job'])">
                                    <xsl:variable name="outerIndex" select="."/>
                                    <option value="{$outerIndex}">
                                        Image <xsl:choose>
                                                <xsl:when test="position()-1 &lt; 10"><xsl:value-of select="concat(0,position()-1)"/></xsl:when>
                                                <xsl:otherwise><xsl:value-of select="position()-1"/></xsl:otherwise>
                                              </xsl:choose>
                                    </option>
                                </xsl:for-each>
                            </select>
                            
                        <script type="text/javascript">  
                        	delete Jmol._tracker;
                            Jmol._local = true;
                                                          
                            var jmolAppletNeb = null;
                           
                            var unitCell = "<xsl:value-of select="$unitCell"/>"; 

                            jmolNeb_isReady = function(applet) {                               
                               resizeNebApplet();
                            }	
                            
                            var JmolConfigInfoNeb = {                                       
                                debug: false,
                                color: "0xFFFFFF",
                                addSelectionOptions: false,		
                                use: "HTML5",
                                j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                readyFunction: jmolNeb_isReady,
                                disableJ2SLoadMonitor: true,
                                disableInitialConsole: true,
                                allowJavaScript: true,
                                console: "none"
                            };
                            
                            function loadJSmolNeb(){
                                $("select#structure").val("1");
                                var symmetry = $("select#nebsymmetry1").val() + ' ' + $("select#nebsymmetry2").val() + ' ' + $("select#nebsymmetry3").val();
                                jmolAppletNeb = Jmol.getApplet("jmolAppletNeb", JmolConfigInfoNeb);
                                $("#nebJSmolDiv").html(Jmol.getAppletHtml(jmolAppletNeb));                                                                        
                                Jmol.script(jmolAppletNeb,"load <xsl:value-of select="$xyzurl"/>&#38;mode=vasp_neb {" + symmetry +  "} unitcell " + unitCell +  "; set platformSpeed 2;");                                                                         
                            }
                            
                            function resizeNebApplet(){
                                var width = $("div#structure-<xsl:value-of select="generate-id($finalMolecules[1])"/>").parent().width() - 50;
                                var height = $( window ).height() - 50; 
                                Jmol.resizeApplet(jmolAppletNeb, [width,height]);
                                $("select#structure").change(function() {
                                    nebChanged($("select#structure").val());
                                });                                                                               
                            }
                                                                                                  
                            window.onresize = function () {
                                resizeNebApplet();
                            };
                            
                            function nebChanged(model){
                                Jmol.script(jmolAppletNeb, "model " + model);
                            }                                    
                        </script>
                        <div id="nebJSmolDiv"></div>
                        </div>                         
                    </div>
                </div>
            </div>
        </div>
            
    </xsl:template>
 
 
    <!--Molecular dynamics animation  -->
    <xsl:template name="molecularDynamics">
        <xsl:if test="compare($calcType, $vasp:MolecularDynamics) = 0">
            <xsl:variable name="cifurl" select="replace($xyzurl, 'xyz', 'cif')"/>
            
            <div class="panel panel-default">
                <div class="panel-heading" data-toggle="collapse" data-target="div#molecularDynamics" style="cursor: pointer; cursor: hand;">
                    <h4 class="panel-title">
                        Molecular dynamics
                    </h4>
                </div>          
                <div id="molecularDynamics" class="panel-collapse collapse">
                    <div class="panel-body">                   
                        <div class="row bottom-buffer">                                
                            <div class="col-sm-12">
                                <script type="text/javascript">
                                    delete Jmol._tracker;
                                    Jmol._local = true;
                                    
                                    var jmolAppletMD = null;
                                                                      
                                    jmolMD_isReady = function(applet) {                               
                                        resizeMDApplet();
                                    }	
                                    
                                    var JmolConfigInfoMD = {
                                        debug: false,
                                        color: "0xFFFFFF",
                                        addSelectionOptions: false,		
                                        use: "HTML5",
                                        j2sPath: "<xsl:value-of select="$webrootpath"/>/xslt/jsmol/j2s",
                                        readyFunction: jmolMD_isReady,
                                        disableJ2SLoadMonitor: true,
                                        disableInitialConsole: true,
                                        allowJavaScript: true,
                                        console: "none"
                                    };
                                    
                                    $('#molecularDynamics').on('shown.bs.collapse', function () {
                                        if(jmolAppletMD == null)
                                            loadJSmolMD();
                                        else {
                                            resizeMDApplet();
                                            Jmol.script(jmolAppletMD,"animation MODE LOOP; animation OFF;");
                                        }
                                    });
                                    
                                    function loadJSmolMD(){
                                        jmolAppletMD = Jmol.getApplet("jmolAppletMD", JmolConfigInfoMD);
                                        $("#mdJSmolDiv").html(Jmol.getAppletHtml(jmolAppletMD));                                                                                                                   
                                    }
                                    
                                    function setMdSpeed() {
                                        var speed = $('#mdSpeedSelect').val();
                                        Jmol.script(jmolAppletMD,"animation FPS " + speed);
                                    }
                                    
                                    function displayMolecularDynamicClip() {
                                        var potim = <xsl:value-of select="//module[@id='initialization']/parameterList/parameter[@dictRef='v:potim']/scalar/text()"/> / 1000;
                                        var clip = $('#mdClipSelect').val();
                                        if(clip == 0)
                                            return;
                                        
                                        var speed = $('#mdSpeedSelect').val();
                                        var range = <xsl:value-of select="if($isLargeMolecule) then '50' else '200'"/>;
                                        var from = (clip -1) * range;
                                        Jmol.script(jmolAppletMD,"set echo top right; echo Loading, please wait.;");
                                        if(Jmol.getPropertyAsString(jmolAppletMD, 'modelInfo.modelCount').endsWith('\t1'))
                                            Jmol.script(jmolAppletMD,"load <xsl:value-of select="$cifurl"/>&#38;mode=vasp_md&#38;index=" + clip + "&#38;range=" + range +  "; set platformSpeed  <xsl:value-of select="if($isLargeMolecule) then '2' else '3'"/>; center;");    
                                        else
                                            Jmol.script(jmolAppletMD,"save ORIENTATION previousOrientation; load <xsl:value-of select="$cifurl"/>&#38;mode=vasp_md&#38;index=" + clip + "&#38;range=" + range + "; set platformSpeed  <xsl:value-of select="if($isLargeMolecule) then '2' else '3'"/>; center;");                                    
                                                        
                                        Jmol.script(jmolAppletMD,"set echo top right; echo Frame: @{(_modelIndex + 1  + " + from  + ")} / <xsl:value-of select="count(//module[@dictRef='cc:calculation']/module[@id='positions']//molecule)"/>| Time: @{((_modelIndex + 1 + " + from  + ")*" + potim + ")%3} ps; restore ORIENTATION previousOrientation 0; animation MODE LOOP; animation ON; animation FPS " + speed + ";");
                                    }
                                    
                                    function resizeMDApplet(){
                                        var width = $("div#molecularDynamics").parent().width() - 50;
                                        var height = $( window ).height() - 50; 
                                        Jmol.resizeApplet(jmolAppletMD, [width,height]);                                                                 
                                    }
                                    
                                    window.onresize = function () {
                                        resizeMDApplet();
                                    };
                                    
                                    function startMD() {
                                        Jmol.script(jmolAppletMD,"animation ON;");
                                    }

                                    function stopMD() {
                                        Jmol.script(jmolAppletMD,"animation OFF;");
                                    }
                                    
                                </script>
                                
                                <div style="padding-bottom: 5px; display: flex; justify-content: flex-start;">                                    
                                    <form class="form-inline">
                                        <div class="form-group">
                                            <label for="mdClipSelect" class="mr-2">Load clip: </label>
                                            <select class="form-control mr-4" id="mdClipSelect" onchange="displayMolecularDynamicClip()">
                                                <option value="0">Select one</option>
                                                <xsl:variable name="range" select="if($isLargeMolecule) then 50 else 200"/>                                    
                                                <xsl:variable name="clipNumber" select="round(count(//module[@dictRef='cc:calculation']/module[@id='positions']//molecule) div $range)" />
                                                <xsl:variable name="clipModulus" select="if((count(//module[@dictRef='cc:calculation']/module[@id='positions']//molecule) mod $range) &gt; 0) then 1 else 0 " />
                                                <xsl:for-each select="1 to xs:integer($clipNumber + $clipModulus)">
                                                    <option value="{.}" ><xsl:value-of select="."/></option>
                                                </xsl:for-each>
                                            </select>   
                                        </div>
                                        <div class="form-group">
                                            <label for="mdSpeedSelect" class="mr-2"> FPS: </label>
                                            <select class="form-control mr-4" id="mdSpeedSelect" onchange="setMdSpeed()">
                                                <option value="5">5</option>
                                                <option value="10" >10</option>
                                                <option value="20" selected="selected">20</option>
                                                <option value="30" >30</option>
                                                <option value="50" >50</option>                                                
                                            </select>   
                                        </div>                                                                                
                                    </form>                                                                     
                                    <button type="button" class="btn btn-default mr-3" name="mdStartAnimation" id="mdStartAnimation" value="Start" onclick="startMD()">
                                        <span class="fas fa-play" aria-hidden="true" /> Play                                        
                                    </button>
                                    <button type="button"  class="btn btn-default mr-5" name="mdStopAnimation" id="mdStopAnimation" value="Stop" onclick="stopMD()">
                                        <span class="fas fa-stop" aria-hidden="true" /> Stop
                                    </button>
                                    <a class="btn btn-secondary" href="{concat($cifurl, '&#38;mode=vasp_md')}" target="_blank" title="Download entire MD as a multi-CIF file"><span class="fas fa-download" aria-hidden="true" /></a>
                                </div>
                                <div id="mdJSmolDiv" style="min-width: 400px; min-height: 400px; padding-left: 10px; padding-right: 10px; display: flex; justify-content: center;">
                                    
                                </div>                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>                                              
        </xsl:if>
    </xsl:template>
    
    <!-- Print license footer -->
    <xsl:template name="printLicense">
        <div class="row">
            <div class="col-md-12 text-right">
                <br/>
                <span>Report data <a rel="license" href="http://creativecommons.org/licenses/by/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/80x15.png" /></a></span>   
                <br/>
                <span>This HTML file <a rel="license" href="http://creativecommons.org/licenses/by-nc-nd/4.0/" target="_blank"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-nd/4.0/80x15.png" /></a></span>                
            </div>
        </div>
    </xsl:template>
    
    <!-- Override default templates -->
    <xsl:template match="text()"/>
    <xsl:template match="*"/>
    
</xsl:stylesheet>
