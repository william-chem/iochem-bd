<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Renders a whole HTML page for displaying item metadata.  Simply includes
  - the relevant item display component in a standard HTML page.
  -
  - Attributes:
  -    display.all - Boolean - if true, display full metadata record
  -    item        - the Item to display
  -    collections - Array of Collections this item appears in.  This must be
  -                  passed in for two reasons: 1) item.getCollections() could
  -                  fail, and we're already committed to JSP display, and
  -                  2) the item might be in the process of being submitted and
  -                  a mapping between the item and collection might not
  -                  appear yet.  If this is omitted, the item display won't
  -                  display any collections.
  -    admin_button - Boolean, show admin 'edit' button
  --%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="org.dspace.content.Collection" %>
<%@ page import="org.dspace.content.Metadatum" %>
<%@ page import="org.dspace.content.Item" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.handle.HandleManager" %>
<%@ page import="org.dspace.license.CreativeCommons" %>
<%@page import="javax.servlet.jsp.jstl.fmt.LocaleSupport"%>
<%@page import="org.dspace.versioning.Version"%>
<%@page import="org.dspace.core.Context"%>
<%@page import="org.dspace.app.webui.util.VersionUtil"%>
<%@page import="org.dspace.app.webui.util.UIUtil"%>
<%@page import="org.dspace.authorize.AuthorizeManager"%>
<%@page import="java.util.List"%>
<%@page import="org.dspace.core.Constants"%>
<%@page import="org.dspace.eperson.EPerson"%>
<%@page import="org.dspace.versioning.VersionHistory"%>
<%@page import="java.net.URL"%>
<%
	//Social media links
	String socialLink = ConfigurationManager.getBooleanProperty("jspui.social.enabled")?"on":"off";

    // Attributes
    Boolean displayAllBoolean = (Boolean) request.getAttribute("display.all");
    boolean displayAll = (displayAllBoolean != null && displayAllBoolean.booleanValue());
    Boolean suggest = (Boolean)request.getAttribute("suggest.enable");
    boolean suggestLink = (suggest == null ? false : suggest.booleanValue());
    Item item = (Item) request.getAttribute("item");
    Collection[] collections = (Collection[]) request.getAttribute("collections");
    Boolean admin_b = (Boolean)request.getAttribute("admin_button");
    boolean admin_button = (admin_b == null ? false : admin_b.booleanValue());
    
    // get the workspace id if one has been passed
    Integer workspace_id = (Integer) request.getAttribute("workspace_id");

    // get the handle if the item has one yet
    String handle = item.getHandle();

    // CC URL & RDF
    String cc_url = CreativeCommons.getLicenseURL(item);
    String cc_rdf = CreativeCommons.getLicenseRDF(item);

    // Full title needs to be put into a string to use as tag argument
    String title = "";
    if (handle == null)
 	{
		title = "Workspace Item";
	}
	else 
	{
		Metadatum[] titleValue = item.getDC("title", null, Item.ANY);
		if (titleValue.length != 0)
		{
			title = titleValue[0].value;
		}
		else
		{
			title = "Item " + handle;
		}
	}
    
    Boolean versioningEnabledBool = (Boolean)request.getAttribute("versioning.enabled");
    boolean versioningEnabled = (versioningEnabledBool!=null && versioningEnabledBool.booleanValue());
    Boolean hasVersionButtonBool = (Boolean)request.getAttribute("versioning.hasversionbutton");
    Boolean hasVersionHistoryBool = (Boolean)request.getAttribute("versioning.hasversionhistory");
    boolean hasVersionButton = (hasVersionButtonBool!=null && hasVersionButtonBool.booleanValue());
    boolean hasVersionHistory = (hasVersionHistoryBool!=null && hasVersionHistoryBool.booleanValue());
    
    Boolean newversionavailableBool = (Boolean)request.getAttribute("versioning.newversionavailable");
    boolean newVersionAvailable = (newversionavailableBool!=null && newversionavailableBool.booleanValue());
    Boolean showVersionWorkflowAvailableBool = (Boolean)request.getAttribute("versioning.showversionwfavailable");
    boolean showVersionWorkflowAvailable = (showVersionWorkflowAvailableBool!=null && showVersionWorkflowAvailableBool.booleanValue());
    
    String latestVersionHandle = (String)request.getAttribute("versioning.latestversionhandle");
    String latestVersionURL = (String)request.getAttribute("versioning.latestversionurl");
    
    VersionHistory history = (VersionHistory)request.getAttribute("versioning.history");
    List<Version> historyVersions = (List<Version>)request.getAttribute("versioning.historyversions");
    
    // Is the logged in user a sys admin, otherwise he won't have access to edition buttons
    Boolean admin = (Boolean)request.getAttribute("is.admin");
    boolean isAdmin = (admin == null ? false : admin.booleanValue());
	if(!isAdmin){
		admin_button = false;
	}
			
	String baseURL = new URL(request.getScheme(), request.getServerName(), request.getServerPort(), request.getContextPath()).toString();	  
	String sourceURL = baseURL + "/handle/" + item.getHandle();
	String cml2xyzUrl = new URL(request.getScheme(),request.getServerName(),request.getServerPort(),request.getContextPath() + "/cml2xyz?id=" + item.getID()).toString() + "&mode=single_xyz";  
	
%>

<%@page import="org.dspace.app.webui.servlet.MyDSpaceServlet"%>
<dspace:layout title="<%= title %>" sociallinks="<%=socialLink%>">
<%
    if (handle != null)
    {
%>

		<%		
		if (newVersionAvailable)
		   {
		%>
		<div class="alert alert-warning"><b><fmt:message key="jsp.version.notice.new_version_head"/></b>		
		<fmt:message key="jsp.version.notice.new_version_help"/><a href="<%=latestVersionURL %>"><%= latestVersionHandle %></a>
		</div>
		<%
		    }
		%>
		
		<%		
		if (showVersionWorkflowAvailable)
		   {
		%>
		<div class="alert alert-warning"><b><fmt:message key="jsp.version.notice.workflow_version_head"/></b>		
		<fmt:message key="jsp.version.notice.workflow_version_help"/>
		</div>
		<%
		    }
		%>
<%
        if (admin_button)  // admin edit button
        { %>
        <dspace:sidebar>
            <div class="panel panel-warning">
            	<div class="panel-heading"><fmt:message key="jsp.admintools"/></div>
            	<div class="panel-body">
                <form method="get" action="<%= request.getContextPath() %>/tools/edit-item">
                    <input type="hidden" name="item_id" value="<%= item.getID() %>" />
                    <%--<input type="submit" name="submit" value="Edit...">--%>
                    <input class="btn btn-default col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.general.edit.button"/>" />
                </form>
                <form method="post" action="<%= request.getContextPath() %>/mydspace">
                    <input type="hidden" name="item_id" value="<%= item.getID() %>" />
                    <input type="hidden" name="step" value="<%= MyDSpaceServlet.REQUEST_EXPORT_ARCHIVE %>" />
                    <input class="btn btn-default col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.mydspace.request.export.item"/>" />
                </form>
                <form method="post" action="<%= request.getContextPath() %>/mydspace">
                    <input type="hidden" name="item_id" value="<%= item.getID() %>" />
                    <input type="hidden" name="step" value="<%= MyDSpaceServlet.REQUEST_MIGRATE_ARCHIVE %>" />
                    <input class="btn btn-default col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.mydspace.request.export.migrateitem"/>" />
                </form>
                <form method="post" action="<%= request.getContextPath() %>/dspace-admin/metadataexport">
                    <input type="hidden" name="handle" value="<%= item.getHandle() %>" />
                    <input class="btn btn-default col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.general.metadataexport.button"/>" />
                </form>
					<% if(hasVersionButton) { %>       
                	<form method="get" action="<%= request.getContextPath() %>/tools/version">
                    	<input type="hidden" name="itemID" value="<%= item.getID() %>" />                    
                    	<input class="btn btn-default col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.general.version.button"/>" />
                	</form>
                	<% } %> 
                	<% if(hasVersionHistory) { %>			                
                	<form method="get" action="<%= request.getContextPath() %>/tools/history">
                    	<input type="hidden" name="itemID" value="<%= item.getID() %>" />
                    	<input type="hidden" name="versionID" value="<%= history.getVersion(item)!=null?history.getVersion(item).getVersionId():null %>" />                    
                    	<input class="btn btn-info col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.general.version.history.button"/>" />
                	</form>         	         	
					<% } %>
             </div>
          </div>
        </dspace:sidebar>
<%      } %>

<%
    }

    String displayStyle = (displayAll ? "full" : "");
%>
    <dspace:item-preview item="<%= item %>" />
    <dspace:item item="<%= item %>" collections="<%= collections %>" style="<%= displayStyle %>" />
    
<div class="row">
	<div class="col-md-6 col-sm-12">
<%
    String locationLink = request.getContextPath() + "/handle/" + handle;

    if (displayAll)
    {
%>
<%
        if (workspace_id != null)
        {
%>
    <form class="col-md-2" method="post" action="<%= request.getContextPath() %>/view-workspaceitem">
        <input type="hidden" name="workspace_id" value="<%= workspace_id.intValue() %>" />
        <input class="btn btn-default" type="submit" name="submit_simple" value="<fmt:message key="jsp.display-item.text1"/>" />
    </form>
<%
        }
        else
        {
%>
    <a class="btn btn-default" href="<%=locationLink %>?mode=simple">
        <fmt:message key="jsp.display-item.text1"/>
    </a>
<%
        }
%>
<%
    }
    else
    {
%>
<%
        if (workspace_id != null)
        {
%>
    <form class="col-md-2" method="post" action="<%= request.getContextPath() %>/view-workspaceitem">
        <input type="hidden" name="workspace_id" value="<%= workspace_id.intValue() %>" />
        <input class="btn btn-default" type="submit" name="submit_full" value="<fmt:message key="jsp.display-item.text2"/>" />
    </form>
<%
        }
        else
        {
%>
    <a class="btn btn-default" href="<%=locationLink %>?mode=full">
        <fmt:message key="jsp.display-item.text2"/>
    </a>
<%
        }
    }

    if (workspace_id != null)
    {
%>
   <form class="col-md-2" method="post" action="<%= request.getContextPath() %>/workspace">
        <input type="hidden" name="workspace_id" value="<%= workspace_id.intValue() %>"/>
        <input class="btn btn-primary" type="submit" name="submit_open" value="<fmt:message key="jsp.display-item.back_to_workspace"/>"/>
    </form>
<%
    } else {

		if (suggestLink)
        {
%>
    <a class="btn btn-success" href="<%= request.getContextPath() %>/suggest?handle=<%= handle %>" target="new_window">
       <fmt:message key="jsp.display-item.suggest"/></a>
<%
        }
		
		if(isAdmin){		
%>
    <a class="statisticsLink  btn btn-primary" href="<%= request.getContextPath() %>/handle/<%= handle %>/statistics"><fmt:message key="jsp.display-item.display-statistics"/></a>

<%      }
    

    if (ConfigurationManager.getProperty("sfx.server.url") != null)
    {
        String sfximage = ConfigurationManager.getProperty("sfx.server.image_url");
        if (sfximage == null)
        {
            sfximage = request.getContextPath() + "/image/sfx-link.gif";
        }
%>
        <a class="btn btn-default" href="<dspace:sfxlink item="<%= item %>"/>" /><img src="<%= sfximage %>" border="0" alt="SFX Query" /></a>
<%
    }
    }
%>
	</div>	
	<%
    if(socialLink.equals("on"))
    {
%>  
	<div class="col-md-6 col-sm-12">
		<div class="text-right">
			<span class='st_twitter_large' displayText='Tweet'></span>
			<span class='st_linkedin_large' displayText='LinkedIn'></span>
			<span class='st_facebook_large' displayText='Facebook'></span>
			<span class='st_email_large' displayText='Email'></span>
			<span class='st_sharethis_large' displayText='ShareThis'></span>
		</div>
	</div>	
<%	} %>
</div>
<br/>
    <%-- Versioning table --%>
<%
    if (versioningEnabled && hasVersionHistory)
    {
        boolean item_history_view_admin = ConfigurationManager
                .getBooleanProperty("versioning", "item.history.view.admin");
        if(!item_history_view_admin || admin_button) {         
%>
	<div id="versionHistory" class="panel panel-info">
	<div class="panel-heading"><fmt:message key="jsp.version.history.head2" /></div>
	
	<table class="table panel-body">
		<tr>
			<th id="tt1" class="oddRowEvenCol"><fmt:message key="jsp.version.history.column1"/></th>
			<th 			
				id="tt2" class="oddRowOddCol"><fmt:message key="jsp.version.history.column2"/></th>
			<th 
				 id="tt3" class="oddRowEvenCol"><fmt:message key="jsp.version.history.column3"/></th>
			<th 
				
				id="tt4" class="oddRowOddCol"><fmt:message key="jsp.version.history.column4"/></th>
			<th 
				 id="tt5" class="oddRowEvenCol"><fmt:message key="jsp.version.history.column5"/> </th>
		</tr>
		
		<% for(Version versRow : historyVersions) {  
		
			EPerson versRowPerson = versRow.getEperson();
			String[] identifierPath = VersionUtil.addItemIdentifier(item, versRow);
		%>	
		<tr>			
			<td headers="tt1" class="oddRowEvenCol"><%= versRow.getVersionNumber() %></td>
			<td headers="tt2" class="oddRowOddCol"><a href="<%= request.getContextPath() + identifierPath[0] %>"><%= identifierPath[1] %></a><%= item.getID()==versRow.getItemID()?"<span class=\"glyphicon glyphicon-asterisk\"></span>":""%></td>
			<td headers="tt3" class="oddRowEvenCol"><% if(admin_button) { %><a
				href="mailto:<%= versRowPerson.getEmail() %>"><%=versRowPerson.getFullName() %></a><% } else { %><%=versRowPerson.getFullName() %><% } %></td>
			<td headers="tt4" class="oddRowOddCol"><%= versRow.getVersionDate() %></td>
			<td headers="tt5" class="oddRowEvenCol"><%= versRow.getSummary() %></td>
		</tr>
		<% } %>
	</table>
	<div class="panel-footer"><fmt:message key="jsp.version.history.legend"/></div>
	</div>
<%
        }
    }
%>
<br/>
<div class="well"><fmt:message key="jsp.display-item.identifier"/>
           <code><%= HandleManager.getCanonicalForm(handle) %></code>
</div>
    <%-- Create Commons Link --%>
<%
    if (cc_url != null)
    {
%>
    <p class="submitFormHelp alert alert-info"><fmt:message key="jsp.display-item.text3"/> <a href="<%= cc_url %>"><fmt:message key="jsp.display-item.license"/></a>
    <a href="<%= cc_url %>"><img src="<%= request.getContextPath() %>/image/cc-somerights.gif" border="0" alt="Creative Commons" style="margin-top: -5px;" class="pull-right"/></a>
    </p>
    <!--
    <%= cc_rdf %>
    -->
<%
    } else {
%>
    <p class="submitFormHelp alert alert-info text-right"><fmt:message key="jsp.display-item.copyright"/></p>
<%
    } 
%>


<div class="modal fade" id="modalDisplayItem" tabindex="-1" role="dialog" aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog">
     	<div class="modal-content">        	
         	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal">
                	<span aria-hidden="true">&times;</span>
                	<span class="sr-only">Close</span>
            	</button>
             	<h4 class="modal-title" id="modalDisplayItemLabel">
                	<fmt:message key="org.dspace.app.webui.jsptag.ItemTag.jmolView"/>
             	</h4>
         </div>         
            <div class="modal-body">	                
                <form class="form-horizontal" role="form">
                	<div class="form-group">
                   		<label for="modalDisplayItemInputTextArea">Copy paste following text:</label>
                   		<textarea class="form-control" id="modalDisplayItemTextArea" rows="20"></textarea>
                 	</div>
                </form>
            </div>
        </div>
    </div>
</div>




<!--  HTML Code snippet -->
<script type="text/javascript">
	var xyz = null;	
	var cml2xyzUrl = '<%=cml2xyzUrl%>';

	$(document).ready(function(){
		retrieveXYZ(cml2xyzUrl);
	});
</script>
<div class="modal fade" id="htmlCodeSnippet" role="dialog"  aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog modal-dialog-centered modal-lg">
     	<div class="modal-content">        
         	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal">
                	<span aria-hidden="true">&times;</span>
                	<span class="sr-only">Close</span>
            	</button>
             	<h4 class="modal-title" id="htmlCodeSnippetLabel">
                	<fmt:message key="org.dspace.app.webui.jsptag.ItemTag.htmlSnippetLabel"/>
             	</h4>
         	</div>                 
            <div class="modal-body">
				<code id="htmlCode" class="language-html"><!-- HTML generated using hilite.me -->				
<div style="background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;">
	<pre style="margin: 0; line-height: 125%"><span style="color: #557799">&lt;!DOCTYPE html&gt;</span>
<span style="color: #007700">&lt;html</span> <span style="color: #0000CC">lang=</span><span style="background-color: #fff0f0">&quot;en&quot;</span><span style="color: #007700">&gt;</span>
    <span style="color: #007700">&lt;head&gt;</span>
        <span style="color: #007700">&lt;meta</span> <span style="color: #0000CC">charset=</span><span style="background-color: #fff0f0">&quot;utf-8&quot;</span> <span style="color: #007700">/&gt;</span>
        <span style="color: #007700">&lt;meta</span> <span style="color: #0000CC">http-equiv=</span><span style="background-color: #fff0f0">&quot;x-ua-compatible&quot;</span> <span style="color: #0000CC">content=</span><span style="background-color: #fff0f0">&quot;ie=edge&quot;</span> <span style="color: #007700">/&gt;</span>
        <span style="color: #007700">&lt;meta</span> <span style="color: #0000CC">name=</span><span style="background-color: #fff0f0">&quot;viewport&quot;</span> <span style="color: #0000CC">content=</span><span style="background-color: #fff0f0">&quot;width=device-width, initial-scale=1&quot;</span> <span style="color: #007700">/&gt;</span>        
        <span style="color: #007700">&lt;title&gt;&lt;/title&gt;</span>
        <span style="color: #007700">&lt;link</span> <span style="color: #0000CC">rel=</span><span style="background-color: #fff0f0">&quot;icon&quot;</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;<%=baseURL%>/images/favicon.png&quot;</span> <span style="color: #007700">/&gt;</span>        
    <span style="color: #007700">&lt;/head&gt;</span>
    
    <span style="color: #007700">&lt;body&gt;</span>
      <span style="color: #007700">&lt;script </span><span style="color: #0000CC">src=</span><span style="background-color: #fff0f0">&quot;https://cdnjs.cloudflare.com/ajax/libs/3Dmol/1.4.0/3Dmol-min.js&quot;</span><span style="color: #007700">&gt;&lt;/script&gt;</span> 
      <span style="color: #007700">&lt;div</span> <span style="color: #0000CC">id=</span><span style="background-color: #fff0f0">&quot;iochembdGeometry&quot;</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;height: 400px; width: 400px; position: relative;&quot;</span> <span style="color: #0000CC">data-backgroundcolor=</span><span style="background-color: #fff0f0">&#39;0xffffff&#39;</span><span style="color: #007700">&gt;</span>
      	<span style="color: #007700">&lt;span</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;display:inline-block;z-index:1;position: absolute;bottom: 0px;right: 0px;font-family: &amp;quot;Helvetica Neue&amp;quot;, Helvetica, Arial, sans-serif;&quot;</span><span style="color: #007700">&gt;</span>Source at <span style="color: #007700">&lt;a</span> <span style="color: #0000CC">target=</span><span style="background-color: #fff0f0">&quot;_blank&quot;</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;<%=sourceURL %>&quot;</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;&quot;</span><span style="color: #007700">&gt;</span>ioChem-BD<span style="color: #007700">&lt;/a&gt;&lt;/span&gt;</span>
      <span style="color: #007700">&lt;/div&gt;</span>
      
      <span style="color: #007700">&lt;script&gt;</span>
   <span style='color: #008800; font-weight: bold'>var</span> data <span style='color: #333333'>=</span><span style='background-color: #fff0f0'>&quot;XYZ_PLACEHOLDER&quot;</span>; 
   <span style="color: #008800; font-weight: bold">var</span> viewer_first <span style="color: #333333">=</span> $3Dmol.createViewer(<span style="background-color: #fff0f0">&quot;iochembdGeometry&quot;</span>, {backgroundColor<span style="color: #333333">:</span><span style="background-color: #fff0f0">&quot;white&quot;</span>}); 
   viewer_first.addModel(data,<span style="background-color: #fff0f0">&quot;xyz&quot;</span>); 
   viewer_first.setStyle({<span style="background-color: #fff0f0">&quot;sphere&quot;</span><span style="color: #333333">:</span> {<span style="background-color: #fff0f0">&quot;radius&quot;</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0.3</span>}}); 
   viewer_first.addStyle({<span style="background-color: #fff0f0">&quot;stick&quot;</span><span style="color: #333333">:</span> {<span style="background-color: #fff0f0">&quot;radius&quot;</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0.2</span>}});
   viewer_first.zoomTo(); 
   viewer_first.render(); 
<span style="color: #007700">&lt;/script&gt;</span>   
                   
    <span style="color: #007700">&lt;/body&gt;</span>
<span style="color: #007700">&lt;/html&gt;</span>
</pre></div></code>
            </div>
        </div>
    </div>

</div>

<!-- Authorea Code snippet -->
<div class="modal fade" id="authoreaCodeSnippet" role="dialog"  aria-hidden="true" style="z-index:999999">
	<div class="modal-dialog modal-dialog-centered modal-lg">
     	<div class="modal-content">        
         	<div class="modal-header">
            	<button type="button" class="close" data-dismiss="modal">
                	<span aria-hidden="true">&times;</span>
                	<span class="sr-only">Close</span>
            	</button>
             	<h4 class="modal-title" id="authoreaCodeSnippetLabel">
                	<fmt:message key="org.dspace.app.webui.jsptag.ItemTag.authoreaSnippetLabel"/>
             	</h4>
         	</div>                 
            <div class="modal-body">	                                
				<code id="authoreaCode" class="language-html">										
					<!-- HTML generated using hilite.me -->					
<div style='background: #ffffff; overflow:auto;width:auto;border:solid gray;border-width:.1em .1em .1em .8em;padding:.2em .6em;'><pre style='margin: 0; line-height: 125%'> 
<span style="color: #007700">&lt;script </span><span style="color: #0000CC">src=</span><span style="background-color: #fff0f0">&quot;https://cdnjs.cloudflare.com/ajax/libs/3Dmol/1.4.0/3Dmol-min.js&quot;</span><span style="color: #007700">&gt;&lt;/script&gt;</span> 
<span style="color: #007700">&lt;div</span> <span style="color: #0000CC">id=</span><span style="background-color: #fff0f0">&quot;iochembdGeometry&quot;</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;height: 400px; width: 400px; position: relative;&quot;</span> <span style="color: #0000CC">data-backgroundcolor=</span><span style="background-color: #fff0f0">&#39;0xffffff&#39;</span><span style="color: #007700">&gt;</span>
<span style="color: #007700">&lt;span</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;display:inline-block;z-index:1;position: absolute;bottom: 0px;right: 0px;font-family: &amp;quot;Helvetica Neue&amp;quot;, Helvetica, Arial, sans-serif;&quot;</span><span style="color: #007700">&gt;</span>Source at <span style="color: #007700">&lt;a</span> <span style="color: #0000CC">target=</span><span style="background-color: #fff0f0">&quot;_blank&quot;</span> <span style="color: #0000CC">href=</span><span style="background-color: #fff0f0">&quot;<%=sourceURL %>&quot;</span> <span style="color: #0000CC">style=</span><span style="background-color: #fff0f0">&quot;&quot;</span><span style="color: #007700">&gt;</span>ioChem-BD<span style="color: #007700">&lt;/a&gt;&lt;/span&gt;</span>
<span style="color: #007700">&lt;/div&gt;</span>
<span style="color: #007700">&lt;script&gt;</span>
   <span style='color: #008800; font-weight: bold'>var</span> data <span style='color: #333333'>=</span><span style='background-color: #fff0f0'>&quot;XYZ_PLACEHOLDER&quot;</span>; 
   <span style="color: #008800; font-weight: bold">var</span> viewer_first <span style="color: #333333">=</span> $3Dmol.createViewer(<span style="background-color: #fff0f0">&quot;iochembdGeometry&quot;</span>, {backgroundColor<span style="color: #333333">:</span><span style="background-color: #fff0f0">&quot;white&quot;</span>}); 
   viewer_first.addModel(data,<span style="background-color: #fff0f0">&quot;xyz&quot;</span>); 
   viewer_first.setStyle({<span style="background-color: #fff0f0">&quot;sphere&quot;</span><span style="color: #333333">:</span> {<span style="background-color: #fff0f0">&quot;radius&quot;</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0.3</span>}}); 
   viewer_first.addStyle({<span style="background-color: #fff0f0">&quot;stick&quot;</span><span style="color: #333333">:</span> {<span style="background-color: #fff0f0">&quot;radius&quot;</span><span style="color: #333333">:</span> <span style="color: #6600EE; font-weight: bold">0.2</span>}});
   viewer_first.zoomTo(); 
   viewer_first.render(); 
<span style="color: #007700">&lt;/script&gt;</span> 
</pre></div>							
				</code>
				                
            </div>
        </div>
    </div>

</div>
   
    
</dspace:layout>



