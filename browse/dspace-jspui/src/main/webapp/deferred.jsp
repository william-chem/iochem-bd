<%--

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Display a tombstone indicating that an item has been withdrawn.
  --%>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<%@ page import="org.dspace.core.ConfigurationManager" %>

<% String status = (String) request.getAttribute("status"); %>    

<dspace:layout titlekey="jsp.deferred.title">

<% if(status == null || "display_form".equals(status)) {
     String recaptcha = (String) request.getAttribute("recaptcha.site.key");
%>
	<script type="text/javascript" src="https://www.google.com/recaptcha/api.js?render=<%=recaptcha%>"></script>	
	<script>
	    grecaptcha.ready(function () {
	        grecaptcha.execute('<%=recaptcha%>', {action: 'submit'}).then(function (token) {	            
	            document.getElementById('g-recaptcha-response').value = token;
	        });
	    });
	</script>


    <h2><fmt:message key="jsp.deferred.title"/></h2>
    <p>
    	<fmt:message key="jsp.deferred.text">
    		<fmt:param><%=request.getAttribute("file.size.gb") %></fmt:param>
    	</fmt:message>
    </p>
    <br/>
    
	<form id="deferred-download-form" method="post" action="">
		<div class="row">
            <label class="col-md-2" style="font-size:1em" for="itemname">Item name:</label>
            <div class="col-md-10">
            	<p id="itemname"><%=request.getAttribute("item.name") %></p>
            </div>           
        </div>		
		<div class="row">
            <label class="col-md-2" style="font-size:1em" for="itemhandle">Handle:</label>
            <div class="col-md-10">
            	<p id="itemhandle">
					<a href="<%= request.getContextPath() %>/handle/<%=request.getAttribute("item.handle") %>"><%=request.getAttribute("item.handle") %></a>
            	</p>
            </div>  
            
        </div>
		<div class="row">
            <label class="col-md-2" style="font-size:1em" for="bitstreamname">File name:</label>
            <div class="col-md-10">
            	<p id="bitstreamname"><%=request.getAttribute("bitstream.name") %></p>
            </div>           
        </div>
		<div class="row">
            <label class="col-md-2" style="font-size:1em" for="bitstreamsize">File size:</label>
            <div class="col-md-10">
            	<p id="bitstreamsize"><%=request.getAttribute("bitstream.size") %> MB</p>
            </div>           
        </div>	
	
	
		<div class="row">
            <label class="col-md-2" style="font-size:1em" for="temail"><fmt:message key="jsp.dspace-admin.eperson-edit.email"/></label>
            <div class="col-md-4" id="temaildiv">
            	<input class="form-control" name="email" id="temail" size="24" value="" required/>
            </div>
            <div class="col-md-4">               
               <input type="hidden" id="bitstream" name="bitstream" value="<%=request.getAttribute("bitstream.id")%>">
               <input type="hidden" id="url" name="url" value="<%=request.getAttribute("bitstream.url")%>">
               <input type="hidden" id="g-recaptcha-response" name="g-recaptcha-response">
               <input type="submit" id="submitBtn" class="btn btn-block btn-primary" value="Request download"/>
            </div>
        </div>	
        
        <script>
	        $(document).ready(function () {
	        	const email = document.getElementById('temail');
	            email.addEventListener('blur', ()=> {
	               let regex = /^([_\-\.0-9a-zA-Z]+)@([_\-\.0-9a-zA-Z]+)\.([a-zA-Z]){2,7}$/;
	               let s = email.value;
	               if(regex.test(s)){
						document.getElementById('temaildiv').classList.remove('has-error');
	                	emailError = true;
	               }else{
	            	  	document.getElementById('temaildiv').classList.add('has-error');
	                  	emailError = false;
	               }
	            });
	            
				$('#submitBtn').click(function () {
				    if ((emailError == true)) {
				        return true;
				    } else {
				        return false;
				    }
				});
				
	        });
        </script>        
	</form>   
<% } else if("success".equals(status)) { %>

    <h2><fmt:message key="jsp.deferred.title"/></h2>
    <br/>
    <p><fmt:message key="jsp.deferred.success.text"/></p>    
	<br/>
	<br/>

<% } else if("error".equals(status)) { %>

    <h2><fmt:message key="jsp.deferred.title"/></h2>
    <br/>
    <p>
    	<fmt:message key="jsp.deferred.error.text">
      	    <fmt:param><%= ConfigurationManager.getProperty("feedback.recipient") %></fmt:param>
    	</fmt:message>
    </p>

<% } %>
	<br/>
	<br/>
	<br/>
	<div class="row">
		<div class="col-md-12">
			<p class="text-center">
		        <a href="<%= request.getContextPath() %>/"><fmt:message key="jsp.general.gohome"/></a>
		    </p>
		</div>		
	</div>

</dspace:layout>
