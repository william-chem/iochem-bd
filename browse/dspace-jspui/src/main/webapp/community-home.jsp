<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Community home JSP
  -
  - Attributes required:
  -    community             - Community to render home page for
  -    collections           - array of Collections in this community
  -    subcommunities        - array of Sub-communities in this community
  -    last.submitted.titles - String[] of titles of recently submitted items
  -    last.submitted.urls   - String[] of URLs of recently submitted items
  -    admin_button - Boolean, show admin 'edit' button
  --%>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="org.dspace.app.webui.components.RecentSubmissions" %>

<%@ page import="org.dspace.app.webui.servlet.admin.EditCommunitiesServlet" %>
<%@ page import="org.dspace.app.webui.util.UIUtil" %>
<%@ page import="org.dspace.browse.BrowseIndex" %>
<%@ page import="org.dspace.browse.ItemCounter" %>
<%@ page import="org.dspace.content.*" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.Utils" %>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.content.service.ItemService" %>
<%@ page import="org.dspace.core.Constants" %>
<%@ page import="org.dspace.core.Context"%>
<%@ page import="java.util.List" %>
<%@ page import="org.dspace.content.ItemIterator" %>
<%@ page import="org.dspace.identifier.DOIIdentifierProvider" %>
<%@ page import="org.dspace.identifier.doi.CrossRefDOIConnector" %>
<%@ page import="org.dspace.app.webui.servlet.CollectionEditServlet" %>

<%	//Social media links
	String socialLink = ConfigurationManager.getBooleanProperty("jspui.social.enabled")?"on":"off";

    // Retrieve attributes
    Community community = (Community) request.getAttribute( "community" );
    Collection[] collections = (Collection[]) request.getAttribute("collections");
    Community[] subcommunities = (Community[]) request.getAttribute("subcommunities");
    
    RecentSubmissions rs = (RecentSubmissions) request.getAttribute("recently.submitted");
    
    Boolean editor_b = (Boolean)request.getAttribute("editor_button");
    boolean editor_button = (editor_b == null ? false : editor_b.booleanValue());
    Boolean add_b = (Boolean)request.getAttribute("add_button");
    boolean add_button = (add_b == null ? false : add_b.booleanValue());
    Boolean remove_b = (Boolean)request.getAttribute("remove_button");
    boolean remove_button = (remove_b == null ? false : remove_b.booleanValue());

	// get the browse indices
    BrowseIndex[] bis = BrowseIndex.getBrowseIndices();

    // Put the metadata values into guaranteed non-null variables
    String name = community.getMetadata("name");
    String intro = community.getMetadata("introductory_text");
    String copyright = community.getMetadata("copyright_text");
    String sidebar = community.getMetadata("side_bar_text");
    Bitstream logo = community.getLogo();
    
    boolean feedEnabled = ConfigurationManager.getBooleanProperty("webui.feed.enable");
    String feedData = "NONE";
    if (feedEnabled)
    {
        feedData = "comm:" + ConfigurationManager.getProperty("webui.feed.formats");
    }
    
    ItemCounter ic = new ItemCounter(UIUtil.obtainContext(request));
    
    // Is the logged in user a sys admin, otherwise he won't have access to edition buttons
    Boolean admin = (Boolean)request.getAttribute("is.admin");
    boolean isAdmin = (admin == null ? false : admin.booleanValue());
	if(!isAdmin){
		editor_button = false;
		add_button = false;
		remove_button = false;		
	}
	
	DOIIdentifierProvider doiProvider = new DOIIdentifierProvider();
	doiProvider.setDOIConnector(new CrossRefDOIConnector());
    
%>

<%@page import="org.dspace.app.webui.servlet.MyDSpaceServlet"%>
<dspace:layout locbar="commLink" title="<%= name %>" feedData="<%= feedData %>" sociallinks="<%=socialLink%>">

<div class="well">
<div class="row">
	<div class="col-md-12">
		<div id="community_accordion">		
        	<h2><%= name %>
        <%
            if(ConfigurationManager.getBooleanProperty("webui.strengths.show"))
            {
%>
                : [<%= ic.getCount(community) %>]
<%
            }
%>
			<small><fmt:message key="jsp.community-home.heading1"/></small>
		<% if(isAdmin){ %>        
        	<a class="statisticsLink btn btn-info" href="<%= request.getContextPath() %>/handle/<%= community.getHandle() %>/statistics"><fmt:message key="jsp.community-home.display-statistics"/></a>
		<% 	} %>
			<small><span id="community_expand" class="glyphicon glyphicon-chevron-down" style="visibility:hidden"></span></small>       
			</h2>
<% if (logo != null || StringUtils.isNotBlank(intro)) { %>			
			<div class="row">
				<%  if (logo != null) { %>
				     <div class="col-md-4 col-sm-12">
				     	<img class="img-responsive" alt="Logo" src="<%= request.getContextPath() %>/retrieve/<%= logo.getID() %>" />
				     </div> 
				<% }			
			 		if (StringUtils.isNotBlank(intro)) { %>
					  <div class="col-md-8 col-sm-12">
  						<%= intro %>
  					  </div>
				<% } %>
				<script type="text/javascript">
					$("#community_accordion").accordion({
	  						collapsible: true,
	  						active: false 
					});
					$("#community_expand").css("visibility","visible");
					
				</script>
			</div>
<% } %>			
			
		</div>

 		</div>
	</div>
</div>


<p class="copyrightText"><%= copyright %></p>



<%-- Browse --%>
<div class="panel panel-primary">
	<div class="panel-heading"><fmt:message key="jsp.general.browse"/></div>
	<div class="panel-body">
   				<%-- Insert the dynamic list of browse options --%>
<%
	for (int i = 0; i < bis.length; i++)
	{
		String key = "browse.menu." + bis[i].getName();
%>
	<form method="get" action="<%= request.getContextPath() %>/handle/<%= community.getHandle() %>/browse">
		<input type="hidden" name="type" value="<%= bis[i].getName() %>"/>
		<%-- <input type="hidden" name="community" value="<%= community.getHandle() %>" /> --%>
		<input class="btn btn-default col-md-3 col-sm-12" type="submit" name="submit_browse" value="<fmt:message key="<%= key %>"/>"/>
	</form>
<%	
	}
%>
			
	</div>
</div>
	
<div class="row">
<%
	boolean showLogos = ConfigurationManager.getBooleanProperty("jspui.community-home.logos", true);
	if (subcommunities.length != 0)
    {
%>
	<div class="col-md-12">

		<h3><fmt:message key="jsp.community-home.heading3"/></h3>
   
        <div class="list-group row">
<%
        for (int j = 0; j < subcommunities.length; j++)
        {
%>
			<div class="list-group-item col-md-6 col-sm-12">
				<div class="row">  
<%  
		Bitstream logoCom = subcommunities[j].getLogo();
		if (showLogos && logoCom != null) { %>
			<div class="col-md-3">
		        <img alt="Logo" class="img-responsive" src="<%= request.getContextPath() %>/retrieve/<%= logoCom.getID() %>" /> 
			</div>
			<div class="col-md-9">
<% } else { %>
			<div class="col-md-12">
<% }  %>		

	      <h4 class="list-group-item-heading"><a href="<%= request.getContextPath() %>/handle/<%= subcommunities[j].getHandle() %>">
	                <%= subcommunities[j].getMetadata("name") %></a>
<%
                if (ConfigurationManager.getBooleanProperty("webui.strengths.show"))
                {
%>
                    [<%= ic.getCount(subcommunities[j]) %>]
<%
                }
%>
	    		<% if (remove_button) { %>
	                <form class="btn-group" method="post" action="<%=request.getContextPath()%>/tools/edit-communities">
			          <input type="hidden" name="parent_community_id" value="<%= community.getID() %>" />
			          <input type="hidden" name="community_id" value="<%= subcommunities[j].getID() %>" />
			          <input type="hidden" name="action" value="<%=EditCommunitiesServlet.START_DELETE_COMMUNITY%>" />
	                  <button type="submit" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
	                </form>
	    		<% } %>
			    </h4>
                <p class="collectionDescription"><%= subcommunities[j].getMetadata("short_description") %></p>          
        	 </div> 
     	</div>
     </div>
<%
        }
%> 		
		</div>
	</div>
<% }
	
    if (collections.length != 0)
    {
%>
	<div class="col-md-12">

        <%-- <h2>Collections in this community</h2> --%>
		<h3><fmt:message key="jsp.community-home.heading2"/></h3>
		<div class="list-group row">
<%
        for (int i = 0; i < collections.length; i++)
        {
%>
			<div class="list-group-item col-md-12 col-sm-12">
				<div class="row">
<%  		      Bitstream logoCol = collections[i].getLogo();   
%>
					<div class="col-md-12">
	      				<h4 class="list-group-item-heading">
	      					<a href="<%= request.getContextPath() %>/handle/<%= collections[i].getHandle() %>"><%= collections[i].getMetadata("name") 
%>							</a>		      					
<%								if(ConfigurationManager.getBooleanProperty("webui.strengths.show")) { 
%>
                					[<%= ic.getCount(collections[i]) %>]
<%           					} 
 								if(remove_button){ 
%>
							      <form class="btn-group" method="post" action="<%=request.getContextPath()%>/tools/edit-communities">
							          <input type="hidden" name="parent_community_id" value="<%= community.getID() %>" />
							          <input type="hidden" name="community_id" value="<%= community.getID() %>" />
							          <input type="hidden" name="collection_id" value="<%= collections[i].getID() %>" />
							          <input type="hidden" name="action" value="<%=EditCommunitiesServlet.START_DELETE_COLLECTION%>" />
							          <button type="submit" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-trash"></span></button>
							      </form>
<% 							    }
 								String doi = null;
 								try{
 									doi = doiProvider.lookup(UIUtil.obtainContext(request), (DSpaceObject)collections[i]);
 									if(doi != null)
 										doi = doi.replaceFirst("doi:", "");
 								}catch(Exception e){}
 								
 								if(doi != null){ 
%> 
 									<small>DOI: <a href="https://doi.org/<%=doi%>" target="_blank"><%=doi%></a></small>
<%								} 
%>
					  </h4>
    				  <p class="collectionDescription"><%= collections[i].getMetadata("introductory_text") %></p>
<%                      ItemIterator itemIter = collections[i].getItems(1, 0);
						if(itemIter.hasNext()){							
							Item item = itemIter.next();						
							List<Metadatum> manuscriptInfo = item.getMetadata("dc","relation", null, Item.ANY, Item.ANY);						      				 
					  		if(manuscriptInfo.size() > 0 && manuscriptInfo.get(0).value.startsWith("Original title")) {
							  	String paperMetadata = manuscriptInfo.get(0).value;       
							    String title = paperMetadata.replaceAll(CollectionEditServlet.TITLE_REGEX,"").trim();
							    String paperDoi = paperMetadata.replaceAll(CollectionEditServlet.DOI_REGEX,"").trim();
							    String journal = paperMetadata.replaceAll(CollectionEditServlet.JOURNAL_REGEX, "").trim();
%>
					  			<p class="collectionJournalDescription">This dataset derived results are published in:</p>
<%								if(!title.equals("")) { 
%>									<p class="collectionJournalLine"><span style='font-style:oblique'>Manuscript title</span>: <%=title%></p>
<%								} if(!journal.equals("")) { 
%>									<p class="collectionJournalLine"><span style='font-style:oblique'>Journal</span>: <%=journal%></p>										
<% 					    		} if(!paperDoi.equals("")) {%>						  						  	
									<p class="collectionJournalLine"><span style='font-style:oblique'>DOI</span>: <a href="https://doi.org/<%=paperDoi%>" target="_blank"><%=paperDoi%></a> </p>
<%                      		}
%>					  	
<%					  		} else { 
%>				  				<p class="collectionJournalDescription">No other publication derived</p>
<%					  		}
				       } else { 
%>				  		  <p class="collectionJournalDescription">No other publication derived</p>
<%					   }
%>    				  
	    			</div>
  				</div>  
  			</div>  
<%   	}
%>  
			</div>
		</div>
<% } 
%>

</div>

<div class="row">

    <%
    	int discovery_panel_cols = 12;
    	int discovery_facet_cols = 4;
    %>
	<%@ include file="discovery/static-sidebar-facet.jsp" %>
</div>

<div class="row">
	<%@ include file="discovery/static-tagcloud-facet.jsp" %>
</div>



<% if(socialLink.equals("on")){ %>
<div class="row">	
<%
	if (rs != null)
	{ %>
	<div class="col-md-12">
        <div class="panel panel-primary recent-submissions">        
        <div id="recent-submissions-carousel" class="panel-heading carousel slide">
        <%-- Recently Submitted items --%>
			<h3>
				<fmt:message key="jsp.community-home.recentsub"/>
<%
    if(feedEnabled)
    {
    	String[] fmts = feedData.substring(5).split(",");
    	String icon = null;
    	int width = 0;
    	for (int j = 0; j < fmts.length; j++)
    	{
    		if ("rss_1.0".equals(fmts[j]))
    		{
    		   icon = "rss1.png";
    		   width = 43;
    		}
    		else if ("rss_2.0".equals(fmts[j]))
    		{
    		   icon = "rss2.png";
    		   width = 43;
    		}
    		else
    	    {
    	       icon = "rss.gif";
    	       width = 36;
    	    }
%>
    <a href="<%= request.getContextPath() %>/feed/<%= fmts[j] %>/<%= community.getHandle() %>"><img src="<%= request.getContextPath() %>/image/<%= icon %>" alt="RSS Feed" width="<%= width %>" height="32" style="margin: 3px 0 3px" /></a>
<%
    	}
    }
%> 
			</h3>		
	<%
		Item[] items = rs.getRecentSubmissions();
		boolean first = true;
		if(items!=null && items.length>0) 
		{ 
	%>	
		<!-- Wrapper for slides -->
		  <div class="carousel-inner">
	<%	for (int i = 0; i < items.length; i++)
		{
			Metadatum[] dcv = items[i].getMetadata("dc", "title", null, Item.ANY);
			
			String displayTitle = "Untitled";
			String displayAuthor = "Unknown";
			String displayDate = "Unknown";
			String displaySubject  = "";		
			String thumbnailImage = null;
			
			if (dcv != null)
			{
				if (dcv.length > 0)
				{
					displayTitle = Utils.addEntities(dcv[0].value);
					
				}
			}
			
			dcv = items[i].getMetadata("dc", "contributor", "author", Item.ANY);
			if( dcv != null)
			{
				if(dcv.length > 0)
				{
					displayAuthor = Utils.addEntities(dcv[0].value);
				}
			}
			
			dcv = items[i].getMetadata("dc", "date", "issued", Item.ANY);
			if( dcv != null)
			{
				if(dcv.length > 0)
				{
					displayDate = Utils.addEntities(dcv[0].value);
				}
			}			
			
			dcv = items[i].getMetadata("dc", "subject", null, Item.ANY);
			if( dcv != null)
			{
				for(Metadatum meta : dcv)
				{
					displaySubject += Utils.addEntities(meta.value) + "<br/>";
				}
			}
			
			try{
				Context c = UIUtil.obtainContext(request);
				Thumbnail thumbnail = ItemService.getThumbnail(c, items[i].getID(), false);
	            if (thumbnail != null)
	    		{
		        	StringBuffer thumbFrag = new StringBuffer();
		        	String link = request.getContextPath() + "/handle/" + items[i].getHandle();
		        	thumbFrag.append("<a href=\"" + link + "\" />");

		        	Bitstream thumb = thumbnail.getThumb();
		        	String img = request.getContextPath() + "/retrieve/" + thumb.getID() + "/" + UIUtil.encodeBitstreamName(thumb.getName(), Constants.DEFAULT_ENCODING);
		        	String alt = thumb.getName();			            
		            thumbFrag.append("<img src=\"")
		                    .append(img)
		                    .append("\" alt=\"").append(alt).append("\" ")			                     
		                    .append("/ border=\"0\"  class=\"img-responsive\" onerror=\"this.onerror=null;this.src='../../image/loginrequired_big.png';\" ></a>");
		        	thumbnailImage = thumbFrag.toString();
	    		} else {
	    			thumbnailImage = "<img src='../../image/nopreview.png' border='0' class='img-responsive'>";	    				
	    		}
			}catch(Exception e){}
			
			%>
		    <div style="padding-bottom: 50px; min-height: 200px;" class="item <%= first?"active":""%>">
		    	<div class="row">
		    		<div class="col-md-6 col-sm-12 text-center">
					    <% if(thumbnailImage != null) { %>
							<%=thumbnailImage %>		    
					    <% } %>		
		    		</div>
		    		<div class="col-md-6 col-sm-12">	
		    			<table class="table itemDisplayTable">
		    				<tbody>
		    					<tr>
		    						<td class="metadataFieldLabel">Date:</td>		    						
		    						<td class="metadataFieldValue"><%=displayDate%></td>
		    					</tr>
		    					<tr>
		    						<td class="metadataFieldLabel">Title:</td>		    						
		    						<td class="metadataFieldValue"><%=StringUtils.abbreviate(displayTitle, 100)%></td>
		    					</tr>		    				
		    					<tr>
		    						<td class="metadataFieldLabel">Author:</td>		    						
		    						<td class="metadataFieldValue"><%=displayAuthor%></td>
		    					</tr>
		    					
		    					<tr>
		    						<td class="metadataFieldLabel">Keywords:</td>		    						
		    						<td class="metadataFieldValue"><%=displaySubject%></td>
		    					</tr>
		    				</tbody>
		    			</table>				      	
		    		</div>
		    		<div class="col-sm-12 col-md-6">
		    			<a href="<%= request.getContextPath() %>/handle/<%=items[i].getHandle() %>" class="btn btn-success btn-block">View</a>
		    		</div>		    
		    	</div>
		      
		    </div>
<%
				first = false;
		     }
		%>
		</div>
		
		  <!-- Controls -->
		  <a class="left carousel-control" href="#recent-submissions-carousel" data-slide="prev">
		    <span class="icon-prev"></span>
		  </a>
		  <a class="right carousel-control" href="#recent-submissions-carousel" data-slide="next">
		    <span class="icon-next"></span>
		  </a>

          <ol class="carousel-indicators">
		    <li data-target="#recent-submissions-carousel" data-slide-to="0" class="active"></li>
		    <% for (int i = 1; i < rs.count(); i++){ %>
		    <li data-target="#recent-submissions-carousel" data-slide-to="<%= i %>"></li>
		    <% } %>
	      </ol>
		
		<%
		}
		%>
		  
     </div></div></div>
<%
	}
%>


	
	<div class="panel col-md-12 text-right">	
  	Share	 	  	
		<span class='st_twitter_large'></span>
		<span class='st_linkedin_large'></span>
		<span class='st_facebook_large'></span>
		<span class='st_email_large'></span>
		<span class='st_sharethis_large'></span>		
 	</div>
</div>
<%	} %>



    <% if(editor_button || add_button)  // edit button(s)
    { %>
    <dspace:sidebar>
		 <div class="panel panel-warning">
             <div class="panel-heading">
             	<fmt:message key="jsp.admintools"/>
             	<span class="pull-right">
             		<dspace:popup page="<%= LocaleSupport.getLocalizedMessage(pageContext, \"help.site-admin\")%>"><fmt:message key="jsp.adminhelp"/></dspace:popup>
             	</span>
             	</div>
             <div class="panel-body">
             <% if(editor_button) { %>
	            <form method="post" action="<%=request.getContextPath()%>/tools/edit-communities">
		          <input type="hidden" name="community_id" value="<%= community.getID() %>" />
		          <input type="hidden" name="action" value="<%=EditCommunitiesServlet.START_EDIT_COMMUNITY%>" />
                  <%--<input type="submit" value="Edit..." />--%>
                  <input class="btn btn-default col-md-12" type="submit" value="<fmt:message key="jsp.general.edit.button"/>" />
                </form>
             <% } %>
             <% if(add_button) { %>

				<form method="post" action="<%=request.getContextPath()%>/tools/collection-wizard">
		     		<input type="hidden" name="community_id" value="<%= community.getID() %>" />
                    <input class="btn btn-default col-md-12" type="submit" value="<fmt:message key="jsp.community-home.create1.button"/>" />
                </form>
                
                <form method="post" action="<%=request.getContextPath()%>/tools/edit-communities">
                    <input type="hidden" name="action" value="<%= EditCommunitiesServlet.START_CREATE_COMMUNITY%>" />
                    <input type="hidden" name="parent_community_id" value="<%= community.getID() %>" />
                    <%--<input type="submit" name="submit" value="Create Sub-community" />--%>
                    <input class="btn btn-default col-md-12" type="submit" name="submit" value="<fmt:message key="jsp.community-home.create2.button"/>" />
                 </form>
             <% } %>
            <% if( editor_button ) { %>
                <form method="post" action="<%=request.getContextPath()%>/mydspace">
                  <input type="hidden" name="community_id" value="<%= community.getID() %>" />
                  <input type="hidden" name="step" value="<%= MyDSpaceServlet.REQUEST_EXPORT_ARCHIVE %>" />
                  <input class="btn btn-default col-md-12" type="submit" value="<fmt:message key="jsp.mydspace.request.export.community"/>" />
                </form>
              <form method="post" action="<%=request.getContextPath()%>/mydspace">
                <input type="hidden" name="community_id" value="<%= community.getID() %>" />
                <input type="hidden" name="step" value="<%= MyDSpaceServlet.REQUEST_MIGRATE_ARCHIVE %>" />
                <input class="btn btn-default col-md-12" type="submit" value="<fmt:message key="jsp.mydspace.request.export.migratecommunity"/>" />
              </form>
               <form method="post" action="<%=request.getContextPath()%>/dspace-admin/metadataexport">
                 <input type="hidden" name="handle" value="<%= community.getHandle() %>" />
                 <input class="btn btn-default col-md-12" type="submit" value="<fmt:message key="jsp.general.metadataexport.button"/>" />
               </form>
			<% } %>
			</div>
		</div>
  </dspace:sidebar>
    <% } %>
</dspace:layout>
