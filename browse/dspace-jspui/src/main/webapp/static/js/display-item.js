Jmol._tracker = false; 

var jmolApplet = null;
var jmolInfo = null;
var maxJsmolAppletSize = 560;
var placeholderDivName = "#jsmolPlaceholderDiv";
var placeholderMoldenDivName = "#moldenIframeDiv";
var moldenIframeSrc = null;


$(function() {
    $('ul.nav-tabs > li > a').on('shown.bs.tab', function(){
        loadGeometry();
   }); 
   loadGeometry();
});

function getConstrainedWidth(){
    if(jQuery(window).width() > maxJsmolAppletSize){
    	jQuery(placeholderDivName).removeClass('jsmol-border');
    	jQuery(placeholderMoldenDivName).removeClass('jsmol-border');
        return maxJsmolAppletSize;
    }else{
    	jQuery(placeholderDivName).addClass('jsmol-border');
    	jQuery(placeholderMoldenDivName).addClass('jsmol-border');
        return jQuery(window).width() - 35;
        
    }
}   

function getConstrainedHeight(){
    if(jQuery(window).height() > maxJsmolAppletSize){
        return maxJsmolAppletSize;
    }else{
        return jQuery(window).width();
    }
}

function loadGeometry(){
	j2sPath = "../../xslt/jsmol/j2s";
	jsmolScript = 'vibration off; vectors off;sync on;' + $('ul.nav-tabs > li.active > a.geometryTab').attr('url')+ ';centerAt average;';

    jmolInfo = {
            	width: getConstrainedWidth(),
             	height: getConstrainedHeight(),
             	debug: false,
                color: "0xFFFFFF",
             	addSelectionOptions: false,
             	use: "HTML5",
     			j2sPath: j2sPath,
             	readyFunction: null,
     			disableJ2SLoadMonitor: true,
     			disableInitialConsole: true,
                script: jsmolScript,        
               };  
               
    jQuery(placeholderDivName).html(Jmol.getAppletHtml("jmolApplet", jmolInfo));                                
}

function loadMolden(){
	jQuery("#orbitalFrame").remove();		//Remove previous iframe
	var iframe = document.createElement('iframe');
	iframe.id = "orbitalFrame";
	if(!iframe.classList.contains('moldenIframe')){
		iframe.classList.add('moldenIframe');
	}
	iframe.frameborder= "0";	
	iframe.scrolling = "no";
	iframe.width = getConstrainedWidth();
	iframe.height = getConstrainedHeight();	
	jQuery("#moldenIframeDiv").append(iframe);
}

/* Resizing current window will force to reload jsmol with new dimensions */
var resizeTimer;
jQuery(window).on('resize', function(e) {                            
    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
        loadGeometry();
        loadMolden();
    }, 250);                            
});


function itemViewDisplayXYZ(){
	var geometry = Jmol.scriptEcho(jmolApplet, "select {*/1};  write XYZ");
	jQuery("#modalDisplayItemTextArea").val(geometry);
	jQuery("#modalDisplayItem").modal({backdrop: 'static', keyboard: false});
}


function showAuthoreaCodeSnippet() {
	retrieveXYZ();		
	$('#authoreaCodeSnippet').modal({backdrop: 'static', keyboard: false});
}

function showHtmlCodeSnippet() {
	retrieveXYZ();
	$('#htmlCodeSnippet').modal({backdrop: 'static', keyboard: false});
}

function retrieveXYZ(url){
	if(xyz === null){
		$.ajax({
		    url: url
		}).done(function(data){
			xyz = data.replace(/\n/g, '\\n').replace(/[\'\"]/g, '_');
			$('#htmlCode').html($('#htmlCode').html().replace(/XYZ_PLACEHOLDER/g, xyz));
			$('#authoreaCode').html($('#authoreaCode').html().replace(/XYZ_PLACEHOLDER/g, xyz));
		});
	}
}

