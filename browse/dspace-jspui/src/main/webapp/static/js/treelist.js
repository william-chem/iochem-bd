var mode='tree';	//Modes are: tree and full

var defaultOpenedNodeIndex = 0;
$.fn.extend({
    treed: function (o) {

      var openedClass = 'fa-chevron-down';
      var closedClass = 'fa-chevron-right';
      
      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };
      
      //initialize each of the top levels      
      var tree = $(this);
      tree.addClass("tree");
      tree.find('li').has("ul").each(function () {
    	  var branch = $(this); //li with children ul
          branch.prepend("<i class='indicator fas " + openedClass + "'></i>");
          branch.addClass('branch');
          branch.on('click', function (e) {
        	  if (this == e.target) {
        		  var icon = $(this).children('i:first');
                  icon.toggleClass(openedClass + " " + closedClass);
                  $(this).children().children().toggle();
              }
          })
          //branch.children().children().toggle();
      });
      
      //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
      //fire event to open branch if the li contains an anchor instead of text
      tree.find('.branch>a').each(function () {
    	  $(this).on('click', function (e) {
    		  $(this).closest('li').click();
              e.preventDefault();
          });
      });
      //fire event to open branch if the li contains a button instead of text
      tree.find('.branch>button').each(function () {
    	  $(this).on('click', function (e) {
    		  $(this).closest('li').click();
              e.preventDefault();
          });
      });
    }
});

function changeDisplay(treeId, tableId, newMode) {
	mode = newMode;
	clearTableFilters(tableId);	
	if(mode == 'tree'){
		$(".container").removeClass('container').addClass('container-fluid');
		$("#detailDiv").removeClass('col-md-12').addClass('col-md-9');
		$("#masterDiv").show();	
		$("[name=viewModeOptions]").val(["tree"]);
		
	}else{
		$(".container-fluid").removeClass('container-fluid').addClass('container');
		$("#detailDiv").removeClass('col-md-9').addClass('col-md-12');
		$("#masterDiv").hide();
		$("[name=viewModeOptions]").val(["list"]);
		displayNodeCalculations(tableId, 0);
	}			
	stickTableToTop();
}

function clearTableFilters(tableId) {
	$('#' + tableId + '_filter input[type=search]').val('')
	$('#' + tableId + '_filter input[type=search]').keyup()
}

function displayNodeCalculations(tableId, index){
	var filteredRows;
	clearTableFilters(tableId);	
	if(mode == 'tree'){
		filteredRows=calculationDetails.filter(function(item){
			return item[0]==index;             
		});	
	}else if(mode =='full'){ 
		filteredRows=calculationDetails;
	}
	table = $('#' + tableId).DataTable();
	table.clear(); 
	if(filteredRows.length > 0)
		table.rows.add(filteredRows);
	table.draw();
	table.columns.adjust().draw();
}

function initTree(treeId, tableId, columns, columnDefs, defaultNodeIndex){
	defaultOpenedNodeIndex = defaultNodeIndex;	
	//Build tree
	$('#' + treeId).treed();
	//Attach action only to occupied nodes
	$('.node').bind('click', function(event) {
		event.stopPropagation();		
		index = 'node' + $(this).attr('index');		
		if(nodeOccupation[index] > 0){
			$('.nodeSelected').removeClass('nodeSelected'); 
			$(this).find('span').first().toggleClass('nodeSelected');  		
			displayNodeCalculations(tableId, parseInt($(this).attr('index')));	
		}			
	});	
	//Set occupation numbers
	$.each($('.node'), function(i, val){
		index = 'node' + $(val).attr('index');
		if(nodeOccupation[index] > 0){
			$(val).children('span').append("<small style='margin-left:5px'>(" + nodeOccupation[index] + ")</small>");	
		}else {
			$(val).addClass("emptyNode");
		}
	});	
	
	//Move view selector to parent 
	$('.viewModeSelector').prependTo('.mainDiv');
	//Build table with the passed column definitions and its render functions
	$('#' + tableId).dataTable({
		"columns": columns,         
		"columnDefs" : columnDefs,
		scrollY:        '50vh',				
		scrollX: 		true,
		deferRender:    true,
		scroller:       true,
		scroller: {
	        rowHeight: 80
	    },
		searching: 		true,		
		autoWidth: 		true,
		fixedHeader:    true,
	        responsive:     !isLargeView()	  
	}); 
	$('.viewModeSelector').prependTo('#detailTable_wrapper')	

	stickTableToTop();
}

function isLargeView() {
	if($(window).width() > 1200) 
		return true;
	else 
		return false;
}

function stickTableToTop(){
	if(isLargeView() && $('#viewMode1').prop('checked'))
		$('.detailTableAffix').sticky({topSpacing:60});
	else
		$('.detailTableAffix').unstick();
}

function expandTreeRoot(treeId){
	$('#' + treeId +'.tree .node.branch .indicator')[0].closest('li').click()	
}

function resetTree(treeId, tableId) {
	changeDisplay(treeId, tableId, 'tree')
	displayNodeCalculations(tableId, defaultOpenedNodeIndex);
	$('.nodeSelected').removeClass('nodeSelected');
	$('.node[index=' + defaultOpenedNodeIndex + ']').find('span').first().toggleClass('nodeSelected');	
}

function setFullModeOnly(treeId, tableId) {
	changeDisplay(treeId, tableId, 'full')
	$('#viewModeSelectorDiv').hide();
}
