<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:

    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>
<%--
  - Password changed message
  --%>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"
    prefix="fmt" %>

<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<dspace:layout titlekey="jsp.register.password-changed.title">

    <%-- <h1>Password Changed</h1> --%>
	<h1><fmt:message key="jsp.register.password-changed.title"/></h1>

    <%-- <p>Thank you, your new password has been set and is active immediately.</p> --%>
	<p><fmt:message key="jsp.register.password-changed.info"/></p>
	
	<%-- <p>Please download again your shell client or update its password with this command :   $ . start-rep-shell -t update_password </p> --%>	
	<p><fmt:message key="jsp.register.password-changed.shell"/></p>

    <%-- <p><a href="<%= request.getContextPath() %>/">Go to DSpace Home</a></p> --%>
	<p><a href="<%= request.getContextPath() %>/"><fmt:message key="jsp.register.password-changed.link"/></a></p>

</dspace:layout>
