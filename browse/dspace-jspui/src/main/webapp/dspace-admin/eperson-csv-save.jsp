<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>
<%@ page import="java.util.Locale"%>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>
<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.eperson.EPerson" %>
<%@ page import="org.dspace.eperson.Group" %>
<%@ page import="org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.core.Utils" %>
<%@ page import="org.dspace.core.Context"%>
<%@ page import="org.dspace.app.webui.util.UIUtil"%>
<%@ page import="org.dspace.app.webui.util.EpersonCSV" %>
<%@ page import="org.dspace.eperson.AccountManager" %>
<%@ page import="java.util.Scanner" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.HashMap" %>

<%!
public static boolean areFieldsValid(String[] fields){
	if(fields.length != 4)
		return false;
  	String email = fields[0].trim();
  	String firstName = fields[1].trim();
  	String lastName = fields[2].trim();
  	String groupName = fields[3].trim();
	if(email.length() == 0 && firstName.length() == 0 &&  lastName.length() == 0 && groupName.length() == 0 )
		return false;
	if(!email.matches("[\\w._%-]+@[\\w._%-]+\\.[\\w]{2,4}"))
		return false;
	if(groupName.toLowerCase().equals("administrator") || groupName.toLowerCase().equals("anonymous")) 		//This groups can't be assigned
		return false;
	return true;
}

public static boolean existsEmail(Context context, String email) throws Exception{
	return EPerson.findByEmail(context, email) != null;
}
%>

<%
	ArrayList<String> errorLines = new ArrayList<String>();
	ArrayList<Integer> newEPersonIds = new ArrayList<Integer>();
	HashMap<Integer, Group> userGroups = new HashMap<Integer, Group>();
	
	Context context = UIUtil.obtainContext(request);
	boolean generateCommunities = request.getAttribute("generate_communities") != null && ((String)request.getAttribute("generate_communities")).equals("on");
	boolean sendResetPasswordEmails = request.getAttribute("send_reset_emails") != null && ((String)request.getAttribute("send_reset_emails")).equals("on");
	
	Scanner scanner = new Scanner((String) request.getAttribute("eperson_csv"));
	//User generation
	while (scanner.hasNextLine()) {
		String line = scanner.nextLine();
	  	String fields[] = line.split(";");	
	  	if(!areFieldsValid(fields)){
			errorLines.add(line + " Reason: Wrong parameters");	  		
	  		continue;
	  	}		 	  	
	  	try{
		  	String email = fields[0].trim();
		  	String firstName = fields[1].trim();
		  	String lastName = fields[2].trim();
		  	String groupName = fields[3].trim();
		  	if(existsEmail(context, email)){
		  		errorLines.add(line + " Reason: User already exists");
		  		continue;
		  	}
		  	String username = EpersonCSV.buildUsername(context, firstName, lastName);	
		  	Group maingroup = EpersonCSV.createGroup(context, email, groupName);
		  	if(!userGroups.containsKey(maingroup.getID()))
		  		userGroups.put(maingroup.getID(), maingroup);	
		    //Check required fields, possible duplicates eperson email
	      	EPerson e = EPerson.create(context);
	      	e.setEmail(email);
	      	e.setFirstName(firstName);
	      	e.setLastName(lastName);
	      	e.setNetid(email);
	      	e.setCanLogIn(true);
	      	e.setLanguage("en");
	      	e.setRequireCertificate(false);
	      	e.setUsername(username);
	      	e.setMainGroupId(String.valueOf(maingroup.getID()));	      	
	      	maingroup.addMember(e);	
	      	maingroup.update();
	      	e.update();	
	      	newEPersonIds.add(e.getID());
	  	}catch(Exception e){
	  		String errorMessage = e.getMessage();
	  		errorLines.add(line + " Exception raised: " + errorMessage);
	  		continue;
	  	}
	}
	scanner.close();
	//Create publication endpoints
	if(generateCommunities)
		for(Group group: userGroups.values())
			EpersonCSV.createCommunityFromUserGroup(context, group);
	//Send reset password emails to users
	if(sendResetPasswordEmails)
		for(Integer id : newEPersonIds){
			EPerson eperson = EPerson.find(context, id);
			try{
			 	AccountManager.sendForgotPasswordInfo(context, eperson.getEmail());
			}catch(Exception e){}			
		}					
	context.complete();	
%>
<dspace:layout style="submission" titlekey="jsp.dspace-admin.eperson-main.addbulk.title"
               navbar="admin"
               locbar="link"
               parenttitlekey="jsp.administer"
               parentlink="/dspace-admin"
               nocache="true">
               
               <div class="row">
               	<label class="col-md-2">Error line resume:</label>
               	<textarea rows="30" class="col-md-10">
<%
for(String errorLine : errorLines){
		out.write("Line: " + errorLine + "&#10;");	
}
%>  	
               	</textarea>
               </div>
               
</dspace:layout>

