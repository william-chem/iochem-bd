<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.

  This file incorporates work covered by the following copyright and
  permission notice:


    The contents of this file are subject to the license and copyright
    detailed in the LICENSE and NOTICE files at the root of the source
    tree and available online at

    http://www.dspace.org/license/

--%>

<%--
  - eperson editor - for new or existing epeople
  -
  - Attributes:
  -   eperson - eperson to be edited
  -   email_exists - if non-null, user has attempted to enter a duplicate email
  -                  address, so an error should be displayed
  -   username_exists - if non-null, user has attempted to enter a duplicate Create username,
  -                  	so an error should be displayed
  -
  - Returns:
  -   submit_save   - admin wants to save edits
  -   submit_delete - admin wants to delete edits
  -   submit_cancel - admin wants to cancel
  -
  -   eperson_id
  -   email
  -   firstname
  -   lastname
  -   phone
  -   language
  -   can_log_in          - (boolean)
  -   require_certificate - (boolean)
  -   main group		  - user main group chosen from the ones he belong, used inside CREATE
  -	  username			  - username path (used to generate paths inside CREATE system, follow some validation rules)   
  
  --%>

<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"
    prefix="fmt" %>


<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="java.util.Locale"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>

<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.eperson.EPerson, org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.eperson.Group"   %>
<%@ page import="org.dspace.content.Community" %>
<%@ page import="org.dspace.core.Utils" %>
<%@ page import="org.dspace.core.Context"%>
<%@ page import="org.dspace.app.webui.util.UIUtil"%>


<%
	Context context = UIUtil.obtainContext(request);
    EPerson eperson = (EPerson) request.getAttribute("eperson");
    boolean isNewEperson = (Boolean) request.getAttribute("is.new.eperson") == null ? false :(Boolean) request.getAttribute("is.new.eperson");
	
    Group[] groups = null;    
	Group [] groupMemberships = null;
	ArrayList<Integer> groupMembershipIds = null;
	
	if(request.getAttribute("group.memberships") != null) {
		groupMemberships = (Group []) request.getAttribute("group.memberships");
		
		if(groupMemberships != null){
			groupMembershipIds = new ArrayList<Integer>();
			for(Group group : groupMemberships) {
				groupMembershipIds.add(group.getID());
			}
		}
	}
	
	Community[] communities = null;
	Community[] publishCommunities = null;
	ArrayList<Integer> communityIds = null;
	if(request.getAttribute("publish.communities") != null) {
		publishCommunities = (Community[]) request.getAttribute("publish.communities");
		
		if(publishCommunities != null){
			communityIds = new ArrayList<Integer>();
			for(Community community: publishCommunities)
				communityIds.add(community.getID());
		}
		
	}
	

    String email     = eperson.getEmail();
    String firstName = eperson.getFirstName();
    String lastName  = eperson.getLastName();
    String phone     = eperson.getMetadata("phone");
    String netid 	 = eperson.getNetid();
    String language  = eperson.getMetadata("language");
    String username	 = eperson.getUsername();
    int mainGroupId	 = eperson.getMainGroupId() == null || eperson.getMainGroupId().equals("") ? 0 : Integer.valueOf(eperson.getMainGroupId());
  
    boolean emailExists = (request.getAttribute("email_exists") != null);
    boolean usernameExists	= (request.getAttribute("username_exists") != null);
    boolean isReviewer = eperson.getEmail().toUpperCase().startsWith("REVIEWER");
    boolean ldap_enabled = ConfigurationManager.getBooleanProperty("authentication-ldap", "enable");
    boolean cas_enabled  = ConfigurationManager.getBooleanProperty("authentication-cas" , "webui.cas.enable");
%>

<dspace:layout style="submission" titlekey="jsp.dspace-admin.eperson-edit.title"
               navbar="admin"
               locbar="link"
               parenttitlekey="jsp.administer"
               parentlink="/dspace-admin"
               nocache="true">



        <%-- <h1>Edit EPerson <%= eperson.getEmail() %>:</h1> --%>
        <h1><fmt:message key="jsp.dspace-admin.eperson-edit.heading">
            	<fmt:param><%= Utils.addEntities(eperson.getEmail()) %></fmt:param>
        	</fmt:message>        
        </h1>

<% if (emailExists)
	{ %><div class="alert alert-danger" role="alert">
	     <fmt:message key="jsp.dspace-admin.eperson-edit.emailexists"/>
	   </div>
<%  } 
   else if(usernameExists)
{ %><div class="alert alert-danger" role="alert">
     <fmt:message key="jsp.dspace-admin.eperson-edit.usernameexists"/>
   </div>

<%  } %>
<% if(isReviewer){ %>
	<script type="text/javascript">
		$(function(){
			$("form :input").attr("disabled", true);
			$("form :input[name='submit_delete']").attr("disabled", false);
		});	
	</script>
<%	} %>




<script type="text/javascript">
   isNewEperson = <%=isNewEperson%>;
   $(document).ready( function() {
   	
   	$("#delete_btn").click(function(e) {
   		$("#temail").removeAttr("required");
   		$("#tlastname").removeAttr("required");
   		$("#tfirstname").removeAttr("required");
   		$("#susergroups").removeAttr("required");
 		$("#tmaingroup").removeAttr("required");
 		$("#submit_delete").click();
   	});
   	
   
   	
    $('#userForm').submit(function() {
    	$("#susergroups option").each(function(){
    		$(this).prop('selected', true);
    	});
    	
    	$("#susercommunities option").each(function(){
    		$(this).prop('selected', true);
    	});
    	
  	});
    
    var firstNameKeyUp = debounce(function(){
		searchUsername()	
    }, 600);
    
    $("#tfirstname").keyup(firstNameKeyUp);
    $("#tlastname").keyup(firstNameKeyUp);
       
    
   });
   
    function debounce(func, wait, immediate) {
		var timeout;
		return function() {
			var context = this, args = arguments;
			var later = function() {
				timeout = null;
				if (!immediate) func.apply(context, args);
			};
			var callNow = immediate && !timeout;
			clearTimeout(timeout);
			timeout = setTimeout(later, wait);
			if (callNow) func.apply(context, args);
		};
	};
   
   function searchUsername() {
	  if((isNewEperson || isEmpty("tusername")) && !isEmpty("tlastname") && !isEmpty("tfirstname")) {
		  var formData = $($('form')[0]).serialize() + "&submit_request_username=request_username";
	      $.ajax({
	        url: './edit-epeople',
	        method: 'POST',
	        data: formData
	      }).done(function(data) {
			$("#tusername").val(data);
	      });
	  }
   }
   
   function isEmpty(textboxId) {
	   textbox = $("#" + textboxId);	   
	   return  (textbox.val()== null) ||(textbox.val() == '');
   }
   
   function removeGroup() {
       $("#susergroups option:selected").each(function() {
           $(this).remove()
           $("#sgroups").append($(this));                   
       });
       $("#sgroups").val([]);        
       $("#susergroups").val([]);
       updateDefaultGroups();
  }
  
  function addGroup() {
       $("#sgroups option:selected").each(function() {
           $(this).remove()
           $("#susergroups").append($(this));
       });        
       $("#sgroups").val([]);        
       $("#susergroups").val([]);
       updateDefaultGroups();
  }
 
  function updateDefaultGroups() {
       lastValue = $('#tmaingroup').val();
       $('#tmaingroup option').each(function() {
           $(this).remove();
       });
       
       $("#susergroups option").each(function() {            
           $("#tmaingroup").append($(this).clone());                   
       });
       $('#tmaingroup').val(lastValue);
       
       if( $('#tmaingroup').val() == null && $('#tmaingroup option').length == 1 ) {
           $('#tmaingroup').val($('#tmaingroup option').val());
       }
  }

  function removeCommunity() {
      $("#susercommunities option:selected").each(function() {
          $(this).remove()
          $("#scommunities").append($(this));                   
      });
      $("#scommunities").val([]);        
      $("#susercommunities").val([]);      
 }

 function addCommunity() {
      $("#scommunities option:selected").each(function() {
          $(this).remove()
          $("#susercommunities").append($(this));
      });        
      $("#scommunities").val([]);        
      $("#susercommunities").val([]);
 }
</script>


    <form id="userForm" method="post" action="">
   		<div class="row">
            <label class="col-md-2" for="tcan_log_in"><fmt:message key="jsp.dspace-admin.eperson-edit.can"/></label>
            <div class="col-md-6">
            	<label class="switch">
					<input type="checkbox" name="can_log_in" id="tcan_log_in" value="true"<%= eperson.canLogIn() ? " checked=\"checked\"" : "" %> ></input>
					<span class="slider"></span>					
				</label>
			</div>
        </div>
		<div class="row">
            <%-- <td>Email:</td> --%>         
            <label class="col-md-2" for="temail"><fmt:message key="jsp.dspace-admin.eperson-edit.email"/><span style="color:#db3e3e">*</span></label>
            <div class="col-md-6">
            	<input type="hidden" name="eperson_id" value="<%=eperson.getID()%>"/>
            	<input class="form-control" name="email" id="temail" size="24" value="<%=email == null ? "" : Utils.addEntities(email) %>" required/>
            </div>
        </div>

        <div class="row">
            <%-- <td>Last Name:</td> --%>
            <label class="col-md-2" for="tlastname"><fmt:message key="jsp.dspace-admin.eperson.general.lastname"/><span style="color:#db3e3e">*</span></label>
            <div class="col-md-6">
				<input class="form-control" name="lastname" id="tlastname" size="24" value="<%=lastName == null ? "" : Utils.addEntities(lastName) %>" required/>
			</div>
       </div>     

        <div class="row">           
            <%-- <td>First Name:</td> --%>
            <label class="col-md-2" for="tfirstname"><fmt:message key="jsp.dspace-admin.eperson.general.firstname"/><span style="color:#db3e3e">*</span></label>
            <div class="col-md-6">
                <input class="form-control" name="firstname" id="tfirstname" size="24" value="<%=firstName == null ? "" : Utils.addEntities(firstName) %>" required/>
            </div>
         </div>

		<div class="row">
		    <%-- <td>Username:</td> --%>
			<label class="col-md-2" for="tusername"><fmt:message key="jsp.dspace-admin.eperson.general.username"/></label>
		    <div class="col-md-6">
		    	<input class="form-control" name="username" id="tusername" size="24" value="<%=username == null? "": Utils.addEntities(username) %>" readonly/>
		   	</div>
		</div>		


        <% if (ldap_enabled || cas_enabled) { %>
		<div class="row" style="display:none">
            <label class="col-md-2">LDAP /CAS NetID:</label>
            <div class="col-md-6">
                <input class="form-control" name="netid" size="24" value="<%=netid == null ? email : Utils.addEntities(netid) %>" />
            </div>
        </div>
        <% } %>

		
		<div class="row">
            <label class="col-md-2" style="margin-top: 10px">Password:</label>
            <div class="col-md-6">
            	<div class="row">
            		<div class="col-md-12" style="margin-top: 10px">
            	    	<input type="radio" name="passwordAction" id="passwordActionRadio1" value="sendEmail" <% if(isNewEperson) { %> checked <% } %> >
				    	<fmt:message key="jsp.dspace-admin.eperson.general.password.send.email"/>	
            		</div>
            		<div class="col-md-12">
            			<div style="display:flex; flex-direction: row; flex-wrap: nowrap; align-items: center">
	            			<input type="radio" name="passwordAction" id="passwordActionRadio2" value="saveAutogenerated" onselect="generatePassword()" onclick="generatePassword()" style="margin-right:5px" >
	            			
	            			<fmt:message key="jsp.dspace-admin.eperson.general.password.autogenerated"/> 
				      		<input type="text" class="form-control" id="password" name="password" size="11" maxlength="11" autocomplete="off" style="margin-left: 10px; width: auto ">
				      		<button type="button" class="btn glyphicon glyphicon-refresh" onclick="generatePassword();" />
            			</div>
            		</div>            		
            		<div class="col-md-12" style="margin-bottom: 10px">
            	    	<input type="radio" name="passwordAction" id="passwordActionRadio3" value="noAction" <% if(!isNewEperson) { %> checked <% } %> >
            	    	<% if (isNewEperson) { %> 
            	    		<fmt:message key="jsp.dspace-admin.eperson.general.password.noaction"/>
            	    	 <% } else { %>
            	    	 	<fmt:message key="jsp.dspace-admin.eperson.general.password.keep"/>            	    	 	
            	    	 <% } %>				    		
            		</div>
       				<script type="text/javascript">
						function generatePassword() {							
							$('#password').val(Math.random().toString(36).replace('0.', ''));
						}
					</script>
            	</div>								            
            </div>
        </div>						
		
        <div class="row">            
            <label class="col-md-2" for="tphone"><fmt:message key="jsp.dspace-admin.eperson-edit.phone"/></label>
            <div class="col-md-6">
				<input class="form-control" name="phone" id="tphone" size="24" value="<%=phone == null ? "" : Utils.addEntities(phone) %>"/>
			</div>  
  		</div>
  		
  		<div class="row" style="display:none">          
            <label class="col-md-2" for="tlanguage"><fmt:message key="jsp.register.profile-form.language.field"/></label>
            <div class="col-md-6">            
       		<select class="form-control" name="language" id="tlanguage">
<%
		Locale[] supportedLocales = I18nUtil.getSupportedLocales();

        for (int i = supportedLocales.length-1; i >= 0; i--)
        {
        	String lang = supportedLocales[i].toString();
        	String selected = "";
        	
        	if (language == null || language.equals(""))
        	{ if(lang.equals(I18nUtil.getSupportedLocale(request.getLocale()).getLanguage()))
        		{
        			selected = "selected=\"selected\"";
        		}
        	}
        	else if (lang.equals(language))
        	{ selected = "selected=\"selected\"";}
%>
          	 <option <%= selected %>
                value="<%= lang %>"><%= supportedLocales[i].getDisplayName(I18nUtil.getSupportedLocale(request.getLocale())) %></option>
<%
        }
%>
        	</select>
        	</div>
   		</div>
        <div class="row" style="display:none">
        <%-- <td>Require Certificate:</td> --%>
            <label class="col-md-2" for="trequire_certificate"><fmt:message key="jsp.dspace-admin.eperson-edit.require"/></label>
            <div class="col-md-6">
			<input class="form-control"  type="checkbox" name="require_certificate" id="trequire_certificate" value="true"<%= eperson.getRequireCertificate() ? " checked=\"checked\"" : "" %> />
			</div>
		</div>

        <div class="row" style="padding-top:2em">        
            <label class="col-md-2"><fmt:message key="jsp.dspace-admin.eperson-edit.belongs-to-group"/><span style="color:#db3e3e">*</span></label>
            <div class="col-md-6">
            	<div class="row equal">
            		<div multiple class="col-md-5">
            			<p><fmt:message key="jsp.dspace-admin.eperson-edit.belongs-to-group.label"/></p>
            			<select multiple class="form-control" id="susergroups" name="susergroups" size="5">            			
							<%
								if(groupMemberships != null){
									for(Group group : groupMemberships){
										if(group.getName().startsWith("COLLECTION") || 
												group.getName().startsWith("COMMUNITY") || 
												group.getName().equals("Anonymous")  || 
												group.getName().equals("Administrator") ||
												group.getName().equals("Reviewers"))
							        		continue;
							%>
								<option value="<%=String.valueOf(group.getID())%>"><%=group.getName()%></option>
							<%      }
								}
							%>
            			</select>
            		</div>
            		<div class="col-md-2">
            			<div style="padding-top: 2em; height: 100%; display:flex; flex-direction:column; align-content: space-evenly;justify-content: space-evenly;">
            				<button type="button" class="btn glyphicon glyphicon-chevron-left" onclick="addGroup()"></span>
            				<button type="button" class="btn glyphicon glyphicon-chevron-right" onclick="removeGroup()"></span>
            			</div>
            		</div>
            		<div class="col-md-5">
            			<p><fmt:message key="jsp.dspace-admin.eperson-edit.available.groups.label"/></p>
            			<select multiple class="form-control" id="sgroups" name="sgroups" size="5">
           					<%
           						groups = Group.findAll(context, Group.NAME);
								for (int i = groups.length-1; i >= 0; i--) {
	        						String groupName 	= groups[i].getName();
	        						int groupId	 		= groups[i].getID();   	        						
						        	//Discard autogenerated collection and community groups  
						        	if(groupName.startsWith("COLLECTION") || 
						        			groupName.startsWith("COMMUNITY") || 
						        			groupName.equals("Anonymous")  || 
						        			groupName.equals("Administrator") ||
						        			groupName.equals("Reviewers"))
						        		continue;
						        	if(groupMembershipIds != null && groupMembershipIds.contains(groupId))
						        		continue;						        	
							%>
	          	 					<option value="<%= String.valueOf(groupId) %>"><%= groupName%></option>
	          	 			<%
	        					}
							%>
					    </select>
            		</div>	
            	</div>           
			</div>
		</div>
		
		<div class="row">
			<%-- <td>Main group</td> --%>
			<label class="col-md-2" for="tmaingroup"><fmt:message key="jsp.register.profile-form.maingroup.field"/><span style="color:#db3e3e">*</span></label>
			<div class="col-md-6">
				<select class="form-control" name="maingroup" id="tmaingroup" required>				
				<%
					if(groupMemberships != null){
						for(Group group : groupMemberships){
							if(group.getName().startsWith("COLLECTION") || 
									group.getName().startsWith("COMMUNITY") || 
									group.getName().equals("Anonymous")  || 
									group.getName().equals("Administrator") ||
									group.getName().equals("Reviewers"))
				        		continue;
							String selected = group.getID() == mainGroupId? "selected=\"selected\"" : "";
				%>
					<option <%= selected %> value="<%=String.valueOf(group.getID())%>"><%=group.getName()%></option>
				<%      }
					}
				%>	        
        	</select>			
			</div>
		</div>
		
		<div class="row" style="padding-top:2em">        
            <label class="col-md-2"><fmt:message key="jsp.dspace-admin.eperson-edit.publishing.communities"/></label>
            <div class="col-md-6">
            	<div class="row equal">
            		<div multiple class="col-md-5">
            			<p><fmt:message key="jsp.dspace-admin.eperson-edit.publishing.communities.label"/></p>
            			<select multiple class="form-control" id="susercommunities" name="susercommunities" size="5">            			
							<%	if(publishCommunities != null) {
									for(Community community : publishCommunities) {	
							%>
								<option value="<%=String.valueOf(community.getID())%>"><%=community.getName()%></option>	
							<% 		}
								}
							%>						
            			</select>
            		</div>
            		<div class="col-md-2">
            			<div style="padding-top: 2em; height: 100%; display:flex; flex-direction:column; align-content: space-evenly;justify-content: space-evenly;">
            				<button type="button" class="btn glyphicon glyphicon-chevron-left" onclick="addCommunity()"></span>
            				<button type="button" class="btn glyphicon glyphicon-chevron-right" onclick="removeCommunity()"></span>
            			</div>
            		</div>
            		<div class="col-md-5">
            			<p><fmt:message key="jsp.dspace-admin.eperson-edit.available.communities.label"/></p>
            			<select multiple class="form-control" id="scommunities" name="scommunities" size="5">
           					<%
           						communities = Community.findAll(context);
								for (int i = communities.length-1; i >= 0; i--) {
	        						String communityName 	= communities[i].getName();
	        						int communityId	 		= communities[i].getID();   	        						
	        						
						        	if(communityIds != null && communityIds.contains(communityId))
						        		continue;						        	
							%>
	          	 					<option value="<%= String.valueOf(communityId) %>"><%= communityName%></option>
	          	 			<%
	        					}
							%>
					    </select>
            		</div>	
            	</div>           
			</div>
		</div>	
		
		<div class="row" style="padding:2em;">
			<div class="col-md-8">
				<p class="text-right"><span style="color:#db3e3e">*</span>Required</p>
			</div>		
		</div>
		<br/>
    	<div class="col-md-4 btn-group">                    
                    <input class="btn btn-default" type="submit" name="submit_save" value="<fmt:message key="jsp.dspace-admin.general.save"/>" />
                    <% if (!isNewEperson && !emailExists && !usernameExists) { %>
                    	<input class="btn btn-default" style="display:none" type="submit" name="submit_resetpassword" value="<fmt:message key="jsp.dspace-admin.eperson-main.ResetPassword.submit"/>"/>
                    <% } %>                    
                    <input class="btn btn-danger" type="button" id="delete_btn" name="delete_btn" value="<fmt:message key="jsp.dspace-admin.general.delete"/>" />
                    <input class="btn btn-danger" type="submit" id="submit_delete" name="submit_delete" value="<fmt:message key="jsp.dspace-admin.general.delete"/>"  style="display:none"/>
         </div>
	
    </form>


<!--  
<%
  if((groupMemberships != null) && (groupMemberships.length>0))
  {
%>
	<br/>
	<br/>
	
	<h3><fmt:message key="jsp.dspace-admin.eperson-edit.groups"/></h3>
	
	<div class="row">    
    <ul>
	<%  for(int i=0; i<groupMemberships.length; i++)
     	{
        String myLink = groupMemberships[i].getName();
        String args   = "submit_edit&amp;group_id="+groupMemberships[i].getID();
        
        myLink = "<a href=\""
        +request.getContextPath()
        +"/tools/group-edit?"+args+"\">" + myLink + "</a>";
	%>
    	<li><%=myLink%></li>
	<%  } %>
    </ul>
    </div>
<% } %>  
-->


</dspace:layout>
