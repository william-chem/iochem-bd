<%--
  Browse module - Browse module inside the ioChem-BD software.
  Copyright © 2019 ioChem-BD (contact@iochem-bd.org)

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <http://www.gnu.org/licenses/>.
--%>
<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"
    prefix="fmt" %>


<%@ taglib uri="http://www.dspace.org/dspace-tags.tld" prefix="dspace" %>

<%@ page import="java.util.Locale"%>

<%@ page import="javax.servlet.jsp.jstl.fmt.LocaleSupport" %>

<%@ page import="org.dspace.core.I18nUtil" %>
<%@ page import="org.dspace.eperson.EPerson, org.dspace.core.ConfigurationManager" %>
<%@ page import="org.dspace.eperson.Group"   %>
<%@ page import="org.dspace.core.Utils" %>
<%@ page import="org.dspace.core.Context"%>
<%@ page import="org.dspace.app.webui.util.UIUtil"%>

<%
	Context context = UIUtil.obtainContext(request);
	Group[] groups = Group.findAll(context, Group.NAME);

%>



<dspace:layout style="submission" titlekey="jsp.dspace-admin.eperson-main.addbulk.title"
               navbar="admin"
               locbar="link"
               parenttitlekey="jsp.administer"
               parentlink="/dspace-admin"
               nocache="true">
<style type="text/css">
	.section{
	  margin-top: 30px;
	  margin-bottom: 30px
	}
</style>


	 <form method="post" action="">
			<div class="row section">
				<label class="col-md-2">Help</label>
				<div class="col-md-10">
					<fmt:message key="jsp.dspace-admin.eperson-main.addbulk.linkhelp"/>	
					<div class="row">
						<div class="col-md-2">Format</div>
						<div class="col-md-10"><fmt:message key="jsp.dspace-admin.eperson-main.addbulk.lineformat"/></div>
					</div>		
					<div class="row">
						<div class="col-md-2">Example</div>
						<div class="col-md-10"><fmt:message key="jsp.dspace-admin.eperson-main.addbulk.example"/></div>
					</div>		
					<br/>							
					<div class="row">
						<div class="col-md-2"><fmt:message key="jsp.dspace-admin.eperson-main.addbulk.groups"/></div>
						<div class="col-md-10">							
							<ul>
								<%	for(Group group : groups) {
										if(group.getName().startsWith("COLLECTION") || 
											group.getName().startsWith("COMMUNITY") || 
											group.getName().equals("Anonymous")  || 
											group.getName().equals("Administrator") ||
											group.getName().equals("Reviewers"))
						        			continue;
								%>
								<li><%=group.getName()%></li>
								<% } %>    
						  	</ul>													
						</div>
					</div>
				</div>				
			</div>
			<div class="row section">
	            <%-- <td>Email:</td> --%>         
	            <label class="col-md-2" for="temail"><fmt:message key="jsp.dspace-admin.eperson-main.addbulk.csvfield"/></label>	            
	            <div class="col-md-10">
	            	<textarea class="form-control" name="csvfields" rows="20"></textarea>            	
	            </div>
	        </div>
	         <div class="row section">
	        	<label class="col-md-2">Options</label>
	        	<div class="col-md-10">	        		
	        		<input type="checkbox" checked="checked" name="generateCommunities"/> Generate publishing communities for each user group and assign created users to it
	        		<br/>
					<input type="checkbox" checked="checked" name="sendResetPasswordEmails"/> Send reset password email to generated users 						        		  	        	
	        	</div>
	        </div>
	        <div class="row section">
	        	<label class="col-md-2"></label>
	        	<div class="col-md-10">
	        		<input class="btn btn-primary btn-block" type="submit" name="submit_csv_save" value="<fmt:message key="jsp.dspace-admin.general.save"/>" />	        	
	        	</div>
	        </div>
	</form>
</dspace:layout>
