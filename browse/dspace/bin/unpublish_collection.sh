#!/bin/bash

configFilePath=$PWD/../../create/resources.properties
handle=
queryResponse=
collectionUrl=

HOST=
PORT=
USER=
PASSWORD=
SSL=false
CREATE_DATABASE=iochemCreate

function checkValidPath(){
   if [ ! -f $configFilePath ]; then
        echo "Please run this script inside 'IOCHEM-BD'/browse/bin folder, being 'IOCHEM-BD' the base installation folder of ioChem-BD software"
        exit 1
    fi
}

function readHandle(){
   read  handle
   handle=`echo $handle |sed -e 's/ //g'` #Remove blank spaces
   isvalidregex=`echo $handle | grep -q -e '^[0-9]*/[0-9]*$'; echo $?`
   if [ $isvalidregex -ne 0 ]; then
        echo "Invalid provided handle identifier, format: 100/300"
        exit 0
   fi
   echo "Provided handle is:"$handle
}

function readParameter(){
  echo `grep "$1" $configFilePath | cut -d'=' -f 2- | sed -e 's/ //g'`
}

function readDbParameters(){
   HOST=`readParameter create.database.host`
   PORT=`readParameter create.database.port`
   USER=`readParameter create.database.user`
   PASSWORD=`readParameter create.database.pwd`
   if [[ $(readParameter database.extra.parameters) == *"ssl=true"* ]]; then SSL=true; fi

   collectionUrl=`readParameter jumbo.template.base.url | sed -e 's/\/create\/jumbo//g'`"/browse/handle/$handle"
}


function query(){
  export PGHOST=$HOST
  export PGPORT=$PORT
  export PGDATABASE=$1
  export PGUSER=$USER
  export PGPASSWORD=$PASSWORD
  if $SSL ; then export PGSSLMODE=require; fi

  queryReturn=`psql -t -c  "$2" | sed -e 's/ //g' ` 
  unset PGHOST
  unset PGPORT
  unset PGDATABASE
  unset PGUSER
  unset PGPASSWORD
  unset PGSSLMODE
}

checkNotEmpty(){
  if [ "$#" -ne 1 ] || [[ -z $1 ]]; then
    echo "Error setting variable value, exiting!"
    exit 1
  fi
}


clear
checkValidPath
echo "ioChem-BD - Script utility to unpublish projects and calculations"
echo "-----------------------------------------------------------------"
echo "It will remove requested collection and its child items from Browse module"
echo "And also set as unpublished all projects and calculations from Create module"
echo "Please provide the handle identifier of the published collection, its the last part of the collection url after 'https://.../handle/'"
echo 
echo -n "Handle (example: 100/300) :"
readHandle
readDbParameters
echo
echo
echo "- Setting Create project as not published"
query $CREATE_DATABASE "select path ||  '/' || name as fullpath from projects where handle = '$handle' order by fullpath asc limit 1"
projectPath=$queryReturn
checkNotEmpty $projectPath

echo "Fixing calculations"
query $CREATE_DATABASE "update calculations set published=false, handle='', published_name='', publication_time=null where path like '$projectPath/%'" #Calculations inside subprojects
query $CREATE_DATABASE "update calculations set published=false, handle='', published_name='', publication_time=null where path like '$projectPath'"   #Calculations inside project
echo "Fixing subprojects"
query $CREATE_DATABASE "update projects set published=false, handle='', published_name='', publication_time=null, state='created' where path || '/' || name like '$projectPath/%'"
echo "Fixing project"
query $CREATE_DATABASE "update projects set published=false, handle='', published_name='', publication_time=null, state='created' where path || '/' || name like '$projectPath'"
echo 
echo "Create project succesfully updated"
echo "Please now visit as administrator the Browse collection page at: $collectionUrl and delete it, if its not already removed"
echo "Fix ended"

