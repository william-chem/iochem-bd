/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.rest;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.dspace.content.Collection;
import org.dspace.content.Site;
import org.dspace.core.Constants;
import org.dspace.event.Event;
import org.dspace.handle.HandleManager;
import org.dspace.sync.SyncEventConsumer;
import org.dspace.twitter.TwitterEventConsumer;

/**
 * Class which provides CRUD methods over OAI synchronizable content.
 */
@Path("/oai")
public class OAIResource extends Resource{
		
    private static final SyncEventConsumer eventConsumer = new SyncEventConsumer();
    private static final TwitterEventConsumer teventConsumer = new TwitterEventConsumer();
	
    private static Logger log = Logger.getLogger(OAIResource.class);

    static{
    	try {
			eventConsumer.initialize();
			teventConsumer.initialize();
		} catch (Exception e) {
			log.error(e.getMessage());
		}
    }
    
    @POST
    @Path("/collections/{collection_handle}")
    @Consumes({ MediaType.APPLICATION_JSON})
    public Response addNewCollection(@PathParam("collection_handle") String handle, @Context HttpHeaders headers, @Context HttpServletRequest request) throws WebApplicationException
    {
    	if(handle != null){
    		handle = handle.replace("_", "/");
    		org.dspace.core.Context context = null;
    		try{
    			context =  new org.dspace.core.Context();    			
    			Collection  collection = (Collection)HandleManager.resolveToObject(context, handle);
                if (collection == null)
                {
                    context.abort();
                    log.warn("Collection(handle=" + handle + ") was not found!");
                    throw new WebApplicationException(Response.Status.NOT_FOUND);
                }
            	//Raise add collection to institution event
                Event addCollectionEvent = new Event(Event.ADD, Constants.SITE, Site.SITE_ID, Constants.COLLECTION, collection.getID(), null , collection.getIdentifiers(context));
                teventConsumer.consume(context, addCollectionEvent );
            	eventConsumer.consume(context, addCollectionEvent );
    		}catch (Exception e){
                processException("Someting went wrong while processing 'add new collection' request(handle=" + handle + ")! Exception. Message: "+ e.getMessage() + e,context);
                return Response.serverError().build();        
    		}finally{
                processFinally(context);
    		}
    	}
    	return Response.ok().build();
    }
    
    @PUT
    @Path("/collections/{collection_handle}")
    @Consumes({ MediaType.APPLICATION_JSON})
    public Response updateCollection(@PathParam("collection_handle") String handle, @Context HttpHeaders headers, @Context HttpServletRequest request) throws WebApplicationException
    {
    	if(handle != null){
    		handle = handle.replace("_", "/");
    		org.dspace.core.Context context = null;
    		try{
    			context =  new org.dspace.core.Context();    			
    			Collection  collection = (Collection)HandleManager.resolveToObject(context, handle);
                if (collection == null)
                {
                    context.abort();
                    log.warn("Collection(handle=" + handle + ") was not found!");
                    throw new WebApplicationException(Response.Status.NOT_FOUND);
                }
            	//Raise add collection to institution event
                Event modifyCollectionEvent = new Event(Event.MODIFY, Constants.SITE, Site.SITE_ID, Constants.COLLECTION, collection.getID(), null , collection.getIdentifiers(context));
                teventConsumer.consume(context, modifyCollectionEvent);
            	eventConsumer.consume(context, modifyCollectionEvent);
    		}catch (Exception e){
                processException("Someting went wrong while processing 'append new content to collection' request(handle=" + handle + ")! Exception. Message: "+ e.getMessage() + e,context);
                return Response.serverError().build();        
    		}finally{
                processFinally(context);
    		}
    	}
    	return Response.ok().build();
    }

}
