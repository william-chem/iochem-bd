/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.rest.common;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import org.codehaus.jackson.annotate.JsonProperty;

@XmlRootElement(name = "administrator")
public class Administrator {
	private int userId;
	private List<Integer> communities = null;
	
	 public Administrator() {
		 userId = -1;
		 communities = new ArrayList<Integer>();
	 }

	 public Administrator(int userId) {
		 this.userId = userId;
		 communities = new ArrayList<Integer>();
	 }
	 
	 public Administrator(int userId, List<Integer> communities){
		 this.userId = userId;
		 this.communities = communities;		 		 
	 }

	 @JsonProperty("userid")
	 public int getUserId() {
		 return userId;
	 }
	 
	 @JsonProperty("userid")
	 public void setUserId(int userId) {
		 this.userId = userId;
	 }
	 
	 @JsonProperty("communities")
	 public List<Integer> getCommunities() {
		 return communities;
	 }

	 @JsonProperty("communities")
	 public void setCommunities(List<Integer> communities) {
		 this.communities = communities;
	 }	 
	 
}
