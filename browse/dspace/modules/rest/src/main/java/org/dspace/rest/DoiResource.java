/**
 * Browse module - Browse module inside the ioChem-BD software.
 * Copyright © 2014 ioChem-BD (contact@iochem-bd.org)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 * 
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package org.dspace.rest;

import java.sql.SQLException;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlRootElement;

import org.apache.log4j.Logger;
import org.dspace.authorize.AuthorizeException;
import org.dspace.content.Collection;
import org.dspace.content.Item;
import org.dspace.content.ItemIterator;
import org.dspace.content.Site;
import org.dspace.core.ConfigurationManager;
import org.dspace.core.Constants;
import org.dspace.event.Event;
import org.dspace.handle.HandleManager;
import org.dspace.identifier.DOI;
import org.dspace.identifier.DOIIdentifierProvider;
import org.dspace.identifier.IdentifierException;
import org.dspace.identifier.doi.CrossRefDOIConnector;
import org.dspace.servicemanager.DSpaceKernelImpl;
import org.dspace.servicemanager.DSpaceKernelInit;
import org.dspace.services.ConfigurationService;
import org.dspace.sync.SyncEventConsumer;
import org.dspace.utils.DSpace;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.sun.istack.NotNull;

@Path("/doi")
public class DoiResource extends Resource{
    private static Logger log = Logger.getLogger(DoiResource.class);
    public static final String RELATION_ELEMENT = "relation";
    public static final String ISPARTOF_QUALIFIER = "ispartof";
    private static final String DOI_PREFIX = "doi:";
       
    private static final SyncEventConsumer eventConsumer = new SyncEventConsumer();
    
    @Path("{token}")
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addBitstreamPolicy(@PathParam("token") String token, DoiRequest doiRequest){
    	log.info("Processing DOI for url" + doiRequest.getRequest().getUrl() + " with assigned DOI " + doiRequest.getRequest().getDoi());    	   
        org.dspace.core.Context context = null;
        try{
        	context = createContext(null);
        	if(!isRequestValid(token, doiRequest)){        		
        		processException("Someting went wrong while processing your doi request (doi=" + doiRequest.getRequest().getDoi() + ")! " ,context);
        		return Response.serverError().build();
        	}
        	else{
        		processDoiRequest(context, doiRequest);        		
        	}
        }catch (Exception e){
              processException("Someting went wrong while processing your doi request (doi=" + doiRequest.getRequest().getDoi() + ")! Exception. Message: "+ e.getMessage() + e,context);
              return Response.serverError().build();        
        }finally{
              processFinally(context);
        }    	
        return Response.ok().build(); 
    }
        
    private boolean isRequestValid(String token, DoiRequest doiRequest){  
    	String salt = new DSpace().getConfigurationService().getProperty("external.communication.secret");    	
    	
		StringBuilder tokenRequest = new StringBuilder();
		tokenRequest.append(doiRequest.getRequest().getTimestamp());
		tokenRequest.append("#");
		tokenRequest.append(doiRequest.getRequest().getDoi());
		tokenRequest.append("#");
		tokenRequest.append(doiRequest.getInstitution().getName());		
		String buildToken = TokenManager.encode(salt, tokenRequest.toString());    	
    	return token.equals(buildToken);
    }
    
    private void processDoiRequest(org.dspace.core.Context context, DoiRequest doiRequest) throws Exception{
    	String handlePrefix = new DSpace().getConfigurationService().getProperty("handle.canonical.prefix");    	
    	String handle = doiRequest.getRequest().getUrl().replace(handlePrefix, "");
    	Collection collection = (Collection)HandleManager.resolveToObject(context, handle);    	        
        ItemIterator items = collection.getItems();
        
        String doi = DOI_PREFIX + doiRequest.getRequest().getDoi();
        registerDoi(context, collection, doi);
        //appendIdentifierMetadata(context, collection, doi);        
		while(items.hasNext())
			appendIsPartOfMetadata(context, items.next(), doi);
		
     	eventConsumer.consume(context, new Event(Event.MODIFY_METADATA, Constants.SITE, Site.SITE_ID, Constants.COLLECTION, collection.getID(), doi , collection.getIdentifiers(context)));
        context.complete();
    }
    
    
    private void registerDoi(org.dspace.core.Context context, Collection collection, String doi) throws IdentifierException, IllegalArgumentException, SQLException{    	
    	 DSpaceKernelImpl kernelImpl = DSpaceKernelInit.getKernel(null);
         if (!kernelImpl.isRunning())
             kernelImpl.start(ConfigurationManager.getProperty("dspace.dir"));
         
         ConfigurationService config = kernelImpl.getConfigurationService();
         context.turnOffAuthorisationSystem();
         DOIIdentifierProvider provider = new DOIIdentifierProvider();
         CrossRefDOIConnector connector = new CrossRefDOIConnector();
         provider = new DOIIdentifierProvider();
         provider.setConfigurationService(config);
         provider.setDOIConnector(connector);
         provider.registerOnline(context, collection, doi);
         context.restoreAuthSystemState();
         context.commit();            	
    }
   
    private void appendIsPartOfMetadata(org.dspace.core.Context context, Item item, String doi) throws SQLException, IdentifierException, AuthorizeException {
        context.turnOffAuthorisationSystem();        
        item.addMetadata(DOIIdentifierProvider.MD_SCHEMA,
                DoiResource.RELATION_ELEMENT,
                DoiResource.ISPARTOF_QUALIFIER,
                null,
                DOI.DOIToExternalForm(doi));
        item.update();           
        context.restoreAuthSystemState();
        context.commit();        
    }    
}


@XmlRootElement(name = "doirequest")
class DoiRequest{
	@NotNull
	private Institution institution;
	@NotNull
	private Request request;
	
	public DoiRequest(){
		
	}
	
	public DoiRequest(@JsonProperty("institution") Institution institution, @JsonProperty("request")Request request){
		this.institution = institution;
		this.request = request;
	}	
	@JsonProperty
	public Institution getInstitution() {
		return institution;
	}
	@JsonProperty
	public void setInstitution(Institution institution) {
		this.institution = institution;
	}
	@JsonProperty
	public Request getRequest() {
		return request;
	}
	@JsonProperty
	public void setRequest(Request request) {
		this.request = request;
	}
}


@XmlRootElement(name = "institution")
class Institution {
	@NotNull
	private String name;
	@NotNull
	private String url;
	@NotNull
	private String acronym;
	
	public Institution(){
		
	}
	
	public Institution(@JsonProperty("name") String name, @JsonProperty("url") String url, @JsonProperty("acronym") String acronym){
		this.name = name;
		this.url = url;
		this.acronym = acronym;
	}
	
	@JsonProperty
	public String getName() {
		return name;
	}
	@JsonProperty
	public void setName(String name) {
		this.name = name;
	}
	@JsonProperty
	public String getUrl() {
		return url;
	}
	@JsonProperty
	public void setUrl(String url) {
		this.url = url;
	}
	@JsonProperty
	public String getAcronym() {
		return acronym;
	}
	@JsonProperty
	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}
}


@XmlRootElement(name = "request")
class Request {
	@NotNull
	private String type;
	@NotNull
	private String email;
	@NotNull
	private String url;
	@NotNull
	private String doi;
	@NotNull
	private String timestamp;
	
	public Request(){
		
	}
	
	public Request(@JsonProperty("type") String type, @JsonProperty("email") String email, @JsonProperty("url") String url,@JsonProperty("doi") String doi, @JsonProperty("timestamp") String timestamp){
		this.type = type;
		this.email = email;
		this.url = url;
		this.doi = doi;
		this.timestamp = timestamp;
	}
	
	@JsonProperty
	public String getType() {
		return type;
	}
	@JsonProperty
	public void setType(String type) {
		this.type = type;
	}
	@JsonProperty
	public String getEmail() {
		return email;
	}
	@JsonProperty
	public void setEmail(String email) {
		this.email = email;
	}
	@JsonProperty
	public String getUrl() {
		return url;
	}
	@JsonProperty
	public void setUrl(String url) {
		this.url = url;
	}
	@JsonProperty
	public String getDoi() {
		return doi;
	}
	@JsonProperty
	public void setDoi(String doi) {
		this.doi = doi;
	}
	@JsonProperty
	public String getTimestamp() {
		return timestamp;
	}
	@JsonProperty
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
}
